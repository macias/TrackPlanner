*This project serves as canary for migration to [CodeBerg](https://codeberg.org/macias/TrackPlanner).
Read more why we should [give up GitHub](https://GiveUpGitHub.org).*

# Status

It looks I am phasing out cycling from my life, so in case it is for good here it short status as of 2023-07-31. The program is operational,
but ugly and slow. Nevertheless I successfully planned 3-weeks trips using it. The crucial issues I wanted to address:

* routing speed (naive bi A-star is way too slow),
* feature-wise syncing with [TrackRadar](https://github.com/macias/TrackRadar) -- currently the turn notification part is clipped because TrackRadar
  would not understand any new hints/notifications,
* implementing "native" map rendering and converting it to desktop app,
* making it hybrid desktop and mobile (Android) app.

# About

It is a messy, ugly-looking, but yet useful (at least for the author) planner for bikepacking trips.
It can compute shortest routes from A to B, take into the account daily chores (like shopping or laundry),
so once you load your plan into [TrackRadar](https://github.com/macias/TrackRadar) you can relax and focus on
the views (unless it is uphill of course).

Please read [the documentation](documentation/README.md) to learn how to install and use the program.

# Credits/resources

* I use [JetBrains Rider](https://www.jetbrains.com/rider/) as my IDE for which I was granted open-source license (thank you!),
* map data comes from [OpenStreetMap](https://www.openstreetmap.org/); available for download from [GeoFabrik](https://download.geofabrik.de/),
* elevation data comes thanks to [Shuttle Radar Topography Mission](https://www2.jpl.nasa.gov/srtm/); available for download from [30m SRTM downloader](https://dwtkns.com/srtm30m/),
* the tile service is [Mapnik](https://mapnik.org/),
* the tile client library is [forked](https://github.com/macias/BlazorLeaflet) [Blazor Leaflet](https://github.com/Mehigh17/BlazorLeaflet),
* [Leaflet MarkerCluster](https://github.com/Leaflet/Leaflet.markercluster),
* flag and camera icons come from [Google Maps](https://www.gstatic.com/mapspro/images/stock/extended-icons5.png),
* pin icon comes from [Leaflet.awesome-markers](https://github.com/lennardv2/Leaflet.awesome-markers),
* [Chart.js](https://www.chartjs.org/) via [BlazorChartjs](https://github.com/erossini/BlazorChartjs)
(currently [forked](https://github.com/macias/BlazorChartjs)) with all the tips and tricks learned from outstanding
[ChartJS tutorials](https://www.youtube.com/c/ChartJS-tutorials).

# Author

See my PL/EN [blog](https://przypadkopis.wordpress.com/) for other, various in nature, resources.

# Alternatives

* [Google Maps](https://www.google.com/maps)
* [Mapy.cz](https://en.mapy.cz/)
* [Felt](https://felt.com/)
* [Ride with GPS](https://ridewithgps.com/?lang=en)
* [Komoot](https://www.komoot.com/)
* [Bikemap](https://www.bikemap.net/en/)
* [cycle.travel](https://cycle.travel/map)
