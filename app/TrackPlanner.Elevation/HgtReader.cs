using System;
using System.Buffers;
using System.Buffers.Binary;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Geo;
using MathUnit;
using TrackPlanner.Structures;

namespace TrackPlanner.Elevation
{
    public sealed class HgtReader : IDisposable, IHgtReader
    {
        private static readonly IHgtReader seaTile = new HgtSeaTile();
        
        public static IDisposable Create(string path,out IHgtReader hgtReader, out short min, out short max)
        {
            var file_data_count = (int) (new System.IO.FileInfo(path).Length / sizeof(short));
            if (file_data_count == 0)
            {
                min = 0;
                max = 0;
                hgtReader = seaTile;
                return CompositeDisposable.None;
            }
            
            if (!TryGetFileCoordinates(path, out var latitude, out var longitude))
                throw new ArgumentException($"Unable to parse file {path}");
            var data = ArrayPool<short>.Shared.Rent(file_data_count);
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 1, FileOptions.SequentialScan))
            {
                using (var reader = new BinaryReader(fs))
                {
                    min = short.MaxValue;
                    max = short.MinValue;
                    for (int i = 0; i < file_data_count; ++i)
                    {
                        var val = BinaryPrimitives.ReverseEndianness(reader.ReadInt16());
                        data[i] = val;

                        min = Math.Min(min, val);
                        max = Math.Max(max, val);
                    }
                }
            }

            var result = new HgtReader(System.IO.Path.GetFileNameWithoutExtension(path),
                latitude, longitude, data, file_data_count);
            hgtReader = result;
            return result;
        }

        // https://lpdaac.usgs.gov/documents/179/SRTM_User_Guide_V3.pdf

        private readonly int scaledLongitude;
        private readonly int scaledLatitude;
        private readonly int cellScale;
        private readonly short[] data;
        private readonly string DEBUG_filename;
        private readonly int dataCount;
        private readonly int dataDimension;

        private HgtReader(string DEBUG_filename, int latitude, int longitude, short[] data,int dataCount)
        {
            this.DEBUG_filename = DEBUG_filename;
            this.dataCount = dataCount;
            this.dataDimension = (int) Math.Sqrt(dataCount);
            // edges are repatead, thus -1
            this.cellScale = dataDimension - 1;
            scaledLatitude =latitude* cellScale;
            scaledLongitude = longitude * cellScale;
            this.data = data;
        }

        public void Dispose()
        {
            ArrayPool<short>.Shared.Return(this.data);
        }
        
        public static bool TryGetFileCoordinates(string path, out int latitude, out int longitude)
        {
            latitude = default;
            longitude = default;

            Match match = new Regex("^([NS])(\\d\\d)([WE])(\\d\\d\\d)")
                .Match(System.IO.Path.GetFileNameWithoutExtension(path));
            if (!match.Success)
                return false;

            // first group is the global one so we skip over it
            var captures = match.Groups.Skip<Group>(1).Select(it => it.Captures[0].Value).ToList(); 

            if (!int.TryParse(captures[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out latitude))
                return false;
            if (captures[0] == "S")
                latitude *= -1;

            if (!int.TryParse(captures[3], NumberStyles.Integer, CultureInfo.InvariantCulture, out longitude))
                return false;
            if (captures[2] == "W")
                longitude *= -1;

            return true;
        }

        public Length GetHeight(GeoPoint point)
        {
            double lat_idx = point.Latitude.Degrees * this.cellScale - this.scaledLatitude;
            double lon_idx = point.Longitude.Degrees * this.cellScale - this.scaledLongitude;

            if (lat_idx < 0 || lat_idx >= this.cellScale
                            || lon_idx < 0
                            || lon_idx >= this.cellScale)
                throw new ArgumentException($"Point {point} is out of the cell boundaries.");

            double north_west = getValue((int) Math.Ceiling(lat_idx), (int) Math.Floor(lon_idx));
            double north_east = getValue((int) Math.Ceiling(lat_idx), (int) Math.Ceiling(lon_idx));
            double south_west = getValue((int) Math.Floor(lat_idx), (int) Math.Floor(lon_idx));
            double south_east = getValue((int) Math.Floor(lat_idx), (int) Math.Ceiling(lon_idx));

            // first we computed weighted height for west and east edge ...
            var west = north_west * (lat_idx - (int) Math.Floor(lat_idx)) + south_west * ((int) Math.Ceiling(lat_idx) - lat_idx);
            var east = north_east * (lat_idx - (int) Math.Floor(lat_idx)) + south_east * ((int) Math.Ceiling(lat_idx) - lat_idx);

            // ... and then we compute final weighted height
            var height = west * ((int) Math.Ceiling(lon_idx) - lon_idx) + east * (lon_idx - (int) Math.Floor(lon_idx));

            //if (height < -50 || height > 200)
//                throw new Exception($"Elevation failed: {this.DEBUG_filename} {point}, values {north_west}, {north_east}, {south_east}, {south_west}, computed {height}");
            return Length.FromMeters(height);
        }

        private short getValue(int latitudeIndex, int longitudeIndex)
        {
            var index = (this.dataDimension-1 - latitudeIndex) * this.dataDimension + longitudeIndex;
            if (index >= this.dataCount)
                throw new ArgumentOutOfRangeException($"{nameof(index)} = {index} >= {this.dataCount}.");
            return this.data[index];
        }
    }
}