using Geo;
using MathUnit;

namespace TrackPlanner.Elevation
{
    public interface IHgtReader
    {
        Length GetHeight(GeoPoint point);
    }
}