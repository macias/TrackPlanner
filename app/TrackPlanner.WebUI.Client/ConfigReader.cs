using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Toast;
using Force.DeepCloner;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.WebUI.Client
{
    public static class ConfigReader
    {
        private static readonly ProxySerializer serializer = new ProxySerializer();
        
        public static (SystemConfiguration systemConfiguration,
            ScheduleSettings scheduleSettings, GlobalSettings globalSettings) ReadConfigurations(string configContent)
        {
            var config = JObject.Parse(configContent)!;

            var system_configuration = GetConfig<SystemConfiguration>(SystemConfiguration.SectionName, config);
            system_configuration.Check();

            var schedule_settings = GetConfig<ScheduleSettings>(ScheduleSettings.SectionName, config);
            schedule_settings.Check();

            var global_settings = GetConfig<GlobalSettings>(GlobalSettings.SectionName, config);
            global_settings.Check();

            return (system_configuration, schedule_settings, global_settings);
        }

        private static T GetConfig<T>(string sectionName, JObject config)
        {
            // ConfigurationBinder is buggy as hell
            return serializer.Deserialize<T>( config[sectionName]!.ToString())!;
        }
    }
}