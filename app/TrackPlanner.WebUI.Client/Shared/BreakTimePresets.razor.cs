using System;
using Microsoft.AspNetCore.Components;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.WebUI.Client.Shared
{
    public partial class BreakTimePresets
    {
        [Parameter] [EditorRequired] public UserPlannerPreferences PlannerPreferences { get; set; } = default!;

       // private TimeSpan value;
        [Parameter] [EditorRequired] public TimeSpan Value { get; set; }
        /*{
            get { return this.value; }
            set { SetBreakTimeAsync(value); }
        }*/

        [Parameter] public EventCallback<TimeSpan> ValueChanged { get; set; }

        public BreakTimePresets()
        {
        }
        
        private void SetBreakTimeAsync(TimeSpan value)
        {
            if (this.Value == value)
                return;
            this.Value = value;
            _ = ValueChanged.InvokeAsync(Value);
        }


    }
}