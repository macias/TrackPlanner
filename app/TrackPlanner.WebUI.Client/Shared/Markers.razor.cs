using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Blazored.Modal.Services;
using BlazorLeaflet;
using BlazorLeaflet.Models;
using BlazorLeaflet.Models.Events;
using Geo;
using MathUnit;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using TrackPlanner.RestClient;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.WebUI.Client.Pages;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Structures;
using TrackPlanner.WebUI.Client.Dialogs;
using TrackPlanner.WebUI.Client.Widgets;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Client.Shared
{
    public partial class Markers : IDisposable
    {
        private readonly EditContext editContext;

        public VisualSchedule<TNodeId, TRoadId> visualSchedule => this.TotalManager.Schedule;
        public IReadOnlyList<IVisualDay> Days => this.TotalManager.Schedule.Days;
        [Inject] public IModalService Modal { get; set; } = default!;
        [Inject] public ILoggerFactory LoggerFactory { get; set; } = default!;

        private CommonDialog commonDialog = default!;
        private Microsoft.Extensions.Logging.ILogger msLogger = default!;

        [Parameter] public IMapManager MapManager { get; set; } = default!;
        [Inject] public IJSRuntime JsRuntime { get; set; } = default!;

        [Parameter] [EditorRequired] public ScheduleManager<WorldIdentifier,WorldIdentifier>  TotalManager { get; set; } = default!;

        public Markers()
        {
            this.editContext = new EditContext(this);
        }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

            this.commonDialog = new CommonDialog(JsRuntime);
            this.msLogger = LoggerFactory.CreateLogger("Markers");
        }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }

        public void Dispose()
        {
        }

        public void Test()
        {
            //   attachAttractionMarker(new LatLng(53.182995f, 18.484497f ), "foobar","whatever");
        }

        private void onDayToggled(AccordionToggle toggle) // this one was already toggled
        {
            TotalManager.OnDayToggled(toggle.Tag,toggle.Collapsed);
        }

        private async void uiAnchorClickedAsync(MouseEventArgs mouseArgs, int dayIndex,
            int checkpointIndex)
        {
            // double click would be more convienient, but not within web:
            // https://stackoverflow.com/a/63633319/210342
            if (mouseArgs.CtrlKey)
            {
                await this.MapManager.CheckpointFocusAsync(dayIndex, checkpointIndex);
            }
            else
                await this.MapManager.EditCheckpointDetailsAsync(dayIndex, checkpointIndex);
        }

        private async void uiOrderAnchorPreviousAsync( int dayIndex, int anchorIndex)
        {
            await this.TotalManager.Dispatcher.ExecuteAsync(OrderAnchor.Previous(dayIndex, anchorIndex),
                CancellationToken.None);
        }

        private async void uiOrderAnchorNextAsync( int dayIndex, int anchorIndex)
        {
            await this.TotalManager.Dispatcher.ExecuteAsync(OrderAnchor.Next(dayIndex, anchorIndex),
                CancellationToken.None);
        }

        public AttrMarker CreateAttractionMarker(LatLng coords, string title, string? tooltip)
        {
            string? html = null;
            if (!String.IsNullOrEmpty(title))
                html = $"<div class='attraction-icon-text'>{title}</div>";
            var icon = new DivIcon()
            {
                Size = new Size(36, 46),
                Anchor = new Point(18, 43),
                PopupAnchor = new Point(3, -40),
                ClassName = "camera-icon",
                Html = html,
            };

            var marker = new AttrMarker(coords)
            {
                Draggable = false,
                Title = title,
                //Popup = new Popup {Content = $"I am at {coords.Lat:0.00}° lat, {coords.Lng:0.00}° lng"},
                Icon = icon,
            };
            if (!String.IsNullOrEmpty(tooltip))
                marker.Tooltip = new Tooltip {Content = tooltip};

            // Console.WriteLine($"Attaching marker with title {marker.Title} and icon {icon.Html}");

            return marker;
        }




        private async void uiAddMarkerByPositionAsync()
        {
            var null_coords = await askForPositionAsync(position: null);
            if (null_coords is { } coords)
                await this.TotalManager.Dispatcher.ExecuteAsync(new AddAnchorAtPlaceholder(coords),
                    CancellationToken.None);
        }

        private async ValueTask<GeoPoint?> askForPositionAsync(GeoPoint? position)
        {
            var initial = DataFormat.FormatUserInput(position);
            string? input = await this.commonDialog.PromptAsync("Anchor position", initial);
            return DataFormat.TryParseUserPoint(input);
        }


        private async Task uiGetAttractionsAsync(int dayIndex)
        {
            using (Modal.ShowGuardDialog("Looking for attractions..."))
            {
                await this.TotalManager.Dispatcher.ExecuteAsync(new FindRouteAttractions(dayIndex,
                    PointOfInterest.Category.Historic),CancellationToken.None);
            }
        }

        private async Task uiGetFoodStoresAsync(int dayIndex)
        {
            using (Modal.ShowGuardDialog("Looking for shops..."))
            {
                await this.TotalManager.Dispatcher.ExecuteAsync(new FindRouteAttractions(dayIndex,
                    PointOfInterest.Category.Utility),CancellationToken.None);
            }
        }
    

   
        public void RefreshState()
        {
            StateHasChanged();
        }

        private async Task uiClearAttractionsAsync(int dayIdx)
        {
            await this.TotalManager.Dispatcher.ExecuteAsync(new ClearAttractions(dayIdx), CancellationToken.None);
        }

      
        private async Task uiDeletePlaceholderAsync()
        {
            await this.TotalManager.Dispatcher.ExecuteAsync(new DeletePlaceholder(),CancellationToken.None);
        }


        private async Task uiSetPlaceholderAsync(int dayIndex, int anchorIndex)
        {
            await this.TotalManager.Dispatcher.ExecuteAsync(new SetPlaceholder(dayIndex, anchorIndex),CancellationToken.None);
        }
    }
}