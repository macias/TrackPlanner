using System.Threading.Tasks;
using BlazorLeaflet;

namespace TrackPlanner.WebUI.Client
{
    public interface IMapManager
    {
        Map Map { get; }
      ValueTask EditCheckpointDetailsAsync(int dayIndex, int checkpointIndex);
      ValueTask CheckpointFocusAsync(int dayIndex, int checkpointIndex);
    }
}