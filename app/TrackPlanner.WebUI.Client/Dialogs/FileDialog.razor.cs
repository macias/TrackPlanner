using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.WebUI.Client.Dialogs
{
    public partial class FileDialog
    {
        
        public enum DialogKind
        {
            Open,
            Save
        }

        [Inject] public IJSRuntime jsRuntime { get; set; } = default!;
        [CascadingParameter] private BlazoredModalInstance ModalInstance { get; set; } = default!;
        [Parameter] public IPlanClient PlanClient { get; set; } = default!;
        [Parameter] public DialogKind Kind { get; set; }
        private string currentDirectory = "";
        private string? selectedFileName = null;
        private DirectoryData? directoryData;
        private CommonDialog commonDialog = default!;
        private readonly EditContext editContext;

        public FileDialog()
        {
            this.editContext = new EditContext(this);
        }
        
        protected override async Task OnInitializedAsync()
        {
           
            await base.OnInitializedAsync();
        }

        protected override async Task OnParametersSetAsync()
        {
            this.commonDialog = new CommonDialog(jsRuntime);
            this.currentDirectory = this.PlanClient.LastUsedDirectory ?? "";
            
            await getDirectoryDataAsync();

            await base.OnParametersSetAsync();
        }

        private async Task getDirectoryDataAsync()
        {
            this.selectedFileName = null;
            
            var result =  await this.PlanClient.GetScheduleDirectoryAsync(this.currentDirectory,
                CancellationToken.None);

            if (result.HasValue)
                this.directoryData = result.Value;
            else
            {
                this.directoryData = null;
                Console.WriteLine(result.ProblemMessage);
            }

            StateHasChanged();
        }

        private async void backToParent()
        {
            this.currentDirectory = System.IO.Path.GetDirectoryName(this.currentDirectory)!;

            await getDirectoryDataAsync();
        }
        
        private async void changeDirectory(string dir)
        {
            this.currentDirectory = System.IO.Path.Combine(this.currentDirectory, dir);

            await getDirectoryDataAsync();
        }

        private void selectFile(string file)
        {
            this.selectedFileName = file;
        }
        
        private async void confirmDialog()
        {
            if (Kind == DialogKind.Open)
            {
                if (!this.directoryData!.Files.Contains(this.selectedFileName))
                {
                    await this.commonDialog.AlertAsync("File does not exist.");
                    return;
                }
            }
            else
            {
                if (this.directoryData!.Files.Contains(this.selectedFileName) && !await this.commonDialog.ConfirmAsync("Overwrite existing file?"))
                    return;
            }
            string path = System.IO.Path.Combine(this.currentDirectory, this.selectedFileName!);
            await ModalInstance.CloseAsync(ModalResult.Ok(path));
        }

        private async void cancelDialog()
        {
            await ModalInstance.CancelAsync();
        }
    }
}