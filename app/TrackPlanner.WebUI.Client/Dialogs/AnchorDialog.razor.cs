using System;
using System.Threading.Tasks;
using System.Xml.XPath;
using Blazored.Modal;
using Blazored.Modal.Services;
using Geo;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Visual;

namespace TrackPlanner.WebUI.Client.Dialogs
{
   
    public partial class AnchorDialog
    {
        [CascadingParameter] private BlazoredModalInstance ModalInstance { get; set; } = default!;
        private readonly EditContext editContext;
        private GeoPoint initPoint = default!;
        private readonly ApproximateCalculator calc;
        private string point { get; set; } = default!;
        private string anchorLabel { get; set; } = default!;
        private TimeSpan startTime { get; set; }
        private TimeSpan breakTime { get; set; }
        private bool allowGap { get; set; }
        private bool hasMap { get; set; }
        
        [Parameter] [EditorRequired] public ISummarySchedule Schedule { get; set; } = default!;
        [Parameter] [EditorRequired] public int CheckpointDayIndex { get; set; } = default!;
        [Parameter] [EditorRequired] public int CheckpointIndex { get; set; } = default!;

        public AnchorDialog()
        {
            this.editContext = new EditContext(this);
            this.calc = new ApproximateCalculator();
        }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();

            var (day, anchor) = getDayAnchor();

            this.startTime = day.Start;
            this.breakTime = anchor.UserBreak;
            {
                this.calc.TryStabilizeUserData(anchor.UserPoint, out this.initPoint, out var p);
                this.point = p;
            }
            this.anchorLabel = anchor.Label;
            this.allowGap = anchor.AllowGap;
            this.hasMap = anchor.HasMap;
        }


        private (IReadOnlyDay,IReadOnlyAnchor) getDayAnchor()
        {
            var (day_anchor_idx, anchor_idx) = Schedule.CheckpointIndexToAnchor(CheckpointDayIndex,
                CheckpointIndex);
            var day = Schedule.Days[day_anchor_idx];
            var anchor = day.Anchors[anchor_idx];
            return (day,anchor);
        }

        private Task uiConfirmDialogAsync()
        {
            var (day,anchor) = getDayAnchor();
            
            var res_point = DataFormat.TryParseUserPoint(point);
            return ModalInstance.CloseAsync(ModalResult.Ok(
                new AnchorEdit(label:anchor.Label.Trim()==anchorLabel.Trim()?null:anchorLabel,
                    startTime: day.Start == startTime ? null : startTime,
                breakTime: anchor.UserBreak == breakTime ? null : breakTime,
                point: res_point == null || res_point == this.initPoint ? null : res_point.Value,
                    allowGap:anchor.AllowGap == allowGap ? null : allowGap,
                    hasMap:anchor.HasMap == hasMap ? null : hasMap)));
        }

        private Task uiMergeDayToPreviousAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorEdit.DayMergePrevious));
        }

        private Task uiPinMarkerAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorEdit.AnchorPin));
        }
        
        private Task uiFindPeaksAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorEdit.FindPeaks));
        }
        
        private Task uiRemoveSingleAnchorAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorEdit.AnchorDelete));
        }
        
        private Task uiSplitDayAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorEdit.DaySplit));
        }

        private Task uiOrderNextAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorEdit.OrderNext));
        }

        private Task uiOrderPreviousAsync()
        {
            return ModalInstance.CloseAsync(ModalResult.Ok(AnchorEdit.OrderPrevious));
        }

        private Task uiancelDialogAsync()
        {
            return ModalInstance.CancelAsync();
        }
    }
}