using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;
using TrackPlanner.RestClient;
using TrackPlanner.WebUI.Client.Data;

namespace TrackPlanner.WebUI.Client.Dialogs
{
    // https://stackoverflow.com/a/58346898/210342
    public partial class LayerDialog
    {
        [CascadingParameter] private BlazoredModalInstance ModalInstance { get; set; } = default!;
        [Parameter] [EditorRequired] public List<UserLayer>  Entries { get; set; } = default!;
        
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
        }

        private async void uiCancelDialog()
        {
            await ModalInstance.CancelAsync();
        }
        private async void uiConfirmDialog()
        {
            await ModalInstance.CloseAsync(ModalResult.Ok(Entries));
        }
    }
}