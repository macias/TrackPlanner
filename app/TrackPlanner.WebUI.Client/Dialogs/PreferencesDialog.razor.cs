using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Force.DeepCloner;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.WebUI.Client.Dialogs
{
    public partial class PreferencesDialog
    {
        [CascadingParameter] private BlazoredModalInstance ModalInstance { get; set; } = default!;
        [Parameter] [EditorRequired] public ScheduleSettings Settings { get; set; } = default!;
        private readonly EditContext editContext;

        public PreferencesDialog()
        {
            this.editContext = new EditContext(this);
        }
        
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
        }
        
        private  void useDefaults()
        {
            Settings = Program.ScheduleSettings.DeepClone();
            StateHasChanged();
        }
   
      
        private async void confirmDialog()
        {
            await ModalInstance.CloseAsync(ModalResult.Ok(Settings));
        }

        private async void cancelDialog()
        {
            await ModalInstance.CancelAsync();
        }
    }
}