namespace TrackPlanner.WebUI.Client.Data
{
    public sealed class UserLayer
    {
        public string Name { get; }
        public bool IsEnabled { get; set; }

        public UserLayer(string name)
        {
            Name = name;
        }
    }
}