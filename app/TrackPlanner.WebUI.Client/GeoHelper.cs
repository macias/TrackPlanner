﻿using BlazorLeaflet.Models;
using Geo;

namespace TrackPlanner.WebUI.Client
{
    public static class GeoHelper
    {
        public static LatLng GeoPointToLatLng(in GeoPoint pt)
        {
            return new LatLng((float) pt.Latitude.Degrees, (float) pt.Longitude.Degrees);
        }

        public static GeoPoint GeoPointFromLatLng(LatLng pt)
        {
            return GeoPoint.FromDegrees(pt.Lat, pt.Lng);
        }
   
       
    }
}
