using System;
using System.Threading.Tasks;
using BlazorLeaflet.MarkerCluster;
using Microsoft.JSInterop;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Client.Widgets
{
    public sealed class AttractionsLayer : IAsyncDisposable
    {
        public static async ValueTask<AttractionsLayer> CreateAsync(IJSRuntime jsRuntime, string name)
        {
            return new AttractionsLayer(name, await MarkerClusterGroup.CreateAsync(jsRuntime).ConfigureAwait(false));
        }

        public string Name { get; }
        public MarkerClusterGroup MarkerCluster { get; }

        private AttractionsLayer(string name, MarkerClusterGroup markerCluster)
        {
            this.MarkerCluster = markerCluster;
            Name = name;
        }

        public ValueTask DisposeAsync()
        {
            return this.MarkerCluster.DisposeAsync();
        }
    }
}