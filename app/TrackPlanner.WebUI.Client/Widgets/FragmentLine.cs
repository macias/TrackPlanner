using BlazorLeaflet.Models;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.WebUI.Client.Widgets
{
    public sealed class FragmentLine<TNodeId,TRoadId> : Polyline
        where TNodeId:struct
    {
        public LegPlan<TNodeId,TRoadId> LegRef { get; }
        public LegFragment<TNodeId,TRoadId> FragmentRef { get; }

        public FragmentLine(LegPlan<TNodeId,TRoadId> legRef, LegFragment<TNodeId,TRoadId> fragmentRef)
        {
            LegRef = legRef;
            FragmentRef = fragmentRef;
        }
    }
}