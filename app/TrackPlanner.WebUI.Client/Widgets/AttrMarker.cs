using System.Drawing;
using BlazorLeaflet.Models;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.WebUI.Client.Widgets
{
    public sealed class AttrMarker : Marker
    {
        public PlacedAttraction? Attraction { get; set; }

        public AttrMarker(LatLng latLng) : base(latLng)
        {
        }

        public AttrMarker(float x, float y) : base(x, y)
        {
        }

        public AttrMarker(PointF position) : base(position)
        {
        }
    }
}