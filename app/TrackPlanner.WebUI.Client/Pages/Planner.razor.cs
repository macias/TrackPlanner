using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Blazor.DownloadFileFast.Interfaces;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazored.Toast.Services;
using BlazorLeaflet;
using BlazorLeaflet.MarkerCluster;
using BlazorLeaflet.Models;
using BlazorLeaflet.Models.Events;
using Force.DeepCloner;
using Geo;
using MathUnit;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Visual;
using TrackPlanner.WebUI.Client.Shared;
using TrackPlanner.RestClient;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.RestSymbols;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.WebUI.Client.Data;
using TrackPlanner.WebUI.Client.Dialogs;
using TrackPlanner.WebUI.Client.Widgets;
using ILogger = Microsoft.Extensions.Logging.ILogger;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Client.Pages
{
    public partial class Planner : IMapManager, IVisualReceiver, IVisualActor
    {
        private LatLng onDragMarkerLatLng = default!;

        public Map Map { get; private set; } = default!;

        private Markers markers = default!;

        private VisualSchedule<TNodeId, TRoadId> visualSchedule => this.totalManager.Schedule;

        public bool AutoBuild
        {
            get { return this.totalManager.AutoBuild; }
            set { this.dispatcher.ExecuteAsync(new AutoBuildSwitch(value), CancellationToken.None); }
        }

        public bool ShowElevation { get; set; }

        private IEnumerable<FragmentLine<TNodeId, TRoadId>> fragmentLines => this.Map.NewLayers
            .Select(it => (it as FragmentLine<TNodeId, TRoadId>)!)
            .Where(it => it != null);

        private IEnumerable<AnchorMarker> anchorMarkers => this.Map.NewLayers
            .Select(it => (it as AnchorMarker)!)
            .Where(it => it != null);


        private bool alreadyRendered;

        private CommonDialog commonDialog = default!;
        private Microsoft.Extensions.Logging.ILogger msLogger = default!;
        private Marker? traceMarker;
        private readonly ApproximateCalculator calculator;
        private UiLogger uiLogger = default!;
        private ScheduleManager<WorldIdentifier, WorldIdentifier> totalManager = default!;
        private DispatchRecorder<TNodeId, TRoadId> dispatcher => this.totalManager.Dispatcher;
        public RestPlannerClient<TNodeId, TRoadId> RestPlannerClient { get; private set; } = default!;
        public MarkerClusterGroup RouteAttractionsCluster { get; private set; } = default!;
        private List<AttractionsLayer> attractionLayers;
        private List<Marker> peaksMarkers;
        private bool renderedShowElevation;
        private Polyline? placeholderLine;
        private FragmentLine<WorldIdentifier,WorldIdentifier>[] legLiveReplacement;
        private LivePlaceholderLine? liveLineCoords;
        // when converting one marker (via double click) to another which handles also mouse clicking
        // the original click event leaks to the newly created. Going through JS/Leaftlet is too much
        // at the moment, thus this hack
        private bool HACK_HandlingMouseClick;

        private IVisualReceiver asVisualReceiver => this;
        private IVisualActor asActor => this;

        [Inject] public IToastService ToastService { get; set; } = default!;
        [Inject] public IJSRuntime JsRuntime { get; set; } = default!;
        [Inject] public JsonRestClient Rest { get; set; } = default!;
        [Inject] public IBlazorDownloadFileService DownloadService { get; set; } = default!;
        [Inject] public IModalService Modal { get; set; } = default!;
        [Inject] public ILoggerFactory LoggerFactory { get; set; } = default!;

        private bool uiBuildEnabled => this.visualSchedule.HasEnoughAnchorsForBuild();

        public Planner()
        {
            this.calculator = new ApproximateCalculator();
            this.attractionLayers = new List<AttractionsLayer>();
            this.ShowElevation = false;
            this.peaksMarkers = new List<Marker>();
            this.legLiveReplacement = Array.Empty<FragmentLine<WorldIdentifier, WorldIdentifier>>();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);

            if (!this.alreadyRendered)
            {
                this.alreadyRendered = true;
            }

            if (this.renderedShowElevation != this.ShowElevation && this.Map is { } m)
                await m.InvalidateSizeAsync();
            
            renderedShowElevation = this.ShowElevation;
        }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

            this.msLogger = LoggerFactory.CreateLogger("Planner");
            this.uiLogger = new UiLogger(this.msLogger);

            this.msLogger.LogDebug($"Creating map with {Program.SystemConfiguration.TileServer}");

            this.commonDialog = new CommonDialog(JsRuntime);
            this.RestPlannerClient = new RestPlannerClient<TNodeId, TRoadId>(this.uiLogger, Rest,
                Program.SystemConfiguration.PlannerServer);
            this.Map = new Map(JsRuntime)
            {
                Center = new LatLng {Lat = 53.16844f, Lng = 18.73222f},
                Zoom = 9.8f
            };

            this.totalManager = new ScheduleManager<TNodeId, TRoadId>(this.uiLogger, this,
                this.calculator,
                Program.ScheduleSettings,
                Program.GlobalSettings.VisualPreferences,
                RestPlannerClient, this,
                Program.GlobalSettings.UndoHistory,
                Program.GlobalSettings.AutoBuild);
            this.RouteAttractionsCluster = await MarkerClusterGroup.CreateAsync(JsRuntime);

            this.Map.OnInitialized += async () =>
            {
                var tile_layer = new BlazorLeaflet.Models.TileLayer
                {
                    UrlTemplate = Program.SystemConfiguration.TileServer + "{z}/{x}/{y}.png",

                    Attribution = "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors",
                };
                this.Map.AddLayer(tile_layer);
                if (this.visualSchedule.Settings.PlannerPreferences.Home is { } home)
                    this.Map.Center = GeoHelper.GeoPointToLatLng(home);
                this.Map.OnClick += onMapClickAsync;
                this.Map.OnMouseOut += onMapMouseOut; // does not work

                this.markers.Test();
                //TestAsync();

                await Map.AddNewLayerAsync(this.RouteAttractionsCluster);
                await this.totalManager.InitializeDefaultAsync();
            };
        }

        private async Task uiDeletePlaceholderAsync()
        {
            await this.totalManager.Dispatcher.ExecuteAsync(new DeletePlaceholder(),CancellationToken.None);
        }

        private async void onMapMouseOut(Map sender, MouseEvent e)
        {
            await clearPlaceholderLineAsync();
        }

        private void DownloadAsync()
        {
            /*  if (this.Plan == null)
              {
                  return;
              }
  
              byte[] bytes;
              using (var stream = new MemoryStream())
              {
                  TrackPlanner.DataExchange.TrackWriter.SaveAsKml(Program.Configuration, stream,"something meaningful", this.Plan);
                  bytes = stream.ToArray();
              }
  
              await DownloadService.DownloadFileAsync("export.kml", bytes, "application/vnd.google-earth.kml+xml");
  */
        }


        ValueTask IVisualReceiver.OnAttractionsAddedAsync(IEnumerable<PlacedAttraction> attractions)
        {
            return this.addAttractionMarkersAsync(this.RouteAttractionsCluster, attractions, isExternal: false);
        }

        private async ValueTask addAttractionMarkersAsync(MarkerClusterGroup cluster,
            IEnumerable<PlacedAttraction> attractions, bool isExternal)
        {
            foreach (var placed_attr in attractions)
            {
                var (title, description) = placed_attr.Attraction.GetDisplay();
                  var attr_marker = this.markers.CreateAttractionMarker(GeoHelper.GeoPointToLatLng(placed_attr.Point.Convert2d()),
                    title, tooltip: description);
                attr_marker.OnClick += onAttractionMarkerClick;
                attr_marker.OnDblClick += isExternal ? externalAttractionMarkerOnDblClick : routeAttractionMarkerOnDblClick;

                attr_marker.Attraction = placed_attr;
                await cluster.AddLayerAsync(attr_marker);
            }
        }

        private void onAttractionMarkerClick(InteractiveLayer sender, MouseEvent e)
        {
            var marker = (sender as Marker)!;
            this.msLogger.LogInformation(marker.Tooltip?.Content);
        }


        async ValueTask IVisualReceiver.OnAllLegsRemovingAsync()
        {
            int count = 0;
            while (this.fragmentLines.Any())
            {
                await removeLegLayerAsync(fragmentLines.First());

                ++count;
            }

            this.msLogger.LogDebug($"Removed {count} fragment layers.");
        }

        private async ValueTask removeLegLayerAsync(InteractiveLayer layer)
        {
            await this.Map.RemoveNewLayerAsync(layer);
            await layer.DisposeAsync();
        }

        async ValueTask IVisualReceiver.OnPeakAddedAsync(GeoZPoint peakPoint)
        {
            var marker = createPeakMarker(GeoHelper.GeoPointToLatLng(peakPoint.Convert2d()),
                DataFormat.FormatHeight(peakPoint.Altitude, withUnit: true));
            marker.OnDblClick += async (sender, @event) =>
            {
                this.HACK_HandlingMouseClick = true;
                await this.dispatcher.ExecuteAsync(new SnapToPeak(peakPoint),CancellationToken.None).ConfigureAwait(false);
                this.HACK_HandlingMouseClick = false;
                this.peaksMarkers.Remove(marker);
                await this.Map.RemoveNewLayerAsync(marker).ConfigureAwait(false);
                await marker.DisposeAsync().ConfigureAwait(false);
            }; 

            this.peaksMarkers.Add(marker);
            await this.Map.AddNewLayerAsync(marker);
        }
        
        private async Task uiClearPeaksAsync()
        {
            await this.dispatcher.ExecuteAsync(new ClearPeaks(), CancellationToken.None);
        }
        
        private async Task uiClearAllAttractionsAsync()
        {
            await this.dispatcher.ExecuteAsync(new ClearAttractions(null), CancellationToken.None);
        }

        async ValueTask IVisualReceiver.OnPeaksClearedAsync()
        {
            foreach (var marker in this.peaksMarkers)
            {
                await this.Map.RemoveNewLayerAsync(marker);
                await marker.DisposeAsync();
            }
            peaksMarkers.Clear();
        }

        public async Task newProjectAsync()
        {
                await this.dispatcher.ExecuteAsync(new NewProject(), CancellationToken.None);
        }
        
        ValueTask<bool> IVisualActor.ConfirmAsync(string message)
        {
            return this.commonDialog.ConfirmAsync(message);
        }


        private async Task uiLoadScheduleAsync()
        {
            if (this.visualSchedule.IsModified)
            {
                if (!await this.commonDialog.ConfirmAsync("Current plan is modified, load anyway?"))
                    return;
            }

            var schedule_path = await Modal.ShowFileDialogAsync(this.RestPlannerClient, "Load schedule", FileDialog.DialogKind.Open);
            if (schedule_path == null)
            {
                this.msLogger.LogInformation("Modal was cancelled");
                return;
            }

            await this.dispatcher.ExecuteAsync(new Load(schedule_path), CancellationToken.None);
        }

        private async Task saveScheduleAsync()
        {
            string? schedule_path = this.visualSchedule.Filename;
            if (schedule_path == null)
            {
                schedule_path = await Modal.ShowFileDialogAsync(this.RestPlannerClient,"Save schedule",
                    FileDialog.DialogKind.Save);
                if (schedule_path == null)
                {
                    this.msLogger.LogInformation("Modal was cancelled");
                    return;
                }

                if (System.IO.Path.GetExtension(schedule_path).ToLowerInvariant() != SystemCommons.ProjectFileExtension)
                    schedule_path += SystemCommons.ProjectFileExtension;
            }

            await this.dispatcher.ExecuteAsync(new Save(schedule_path), CancellationToken.None);
        }

        public async ValueTask RecreateMarkersAsync(int dayIndex, int anchorIndex)
        {
            foreach (var (anchor, d_idx, a_idx) in this.visualSchedule.IndexedAnchors(dayIndex, anchorIndex))
            {
                var day = this.visualSchedule.Days[d_idx];

                var marker = tryFindMarker(anchor)!;

                string title = ScheduleSummaryExtension.GetMarkerCode(d_idx, a_idx);

                if (title == marker.Title && IconClassName(this.visualSchedule.IsAnchorImportant(d_idx, a_idx), anchor.IsPinned) == marker.Icon.ClassName)
                {
                    continue;
                }

                await MarkerDeletedAsync(marker);
                await this.AttachAnchorMarkerAsync(anchor, d_idx, a_idx);
            }
        }

        public static string IconClassName(bool isImportant, bool isPinned)
        {
            if (isPinned)
                return isImportant ? "orange-number-icon" : "navy-number-icon";
            else
                return "auto-anchor-icon";
        }


        ValueTask IVisualReceiver.OnAnchorRemovingAsync(VisualAnchor anchor)
        {
            var marker = tryFindMarker(anchor);
            return this.MarkerDeletedAsync(marker!);
        }

        private AnchorMarker? tryFindMarker(IReadOnlyAnchor anchor)
        {
            return this.anchorMarkers.SingleOrDefault(it => it.Anchor == anchor);
        }

        ValueTask IVisualReceiver.OnAnchorChangedAsync(int dayIndex, int anchorIndex)
        {
            return this.RecreateMarkersAsync(dayIndex, anchorIndex);
        }

        void IVisualReceiver.OnDaySplit(int dayIndex)
        {
        }

        void IVisualReceiver.OnDayMerged(int dayIndex)
        {
        }

        async ValueTask IVisualReceiver.OnAttractionsRemovingAsync(IEnumerable<PlacedAttraction> attractions)
        {
            string? problem = null;

            foreach (var attr in attractions)
            {
                AttrMarker? marker = this.RouteAttractionsCluster
                    .FindMarkerOrDefault<AttrMarker>(it => it.Attraction == attr);
                if (marker == null)
                {
                    problem ??= "Cannot find marker based on provided attraction";
                }
                else
                {
                    if (!await this.RouteAttractionsCluster.RemoveLayerAsync(marker))
                        problem ??= "There is no such marker within cluster";
                    await marker.DisposeAsync();
                }
            }

            await this.asActor.AlertAsync(problem, MessageLevel.Warning);
        }


        public async ValueTask MarkerDeletedAsync(Marker marker)
        {
            if (!await this.Map.RemoveNewLayerAsync(marker))
                this.msLogger.LogError("We don't have given marker on the map.");
            await marker.DisposeAsync();
        }

        private static Color getColor(LineDecoration lineDecoration)
        {
            return Color.FromArgb(lineDecoration.GetArgbColor());
        }

        private async void onLineMouseOverAsync(InteractiveLayer sender, MouseEvent e)
        {
            if (!e.OriginalEvent.ShiftKey)
                return;

            var line = sender as FragmentLine<TNodeId, TRoadId>;
            if (line == null)
            {
                this.msLogger.LogError($"Unknown segment");
                return;
            }

            (string running, string remaining, string label) = this.totalManager.GetFragmentInformation(
                line.LegRef, line.FragmentRef, GeoPoint.FromDegrees(e.LatLng.Lat, e.LatLng.Lng));

            var p = new Popup
            {
                Position = e.LatLng,
                Content = $"{running}<br/>{remaining}<br/>{label}"
            };

            await p.OpenOnAsync(this.Map);
            await Task.Delay(Program.GlobalSettings.PopupTimeout);
            await p.CloseAsync(this.Map);
        }

        private async Task uiPartialBuildPlanAsync()
        {
            await this.dispatcher.ExecuteAsync(new PartialBuild(), CancellationToken.None);
        }

        private async Task uiCompleteRebuildPlanAsync()
        {
            await this.dispatcher.ExecuteAsync(new CompleteRebuild(), CancellationToken.None);
        }

        private async Task uiUndoAsync()
        {
            await this.dispatcher.ExecuteAsync(new Undo(), CancellationToken.None);
        }

        private async Task uiRedoAsync()
        {
            await this.dispatcher.ExecuteAsync(new Redo(), CancellationToken.None);
        }

        private Marker createPeakMarker(LatLng coords, string title)
        {
            DivIcon icon;
            icon = new DivIcon()
            {
                Size = new Size(36, 46),
                Anchor = new Point(18, 43),
                PopupAnchor = new Point(3, -40),
                ClassName = "mountain-icon",
                Html = $"<div class='attraction-icon-text'>{title}</div>",
            };

            var marker = new Marker(coords)
            {
                Draggable = false,
                Title = title,
                //Popup = new Popup {Content = $"I am at {coords.Lat:0.00}° lat, {coords.Lng:0.00}° lng"},
                Icon = icon,
            };
            
            // Console.WriteLine($"Attaching marker with title {marker.Title} and icon {icon.Html}");

            return marker;
        }


        async ValueTask IVisualReceiver.OnLegsAddedAsync(int firstLegIndex, int legCount)
        {
            foreach (var leg in this.visualSchedule.Route.Legs.Skip(firstLegIndex).Take(legCount))
            {
               for (int frag_idx=0;frag_idx< leg.Fragments.Count;++frag_idx)
                {
                    var fragment = leg.Fragments[frag_idx];
                    var line = await addLineAsync(new FragmentLine<TNodeId, TRoadId>(leg, fragment),
                        fragment.IsForbidden
                        ? Program.GlobalSettings.VisualPreferences.ForbiddenStyle
                        : Program.GlobalSettings.VisualPreferences.SpeedStyles[fragment.SpeedStyling.Style],
                        fragment.GetSteps().Select(it => it.Point.Convert2d()).ToArray());

                    line.OnMouseOver += onLineMouseOverAsync;
                }
            }
        }

        async ValueTask IVisualReceiver.OnAllAnchorsRemovingAsync()
        {
            while (this.anchorMarkers.Any())
                await MarkerDeletedAsync(anchorMarkers.First());
        }

        private async Task uiEditPreferencesAsync()
        {
            var prefs = await Modal.ShowPreferencesDialogAsync("Schedule preferences",
                this.visualSchedule.Settings);
            await this.dispatcher.ExecuteAsync(new SetSettings(prefs), CancellationToken.None);
        }

        private async Task uiSaveDebugAsync()
        {
            using (Modal.ShowGuardDialog("Saving debug..."))
            {
                string content = String.Join("",
                    this.dispatcher.HistoryAsCode().Select(it => $"{it}{Environment.NewLine}"));
                await this.RestPlannerClient.SaveDebugAsync(content, CancellationToken.None);
            }
        }

        async ValueTask IVisualReceiver.OnAnchorAddedAsync(VisualAnchor anchor, int dayIndex, int anchorIndex)
        {
            await AttachAnchorMarkerAsync(anchor, dayIndex, anchorIndex);
        }

        public async ValueTask<AnchorMarker> AttachAnchorMarkerAsync(IAnchor anchor,
            int dayIndex, int anchorIndex)
        {
            string title = ScheduleSummaryExtension.GetMarkerCode(dayIndex, anchorIndex);
            bool is_important = this.visualSchedule.IsAnchorImportant(dayIndex, anchorIndex);
            // https://dotscrapbook.wordpress.com/2014/11/28/simple-numbered-markers-with-leaflet-js/
            DivIcon icon;
            if (anchor.IsPinned)
                icon = new DivIcon()
                {
                    Size = new Size(36, 46),
                    Anchor = new Point(18, 43),
                    PopupAnchor = new Point(3, -40),
                };
            else
                /*  icon= new DivIcon()
                  {
                      Size = new Size(36, 46),
                      Anchor = new Point(18, 43),
                      PopupAnchor= new Point(3, -40),
                  };*/
                icon = new DivIcon()
                {
                    Size = new Size(48, 48),
                    Anchor = new Point(15, 38),
                    PopupAnchor = new Point(3, -40),
                };

            icon.ClassName = IconClassName(is_important, anchor.IsPinned);
            icon.Html = $"<div class='{(anchor.IsPinned ? "pinned" : "auto")}-anchor-icon-text'>{title}</div>";

            var marker = new AnchorMarker(anchor, GeoHelper.GeoPointToLatLng(anchor.UserPoint))
            {
                Draggable = true,//anchor.IsPinned,
                Title = title,
                //Popup = new Popup {Content = $"I am at {coords.Lat:0.00}° lat, {coords.Lng:0.00}° lng"},
                Tooltip = new Tooltip {Content = "tooltip"},
                Icon = icon,
            };

            // Console.WriteLine($"Attaching marker with title {marker.Title} and icon {icon.Html}");

            marker.OnClick += uiOnAnchorMarkerClickAsync;
            if (anchor.IsPinned)
            {
            }
            marker.OnMove += onMarkerDrag;
            marker.OnMoveEnd += onAnchorMarkerDragEndAsync;

            await this.Map.AddNewLayerAsync(marker);

            return marker;
        }


        public async ValueTask EditCheckpointDetailsAsync(int dayIndex, int checkpointIndex)
        {
            AnchorEdit? result = await this.Modal.ShowAnchorDialogAsync("Anchor", this.visualSchedule, dayIndex,
                checkpointIndex);
            await totalManager.EditCheckpointDetailsAsync(dayIndex, checkpointIndex, result);
        }

        public  ValueTask CheckpointFocusAsync(int dayIndex, int checkpointIndex)
        {
            var (day_anchor_idx, anchor_idx) = this.visualSchedule.CheckpointIndexToAnchor(dayIndex,
                checkpointIndex);
            var day = this.visualSchedule.Days[day_anchor_idx];
            var anchor = day.Anchors[anchor_idx];
            var lat_lng = GeoHelper.GeoPointToLatLng(anchor.UserPoint);
            return Map.PanToAsync(lat_lng.ToPointF());
        }

        private async void uiOnAnchorMarkerClickAsync(InteractiveLayer sender, MouseEvent e)
        {
            if (this.HACK_HandlingMouseClick)
                return;
            
            var marker = (sender as AnchorMarker)!;
            (int day_idx, int anchor_idx, var anchor) = this.totalManager.IndexOfAnchor(marker.Anchor);
            if (e.OriginalEvent.ShiftKey)
            {
                await this.dispatcher.ExecuteAsync(new PrepareAnchorAppend(day_idx, anchor_idx), CancellationToken.None);
            }
            else
            {
                await EditCheckpointDetailsAsync(day_idx,
                    this.visualSchedule.AnchorIndexToCheckpoint(day_idx, anchor_idx));
            }
        }

        private async void onAnchorMarkerDragEndAsync(Marker marker, Event e)
        {
            var anchor_marker = (AnchorMarker) marker;
            var position = GeoHelper.GeoPointFromLatLng(this.onDragMarkerLatLng);

            anchor_marker.Position = GeoHelper.GeoPointToLatLng(position);
            (int day_idx, int anchor_idx, var anchor) = this.totalManager.IndexOfAnchor(anchor_marker.Anchor);
            await this.dispatcher.ExecuteAsync(new ChangeAnchorPosition(position, day_idx, anchor_idx), CancellationToken.None);
        }

        private async ValueTask onChartMouseOutAsync()
        {
            if (this.traceMarker == null)
                return;

            await Map.RemoveNewLayerAsync(this.traceMarker);
            await this.traceMarker.DisposeAsync();
            this.traceMarker = null;
        }

        private void onMarkerDrag(Marker marker, DragEvent evt)
        {
            this.onDragMarkerLatLng = evt.LatLng;

            StateHasChanged();
        }

        private ValueTask onChartHoverAsync(GeoZPoint point)
        {
            if (this.traceMarker == null)
                return setTraceMarkerAsync(point);
            else
            {
                return this.traceMarker.SetLatLngAsync(JsRuntime, GeoHelper.GeoPointToLatLng(point.Convert2d()));
            }
        }

        public ValueTask setTraceMarkerAsync(GeoZPoint point)
        {
            // https://dotscrapbook.wordpress.com/2014/11/28/simple-numbered-markers-with-leaflet-js/
            DivIcon icon;

            icon = new DivIcon()
            {
                Size = new Size(48, 48),
                Anchor = new Point(24, 24),
            };

            icon.ClassName = "dot-icon";

            var marker = new Marker(GeoHelper.GeoPointToLatLng(point.Convert2d()))
            {
                Draggable = false,
                Icon = icon,
            };

            this.traceMarker = marker;
            return this.Map.AddNewLayerAsync(marker);
        }

        ValueTask IVisualReceiver.OnPlanChangedAsync()
        {
            return ValueTask.CompletedTask;
        }

        async ValueTask IVisualReceiver.OnAnchorMovedAsync(VisualAnchor anchor)
        {
            var marker = tryFindMarker(anchor);
            if (marker != null)
            {
                await marker.SetLatLngAsync(JsRuntime, GeoHelper.GeoPointToLatLng(anchor.UserPoint));
            }
        }

        ValueTask IVisualReceiver.OnLoopChangedAsync()
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IDataContext.NotifyOnChangedAsync(object sender, string propertyName)
        {
            StateHasChanged(); // for example flag "hasMap" -- it does not affect anything, but we need to show it
            return ValueTask.CompletedTask;
        }

        IDisposable IDataContext.NotifyOnChanging(object sender, string propertyName)
        {
            throw new NotImplementedException();
        }

        ValueTask IVisualActor.AlertAsync(string? message, MessageLevel severity)
        {
            this.uiLogger.Log(severity,message);
            // Console.WriteLine($"Alert {severity} {message}");
            if (message != null)
            {
                if (severity == MessageLevel.Error)
                    //   ToastService.ShowError(message, settings => settings.Timeout = Timeout.Infinite);
                    return this.commonDialog.AlertAsync(message);
                else if (severity == MessageLevel.Warning)
                    ToastService.ShowWarning(message);
                else if (severity == MessageLevel.Debug)
                {
                    ;
                }
                else
                    throw new NotSupportedException($"{severity}");
            }

            return ValueTask.CompletedTask;
        }

        IDisposable IVisualActor.GetConcurrentLock(string message)
        {
            return Modal.ShowGuardDialog(message);
        }

        private async Task uiSelectLayersAsync()
        {
            var layers_selection = (await this.RestPlannerClient.GetLayersDirectoryAsync(directory: null, CancellationToken.None))
                .Value.Files
                .ToDictionary(it => it, it => new UserLayer(it));

            for (int i = this.attractionLayers.Count - 1; i >= 0; --i)
            {
                var layer = this.attractionLayers[i];
                if (layers_selection.TryGetValue(layer.Name, out var selection))
                    selection.IsEnabled = true;
                else
                {
                    await Map.RemoveNewLayerAsync(layer.MarkerCluster);
                    await layer.DisposeAsync();
                    this.attractionLayers.RemoveAt(i);
                }
            }

            if (!await this.Modal.ShowLayersDialogAsync(layers_selection.Values.OrderBy(it => it.Name).ToList()))
                return;

            for (int i = this.attractionLayers.Count - 1; i >= 0; --i)
            {
                var layer = this.attractionLayers[i];
                var selection = layers_selection[layer.Name];
                // we already have this layer so disable it to avoid double loading
                if (selection.IsEnabled)
                    selection.IsEnabled = false;
                else
                {
                    await Map.RemoveNewLayerAsync(layer.MarkerCluster);
                    await layer.DisposeAsync();
                    this.attractionLayers.RemoveAt(i);
                }
            }

            foreach (var name in layers_selection.Where(it => it.Value.IsEnabled).Select(it => it.Value.Name))
            {
                var data = await this.RestPlannerClient.GetLayerAsync(name, CancellationToken.None);
                await this.asActor.AlertAsync(data.ProblemMessage);

                if (!data.HasValue)
                    continue;

                var layer = await AttractionsLayer.CreateAsync(JsRuntime, name);
                await Map.AddNewLayerAsync(layer.MarkerCluster);
                this.attractionLayers.Add(layer);
                await this.addAttractionMarkersAsync(layer.MarkerCluster,
                    data.Value.Attractions.SelectMany(x => x), isExternal: true);
            }
        }

        private async void routeAttractionMarkerOnDblClick(InteractiveLayer sender, MouseEvent e)
        {
            var attr_marker = (AttrMarker) sender;
            if (this.visualSchedule.FindAttractionIndex(attr_marker.Attraction!, out var leg_idx, out var attr_idx))
            {
                this.HACK_HandlingMouseClick = true;
                await this.dispatcher.ExecuteAsync(new AddRouteAttraction(leg_idx, attr_idx,attr_marker.Attraction!), CancellationToken.None);
                this.HACK_HandlingMouseClick = false;
            }
        }

        private async void externalAttractionMarkerOnDblClick(InteractiveLayer sender, MouseEvent e)
        {
            var attr_marker = (AttrMarker) sender;
            this.HACK_HandlingMouseClick = true;
            await dispatcher.ExecuteAsync(new AddExternalAttraction(attr_marker.Attraction!),
                CancellationToken.None);
            this.HACK_HandlingMouseClick = false;
            var layer = this.attractionLayers.First(it => it.MarkerCluster.Contains(attr_marker));
            if (!await layer.MarkerCluster.RemoveLayerAsync(attr_marker))
                await this.asActor.AlertAsync($"Couldn't find external attraction marker.", MessageLevel.Error);
        }

        private void uiRefresh()
        {
            StateHasChanged();
        }

        ValueTask IVisualReceiver.OnScheduleSetAsync()
        {
            return ValueTask.CompletedTask;
        }

        private async Task onBeforeInternalNavigation(LocationChangingContext context)
        {
            if (visualSchedule.IsModified)
            {
                var isConfirmed = await JsRuntime.InvokeAsync<bool>("confirm",
                    "Are you sure you want to navigate away?");

                if (!isConfirmed)
                {
                    context.PreventNavigation();
                }
            }
        }

        private async ValueTask<Polyline> addLineAsync(Polyline line, LineDecoration deco, IReadOnlyList<GeoPoint> points)
        {
            line.StrokeColor = getColor(deco);
            line.StrokeWidth = deco.Width;
            line.Fill = false;

            var shape = new PointF[1][];
            int size = points.Count;
            shape[0] = new PointF[size];
            for (int i = 0; i < size; ++i)
            {
                double lat = points[i].Latitude.Degrees;
                double lon = points[i].Longitude.Degrees;
                shape[0][i] = new PointF((float) lat, (float) lon);
            }

            line.Shape = shape;

            await this.Map.AddNewLayerAsync(line);

            return line;
        }

        private async void onMapClickAsync(object sender, MouseEvent e)
        {
            var point = GeoHelper.GeoPointFromLatLng(e.LatLng);
            if (e.OriginalEvent.ShiftKey)
            {
                if (this.visualSchedule.AnchorPlaceholder.HasValue)
                {
                    this.HACK_HandlingMouseClick = true;
                    await this.dispatcher.ExecuteAsync(new AddAnchorAtPlaceholder(point),
                        CancellationToken.None);
                    await this.dispatcher.ExecuteAsync(new DeletePlaceholder(), CancellationToken.None);
                    this.HACK_HandlingMouseClick = false;
                }
                else
                    await this.dispatcher.ExecuteAsync(new SnapToLineAt(point), CancellationToken.None).ConfigureAwait(false);
            }
            else if (this.visualSchedule.AnchorPlaceholder.HasValue)
                await this.dispatcher.ExecuteAsync(new AddAnchorAtPlaceholder(point),
                    CancellationToken.None);
        }

        async  ValueTask IVisualReceiver.OnPlaceholderChangedAsync()
        {
            this.markers.RefreshState();
        
            this.Map.OnMouseMove -= onMapMouseMoveAsync ;

            if (this.visualSchedule.AnchorPlaceholder == null)
            {
                await clearPlaceholderLineAsync();
                foreach (var layer in this.legLiveReplacement)
                    await this.Map.AddNewLayerAsync(layer);
                this.legLiveReplacement = Array.Empty<FragmentLine<WorldIdentifier, WorldIdentifier>>();
            }
            else
            {
                this.liveLineCoords = this.visualSchedule.GetLivePlaceholderLine();
             
                foreach (var layer in this.legLiveReplacement)
                    await layer.DisposeAsync();
                if (liveLineCoords?.LegIndex is { } leg_idx)
                {
                    this.legLiveReplacement = this.fragmentLines
                        .Where(it => it.LegRef == this.visualSchedule.Route.Legs[leg_idx]).ToArray();
                    //this.uiLogger.Info($"Hiding {this.legLiveReplacement.Length} legs");
                    foreach (var layer in this.legLiveReplacement)
                        await this.Map.RemoveNewLayerAsync(layer);
                }
                else
                    this.legLiveReplacement = Array.Empty<FragmentLine<WorldIdentifier, WorldIdentifier>>();

                //this.uiLogger.Info($"Placeholder {this.visualSchedule.AnchorPlaceholder}, live coords {this.liveLineCoords}");
                
                this.Map.OnMouseMove += onMapMouseMoveAsync;
            }
        }

       private async void onMapMouseMoveAsync(Map sender, MouseEvent e)
       {
           await clearPlaceholderLineAsync();

           if (this.liveLineCoords is { } line)
           {
               var points = new List<GeoPoint>();
               if (line.Start is { } start)
                   points.Add(this.visualSchedule.Days[ start.dayIndex].Anchors[start.anchorIndex].UserPoint);
               points.Add(GeoHelper.GeoPointFromLatLng(e.LatLng));
               if (line.End is { } end)
                   points.Add(this.visualSchedule.Days[ end.dayIndex].Anchors[end.anchorIndex].UserPoint);

               this.placeholderLine = await addLineAsync(new Polyline(),
                   Program.GlobalSettings.VisualPreferences.SpeedStyles[RoadStyling.Unknown],
                   points);
           }
       }

       private async ValueTask clearPlaceholderLineAsync()
        {
            if (this.placeholderLine == null)
                return;

            await this.Map.RemoveNewLayerAsync(this.placeholderLine);
            await this.placeholderLine.DisposeAsync();
            this.placeholderLine = null;
        }

        async ValueTask IVisualReceiver.OnLegRemovingAsync(int legIndex)
        {
            int count = 0;
            var leg = this.visualSchedule.Route.Legs[legIndex];
            foreach (var layer in this.fragmentLines.Where(it => it.LegRef == leg).ToArray())
            {
                await removeLegLayerAsync(layer);

                ++count;
            }

            //    this.uiLogger.Info($"{this.FuncName()}: for {legIndex} leg removed {count} fragments, remains {fragmentLines.Count()}");
        }

        
        async ValueTask IVisualReceiver.OnSummarySetAsync()
        {
            foreach (var (anchor, day_idx, anchor_idx) in this.visualSchedule.IndexedReadOnlyAnchors())
            {
                var marker = tryFindMarker(anchor);
                if (marker?.Tooltip == null)
                {
                    this.uiLogger.Warning($"{day_idx}:{anchor_idx} is null");
                }
                else
                {
                    var content = visualSchedule.GetAnchorBriefInfo(day_idx, anchor_idx);
                    await marker.SetTooltipContentAsync(JsRuntime, content);
                }
            }

            StateHasChanged();
        }


        private async Task uiTestAsync()
        {
            //   ToastService.ShowInfo("hello there");
            //using (Modal.ShowGuardDialog("Testing..."))
            {
                //await setTraceMarkerAsync(Program.ScheduleSettings.PlannerPreferences.Home!.Value.Convert3d());
                //await Task.Delay(TimeSpan.FromSeconds(30));
                //await this.dispatcher.ExecuteAsync(new Save("xx-test.trproj"), CancellationToken.None);

            }
            //return;
            
            
            using (Modal.ShowGuardDialog("Testing..."))
            {
                // if (false)
                {
                    this.visualSchedule.Settings.LoopRoute = false;
                    //await this.dispatcher.ExecuteAsync(new DeleteAnchor(0, 0), CancellationToken.None);
                    await this.dispatcher.ExecuteAsync(new SetPlaceholder(0, 0),CancellationToken.None).ConfigureAwait(false);
//                    foreach (var pt in RequestPoint<TNodeId>.ParsePoints("S 52.9489612°,18.0301981°; SE 53.2595618°,19.0909864°; S 53.10906039999999°,18.1195797°"))
                    foreach (var pt in RequestPoint<TNodeId>.ParsePoints("S 53.1974891°,18.6125959°;  53.1984913°,18.6128783°; S 53.1991364°,18.613114799999998°°"))
                        
                        
                        
                        //foreach (var pt in RequestPoint<TNodeId>.ParsePoints( " S 53.0118078°,18.5645971°; S 53.0110787°,18.5660309°; S 53.0098648, 18.5794468"))
                        //foreach (var pt in RequestPoint<TNodeId>.ParsePoints( " S 53.0118078°,18.5645971°; S 53.0110787°,18.5660309°; S 53.0107574, 18.5694485"))
                        //foreach (var pt in RequestPoint<TNodeId>.ParsePoints( " S 53.0118078°,18.5645971°; S 53.0110787°,18.5660309°; S 53.011219, 18.5671463"))
                        //foreach (var pt in RequestPoint<TNodeId>.ParsePoints( " S 53.011528, 18.5656605°; S 53.0110787°,18.5660309°; S 53.011219, 18.5671463"))
                    {
                        Console.WriteLine(pt.ToString());
                        var anchor_res = await dispatcher.ExecuteAsync<VisualAnchor>(new AddAnchorAtPlaceholder(pt.UserPointFlat), CancellationToken.None).ConfigureAwait(false);
#if DEBUG
                        if (anchor_res.HasValue)
                        {
                            anchor_res.Value.DEBUG_Smoothing = pt.AllowSmoothing;
                            anchor_res.Value.DEBUG_Enforce = pt.EnforcePoint;
                        }
#endif
                    }

                    //await uiPartialBuildPlanAsync();
                    //await uiCompleteRebuildPlanAsync();

                    //await addMarkerAsync(GeoPoint.FromDegrees(53.01777, 18.59697)); // Torun
                    //await addMarkerAsync(GeoPoint.FromDegrees(53.1376, 18.19631)); // Ostromecko
                    //await addMarkerAsync(GeoPoint.FromDegrees(53.34573, 18.42256)); // Chelmno
                    // await addMarkerAsync(GeoPoint.FromDegrees(53.47588, 18.75662)); // Grudziadz
                    // await addMarkerAsync(GeoPoint.FromDegrees(53.27762, 18.94476)); // Wabrzezno

                    //    await newProjectAsync();

                    //  await completeRebuildPlanAsync(this.TrueCalculations);

                    //await this.markers.GetAttractionsAsync(dayIndex:0);
                }

                //     await loadScheduleAsync("jesien.trproj");
                //   this.markers.InsertMarkerPlaceholder(0, 6);

                await uiDeletePlaceholderAsync();
            }
        }
    }
}

// https://stackoverflow.com/questions/7060009/css-max-height-remaining-space
// https://stackoverflow.com/questions/90178/make-a-div-fill-the-height-of-the-remaining-screen-space
// https://stackoverflow.com/questions/68516283/flexbox-layout-take-remaining-space-but-do-not-expand-more-than-100-of-remaini

// https://css-tricks.com/snippets/css/complete-guide-grid/
// https://cssgrid-generator.netlify.app/