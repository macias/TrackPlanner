using System;
using TrackPlanner.Shared;
using Microsoft.Extensions.Logging;

namespace TrackPlanner.WebUI.Client
{
    public sealed class UiLogger : TrackPlanner.Shared.ILogger
    {
        private readonly Microsoft.Extensions.Logging.ILogger  msLogger;

        public UiLogger(Microsoft.Extensions.Logging.ILogger msLogger)
        {
            this.msLogger = msLogger;
        }

        public void Log(MessageLevel level, string? message)
        {
            if (message == null)
                return;
            
            var ms_level = level switch
            {
                TrackPlanner.Shared.  MessageLevel.Error => Microsoft.Extensions.Logging.LogLevel.Error,
                TrackPlanner.Shared.   MessageLevel.Info => Microsoft.Extensions.Logging.LogLevel.Information,
                TrackPlanner.Shared.   MessageLevel.Debug => Microsoft.Extensions.Logging.LogLevel.Debug,
                TrackPlanner.Shared.   MessageLevel.Warning => Microsoft.Extensions.Logging.LogLevel.Warning,
                _ => throw new NotImplementedException(),
            };
            this.msLogger.Log(ms_level,message);
        }

        public TrackPlanner.Shared.ILogger With(SinkSettings settings)
        {
            return this;
        }
    }
}