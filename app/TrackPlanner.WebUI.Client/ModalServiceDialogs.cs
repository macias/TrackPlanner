using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Force.DeepCloner;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.Structures;
using TrackPlanner.WebUI.Client.Data;
using TrackPlanner.WebUI.Client.Dialogs;

namespace TrackPlanner.WebUI.Client
{
    public static class ModalServiceDialogs
    {
        public static IDisposable ShowGuardDialog(this IModalService modalService, string message)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
                HideHeader = true,
                HideCloseButton = true,

            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(ProgressDialog.Message), message);
            var dialog = modalService.Show<ProgressDialog>(title: null, parameters, options);
            return new CompositeDisposable(() => dialog.Close());
        }

        public static async ValueTask<string?> ShowFileDialogAsync(this IModalService modalService,IPlanClient client,
            string title, FileDialog.DialogKind kind)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(FileDialog.Kind), kind);
            parameters.Add(nameof(FileDialog.PlanClient), client);
            var dialog = modalService.Show<FileDialog>(title, parameters, options);
            var modal_result = await dialog.Result;

            if (modal_result.Cancelled)
                return null;
            else
                return $"{modal_result.Data}";
        }

        public static async ValueTask<bool> ShowLayersDialogAsync(this IModalService modalService,
          List<UserLayer> layers)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
            };
            var parameters = new ModalParameters();
            List<UserLayer> dialog_input = layers.DeepClone();
            parameters.Add(nameof(LayerDialog.Entries), dialog_input);
            var dialog = modalService.Show<LayerDialog>("Layers", parameters, options);
            var modal_result = await dialog.Result;

            if (modal_result.Cancelled)
                return false;

            for (int i = 0; i < dialog_input.Count; ++i)
                    layers[i].IsEnabled = dialog_input[i].IsEnabled;
            return true;
        }



        public static async ValueTask<ScheduleSettings?> ShowPreferencesDialogAsync(this IModalService modalService, string title, ScheduleSettings prefs)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(PreferencesDialog.Settings), prefs.DeepClone());
            var dialog = modalService.Show<PreferencesDialog>(title, parameters, options);
            var modal_result = await dialog.Result;
            return (ScheduleSettings?) modal_result.Data;
        }

        public static async ValueTask<AnchorEdit?> ShowAnchorDialogAsync(this IModalService modalService, string title,
            IReadOnlySchedule schedule, int dayIndex, int checkpointIndex)
        {
            var options = new ModalOptions()
            {
                DisableBackgroundCancel = true,
            };
            var parameters = new ModalParameters();
            parameters.Add(nameof(AnchorDialog.Schedule), schedule);
            parameters.Add(nameof(AnchorDialog.CheckpointDayIndex), dayIndex);
            parameters.Add(nameof(AnchorDialog.CheckpointIndex), checkpointIndex);
            var dialog = modalService.Show<AnchorDialog>(title, parameters, options);
            var modal_result = await dialog.Result;
            return (AnchorEdit?) modal_result.Data;
        }
    }
}