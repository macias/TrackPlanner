﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace TrackPlanner.WebUI.Client
{
    public sealed class CommonDialog 
    {
        private readonly IJSRuntime jsRuntime;

        public CommonDialog(IJSRuntime jsRuntime)
        {
            this.jsRuntime = jsRuntime;
        }
       
        public async ValueTask AlertAsync(string? message)
        {
            message ??= "Missing error message";
            await jsRuntime.InvokeAsync<object?>("alert", message );
        }

        public async ValueTask<bool> ConfirmAsync(string message)
        {
            bool confirmed = await jsRuntime.InvokeAsync<bool>("confirm", message); 
            return confirmed;
        }

        public async ValueTask<string?> PromptAsync(string message,string? initialValue = null)
        {
            var parameters = new object?[] {(object?)message, (object?)initialValue}.Where(it => it != null).ToArray();
            var input = await jsRuntime.InvokeAsync<string?>("prompt", args: parameters); 
            return input;
        }
        
    }
}
