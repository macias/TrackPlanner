check why in bi-directional Dijkstra it is possible to get full route from start-end or end-start
such far reaching suggest one side does too much work, we should join halves somewhere and
then polish just boundaries, not go deep inside other side backtrack

Fixing it would be beneficial to CH because in case of many-many routing we would not have
to worry about crosspoints and userpoints from opposite buckets
------------------------
finding path should be context-free, currently it is not, this means we cannot compute A-B-C
cost as A-B plus B-C because there could be extra cost at the joint (road switching)

This might be not that useful with weight limits, because of numeric rounding, so we would have
to pass exact weight limit as the main algorithm works, not just ad-hoc sum
----------------------

independent, parallel leg computation
2-3 second PER leg timeout and replacement with draft one
favourite routes as shortcuts (entire replacement or nothing) (switchable)
resume working on shortcuts algorithm

interactive notification as sink for logger, logger with message AND description
all communication over net should wrap returned data in Result (or use IResult interface; 
  remove Problem from Route) 







--------------------------------------


https://adadevelopment.github.io/gdal/gdal-read-write.html
https://stackoverflow.com/questions/9188572/how-to-read-geotiff-file-from-c-sharp
https://www.nuget.org/packages/GDAL/
http://build-failed.blogspot.com/2014/12/processing-geotiff-files-in-net-without.html
https://gis.stackexchange.com/questions/37413/how-to-read-geotiff-tags-and-pixel-per-pixel-values-using-gdal-c
https://github.com/BitMiracle/libtiff.net

https://portal.opentopography.org/raster?opentopoID=OTSRTM.082015.4326.1
https://dwtkns.com/srtm30m/

https://www2.jpl.nasa.gov/srtm/faq.html
https://lpdaac.usgs.gov/documents/179/SRTM_User_Guide_V3.pdf


https://github.com/erossini/BlazorChartjs

https://github.com/mariusmuntean/ChartJs.Blazor

https://theclimbingcyclist.com/gradients-and-cycling-how-much-harder-are-steeper-climbs/
https://www.gribble.org/cycling/power_v_speed.html
https://betterbicycles.org/bicycle-power-calculations/
http://bikecalculator.com/


// https://ai.googleblog.com/2021/09/efficient-partitioning-of-road-networks.html