﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geo;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO.Esri;
using NetTopologySuite.IO.Esri.Shapefiles.Readers;
using TrackPlanner.Backend;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Structures;

namespace BdlReader;

public sealed class FooPolygon
{
    public IReadOnlyList<GeoPoint> Points { get; }

    public FooPolygon(GeoPoint[] points)
    {
        if (points[0] != points[^1])
            throw new ArgumentException("Not closed");
        
        Points = points;
    }
}

public sealed class FooRegion
{
    public List<FooPolygon> Polygons { get; }
    public Dictionary<Segment,FooPolygon> SegmentReferences { get;  }

    public FooRegion()
    {
        this.Polygons = new List<FooPolygon>();
        this.SegmentReferences = new Dictionary<Segment, FooPolygon>();
    }

}
    
public readonly record struct Segment
{
    private readonly GeoPoint a;
    private readonly GeoPoint b;

    public static Segment Create(GeoPoint a, GeoPoint b)
    {
        if (a.Latitude < b.Latitude || (a.Latitude == b.Latitude && a.Longitude <= b.Longitude))
            return new Segment(a, b);
        else
            return new Segment(b, a);
    }
    private Segment(GeoPoint a, GeoPoint b)
    {
        this.a = a;
        this.b = b;
    }

}
public sealed class Foobar
{
    private readonly Navigator navigator;

    public Foobar(Navigator navigator)
    {
        this.navigator = navigator;
    }
    private void addToInputGeometry(FooRegion region, string id, Geometry geometry)
    {
        if (geometry is MultiPolygon multi_poly)
        {
            foreach (var geom in multi_poly.Geometries)
            {
                addToInputGeometry(region, id, geom);
            }
        }
        else if (geometry is Polygon poly)
        {
            addToInputPolygon(region, id, poly);
        }
        else
            throw new NotImplementedException(geometry.GetType().FullName);
    }
    
    private void addToInputPolygon(FooRegion region, string id, Polygon polygon)
    {
        region.Polygons.Add(new FooPolygon( polygon.Coordinates.Select(it => GeoPoint.FromDegrees(it.Y, it.X)).ToArray()));
    }
    
    public void Extract(string path)
    {
        var site_types = new HashSet<string>();

        var geom_dict = new Dictionary<string, FooRegion>();
        
        foreach (var feature in Shapefile.ReadAllFeatures(path,
                     new ShapefileReaderOptions()
                     {
                         GeometryBuilderMode = GeometryBuilderMode.IgnoreInvalidShapes
                     }))
        {
            var site_type = $"{feature.Attributes["site_type"]}".ToLowerInvariant();
            site_types.Add(site_type);
            if (site_type == "bs" || site_type == "bśw")
            {
                if (!geom_dict.TryGetValue(site_type, out var input))
                {
                    input = new FooRegion();
                    geom_dict.Add(site_type, input);
                }

                var geom_id = $"{feature.Attributes["a_i_num"]}";

                addToInputGeometry(input, geom_id, feature.Geometry);
            }
        }

        foreach (var (type, input) in geom_dict)
        {
            var writer_input = new TrackWriterInput() {Title = type};
            foreach (var (poly,idx) in input.Polygons.ZipIndex())
            {
                var line = new LineDefinition(poly.Points.Select(it => it.Convert3d()).ToArray(),
                    $"{idx}", description: null, KmlLineDecoration.ThinBlack);
                writer_input.Lines.Add(line);
            }

            using (FileStream stream = new FileStream(Navigator.GetUniquePath(navigator.GetOutputDirectory(),
                       $"bdl-{type}.kml"), FileMode.CreateNew))
            {
                writer_input.SaveDecorated(stream);
            }
        }
        
        Console.WriteLine(String.Join(", ",site_types));

    }
}
