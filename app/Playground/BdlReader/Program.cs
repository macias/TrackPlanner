﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BdlReader;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO.Esri;
using NetTopologySuite.IO.Esri.Shapefiles.Readers;
using SharpKml.Base;
using TrackPlanner.Backend;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;

// https://ithoughthecamewithyou.com/post/esri-shapefile-reader-in-net
// http://www.independent-software.com/dbase-dbf-dbt-file-format.html

// https://github.com/NetTopologySuite/NetTopologySuite.IO.Esri

// https://github.com/Tronald/CoordinateSharp

/*void NewFunction(string path)
{
    using (Shapefile shapefile = new Shapefile(path))
    {
        Console.WriteLine($"Shapefile type {shapefile.Type}, contains {shapefile.Count:n0} shapes.");
        Console.WriteLine($"Bounding box is {shapefile.BoundingBox.Top:n2}, {shapefile.BoundingBox.Left:n2} - {shapefile.BoundingBox.Bottom:n2}, {shapefile.BoundingBox.Right:n2}");

        foreach (Shape shape in shapefile)
        {
            string[] metadataNames = shape.GetMetadataNames();
            foreach (string metadataName in metadataNames)
            {
                Console.WriteLine($"Shape {shape.RecordNumber} ({shape.Type})");
                Console.WriteLine("Metadata:");
                Console.WriteLine($"  {metadataName}: {shape.GetMetadata(metadataName)}");

                ShapePolygon? polygon = shape as ShapePolygon;
                if (polygon != null)
                {
                    foreach (PointD[] part in polygon.Parts)
                    {
                        foreach (PointD point in part)
                        {
                            Console.WriteLine($"First point: {point.X:n2}, {point.Y:n2}");
                            break;
                        }

                        break;
                    }
                }
            }

            // only print the first shape
            break;
        }
    }
}*/

var navigator = new Navigator();
// EPSG:2180 - ETRS89 / Poland CS92 - Projected
var shp_path = System.IO.Path.Combine(navigator.GetBdl(),
    "BDL_01_01_AUGUSTOW_2022/X_SUBAREA.shp");

if (false)
using (var dbf = new NetTopologySuite.IO.Esri.Dbf.DbfReader(shp_path))
{
    foreach (var record in dbf)
    {
        foreach (var fieldName in record.GetNames())
        {
            Console.WriteLine($"{fieldName} {record[fieldName]}");
        }

        Console.WriteLine();
        break;
    }
}

if (false)
foreach (Geometry geometry in NetTopologySuite.IO.Esri.Shapefile.ReadAllGeometries(shp_path,
             new ShapefileReaderOptions()
         {
             GeometryBuilderMode = GeometryBuilderMode.IgnoreInvalidShapes
         }))
{
    Console.WriteLine(geometry.SRID);
    Console.WriteLine(geometry);
    break;
}


foreach (var feature in Shapefile.ReadAllFeatures(shp_path,
             new ShapefileReaderOptions()
             {
                 GeometryBuilderMode = GeometryBuilderMode.IgnoreInvalidShapes
             }))
{
    foreach (var attrName in feature.Attributes.GetNames())
    {
        Console.WriteLine($"{attrName}: {feature.Attributes[attrName]}");
    }
    Console.WriteLine($"     SHAPE: {feature.Geometry}");
    Console.WriteLine();
    break;
}

var foobar = new Foobar(navigator);
foobar.Extract(shp_path);



// =======================================================

//var  c= CoordinateSharp.Coordinate.Parse("771214.98 665105.63");
//Console.WriteLine($"{c.Latitude}, {c.Longitude}");

// =======================================================
/*using var proj_from = CoordinateReferenceSystem.CreateFromEpsg(2180);
using var proj_to = CoordinateReferenceSystem.CreateFromEpsg(4326);

using (var t = CoordinateTransform.Create(proj_from, proj_to))
{
    var r = t.Apply(new PPoint(771214.98, 665105.63));
    Console.WriteLine(r);
}
*/
// ============================================

/*
var epsg25832 = new DotSpatial.Projections.ProjectionInfo();
var epsg3857 = new DotSpatial.Projections.ProjectionInfo();
epsg25832.ParseEsriString(DotSpatial.Projections. ESRI_EPSG_25832);
epsg3857.ParseEsriString(ESRI_EPSG_3857);

DotSpatial.Projections.Reproject.ReprojectPoints(pointArray, zArray, epsg25832, epsg3857, 0, (pointArray.Length / 2));
*/
// =======================================================
