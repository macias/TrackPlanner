#!/bin/sh

CONFIG=$(realpath  ../config/)
OSM=$(realpath  ../osm/)
SRTM=$(realpath  ../srtm/)
MAPS=$(realpath  ../maps/)
SCHEDULES=$(realpath  ../schedules/)

docker run \
  -p 8700:8700 \
  -p 5200:5200 \
  -v $CONFIG:/config/ \
  -v $OSM:/osm/ \
  -v $SRTM:/srtm/ \
  -v $MAPS:/maps/ \
  -v $SCHEDULES:/schedules/ \
  --name trackplanner xmacias/trackplanner