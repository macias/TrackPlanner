﻿using System;
using System.Collections.Generic;
using System.IO;
using TrackPlanner.Structures;

namespace TrackPlanner.Storage
{
    public static class ReaderOffsets
    {
        public static ReaderOffsets<TKey> Read<TKey>(BinaryReader reader, Func<BinaryReader,TKey> readKey)
            where TKey : notnull
        {
            return new ReaderOffsets<TKey>(reader, readKey);
        }
    }
    // this is basically a dictionary with disk offsets for a reader with reader as well 
    public sealed class ReaderOffsets<TKey> 
    where TKey:notnull
    {
        private readonly BinaryReader reader;
        private readonly CompactDictionaryFill<TKey, long> offsets;

        public long this[TKey key] => this.offsets[key];

        public IEnumerable<KeyValuePair<TKey, long>> Offsets => this.offsets;

        public int Count => this.offsets.Count;
            
        public ReaderOffsets(BinaryReader reader,Func<BinaryReader,TKey> readKey)
        {
            this.reader = reader;

            int count = reader.ReadInt32();
            this.offsets = new CompactDictionaryFill<TKey, long>(capacity: count);
            
            for (int i = 0; i < count; ++i)
            {
                var key = readKey(reader);
                var offset = this.reader.ReadInt64();
               
                this.offsets.Add(key, offset);
            }

        }

        public void Deconstruct(out BinaryReader reader, out IReadOnlyDictionary<TKey, long> offsets)
        {
            reader = this.reader;
            offsets = this.offsets;
        }

     

      
    }
}