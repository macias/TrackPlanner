﻿using Geo;
using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TrackPlanner.Backend;
using TrackPlanner.Elevation;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder;
using TrackPlanner.PathFinder.ContractionHierarchies;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using SystemConfiguration = TrackPlanner.Shared.Stored.SystemConfiguration;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Runner
{
    /*
    https://scihub.copernicus.eu/dhus/#/home



    super sprawa, po zarejestrowaniu się można przeglądać dane z sentineli dla wybranych obszarów

    Request Done: ( footprint:"Intersects(POLYGON((21.906846278110624 49.97692768443906,21.92511191789837 49.97692768443906,21.92511191789837 49.9830247493573,21.906846278110624 49.9830247493573,21.906846278110624 49.97692768443906)))" ) AND ( beginPosition:[2021-01-01T00:00:00.000Z TO 2021-02-15T23:59:59.999Z] AND endPosition:[2021-01-01T00:00:00.000Z TO 2021-02-15T23:59:59.999Z] ) 

    */
    // todo: byc moze uzupelniac trakc o node'y z zadanej drogi
    // todo: wczytywac mapy, i tworzyc (JSON) slownik ich parametrow, tak aby pozniej doczytywac mape on-demand w zaleznosci od trasy


    // testowac pigze, dlaczego na zakrecie nie mamy takze mapowania na crossing
    // doprowadzic do tego, zeby punkty ktore sie nakladaly z crossing, zeby je usuwac

    class Program
    {
        private static readonly Navigator navigator = new Navigator();

        static void Main(string[] args)
        {
          
            {
                //RunTurner(logger);
                runRouter();
                //runContractionHierarchies();
                //runLogger();
                //RunRawElevations(logger);
                //CompareRouting(logger);
                //RunRestRoutingAsync(logger).GetAwaiter().GetResult();
                //RunRestAttractionsAsync(logger).GetAwaiter().GetResult();
                //RunCity<WorldIdentifier,WorldIdentifier>(logger,GeoPoint.FromDegrees(    53.28021, 18.94468));
                //RunCalc();
                //DictionaryEval();
                //ExtractMapData(logger);
                //readElevation(logger);
            }

            Console.WriteLine("Done!");
            //Console.ReadLine();
        }

        private static void RunRawElevations(ILogger logger)
        {
            foreach (var fn in System.IO.Directory.GetFiles(navigator.GetSrtmMaps(), "N53E018.hgt"))
            {
                using (HgtReader.Create(fn, out _, out var min, out var max))
                {
                    logger.Info($"Min {min}, max {max} for {System.IO.Path.GetFileNameWithoutExtension(fn)}");
                }
            }
        }

        private static void readElevation(ILogger logger)
        {
            // var reader = new ElevationReader(logger);
            //   reader.ReadMe("/bigdata/srtm/SRTM_NE_250m.tif");
        }

        private static void ExtractMapData(ILogger logger)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("runner.json", optional: true);

            IConfiguration config = builder.Build();

            var schedule_settings = new ScheduleSettings();
            config.GetSection(ScheduleSettings.SectionName).Bind(schedule_settings);
            schedule_settings.Check();

            var sysConfig = FinderConfiguration.Defaults() with {EnableDebugDumping = true};

            var collector = new OsmCollector(logger);
            var osm_files = System.IO.Directory.GetFiles(navigator.GetOsmMapsDirectory("kujawsko-pomorskie"), "*.osm.pbf");
            foreach (var file in osm_files)
            {
                logger.Info($"Reading {file}");
                var hist_objects = collector.ReadOsm(file);
                // 1.0 -- castles only
                // 2.0 -- added historic objects
                // 2.1 -- added new historic objects
                // 2.2 -- switch to tourist attraction
                var extract_fileName = Navigator.GetUniquePath(navigator.GetOutputDirectory(), "2.2-" + System.IO.Path.GetFileName((file).Replace(".osm.pbf", "") + "-historic.kml"));
                using (FileStream stream = new FileStream(extract_fileName, FileMode.CreateNew))
                {
                    var input = new TrackWriterInput();
                    foreach (var hist in hist_objects)
                    {
                        var description = new List<string>();
                        description.Add(hist.attraction.GetCompactLabel());
                        if (hist.attraction.Url != null)
                            description.Add(hist.attraction.Url);
                        input.AddPoint(hist.location.Point, $"{hist.attraction.Name} {hist.location.NodeId}", String.Join(Environment.NewLine, description),
                            hist.attraction.Features.HasFlag(PointOfInterest.Feature.Ruins) ? PointIcon.CircleIcon : PointIcon.StarIcon);
                    }

                    var kml = input.BuildDecoratedKml();
                    kml.Save(stream);
                }

                logger.Info($"Data saved to {extract_fileName}");
            }

            logger.Info($"Unused values: {(String.Join(Environment.NewLine, collector.Unused.OrderBy(x => x)))}");
        }

        private static void DictionaryEval()
        {
            //Console.WriteLine(new Dictionary<int,int>(capacity:2_014_641).GetCapacity()); // 2_411_033

            string path = System.IO.Path.Combine(navigator.GetCustomMapsDirectory("dolnoslaskie"), "dolnoslaskie-2022-01-10.xtr");
            var res_dict = new Dictionary<long, long>();
            Dictionary<long, long> dict;
            SeededDictionary<long, long> seed_dict;
            CompactDictionaryFill<long, long> compact;
            CompactDictionaryShift<long, long> compact0;
            var res_compact = new CompactDictionaryFill<long, long>();
            var res_compact0 = new CompactDictionaryShift<long, long>();

            using (var mem = new MemoryStream(System.IO.File.ReadAllBytes(path)))
            {
                using (var reader = new BinaryReader(mem))
                {
                    reader.ReadInt64(); // timestamp
                    reader.ReadInt32(); // cell size

                    var north_most = GeoZPoint.ReadRawAngle(reader);
                    var east_most = GeoZPoint.ReadRawAngle(reader);
                    var south_most = GeoZPoint.ReadRawAngle(reader);
                    var west_most = GeoZPoint.ReadRawAngle(reader);

                    var nodes_count = reader.ReadInt32();
                    var roads_count = reader.ReadInt32();
                    var cells_count = reader.ReadInt32();

                    seed_dict = new SeededDictionary<long, long>(EqualityComparer<long>.Default, nodes_count);
                    dict = new Dictionary<long, long>(capacity: nodes_count);
                    compact = new CompactDictionaryFill<long, long>(nodes_count);
                    compact0 = new CompactDictionaryShift<long, long>(nodes_count);
                    var roads_offset = reader.ReadInt64();
                    var grid_offset = reader.ReadInt64();

                    var pos = reader.BaseStream.Position;

                    var start = Stopwatch.GetTimestamp();
                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        res_dict.Add(node_id, off);
                    }

                    Console.WriteLine($"RES Dict filled in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");

                    reader.BaseStream.Seek(pos, SeekOrigin.Begin);
                    start = Stopwatch.GetTimestamp();
                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        dict.Add(node_id, off);
                    }

                    Console.WriteLine($"Dict filled in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");

                    reader.BaseStream.Seek(pos, SeekOrigin.Begin);
                    start = Stopwatch.GetTimestamp();
                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        compact.Add(node_id, off);
                    }

                    Console.WriteLine($"Compact filled in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");

                    reader.BaseStream.Seek(pos, SeekOrigin.Begin);
                    start = Stopwatch.GetTimestamp();
                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        compact0.Add(node_id, off);
                    }

                    Console.WriteLine($"Compact0 filled in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");


                    reader.BaseStream.Seek(pos, SeekOrigin.Begin);
                    start = Stopwatch.GetTimestamp();
                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        res_compact.Add(node_id, off);
                    }

                    Console.WriteLine($"RES Compact filled in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");

                    reader.BaseStream.Seek(pos, SeekOrigin.Begin);
                    start = Stopwatch.GetTimestamp();
                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        res_compact0.Add(node_id, off);
                    }

                    Console.WriteLine($"RES Compact0 filled in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");


                    reader.BaseStream.Seek(pos, SeekOrigin.Begin);
                    start = Stopwatch.GetTimestamp();

                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        seed_dict.AddSeed(node_id, off);
                    }

                    reader.BaseStream.Seek(pos, SeekOrigin.Begin);

                    for (int i = 0; i < nodes_count; ++i)
                    {
                        var node_id = reader.ReadInt64();
                        var off = reader.ReadInt64();

                        seed_dict.TryAdd(node_id, off);
                    }

                    Console.WriteLine($"Seed filled in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
                }
            }

            Console.WriteLine("Map loaded");

            var copied = res_dict.ToList();

            {
                var start = Stopwatch.GetTimestamp();
                for (int i = copied.Count - 1; i >= 0; --i)
                {
                    if (!res_dict.TryGetValue(copied[i].Key, out var existing))
                        throw new ArgumentException();
                    if (copied[i].Value != existing)
                        throw new ArgumentException();
                }

                Console.WriteLine($"RES Dict queried in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            }
            {
                var start = Stopwatch.GetTimestamp();
                for (int i = copied.Count - 1; i >= 0; --i)
                {
                    if (!dict.TryGetValue(copied[i].Key, out var existing))
                        throw new ArgumentException();
                    if (copied[i].Value != existing)
                        throw new ArgumentException();
                }

                Console.WriteLine($"Dict queried in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            }
            {
                var start = Stopwatch.GetTimestamp();
                for (int i = copied.Count - 1; i >= 0; --i)
                {
                    if (!seed_dict.TryGetValue(copied[i].Key, out var existing))
                        throw new ArgumentException();
                    if (copied[i].Value != existing)
                        throw new ArgumentException();
                }

                Console.WriteLine($"Seed queried in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            }
            {
                var start = Stopwatch.GetTimestamp();
                for (int i = copied.Count - 1; i >= 0; --i)
                {
                    if (!compact.TryGetValue(copied[i].Key, out var existing))
                        throw new ArgumentException();
                    if (copied[i].Value != existing)
                        throw new ArgumentException();
                }

                Console.WriteLine($"Compact queried in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            }
            {
                var start = Stopwatch.GetTimestamp();
                for (int i = copied.Count - 1; i >= 0; --i)
                {
                    if (!compact0.TryGetValue(copied[i].Key, out var existing))
                        throw new ArgumentException();
                    if (copied[i].Value != existing)
                        throw new ArgumentException();
                }

                Console.WriteLine($"Compact0 queried in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            }
            {
                var start = Stopwatch.GetTimestamp();
                for (int i = copied.Count - 1; i >= 0; --i)
                {
                    if (!res_compact.TryGetValue(copied[i].Key, out var existing))
                        throw new ArgumentException("RES Compact failed " + res_compact.Keys.Contains(copied[i].Key).ToString());
                    if (copied[i].Value != existing)
                        throw new ArgumentException();
                }

                Console.WriteLine($"RES Compact queried in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            }
            {
                var start = Stopwatch.GetTimestamp();
                for (int i = copied.Count - 1; i >= 0; --i)
                {
                    if (!res_compact0.TryGetValue(copied[i].Key, out var existing))
                        throw new ArgumentException();
                    if (copied[i].Value != existing)
                        throw new ArgumentException();
                }

                Console.WriteLine($"RES Compact0 queried in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            }
        }

        private static void RunCalc()
        {
            Geo.GeoCalculator.GetArcSegmentIntersection(GeoPoint.FromDegrees(52.8970606, 18.657783399999996), GeoPoint.FromDegrees(53.02517820000001, 18.657783399999996),
                GeoPoint.FromDegrees(52.97015, 18.65988),
                GeoPoint.FromDegrees(52.97147, 18.655),
                out GeoPoint? cx1, out GeoPoint? cx2);
            Console.WriteLine("Crosspoints");
            Console.WriteLine(cx1);
            Console.WriteLine(cx2);
        }

        private static void runLogger()
        {
            using (Logger.Create(new LoggerSettings()
                   {
                       LogPath = navigator.GetLogPath("log-log.txt")
                   }, out ILogger logger))
            {
                try
                {
                    for (int i = 0; i < 300; ++i)
                    {
                        if (i%5==0)
                            logger.Info($"LONGER This is info longer message than before {i}");
                        else
                            logger.Verbose($"This is verbose message {i}");
                    }

                    try
                    {
                        throw new Exception("foo bar");
                    }
                    catch
                    {
                        logger.Warning("Hello there");
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    ex.LogDetails(logger, MessageLevel.Error);
                }
            }
        }

        private static void runContractionHierarchies()
        {
            using (Logger.Create(new LoggerSettings()
                   {
                       LogPath = navigator.GetLogPath("ch-log.txt")
                   }, out ILogger logger))
            {
                try
                {
                    var finder_config = FinderConfiguration.Defaults();
                    finder_config.PreserveRoundabouts = true;

                    using (RouteManager<TNodeId, TRoadId>.Create(logger, navigator, mapFolder: "kujawsko-pomorskie",
                               finder_config,
                               out var manager))
                    {
                        double start = Stopwatch.GetTimestamp();
                        var preproc = new MultiPreprocessor<TNodeId, TRoadId>(logger, new Navigator(),
                            manager.Map, manager.Calculator, finder_config,
                            UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds());
                        logger.Info($"CH done in {DataFormat.Format(TimeSpan.FromSeconds((Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency))}");
                    }
                }
                catch (Exception ex)
                {
                    ex.LogDetails(logger, MessageLevel.Error);
                }
            }
        }

        private static void runRouter()
        {
            using (Logger.Create(new LoggerSettings()
                   {
                       LogPath = navigator.GetLogPath("log.txt")
                   }, out ILogger logger))
            {
                /*
                        var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile(EnvironmentConfiguration.Filename, optional: false);
            
                        IConfiguration config = builder.Build();
            
                        var env_config = new EnvironmentConfiguration();
                        config.GetSection(EnvironmentConfiguration.SectionName).CustomBind(env_config);
                        env_config.Check();*/

                // going around highway
                //GeoZPoint[] user_points = new[] { GeoZPoint.FromDegreesMeters(53.07388, 18.75152, null), GeoZPoint.FromDegreesMeters(53.08105, 18.75141, null), };
                // going through range
                GeoZPoint[]? user_points = null;
                MapZPoint<OsmId,OsmId>[]? user_places = null;
                var user_configuration = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds();
                user_configuration.CompactingDistanceDeviation = Length.Zero;
                // no leg splitting
                user_configuration.CheckpointIntervalLimit = TimeSpan.Zero;

                if (!DevelModes.True)
                {
                    user_points = new[]
                    {
                        GeoZPoint.FromDegreesMeters(52.9866828918457, 18.867645263671875, 0),
                        GeoZPoint.FromDegreesMeters(53.0147819519043, 19.044799804687496, 0),
                        GeoZPoint.FromDegreesMeters(53.085777282714844, 19.105224609375, 0),
                    };
                }

                if (!DevelModes.True)
                {
                    GeoZPoint south_legal_point = GeoZPoint.FromDegreesMeters(52.90048, 18.57341, null);
                    GeoZPoint north_nozone_point = GeoZPoint.FromDegreesMeters(52.98162, 18.60957, null);

                    GeoZPoint south_deep_nozone_point = GeoZPoint.FromDegreesMeters(52.9265, 18.58358, null);
                    GeoZPoint north_deep_nozone_point = GeoZPoint.FromDegreesMeters(52.96128, 18.59625, null);
                    //user_points = new[] { south_legal_point, north_nozone_point, };
                    user_points = new[]
                    {
                        north_nozone_point,
                        south_legal_point,
                    };
                }

                {
                    GeoZPoint torun_bridge = GeoZPoint.FromDegreesMeters(53.00061, 18.60424, null); // most w toruniu
                    GeoZPoint inowroclaw_tupadly = GeoZPoint.FromDegreesMeters(52.75295, 18.25615, null);

                    if (!DevelModes.True)
                    {
                        // wersja memory (wszystko jest wrzucane w oszczedny pamieciowo sposob do pamieci, zero operacji na dysku podczas liczenia) Time route: 3.008533185 s, 2.607847712 s


                        // top -o %MEM
                        // dla tej trasy szczytowa pamiec to ok. 9GB 
                        // ale po wczytaniu map, zrobieniu grida spada do 8GB
                        // podczas liczenia trasy w ogole nie widac wzrostu pamieci
                        // glowny czynnik zajetosci RAM, to same mapy

                        // przy korzystaniu z double-passa i same funkcji odciecia zbyt dalekich punktow, liczba analizowanych
                        // punktow ZWIEKSZYLA SIE, z 84095 do 84630 (nie rozumiem tego zupelnie); obliczona trasa
                        // na szczescie jest identyczna (to jest troche bez sensu, bo rejected nodes = 0, wiec z czego
                        // wynika wieksza liczba na wejsciu?)
                        user_points = new[]
                        {
                            torun_bridge,
                            inowroclaw_tupadly,
                        };
                        // exact distance to target: 48_304 updates
                        // approx distance to target: 70_922 updates
                    }

                    if (!DevelModes.True)
                    {
                        user_points = new[]
                        {
                            torun_bridge,
                            GeoZPoint.FromDegreesMeters(52.935, 18.5158, null), // in the middle of high-traffic road
                            inowroclaw_tupadly,
                        };
                    }
                }

                if (!DevelModes.True)
                {
                    user_points = new[]
                    {
                        // sprawdzenie wagi, zeby program odbijal z chodnika przy szosie
                        //GeoZPoint.FromDegreesMeters(52.91243, 18.48199, null), // nieco wczesniej przed Suchatowka                   
                        GeoZPoint.FromDegreesMeters(52.91158, 18.47988, null), // przed Suchatowka
                        GeoZPoint.FromDegreesMeters(52.89978, 18.47233, null), // za Suchatowka
                    };
                }

                if (!DevelModes.True)
                {
                    user_points = new[]
                    {
                        // Cierpice. Mapa zawiera blad, bo sciezka ktora wybiera nie przecina sie niby z torami kolejowymi
                        GeoZPoint.FromDegreesMeters(52.98716, 18.49451, null),
                        GeoZPoint.FromDegreesMeters(52.98662, 18.46635, null),
                    };
                }

                if (!DevelModes.True)
                {
                    // test dlugiej trasy, Torun-Osie
                    user_points = new[]
                    {
                        GeoZPoint.FromDegreesMeters(53.024, 18.60917, null), // Torun 
                        GeoZPoint.FromDegreesMeters(53.61702, 18.27056, null), // Osie
                    };
                }

                if (!DevelModes.True)
                {
                    // something really short but with 3 points to check out smoothing middle points
                    user_points = new[]
                    {
                        GeoZPoint.FromDegreesMeters(53.38503, 18.93153, null),
                        GeoZPoint.FromDegreesMeters(53.38563, 18.93642, null),
                        GeoZPoint.FromDegreesMeters(53.38943, 18.93627, null),
                    };
                }

                if (!DevelModes.True)
                {
                    // m n#9223673711@52.40488815307617°, 19.027236938476562° to 53.59299850463868°, 17.86724090576172°

                    user_places = new[]
                    {
                        new MapZPoint<OsmId,OsmId>(new GeoZPoint(),OsmId.PureOsm( 9223673711)
#if DEBUG
                            , DEBUG_mapRef:null
#endif
                        ),
                        new MapZPoint<OsmId,OsmId>(GeoZPoint.FromDegreesMeters(53.59299850463868, 17.86724090576172, null), null
#if DEBUG
                            , DEBUG_mapRef:null
#endif
                        ),
                    };
                    user_configuration = new UserRouterPreferences() {HACK_ExactToTarget = false}.SetUniformSpeeds();
                }

                if (DevelModes.True)
                {
                    // testing for computation speed
                    // mapy.cz -- 4.92 seconds
                    // TR -- 19.81293994 s (z cala mapa polski 25s)
                    // NUC z cala Polska -- 9 s
                    // NUC z cala Polska bidir -- 7.1 s
                    // NUC z cala Polska bidir singlepass -- 6.9 s (krotko mowiac, pozytku z fast-pass nie ma)
                    //  1 019 549 updates
                    // cell size 182, limit 20000: 17s
                    // cell size 120, limit 13000: 16s (!)
                    // cell size 50, limit 4000: 15s (!)
                    // cell size 50, limit 4000: 15s (!) [this time without searching for labels]


                    user_points = new[]
                    {
                        GeoZPoint.FromDegreesMeters(52.4049, 19.02756, null), // Chodecz 
                        GeoZPoint.FromDegreesMeters(53.593, 17.86724, null), // Tuchola
                    };
                }

                if (!DevelModes.True)
                {
                    // testing if Poland-long route can be computed
                    user_points = new[]
                    {
                        GeoZPoint.FromDegreesMeters(49.05505, 22.70635, null), // Bieszczady  
                        GeoZPoint.FromDegreesMeters(53.91467, 14.23544, null), // Swinoujscie 
                    };
                }

                if (!DevelModes.True)
                {
                    // just testing node routing
                    user_places = new[]
                    {
                        // https://www.openstreetmap.org/node/1545695728
                        new MapZPoint<OsmId,OsmId>(new GeoZPoint(),OsmId.PureOsm( 1545695728L)
                        #if DEBUG
                            ,DEBUG_mapRef:null
                        #endif
                            ),
                        // https://www.openstreetmap.org/node/3661740250
                        new MapZPoint<OsmId,OsmId>(new GeoZPoint(),OsmId.PureOsm( 3661740250L)
#if DEBUG
                            ,DEBUG_mapRef:null
#endif
                        ),
                    };
                }

                if (!DevelModes.True)
                {
                    user_points = new[]
                    {
                        // prostowanie ronda 
                        GeoZPoint.FromDegreesMeters(53.27968, 18.93167, null), // rondo w Wabrzeznie 
                        GeoZPoint.FromDegreesMeters(53.27856, 18.93023, null),
                    };
                }

                //track = track.Reverse().ToArray();

                findRoute<WorldIdentifier, WorldIdentifier>(logger, user_configuration,
                    user_points, user_places, gridCellSize: 50);
            }
        }

        private static void RunCity<TNodeId, TRoadId>(ILogger logger, GeoPoint focusPoint)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            var user_configuration = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds();


            var sys_config = FinderConfiguration.Defaults() with {EnableDebugDumping = true, CompactPreservesRoads = true};
            sys_config.MemoryParams.SetMapMode(WorldMapExtension.GetMapMode<TNodeId, TRoadId>());

            logger.Info($"Running find with {sys_config.MemoryParams}");

            using (RouteManager<TNodeId, TRoadId>.Create(logger, navigator, mapFolder: "kujawsko-pomorskie",
                       sys_config,
                       out var manager))
            {
                double start = Stopwatch.GetTimestamp();

                var cities = manager.GetCities(focusPoint, Length.FromKilometers(7));


                logger.Info($"Time cities in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");


                using (FileStream stream = new FileStream(Navigator.GetUniquePath(navigator.GetOutputDirectory(),
                           "cities.kml"), FileMode.CreateNew))
                {
                    var input = new TrackWriterInput();
                    input.Waypoints = cities.Select(it => new WaypointDefinition(it.point.Convert3d(),
                            it.info.Name, null, null))
                        .ToList();
                    input.SaveDecorated(stream);
                }
            }
        }

        public static async ValueTask RunRestRoutingAsync(ILogger logger)
        {
            var host = TrackPlanner.WebUI.Server.Program.CreateHost(new string[] { });

            await host.StartAsync(CancellationToken.None).ConfigureAwait(false);

            var default_config = new ScheduleSettings();
            using (var http_client = new HttpClient() {Timeout = TimeSpan.FromMinutes(30)})
            {
                var worker = new RestPlannerClient<TNodeId, TRoadId>(logger,new JsonRestClient(http_client), new SystemConfiguration().PlannerServer);

                double start = Stopwatch.GetTimestamp();
                await worker.GetPlanAsync(new PlanRequest<TNodeId>()
                {
                    DailyPoints = new List<List<RequestPoint<TNodeId>>>()
                    {
                        new List<RequestPoint<TNodeId>>()
                        {
                            new RequestPoint<TNodeId>(GeoPoint.FromDegrees(53.024, 18.60917), false, true), // Torun 
                            new RequestPoint<TNodeId>(GeoPoint.FromDegrees(53.61702, 18.27056), false, true), // Osie
                        }
                    },
                    RouterPreferences = default_config.RouterPreferences,
                    TurnerPreferences = default_config.TurnerPreferences,
                }, calcReal: true, CancellationToken.None).ConfigureAwait(false);

                logger.Info($"Route received in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");
            }
        }

        public static async ValueTask RunRestAttractionsAsync(ILogger logger)
        {
            var host = TrackPlanner.WebUI.Server.Program.CreateHost(new string[] { });

            await host.StartAsync(CancellationToken.None).ConfigureAwait(false);

            var default_config = new ScheduleSettings();
            using (var http_client = new HttpClient() {Timeout = TimeSpan.FromMinutes(30)})
            {
                var worker = new RestPlannerClient<TNodeId, TRoadId>(logger, new JsonRestClient(http_client),
                    new SystemConfiguration().PlannerServer);

                double start = Stopwatch.GetTimestamp();
                var attr_result = await worker.FindAttractionsAsync(new InterestRequest()
                {
                    CheckPoints = new List<GeoPoint>()
                    {
                        GeoPoint.FromDegrees(53.024, 18.60917), // Torun 
                        GeoPoint.FromDegrees(53.61702, 18.27056), // Osie
                    },
                    Range = new UserPlannerPreferences().AttractionsRange,
                    ExcludeFeatures = new UserPlannerPreferences().ExcludeAttractions,
                }, CancellationToken.None).ConfigureAwait(false);

                if (attr_result.HasValue)
                logger.Info($"{attr_result.ProblemMessage.TryFullMessage()} {attr_result.Value.Attractions.Count} attractions received in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");
                else
                    logger.Error(attr_result.ProblemMessage.EnsureFullMessage());
            }
        }

        public static void CompareRouting(ILogger logger)
        {
            GeoZPoint[]? user_points = null;
            var user_configuration = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds();

            user_points = new[]
            {
                GeoZPoint.FromDegreesMeters(53.024, 18.60917, null), // Torun 
                GeoZPoint.FromDegreesMeters(53.61702, 18.27056, null), // Osie
            };

            //  findRoute<long,long>(logger, user_configuration, user_points, userPlaces:null);
            findRoute<WorldIdentifier, WorldIdentifier>(logger, user_configuration, user_points,
                osmUserPlaces: null, gridCellSize: 182);
            // findRoute<WorldIdentifier,WorldIdentifier>(logger, user_configuration, user_points, userPlaces:null,gridCellSize:150);
            //findRoute<WorldIdentifier,WorldIdentifier>(logger, user_configuration, user_points, userPlaces:null,gridCellSize:100);
        }

        private static void findRoute<TNodeId, TRoadId>(ILogger logger, UserRouterPreferences userConfig,
            GeoZPoint[]? userPoints, MapZPoint<OsmId,OsmId>[]? osmUserPlaces, int? gridCellSize = null)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            userConfig.FastModeLimit = null;
            var finder_config = FinderConfiguration.Defaults() with
            {
                EnableDebugDumping = true,
                CompactPreservesRoads = true,
                TwoPassStage = false,
                AStarMode = true,
            };
            finder_config.MemoryParams.SetMapMode(WorldMapExtension.GetMapMode<TNodeId, TRoadId>());
            if (gridCellSize != null)
                finder_config.MemoryParams.GridCellSize = gridCellSize.Value;
            finder_config.MemoryParams.WorldCacheCellsLimit = 4_000;

            logger.Info($"Running find with {finder_config.MemoryParams}");

            using (RouteManager<TNodeId, TRoadId>.Create(logger, navigator, mapFolder: "kujawsko-pomorskie",
                       finder_config,
                       out var manager))
            {
                if (!DevelModes.True)
                {
                    long start = Stopwatch.GetTimestamp();
                    var cover_grid = new CoverGrid<TNodeId, TRoadId>(logger,
                        RouteManager<TNodeId, TRoadId>.Create(logger.With(new SinkSettings()
                        {
                            FileLevel = MessageLevel.Warning,
                            ConsoleLevel = MessageLevel.Warning,
                        }), navigator, manager.Map, finder_config), 
                        userConfig, CancellationToken.None);
                    logger.Info($"Shortcuts computed in {((Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency)}s");
                    manager.EnableShortcuts(cover_grid);
                }
                if (!DevelModes.True)
                manager.Map.ComputeRoadExtras(logger, finder_config.MapSettings.RedundantCyclewayRange,
                    manager.Map.FromOsmRoadId(OsmId.PureOsm( 681702453L)),
                    manager.Map.FromOsmRoadId(OsmId.PureOsm( 681702452L)),
                    manager.Map.FromOsmRoadId(OsmId.PureOsm( 681702451L))
                    );
                
                if (!DevelModes.True)
                {
                    double start = Stopwatch.GetTimestamp();

                    Result<RoutePlan<TNodeId,TRoadId>> res_plan = manager.TryFindCompactRoute(
                        UserRouterPreferencesHelper.CreateBikeOriented().SetUniformSpeeds(),
                        userPoints!.Select(it => new RequestPoint<TNodeId>(it.Convert2d(), false, true)).ToArray(),
                        CancellationToken.None);
                    if (!res_plan.HasValue)
                        throw new Exception();

                    Console.WriteLine($"Distance route in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");

                    using (FileStream stream = new FileStream(Navigator.GetUniquePath(navigator.GetOutputDirectory(),
                               "distance-track-with-turns.kml"), FileMode.CreateNew))
                    {
                        TrackWriter.SaveAsKml(UserVisualPreferences.Defaults(), stream, "something meaningful", 
                            res_plan.Value);
                    }
                }

                //if (false)
                {
                    double start = Stopwatch.GetTimestamp();


                    RoutePlan<TNodeId, TRoadId>? computed_track = null;
                    if (userPoints != null)
                    {
                        Result<RoutePlan<TNodeId,TRoadId>> res_plan = manager.TryFindCompactRoute(userConfig, userPoints
                                .Select(it => new RequestPoint<TNodeId>(it.Convert2d(), allowSmoothing: false,
                                    findLabel:false)).ToArray(),
                            CancellationToken.None);
                        if (!res_plan.HasValue)
                            throw new Exception(res_plan.ProblemMessage.TryFullMessage());
                    }
                    else if (osmUserPlaces != null)
                    {
                        RequestPoint<TNodeId>[] user_places = osmUserPlaces
                            .Select(it => manager.Map.ConvertFromOsm(it))
                            .Select(it => new RequestPoint<TNodeId>(it.Point.Convert2d()) {NodeId = it.NodeId})
                            .ToArray();
                        Result<RoutePlan<TNodeId,TRoadId>> res_plan = manager.TryFindRoute(userConfig, user_places, 
                            allowSmoothing: false,allowGap:false, CancellationToken.None);
                        if (!res_plan.HasValue)
                            throw new Exception(res_plan.ProblemMessage.TryFullMessage());
                    }
                    else
                        logger.Warning("No input given");

                    logger.Info($"Time route in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");


                    if (computed_track != null)
                    {
                        using (FileStream stream = new FileStream(Navigator.GetUniquePath(navigator.GetOutputDirectory(),
                                   "time-track.kml"), FileMode.CreateNew))
                        {
                            TrackWriter.SaveAsKml(UserVisualPreferences.Defaults(), stream, "something meaningful",
                                MapTranslator.ConvertToOsm(manager.Map, computed_track));
                        }
                    }
                }
            }
        }
    }
}