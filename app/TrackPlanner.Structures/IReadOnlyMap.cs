﻿using System.Collections.Generic;

namespace TrackPlanner.Structures
{
    public interface IReadOnlyMap<TKey, TValue> : IReadOnlyEnumerableDictionary<TKey,TValue>
    {
        int Count { get; }
        IEnumerable<TKey> Keys { get; }
        IEnumerable<TValue> Values { get; }
    }
   
}