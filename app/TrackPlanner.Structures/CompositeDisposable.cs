﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

#nullable enable

namespace TrackPlanner.Structures
{
    public sealed class CompositeDisposable : IDisposable
    {
        public static CompositeDisposable Create(IEnumerable<Action> stack) 
        {
            return new CompositeDisposable(stack.ToArray());
        }
        public static CompositeDisposable Create(IEnumerable<IDisposable> stack) 
        {
            return new CompositeDisposable(toActions(stack));
        }

        public static IDisposable None { get; } = new CompositeDisposable(Array.Empty<IDisposable>());
        
        private readonly Stack<Action> stack;

        public CompositeDisposable(params Action[] stack)
        {
            this.stack = new Stack<Action>(stack);
        }

        public CompositeDisposable(params IDisposable[] stack) : this(toActions(stack))
        {
        }

        private static Action[] toActions(IEnumerable<IDisposable> stack)
        {
            return stack.Select<IDisposable, Action>(it => it.Dispose).ToArray();
        }

        public CompositeDisposable Stack( IDisposable top)
        {
            return Stack(this, top);
        }
        public CompositeDisposable Stack( Action top)
        {
            return Stack(this, top);
        }


        public static CompositeDisposable Stack( IDisposable stack,Action top)
        {
            return new CompositeDisposable(stack.Dispose,top );
        }

        public static CompositeDisposable Stack( IDisposable stack,IDisposable top)
        {
            return new CompositeDisposable( stack.Dispose,top.Dispose);
        }

        public void Dispose()
        {
            foreach (var disp in this.stack)
                disp();
        }
    }
}