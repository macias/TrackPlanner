namespace TrackPlanner.Structures
{
    public static class DevelModes
    {
        // a flag/variable which compiler will not be able to deduce in compile it is true
        // used to disable parts of code
        public static bool True => true;
    }
}