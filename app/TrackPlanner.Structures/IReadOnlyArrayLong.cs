﻿namespace TrackPlanner.Structures
{
    public interface IReadOnlyArrayLong<TValue> : IReadOnlyMap<long, TValue>
    {
         // this interface guarantees linear indexing (like array)
    }
}