﻿using System;

namespace TrackPlanner.Structures
{
    public static class AfterMath
    {
        public static double TrueMod(double k,double n)
        {
            // https://stackoverflow.com/a/6400477/210342
            return k - n * Math.Floor(k / n);
        }
    }
}