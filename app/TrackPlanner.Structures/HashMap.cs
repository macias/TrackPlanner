﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace TrackPlanner.Structures
{
    public static class HashMap
    {
        public static HashMap<TKey, TValue> ToHashMap<T,TKey,TValue>(this IEnumerable<T> source,
            Func<T,TKey>keySelector,Func<T,TValue> valueSelector)
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(source.ToDictionary(keySelector,valueSelector));
        }
        public static HashMap<TKey, TValue> Create<TKey, TValue>()
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(0);
        }

        public static HashMap<TKey, TValue> Create<TKey, TValue>(int capacity)
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(capacity);
        }

        public static HashMap<TKey, TValue> Create<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> sequence)
            where TKey : IEquatable<TKey>
        {
            return new HashMap<TKey, TValue>(new Dictionary<TKey, TValue>( sequence));
        }
    }
    
    public sealed class HashMap<TKey, TValue> : IMap<TKey,TValue>
        where TKey : notnull
    {
        private  Dictionary<TKey, TValue> dict;

        public IEnumerable<TKey> Keys => this.dict.Keys;

        public int Count => this.dict.Count;

        public IEnumerable<TValue> Values => this.dict.Values;

        public TValue this[TKey index]
        {
            get { return this.dict[index]; }
            set { this.dict[index] = value; }
        }

        internal HashMap(int capacity) 
        {
            this.dict = new Dictionary<TKey, TValue>(capacity);
        }

        internal HashMap(Dictionary<TKey, TValue> source)
        {
            this.dict = source;
        }

        public bool TryAdd(TKey key, TValue value, [MaybeNullWhen(true)] out TValue existing)
        {
            if (this.dict.TryGetValue(key, out existing))
                return false;
            else
            {
                this.dict.Add(key, value);
                return true;
            }
        }

        public bool TryAdd(TKey key, TValue value)
        {
            return TryAdd(key, value, out _);
        }

        public bool TryGetValue(TKey index, [MaybeNullWhen(false)] out TValue value)
        {
            return this.dict.TryGetValue(index, out value);
        }

        public void Add(TKey index, TValue value)
        {
            this.dict.Add(index, value);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this.dict.GetEnumerator();
        }

        public bool ContainsKey(TKey idx)
        {
            return this.dict.ContainsKey(idx);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void ExceptWith(IEnumerable<TKey> keys)
        {
            foreach (TKey idx in keys)
                this.dict.Remove(idx);
        }
        
        public void IntersectWith(IEnumerable<TKey> keys)
        {
            var intersection = new Dictionary<TKey, TValue>(capacity:this.dict.Count, this.dict.Comparer);
            foreach (var k in keys)
                if (this.dict.TryGetValue(k,out var value))
                    intersection.TryAdd(k,value);

            this.dict = intersection;
        }

        public void TrimExcess()
        {
            this.dict.TrimExcess();
        }

        public void Clear()
        {
            this.dict.Clear();
        }
    }
}