﻿using System;

namespace TrackPlanner.Structures
{
    public sealed class CacheGetter<T>
    {
        private readonly Func<T> getter;
        private Option<T> cache;

        public T Value
        {
            get
            {
                if (!this.cache.HasValue)
                {
                    this.cache = new Option<T>(this.getter());
                }

                return this.cache.Value;
            }
        }

        public CacheGetter(Func<T> getter)
        {
            this.getter = getter;
            this.cache = new Option<T>();
        }

        public bool Reset()
        {
            if (!this.cache.HasValue)
                return false;
            this.cache = new Option<T>();
            return true;
        }
    }
}