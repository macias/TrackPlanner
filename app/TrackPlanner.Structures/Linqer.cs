﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;

namespace TrackPlanner.Structures
{
    public static class Linqer
    {
        private static readonly MethodInfo hashCodeAdd;

        static Linqer()
        {
            hashCodeAdd = typeof(HashCode).GetMethod("Add", BindingFlags.NonPublic | BindingFlags.Instance)!;
        }

        public static void ThrowCanceledException(this CancellationToken token, string message)
        {
            // do NOT add "if" here, because message would be evaluated always
            throw new OperationCanceledException(message, token);
        }

        public static bool IsGreater<T>(this IComparer<T> comparer, T a, T b)
        {
            return comparer.Compare(a, b) > 0;
        }


        public static int FindLastIndex<T>(this IReadOnlyList<T> list, Func<T, bool> predicate)
        {
            for (int i = list.Count - 1; i >= 0; --i)
                if (predicate(list[i]))
                    return i;

            return -1;
        }

        public static int FindIndex<T>(this IReadOnlyList<T> list, Func<T, bool> predicate)
        {
            return list.FindIndex(0, predicate);
        }

        public static int FindIndex<T>(this IReadOnlyList<T> list, int startIndex, Func<T, bool> predicate)
        {
            for (int i = startIndex; i < list.Count; ++i)
                if (predicate(list[i]))
                    return i;

            return -1;
        }

        public static void InsertRange<T>(this List<T> list, Index index, IEnumerable<T> collection)
        {
            list.InsertRange(index.GetOffset(list.Count), collection);
        }

        public static bool IsLess<T>(this IComparer<T> comparer, T a, T b)
        {
            return comparer.Compare(a, b) < 0;
        }

        public static IEnumerable<T> SkipLastWhile<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            if (enumerable is IReadOnlyList<T> list)
            {
                for (int i = list.Count - 1; i >= 0; --i)
                {
                    if (!predicate(list[i]))
                        return list.Take(i + 1);
                }

                return ArraySegment<T>.Empty;
            }
            else
                return enumerable.Reverse().SkipWhile(predicate).Reverse();
        }

        public static bool IndexInRange<T>(this IReadOnlyList<T> list, int index)
        {
            return index >= 0 && index < list.Count;
        }

        public static string FuncName<T>(this T _this, [CallerMemberName] string? callerName = null)
        {
            return callerName ?? "?";
        }

        public static T Max<T>(this T a, T b)
            where T : IComparable<T>
        {
            if (Comparer<T>.Default.Compare(a, b) < 0)
                return b;
            else
                return a;
        }

        public static void InvokeAddHashCode(this HashCode hashCode, int hashCodeValue)
        {
            hashCodeAdd.Invoke(hashCode, new object[] {hashCodeValue});
        }

        public static int GetUnorderedHashCode<T>(this IEnumerable<T> set)
        {
            // primary usage are sets, because set is unordered collection so weights
            // cannot be applied to the hash code values
            return set.Aggregate(0, (acc, it) => acc ^ it?.GetHashCode() ?? 0);
        }

        public static int ApproxLinearIndexOf<T>(this IReadOnlyList<T> collection, Func<T, double> selector, double value)
        {
            // if exact match exists, it will return its index, if not -- it will return the almost-closest index to it
            // almost -- because it might be off by +1/-1 index
            if (collection.Count == 0)
                return -1;

            int start_idx = 0;
            int end_idx = collection.Count - 1;
            while (true)
            {
                if (start_idx == end_idx)
                    return start_idx;

                var start_value = selector(collection[start_idx]);

                var step = (selector(collection[end_idx]) - start_value) / (end_idx - start_idx);
                var index = Math.Max(start_idx, Math.Min(end_idx, (int) Math.Round((value - start_value) * step)));
                var curr_value = selector(collection[index]);
                if (curr_value > value)
                    end_idx = Math.Max(start_idx, index - 1);
                else if (curr_value < value)
                    start_idx = Math.Min(end_idx, index + 1);
                else
                    return index;
            }
        }

        public static IReadOnlyList<T> ReadOnlyList<T>(this IReadOnlyList<T> list) => list;

        // covariance helper 
        public static T Me<T>(this T t) => t;

        public static IEnumerable<T> Concat<T>(this IEnumerable<T> colletion, params T[] elems)
        {
            return Enumerable.Concat(colletion, elems);
        }

        public static Option<T> SingleOrNone<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.SingleOrNone(_ => true);
        }

        public static Option<T> SingleOrNone<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            using (var iter = enumerable.Where(predicate).GetEnumerator())
            {
                if (!iter.MoveNext()) // no elements
                    return Option<T>.None;

                var result = iter.Current;

                if (iter.MoveNext()) // multiple elements
                    return Option<T>.None;

                return new Option<T>(result);
            }
        }

        public static int GetCapacity<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
            where TKey : notnull
        {
            var dict_type = dictionary.GetType();
            var buckets_field = dict_type.GetField("_buckets", BindingFlags.NonPublic | BindingFlags.Instance)!;
            var buckets = buckets_field.GetValue(dictionary) as int[];
            return buckets!.Length;
        }

        public static T RemoveFirst<T>(this List<T> list)
        {
            var result = list[0];
            list.RemoveAt(0);
            return result;
        }

        public static T RemoveLast<T>(this List<T> list)
        {
            var result = list[^1];
            list.RemoveAt(list.Count - 1);
            return result;
        }

        public static void Expand<T>(this List<T> list, int size)
        {
            if (size > list.Count)
            {
                if (size > list.Capacity)
                    list.Capacity = size;
                for (int i = size - list.Count; i >= 0; --i)
                    list.Add(default!);
            }
        }

        public static void AddRange<T>(this HashSet<T> set, IEnumerable<T> values)
        {
            foreach (T elem in values)
                set.Add(elem);
        }

        public static void AddRange<TKey,TValue>(this IMap<TKey,TValue> map, 
            IEnumerable<KeyValuePair<TKey, TValue>> other)
            where TKey : notnull
        {
            foreach (var (key,val) in other)
                map.Add(key,val);
        }

        public static void RemoveExcept<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key)
            where TKey : notnull
        {
            bool has_key = dict.TryGetValue(key, out TValue? value);
            dict.Clear();
            if (has_key)
                dict.Add(key, value!);
        }

        public static int CountAtLeast<T>(this IEnumerable<T> enumerable, int limit)
        {
            if (limit < 0)
                throw new ArgumentOutOfRangeException($"{nameof(limit)} = {limit}");

            using (var iter = enumerable.GetEnumerator())
            {
                int count = 0;
                for (; count < limit; ++count)
                {
                    if (!iter.MoveNext())
                        return count;
                }

                return count;
            }
        }

        public static IEnumerable<(T prev, T next)> Slide<T>(this IEnumerable<T> enumerable, bool wrapAround = false)
        {
            using (var iter = enumerable.GetEnumerator())
            {
                if (!iter.MoveNext())
                    yield break;

                var first = iter.Current;
                var prev = first;
                Option<T> last = Option<T>.None;

                while (iter.MoveNext())
                {
                    var curr = iter.Current;
                    yield return (prev, curr);
                    prev = curr;
                    last = new Option<T>(curr);
                }

                if (last.HasValue && wrapAround)
                    yield return (last.Value, first);
            }
        }

        public static IEnumerable<T> Concat<T>(this IEnumerable<T> enumerable, T extra)
        {
            foreach (T elem in enumerable)
                yield return elem;
            yield return extra;
        }

        public static IEnumerable<(T item, int index)> ZipIndex<T>(this IEnumerable<T> enumerable)
        {
            int count = 0;
            foreach (T elem in enumerable)
                yield return (elem, count++);
        }

        public static int IndexOf<T>(this IEnumerable<T> enumerable, T elem)
        {
            return enumerable.IndexOf(it => EqualityComparer<T>.Default.Equals(it, elem));
        }

        public static int IndexOf<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            return enumerable.IndexOf(0, predicate);
        }

        public static int IndexOf<T>(this IEnumerable<T> enumerable, int startIndex, Func<T, bool> predicate)
        {
            int index = startIndex;
            foreach (T elem in enumerable.Skip(startIndex))
            {
                if (predicate(elem))
                    return index;
                ++index;
            }

            return -1;
        }

        public static int LastIndexOf<T>(this IReadOnlyList<T> enumerable, Func<T, bool> predicate)
        {
            return LastIndexOf(enumerable, enumerable.Count - 1, predicate);
        }

        public static int LastIndexOf<T>(this IReadOnlyList<T> enumerable, int startIndex, Func<T, bool> predicate)
        {
            for (int i = startIndex; i >= 0; --i)
            {
                if (predicate(enumerable[i]))
                    return i;
            }

            return -1;
        }

        public static Option<T> FirstOrNone<T>(this IEnumerable<T> enumerable)
        {
            using (var iter = enumerable.GetEnumerator())
            {
                if (!iter.MoveNext())
                    return Option<T>.None;
                else
                    return new Option<T>(iter.Current);
            }
        }

        public static Option<T> LastOrNone<T>(this IEnumerable<T> enumerable)
        {
            using (var iter = enumerable.GetEnumerator())
            {
                Option<T> last = Option<T>.None;

                while (iter.MoveNext())
                {
                    last = new Option<T>(iter.Current);
                }

                return last;
            }
        }


        public static Option<T> FirstOrNone<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            return enumerable.Where(predicate).FirstOrNone();
        }

        public static Option<T> LastOrNone<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            return enumerable.Where(predicate).LastOrNone();
        }

        public static bool TryMinMax<T>(this IEnumerable<T> enumerable,
            [MaybeNullWhen(false)] out T min, [MaybeNullWhen(false)] out T max)
        {
            using (var iter = enumerable.GetEnumerator())
            {
                if (!iter.MoveNext())
                {
                    min = default;
                    max = default;
                    return false;
                }

                min = iter.Current;
                max = iter.Current;

                var comparer = Comparer<T>.Default;
                while (iter.MoveNext())
                {
                    {
                        int comp = comparer.Compare(min, iter.Current);
                        if (comp > 0)
                            min = iter.Current;
                    }
                    {
                        int comp = comparer.Compare(max, iter.Current);
                        if (comp < 0)
                            max = iter.Current;
                    }
                }
            }

            return true;
        }

        private static IEnumerable<T> take<T>(IEnumerator<T> iter, int count)
        {
            while (count > 0 && iter.MoveNext())
            {
                yield return iter.Current;
                --count;
            }
        }

        public static IEnumerable<IEnumerable<TSource>> Partition<TSource>(this IEnumerable<TSource> enumerable,
            IEnumerable<int> sizes)
        {
            using (var iter = enumerable.GetEnumerator())
            {
                foreach (var size in sizes)
                {
                    yield return take(iter, size);
                }
            }
        }

        public static List<T> SoftList<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable is List<T> list)
                return list;
            else
                return enumerable.ToList();
        }

        public static IEnumerable<IEnumerable<TSource>> Partition<TSource, TKey>(this IEnumerable<TSource> enumerable,
            Func<TSource, TKey> keySelector)
        {
            return Partition(enumerable, keySelector, EqualityComparer<TKey>.Default);
        }

        public static IEnumerable<IEnumerable<TSource>> Partition<TSource, TKey>(this IEnumerable<TSource> enumerable,
            Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            using (var iter = enumerable.GetEnumerator())
            {
                if (!iter.MoveNext())
                    yield break;

                var buffer = new List<TSource>();

                buffer.Add(iter.Current);

                while (iter.MoveNext())
                {
                    if (!comparer.Equals(keySelector(iter.Current), keySelector(buffer[^1])))
                    {
                        yield return buffer;
                        buffer = new List<TSource>(); // do not use clear!
                    }

                    buffer.Add(iter.Current);
                }

                yield return buffer;
            }
        }

        public static IEnumerable<T> ConsecutiveDistinct<T>(this IEnumerable<T> enumerable)
            where T : IEquatable<T>
        {
            return enumerable.ConsecutiveDistinctBy(x => x);
        }

        public static IEnumerable<TSource> ConsecutiveDistinctBy<TSource, TKey>(this IEnumerable<TSource> enumerable,
            Func<TSource, TKey> selector)
            where TKey : IEquatable<TKey>
        {
            return enumerable.ConsecutiveDistinctBy(selector, EqualityComparer<TKey>.Default);
        }

        public static IEnumerable<TSource> ConsecutiveDistinctBy<TSource, TKey>(this IEnumerable<TSource> enumerable,
            Func<TSource, TKey> selector, IEqualityComparer<TKey> comparer)
        {
            return ConsecutiveDistinctBy(enumerable, (a, b) => comparer.Equals(selector(a), selector(b)));
        }

        public static IEnumerable<TSource> ConsecutiveDistinctBy<TSource>(this IEnumerable<TSource> enumerable,
            Func<TSource, TSource, bool> comparer)
        {
            using (var iter = enumerable.GetEnumerator())
            {
                if (!iter.MoveNext())
                    yield break;

                TSource last_selected;

                last_selected = iter.Current;
                yield return last_selected;

                while (iter.MoveNext())
                {
                    if (!comparer(iter.Current, last_selected))
                    {
                        last_selected = iter.Current;
                        yield return last_selected;
                    }
                }
            }
        }

        /*public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> enumerable, Func<TSource, TKey> selector)
        {
            var seen = new HashSet<TKey>();

            using (var iter = enumerable.GetEnumerator())
            {
                while (iter.MoveNext())
                {
                    TKey key = selector(iter.Current);
                    if (seen.Add(key))
                        yield return iter.Current;
                }
            }
        }*/

        public static Dictionary<TKey, IReadOnlyList<TValue>> Intersect<TKey, TValue>(IReadOnlyDictionary<TKey, TValue> first,
            IEnumerable<KeyValuePair<TKey, TValue>> second)
            where TKey : notnull
        {
            IReadOnlyList<TValue> caster(List<TValue> list) => list;

            return Intersect(first, second, (a, b) => caster(new List<TValue>() {a, b}));
        }

        public static Dictionary<TKey, TResult> Intersect<TKey, TValue, TResult>(IReadOnlyDictionary<TKey, TValue> first,
            IEnumerable<KeyValuePair<TKey, TValue>> second,
            Func<TValue, TValue, TResult> combine)
            where TKey : notnull
        {
            var result = new Dictionary<TKey, TResult>();
            foreach (var entry in second)
            {
                if (first.TryGetValue(entry.Key, out TValue? idx_along_road))
                {
                    result.Add(entry.Key, combine(idx_along_road, entry.Value));
                }
            }

            return result;
        }

        public static void Coalesce<T>(ref T? value, T? other)
            where T : class
        {
            // this method is only for forcing the second argument to be evaluated
            // if we use the coalesce operator the logic could be short-circuited
            value ??= other;
        }
    }
}