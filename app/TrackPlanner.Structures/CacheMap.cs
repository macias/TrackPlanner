﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace TrackPlanner.Structures
{
    public sealed class CacheMap<TKey,TValue> 
        where TKey : notnull
    {
        private readonly CompactDictionaryFill<TKey, (LinkedListNode<TKey> historyNode,TValue value)> data;
        private readonly Action<TValue> cleaner;
        private readonly int memoryLimit;
        
        private int DEBUG_clearCounter;
        private readonly LinkedList<TKey> history;

        /*public TValue this[TKey key]
        {
            get
            {
                if (!TryGetValue(key, out var result))
                    throw new ArgumentException($"Key not found {key}");

                return result;
            }
        }*/
        
        public CacheMap(Action<TValue> cleaner, int memoryLimit,IEqualityComparer<TKey> keyComparer)
        {
            this.cleaner = cleaner;
            this.memoryLimit = memoryLimit;
            this.data = new CompactDictionaryFill<TKey, (LinkedListNode<TKey>,TValue)>(keyComparer, capacity:memoryLimit+1);
            this.history = new LinkedList<TKey>();
        }

        public void Clear()
        {
            this.DEBUG_clearCounter = 0;

            foreach (var disp in this.data.Values)
                this.cleaner(disp.value);
            this.data.Clear();
            this.history.Clear();
        }
        
        public bool ContainsKey(TKey key)
        {
            return TryGetValue(key, out _);
        }

        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value)
        {
            if (this.data.TryGetValue(key, out var value_entry))
            {
                value = value_entry.value;
                this.history.Remove(value_entry.historyNode);
                this.history.AddLast(value_entry.historyNode);
                return true;
            }

            value = default;
            return false;
        }
        
        public string GetStats()
        {
            return $"{nameof(this.memoryLimit)} {this.memoryLimit}, {nameof(this.DEBUG_clearCounter)} {this.DEBUG_clearCounter}, {nameof(this.data.Count)} {this.data.Count}";
        }

        public override string ToString()
        {
            return GetStats();
        }

      /*  public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
           return this.data.Select(it => KeyValuePair.Create(it.Key, it.Value.value)).GetEnumerator();
        }*/

        public void Add(TKey key, TValue value)
        {
            if (!TryAdd(key, value, out _))
                throw new ArgumentException($"Cannot add entry with key {key}.");
        }

        public bool TryAdd(TKey key, TValue value, [MaybeNullWhen(true)] out TValue existing)
        {
            var node = new LinkedListNode<TKey>(key);
            if (!this.data.TryAdd(key, (node,value), out var existing_entry))
            {
                existing = existing_entry.value;
                this.history.Remove(existing_entry.historyNode);
                this.history.AddLast(existing_entry.historyNode);
                return false;
            }

            this.history.AddLast(node);

            if (this.data.Count > this.memoryLimit)
            {
                ++this.DEBUG_clearCounter;

                if (!this.data.TryRemove(this.history.First!.Value,out _))
                    throw new InvalidOperationException("Data was not removed from the cache.");
                this.history.RemoveFirst();
            }

            existing = default;
            return true;
        }

     
    }

}