using System.Linq;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Serialization;

namespace TrackPlanner.WebUI.Server
{
    public static class ConfigHelper
    {
        public static string InitializeConfigFile(string targetConfigFilename,
            params (string sectionName, object defaultConfig)[] configs)
        {
            var path = new Navigator().GetConfigPath(targetConfigFilename);

            if (!System.IO.File.Exists(path))
            {
                var config_dir = System.IO.Path.GetDirectoryName(path);
                System.IO.Directory.CreateDirectory(config_dir!);
                System.IO.File.WriteAllText(path, new ProxySerializer().Serialize(
                    configs.ToDictionary(it => it.sectionName,it => it.defaultConfig)));
            }

            return path;
        }
    }
}