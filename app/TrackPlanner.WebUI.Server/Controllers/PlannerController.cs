﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TrackPlanner.Backend;
using TrackPlanner.Backend.Stored;
using TrackPlanner.RestClient;
using TrackPlanner.Structures;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.RestSymbols;
using TrackPlanner.Shared.Stored;
using TrackPlanner.TestToolbox;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.WebUI.Server.Controllers
{
    [ApiController]
    [Route(Routes.Planner)]
    public sealed class PlannerController : ControllerBase
    {
        private readonly Navigator navigator;
        private readonly ILogger logger;
        private readonly IWorker<TNodeId, TRoadId> worker;
        public StorageManager<TNodeId, TRoadId> StorageManager { get; }

        internal PlannerController(ILogger? logger, IWorker<TNodeId, TRoadId>? worker,
            RestServiceConfig serviceConfig,
            Navigator navigator)
        {
            this.worker = worker ?? throw new ArgumentNullException(nameof(worker));
            this.navigator = navigator;
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));

            navigator.CreateSchedules();

            this.StorageManager = new StorageManager<TNodeId, TRoadId>(
                new FileStorageProvider(navigator.GetSchedules()),
                serviceConfig);
        }


        [HttpGet(Methods.Get_About)]
        public string About()
        {
            this.logger.Info("Received about request");
            return "Hello world";
        }

        [HttpPost(Methods.Post_SaveDebug)]
        public ActionResult SaveDebug([FromBody] string content)
        {
            Navigator.UniqueSaveText(this.navigator.GetDebugDirectory(), "ui-commands.code",
                new[] {content});
            return Ok();
        }

        [HttpPost(Methods.Post_SaveSchedule)]
        public Result<ValueTuple> SaveFullSchedule([FromBody] SaveRequest<TNodeId, TRoadId> saveRequest)
        {
            // todo: this retrieval is lame, it was done in a hurry just before starting travelling

            ContextMessage? message = null;

            List<IReadOnlyList<PlacedAttraction>>? stores = null;
            if (!DevelModes.True)
            {
                // todo: remove this functionality 2024-09-01
                // I didn't realize OsmAnd has much better functinality, so this was wasted effort

                stores ??= new List<IReadOnlyList<PlacedAttraction>>();
                try
                {
                    foreach (var (_, day_idx) in saveRequest.Schedule.Days.ZipIndex())
                    {
                        var req = saveRequest.Schedule.CreateInterestRequest(day_idx, PointOfInterest.Category.Utility);
                        stores.Add(this.worker.GetAttractions(req.CheckPoints, req.Range, req.ExcludeFeatures).SelectMany(it => it).ToList());
                    }
                }
                catch (Exception ex)
                {
                    ex.LogDetails(this.logger, MessageLevel.Error, "Store extraction failed");
                    message = message.Override(ContextMessage.Create(ex));
                    stores = null;
                }
            }

            List<IReadOnlyList<string>>? leg_cities = null;
            if (!DevelModes.True)
            {
                // todo: remove this functionality 2024-09-01
                // getting cities even for summary is not very useful, because we have to look at GPS-based map anyway
                // so we will see the cities and more
                leg_cities ??= new List<IReadOnlyList<string>>();
                try
                {
                    foreach (var (_, day_idx) in saveRequest.Schedule.Days.ZipIndex())
                    {
                        var day_bag = new HashSet<string>();

                        foreach (var (chk, chk_idx) in saveRequest.Schedule.GetSummary().Days[day_idx].Checkpoints.ZipIndex().Skip(1))
                        {
                            var cities = new List<string>();
                            var leg_idx = chk.IncomingLegIndex!.Value;
                            foreach (var pt in saveRequest.Schedule.Route.Legs[leg_idx].AllSteps().Select(it => it.Point))
                            {
                                var name = this.worker.GetClosestCityName(pt.Convert2d(),
                                    saveRequest.Schedule.Settings.RouterPreferences.GetRouteDirectionsSearchRange_RETIRED());
                                if (name == null
                                    || !day_bag.Add(name)
                                    || chk.Label == name
                                    || saveRequest.Schedule.GetSummary().Days[day_idx].Checkpoints[chk_idx - 1].Label == name)
                                    continue;

                                cities.Add(name);
                            }

                            leg_cities.Add(cities);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.LogDetails(this.logger, MessageLevel.Error, "Cities extraction failed");
                    message = message.Override(ContextMessage.Create(ex));
                    leg_cities = null;
                }
            }


            this.StorageManager.SaveAllSchedule(saveRequest.Schedule, stores, leg_cities, saveRequest.Path);

            return Result.ValidVoid(message);
        }

        [HttpPut(Methods.Put_ComputeTrack)]
        public Result<RouteResponse<TNodeId, TRoadId>> ComputeRoute([FromBody] PlanRequest<TNodeId> request)
        {
            if (!DevelModes.True)
            {
                if (DateTimeOffset.UtcNow < DateTimeOffset.Parse("2023-04-03T20:00:00Z"))
                    request.RouterPreferences = PrefsFactory.GetRouterPreferences();
                else
                    throw new Exception("REMOVE THIS from controller");
            }

            var points_info = String.Join("; ", request.DailyPoints.SelectMany(it => it));
            //this.logger.Info($"Received turner config : {request.TurnerPreferences}");

            try
            {
                Result<RouteResponse<WorldIdentifier, WorldIdentifier>> response
                    = this.worker.ComputeTrack(request, true, CancellationToken.None, out _);
                if (!response.HasValue)
                {
                    this.logger.Info($"Didn't find any route for {points_info}");
                }
                else
                {
                    this.logger.Log(response.ProblemMessage == null ? MessageLevel.Info : MessageLevel.Warning,
                        $"Route was found for {points_info}{(response.ProblemMessage == null ? "" : $" with problem: {response.ProblemMessage.TryFullMessage()}")}");
                }

                return response;
            }
            catch (Exception ex)
            {
                ex.LogDetails(this.logger, MessageLevel.Error,
                    $"Program crashed for {(String.Join("; ", request.DailyPoints.SelectMany(it => it)))}");
                return Result<RouteResponse<TNodeId, TRoadId>>.Fail(ContextMessage.Create(ex));
            }
        }

        [HttpGet(Methods.Get_LayersDirectory)]
        public ActionResult<DirectoryData> GetLayersDirectory([FromQuery(Name = Parameters.Directory)] string? directory = null)
        {
            var entries = GetDirectoryEntries(this.navigator.GetLayers(), directory, SystemCommons.LayerFileExtension);
            return entries;
        }

        [HttpGet(Methods.Get_ScheduleDirectory)]
        public ActionResult<DirectoryData> GetScheduleDirectory([FromQuery(Name = Parameters.Directory)] string? directory = null)
        {
            return GetDirectoryEntries(this.navigator.GetSchedules(), directory, SystemCommons.ProjectFileExtension);
        }

        private static DirectoryData GetDirectoryEntries(string parentDirectory, string? directory,
            string fileExtension)
        {
            var main_dir = System.IO.Path.Combine(parentDirectory,
                // for frontend it is root directory, but for us it is relative path
                (directory ?? "").TrimStart('/', '\\'));
            // get only relative portion
            var directories = System.IO.Directory.GetDirectories(main_dir)
                .Select(it => System.IO.Path.GetFileName(it)!)
                .OrderBy(it => it.ToLowerInvariant()).ToArray();
            var files = System.IO.Directory.GetFiles(main_dir, "*" + fileExtension)
                .Select(it => System.IO.Path.GetFileName(it)!)
                .OrderBy(it => it!.ToLowerInvariant()!).ToArray();
            return new DirectoryData() {Directories = directories, Files = files};
        }


        [HttpGet(Methods.Get_LoadSchedule)]
        public ActionResult<ScheduleJourney<TNodeId, TRoadId>> LoadSchedule([FromQuery(Name = Parameters.Path)] string path)
        {
            if (this.StorageManager.TryLoadSchedule(path, out var schedule))
                return schedule;
            else
                return NotFound();
        }

        [HttpGet(Methods.Get_GetLayer)]
        public ActionResult<AttractionsResponse> GetLayer([FromQuery(Name = Parameters.Path)] string path)
        {
            path = System.IO.Path.Combine(this.navigator.GetLayers(), path);
            return new AttractionsResponse()
            {
                Attractions = new List<List<PlacedAttraction>>() {loadLayer(path).ToList()},
            };
        }

        private static IEnumerable<PlacedAttraction> loadLayer(string path)
        {
            var kml_track = TrackReader.ReadUnstyled(path);

            foreach (var waypoint in kml_track.Waypoints)
            {
                var attr = new PointOfInterest(waypoint.Name, url: null, PointOfInterest.Feature.None);
                var place = new PlacedAttraction(new MapZPoint<OsmId, OsmId>(
                    waypoint.Point, nodeId: null
#if DEBUG
                    , DEBUG_mapRef: null
#endif
                ), attr);
                yield return place;
            }
        }

        [HttpPut(Methods.Put_ComputeAttractions)]
        public ActionResult<AttractionsResponse> GetAttractions([FromBody] InterestRequest request)
        {
            return new AttractionsResponse()
            {
                Attractions = this.worker.GetAttractions(request.CheckPoints, request.Range, request.ExcludeFeatures).ToList(),
            };
        }

        [HttpPut(Methods.Put_FindPeaks)]
        public ActionResult<Result<PeaksResponse<TNodeId, TRoadId>>> GetFindPeaks([FromBody] PeaksRequest request)
        {
            var response = this.worker.FindPeaks(request.FocusPoint, request.SearchRange,
                request.SeparationDistance,
                request.Count);

            return Result<PeaksResponse<WorldIdentifier, WorldIdentifier>>.Valid(response,
                response.Peaks.Length == 0 ? "No peaks were found." : null, MessageLevel.Info);
        }
    }
}