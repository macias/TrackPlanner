using System;
using System.IO;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using Xunit;

namespace TrackPlanner.Tests
{
    // the tests which do not use extracted mini map files, because the region is too
    // big. But each test should be able to save extracted region on demand

    public class BigMapTurnTest : MiniWorld
    {
        public BigMapTurnTest(ILogger? logger = null, bool manual = false) : base(logger,manual)
        {
        }

        protected override MiniTestSettings CreateTestSettings()
        {
            return new MiniTestSettings() {UseFullMap = true};
        }


        [Fact]
        public void ChoosingUknownFootwayTest()
        {
            // originally program avoided "obvious" shortest path because it would mean going via
            // footway with unknown surface, so it chose going longer route but partially known (asphalt).
            // Now when converting the map we upgrade some footways to urban sidewalks, and later on
            // we assume better surface for them. This allowed program to pick correctly the shortest path

            var map_filename = "choosing-uknown-footway.kml";

            var start = CreateRequestPoint(GeoPoint.FromDegrees(52.6590881, 19.0447788));
            var end = CreateRequestPoint(GeoPoint.FromDegrees(52.6572609, 19.0478363));


            // this test is about map conversion, so we use mini map only for debugging purpose
            if (!DevelModes.True)
            new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                boundary: Region.Build(
                    GeoPoint.FromDegrees(52.65943, 19.04418),
                    GeoPoint.FromDegrees(52.6599, 19.04713),
                    GeoPoint.FromDegrees(52.6579, 19.04931),
                    GeoPoint.FromDegrees(52.65664, 19.0471)
                ), extractionRange: null);


            {
                var settings = new MiniTestSettings() {UseFullMap = true};
                settings.FinderConfiguration ??= GetFinderPreferences();
                settings.FinderConfiguration.EnableDebugDumping = Manual;
                settings.FinderConfiguration.DumpProgress = this.Manual;
                settings.FinderConfiguration.AStarMode = false;

                var (plan, turns) = ComputeTurns(settings,
                    map_filename,
                    start,
                    end
                );

                if (Manual)
                    SaveData(plan, turns, map_filename);

                // in fist iteration we found the correct route using sidewalk...
                if (!DevelModes.True)
                    // sidewalk node
                    Assert.Contains(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 2955311817);

                // ... but then we added bucket expansion feature (to try connect disconnected areas)
                // and here this algorithm kick in, polishing much better sidewalk and using road instead


                Assert.Equal(OsmId.PureOsm(2955312842), plan[10].NodeId); // one of road nodes 


                // we don't test turns here on purpose because we don't have any method to just jump
                // to the road, and riding along sidewalk generates too many turns
            }
        }

        [Fact]
        public void TorunAvoidingHighTraffic_UNFINISHED_Test()
        {
// the weights are off, because program chose route right through high traffic road

            var map_filename = "torun-avoiding-high-traffic.kml";

            var start_west = CreateRequestPoint(GeoPoint.FromDegrees(53.0229683, 18.6894932));
            var end_east = CreateRequestPoint(GeoPoint.FromDegrees(53.0317421, 18.7597561));


            using (var map_stream = new MemoryStream())
            {
                var settings = new MiniTestSettings() {UseFullMap = true};
                settings.FinderConfiguration ??= GetFinderPreferences();
                settings.FinderConfiguration.EnableDebugDumping = Manual;
                settings.FinderConfiguration.DumpProgress = this.Manual;
                var (plan, turns) = ComputeTurns(settings,
                    map_filename,
                    start_west,
                    end_east
                );

                if (Manual)
                    SaveData(plan, turns, map_filename);

//                Assert.Equal(0,turns.Count);

                // some nodes from the express road, we shouldn't be there
                Assert.DoesNotContain(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 2480340676); // speed limit: 90km/h
                Assert.DoesNotContain(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 2483691939); // speed limit: 70km/h
                // the actual route is mostly via sand/unknown surface so I cannot force program (in sane way)
                // to choose the "correct" route
                if (!DevelModes.True)
                    Assert.DoesNotContain(plan.Where(it => it.IsNode()), it => it.NodeId !.Value.Identifier == 9092631517); // speed limit: 50km/h
            }
        }


        [Fact]
        public void TorunBydgoskaRoundaboutFalseExitTest()
        {
            // program tricked itself into assuming the roundabout has such a lengthy
            // exit segment that it removed almost entire route

            // it should be done in mini-map fashion, but it was hard to find out
            // what exactly triggers this problem (i.e. what has to be on map)
            // and I already burnt too much time on this case

            var map_filename = "torun-bydgoska-roundabout-false-exit.kml";

            var start_west = CreateRequestPoint(GeoPoint.FromDegrees(53.0118078, 18.5645971));
            var middle = CreateRequestPoint(GeoPoint.FromDegrees(53.0110787, 18.5660309)) with
            {
                EnforcePoint = true
            };
            var end_east = CreateRequestPoint(GeoPoint.FromDegrees(53.0082178, 18.597325500000004));


            using (var map_stream = new MemoryStream())
            {
                var settings = new MiniTestSettings() {UseFullMap = true};
                settings.FinderConfiguration ??= GetFinderPreferences();
                //    settings.FinderConfiguration.EnableDebugDumping = true;
                var (plan, turns) = ComputeTurns(settings,
                    map_filename,
                    start_west,
                    middle,
                    end_east
                );

//                SaveData(plan, turns, map_filename);

                Assert.Equal(0, turns.Count);
            }
        }

        [Fact]
        public void ChoppedRoundaboutTest()
        {
            // this test is about map itself, thus cannot extract mini-map in advance
            // the problem is some roundabouts are chopped into pieces, and turn-worker
            // cannot handle such cases

            // the success of this test -- no exceptions

            var map_filename = "chopped-roundabout.kml";
            var points = new[]
            {
                GeoPoint.FromDegrees(53.04105, 18.59862),
                GeoPoint.FromDegrees(53.040363, 18.597343)
            };
            using (var map_stream = new MemoryStream())
            {
                MiniExtractor.ExtractMiniMapFromPoints(Logger, map_filename, map_stream,
                    boundary: null,
                    strict: false, Length.FromMeters(60), points);
                var (plan, turns) = ComputeTurns(new MiniTestSettings() {MapStream = map_stream}, map_filename,
                    points);
            }
        }

        [Fact]
        public void SilnoAvoidingStraightRoadTest()
        {
            // program used some road around instead straight one
            // the root cause was problem with deserialization of configuration
            // despite client sent no-fast-forward mode, the server enabled using this mode

            var map_filename = "silno-avoiding-straight-road.kml";
            var start_north = GeoPoint.FromDegrees(52.96699142456054, 18.721389770507816);
            var end_south = GeoPoint.FromDegrees(52.93984603881836, 18.732891082763672);


            using (var map_stream = new MemoryStream())
            {
                MiniExtractor.ExtractMiniMapFromPoints(Logger, map_filename, map_stream,
                    boundary: Region.Build(
                        GeoPoint.FromDegrees(52.96763, 18.71709),
                        GeoPoint.FromDegrees(52.940311431884766, 18.750314712524414)
                    ),
                    strict: true, extractionRange: null);

                var (plan, turns) = ComputeTurns(new MiniTestSettings() {MapStream = map_stream}, map_filename,
                    start_north,
                    end_south
                );

                //SaveData(plan, turns, map_filename);

                // one of the nodes on the straight road, this is the essense of this test
                Assert.Contains(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 534618533);


                Assert.Equal(1, turns.Count);

                Index index = 0;

                //checking only the last turn notification
                Assert.Equal(3610427916, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.False(turns[index].Backward);
                AssertTrackIndex(75, turns, index);
            }
        }
    }
}