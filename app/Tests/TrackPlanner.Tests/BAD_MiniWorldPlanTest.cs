using System.Linq;
using Geo;
using TrackPlanner.Shared;
using TrackPlanner.TestToolbox;
using Xunit;

namespace TrackPlanner.Tests
{
    public class BAD_MiniWorldPlanTest : MiniWorld
    {
        public BAD_MiniWorldPlanTest(ILogger? logger = null) : base(logger,manual:false)
        {
            
        }
        
        [Fact]
        public void CierpiceCrossingRoadTest()
        {
            // there is an error in OSM data, it does not have common node on one lane at the road intersection
            // we should fix the map on the fly

            // in "buggy" version program relies on OSM data, goes as the data dictates, finds the first common node
            // and goes back on the target lane

            var map_filename = "cierpice-crossing_road.kml";

            var plan = ComputeRoute(map_filename,
                GeoPoint.FromDegrees(52.983727, 18.485634),
                GeoPoint.FromDegrees(52.987045, 18.49471)
            );

            // SaveData(plan,map_filename);

            if (!TrueVariant)
            {
                Assert.DoesNotContain(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 4332109258);
        }
    }

    }
}