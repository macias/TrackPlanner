using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using Xunit;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Tests
{
    // tests based on legacy data come from times when the track plan came from the external planner/router and turner
    // had to add turn points, in other words track was given "in advance" 
    public class BAD_MiniWorldTurnTest : MiniWorld
    {
        public BAD_MiniWorldTurnTest(ILogger? logger = null,bool manual = false) : base(logger,manual)
        {
        }


              [Fact]
        public void TODO_SilnoCyclewayBumpTest()
        {
            //COMP_MODE.WeightedTurns = true;
            var map_filename = "legacy/silno_cycleway_bump.kml";
            var settings = CreateDebugSettings();
            //settings.RouterPreferences.SnapCyclewaysToRoads = Length.Zero;
           settings.FinderConfiguration!.DumpProgress = this.Manual;
            var (plan, turns) = ComputeTurns(settings, map_filename,
                GeoPoint.FromDegrees(52.9426, 18.73246),
                GeoPoint.FromDegrees(52.93953, 18.73554)
            );

            if (this.Manual)
                SaveData(plan,turns,map_filename);

            // turner depends on routing so to avoid constant changes due to router
            // we added conditional checks

            if (COMP_MODE.UniTestWeightedTurns)
            {
                // this is wrong, we should have at least one turn. Currently, i.e. after introducing
                // penalty for turns, when straightening cycleway program chooses secondary road all along.
                // Currently we don't see strategy how to hop off that road
                // (and not make regressions in all tests)
                
                // why did it happen? Originally (without turn weighting) program chose going at start via road and then
                // TURN to the cycleway and continue on it.
                // Then program tried to "polish" cycleway but the starting point on cycleway was too far away, so it
                // didn't work
                
                // With turn weighting the penalty caused program to chose cycleway all the way. Then when polishing
                // the starting point of cycleway was much closer and program was able to snap entirely to the road
                // and thus we have no turn
                
                // So basically from the beginning program tried to do the "wrong" thing, but it didn't manage, now after
                // correction we see the outcome
                
                Assert.Equal(0, turns.Count);
            }
            else
            {
                int index = 0;
                int second_turn_track_index;
                // if the route starts with road part then we have two turns
                if (plan.Any(it => it.IsNode() && it.NodeId!.Value.Identifier == 3610427916))
                {
                    Assert.Equal(2, turns.Count);

                    second_turn_track_index = 10;

                    Assert.Equal(3610427916, turns[index].NodeId!.Value.Identifier);
                    Assert.True(turns[index].Forward);
                    Assert.False(turns[index].Backward);
                    AssertTrackIndex(5 + 1, turns, index);

                    ++index;
                }
                else // if it starts from parallel cycleway we will have only one turn
                {
                    Assert.Equal(1, turns.Count);

                    second_turn_track_index = 8;
                }

                // this turn is common in both cases

                Assert.Equal(6384120377, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(second_turn_track_index + 1, turns, index);
            }
        }


        [Fact]
        public void WeirdAxeTurnTest()
        {
            // program created weird axe/star-like turn for two reasons:
            // 1) it is an effect to snapping/aligning cycleway to regular road
            // 2) the weight of the roads are off because of "joining the high traffic" weight
            // instead of directly joining opposite lane of the road with high traffic (because of penalty)
            // program chose to go along with one lane, and on the joint of the lanes make a 180 turn
            // avoiding the penalty
            // -----+---\
            //      |    >----
            // -----+---/
            // horizontal lanes are one-ways, the route goes from the left, instead of going through
            // veritcal segment, program continues to the right

            // if we store the history it is easy to detect and add penalty, but weighting with history
            // prevents adding weights. I.e. weight(A)+weight(B)+weight(C) no longer could have be equal
            // to weight(A+B)+weight(C).

            var map_filename = "weird-axe-turn.kml";

            var start = CreateRequestPoint(GeoPoint.FromDegrees(53.0134811, 18.8184433));
            var end = CreateRequestPoint(GeoPoint.FromDegrees(53.0055389, 18.8431835));

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromPointsToFile(map_filename,
                    Length.FromMeters(20), strict: true,
                    start.UserPointFlat,
                    GeoPoint.FromDegrees(53.01434, 18.81931),
                    GeoPoint.FromDegrees(53.00734, 18.83648),
                    GeoPoint.FromDegrees(53.00708, 18.83666),
                    GeoPoint.FromDegrees(53.00748, 18.83689),
                    end.UserPointFlat,
                    GeoPoint.FromDegrees(53.00666, 18.83728)
                );

            var settings = new MiniTestSettings() {UseFullMap = false};
            settings.FinderConfiguration ??= GetFinderPreferences();
            settings.FinderConfiguration.EnableDebugDumping = Manual;
            settings.FinderConfiguration.DumpProgress = this.Manual;
            var (plan, turns) = ComputeTurns(settings,
                map_filename,
                start,
                end
            );

            if (Manual)
                SaveData(plan, turns, map_filename);

        }
        
          //[Fact]
        public void TODO_BydgoszczGlinkiRoundaboutTest()
        {
            // OSM data has changed and the roundabout which we focused on
            // now is split into few parts. We consider such entities as big
            // roundabouts and do not apply shortcuts there. As the effect
            // instead of 1 turn notification we get multiple. 
            
            // Until we start handling any roundabout properly this test has no use.
            // The aim is of course to keep notification low, but new logic is needed
            // and it won't be written overnight. So for now -- frozen case.
            
            // The one piece mini map should be still store for comparison.
            
            // program reported it cannot compute turns because of some null error

            var map_filename = "bydgoszcz-glinki-roundabout.kml";
            var start_west_long = GeoPoint.FromDegrees(53.1060125, 18.0299461);
            var start_west_short = GeoPoint.FromDegrees(53.10452949999999, 18.0270235);
            var end_east_short = GeoPoint.FromDegrees(53.1046609, 18.026267299999997);
            var end_east_long = GeoPoint.FromDegrees(53.10483, 18.02579);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_west_long,
                    GeoPoint.FromDegrees(53.10439, 18.02673),
                    end_east_long
                );

            var prefs = PrefsFactory.GetRouterPreferences();
            prefs.Speeds[RoadSpeeding.UnknownStructured] = prefs.Speeds[RoadSpeeding.Sand];

            var settings = new MiniTestSettings() {RouterPreferences = prefs};
            
            long_plan();
            short_plan();
            mid_plan();

            void short_plan()
            {
                var (plan, turns) = ComputeTurns(settings!, map_filename!,
                    start_west_short,
                    end_east_short
                );

                //SaveData(plan,turns,map_filename);

                // there are no turns here because the entire route is too short, and the crosspoints
                // are too close to the turn, but if we count them in, we have turn

                if (TrueVariant)
                {
                    Assert.Equal(1, turns.Count);

                    int index = 0;

                    Assert.Equal(4880611741, turns[index].NodeId!.Value.Identifier);
                    Assert.True(turns[index].Forward);
                    Assert.True(turns[index].Backward);
                    Assert.Equal(1, turns[index].TrackIndex);
                    ++index;
                }
                else
                    Assert.Equal(0, turns.Count);
            }

            void mid_plan()
            {
                var (plan, turns) = ComputeTurns( //CreateDebugSettings(),
                    map_filename!,
                    start_west_long,
                    end_east_short
                );

                //  SaveData(plan,turns,map_filename);

                // still one user point is too close to turn, BUT current planning algorithm does not
                // handle roundabout too well, so here we have ugly turn

                // we should improve planning algorithm and get rid of first turn notification
                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(496353198, turns[index].RoadId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                Assert.Equal(13, turns[index].TrackIndex);

                // we are checking if program didn't make roundabout link, here it should not do it
                // west arm from the roundabout 
                Assert.Contains(1431352030, plan.Where(it => it.IsNode()).Select(it => it.NodeId!.Value.Identifier));
                // east arm
                Assert.Contains(4880611728, plan.Where(it => it.IsNode()).Select(it => it.NodeId!.Value.Identifier));
            }

            void long_plan()
            {
                // same as above but now we have more space for the route and turn

                var (plan, turns) = ComputeTurns(new MiniTestSettings(),
                    map_filename!,
                    start_west_long,
                    end_east_long
                );

                if (this.Manual)
                 SaveData(plan,turns,map_filename!);

                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(496353198, turns[index].RoadId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                Assert.Equal(13, turns[index].TrackIndex);
                ++index;

                // we are checking if program didn't make roundabout link
                // west arm from the roundabout -- just like mid version, no link 
                Assert.Contains(1431352030, plan.Where(it => it.IsNode()).Select(it => it.NodeId!.Value.Identifier));
                // east arm, but here our user point is more distance, so we are able compute 
                // link, thus we don't have any node on roundabout
                Assert.DoesNotContain(4880611741, plan.Where(it => it.IsNode()).Select(it => it.NodeId!.Value.Identifier));
                Assert.DoesNotContain(5595138206, plan.Where(it => it.IsNode()).Select(it => it.NodeId!.Value.Identifier));
                Assert.DoesNotContain(4880611728, plan.Where(it => it.IsNode()).Select(it => it.NodeId!.Value.Identifier));
            }
        }

      
       
        [Fact]
        public void A_FIX_RETHINK_KaszczorekRoundaboutCyclewayShortcutTest()
        {
            if (DevelModes.True)
            return;
            
            // we should avoid roundabout and go in straight line
            var map_filename = "legacy/kaszczorek_roundabout_cycleway_shortcut.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.01299, 18.68607),
                GeoPoint.FromDegrees(53.01034, 18.69025)
            );
            //SaveData(plan, turns, map_filename);

            if (!TrueVariant)
            {
                Assert.Equal(2, turns.Count);

                int index = 0;

                Assert.Equal(53.011958400000005, turns[index].Point.Latitude.Degrees, Precision);
                Assert.Equal(18.687863100000001, turns[index].Point.Longitude.Degrees, Precision);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                Assert.Equal(7, turns[index].TrackIndex);

                ++index;
                // this point PROBABLY can be moved a bit towards main road
                Assert.Equal(53.011309300000001, turns[index].Point.Latitude.Degrees, Precision);
                Assert.Equal(18.688785800000002, turns[index].Point.Longitude.Degrees, Precision);
                Assert.False(turns[index].Forward);
                Assert.True(turns[index].Backward);
                Assert.Equal(14, turns[index].TrackIndex);
            }
        }

     
    }
}