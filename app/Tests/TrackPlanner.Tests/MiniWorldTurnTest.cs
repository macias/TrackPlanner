using System;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using Xunit;

namespace TrackPlanner.Tests
{
    // tests based on legacy data come from times when the track plan came from the external planner/router and turner
    // had to add turn points, in other words track was given "in advance" 
    public class MiniWorldTurnTest : MiniWorld
    {
        // if we include crosspoints in computing turns, all turn indices are shifted by one 
        const int CrosspointOffset = 1;

        public MiniWorldTurnTest(ILogger? logger = null, bool manual = false) : base(logger,manual)
        {
        }

         [Fact]
        public void WalyczEmptySequenceTest()
        {
            // todo: we are using (on purpose) minimap which has two "islands"
            // of roads, once we hit both, we have "route not found". We should
            // find way to connect them anyway (i.e. points, not by "fixing" the minimap)
            
            // program reported "Sequence contains no elements"

            var map_filename = "walycz-empty-sequence.kml";
            var start = GeoPoint.FromDegrees(53.2625978, 18.9860908);
            var mid1 = GeoPoint.FromDegrees(53.26208622983437, 18.985892115975506);
            var mid2 = GeoPoint.FromDegrees(53.2630307827455, 18.98250423338076);


            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(GeoPoint.FromDegrees(53.26744, 18.98003),
                        GeoPoint.FromDegrees(53.25755, 18.99153))
                );

            var (plan, turns) = ComputeTurns(map_filename!,
                start,
                mid1,
                mid2,
                start
            );

            if (this.Manual)
              SaveData(plan,turns,map_filename);

            Assert.Equal(4, turns.Count);

            int index = 0;

            Assert.Equal(3398083213, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(3, turns, index);
            ++index;

            Assert.Equal(2282175275, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(13, turns, index);
            ++index;

            Assert.Equal(2282175275, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(39, turns, index);
            ++index;

            Assert.Equal(3398083213, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(49, turns, index);
            ++index;
        }

        [Fact]
        public void SplittingLongDraftTest()
        {
            // program crashed as the effect of splitting long draft leg

            var map_filename = "splitting-long-draft.kml";
            // forbidden zone
            var start = GeoPoint.FromDegrees(52.9243431, 18.559166);
            // middle of the lake
            var end = GeoPoint.FromDegrees(52.7903824, 17.7453632);

            var settings = new MiniTestSettings()with {UseFullMap = true};
            settings.FinderConfiguration = GetFinderPreferences();
            settings.ExpectedProblem = RouteFinding.RouteNotFound.ToString();
            var (route, plan, turns) = ComputeTurns(settings,
                map_filename,
                start,
                end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(1, route.Legs.Count);
            Assert.Equal(1, route.Legs[0].Fragments.Count);
            Assert.True(route.Legs[0].IsDrafted);
            Assert.Equal(start.Convert3d(),route.Legs[0].Fragments[0].Steps[0].Point);
            Assert.Equal(end.Convert3d(),route.Legs[0].Fragments[0].Steps[1].Point);
        }

        [Fact]
        public void HandlingForbiddenTest()
        {
            // program kept together segments igoring the fact one segment
            // does not have public access, visually it gave red color indicating
            // the entire line does not have access

            var map_filename = "handling-forbidden.kml";
            var start_north = GeoPoint.FromDegrees(52.860527, 18.5770226);
            var end_south = GeoPoint.FromDegrees(52.8551903, 18.5740833);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename,
                    strict: true,
                    boundary: Calculator.GetBoundary(Length.FromMeters(20),
                        start_north,
                        GeoPoint.FromDegrees(52.85458, 18.5745),
                        end_south));

            var settings = new MiniTestSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.CompactPreservesRoads = false;
            var (route, plan, turns) = ComputeTurns(settings, map_filename,
                start_north,
                end_south
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            // this is the reason for this test, to check if segments
            // are kept together despite one is forbidden and the other is not

            Assert.Equal(1, route.Legs.Count);
            Assert.Equal(2, route.Legs[0].Fragments.Count);
            Assert.True(route.Legs[0].Fragments[0].IsForbidden);
            Assert.False(route.Legs[0].Fragments[1].IsForbidden);
            // this section is not important 
            Assert.Equal(1, turns.Count);

            var idx = 0;

            Assert.Equal(1376259419, turns[idx].NodeId!.Value.Identifier);
            Assert.True(turns[idx].Forward);
            Assert.True(turns[idx].Backward);
            AssertTrackIndex(10, turns, idx);
        }


        [Fact]
        public void ResettingInitialLegRoadTest()
        {
            // program crashed because when smoothing joint point it removed the first place from the second
            // leg and didn't reset road for now-first place of the second leg. So program reported it has leg
            // with single place (the initial one)
            
            var map_filename = "resetting-initial-leg-road.kml";

            var start = GeoPoint.FromDegrees(53.1617978,18.8762357);
            var middle = GeoPoint.FromDegrees(   53.1549873,18.8734699);
            var end = GeoPoint.FromDegrees(    53.1554547,18.8712571);
            // something wrong with minimap, even with range of 5km
            // the test case was not recreated properly
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary:null, Length.FromMeters(5000),
                        start,
                    middle,
                        end);

            var settings = new MiniTestSettings
            {
                FinderConfiguration = GetFinderPreferences(),
                UseFullMap = true,
            };
            settings.FinderConfiguration.CompactPreservesRoads = false;
            var (route, plan, turns) = ComputeTurns(settings,
                map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan,turns, map_filename);

            // nothing special, just any node from the route
            Assert.Equal(OsmId.PureOsm(2266136874),plan[1].NodeId);
            
            Assert.Equal(0,turns.Count);
        }

        [Fact]
        public void SmoothingOnSharedCrossNodeTest()
        {
            // program crashed because when smoothing crosspoints, we had really cross-node, and when computing
            // which one is shared between next/previous leg we picked the wrong node
            
            var map_filename = "smoothing-on-shared-cross-node.kml";

            var start = GeoPoint.FromDegrees(53.2918472, 18.6372757);
            var middle = GeoPoint.FromDegrees(   53.2925215,18.642911);
            var end = GeoPoint.FromDegrees(  53.2908589,18.6434277);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: Calculator.GetBoundary(Length.FromMeters(100),
                        start,
                        middle,
                        end));

            var (route, plan, turns) = ComputeTurns(map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan,turns, map_filename);

            // the problematic cross-node
            Assert.Equal(OsmId.PureOsm(2701870421),plan[3].NodeId);
            
            Assert.Equal(1,turns.Count);

            int index = 0;
            
            Assert.Equal(OsmId.PureOsm( 587577009), turns[index].NodeId);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(8, turns, index);
            
        }

        [Fact]
        public void SettingInitialRoadDataTest()
        { 
            // program reported that adjacent nodes are not adjacent. This was the result of incorrect
            // crosspoint smoothing -- road data was not refreshed and adjacent nodes were checked
            // via irrelevant (after smoothing) roads
            
            var map_filename = "setting-initial-road-data.kml";

            var start = GeoPoint.FromDegrees(53.1548538,18.7153374);
            var middle = GeoPoint.FromDegrees(53.1552708,18.714615);
            var end = GeoPoint.FromDegrees(53.156498, 18.7131081);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: Calculator.GetBoundary(Length.FromMeters(200),
                        start,
                        middle,
                        end));

            var (route, plan, turns) = ComputeTurns(
                map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan,turns, map_filename);

            // the node which was not reachable
            Assert.Equal(OsmId.PureOsm(2375059497),plan[3].NodeId);
            
            Assert.Equal(0,turns.Count);
        }

        [Fact]
        public void AdjacentPlaceNotFoundTest()
        {
            // program crashed with message it cannot find adjacent place to given node.
            // This was the result on incorrect smoothing of middle crosspoint
            // (it was too blindly removed)

            var map_filename = "adjacent-place-not-found.kml";

            var start = GeoPoint.FromDegrees(53.1405148, 18.7086713);
            var middle = GeoPoint.FromDegrees(53.13998840000001, 18.7086641);
            var end = GeoPoint.FromDegrees(53.1383133, 18.7155113);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: Calculator.GetBoundary(Length.FromMeters(20),
                        start,
                        middle,
                        end));

            var (route, plan, turns) = ComputeTurns(
                map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan,turns, map_filename);

            // originally this node was incorrectly removed 
            Assert.Equal(OsmId.PureOsm(1548542256),plan[1].NodeId);
            
            Assert.Equal(1,turns.Count);
            
            int index = 0;
            
            Assert.Equal(4165604813, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(4, turns, index);
        }


        [Fact]
        public void DraftPlaceInsteadNodeTest()
        {
            // program crashed with message there is some draft place used when real node was expected.

            // The "sides" of the points create disconnected islands so draft leg has to be used
            // as an emergency connection

            var map_filename = "draft-place-instead-node.kml";

            var start = GeoPoint.FromDegrees(53.5361653, 18.899769);
            var middle = GeoPoint.FromDegrees(53.5374823, 18.912025000000003);
            var end = GeoPoint.FromDegrees(53.5285782, 18.908615);
            // the needed range for this test creates too big mini map, so we use
            // real one
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: Calculator.GetBoundary(Length.FromKilometers(1),
                        start,
                        middle,
                        end));

            var settings = new MiniTestSettings() with {UseFullMap = true};
            settings.FinderConfiguration = GetFinderPreferences();
            settings.ExpectedProblem = RouteFinding.RouteNotFound.ToString();
            var (route, plan, turns) = ComputeTurns(settings,
                map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                // saving other data won't work because they are turn-oriented, and we don't have any because
                // both legs are drafted only
                SaveRoute(route, map_filename);

            Assert.Equal(2, route.Legs.Count);
            Assert.DoesNotContain(false, route.Legs.Select(it => it.IsDrafted));
            Assert.Equal(start.Convert3d(), route.Legs[0].Fragments[0].Steps[0].Point);
            Assert.Equal(middle.Convert3d(), route.Legs[0].Fragments[0].Steps[1].Point);
            Assert.Equal(middle.Convert3d(), route.Legs[1].Fragments[0].Steps[0].Point);
            Assert.Equal(end.Convert3d(), route.Legs[1].Fragments[0].Steps[1].Point);

            ;
        }

        [Fact]
        public void KaszczorekBridgeMinorPassTest()
        {
            var map_filename = "kaszczorek-bridge-minor-pass.kml";
            var start_north = GeoPoint.FromDegrees(53.0023, 18.69977);
            var end_south = GeoPoint.FromDegrees(53.00024, 18.7026);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: null, Length.FromMeters(50),
                    GeoPoint.FromDegrees(53.00269, 18.6996),
                    end_south);

            var prefs = PrefsFactory.GetRouterPreferences();
            prefs.Speeds[RoadSpeeding.UnknownStructured] = prefs.Speeds[RoadSpeeding.Sand];

            var settings = new MiniTestSettings() {RouterPreferences = prefs};

            var (plan, turns) = ComputeTurns(settings, map_filename,
                start_north,
                end_south
            );

            // SaveData(plan,turns,map_filename);

            const int turns_count = 2;
            Assert.Equal(turns_count, turns.Count);

            int index = 0;

            if (turns_count == 2)
            {
                // program once is written this way this turn is reported, other time it is not
                // the question is -- should we report turns which are super close to the start/end
                // of the trip? With answer "no", the rationale is user has to turn on
                // GPS, check the map so she/he is aware of the start, at the end turns which changes
                // 20 meters do not matter than much. And we avoid problem with notification because
                // start/end is 1.5 cm from the start, yet we alarm the user
                // With answer "yes", we have take no chances

                Assert.Equal(2577250346, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(2 - CrosspointOffset, turns, index);

                ++index;
            }

            Assert.Equal(6194943077, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(3 + CrosspointOffset, turns, index);

            // if going backwards there is road split (into cycleway)
            // but the program ignores splits to cycleway on purpose
            // so there is no notification
        }

        [Fact]
        public void PerkowoTest()
        {
            var map_filename = "perkowo.kml";
            var start_north = GeoPoint.FromDegrees(52.90463, 18.47046);
            var end_south = GeoPoint.FromDegrees(52.90219, 18.47163);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: null, Length.FromMeters(50),
                    start_north,
                    GeoPoint.FromDegrees(52.90429, 18.47206),
                    end_south);
            var (plan, turns) = ComputeTurns(map_filename,
                start_north,
                end_south
            );

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void SuchatowkaTurnRailwayTest()
        {
            var map_filename = "suchatowka-turn-railway.kml";
            var start = GeoPoint.FromDegrees(52.91112, 18.47965);
            var end = GeoPoint.FromDegrees(52.90999, 18.47969);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: null, Length.FromMeters(50),
                    GeoPoint.FromDegrees(52.91131, 18.47921),
                    GeoPoint.FromDegrees(52.91058, 18.48212),
                    GeoPoint.FromDegrees(52.90989, 18.47916));
            var (plan, turns) = ComputeTurns(map_filename,
                start,
                end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            var idx = 0;

            Assert.Equal(4630332360, turns[idx].NodeId!.Value.Identifier);
            Assert.True(turns[idx].Forward);
            Assert.True(turns[idx].Backward);
            AssertTrackIndex(10, turns, idx);
        }

        [Fact]
        public void TorunSouthRangeTest()
        {
            var map_filename = "torun-south_range.kml";
            var start = GeoPoint.FromDegrees(52.96484, 18.53726);
            var middle = GeoPoint.FromDegrees(52.9352, 18.51589);
            var end = GeoPoint.FromDegrees(52.87777, 18.63722);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromSourceToFile(map_filename,
                    extractionRange: Length.FromMeters(100));

            var (plan, turns) = ComputeTurns(map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            Assert.Equal(281249332, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.True(turns[0].Backward);
            AssertTrackIndex(46 + CrosspointOffset, turns, 0);
        }
        
        [Fact]
        public void MisleadingLTurnTest()
        {
            // T-junction, with L part being tertiary road, and the straight extension of tertiary road
            // is residential. Without notification is very easy to go straight, instead of turning

            var map_filename = "misleading-L-turn.kml";

            var start_west = GeoPoint.FromDegrees(53.20926, 18.54835);
            var end_north = GeoPoint.FromDegrees(53.20993, 18.55113);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromPointsToFile(map_filename,
                    Length.FromMeters(20), strict: true,
                    start_west,
                    GeoPoint.FromDegrees(53.20871, 18.55089), // turn point
                    end_north
                );


            var (plan, turns) = ComputeTurns(map_filename,
                start_west,
                end_north
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            int index = 0;


            Assert.Equal(587567882, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            // we could skip this notification, because when going back there is no straight-ahead alternate road
            // so it is harder to make wrong turn 
            Assert.True(turns[index].Backward);
            AssertTrackIndex(4, turns, index);

            ++index;
        }

        

        [Fact]
        public void TorunSkarpaIgnoringCyclewayTest()
        {
            var map_filename = "torun_skarpa-ignoring-cycleway.kml";
            var start_north = GeoPoint.FromDegrees(53.02259, 18.66845);
            var end_end = GeoPoint.FromDegrees(53.01858, 18.67595);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true, boundary: null,
                    Length.FromMeters(25),
                    GeoPoint.FromDegrees(53.02334, 18.66802),
                    start_north,
                    GeoPoint.FromDegrees(53.02022, 18.67116),
                    GeoPoint.FromDegrees(53.01905, 18.67321),
                    GeoPoint.FromDegrees(53.01874, 18.67298),
                    end_end,
                    GeoPoint.FromDegrees(53.01855, 18.67634)
                );

            var (plan, turns) = ComputeTurns(map_filename,
                start_north,
                end_end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(3, turns.Count);

            var idx = 0;

            // it would be good to remove somehow those turns, because they are really 90 degrees turn, but the curve
            // is so gentle that it triggers notification
            Assert.Equal(10141526318, turns[idx].NodeId!.Value.Identifier);
            Assert.True(turns[idx].Forward);
            Assert.False(turns[idx].Backward);
            AssertTrackIndex(1, turns, idx);

            ++idx;

            Assert.Equal(10141526322, turns[idx].NodeId!.Value.Identifier);
            Assert.False(turns[idx].Forward);
            Assert.True(turns[idx].Backward);
            AssertTrackIndex(2, turns, idx);

            ++idx;

            // this entire turn-notification is because we snapped path to one-direction road (partially)
            // so when we go along no problem, but when we go back
            // turn calculator see we go against current thus it gives us notification


            Assert.Equal(271382569, turns[idx].NodeId!.Value.Identifier);
            Assert.False(turns[idx].Forward);
            Assert.True(turns[idx].Backward);
            AssertTrackIndex(6, turns, idx);

            ++idx;
        }

        [Fact]
        public void KaszczorekBridgeMinorPassExitTest()
        {
            var map_filename = "kaszczorek-bridge-minor-pass-exit.kml";

            var start_north = GeoPoint.FromDegrees(52.99863, 18.70227);
            var end_north = GeoPoint.FromDegrees(52.99735, 18.70238);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(
                        GeoPoint.FromDegrees(52.99891, 18.70041),
                        GeoPoint.FromDegrees(52.99882, 18.70275),
                        GeoPoint.FromDegrees(52.997, 18.70323),
                        GeoPoint.FromDegrees(52.99782, 18.70021)
                    )
                );

            var (plan, turns) = ComputeTurns(map_filename,
                start_north,
                end_north
            );

            //SaveData(plan,turns,map_filename);

            Assert.Equal(1, turns.Count);

            int index = 0;

            Assert.Equal(773474234, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(5, turns, index);
        }


        [Fact]
        public void GrabowiecFlatRunTest()
        {
            // this test is about using road (as for cars) instead of parallel cycleway to avoid excessive turn notifications
            // one can think of few methods to handle this:
            // 1) when routing add penalty for road switching (original approach, but it means complex routing)
            // 2) preprocessing the map and disabling parallel cycleways 
            // 3) preprocessing the map and in routing postprocessing move cycleway fragments onto the roads (current one)

            var map_filename = "grabowiec-flat-run.kml";
            var start = GeoPoint.FromDegrees(52.99471, 18.7021);
            var end = GeoPoint.FromDegrees(52.95359, 18.72525);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: null, Length.FromMeters(25),
                    start,
                    GeoPoint.FromDegrees(52.97517, 18.71322),
                    GeoPoint.FromDegrees(52.96906, 18.72061),
                    GeoPoint.FromDegrees(52.963, 18.7223), // we need to extract roads in the middle 
                    end);

            var (plan, turns) = ComputeTurns(map_filename,
                start,
                end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void ChelmnoRoundaboutLTurnTest()
        {
            // O>----
            // O roundabout
            // > split to entry+exit road
            // - regular road
            var map_filename = "chelmno-roundabout_Lturn.kml";

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(
                        GeoPoint.FromDegrees(53.33068, 18.41967),
                        GeoPoint.FromDegrees(53.32759, 18.42524),
                        GeoPoint.FromDegrees(53.31736, 18.42234),
                        GeoPoint.FromDegrees(53.32617, 18.40972)
                    )
                );

            var (plan, turns) = ComputeTurns(
                map_filename,
                GeoPoint.FromDegrees(53.32023, 18.42174),
                GeoPoint.FromDegrees(53.32692, 18.4115)
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            int index = 0;

            Assert.Equal(TurnInfo.EntityReference.Roundabout, turns[index].Entity);
            Assert.Equal(OsmId.PureOsm( 235135545), turns[index].RoadId);
            Assert.Equal(0, turns[index].RoundaboutCounter);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(8 + CrosspointOffset, turns, index);
        }


        [Fact]
        public void SwiecieNegativeCountTest()
        {
            // program reported negative count when replacing roundabout with its center
            // it was caused by starting right at roundabout so we have only single point
            // at roundabout while program required at least two points

            var map_filename = "swiecie-negative-count.kml";

            var start_east = GeoPoint.FromDegrees(53.41802474885676, 18.451470882812227);
            var end_west = GeoPoint.FromDegrees(53.417179107666016, 18.44617462158203);


            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: Region.Build(
                        GeoPoint.FromDegrees(53.41928, 18.44314),
                        GeoPoint.FromDegrees(53.41539, 18.4546)));


            var (plan, turns) = ComputeTurns(map_filename,
                start_east,
                end_west
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(2, turns.Count);

            int index = 0;

            Assert.Equal(2324589904, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(7, turns, index);

            ++index;

            Assert.Equal(2456260276, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(17, turns, index);
        }

        [Fact]
        public void GrudziadzSzosaTorunskaRoundaboutTest()
        {
            
            // program was unable to compute roundabout exits, because the user points are within
            // roundabout region, so now we are looking further than just a user route

            const string map_filename = "grudziadz_szosa_torunska_roundabout.kml";
            var start_south_close = GeoPoint.FromDegrees(53.44586181640626, 18.728363037109375);
            var start_south_far = GeoPoint.FromDegrees(53.44466, 18.72807);
            var end_north_close = GeoPoint.FromDegrees(53.44756698608399, 18.728534698486328);
            var end_north_far = GeoPoint.FromDegrees(53.44817, 18.72903);
            var roundabout = GeoPoint.FromDegrees(53.44681, 18.72811); // middle of roundabout
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_south_far,
                    roundabout,
                    GeoPoint.FromDegrees(53.44676, 18.72727), // roundabout east exit
                    roundabout,
                    end_north_far);

            close_without_links();
            far_with_links();

            void close_without_links()
            {
                var (plan, turns) = ComputeTurns(map_filename,
                    start_south_close,
                    end_north_close
                );

                if (this.Manual)
                  SaveData(plan, turns, map_filename);

                if (COMP_MODE.UniTestWeightedTurns)
                {
                    Assert.Equal(0,turns.Count);
                }
else
                {
                    Assert.Equal(1,turns.Count);
                
                    // since we are unable to reach links, the entry/exit nodes create a curved line
                    // and thus the turn
                    int index = 0;
                    Assert.Equal(OsmId.PureOsm(181065424), turns[index].RoadId);
                    Assert.True(turns[index].Forward);
                    Assert.True(turns[index].Backward);
                    AssertTrackIndex(7, turns, index);
                }
            }

            void far_with_links()
            {
                var (plan, turns) = ComputeTurns(map_filename,
                    start_south_far,
                    end_north_far
                );

                if (this.Manual)
                    SaveData(plan, turns, map_filename);

                Assert.Equal(0, turns.Count);
            }
        }

        [Fact]
        public void BrzozowoRoundaboutExitsTest()
        {
            // originally program had problems with multiple exits from roundabout
            // because it counted points for entire route, so if we went back and forth
            // through given roundabout everything was double counted

            var map_filename = "brzozowo-roundabout.kml";
            var start = GeoPoint.FromDegrees(53.330745697021484, 18.419523239135742);
            var end = GeoPoint.FromDegrees(53.32502746582031, 18.420724868774414);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(
                        GeoPoint.FromDegrees(53.33326, 18.41906),
                        GeoPoint.FromDegrees(53.32835, 18.41759),
                        GeoPoint.FromDegrees(53.3288, 18.42283),
                        GeoPoint.FromDegrees(53.32199, 18.42147)
                    )
                );

            var (plan, turns) = ComputeTurns(map_filename,
                // going twice through the roundabout
                start, end, start
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void TorunGagarinaCreatingConnectionTest()
        {
            // program reported it cannot create ending connection

            var map_filename = "torun-gagarina-creating-connection.kml";
            var start_north = GeoPoint.FromDegrees(53.01953, 18.57843);
            var end_south = GeoPoint.FromDegrees(53.01837, 18.5789);


            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(
                        GeoPoint.FromDegrees(53.01961, 18.5778),
                        GeoPoint.FromDegrees(53.01833, 18.57907)
                    )
                );

            var (plan, turns) = ComputeTurns(map_filename!,
                start_north,
                end_south
            );

            //  SaveData(plan,turns,map_filename);

            Assert.Equal(2, turns.Count);

            int index = 0;

            Assert.Equal(10128836815, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(1, turns, index);
            ++index;

            Assert.Equal(8194667935, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(2, turns, index);
            ++index;
        }


        [Fact]
        public void TorunUgoryIndexOutOfRangeTest()
        {
            // program reported "index out of range" error while computing turn notifications

            var map_filename = "torun-ugory-index-out-of-range.kml";
            var start_north = GeoPoint.FromDegrees(53.0410195, 18.5979997);
            var end_south = GeoPoint.FromDegrees(53.0399498, 18.6000572);


            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(GeoPoint.FromDegrees(53.04133605957031, 18.597230911254883),
                        GeoPoint.FromDegrees(53.03950881958008, 18.60093116760254))
                );

            var prefs = PrefsFactory.GetRouterPreferences();
            prefs.Speeds[RoadSpeeding.UnknownStructured] = prefs.Speeds[RoadSpeeding.Sand];

            var settings = new MiniTestSettings() {RouterPreferences = prefs};

            var (plan, turns) = ComputeTurns(settings, map_filename!,
                start_north,
                end_south
            );

            // SaveData(plan,turns,map_filename);

            Assert.Equal(1, turns.Count);

            int index = 0;

            Assert.Equal(267869144, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            Assert.Equal(5, turns[index].TrackIndex);
            ++index;

            // a node between two roundabouts, program computes this as a link/shortcut
            // and skip this node, which is not good, and in future should be fixed. But to do
            // so we would need to rework entire idea of roundabout shortcuts
            // so this Assert is not a test of coretness but rather check in what mode
            // program works
            Assert.DoesNotContain(3560261144,
                plan.Where(it => it.IsNode()).Select(it => it.NodeId!.Value.Identifier));
        }

        [Fact]
        public void BydgoszczJaryDictionaryDuplicateTest()
        {
            // internal error, some duplicate entries in dictionary

            var map_filename = "bydgoszcz-jary-dictionary-duplicate.kml";
            var start_west = GeoPoint.FromDegrees(53.11638387140591, 17.9445391429252);
            var end_south = GeoPoint.FromDegrees(53.11877059936524, 17.952476501464847);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, Region.Build(
                    GeoPoint.FromDegrees(53.11916, 17.9531),
                    GeoPoint.FromDegrees(53.11571, 17.94291)));

            var (plan, turns) = ComputeTurns( map_filename!,
                start_west,
                end_south
            );

            if (this.Manual)
             SaveData(plan,turns,map_filename);

            if (COMP_MODE.UniTestWeightedTurns )
            {
                    Assert.Equal(3, turns.Count);

                    int index = 0;

                    Assert.Equal(OsmId.PureOsm(3899267053), turns[index].NodeId); //0
                    Assert.True(turns[index].Forward);
                    Assert.True(turns[index].Backward);
                    AssertTrackIndex(1, turns, index);
                    ++index;

                    Assert.Equal(OsmId.PureOsm(3618320750), turns[index].NodeId); //1
                    Assert.True(turns[index].Forward);
                    Assert.True(turns[index].Backward);
                    AssertTrackIndex(7, turns, index);
                    ++index;

                    Assert.Equal(OsmId.PureOsm(3618320749), turns[index].NodeId); //2
                    Assert.False(turns[index].Forward); // alt road has sharp turn there
                    Assert.True(turns[index].Backward);
                    AssertTrackIndex(8, turns, index);
                    ++index;
            }
            else
            {
                Assert.DoesNotContain(OsmId.PureOsm(2845889351),plan.Where(it => it.IsNode()).Select(it => it.NodeId));

                Assert.Equal(9, turns.Count);

                int index = 0;

                Assert.Equal(OsmId.PureOsm(3899267053), turns[index].NodeId); //0
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(1, turns, index);
                ++index;

                Assert.Equal(OsmId.PureOsm(3618320750), turns[index].NodeId); //1
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(7, turns, index);
                ++index;

                Assert.Equal(OsmId.PureOsm(3618320749), turns[index].NodeId); //2
                Assert.False(turns[index].Forward); // alt road has sharp turn there
                Assert.True(turns[index].Backward);
                AssertTrackIndex(8, turns, index);
                ++index;

                Assert.Equal(OsmId.PureOsm(10031999901), turns[index].NodeId); //3
                Assert.True(turns[index].Forward);
                Assert.False(turns[index].Backward);
                AssertTrackIndex(9, turns, index);
                ++index;

                Assert.Equal(OsmId.PureOsm(10031999904), turns[index].NodeId); //4
                // in micro-scale it is a turn, but major/overall perception is moving forward without turn
                // note, we alert about turns in advance, so micro-turns are simply ignored
                Assert.False(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(14, turns, index);
                ++index;

                Assert.Equal(OsmId.PureOsm(10031999921), turns[index].NodeId); //5
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(18, turns, index);
                ++index;

                // this turn is because of sloppy editing in OSM, instead of single road
                // there are 2 almost identical, overlapping, and our program takes them as a road split
                Assert.Equal(OsmId.PureOsm(10031999918), turns[index].NodeId); //6
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(20, turns, index);
                ++index;

                Assert.Equal(OsmId.PureOsm(10031999919), turns[index].NodeId); //7
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(23, turns, index);
                ++index;

                Assert.Equal(OsmId.PureOsm(5469294084), turns[index].NodeId); //8
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(24, turns, index);
                ++index;
            }
        }

        [Fact]
        public void BydgoszczJagiellonskaChoppedRoundaboutTest()
        {
            // this roundabout is a composition of several roads, and currently (2022-12-30)
            // it is interpreted as regular road 
            var map_filename = "bydgoszcz-jagiellonska-chopped-roundabout.kml";
            var start_west = GeoPoint.FromDegrees(53.12353897094726, 18.00667190551758);
            var end_south = GeoPoint.FromDegrees(53.12287139892578, 18.008882522583004);
            var end_east = GeoPoint.FromDegrees(53.12294, 18.01066);


            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_west,
                    GeoPoint.FromDegrees(53.1231803894043, 18.009010314941406),
                    end_south
                );

            through_roundabout();
            turn_into_oneway();

            void through_roundabout()
            {
                var (plan, turns) = ComputeTurns(map_filename!,
                    start_west,
                    end_south
                );

                if (this.Manual)
                SaveData(plan,turns,map_filename);

                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(589807017, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                Assert.Equal(18, turns[index].TrackIndex);
                ++index;
            }

            void turn_into_oneway()
            {
                // not related to roundabouts, simply reusing the map for another test

                var (plan, turns) = ComputeTurns(map_filename!,
                    end_south,
                    end_east
                );

                //  SaveData(plan,turns,map_filename);

                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(589807017, turns[index].NodeId!.Value.Identifier);
                Assert.False(turns[index].Forward); // going according with road signs
                Assert.True(turns[index].Backward);
                Assert.Equal(2, turns[index].TrackIndex);
                ++index;
            }
        }


        [Fact]
        public void TorunDetectingRoadDirectionTest()
        {
            // program reported it was unable to detect road direction

            var map_filename = "torun-detecting-road-direction.kml";

            var north_lower = GeoPoint.FromDegrees(53.03803, 18.68726);
            var north_low = GeoPoint.FromDegrees(53.03816, 18.68724);
            var end_north = GeoPoint.FromDegrees(53.03828, 18.68726);
            var start1_east_up = GeoPoint.FromDegrees(53.03815, 18.68652);
            var start2_east_low = GeoPoint.FromDegrees(53.03803, 18.68654);


            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: Region.Build(GeoPoint.FromDegrees(53.0386986314437, 18.686584472656246),
                        GeoPoint.FromDegrees(53.038028717041016, 18.688090913854687)));

            test_oneway();
            test_bidirectional();

            void test_oneway()
            {
                // the map has plenty of roads there, so we need to enforce points to prevent
                // snapping to the roads we don't intend to
                var (plan, turns) = ComputeTurns(new MiniTestSettings(),
                    map_filename,
                    CreateRequestPoint(start1_east_up) with {EnforcePoint = true},
                    CreateRequestPoint(north_low) with {EnforcePoint = true},
                    CreateRequestPoint(end_north) with {EnforcePoint = true}
                );

                // SaveData(plan,turns, map_filename);

                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(8646583453, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward); // we violate road signs, so we have notification
                Assert.True(turns[index].Backward); // same here
                AssertTrackIndex(1 + CrosspointOffset, turns, index);
            }

            void test_bidirectional()
            {
                // general route is as above, but we start with bi-directional road

                var (plan, turns) = ComputeTurns(new MiniTestSettings(),
                    map_filename,
                    CreateRequestPoint(start2_east_low) with {EnforcePoint = true},
                    CreateRequestPoint(north_lower) with {EnforcePoint = true},
                    CreateRequestPoint(north_low) with {EnforcePoint = true}
                );

                //  SaveData(plan,turns, map_filename);


                // one notification is when we ignore crosspoints at end/start, if not -- we have
                // second notication right at start
                const int turns_count = 2;
                Assert.Equal(turns_count, turns.Count);

                int index = 0;

                Assert.Equal(8646583451, turns[index].NodeId!.Value.Identifier);
                Assert.False(turns[index].Forward); // forward we go according to road signs
                Assert.True(turns[index].Backward); // backward we violate road signs so we need notifications
                Assert.Equal(2, turns[index].TrackIndex);
                ++index;

                if (turns_count == 2)
                {
                    Assert.Equal(8646583453, turns[index].NodeId!.Value.Identifier);
                    Assert.True(turns[index].Forward); // both ways are valid, so notification is a must
                    Assert.True(turns[index].Backward); // backward we violate road signs so we need notifications
                    Assert.Equal(3, turns[index].TrackIndex);
                    ++index;
                }
            }
        }

        [Fact]
        public void BydgoszczMostPomorskiPrecisionTest()
        {
            // program reported it cannot find road-id between two points
            // the problem was that at this point program was using floats as angles
            // and in this section of the map there nodes so close to each other that it led
            // to overlapping (two nodes with the same coordinates)

            var map_filename = "bydgoszcz-most_pomorski.kml";
            var start_north = GeoPoint.FromDegrees(53.1206, 18.03578);
            var end_south = GeoPoint.FromDegrees(53.11852, 18.03388);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_north,
                    end_south);

            var (plan, turns) = ComputeTurns(map_filename,
                start_north,
                end_south
            );

            //  SaveData(plan,turns,map_filename);

            Assert.Equal(5, turns.Count);

            int index = 0;

            Assert.Equal(3069042756, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(9 + CrosspointOffset, turns, index);

            ++index;

            Assert.Equal(3069042759, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(12 + CrosspointOffset, turns, index);

            ++index;

            Assert.Equal(6386806115, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(13 + CrosspointOffset, turns, index);

            ++index;

            Assert.Equal(6386806089, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(15 + CrosspointOffset, turns, index);

            ++index;

            Assert.Equal(6386804275, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(25 + CrosspointOffset, turns, index);
        }

        [Fact]
        public void ZninTouchingRoundaboutTest()
        {
            const string map_filename = "znin-touching-roundabout.kml";
            var start_east_far = GeoPoint.FromDegrees(52.849082946777344, 17.734582901000977);
            var end_north_far = GeoPoint.FromDegrees(52.848533630371094, 17.738370895385742);
            var start_east_close = GeoPoint.FromDegrees(52.84830856323242, 17.73773193359375);
            var end_north_close = GeoPoint.FromDegrees(52.84822082519531, 17.73702621459961);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_east_far,
                    GeoPoint.FromDegrees(52.84793090820313, 17.737062454223633),
                    end_north_far);

            far_end();
            too_close();
            long_route();

            void long_route()
            {
                // program reported we have too little points on roundabout

                var (plan, turns) = ComputeTurns(map_filename,
                    start_east_far,
                    end_north_far
                );

                // SaveData(plan, turns, map_filename);

                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(1817838666, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(4, turns, index);
            }

            void too_close()
            {
                try
                {
                    // program crashed with nullable object error

                    var (plan, turns) = ComputeTurns( //CreateDebugSettings(),
                        map_filename!,
                        start_east_close,
                        end_north_close
                    );

                    if (plan.Any())
                        throw new Exception("Those points were supposed to be so close that routing is impossible");
                }
                catch (Exception ex)
                {
                    // exception in such case is acceptable (but then the message has to inform about no route)
                    Assert.Equal(RouteFinding.RouteNotFound.ToString(), ex.Message);
                }
            }

            void far_end()
            {
                // the previous test with only 2 user points failed because it was too short
                // so now we add 3rd point, more distant, and now program can squeeze first
                // two points but computation has to succeed
                var (plan, turns) = ComputeTurns(
                    map_filename!,
                    start_east_close,
                    end_north_close,
                    end_north_far
                );

                if (this.Manual)
                    SaveData(plan, turns, map_filename);

                if (DevelModes.True)
                {
                    // since 2023-04-30 we are back here. The new cause for the change in the outcome
                    // is introduction of edges (instead of places) so we can add penalties for turns.
                    // But here the change was triggered by just edge introduction, w/o penalties yet.
                    
                    Assert.Equal(1, turns.Count);

                    int index = 0;

                    Assert.Equal(OsmId.PureOsm( 4703038233), turns[index].NodeId);
                    Assert.True(turns[index].Forward);
                    Assert.True(turns[index].Backward);
                    AssertTrackIndex(1, turns, index);
                }
                else
                {
                    // pre 2023-04-09, now we give more flexibility for snapping middle points, and the program 
                    // uses shorter route, but with turn
                    
                    // 2023-04-26: back to this outcome, but this time the route is a bit shorter

                    Assert.Equal(0, turns.Count);

                    // here we have first leg overlapping with the second, so shared parts
                    // of both will be removed
                    // if we completely remove first leg (possible, but it is corner case)
                    // this would drop to the first [1] node
                    if (!DevelModes.True)
                        // pre 2023-04-09
                        Assert.Equal(1872229413, plan[4].NodeId!.Value.Identifier);
                    else
                        // since 2023-04-26
                        Assert.Equal(OsmId.PureOsm(1872229415), plan[0].NodeId);
                }
            }
        }


        [Fact]
        public void MierucinekTimespanOverflowTest()
        {
            // program reported timespan overflow (because of zero length segments)

            var map_filename = "mierucinek-timespan-overflow.kml";

            var start_west = GeoPoint.FromDegrees(52.75612270697612, 18.013287503102948);
            var middle_turn = GeoPoint.FromDegrees(52.75545117436484, 18.020813733056517);
            var end_north = GeoPoint.FromDegrees(52.761077880859375, 18.02581787109375);


            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: Region.Build(
                        GeoPoint.FromDegrees(52.761077880859375, 18.013287503102948),
                        GeoPoint.FromDegrees(52.75499, 18.02624)));


            var settings = new MiniTestSettings();
            settings.FinderConfiguration ??= GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);

            var (plan, forward_turns) = ComputeTurns(settings,
                map_filename,
                start_west,
                middle_turn,
                end_north
            );

            //   SaveData(plan,forward_turns, map_filename);

            Assert.Equal(0, forward_turns.Count);
        }

        [Fact]
        public void GrudziadzTimespanOverflowTest()
        {
            // program reported an exception of timespan overflow when splitting legs
            // when preparing mini-map this error was replaced with another one:
            // computing roundabout turns near start/end of the route, after fixing it
            // cannot reproduce the original one despite they are not related (?)

            var map_filename = "grudziadz-timespan-overflow.kml";

            var far_east = GeoPoint.FromDegrees(53.4833, 18.74725);
            var end_south = GeoPoint.FromDegrees(53.48288, 18.74574);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: Region.Build(GeoPoint.FromDegrees(53.48388, 18.74574),
                        GeoPoint.FromDegrees(53.48288, 18.74632)));

            test_middle_point_at_roundabout();
            test_from_roundabout();

            void test_from_roundabout()
            {
                // testing starting right from the roundabout
                var (plan, turns) = ComputeTurns(map_filename!,
                    GeoPoint.FromDegrees(53.48388, 18.74632),
                    end_south
                );

                //  SaveData(plan,turns, map_filename);

                if (TrueVariant)
                {
                    // currently program do not alert when we start/end right at the roundabout
                    Assert.Equal(0, turns.Count);
                }
                else
                {
                    Assert.Equal(1, turns.Count);

                    int index = 0;

                    Assert.Equal(171921274, turns[index].RoadId!.Value.Identifier);
                    Assert.True(turns[index].Forward);
                    Assert.True(turns[index].Backward);
                    AssertTrackIndex(0, turns, index);
                }
            }

            void test_middle_point_at_roundabout()
            {
                // testing having middle user point right at rounadbout
                var (plan, turns) = ComputeTurns( //CreateDebugSettings(reverseInput:true),
                    map_filename,
                    far_east,
                    GeoPoint.FromDegrees(53.4837, 18.74615),
                    end_south
                );

                if (this.Manual)
                    SaveData(plan, turns, map_filename);


                Assert.Equal(1, turns.Count);

                int index = 0;

                // center of the roundabout
                Assert.Equal(new OsmId(171921274,1), turns[index].NodeId);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(6, turns, index);
            }
        }

        [Fact]
        public void PointyFragmentAtStartTest()
        {
            // 2023-04-04 there is roundabout center right at the start, followed by roundabout link
            // since roundabout is one road, and the link is another, program split center/link
            // creating 1-point fragment (this is invalid behaviour).

            var map_filename = "pointy-fragment-at-start.kml";
            var start = GeoPoint.FromDegrees(53.4334068, 18.145134);
            var middle = GeoPoint.FromDegrees(53.4352264, 18.1495838);
            var end = GeoPoint.FromDegrees(53.4347687, 18.1445446);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: Calculator.GetBoundary(Length.FromMeters(100), start, middle, end));

            var settings = new MiniTestSettings();
            settings.FinderConfiguration ??= GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);

            var (plan, turns) = ComputeTurns(settings, map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            var idx = 0;

            Assert.Equal(701544469, turns[idx].RoadId!.Value.Identifier);
            Assert.True(turns[idx].Forward);
            Assert.True(turns[idx].Backward);
            AssertTrackIndex(5, turns, idx);
        }

        [Fact]
        public void ReducingPhantomRoundaboutTest()
        {
            // 2023-04-02: after introducing phantom roundabout some previous logic
            // started to crash, like this one

            var map_filename = "reducing-phantom-roundabout.kml";
            var start = GeoPoint.FromDegrees(53.0667, 17.8179359);
            var middle = GeoPoint.FromDegrees(53.0698929, 17.8194866);
            var end = GeoPoint.FromDegrees(53.0717506, 17.8198185);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: Calculator.GetBoundary(Length.FromMeters(20), start, middle, end));

            var (plan, turns) = ComputeTurns(map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void FragmentWithSinglePointTest()
        {
            // after introducing phantom roundabouts the old logic compacting
            // roundabouts removed incorrectly route nodes lying on roundabout 
            // and this caused validation error

            var map_filename = "fragment-with-single-point.kml";
            var start = GeoPoint.FromDegrees(53.069894100000006, 17.8194874);
            var end = GeoPoint.FromDegrees(53.070881799999995, 17.8193027);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: Calculator.GetBoundary(Length.FromMeters(20), start, end));

            var (plan, turns) = ComputeTurns(map_filename,
                start,
                end
            );

            if (this.Manual)
                SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            var idx = 0;
            Assert.Equal(695356649, turns[idx].RoadId!.Value.Identifier);
            Assert.True(turns[idx].Forward);
            Assert.True(turns[idx].Backward);
            AssertTrackIndex(2, turns, idx);
        }


        [Fact]
        public void PruszczRoundaboutTest()
        {
            // the problem we had here was there some nodes on roundabout were shared by two roads -- one incoming
            // to the roundabout and one outgoing

            var map_filename = "pruszcz_roundabout.kml";
            var start_south = GeoPoint.FromDegrees(53.32827, 18.19968);
            var end_north = GeoPoint.FromDegrees(53.33078, 18.19943);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_south,
                    GeoPoint.FromDegrees(53.32998, 18.19815),
                    GeoPoint.FromDegrees(53.32972, 18.1988),
                    end_north);

            var (plan, turns) = ComputeTurns(map_filename,
                start_south,
                end_north
            );

            // SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            int index = 0;

            Assert.Equal(365486257, turns[index].RoadId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(2 + CrosspointOffset, turns, index);
        }


        [Fact]
        public void DorposzSzlachecki_YJunctionTest()
        {
            var map_filename = "legacy/dorposz_szlachecki_y_junction.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.28493, 18.43468),
                GeoPoint.FromDegrees(53.29396, 18.42947)
            );

            //  SaveData(plan, turns, map_filename);
            Assert.Equal(1, turns.Count);

            // it would be great if we could add some extra logic to remove the need of forward turn-notification, currently the angle is around 136 so it seen as turn
            var idx = 0;

            Assert.Equal(1839691512, turns[idx].NodeId!.Value.Identifier);
            Assert.True(turns[idx].Forward);
            Assert.True(turns[idx].Backward);
            AssertTrackIndex(9 + CrosspointOffset, turns, idx);
        }

        [Fact]
        public void BiskupiceTurnOnCyclewayTest()
        {
            // we should stick to the road and give notification and road turn
            // not at the cycleway turn (cycleway turn is earlier than road one)
            var map_filename = "legacy/biskupice_turn_on_cycleway.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.13756, 18.51066),
                GeoPoint.FromDegrees(53.14437, 18.50728)
            );

            //SaveData(plan, turns, map_filename);
            {
                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(1437255500, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(4 + CrosspointOffset, turns, index);
            }
        }

        [Fact]
        public void TorunChelminskaSmoothingCyclewayTest()
        {
            // there is a subtle Y junction on the cycleway (when using road though it
            // does not come to play)

            var map_filename = "legacy/torun_chelminska_smoothing_cycleway.kml";
            var start_south = GeoPoint.FromDegrees(53.06009, 18.55827);
            var end_north = GeoPoint.FromDegrees(53.06251, 18.5549);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_south,
                    end_north);

            var (plan, turns) = ComputeTurns(map_filename,
                start_south,
                end_north
            );

            //SaveData(plan,turns,map_filename);

            if (TrueVariant)
            {
                // the test fragment is so short, the program is capable of smoothing it out completely
                Assert.Equal(0, turns.Count);
            }
            else
            {
                Assert.Equal(1, turns.Count);

                Assert.Equal(1737021333, turns[0].NodeId!.Value.Identifier);
                Assert.True(turns[0].Forward);
                Assert.False(turns[0].Backward);
                Assert.Equal(1, turns[0].TrackIndex);
            }
        }


        [Fact]
        public void BiskupiceSwitchToCyclewayTest()
        {
            var map_filename = "legacy/biskupice_switch_to_cycleway.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.13679, 18.51126),
                GeoPoint.FromDegrees(53.14268, 18.50394)
            );

            //SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            if (TrueVariant)
            {
                // this is when route is set on road (better version)
                Assert.Equal(1437255500, turns[0].NodeId!.Value.Identifier);
                Assert.True(turns[0].Forward);
                Assert.True(turns[0].Backward);
                AssertTrackIndex(4 + CrosspointOffset, turns, 0);
            }
            else
            {
                // this is when route is set on cycleway (worse, because more complex, version)
                Assert.Equal(1437255494, turns[0].NodeId!.Value.Identifier);
                Assert.True(turns[0].Forward);
                Assert.True(turns[0].Backward);
                Assert.Equal(3 + CrosspointOffset, turns[0].TrackIndex);
            }
        }


        [Fact]
        public void BiskupiceSwitchingCyclewaySidesTest()
        {
            // road should be used

            var map_filename = "legacy/biskupice_switching_cycleway_sides.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.14337, 18.50604),
                GeoPoint.FromDegrees(53.14226, 18.50179)
            );

            Assert.Equal(0, turns.Count);
        }


        [Fact]
        public void StareRoznoTest()
        {
            var map_filename = "legacy/stare_rozno.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.88891, 18.6217),
                GeoPoint.FromDegrees(52.87858, 18.63708)
            );

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void LipieSidewalkTest()
        {
            var map_filename = "legacy/lipie_sidewalk.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.87693, 18.44306),
                GeoPoint.FromDegrees(52.87349, 18.43837)
            );

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void MarcinkowoGravelTest()
        {
            var map_filename = "legacy/marcinkowo_gravel.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.7958, 18.35691),
                GeoPoint.FromDegrees(52.79661, 18.35044)
            );

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void LipionkaTest()
        {
            var map_filename = "legacy/lipionka.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.85411, 18.43042),
                GeoPoint.FromDegrees(52.86473, 18.43156)
            );

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void LeszczAngledCrossingTest()
        {
            var map_filename = "legacy/leszcz_angled_crossing.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.10594, 18.51779),
                GeoPoint.FromDegrees(53.10766, 18.52053)
            );

            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void IncorrectFootwayReplacementTest()
        {
            // while replacing cycleway/footway with regular road program made incorrect
            // connection and left crosspoint in the middle of the leg.
            // The reason for the error was program wandering -- it went from user point
            // to crosspoint, to node, and then "back" to crosspoint, node, again to crosspoint,
            // and finally userpoint
            
            var map_filename = "incorrect-footway-replacement.kml";

            var start = GeoPoint.FromDegrees(  53.1086243,17.997824199999997);
            var end = GeoPoint.FromDegrees(    53.1080374,17.9971977);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary:null, Length.FromMeters(50),
                    start,
                    end);

            var settings = new MiniTestSettings
            {
                FinderConfiguration = GetFinderPreferences(),
            };
            settings.FinderConfiguration.CompactPreservesRoads = false;
            
            var (route, plan, turns) = ComputeTurns(settings,
                map_filename,
                start,
                end
            );

            if (this.Manual)
                SaveData(plan,turns, map_filename);

            Assert.Equal(1, turns.Count);

            int index = 0;

            // maybe this turn could be disabled/removed
            
            Assert.Equal(1589890894, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.False(turns[index].Backward);
            Assert.Equal(1, turns[index].TrackIndex);
        }


        [Fact]
        public void RebuildingSharedBucketTest()
        {
           // program crashed because of the gap between legs. The cause of it was
           // incorrect rebuilding of the shared bucket, it left two snaps and
           // program when building second leg chose the snap which didn't match the
           // one from the first leg
            
            var map_filename = "rebuilding-shared-bucket.kml";

            var start = CreateRequestPoint( GeoPoint.FromDegrees( 53.1974891,18.6125959));
            var middle = CreateRequestPoint(GeoPoint.FromDegrees(   53.1984913,18.6128783)) with {
            AllowSmoothing = false}
            ;
            var end = CreateRequestPoint(GeoPoint.FromDegrees( 53.1991364,18.613114799999998   ));
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary:null, Length.FromMeters(20),
                    start.UserPointFlat,
                    middle.UserPointFlat,
                    end.UserPointFlat);

            var settings = new MiniTestSettings
            {
                FinderConfiguration = GetFinderPreferences(),
            };
            
            var (route, plan, turns) = ComputeTurns(settings,
                map_filename,
                start,
                middle,
                end
            );

            if (this.Manual)
                SaveData(plan,turns, map_filename);

            Assert.Equal(0, turns.Count);

            // nothing important, just a node from the route
            Assert.Equal(OsmId.PureOsm(7862158423), plan[2].NodeId);
        }

        [Fact]
        public void WrzosyCyclepathTest()
        {
            var map_filename = "legacy/wrzosy_cyclepath.kml";
            var settings = new MiniTestSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            var (plan, turns) = ComputeTurns(settings, map_filename,
                GeoPoint.FromDegrees(53.05293, 18.56809),
                GeoPoint.FromDegrees(53.05573, 18.5632)
            );

            Assert.Equal(0, turns.Count);
        }


        [Fact]
        public void WymyslowoStraightTest()
        {
            var map_filename = "legacy/wymyslowo_straight.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.163, 18.50388),
                GeoPoint.FromDegrees(53.17132, 18.50534)
            );

            // initial turn is not reported, because we cut "tails" of the track, so basically the turn point becomes starting point
            Assert.Equal(0, turns.Count);
        }

        [Fact]
        public void PigzaSwitchFromCyclewayStraightTest()
        {
            var map_filename = "legacy/pigza_switch_from_cycleway_straight.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.11649, 18.52708),
                GeoPoint.FromDegrees(53.12156, 18.5282)
            );

            Assert.Equal(0, turns.Count);
        }


        [Fact]
        public void SiemonNotACrossJunctionTest()
        {
            // currently there is problem because OSM shows it as a cross junction, while in reality it is NOT cross junction
            // so we should get turn point in the middle (in ideal world)
            var map_filename = "legacy/siemon_not_a_cross_junction.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.16501, 18.39586),
                GeoPoint.FromDegrees(53.16677, 18.38891)
            );

            Assert.Equal(0, turns.Count); // it is impossible to get turn point with current OSM data
        }

        [Fact]
        public void LipieStraightTrackTest()
        {
            var map_filename = "legacy/lipie_straight_track.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.87949, 18.44857),
                GeoPoint.FromDegrees(52.87648, 18.44262)
            );

            //  SaveData(plan,turns,map_filename);

            Assert.Equal(2, turns.Count);

            int index = 0;

            Assert.Equal(746296255, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(8 + CrosspointOffset, turns, index);

            ++index;

            Assert.Equal(1575195605, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(9 + CrosspointOffset, turns, index);
        }

        [Fact]
        public void GaskiTest()
        {
            var map_filename = "legacy/gaski.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.83209, 18.42885),
                GeoPoint.FromDegrees(52.83003, 18.425)
            );

            Assert.Equal(2, turns.Count);

            Assert.Equal(1238387822, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.True(turns[0].Backward);
            AssertTrackIndex(2 + CrosspointOffset, turns, 0);

            Assert.Equal(4463230352, turns[1].NodeId!.Value.Identifier);
            Assert.False(turns[1].Forward);
            Assert.True(turns[1].Backward);
            AssertTrackIndex(4 + CrosspointOffset, turns, 1);
        }

        [Fact]
        public void RadzynChelminskiCrossedLoopTest()
        {
            // the track looks like this
            // ><>
            // the purpose of this test is to check if program correctly handle the entire track and it won't shorten it to
            // >
            // because it detects there is "shorter" path
            var map_filename = "legacy/radzyn_chelminski_crossed_loop.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.3689, 18.97037),
                GeoPoint.FromDegrees(53.35659, 18.99001),
                GeoPoint.FromDegrees(53.37222, 18.99997),
                GeoPoint.FromDegrees(53.35734, 18.96729)
            );

            //SaveData(plan,turns,map_filename);

            Assert.Equal(2, turns.Count);

            int index = 0;

            Assert.Equal(2118304203, turns[index].NodeId!.Value.Identifier);
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(66 + CrosspointOffset, turns, index);

            ++index;

            Assert.Equal(1846783922, turns[index].NodeId!.Value.Identifier);
            // not ideal here, but the angle at the the Y-junction point so sharp (it is twised junction),
            // that triggers need for notification
            // maybe if we could measure the point father apart from turn-point?
            Assert.True(turns[index].Forward);
            Assert.True(turns[index].Backward);
            AssertTrackIndex(98 + CrosspointOffset, turns, index);
        }


        [Fact]
        public void BiskupiceSwitchFromCyclewayTest()
        {
            var map_filename = "legacy/biskupice_switch_from_cycleway.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.14388, 18.50628),
                GeoPoint.FromDegrees(53.14635, 18.50787)
            );

            //SaveData(plan,map_filename);

            Assert.Equal(1, turns.Count);

            Assert.Equal(982422243, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.True(turns[0].Backward);
            AssertTrackIndex(3 + CrosspointOffset, turns, 0);
        }

        [Fact]
        public void TorunUnislawDedicatedCyclewayTest()
        {
            var map_filename = "legacy/torun_unislaw_dedicated_cycleway.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.0601, 18.55826),
                GeoPoint.FromDegrees(53.11972, 18.46862)
            );

            //SaveData(plan,turns,map_filename);

            Assert.Equal(1, turns.Count);

            Assert.Equal(1737021333, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.False(turns[0].Backward);
            AssertTrackIndex(1 + CrosspointOffset, turns, 0);
        }


        [Fact]
        public void TorunChelminskaCyclewaySnapWithTurnTest()
        {
            var map_filename = "legacy/torun_chelminska_cycleway_snap_with_turn.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.05968, 18.55871),
                GeoPoint.FromDegrees(53.05981, 18.55302)
            );

            //  SaveData(plan,turns,map_filename);

            if (!TrueVariant)
            {
                // this version goes at first by cycleway and then makes two gentle turns
                // this is preferred way, despite the fact it has two turns 

                Assert.Equal(2, turns.Count);

                Assert.Equal(1737021333, turns[0].NodeId!.Value.Identifier);
                Assert.True(turns[0].Forward);
                Assert.False(turns[0].Backward);
                Assert.Equal(1, turns[0].TrackIndex);

                Assert.Equal(3417741714, turns[1].NodeId!.Value.Identifier);
                Assert.True(turns[1].Forward);
                Assert.True(turns[1].Backward);
                Assert.Equal(6, turns[1].TrackIndex);
            }
            else
            {
                // this one goes first by major road and then makes rapid turn, it is not bad,
                // but the above one is better choice

                Assert.Equal(1, turns.Count);

                Assert.Equal(1403173205, turns[0].NodeId!.Value.Identifier);
                Assert.True(turns[0].Forward);
                Assert.True(turns[0].Backward);
                AssertTrackIndex(3 + CrosspointOffset, turns, 0);
            }
        }

        [Fact]
        public void GuessingRoadConditionTest()
        {
            // This test was thought as for guessing road condition so we could go in a straight line, instead
            // of some detour. However there is not much data to make a better guess, so turn weight was introduced
            // and only with this help the route is finally straight.
            
            // straight line is unclassified + unknown
            // detour is auxiliary + unpaved
            var map_filename = "guessing-road-condition.kml";

            var start = GeoPoint.FromDegrees(53.0343933, 18.4964333);
            var end = GeoPoint.FromDegrees(    53.035675, 18.4935036);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary:null, Length.FromMeters(100),
                    start,
                    end);

            var (route, plan, turns) = ComputeTurns(
                map_filename,
                start,
                end
            );

            if (this.Manual)
                SaveData(plan,turns, map_filename);

            // node from the straight segment
            Assert.Contains(OsmId.PureOsm(988562972),plan.Select(it => it.NodeId));
            
            Assert.Equal(0,turns.Count);
        }



        [Fact]
        public void RusinowoEasyOverridesSharpTest()
        {
            // the track has easy turn, the alternate road has sharp angle, so when going forward easy turn (track) should override sharp one and we should not get turn point
            var map_filename = "legacy/rusinowo_easy_overrides_sharp.kml";

            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.04441, 19.32008),
                GeoPoint.FromDegrees(53.05342, 19.33372)
            );

            Assert.Equal(1, turns.Count);

            Assert.Equal(1819846285, turns[0].NodeId!.Value.Identifier);
            Assert.False(turns[0].Forward);
            Assert.True(turns[0].Backward);
            AssertTrackIndex(5 + CrosspointOffset, turns, 0);
        }

        [Fact]
        public void PigzaSwitchToCyclewayTest()
        {
            var map_filename = "legacy/pigza_switch_to_cycleway.kml";
            var start_south = GeoPoint.FromDegrees(53.11469, 18.5243);
            var end_north = GeoPoint.FromDegrees(53.11667, 18.5273);
            var (plan, turns) = ComputeTurns(map_filename,
                start_south,
                end_north
            );

            // for now we will live with it, but ideally there should be no notification in this case
            Assert.Equal(1, turns.Count);

            Assert.Equal(1437255316, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.True(turns[0].Backward);
            AssertTrackIndex(3 + CrosspointOffset, turns, 0);
        }

        [Fact]
        public void PigzaSwitchFromCyclewayWithTurnTest()
        {
            var map_filename = "legacy/pigza_switch_from_cycleway_with_turn.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.11639, 18.52682),
                GeoPoint.FromDegrees(53.11876, 18.52663)
            );

            Assert.Equal(1, turns.Count);

            Assert.Equal(1615430797, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.True(turns[0].Backward);
            AssertTrackIndex(4 + CrosspointOffset, turns, 0);
        }

        [Fact]
        public void PigzaTurnOnNamedPathTest()
        {
            var map_filename = "legacy/pigza_turn_on_named_path.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.12008, 18.52574),
                GeoPoint.FromDegrees(53.12848, 18.51757)
            );

            //SaveData(plan, turns, map_filename);

            Assert.Equal(1, turns.Count);

            Assert.Equal(1437255487, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.False(turns[0].Backward);
            AssertTrackIndex(6 + CrosspointOffset, turns, 0);
        }

        [Fact]
        public void PigzaGoingStraightIntoPathTest()
        {
            var map_filename = "legacy/pigza_going_straight_into_path.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.11898, 18.52568),
                GeoPoint.FromDegrees(53.11742, 18.53591)
            );

//            SaveData(plan,turns,map_filename);

            // here we have T-junction  (on the left is regular road)
            // #____
            // #  |
            // with horizontal part of the same kind (path) while vertical is cycleway, we go along path
            // in theory we would not have turn notification (because we don't change road kind) but we have to
            // take into consideration two factors
            // (a) OSM can have errors
            // (b) the short link between road and T-junction can be in practive (while riding) treated more
            // like part of cycleway

            // but overall, it would be better to get rid of this notification

            if (TrueVariant)
            {
                // funny, after completly disabling part of algorithm now we got rid of this notification
                Assert.Equal(0, turns.Count);
            }
            else
            {
                Assert.Equal(1, turns.Count);

                int index = 0;

                Assert.Equal(6635814210, turns[index].NodeId!.Value.Identifier);
                Assert.True(turns[index].Forward);
                Assert.True(turns[index].Backward);
                AssertTrackIndex(2, turns, index);
            }
        }

        [Fact]
        public void DebowoStraightIntoMinorTest()
        {
            var map_filename = "legacy/debowo_straight_into_minor.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.68843, 18.0593),
                GeoPoint.FromDegrees(52.68376, 18.03473)
            );

            // the idea of this test is to check if we get alert on L-shaped major road, with extension of the minor one
            //  ::
            //  ::
            //  ++###
            //  ||
            //  ||
            //
            // :: here is minor one, when we go major->minor we need to get alert despite we are going in straight line, otherwise if we take turn
            // (which does not have alert) we could tell the difference between turning and going straight, because both cases would be alert-free.
            // Only when going back minor->major the alert can be skipped because it is obvious where to go (without alert)

            Assert.Equal(1, turns.Count);

            Assert.Equal(1116766709, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.False(turns[0].Backward);
            AssertTrackIndex(8 + CrosspointOffset, turns, 0);
        }

        [Fact]
        public void GaskiYTurnUnclassifiedTest()
        {
            var map_filename = "legacy/gaski_y-turn_unclassified.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(52.83042, 18.4256),
                GeoPoint.FromDegrees(52.82918, 18.42151)
            );

            // there should be no turn notification here because we suspect the turn-alternative road is a minor one,
            // but OSM at the moment has almost no data about the alternate road
            Assert.Equal(1, turns.Count);

            Assert.Equal(1238387747, turns[0].NodeId!.Value.Identifier);
            Assert.True(turns[0].Forward);
            Assert.False(turns[0].Backward);
            AssertTrackIndex(5 + CrosspointOffset, turns, 0);
        }

        [Fact]
        public void NawraAlmostStraightYJunctionTest()
        {
            var map_filename = "legacy/nawra_almost_straight_Y_junction.kml";
            var (plan, turns) = ComputeTurns(map_filename,
                GeoPoint.FromDegrees(53.18625, 18.49384),
                GeoPoint.FromDegrees(53.19082, 18.49915)
            );

            Assert.Equal(2, turns.Count);

            Assert.Equal(587567345, turns[0].NodeId!.Value.Identifier);
            Assert.False(turns[0].Forward);
            Assert.True(turns[0].Backward);
            AssertTrackIndex(2 + CrosspointOffset, turns, 0);

            Assert.Equal(587567344, turns[1].NodeId!.Value.Identifier);
            // todo: not ideal here, but the angle at the the Y-junction point so sharp (it is twisted junction), that triggers need for notification
            // maybe if we could measure the point father apart from turn-point?
            Assert.True(turns[1].Forward);
            Assert.True(turns[1].Backward);
            AssertTrackIndex(5 + CrosspointOffset, turns, 1);
        }
    }
}