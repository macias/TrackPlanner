using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using Xunit;

namespace TrackPlanner.Tests
{
    public class MiniWorldPlanTest : MiniWorld
    {
        public MiniWorldPlanTest(ILogger? logger = null,bool manual = false) : base(logger,manual)
        {
        }


        [Fact]
        public void WaldowoKrolewskieDisconnectedLegsTest()
        {
            // program reported the route legs were disconnected
            // problem was not in route, simply basic comparison of joint was wrong
            // because it included comparison of kind of point, not just coordinates

            var map_filename = "waldowo_krolewskie-disconnected-legs.kml";

            var start_long = GeoPoint.FromDegrees(53.1455078125, 18.268911361694336);
            var turn_point = GeoPoint.FromDegrees(53.1591104223874, 18.27302858590846);
            var end_short = GeoPoint.FromDegrees(53.14435195922851, 18.22655487060547);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_long,
                    GeoPoint.FromDegrees(53.14141082763672, 18.26648712158203),
                    end_short,
                    GeoPoint.FromDegrees(53.152740478515625, 18.27558517456055),
                    GeoPoint.FromDegrees(53.1589, 18.27466),
                    turn_point
                );

            var plan = ComputeRoute(map_filename,
                start_long,
                turn_point,
                end_short
            );

            //  SaveData(plan, map_filename);

            Assert.Equal(1845185802, plan[10].NodeId!.Value.Identifier);
        }

        [Fact]
        public void DabrowaBiskupiaNoFragmentsTest()
        {
            // program reported the route leg does not have any fragments

            var map_filename = "dabrowa_biskupia-no-fragments.kml";

            var start_east = GeoPoint.FromDegrees(52.776864768874724, 18.543718561040173);
            var end_west = GeoPoint.FromDegrees(52.77870178222657, 18.541725158691406);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: Region.Build(GeoPoint.FromDegrees(52.77918, 18.54078),
                        GeoPoint.FromDegrees(52.77635, 18.54456)));

            var plan = ComputeRoute(map_filename,
                start_east,
                end_west
            );

            if (this.Manual)
                SaveData(plan, map_filename);

            Assert.Equal(OsmId.PureOsm(3901380092), plan.First(it => it.IsNode()).NodeId);
        }

        [Fact]
        public void CzarnowczynPolishingCyclewayTest()
        {
            // program reported after using road instead of cycleway it cannot merge replacement
            // near the end segment (i.e. middle/turning point in this scenario)

            var map_filename = "czarnowczyn-polishing-cycleway.kml";

            var long_start_south = GeoPoint.FromDegrees(53.1618766784668, 18.0900936126709);
            var turn_north = GeoPoint.FromDegrees(53.17131191475751, 18.08650712583355);
            var short_end_south = GeoPoint.FromDegrees(53.16401290893555, 18.09068489074707);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false,
                    boundary: null, Length.FromMeters(25),
                    GeoPoint.FromDegrees(53.16116, 18.09058),
                    long_start_south,
                    GeoPoint.FromDegrees(53.16457, 18.09069),
                    GeoPoint.FromDegrees(53.16658, 18.08917),
                    GeoPoint.FromDegrees(53.17356, 18.08802),
                    turn_north,
                    short_end_south);

            var plan = ComputeRoute(map_filename,
                long_start_south,
                turn_north,
                short_end_south
            );

            if (this.Manual)
                SaveData(plan, map_filename);

            // just check for sake of checking, not important here
            Assert.Equal(893166334, plan[84].NodeId!.Value.Identifier); // regular road
        }

        [Fact]
        public void NowyWitoszynAdjacentEndingTest()
        {
            // program reported that at the end it skipped some nodes, so computed route was not continuous

            var map_filename = "nowy_witoszyn-adjacent-ending.kml";
            var start_north = GeoPoint.FromDegrees(52.70763397216797, 19.037010192871094);
            var end_south = GeoPoint.FromDegrees(52.70415115356445, 19.056560516357422);
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_north,
                    GeoPoint.FromDegrees(52.70546, 19.04381),
                    // going around the end point to extract its problematic region
                    GeoPoint.FromDegrees(52.70812, 19.06073),
                    GeoPoint.FromDegrees(52.7056, 19.07106),
                    end_south);

            var plan = ComputeRoute(map_filename,
                start_north,
                end_south
            );

            // SaveData(plan, map_filename);

            // just check for sake of checking, not important here
            Assert.Equal(1061823719L, plan[11].NodeId!.Value.Identifier);
        }

        [Fact]
        public void SliwiceShortcutTest()
        {
            // prefer longer, asphalt route 

            var map_filename = "sliwice-shortcut.kml";

            var prefs = PrefsFactory.GetRouterPreferences();
            prefs.Speeds[RoadSpeeding.UnknownLoose] = prefs.Speeds[RoadSpeeding.UnknownLoose] with {AddedCostFactor = 0.2};
            prefs.Speeds[RoadSpeeding.UnknownStructured] = prefs.Speeds[RoadSpeeding.UnknownStructured] with {AddedCostFactor = 0.2};

            var plan = ComputeRoute(new MiniTestSettings() {RouterPreferences = prefs}, map_filename,
                GeoPoint.FromDegrees(53.74666, 18.2053),
                GeoPoint.FromDegrees(53.72296, 18.21355)
            );
            //SaveData(plan, map_filename);

            Assert.DoesNotContain(plan, it => it.NodeId ==OsmId.PureOsm(  3146551955L) 
                                              || it.NodeId == OsmId.PureOsm(2495638839L));
        }

        [Fact]
        public void BlachowoShortcutTest()
        {
// just testing if we can set penalties in such way that program won't choose shortcut

            var map_filename = "blachowo-shortcut.kml";

            var start_north = GeoPoint.FromDegrees(54.01154, 17.60768);
            var end_south = GeoPoint.FromDegrees(54.00569, 17.61394);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_north,
                    GeoPoint.FromDegrees(54.00458, 17.60471),
                    GeoPoint.FromDegrees(54.00655, 17.61278),
                    end_south,
                    GeoPoint.FromDegrees(54.00791, 17.61295),
                    GeoPoint.FromDegrees(54.00957, 17.60858),
                    start_north
                );

            var prefs = PrefsFactory.GetRouterPreferences();
            prefs.Speeds[RoadSpeeding.Ground] = prefs.Speeds[RoadSpeeding.Ground] with {AddedCostFactor = 0.7};

            var plan = ComputeRoute(new MiniTestSettings() {RouterPreferences = prefs}, map_filename,
                start_north,
                end_south
            );

            if (this.Manual)
            SaveData(plan, map_filename);

            // node on a shortcut, with given settings we should avoid it
            Assert.DoesNotContain(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 1860295479);
            // node on longer route
            Assert.Contains(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 1948998919L);
        }

        [Fact]
        public void FijewoShortcutTest()
        {
            // initially program went through a "shortcut" -- road within gas station
            // first fix: add penalty when changing roads 

            // another possible approach would be decreasing penalty for riding high-speed roads (this case)
            // so it would not matter much if the ride is 10.1 km or 10 km

            var map_filename = "fijewo-shortcut.kml";

            var start_south = GeoPoint.FromDegrees(53.392567, 18.934572);
            var end_north = GeoPoint.FromDegrees(53.401714, 18.9353);

            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: false, boundary: null, Length.FromMeters(25),
                    start_south,
                    GeoPoint.FromDegrees(53.3958, 18.93158),
                    GeoPoint.FromDegrees(53.39598, 18.93031),
                    end_north);

            var plan = ComputeRoute(map_filename,
                start_south,
                end_north
            );

            Assert.Contains(plan.Where(it => it.IsNode()), it => it.NodeId!.Value.Identifier == 587587510);
        }

        [Fact]
        public void ZakrzewkoNoGoingBackTest()
        {
            // in first version program got to middle point, went back, then went again forward 

            var map_filename = "zakrzewko.kml";

            var plan = ComputeRoute(map_filename,
                GeoPoint.FromDegrees(53.097324, 18.640022),
                GeoPoint.FromDegrees(53.102116, 18.646202),
                GeoPoint.FromDegrees(53.110565, 18.661394)
            );

            // from 2022-12-26 no duplicates
            Assert.Equal(plan.Count, plan.Select(it => it.Point).Distinct().Count());
        }
    }
}