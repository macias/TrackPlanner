using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Geo;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using Xunit;
using TimeSpan = System.TimeSpan;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Tests
{
    public class MiniWorldElevationTests : MiniWorld
    {
        public MiniWorldElevationTests(ILogger? logger = null) : base(logger,manual:false)
        {
            
        }
        [Fact]
        public async Task DolskSpeedTestAsync()
        {
            var map_filename = "dolsk.kml";

            var bottom = GeoPoint.FromDegrees(53.43603, 18.3721);
            var top = GeoPoint.FromDegrees(53.45694, 18.36615);

            Length distance_uphill;
            TimeSpan duration_uphill;
            Length distance_downhill;
            TimeSpan duration_downhill;
            {
                var schedule = CreateSchedule(bottom, top);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();

                duration_uphill = summary.Days[0].TrueDuration;
                distance_uphill = summary.Days[0].Distance;

                var stats = summary.Days[0].ElevationStats;

                // sanity check
                stats.ClimbDistance.Meters.Should().BeGreaterThan(10);
                stats.ClimbHeight.Meters.Should().BeGreaterThan(10);
                stats.ClimbSlope.Percents.Should().BeGreaterThan(3);
            }
            {
                var schedule = CreateSchedule(top, bottom);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();
                
                duration_downhill = summary.Days[0].TrueDuration;
                distance_downhill = summary.Days[0].Distance;
            }

            Assert.Equal(distance_downhill.Meters, distance_uphill.Meters, precision: 7);
            const int just_something = 10; // something which will differentiate uphill vs. downhill time
            duration_uphill.TotalSeconds.Should().BeGreaterThan(duration_downhill.TotalSeconds + just_something);
            // following times have to be in sync with real ride
            // however we play here safely, the computed time can be longer than real
            // ones, especially when downhill is considered
            duration_downhill.TotalSeconds.Should().BeGreaterThan(279);
            duration_downhill.TotalSeconds.Should().BeLessThan(900);
            // so far I didn't ride uphill there
        }
        [Fact]
        public async Task GackiSpeedTestAsync()
        {
            var map_filename = "gacki.kml";

            var bottom = GeoPoint.FromDegrees(53.47752, 18.34136);
            var top = GeoPoint.FromDegrees(53.49701, 18.32391);

            Length distance_uphill;
            TimeSpan duration_uphill;
            Length distance_downhill;
            TimeSpan duration_downhill;
            {
                var schedule = CreateSchedule(bottom, top);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();

                duration_uphill = summary.Days[0].TrueDuration;
                distance_uphill = summary.Days[0].Distance;

                var stats = summary.Days[0].ElevationStats;

                // sanity check
                stats.ClimbDistance.Meters.Should().BeGreaterThan(10);
                stats.ClimbHeight.Meters.Should().BeGreaterThan(3);
                stats.ClimbSlope.Percents.Should().BeGreaterThan(3);
            }
            {
                var schedule = CreateSchedule(top, bottom);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();
                duration_downhill = summary.Days[0].TrueDuration;
                distance_downhill = summary.Days[0].Distance;
            }

            Assert.Equal(distance_downhill.Meters, distance_uphill.Meters, precision: 7);
            const int just_something = 10; // something which will differentiate uphill vs. downhill time
            duration_uphill.TotalSeconds.Should().BeGreaterThan(duration_downhill.TotalSeconds + just_something);
            // following times have to be in sync with real ride
            // however we play here safely, the computed time can be longer than real
            // ones, especially when downhill is considered
            duration_downhill.TotalSeconds.Should().BeGreaterThan(331);
            duration_downhill.TotalSeconds.Should().BeLessThan(1000);
            // so far I didn't ride uphill there
        }
        [Fact]
        public async Task RyjewoSpeedTestAsync()
        {
            var map_filename = "ryjewo.kml";

            var bottom = GeoPoint.FromDegrees(53.83843, 18.95887);
            var top = GeoPoint.FromDegrees(53.83811, 18.96084);

            Length distance_uphill;
            TimeSpan duration_uphill;
            Length distance_downhill;
            TimeSpan duration_downhill;
            {
                var schedule = CreateSchedule(bottom, top);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();

                duration_uphill = summary.Days[0].TrueDuration;
                distance_uphill = summary.Days[0].Distance;

                var stats = summary.Days[0].ElevationStats;

                // sanity check
                stats.ClimbDistance.Meters.Should().BeGreaterThan(10);
                stats.ClimbHeight.Meters.Should().BeGreaterThan(7);
                stats.ClimbSlope.Percents.Should().BeGreaterThan(3);
            }
            {
                var schedule = CreateSchedule(top, bottom);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();
                
                duration_downhill = summary.Days[0].TrueDuration;
                distance_downhill = summary.Days[0].Distance;
            }

            Assert.Equal(distance_downhill.Meters, distance_uphill.Meters, precision: 7);
            const int just_something = 10; // something which will differentiate uphill vs. downhill time
            duration_uphill.TotalSeconds.Should().BeGreaterThan(duration_downhill.TotalSeconds + just_something);
            // following times have to be in sync with real ride
            // however we play here safely, the computed time can be longer than real
            // ones, especially when downhill is considered
            duration_uphill.TotalSeconds.Should().BeGreaterThan(56);
            duration_downhill.TotalSeconds.Should().BeGreaterThan(24);

            duration_uphill.TotalSeconds.Should().BeLessThan(224); // todo: this is too much
            duration_downhill.TotalSeconds.Should().BeLessThan(90);
        }

        [Fact]
        public async Task RogoznoZamekSpeedTestAsync()
        {
            var map_filename = "rogozno-zamek.kml";

            var bottom = GeoPoint.FromDegrees(53.51814, 18.94963);
            var top = GeoPoint.FromDegrees(53.51969, 18.95819);

            Length distance_uphill;
            TimeSpan duration_uphill;
            Length distance_downhill;
            TimeSpan duration_downhill;
            {
                var schedule = CreateSchedule(bottom, top);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();

                duration_uphill = summary.Days[0].TrueDuration;
                distance_uphill = summary.Days[0].Distance;

                var stats = summary.Days[0].ElevationStats;

                // sanity check
                stats.ClimbDistance.Meters.Should().BeGreaterThan(10);
                stats.ClimbHeight.Meters.Should().BeGreaterThan(10);
                stats.ClimbSlope.Percents.Should().BeGreaterThan(3);
            }
            {
                var schedule = CreateSchedule(top, bottom);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();
                
                duration_downhill = summary.Days[0].TrueDuration;
                distance_downhill = summary.Days[0].Distance;
            }

            Assert.Equal(distance_downhill.Meters, distance_uphill.Meters, precision: 7);
            const int just_something = 10; // something which will differentiate uphill vs. downhill time
            duration_uphill.TotalSeconds.Should().BeGreaterThan(duration_downhill.TotalSeconds + just_something);
            // following times have to be in sync with real ride
            // however we play here safely, the computed time can be longer than real
            // ones, especially when downhill is considered
            duration_uphill.TotalSeconds.Should().BeGreaterThan(254);
            duration_downhill.TotalSeconds.Should().BeGreaterThan(46);

            duration_uphill.TotalSeconds.Should().BeLessThan(900);
            duration_downhill.TotalSeconds.Should().BeLessThan(150);
        }

        [Fact]
        public async Task KoszalinSpeedTestAsync()
        {
            var map_filename = "koszalin.kml";

            var bottom = GeoPoint.FromDegrees(54.20084, 16.21298);
            var top = GeoPoint.FromDegrees(54.20429, 16.2256);

            Length distance_uphill;
            TimeSpan duration_uphill;
            Length distance_downhill;
            TimeSpan duration_downhill;
            {
                var schedule = CreateSchedule(bottom, top);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();

                duration_uphill = summary.Days[0].TrueDuration;
                distance_uphill = summary.Days[0].Distance;

                var stats = summary.Days[0].ElevationStats;

                // sanity check
                stats.ClimbDistance.Meters.Should().BeGreaterThan(10);
                stats.ClimbHeight.Meters.Should().BeGreaterThan(10);
                stats.ClimbSlope.Percents.Should().BeGreaterThan(3);
            }
            {
                var schedule = CreateSchedule(top, bottom);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();
                
                duration_downhill = summary.Days[0].TrueDuration;
                distance_downhill = summary.Days[0].Distance;
            }

            Assert.Equal(distance_downhill.Meters, distance_uphill.Meters, precision: 7);
            const int just_something = 10; // something which will differentiate uphill vs. downhill time
            duration_uphill.TotalSeconds.Should().BeGreaterThan(duration_downhill.TotalSeconds + just_something);
            // following times have to be in sync with real ride
            // however we play here safely, the computed time can be longer than real
            // ones, especially when downhill is considered
            duration_downhill.TotalSeconds.Should().BeGreaterThan(113);
            duration_downhill.TotalSeconds.Should().BeLessThan(450);
            // so far I didn't ride uphill there
        }

        [Fact]
        public async Task KozielecSpeedTestAsync()
        {
            var map_filename = "kozielec.kml";

            var bottom = GeoPoint.FromDegrees(53.2442, 18.2596);
            var top = GeoPoint.FromDegrees(53.24591, 18.2516);

            Length distance_uphill;
            TimeSpan duration_uphill;
            Length distance_downhill;
            TimeSpan duration_downhill;
            {
                var schedule = CreateSchedule(bottom, top);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();

                duration_uphill = summary.Days[0].TrueDuration;
                distance_uphill = summary.Days[0].Distance;

                var stats = summary.Days[0].ElevationStats;

                // sanity check
                stats.ClimbDistance.Meters.Should().BeGreaterThan(10);
                stats.ClimbHeight.Meters.Should().BeGreaterThan(10);
                stats.ClimbSlope.Percents.Should().BeGreaterThan(3);
            }
            {
                var schedule = CreateSchedule(top, bottom);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();
                
                duration_downhill = summary.Days[0].TrueDuration;
                distance_downhill = summary.Days[0].Distance;
            }

            Assert.Equal(distance_downhill.Meters, distance_uphill.Meters, precision: 7);
            const int just_something = 10; // something which will differentiate uphill vs. downhill time
            duration_uphill.TotalSeconds.Should().BeGreaterThan(duration_downhill.TotalSeconds + just_something);
            // following times have to be in sync with real ride
            // however we play here safely, the computed time can be longer than real
            // ones, especially when downhill is considered
            duration_uphill.TotalSeconds.Should().BeGreaterThan(279);
            duration_uphill.TotalSeconds.Should().BeLessThan(900);
            // so far I didn't ride downhill there
            // duration_downhill.TotalSeconds.Should().BeGreaterThan(50);
        }

        [Fact]
        public async Task CiechocinSpeedTestAsync()
        {
            var map_filename = "ciechocin.kml";

            var bottom = GeoPoint.FromDegrees(53.06453323364258, 18.925151824951172); // Elgiszewo, bottom of the hill
            var top = GeoPoint.FromDegrees(53.056793212890625, 18.92420768737793); // Ciechocin, top of the hill

            Length distance_uphill;
            TimeSpan duration_uphill;
            Length distance_downhill;
            TimeSpan duration_downhill;
            {
                var schedule = CreateSchedule(bottom, top);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();

                duration_uphill = summary.Days[0].TrueDuration;
                distance_uphill = summary.Days[0].Distance;

                var stats = summary.Days[0].ElevationStats;

                // sanity check
                stats.ClimbDistance.Meters.Should().BeGreaterThan(10);
                stats.ClimbHeight.Meters.Should().BeGreaterThan(10);
                stats.ClimbSlope.Percents.Should().BeGreaterThan(3);
            }
            {
                var schedule = CreateSchedule(top, bottom);
                schedule.Settings.LoopRoute = false;

                await ProcessScheduleAsync(map_filename, schedule).ConfigureAwait(false);

                var summary = schedule.CreateSummary();
                
                duration_downhill = summary.Days[0].TrueDuration;
                distance_downhill = summary.Days[0].Distance;
            }

            Assert.Equal(distance_downhill.Meters, distance_uphill.Meters, precision: 7);
            const int just_something = 10; // something which will differentiate uphill vs. downhill time
            duration_uphill.TotalSeconds.Should().BeGreaterThan(duration_downhill.TotalSeconds + just_something);
        }

        private static void LogProfile(ScheduleJourney<TNodeId,TRoadId> schedule)
        {
            using (Shared.Logger.Create(new LoggerSettings()
                       {
                           LogPath = System.IO.Path.Combine(Navigator.GetOutputDirectory(), "log-test.txt")
                           
                       },
                       out ILogger logger))
            {
                TimeSpan total = TimeSpan.Zero;
                logger.Info("-------------------------------------------------------------------");
                foreach (var fragment in schedule.Route.Legs.SelectMany(it => it.Fragments))
                {
                    foreach (var (prev, next) in fragment.GetSteps().Slide())
                    {
                        TimeSpan time = schedule.Settings.RouterPreferences.GetRideTime(next.IncomingFlatDistance,
                            fragment.SpeedStyling.Mode, prev.Point.Altitude, next.Point.Altitude, out var speed);
                        logger.Info($"Height {DataFormat.FormatHeight(next.Point.Altitude, false)} {time.TotalSeconds:0} {speed.KilometersPerHour:0} ");
                        if (time < TimeSpan.Zero)
                            throw new Exception();
                        total += time;
                    }
                }

                logger.Info($"Total {total.TotalSeconds:0}");
            }
        }
    }
}