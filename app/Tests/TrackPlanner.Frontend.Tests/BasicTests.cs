using System.Threading.Tasks;
using Force.DeepCloner;
using Geo;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Visual;
using TrackPlanner.TestToolbox;
using TrackPlanner.VisualMonkey;
using Xunit;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Frontend.Tests
{
    public sealed class BasicTests
    {
        [Fact]
        public void StateHistoryTest()
        {
            var history = new StateHistory<int>(17,10);
            history.Add(2);
            history.Add(3);
            Assert.Equal(3, history.PeekCurrent());
            Assert.Equal(2, history.Undo());
            Assert.Equal(2, history.PeekCurrent());
            Assert.Equal(3, history.Redo());
            Assert.Equal(2, history.Undo());
            history.Add(8);
            Assert.Equal(8, history.PeekCurrent());
            Assert.Equal(2, history.Undo());
        }

        [Fact]
        public void RestoringUiStateTest()
        {
            var state = new UiState();
            state.Add(true);
            var copy = state.DeepClone();
            state.Add(true);
            Assert.Equal(2, state.CollapsedDays.Count);
            Assert.Equal(1, copy.CollapsedDays.Count);
            state.CopyFrom(copy);
            Assert.Equal(1, state.CollapsedDays.Count);
            Assert.Equal(1, copy.CollapsedDays.Count);

            state.Add(true);
            Assert.Equal(2, state.CollapsedDays.Count);
            Assert.Equal(1, copy.CollapsedDays.Count);
            copy = state.DeepClone();
            Assert.Equal(2, state.CollapsedDays.Count);
            Assert.Equal(2, copy.CollapsedDays.Count);
            state.Clear();
            Assert.Equal(0, state.CollapsedDays.Count);
            Assert.Equal(2, copy.CollapsedDays.Count);
            state.CopyFrom(copy);
            Assert.Equal(2, state.CollapsedDays.Count);
            Assert.Equal(2, copy.CollapsedDays.Count);
        }

        [Fact]
        public async ValueTask AddingAnchorsTestAsync()
        {
            var logger = new NoLogger();

            var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger,new Navigator(), new ApproximateCalculator());
            var schedule = controller.Manager.Schedule;

            await schedule.AddAnchorAsync(0,0,GeoPoint.FromDegrees(10, 10), "", LabelSource.AutoMap).ConfigureAwait(false);
            await schedule.AddAnchorAsync(0,1,GeoPoint.FromDegrees(20, 20), "", LabelSource.AutoMap).ConfigureAwait(false);

            // nothing crashed --> OK
            
            Assert.Empty(controller.ProblemMessages);
        }

        [Fact]
        public async ValueTask CountingLegsOnNewAnchorsTestAsync()
        {
            var logger = new NoLogger();
            //var controller = new DummyVisualReceiver<TNodeId,TRoadId>(logger);
            //var schedule = new VisualSchedule<TNodeId,TRoadId>(logger, controller, 
              //  ScheduleSettings.Defaults());
            //controller.Attach(schedule);
            
            var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger,new Navigator(), new ApproximateCalculator());
            var schedule = controller.Manager.Schedule;

            Assert.Equal(0,controller.LegCount);
            await schedule.AddAnchorAsync(0,0,GeoPoint.FromDegrees(10,10), "", LabelSource.AutoMap).ConfigureAwait(false);
            Assert.Equal(0,controller.LegCount);
            await schedule.AddAnchorAsync(0,1,GeoPoint.FromDegrees(20,20), "", LabelSource.AutoMap).ConfigureAwait(false);
            Assert.Equal(2,controller.LegCount);
            await schedule.AddAnchorAsync(0,2,GeoPoint.FromDegrees(30,30), "", LabelSource.AutoMap).ConfigureAwait(false);
            Assert.Equal(3,controller.LegCount);
            await schedule.AddAnchorAsync(0,3,GeoPoint.FromDegrees(40,40), "", LabelSource.AutoMap).ConfigureAwait(false);
            Assert.Equal(4,controller.LegCount);
 
            Assert.Empty(controller.ProblemMessages);
        }
    }
}