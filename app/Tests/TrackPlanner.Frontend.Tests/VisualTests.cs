using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Geo;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.RestClient;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Visual;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using TrackPlanner.VisualMonkey;
using Xunit;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Frontend.Tests
{
    public sealed class VisualTests : MiniWorld
    {
        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly ApproximateCalculator calc;

        public VisualTests(ILogger? logger = null, bool manual = false) : base(logger,manual)
        {
            this.logger = logger ?? new NoLogger();
            this.navigator = new Navigator();
            this.calc = new ApproximateCalculator();
        }

        private MockVisualReceiver<WorldIdentifier, WorldIdentifier> createMock(DraftOptions? draftRealOptions = null)
        {
            return createMock(createMockSettings(draftRealOptions));
        }

        private static VisualTestSettings createMockSettings(DraftOptions? draftRealOptions = null)
        {
            return new VisualTestSettings()
            {
                MockWorker = true,
                DraftRealOptions = draftRealOptions ?? DraftOptions.GetLegacyTests(),
                AlwaysReal = false
            };
        }

        private MockVisualReceiver<WorldIdentifier, WorldIdentifier> createMock(VisualTestSettings testSettings)
        {
            var disposable = MockVisualReceiver<TNodeId, TRoadId>.Create(this.logger, new Navigator(),
                new ApproximateCalculator(), testSettings, out var receiver);
            Assert.Null(disposable);
            return receiver;
        }

        private IDisposable? createReal(out MockVisualReceiver<TNodeId, TRoadId> controller)
        {
            return createController(createRealSettings(), out controller);
        }

        private IDisposable? createController(VisualTestSettings settings,
            out MockVisualReceiver<TNodeId, TRoadId> controller)
        {
            return MockVisualReceiver<TNodeId, TRoadId>.Create(logger, this.navigator,
                new ApproximateCalculator(), settings,
                out controller);
        }

        private static VisualTestSettings createRealSettings()
        {
            return new VisualTestSettings()
            {
                MockWorker = false,
                DraftRealOptions = DraftOptions.GetLegacyTests(),
                AlwaysReal = false
            };
        }

        [Fact]
        public async Task RouteWithGapTestAsync()
        {
            // here we on purpose create a gap

            const string map_filename = "route-with-gap.kml";

            var start = CreateRequestPoint(GeoPoint.FromDegrees(52.95965, 18.54363));
            // at this position there is no connection between start and end
            var middle = CreateRequestPoint(GeoPoint.FromDegrees(52.96272, 18.54985))
                with
                {
                    AllowGap = true
                };
            var end = CreateRequestPoint(GeoPoint.FromDegrees(52.96425, 18.55287));
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename,
                    strict: true,
                    boundary: null, Length.FromMeters(20),
                    start.UserPointFlat,
                    middle.UserPointFlat,
                    end.UserPointFlat);


            mini_compute_complete_route();
            await full_partial_rebuilds_switched_async().ConfigureAwait(false);
            await mini_partial_rebuilds_async().ConfigureAwait(false);
            await mini_partial_rebuilds_rev_async().ConfigureAwait(false);

            async ValueTask full_partial_rebuilds_switched_async()
            {
                var settings = createRealSettings();
                settings.FinderConfiguration = GetFinderPreferences();
                using (createController(settings, out var controller))
                {
                    await playAsync(controller,
                        new LoopSwitch(false),
                        new SetPlaceholder(0, 0),
                        new AddAnchorAtPlaceholder(start.UserPointFlat),
                        new AddAnchorAtPlaceholder(middle.UserPointFlat),
                        new AddAnchorAtPlaceholder(end.UserPointFlat),
                        // this build is successful, but creates big detour
                        new CompleteRebuild(),
                        new AutoBuildSwitch(true),
                        null
                    ).ConfigureAwait(false);

                    DEBUG_SWITCH.Enabled = true;
                    // this should trigger rebuild on both sides of the middle anchor
                    await controller.Manager.Schedule.Days[0].Anchors[1].SetAllowGapAsync(true).ConfigureAwait(false);

                    if (this.Manual)
                        SaveRoute(controller.Manager.Schedule, map_filename!);

                    Assert.Equal(2, controller.Manager.Schedule.Route.Legs.Count);
                    Assert.DoesNotContain(true,
                        controller.Manager.Schedule.Route.Legs.Select(it => it.IsDrafted));

                    var trans_route = MapTranslator.ConvertToOsm(controller.Map, controller.Manager.Schedule.Route);
                    // making sure the route is indeed straight line
                    Assert.Contains(OsmId.PureOsm(4559384667),
                        trans_route.Legs[0].AllSteps().Select(it => it.NodeId));
                    Assert.Contains(OsmId.PureOsm(1397879759),
                        trans_route.Legs[1].AllSteps().Select(it => it.NodeId));
                }
            }

            async ValueTask mini_partial_rebuilds_async()
            {
                var settings = createRealSettings() with {Filename = map_filename};
                settings.FinderConfiguration = GetFinderPreferences();
                using (createController(settings,
                           out var controller))
                {
                    await playAsync(controller,
                        new LoopSwitch(false),
                        new SetPlaceholder(0, 0),
                        new AddAnchorAtPlaceholder(start.UserPointFlat),
                        new AddAnchorAtPlaceholder(middle.UserPointFlat),
                        null
                    ).ConfigureAwait(false);

                    await controller.Manager.Schedule.Days[0].Anchors[1].SetAllowGapAsync(true).ConfigureAwait(false);

                    await playAsync(controller,
                        new PartialBuild(),
                        new AddAnchorAtPlaceholder(end.UserPointFlat),
                        null
                    ).ConfigureAwait(false);

                    DEBUG_SWITCH.Enabled = true;
                    await playAsync(controller,
                        new PartialBuild(),
                        null
                    ).ConfigureAwait(false);

                    if (this.Manual)
                        SaveRoute(controller.Manager.Schedule, map_filename!);

                    Assert.Equal(2, controller.Manager.Schedule.Route.Legs.Count);
                    Assert.DoesNotContain(true,
                        controller.Manager.Schedule.Route.Legs.Select(it => it.IsDrafted));
                }
            }

            async ValueTask mini_partial_rebuilds_rev_async()
            {
                var settings = createRealSettings() with {Filename = map_filename};
                settings.FinderConfiguration = GetFinderPreferences();
                using (createController(settings,
                           out var controller))
                {
                    await playAsync(controller,
                        new LoopSwitch(false),
                        new SetPlaceholder(0, 0),
                        new AddAnchorAtPlaceholder(middle.UserPointFlat),
                        new AddAnchorAtPlaceholder(end.UserPointFlat),
                        null
                    ).ConfigureAwait(false);

                    await controller.Manager.Schedule.Days[0].Anchors[0].SetAllowGapAsync(true).ConfigureAwait(false);

                    await playAsync(controller,
                        new PartialBuild(),
                        new SetPlaceholder(0, 0),
                        new AddAnchorAtPlaceholder(start.UserPointFlat),
                        new PartialBuild(),
                        null
                    ).ConfigureAwait(false);

                    Assert.Equal(2, controller.Manager.Schedule.Route.Legs.Count);

                    Assert.DoesNotContain(true,
                        controller.Manager.Schedule.Route.Legs.Select(it => it.IsDrafted));
                }
            }

            void mini_compute_complete_route()
            {
                var settings = new MiniTestSettings();
                settings.FinderConfiguration = GetFinderPreferences();
                var (route, plan, turns) = ComputeTurns(settings, map_filename!,
                    start,
                    middle,
                    end
                );

                if (this.Manual)
                    SaveData(plan, turns, map_filename!);

                var problem = route.Validate();
                Assert.Null(problem);

                // this section is less important


                    Assert.Equal(1, turns.Count);

                    var idx = 0;

                    // the fact we have turn notification at gap is questionable, but since it is first test case
                    // for route with gaps and secondly the guess is, gaps will be used very, very rarely
                    // it does not make sense to correct/optimize the behaviour right now

                    Assert.Equal(OsmId.PureOsm( 4559384667), turns[idx].NodeId);
                    Assert.False(turns[idx].Forward);
                    Assert.True(turns[idx].Backward);
                    AssertTrackIndex(4, turns, idx);
            }
        }

        private static void saveOsmRoute(MockVisualReceiver<WorldIdentifier, WorldIdentifier> controller, string mapFilename)
        {
            SaveRoute(MapTranslator.ConvertToOsm(controller.Map, controller.Manager.Schedule.Route), mapFilename);
        }

/*
        [Fact]
        public async Task XTestAsync()
        {
            using (createReal(out var controller))
            {

                await playAsync(controller,
                    null
                ).ConfigureAwait(false);
            }
        }
*/
        [Fact]
        public async Task DeletingLastAnchorOfDayTestAsync()
        {
            // this test ensures the logic -- if we delete last anchor of the day
            // this and the next day is merged automatically

            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0246964, 18.5641479)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1022682, 18.7028503)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1426506, 18.6108398)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0321312, 18.4364319)),
                new DaySplit(0, 2),
                new DeleteAnchor(0, 2),
                null
            ).ConfigureAwait(false);

            // it seems it is more useful to merge days on camp delete, then shifting camp around
            // just to preserve the number of days

            Assert.Equal(1, controller.Manager.Schedule.Days.Count);
        }

        [Fact]
        public async Task BriefAnchorInfoTestAsync()
        {
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0246964, 18.5641479)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1022682, 18.7028503)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1426506, 18.6108398)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0321312, 18.4364319)),
                null
            ).ConfigureAwait(false);

            await controller.Manager.EditCheckpointDetailsAsync(0, 1, new AnchorEdit(label: null, startTime: null,
                breakTime: TimeSpan.FromMinutes(10), point: null, allowGap: null,hasMap:null)).ConfigureAwait(false);

            // the only important test here really is, first and last info should not have break time
            // included, while middle one has to have it (actual values are irrelevant)
            // this one has user break time set > 0
            Assert.Matches("\\d\\d:\\d\\d \\+\\d\\d:\\d\\d", controller.Manager.Schedule.GetAnchorBriefInfo(0, 1));
            // this one does not have user break time
            Assert.Matches("\\d\\d:\\d\\d", controller.Manager.Schedule.GetAnchorBriefInfo(0, 2));

            Assert.Matches("\\d\\d:\\d\\d", controller.Manager.Schedule.GetAnchorBriefInfo(0, 0));
            Assert.Matches("\\d\\d:\\d\\d", controller.Manager.Schedule.GetAnchorBriefInfo(0, 3));
        }


        [Fact]
        public async Task ContinuingWithRoundaboutCenterTestAsync()
        {
            // program crashed because of the gap between legs. The gap was caused by one leg ending on roundabout center
            // and the adjacent leg could not start right from this point (because originally it had to start from some
            // segment between nodes).

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new LoopSwitch(false),
                    new AutoBuildSwitch(true),
                    new SnapToLineAt(GeoPoint.FromDegrees(53.2537473, 18.8659388)),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.3361205, 18.5694903, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new DaySplit(0, 1),
                    new SetPlaceholder(0, 1),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2955042, 18.6931968)),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task DeletingSingleAnchorOfDayTestAsync()
        {
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0246964, 18.5641479)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1022682, 18.7028503)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1426506, 18.6108398)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1368866, 18.4954834)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0915489, 18.4474182)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0321312, 18.4364319)),
                new DeletePlaceholder(),
                new DaySplit(0, 2),
                new DaySplit(1, 1),
                new DaySplit(2, 1),
                null
            ).ConfigureAwait(false);
            DEBUG_SWITCH.Enabled = true;
            await playAsync(controller,
                new DeleteAnchor(1, 0),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task PlaceholderAfterDeleteTestAsync()
        {
            {
                var controller = createMock(createMockSettings() with {AlwaysReal = true});

                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.238147058397615, 19.03031348674653)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.838147058397615, 19.93031348674653)),
                    new PinAutoAnchor(0, 1),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.138147058397615, 18.03031348674653)),
                    null
                ).ConfigureAwait(false);

                await playAsync(controller,
                    new DeleteAnchor(0, 1),
                    null
                ).ConfigureAwait(false);

                Assert.Equal((0, 1), controller.Manager.Schedule.AnchorPlaceholder);
            }
            {
                var controller = createMock(createMockSettings() with {AlwaysReal = true});

                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.238147058397615, 19.03031348674653)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.838147058397615, 19.93031348674653)),
                    new PinAutoAnchor(0, 1),
                    new PinAutoAnchor(0, 2),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.138147058397615, 18.03031348674653)),
                    new DaySplit(0, 1),
                    new SetPlaceholder(1, 0),
                    null
                ).ConfigureAwait(false);

                await playAsync(controller,
                    new DeleteAnchor(1, 0),
                    null
                ).ConfigureAwait(false);

                Assert.Equal((1, 0), controller.Manager.Schedule.AnchorPlaceholder);
            }
            {
                var controller = createMock(createMockSettings() with {AlwaysReal = true});

                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.41919737675338, 18.991663523865018)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.79218979344406, 17.704416849412834)),
                    new PinAutoAnchor(0, 9),
                    new SetPlaceholder(0, 3),
                    new DeleteAnchor(0, 12),
                    null
                ).ConfigureAwait(false);

                Assert.Equal((0, 10), controller.Manager.Schedule.AnchorPlaceholder);
            }
        }


        [Fact]
        public async Task ClearingPlaceholderAfterDeleteTestAsync()
        {
            // this test just checks the logic, it looks for user is better to have no placeholder
            // after delete

            // this is good choice at the moment of writing, so if this (choice) is changed
            // somewhere in the future, maybe it means we need preferences for this behaviour
            // or some other more flexible approach 

            var controller = createMock();
            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2955042, 18.6931968)),
                new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.3361205, 18.5694903, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                new DeletePlaceholder(),
                new DeleteAnchor(0, 0),
                null
            ).ConfigureAwait(false);
            Assert.Null(controller.Manager.Schedule.AnchorPlaceholder);
        }

        [Fact]
        public async Task OpeningDayAfterSplitTestAsync()
        {
            // this test just checks if after the split the previous day is kept open
            // there is no higher logic here, it simply turned out it is more useful
            // because usually after the split there is some fine tuning to the last spot
            // of previous day needed (like setting label for it)

            // this is good rationale at the moment of writing, so if this (rationale) is changed
            // somewhere in the future, maybe it means we need preferences for this behaviour
            // or some other more flexible approach than simply opening next day instead

            var controller = createMock();
            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2955042, 18.6931968)),
                new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.3361205, 18.5694903, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                new DaySplit(0, 1),
                null
            ).ConfigureAwait(false);
            Assert.False(controller.Manager.UiState.CollapsedDays[0]);
            Assert.True(controller.Manager.UiState.CollapsedDays[1]);
        }

        [Fact]
        public async Task PlaceholderLiveLineTestAsync()
        {
            // just checking the logic


            LivePlaceholderLine? line;
            var controller = createMock();
            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0775261, 18.4460449)),
                null
            ).ConfigureAwait(false);
            Assert.Equal((0, 1), controller.Manager.Schedule.AnchorPlaceholder);
            line = controller.Manager.Schedule.GetLivePlaceholderLine();
            Assert.NotNull(line);
            Assert.Equal((0, 0), line.Value.Start);
            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1821747, 18.4446716)),
                null
            ).ConfigureAwait(false);
            Assert.Equal((0, 2), controller.Manager.Schedule.AnchorPlaceholder);
            line = controller.Manager.Schedule.GetLivePlaceholderLine();
            Assert.NotNull(line);
            Assert.Equal((0, 1), line.Value.Start);
            Assert.Equal((0, 0), line.Value.End);
            Assert.Equal(1, line.Value.LegIndex);
            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3021584, 18.7825012)),
                null
            ).ConfigureAwait(false);
            Assert.Equal((0, 3), controller.Manager.Schedule.AnchorPlaceholder);
            line = controller.Manager.Schedule.GetLivePlaceholderLine();
            Assert.NotNull(line);
            Assert.Equal((0, 2), line.Value.Start);
            Assert.Equal((0, 0), line.Value.End);
            Assert.Equal(2, line.Value.LegIndex);
        }

        [Fact]
        public async Task SnapLineIncorrectLegTestAsync()
        {
            // program snapped incorrect leg

            var controller = createMock();
            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0775261, 18.4460449)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1821747, 18.4446716)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3021584, 18.7825012)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2265892, 19.1889954)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0469933, 19.2686462)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.9957771, 18.9266968)),
                new DeletePlaceholder(),
                null
            ).ConfigureAwait(false);
            DEBUG_SWITCH.Enabled = true;
            const double latitude = 53.0445175;
            const double longitude = 19.069519;
            await playAsync(controller,
                new SnapToLineAt(GeoPoint.FromDegrees(latitude, longitude)),
                null
            ).ConfigureAwait(false);

            var user_point = controller.Manager.Schedule.Days[0].Anchors[5].UserPoint;
            const int precision = 7;
            Assert.Equal(latitude, user_point.Latitude.Degrees, precision: precision);
            Assert.Equal(longitude, user_point.Longitude.Degrees, precision: precision);
        }

        [Fact]
        public async Task TransferringAttractionToNextLegTestAsync()
        {
            // todo: good case for mock run 

            // program crashed because it tried to transfer attractions outside the available legs

            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(false),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1919859, 18.8301472)),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.1660603, 18.4947731, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new PartialBuild(),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new SnapToLineAt(GeoPoint.FromDegrees(53.2049846, 18.7120335)),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task DeletingObsoleteFilesTestAsync()
        {
            // program crashed because it couldn't correctly figure out the daily turns file for removal

            // todo: convert it to mock someday, because this test is too trivial to be real
            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2169405, 18.4969176, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2047495, 18.8637229, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new CompleteRebuild(),
                    new Save("0.trproj"),
                    new DaySplit(0, 2),
                    new CompleteRebuild(),
                    new LoopSwitch(false)
                ).ConfigureAwait(false);

                await controller.Manager.EditCheckpointDetailsAsync(0, 0, new AnchorEdit(label: "hello", startTime: null,
                    breakTime: null, point: null, allowGap: null, hasMap:null)).ConfigureAwait(false);

                Assert.Equal("hello", controller.Manager.Schedule.Days[0].Anchors[0].Label);
                Assert.Equal(LabelSource.User, controller.Manager.Schedule.Days[0].Anchors[0].LabelSource);

                await playAsync(controller,
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.1223385, 18.8051577), 0, 0),
                    new Save("0.trproj"),
                    null
                ).ConfigureAwait(false);

                // the label is reset because of the position change
                Assert.Equal("", controller.Manager.Schedule.Days[0].Anchors[0].Label);
                Assert.Equal(LabelSource.AutoMap, controller.Manager.Schedule.Days[0].Anchors[0].LabelSource);
            }
        }


        [Fact]
        public async Task SelectiveAddingAttractionsTestAsync()
        {
            // program crashed because it tried to add duplicate attraction

            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.290646, 18.6045904)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1239786, 18.6300889)),
                    new Save("0.trproj"),
                    new Save("0.trproj"),
                    new DeletePlaceholder(),
                    new ClearAttractions(0),
                    new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                    new DaySplit(0, 1),
                    new SetPlaceholder(1, 0),
                    new DeletePlaceholder(),
                    new FindRouteAttractions(1, PointOfInterest.Category.Historic),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
            }
        }


        [Fact]
        public async Task SavingWithPartialDailyTurnsTestAsync()
        {
            // daily turns are not full built and it caused when saving project

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.3480991, 18.7573724, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1548862, 18.7026289)),
                    new FindPeaks(0, 0),
                    new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2040133, 18.5691858)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1206835, 18.7776633)),
                    new ClearAttractions(0),
                    new CompleteRebuild(),
                    new SetPlaceholder(0, 6),
                    new DeletePlaceholder(),
                    new DaySplit(0, 3),
                    new DeleteAnchor(0, 2),
                    new Redo(),
                    new ClearAttractions(0),
                    new DeletePlaceholder(),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.1399314, 18.6891708), 1, 0),
                    new SetPlaceholder(1, 1),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2119574, 18.457167)),
                    new Save("0.trproj"),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
            }
        }

        [Fact]
        public async Task ValidationWithEmptyLastLegTestAsync()
        {
            // the last leg was empty and this triggered false alarm that the middle leg is ended with
            // crosspoint (in fact "the middle leg" was effective last leg)

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2641854, 18.8460997, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1968337, 18.817333)),
                    new SetPlaceholder(0, 0),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.2013318, 18.7130129), 0, 0),
                    new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                    new AddRouteAttraction(0, 0, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.1808653, 18.6591739, null), OsmId.PureOsm(4891413325)), new PointOfInterest(null, null, PointOfInterest.Feature.Ruins))),
                    new DeleteAnchor(0, 0),
                    new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                    new DaySplit(0, 1),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddRouteAttraction(1, 1, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.1808653, 18.6591739, null), OsmId.PureOsm(4891413325)), new PointOfInterest(null, null, PointOfInterest.Feature.Ruins))),
                    new SetPlaceholder(1, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3369617, 18.6282704)),
                    new CompleteRebuild(),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task SummaryWithEmptyLegsAndNoAnchorsTestAsync()
        {
            // program crashed when creating summary -- when it tried to get user point
            // with empty legs and no anchors for given day

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.1596947, 18.827771, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.3130122, 18.70973, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new AutoBuildSwitch(true),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.29137, 18.8768316), 0, 0),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.2979369, 18.8727908), 0, 1),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new DaySplit(0, 1),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task ReferingStubLegTestAsync()
        {
            // while computing partial route the start/end of given leg is taken from adjacent leg.
            // In this case adjacent leg is a stub/missing leg, and the program crashed because of it.

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2003777, 18.457841, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2635465, 18.6836979, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new PartialBuild(),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.260904, 18.6810924)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.1292797, 18.6863707), 0, 0),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task EmptyLegWhenSmoothingTestAsync()
        {
// program crashed on empty leg when smoothing out shared points (between legs)
            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1642261, 18.814409)),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2136356, 18.8137736, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                    new AddRouteAttraction(1, 0, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2180731, 18.7676391, null), OsmId.PureOsm(9123032762)), new PointOfInterest("Ruins of the Gothic Church of St. John the Baptist from the 13th century", null, PointOfInterest.Feature.Ruins))),
                    new CompleteRebuild(),
                    new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                    new AddRouteAttraction(2, 0, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2180731, 18.7676391, null), OsmId.PureOsm(9123032762)), new PointOfInterest("Ruins of the Gothic Church of St. John the Baptist from the 13th century", null, PointOfInterest.Feature.Ruins))),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new CompleteRebuild(),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task EmptyCyclewayReplacementTestAsync()
        {
            // empty cycleway replacement caused program error

            var settings = createRealSettings();
            settings.SettingsCustomizer = (cfg) => { cfg.RouterPreferences.JoiningHighTraffic = TimeSpan.FromMinutes(180); };
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2116466, 18.8452767, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.2390571, 18.4220706), 0, 0),
                    new LoopSwitch(false),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.2376941, 18.47714), 0, 0),
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1144627, 18.6954262)),
                    new AutoBuildSwitch(true),
                    new PinAutoAnchor(0, 1),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.1139799, 18.7499817), 0, 0),
                    new LoopSwitch(true),
                    new CompleteRebuild(),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task HandlingEmptyStageTestAsync()
        {
            // program crashed because start and end points were the same, so the stage route was "found"
            // but was empty

            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new LoopSwitch(false),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2439707, 18.5708897)),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2546554, 18.809845, null), null), new PointOfInterest("foo", null, PointOfInterest.Feature.None))),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.3276219, 18.6535084, null), null), new PointOfInterest("foo", null, PointOfInterest.Feature.None))),
                    new LoopSwitch(true),
                    new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new AddRouteAttraction(1, 0, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2665519, 18.6510206, null), OsmId.PureOsm(9355330321)), new PointOfInterest("Ruiny zamku w Lipienku", null, PointOfInterest.Feature.Castle | PointOfInterest.Feature.Building))),
                    new DeleteAnchor(0, 3),
                    new DaySplit(0, 2),
                    new SetPlaceholder(1, 0),
                    new AutoBuildSwitch(true),
                    new ClearAttractions(1),
                    new DeleteAnchor(0, 0),
                    new FindRouteAttractions(1, PointOfInterest.Category.Historic),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new AddRouteAttraction(1, 0, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.266503, 18.6513692, null), OsmId.PureOsm(6852666349)), new PointOfInterest("Ruiny zamku w Lipienku", null, PointOfInterest.Feature.Castle))),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task AltitudeNumericRoudingCreatedGapTestAsync()
        {
            // program crashed with "gap between legs", this time the gap was solely because of the altitude difference.
            // and this was caused by incorrect approach to finding stable points. (X,Y) was stable, but to compute
            // altitude unstable (X,Y) was used, hence the Z difference 

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);

            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2506911, 18.7373988)),
                    new AutoBuildSwitch(true),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.24074, 18.73276)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3275997, 18.5489166)),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task EmptyLegOnSummaryTestAsync()
        {
            // program crashed when computing summary because given leg was empty

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.330319, 18.7450065)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1681205, 18.8256753)),
                    new DaySplit(0, 1),
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.329296, 18.7438435)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new CompleteRebuild(),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task PartialBuildWithEmptyLegsTestAsync()
        {
            // program crashed when preparing request for partial build -- it assumed
            // the adjacent leg is not empty

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2264379, 18.6613745)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2213623, 18.4848909)),
                    new DaySplit(0, 1),
                    new FindRouteAttractions(1, PointOfInterest.Category.Historic),
                    new PartialBuild(),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;

                await playAsync(controller,
                    new AddRouteAttraction(1, 6, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.266503, 18.6513692, null), OsmId.PureOsm(6852666349)), new PointOfInterest("Ruiny zamku w Lipienku", null, PointOfInterest.Feature.Castle))),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new AddRouteAttraction(1, 5, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2665519, 18.6510206, null), OsmId.PureOsm(9355330321)), new PointOfInterest("Ruiny zamku w Lipienku", null, PointOfInterest.Feature.Castle | PointOfInterest.Feature.Building))),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task DraftLegCreatesGapTestAsync()
        {
            // one of the leg couldn't be computed, as fallback it was created as a draft.
            // The adjacent leg could be computed but at the exact place, this was
            // reported as a gap

            var map_filename = "draft-leg-creates-gap.kml";
            var start_point = GeoPoint.FromDegrees(53.1926634, 18.7408709);
            var middle_point = GeoPoint.FromDegrees(53.18282, 18.7261);
            var end_point = GeoPoint.FromDegrees(53.20341, 18.72095);

            if (!DevelModes.True)
                new MiniExtractor(logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(
                        start_point,
                        middle_point,
                        end_point
                    )
                );

            var settings = createRealSettings()with {Filename = map_filename};
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings,
                       out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddAnchorAtPlaceholder(start_point),
                    new AddAnchorAtPlaceholder(middle_point),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddAnchorAtPlaceholder(end_point),
                    null
                ).ConfigureAwait(false);

                if (this.Manual)
                    SaveRoute(controller.Manager.Schedule, map_filename);
            }
        }


        [Fact]
        public async Task SkippedDraftedLegRefreshTestAsync()
        {
            // the program crashed with "gap between legs" because original logic checked if the leg was a draft
            // and if it was it skipped computing another draft -- this was a bug, because the start/end of the
            // leg was changed, so despite it was a draft, the refresh was needed anyway


            string map_filename = "skipped-drafted-leg-refresh.kml";
            var first_point = GeoPoint.FromDegrees(53.1910902, 18.7392026);
            var middle_point = GeoPoint.FromDegrees(53.17768, 18.72132);
            var last_point = GeoPoint.FromDegrees(53.18549, 18.70759);

            if (!DevelModes.True)
                new MiniExtractor(logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    Region.Build(
                        first_point,
                        middle_point,
                        last_point
                    )
                );

            var settings = createRealSettings()with {Filename = map_filename};
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings,
                       out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddAnchorAtPlaceholder(first_point),
                    new AddAnchorAtPlaceholder(middle_point),
                    new SetPlaceholder(0, 0),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddAnchorAtPlaceholder(last_point),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task ImplicitSnapshotsCorruptedStateTestAsync()
        {
            // program crash on "gap between legs" when restoring the file. The root
            // cause was during Rebuild the program changed the labels, this triggered
            // implicit state snapshots, so when undo was called it reverted to corrupted
            // in-the-middle state.
            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2780391, 18.76199)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.278394, 18.8177477)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new PartialBuild(),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new Undo(),
                    new Save("0.trproj"),
                    new Load("0.trproj"),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task EnforceOverAllNodesTestAsync()
        {
            // when computing forced point program still preferred nodes with public access.
            // This was the cause for getting gap between legs

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2809944, 18.6165007)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1865529, 18.6083073)),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task EmptyLegHandlingTestAsync()
        {
            // program crashed when calculating summary. The semi-root cause was user points
            // didn't create proper leg, so at first we got empty leg, and then this leg
            // was removed. So we had 3 anchors and only 1 leg
            // And true cause was the map itself was incorrectly converted into cell-form

            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new LoopSwitch(false),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.011528, 18.5656605)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0110787, 18.5660309)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.011219, 18.5671463)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new CompleteRebuild(),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task PreservingAnchorRoleOnAddingTestAsync()
        {
            // Note: this test is not about route shape, only about preserving the role of the anchors

            var controller = createMock();

            ValueTask prepareAsync()
            {
                controller.Reset();
                return playAsync(controller!,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0282593, 18.5910072)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0521507, 18.4950085)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1335907, 18.5564804)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1216431, 18.7144527)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0447235, 18.6667175)),
                    null
                );
            }

            {
                // here we have a loop and at the last segment we are adding anchor via snap-to-line
                // the anchor should be added while keeping the first anchor as the starting one.

                await prepareAsync().ConfigureAwait(false);

                // between first anchor ([0]) and the last one
                var extra_point = GeoPoint.FromDegrees(53.0366707, 18.634779);

                await playAsync(controller,
                    new SnapToLineAt(extra_point),
                    null
                ).ConfigureAwait(false);

                // newly added anchor should be the last, not the first
                var idx = controller.Manager.Schedule.Days[0].Anchors.Select(it => it.UserPoint).IndexOf(extra_point);
                Assert.Equal(controller.Manager.Schedule.Days[0].Anchors.Count - 1, idx);
            }
            {
                // we are checking if snap-to-line alters camping anchor in the middle of the route

                await prepareAsync().ConfigureAwait(false);

                // between anchor[1] and [2]
                var extra_point = GeoPoint.FromDegrees(53.0962906, 18.5399952);

                await playAsync(controller,
                    new DaySplit(0, 1),
                    new SnapToLineAt(extra_point),
                    null
                ).ConfigureAwait(false);

                // newly added anchor should be first of the second day
                var idx = controller.Manager.Schedule.Days[1].Anchors.Select(it => it.UserPoint).IndexOf(extra_point);
                Assert.Equal(0, idx);
            }
            {
                // we are checking if adding anchor alters camping anchor in the middle of the route

                await prepareAsync().ConfigureAwait(false);

                // between anchor[1] and [2]
                var extra_point = GeoPoint.FromDegrees(53.0962906, 18.5399952);

                await playAsync(controller,
                    new DaySplit(0, 1),
                    // add after camping (but not altering camping),
                    new PrepareAnchorAppend(0, 1),
                    new AddAnchorAtPlaceholder(extra_point),
                    null
                ).ConfigureAwait(false);

                // newly added anchor should be first of the second day
                var idx = controller.Manager.Schedule.Days[1].Anchors.Select(it => it.UserPoint).IndexOf(extra_point);
                Assert.Equal(0, idx);
            }
        }


        [Fact]
        public async Task GridCellMissesRoadTestAsync()
        {
            // program reported gap between legs. The root cause for this behaviour
            // was there was long road segment crossing the grid cell, and grid cell
            // didn't register it. And later when querying we couldn't hit this
            // particular spot because according to internal map it didn't exist
            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);

            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2937748, 18.8847693)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1556451, 18.6519957)),
                    new DaySplit(0, 2),
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.1418263, 18.4892986), 0, 0),
                    new SetPlaceholder(0, 1),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.207773, 18.6434061)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.247085, 18.5365381, null), null
#if DEBUG
                            , DEBUG_mapRef: null
#endif
                        ),
                        new PointOfInterest("foo", null, PointOfInterest.Feature.None))),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task GapBetweenLegsAgainTestAsync()
        {
            // program reported gap between legs (so the previous fix does not work)

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2661822, 18.7550898, null), null
#if DEBUG
                            , DEBUG_mapRef: null
#endif
                        ),
                        new PointOfInterest("foo", null, PointOfInterest.Feature.None))),
                    new SetPlaceholder(0, 0),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.2940253, 18.6341979, null), null
#if DEBUG
                            , DEBUG_mapRef: null
#endif
                        ),
                        new PointOfInterest("foo", null, PointOfInterest.Feature.None))),
                    new PartialBuild(),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1805947, 18.8224589)),
                    null
                ).ConfigureAwait(false);
                var problem = controller.Manager.Schedule.Route.Validate();
                Assert.Null(problem);
                await playAsync(controller,
                    new Save("1.trproj"),
                    new Load("1.trproj"),
                    null
                ).ConfigureAwait(false);
            }
        }


        [Fact]
        public async Task EmptyRouteTestAsync()
        {
            // when computing summary program crashed because of empty route

            var error_message = "Not here";

            var controller = createMock();
            controller.PlanClient.GetRealPlanFailurePredicate = _ => error_message;

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AutoBuildSwitch(true),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2135031, 18.6031374)),
                null
            ).ConfigureAwait(false);

            await playAsync(controller, error_message,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1462459, 18.643884))).ConfigureAwait(false);
        }

        private static async ValueTask playAsync(MockVisualReceiver<WorldIdentifier, WorldIdentifier> controller,
            string expectedProblem, VisualCommand command)
        {
            var before_state = controller.GetState();
            var result = await playRawAsync(controller, command).ConfigureAwait(false);

            controller.CommandCheck(command, before_state, result, expectedProblem);
        }

        [Fact]
        public async Task GapBetweenLegsTestAsync()
        {
            // originally program reported gap between legs

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1243869, 18.5193628)),
                    new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.3232363, 18.7247442, null), null
#if DEBUG
                            , DEBUG_mapRef: null
#endif
                        ),
                        new PointOfInterest("foo", null, PointOfInterest.Feature.None))),
                    new LoopSwitch(false),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1699546, 18.638327)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new PartialBuild(),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task PartialBuildFailureTestAsync()
        {
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.03460693359375, 18.658905029296875)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.13853454589844, 18.248291015624996)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.46189117431641, 18.78250122070313)),
                null
            ).ConfigureAwait(false);

            var failure_message = "testfail";

            controller.PlanClient.GetRealPlanFailurePredicate = _ => failure_message;
            controller.PlanClient.GetMockPlanFailureReason = failure_message;

            var result = await playRawAsync(controller, new PartialBuild()).ConfigureAwait(false);

            Assert.Equal(3, controller.Manager.Schedule.Route.Legs.Count);

            Assert.False(result.HasValue);
            Assert.Equal(failure_message, result.ProblemMessage.TryShortMessage());
            if (controller.ProblemMessages.Count > 1)
                Assert.Fail(controller.ProblemMessages[1]);

            _ = controller.Manager.Schedule.GetSummary();

            if (controller.ProblemMessages.Count > 1)
                Assert.Fail(controller.ProblemMessages[1]);
        }

        [Fact]
        public async Task CompleteRebuildFailureTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new Navigator(),new ApproximateCalculator());
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.03460693359375, 18.658905029296875)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.13853454589844, 18.248291015624996)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.46189117431641, 18.78250122070313)),
                null
            ).ConfigureAwait(false);

            var failure_message = "testfail";

            controller.PlanClient.GetRealPlanFailurePredicate = _ => failure_message;
            controller.PlanClient.GetMockPlanFailureReason = failure_message;

            var result = await playRawAsync(controller, new CompleteRebuild()).ConfigureAwait(false);

            Assert.Equal(3, controller.Manager.Schedule.Route.Legs.Count);

            Assert.False(result.HasValue);
            Assert.Equal(failure_message, result.ProblemMessage.TryShortMessage());
            if (controller.ProblemMessages.Count > 1)
                Assert.Fail(controller.ProblemMessages[1]);

            _ = controller.Manager.Schedule.GetSummary();

            if (controller.ProblemMessages.Count > 1)
                Assert.Fail(controller.ProblemMessages[1]);
        }

        [Fact]
        public async Task PartialRebuildTestAsync()
        {
            // testing basic behavior of the partial rebuild
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.03460693359375, 18.658905029296875)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.13853454589844, 18.248291015624996)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.46189117431641, 18.78250122070313)),
                null
            ).ConfigureAwait(false);

            Assert.True(controller.Manager.Schedule.Route.Legs.All(it => it.IsDrafted));

            await playAsync(controller,
                new PartialBuild(),
                null
            ).ConfigureAwait(false);

            DEBUG_SWITCH.Enabled = true;

            Assert.True(controller.Manager.Schedule.Route.Legs.All(it => !it.IsDrafted));

            Assert.Equal(4, controller.OnSummarySetCount);
            // we should have auto anchors at this point
            Assert.True(controller.Manager.Schedule.Days[0].Anchors.Count > 3);
        }

        [Fact]
        public async Task SingleSummarySetOnNewAnchorTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator());
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.238147058397615, 19.03031348674653)),
                null
            ).ConfigureAwait(false);

            Assert.Equal(1, controller.OnSummarySetCount);
            Assert.False(controller.Manager.Schedule.HasEnoughAnchorsForBuild());
            Assert.True(controller.Manager.CanUndo);

            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.238147058397615, 19.03031348674653)),
                null
            ).ConfigureAwait(false);

            Assert.Equal(2, controller.OnSummarySetCount);
            Assert.True(controller.Manager.Schedule.HasEnoughAnchorsForBuild());
        }

        [Fact]
        public async Task PreservingAttractionsTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2647376, 18.5313421)),
                new AutoBuildSwitch(true),
                new SetPlaceholder(0, 1),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1964988, 18.5743909)),
                new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                null
            ).ConfigureAwait(false);
            DEBUG_SWITCH.Enabled = true;
            await playAsync(controller,
                new AddExternalAttraction(new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(53.1202584, 18.465936, null), null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task AddingRouteAttractionTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.238147058397615, 19.03031348674653)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.459233647608826, 18.7735472834922)),
                new SetPlaceholder(0, 3),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2351091338213, 18.093375060804835)),
                new ChangeAnchorPosition(GeoPoint.FromDegrees(53.27595149619618, 18.888579858406843), 0, 7),
                new ChangeAnchorPosition(GeoPoint.FromDegrees(53.30935498932817, 18.74317698934316), 0, 2),
                new LoopSwitch(false),
                new FindRouteAttractions(0, PointOfInterest.Category.Historic),
                new FragmentInformationGetter(GeoPoint.FromDegrees(53.2384266, 19.0291938), 0, 0),
                new DaySplit(0, 3),
                null
            ).ConfigureAwait(false);
            DEBUG_SWITCH.Enabled = true;
            await playAsync(controller,
                new AddRouteAttraction(3, 1, new PlacedAttraction(new MapZPoint<OsmId, OsmId>(GeoZPoint.FromDegreesMeters(52.8692614, 17.8707131, 0), null),
                    new PointOfInterest("foo", null, PointOfInterest.Feature.Bridge))),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task EmptyLegInMiddleTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1101394189723, 18.20879211734954)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.18684508470446, 17.861869018933636)),
                new SetPlaceholder(0, 0),
                new PinAutoAnchor(0, 1),
                new PinAutoAnchor(0, 2),
                new DaySplit(0, 3),
                new PinAutoAnchor(1, 0),
                new DeleteAnchor(0, 3),
                null
            ).ConfigureAwait(false);
            await playAsync(controller,
                new DeleteAnchor(0, 2),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task NotResettingLastLegTestAsync()
        {
            // originally the last leg was reset at the last Add
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator());
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.09069988353853, 18.225547232949246)),
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.71875767560734, 18.5469317865042)),
                new SetPlaceholder(0, 2),
                null
            ).ConfigureAwait(false);
            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.40366310540152, 18.43816146306972)),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task DeleteLeavesDuplicateAnchorTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.00516252303911, 18.384167214592075)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.18132085179817, 17.74480646507058)),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new PinAutoAnchor(0, 2),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new PinAutoAnchor(0, 1),
                new SetPlaceholder(0, 4),
                new DaySplit(0, 1),
                new DeleteAnchor(1, 3),
                new PinAutoAnchor(1, 1),
                // this delete ends with two anchors with the same coordinates 
                new DeleteAnchor(0, 0),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ResetPlaceholderOnCompleteRebuildTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator());
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.540510300512295, 18.32302826650725)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.7548357070363, 18.710959707812854)),
                new AutoBuildSwitch(true),
                new PinAutoAnchor(0, 8),
                new PartialBuild(),
                new FragmentInformationGetter(GeoPoint.FromDegrees(52.9483016, 18.616802), 8, 0),
                new DeleteAnchor(0, 10),
                new SetPlaceholder(0, 15),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new CompleteRebuild(),
                null
            ).ConfigureAwait(false);

            Assert.Null(controller.Manager.Schedule.AnchorPlaceholder);
        }

        [Fact]
        public async Task WorkingOnLoadedRawDataTestAsync()
        {
            // this test was about buggy mock file storage, it returned direct reference to its data instead
            // of copy, but it is good to keep this test to avoid same mistake again in real scenario (like cache)

            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator());
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.09678809396993, 18.427171412512465)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.467930943713995, 18.22271218989079)),
                new PartialBuild(),
                new Save("0.trproj"),
                new Load("0.trproj"),
                new DeletePlaceholder(),
                new LoopSwitch(false),
                new SetPlaceholder(0, 5),
                new ChangeAnchorPosition(GeoPoint.FromDegrees(53.07169877408502, 18.35618112580379), 0, 5),
                new FragmentInformationGetter(GeoPoint.FromDegrees(53.0966902, 18.4268939), 0, 0),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new Load("0.trproj"),
                null
            ).ConfigureAwait(false);

            Assert.Null(controller.Manager.Schedule.AnchorPlaceholder);
        }


        [Fact]
        public async Task PlaceholderShiftOnNewAnchorTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //  alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.26254991385248, 18.12850413772321)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.0271376063279, 18.23167356137207)),
                null
            ).ConfigureAwait(false);
            await playAsync(controller,
                new SetPlaceholder(0, 6),
                null
            ).ConfigureAwait(false);
            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.19096424900974, 18.16751482500079)),
                null
            ).ConfigureAwait(false);
            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.52827961299395, 18.091265409157852)),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task DeleteWithEmptyDayMergeTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //  alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.82768791984576, 17.739036153887362)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.37699728099264, 18.846084459659803)),
                new PinAutoAnchor(0, 17),
                new DaySplit(0, 17),
                new DaySplit(0, 10),
                new DeleteAnchor(1, 6),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ResetPlaceholderOnLoopSwitchTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //  alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.860207724831355, 18.29787793576323)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.05279436927272, 18.99654127727186)),
                new SetPlaceholder(0, 12),
                new LoopSwitch(false),
                null
            ).ConfigureAwait(false);

            Assert.Null(controller.Manager.Schedule.AnchorPlaceholder);
        }

        [Fact]
        public async Task ResetPlaceholderOnPositionChangeTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator());
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.5058701551627, 18.201576206163043)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.865277007392116, 18.02240597179539)),
                new SetPlaceholder(0, 1),
                new ChangeAnchorPosition(GeoPoint.FromDegrees(53.18647678366365, 17.889780202293323), 0, 0),
                null
            ).ConfigureAwait(false);

            Assert.Null(controller.Manager.Schedule.AnchorPlaceholder);
        }

        [Fact]
        public async Task NumericStabilityTestAsync()
        {
            // the program crashed with "gap between legs". This was purely numeric. The second leg
            // started with unstable number, so when the first leg was computed, it ended with slightly
            // different position
            const string map_filename = "numeric-stability.kml";

            var start = GeoPoint.FromDegrees(53.17167, 18.83738);
            var middle = GeoPoint.FromDegrees(53.173139, 18.845265);
            var end = GeoPoint.FromDegrees(53.17528, 18.84938);

            if (!DevelModes.True)
                new MiniExtractor(logger).ExtractMiniMapFromBoundaryToFile(map_filename, boundary:
                    calc.GetBoundary(Length.FromMeters(10),
                        start,
                        middle,
                        end
                    )
                );

            using (createController(createRealSettings() with {Filename = map_filename},
                       out var controller))
            {
                await playAsync(controller,
                    new LoopSwitch(false),
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(start),
                    new AddAnchorAtPlaceholder(middle),
                    new AddAnchorAtPlaceholder(end),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new PartialBuild(),
                    null
                ).ConfigureAwait(false);
                ;
            }
        }

        [Fact]
        public async Task DeleteOnlyAnchorOfDayTestAsync()
        {
            // after deleting single (and only) anchor of second day
            // the day was preserved (empty) and subsequent command crashed
            var controller = createMock(createMockSettings() with {AlwaysReal = false});
            //using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.2909831, 18.5128667, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new LoopSwitch(false),
                    new SnapToLineAt(GeoPoint.FromDegrees(53.2312514, 18.8526844)),
                    new CompleteRebuild(),
                    new PinAutoAnchor(0, 1),
                    new DaySplit(0, 1),
                    new AutoBuildSwitch(true),
                    new OrderAnchor(0, 1, 1),
                    new PartialBuild(),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new DeleteAnchor(1, 1),
                    null
                ).ConfigureAwait(false);
                ;

                Assert.Equal(1, controller.Manager.Schedule.Days.Count);
            }
        }

        [Fact]
        public async Task LastAnchorIndexOnLoopSwitchTestAsync()
        {
            // program incorrectly assumed the last anchor index after loop switch

            var controller = createMock(createMockSettings() with {AlwaysReal = false});
            //using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new LoopSwitch(false),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1502938, 18.7182861)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2812086, 18.8790452)),
                    new SnapToLineAt(GeoPoint.FromDegrees(53.3151687, 18.5460516)),
                    new DaySplit(0, 1),
                    new DeleteAnchor(1, 0),
                    new OrderAnchor(0, 1, -1),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new LoopSwitch(true),
                    null
                ).ConfigureAwait(false);
                ;
            }
        }

        [Fact]
        public async Task RefreshPlaceholderOnAnchorReorderTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = false});
//            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.14574, 17.97292)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(51.98383, 18.02785)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.61105, 21.01613)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.21666, 21.18093)),
                    new CompleteRebuild(),
                    new SetPlaceholder(0, 80),
                    null
                ).ConfigureAwait(false);
                ;
                await playAsync(controller,
                    new OrderAnchor(0, 0, 1),
                    null
                ).ConfigureAwait(false);
                ;
            }
        }

        [Fact]
        public async Task PlaceholderMismatchAfterDayMergeTestAsync()
        {
            var controller = createMock();
//            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new AutoBuildSwitch(true),
                    new SetPlaceholder(0, 0),
                    new SnapToLineAt(GeoPoint.FromDegrees(53.1408761, 18.5813963)),
                    new SnapToLineAt(GeoPoint.FromDegrees(53.3496917, 18.5399041)),
                    new AutoBuildSwitch(false),
                    new SetPlaceholder(0, 2),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3529321, 18.7508698)),
                    new AutoBuildSwitch(true),
                    new DaySplit(0, 2),
                    new PartialBuild(),
                    new SetPlaceholder(0, 3),
                    new AutoBuildSwitch(false),
                    new DaySplitRemove(0),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task PlaceholderRefreshAfterLoopSwitchTestAsync()
        {
            // placeholder was not refreshed after loop switch

            var controller = createMock();
//            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.3302899, 18.5605308, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new SnapToLineAt(GeoPoint.FromDegrees(53.2594236, 18.6088258)),
                    new DaySplit(0, 1),
                    new SetPlaceholder(1, 0),
                    new LoopSwitch(false),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task DeleteLastAnchorAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.705561645716394, 18.60788595879906)),
                new LoopSwitch(false),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.39423496397692, 18.09745153005173)),
                new PinAutoAnchor(0, 7),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new DeleteAnchor(0, 9),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task DaySplitToSingleAnchorTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //  alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.69903291610758, 18.79209179166406)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.171747702484566, 18.354508237912505)),
                new DaySplit(0, 7),
                new PinAutoAnchor(0, 6),
                new DaySplit(0, 6),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task LeaveSingleAnchorTestAsync()
        {
            // var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator());
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new LoopSwitch(false),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.08905438105478, 19.090767494002574)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.16789525319565, 18.6781257917036)),
                new DeleteAnchor(0, 0),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ReorderingAnchorsAroundAutoTestAsync()
        {
            // auto-anchors prevented reordering anchors

            var controller = createMock(createMockSettings() with {AlwaysReal = true});
            {
                await playAsync(controller,
                    new SnapToLineAt(GeoPoint.FromDegrees(53.1982533, 18.5440939)),
                    new AutoBuildSwitch(true),
                    new SnapToLineAt(GeoPoint.FromDegrees(53.3548361, 18.848528)),
                    new DeleteAnchor(0, 0),
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1227925, 18.4644791)),
                    null
                ).ConfigureAwait(false);
                DEBUG_SWITCH.Enabled = true;
                await playAsync(controller,
                    new OrderAnchor(0, 0, 1),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task DeleteStartingDayTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.12335079520171, 18.41258190590181)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.42389384836319, 18.695063569915654)),
                new DaySplit(0, 4),
                null
            ).ConfigureAwait(false);
            DEBUG_SWITCH.Enabled = true;
            await playAsync(controller,
                new DeleteAnchor(0, 0),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task LoopSwitchOnDaySplitTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //  alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.14529993875627, 17.702169161852346)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.95386588369236, 19.106020677705878)),
                new DaySplit(0, 10),
                new LoopSwitch(false),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task MissingAnchorPlaceholderNotificationTestAsync()
        {
            // anchor placeholder in relative sense didn't change, but the data around it -- yes. So testing system
            // reported mismatch

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1578177, 18.7799414)),
                    new SetPlaceholder(0, 1),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.231668, 18.8598493)),
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1370188, 18.4582461)),
                    new SetPlaceholder(0, 5),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new ChangeAnchorPosition(GeoPoint.FromDegrees(53.2941283, 18.739393), 0, 0),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task RefreshingPlaceholderAfterReorderTestAsync()
        {
            // placeholder info was not refreshed after reorder

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.2371033, 18.4243197)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.1757591, 18.6975997)),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.3315066, 18.6872949, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new SetPlaceholder(0, 3),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.1190515, 18.4595808, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new PartialBuild(),
                    new OrderAnchor(0, 3, 1),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task ReorderingTailWithAutoAnchorsTestAsync()
        {
            // we have to pay attention when reordering anchor for next position, there could be
            // next anchors but they could be all auto ones

            var controller = createMock(createMockSettings() with {AlwaysReal = true});
            //using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.350812533739926, 18.77946262677822)),
                    new DeletePlaceholder(),
                    new AutoBuildSwitch(true),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.1276818, 18.5318402, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    null
                ).ConfigureAwait(false);
                Assert.True(controller.Manager.Schedule.Days[0].Anchors.Count == 6);
                Assert.True(controller.Manager.Schedule.Days[0].Anchors[3].IsPinned);
                // all other anchors are not pinned
                var trait = new CheckpointTrait(controller.Manager.Schedule, 0, 3);
                Assert.False(trait.CanOrderNext);
            }
        }

        [Fact]
        public async Task ReorderAnchorOnDayBoundaryTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});
            {
                await playAsync(controller,
                    new NewProject(),
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.31561587367101, 18.76170111687139)),
                    new AutoBuildSwitch(true),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.1626505, 18.77103, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.2828796, 18.6152753, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new DaySplit(0, 1),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new OrderAnchor(1, 0, -1),
                    null
                ).ConfigureAwait(false);
            }
        }


        [Fact]
        public async Task ChangingAnchorOrderAmongAutosTestAsync()
        {
            // the anchor which was supposed to be changed (reordered) was surrounded by auto-anchors which led
            // to change of the indices

            var settings = createRealSettings();
            settings.FinderConfiguration = GetFinderPreferences();
            settings.FinderConfiguration.FinalSnapProximityLimit = Length.FromKilometers(2);
            using (createController(settings, out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AutoBuildSwitch(true),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.327291, 18.7420499, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new SetPlaceholder(0, 0),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.1827113, 18.7376189, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    new DaySplit(0, 1),
                    new DeleteAnchor(0, 0),
                    new AddExternalAttraction(new PlacedAttraction(GeoZPoint.FromDegreesMeters(53.2081584, 18.4210913, null), new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None))),
                    null
                ).ConfigureAwait(false);
                await playAsync(controller,
                    new OrderAnchor(0, 2, -1),
                    null
                ).ConfigureAwait(false);
            }
        }

        [Fact]
        public async Task DeleteSecondAnchorTestAsync()
        {
            //var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //  alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.743869690068685, 18.56197374810425)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.00309539531238, 18.86054454468845)),
                new LoopSwitch(false),
                new DeleteAnchor(0, 4),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task DeleteSecondAnchorLoopedTestAsync()
        {
            //  var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //    alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.108901411721135, 19.097702515034076)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.92530964308462, 17.81801801673799)),
                new DeletePlaceholder(),
                new DaySplit(0, 9),
                new DeleteAnchor(0, 9),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task SwitchingLoopIndexOutOfRangeTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.51316931347602, 18.25925593849097)),
                new NewProject(),
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.109130958923245, 18.029649202002336)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.69933245656272, 17.880135302013272)),
                new DeletePlaceholder(),
                new SetPlaceholder(0, 1),
                new DaySplit(0, 5),
                new FragmentInformationGetter(GeoPoint.FromDegrees(52.7009337, 17.8807139), 5, 0),
                new ChangeAnchorPosition(GeoPoint.FromDegrees(52.928663657831, 18.550729272755948), 0, 0),
                new LoopSwitch(false),
                null
            ).ConfigureAwait(false);
        }


        [Fact]
        public async Task DaySplitRevertedLoopTestAsync()
        {
            // var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //   alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new NewProject(),
                null
            ).ConfigureAwait(false);

            await playAsync(controller, new Undo()).ConfigureAwait(false);

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.43082143807194, 17.97177322639233)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.212625814255105, 18.202900186616446)),
                new LoopSwitch(false),
                new Undo(),
                new SetPlaceholder(0, 6),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new DaySplit(0, 3),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task DaySplitUndoTestAsync()
        {
            // this is scenario which when turned into a test didn't fail
            // (this fact is a failure, but I am unable to track the reason for now)


            //  var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator(),
            //    alwaysReal: true);
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.73299800452929, 18.74095405094555)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.35898798517411, 18.68208530299223)),
                new DaySplit(0, 7),
                new Undo(),
                null
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task ValidatingCrosspointsWithEmptyLegsTestAsync()
        {
            // program crashed because the validation logic for crosspoint detection
            // raised false alarm

            // var controller = createMock();
            using (createReal(out var controller))
            {
                await playAsync(controller,
                    new SetPlaceholder(0, 0),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3423048, 18.8277963)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3471759, 18.83209)),
                    new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.3471759, 18.83209)),
                    new DaySplit(0, 1),
                    null
                ).ConfigureAwait(false);

                await playAsync(controller,
                    new CompleteRebuild(),
                    null
                ).ConfigureAwait(false);
            }
        }


        [Fact]
        public async Task MergeFirstDayTestAsync()
        {
            var controller = createMock();

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AutoBuildSwitch(false),
                new AutoBuildSwitch(true),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.92209770475543, 18.217106768236853)),
                new SetPlaceholder(0, 1),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.53442371623915, 18.368714753052416)),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new DaySplit(0, 7)
            ).ConfigureAwait(false);

            foreach (var trait in controller.Manager.Schedule.GetCheckpointTraits()
                         .Where(it => it.CanMerge))
            {
                // the camping cannot be set on the last day
                Assert.NotEqual(controller.Manager.Schedule.Days.Count - 1, trait.AnchorDayIndex);
            }
        }


        [Fact]
        public async Task StuckWithRoutingTestAsync()
        {
            // at the time being the starting point area was damaged by "graffiti"
            // someone deleted all the tracks, and put the logo + label. 

            // in general sense we had a case when one point is in disconnected area

            const string map_filename = "stuck-with-routing.kml";

            // if this area will get fixed someday, another good case
            // 53.9413414, 21.8733902 (path around church not connected to anything)
            var start = GeoPoint.FromDegrees(53.157589, 18.1481266);
            var middle = GeoPoint.FromDegrees(53.15615, 18.14942);
            var end = GeoPoint.FromDegrees(53.1556931, 18.1471615);

            // we use mini map only for debugging purpose (to see what programs sees)
            if (!DevelModes.True)
                new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(map_filename, strict: true,
                    boundary: Calculator.GetBoundary(Length.FromMeters(200),
                        start, middle, end),
                    extractionRange: null);

            // we stop snapping at first road found -- this leads to disconnected "islands"
            // and route cannot be computed properly
            compute_straight_route(bucketExpansion: false);
            compute_straight_route(bucketExpansion: true);
            await route_partially_found_async().ConfigureAwait(false);

            async ValueTask route_partially_found_async()
            {
                var settings = createRealSettings();
                settings.FinderConfiguration = GetFinderPreferences();
                // setting legacy mode (just snap on the first thing nearby)
                // to fail on first leg

                using (createController(settings, out var controller))
                {
                    await playAsync(controller,
                        new SetPlaceholder(0, 0),
                        new AddAnchorAtPlaceholder(start),
                        new AddAnchorAtPlaceholder(middle),
                        new AddAnchorAtPlaceholder(end),
                        new CompleteRebuild(),
                        null
                    ).ConfigureAwait(false);

                    // first leg failed, but the second has legit points so it should succeed
                    Assert.False(controller.Manager.Schedule.Route.Legs[1].IsDrafted,
                        "Second leg is drafted");
                }
            }

            void compute_straight_route(bool bucketExpansion)
            {
                var settings = new MiniTestSettings() {UseFullMap = true};
                if (!bucketExpansion)
                    settings.ExpectedProblem = RouteFinding.RouteNotFound.ToString();

                settings.FinderConfiguration ??= GetFinderPreferences();
                settings.FinderConfiguration.EnableDebugDumping = Manual;
                settings.FinderConfiguration.DumpProgress = this.Manual;
                settings.FinderConfiguration.FinalSnapProximityLimit
                    = Length.FromMeters(bucketExpansion ? 100 : 50);

                // distance to sane, networked road from the start point is around 90m
                // distance to "island"/disconnected path from the start is around 30m
                var (route, plan, turns) = ComputeTurns(settings,
                    map_filename,
                    start,
                    end
                );

                if (bucketExpansion)
                {
                    if (this.Manual)
                        SaveData(plan, turns, map_filename);

                    Assert.DoesNotContain(true, route.Legs.Select(it => it.IsDrafted));

                    // this less important, but let's make some checks anyway
                    Assert.Equal(OsmId.PureOsm(1827415624), plan[3].NodeId);
                    Assert.Equal(OsmId.PureOsm(1827415769), plan[8].NodeId);
                }
                else
                {
                    if (route.Legs.Any(it => !it.IsDrafted))
                    {
                        // uh-oh, somebody fixed the map, so test with current points does not make
                        // sense. Find some other place with disconnected roads
                        Assert.Fail("Map was fixed");
                    }
                }
            }
        }

        [Fact]
        public async Task PartialBuildIndexOutOfRangeTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.8804941449001, 19.020139310682644)),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.46175953043644, 17.932520122229537)),
                null // terminator for comma
            ).ConfigureAwait(false);

            await playAsync(controller,
                new DeleteAnchor(0, 10)
            ).ConfigureAwait(false);

            await playAsync(controller,
                new Redo(),
                // todo: if delete wouldn't reset placeholder, then get rid of this command
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.46175953043644, 17.932520122229537)),
                new PartialBuild()
            ).ConfigureAwait(false);
        }

        [Fact]
        public async Task AddingDuplicateAnchorTestAsync()
        {
            var controller = createMock();

            var coords = GeoPoint.FromDegrees(52.772744393480636, 18.58282516143944);

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(coords),
                null
            ).ConfigureAwait(false);

            // same coords as before
            var result = await playRawAsync(controller, new AddAnchorAtPlaceholder(coords))
                .ConfigureAwait(false);

            // no crash = success
        }

        [Fact]
        public async Task CannotAddAnchorTestAsync()
        {
            var controller = createMock(createMockSettings() with {AlwaysReal = true});

            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.772744393480636, 18.58282516143944)),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.872744393480636, 18.48282516143944)),
                new DaySplit(0, 2),
                new SetPlaceholder(0, 0),
                null
            ).ConfigureAwait(false);

            await playAsync(controller,
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.833461995121716, 18.977679476350584)),
                null
            ).ConfigureAwait(false);

            ;
        }

        [Fact]
        public async Task LegNotFoundTestAsync()
        {
            //  var controller = new MockVisualReceiver<TNodeId, TRoadId>(logger, new ApproximateCalculator());
            var controller = createMock();


            await playAsync(controller,
                new SetPlaceholder(0, 0),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(52.95353856869263, 18.612161253354156)),
                null).ConfigureAwait(false);

            await playAsync(controller,
                new ChangeAnchorPosition(GeoPoint.FromDegrees(53.38887997150444, 18.19777750184578), 0, 0),
                null).ConfigureAwait(false);

            await playAsync(controller,
                new Redo(),
                new SetPlaceholder(0, 1),
                new AddAnchorAtPlaceholder(GeoPoint.FromDegrees(53.31130530205091, 18.664858053513345)),
                null).ConfigureAwait(false);
        }


        private static ValueTask playAsync(MockVisualReceiver<TNodeId, TNodeId> controller,
            params VisualCommand?[] actions)
        {
            return playAsync(controller, null, actions);
        }

        private static async ValueTask playAsync(MockVisualReceiver<TNodeId, TNodeId> controller,
            DebugState[]? states,
            params VisualCommand?[] history)
        {
            foreach (var (entry, idx) in history.ZipIndex())
            {
                if (entry is not { } cmd)
                    continue;

                var current_state = DebugState.Create(controller.Manager.Schedule);
                if (states != null && current_state != states[idx])
                    throw new Exception("State differs");
                await controller.ValidateExecutionAsync(cmd, CancellationToken.None).ConfigureAwait(false);
            }
        }

        private static ValueTask<Result<ValueTuple>> playRawAsync(MockVisualReceiver<WorldIdentifier, WorldIdentifier> controller,
            VisualCommand cmd)
        {
            return controller.Dispatcher.ExecuteAsync(cmd, CancellationToken.None);
        }
    }
}