﻿using System.Diagnostics;
using System.Threading;
using TrackPlanner.Backend;

namespace TrackPlanner.TestRunner;

// https://learn.microsoft.com/en-us/dotnet/api/system.security.cryptography.randomnumbergenerator.create?view=net-7.0#system-security-cryptography-randomnumbergenerator-create
using System;
using System.Linq;
using System.Threading.Tasks;
using Geo;
using MathUnit;
using TrackPlanner.Frontend.Tests;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.Tests;
using TrackPlanner.TestToolbox;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

public class Disposal : IDisposable
{
    public void Dispose()
    {
        Console.WriteLine("disposed");
    }
}

class Program
{
    private static readonly Navigator navigator = new Navigator();

    static async Task Main(string[] args)
    {
        await Task.CompletedTask.ConfigureAwait(false);

        //  try
        {
            if (!DevelModes.True)
                using (new Disposal())
                {
                    throw new Exception();
                }
            if (!DevelModes.True)
            {
                var logger_settings = new LoggerSettings()
                {
                    LogPath = navigator.GetLogPath(),
                    Sink = new SinkSettings()
                    {
                        ConsoleLevel = MessageLevel.Debug,
                        FileLevel = MessageLevel.Info,
                    }
                };

                using (Logger.Create(logger_settings, out var main_logger))
                {
                    using (var monkey_router = new MonkeyRouter<TNodeId, TRoadId>(main_logger,
                               new SinkSettings()
                               {
                                   FileEnabled = false,
                                   ConsoleLevel = MessageLevel.Debug
                               }))
                    {
                        monkey_router.Simplify(TimeSpan.FromMinutes(8), 
                            // case 4
                           // RequestPoint<TNodeId>.ParsePoints(" L 53.30669340000001°,18.812948599999995°; SL 53.2226758°,18.6601236°;  53.1171401°,18.521380199999996°; L 53.1984913°,18.6128783°; L 53.2600115°,18.7584763°; L 53.30669340000001°,18.812948599999995°")
                            RequestPoint<TNodeId>.ParsePoints("S 53.1974891°,18.6125959°;  53.1984913°,18.6128783°; S 53.1991364°,18.613114799999998° ")
                        );
                    }
                    
                    
                }
            }

            Length extractionRange = Length.FromMeters(25);


        //    if (!DevelModes.True)
            {
                try
                {
                    var settings = new LoggerSettings()
                    {
                        LogPath = navigator.GetLogPath()
                    };
                    settings.Sink = settings.Sink with {FileEnabled = true};
                    using (Logger.Create(settings, out ILogger logger))
                    {
                       await new VisualTests(logger,manual:true).DeletingObsoleteFilesTestAsync().ConfigureAwait(false);
                      //new BigMapTurnTest(logger,manual:true).ChoosingUknownFootwayTest();
                        //new NewtonSerializationTests().ResultValueTest();
                        //new DataFormatTests().RawAngleTest();
                        //new CompactDictionaryFeaturesTests().LiveChangingKeysRemoveTest();
                        //new MiniWorldTurnTest(logger, manual:true).TorunSouthRangeTest();
                        //new MiniWorldPlanTest(logger,manual:true).CzarnowczynPolishingCyclewayTest();
                       //new MapTests().ComputingCellTargetsTest();
                       //new BAD_MiniWorldTurnTest(manual:true).TODO_MisleadingLTurnTest();
                    }
                }
                catch
                {
                    throw;
                }
            }

            if (!DevelModes.True)
                using (Logger.Create(new LoggerSettings()
                       {
                           LogPath = navigator.GetLogPath()
                       }, out ILogger logger))
                {
                    new MiniWorldTurnTest().TorunGagarinaCreatingConnectionTest();
                }

            Console.WriteLine(DateTimeOffset.UtcNow);
        }
        //   catch
        {
            //     throw;
        }
    }

    public void ExtractMiniMapFromFile(string[] args, Length extractionRange, params string[] planFilenames)
    {
        var snap_limit = Length.FromMeters(5);
if (!DevelModes.True)
{
            /*var config_builder = new ConfigurationBuilder()
                .AddJsonFile(EnvironmentConfiguration.Filename, optional: false)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();
            var env_config = new EnvironmentConfiguration();
            config_builder.GetSection(EnvironmentConfiguration.SectionName).CustomBind(env_config);
            //logger.Info($"{nameof(env_config)} {env_config}");
            env_config.Check();*/
}
        var visual_prefs = UserVisualPreferences.Defaults();

        using (Logger.Create(new LoggerSettings()
               {
                   LogPath = System.IO.Path.Combine(navigator.GetOutputDirectory(), "log.txt")
               }, out ILogger logger))
        {
            var sys_config = FinderConfiguration.Defaults() with {CompactPreservesRoads = true};
            using (RouteManager<long, long>.Create(logger, navigator, "kujawsko-pomorskie",
                       sys_config, out var manager))
            {
                foreach (var filename in planFilenames)
                {
                    GeoPoint[] raw_track_plan = TrackReader.LEGACY_Read(
                            System.IO.Path.Combine(navigator.GetLegacyTracks(), filename))
                        .Select(it => it.Convert2d())
                        .ToArray();

                    var nodes = manager.Map.GetNodesAlongLine(manager.Calculator, extractionRange, raw_track_plan);
                    var mini_map = manager.Map.ExtractMiniMap(logger,
                        sys_config.MemoryParams.GridCellSize, navigator.GetDebugDirectory(), onlyRoadBased: true,
                        strict: false, nodes);
                    mini_map.SaveAsKml(visual_prefs,
                        System.IO.Path.Combine(navigator.GetMiniMaps(), filename), flatRoads: true);
                }
            }
        }
    }
}

