﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.TestToolbox;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace MonkeyRunner
{
    class Program
    {
        private static readonly Navigator navigator = new Navigator();

        static async Task Main(string[] args)
        {
            var timeout = TimeSpan.FromMinutes(15);
            var grace_period = TimeSpan.FromMinutes(2);
            const int workers_count = 8;

            var sink_settings = new SinkSettings()
            {
                ConsoleLevel = MessageLevel.Debug,
                FileLevel = MessageLevel.Info,
            };
            using (Logger.Create(new LoggerSettings()
                   {
                       LogPath = navigator.GetLogPath("monkey-route.txt"),
                       Sink = sink_settings
                   }, out var main_logger))
            {
                var threaded_logger = new ThreadedLogger(main_logger, sink_settings);
                try
                {
                    //if (!DevelModes.True)
                    {
                        main_logger.Info("Dry run first");
                        
                        // dry run
                        using (var monkey_router = new MonkeyRouter<TNodeId, TRoadId>(threaded_logger))
                        {
                            ; 
                        }

                    }

                    
                    
                    var all_stats = new List<MonkeyStats<RequestPoint<TNodeId>>>();
                    var alive_count = workers_count;
                    for (int __i = 0; __i < workers_count; ++__i)
                    {
                        int i = __i;
                        var s = new MonkeyStats<RequestPoint<TNodeId>>(grace_period);
                        all_stats.Add(s);
                        // not awaited async call
                        #pragma warning disable CS4014 
                        Task.Factory.StartNew(() =>
                        {
                            threaded_logger.Verbose($"Starting {i} worker");
                            using (var monkey_router = new MonkeyRouter<TNodeId, TRoadId>(threaded_logger))
                            {
                                monkey_router.ComputeMany(timeout, s);
                            }
                        },TaskCreationOptions.LongRunning);
                        #pragma warning restore
                    }

                    await Task.Factory.StartNew(async () =>
                    {
                        long start = Stopwatch.GetTimestamp();

                        threaded_logger.Verbose("Starting reporter");
                        while (true)
                        {
                            int sucess_count = 0;
                            int problems_count = 0;
                            int not_found_count = 0;
                            foreach (var stats in all_stats)
                            {
                                sucess_count += stats.SucessCount;
                                problems_count += stats.ProblemsCount;
                                not_found_count += stats.NotFoundCount;

                                if (stats.Alive)
                                {
                                    if (stats.IsExpired(out var run, out var points))
                                    {
                                        stats.Alive = false;
                                        --alive_count;
                                        threaded_logger.Error($"Stuck at {run} with {(String.Join("; ", points))}, left {alive_count}.");
                                    }
                                }
                            }

                            var seconds = (Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency;

                            var iter_count = sucess_count + problems_count + not_found_count;

                            var message = $"{alive_count}/{workers_count} workers, try {iter_count} ({sucess_count}:{not_found_count})";
                            if (problems_count > 0)
                                message += $" PROBLEMS: {problems_count}";
                            message += $" in {DataFormat.Format(TimeSpan.FromSeconds(seconds))}/{seconds / (iter_count + 1)}";

                            threaded_logger.Verbose(message);

                            if (alive_count == 0)
                                return;

                            await Task.Delay(TimeSpan.FromMinutes(1)).ConfigureAwait(false);
                        }
                    },TaskCreationOptions.LongRunning).Unwrap().ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    ex.LogDetails(threaded_logger,MessageLevel.Error, "Program crashed");
                }
            }
        }
    }
}