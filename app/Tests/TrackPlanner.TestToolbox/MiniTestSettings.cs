using System.IO;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.TestToolbox
{

    public sealed record class MiniTestSettings
    {
        public bool ComputeReverseTurns { get; set; }
        public bool MirrorTurns { get; set; }
        public bool ReverseInput { get; set; }
        public bool UseFullMap { get; set; }
        public UserRouterPreferences RouterPreferences { get; set; }
        public FinderConfiguration? FinderConfiguration { get; set; }
        public Stream? MapStream { get; set; }
        public string? ExpectedProblem { get; set; }

        public MiniTestSettings()
        {
            ComputeReverseTurns = true;
            MirrorTurns = true;
            RouterPreferences = PrefsFactory.GetRouterPreferences();
        }

    }
}