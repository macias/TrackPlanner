using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.TestToolbox
{
    public readonly record struct MiniResult(
        RoutePlan<OsmId, OsmId> Route,
        IReadOnlyList<MapZPoint<OsmId,OsmId>> PlanPoints,
        IReadOnlyList<TurnInfo<OsmId, OsmId>> ForwardTurns,
        IReadOnlyList<TurnInfo<OsmId, OsmId>>? BackwardTurns)
    {
        public void Deconstruct(out IReadOnlyList<MapZPoint<OsmId,OsmId>> plan,
            out IReadOnlyList<TurnInfo<OsmId, OsmId>> forwardTurns)
        {
            plan = this.PlanPoints;
            forwardTurns = this.ForwardTurns;
        }
        public void Deconstruct(out RoutePlan<OsmId, OsmId> route,
            out IReadOnlyList<MapZPoint<OsmId,OsmId>> plan,
            out IReadOnlyList<TurnInfo<OsmId, OsmId>> forwardTurns)
        {
            route = this.Route;
            plan = this.PlanPoints;
            forwardTurns = this.ForwardTurns;
        }
    }

}