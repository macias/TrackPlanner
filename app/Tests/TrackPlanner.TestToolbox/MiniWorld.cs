using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Geo;
using MathUnit;
using TrackPlanner.Shared;
using SharpKml.Base;
using TrackPlanner.Backend;
using TrackPlanner.Turner;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.PathFinder;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.TestToolbox.Implementation;
using Xunit;
using NotSupportedException = System.NotSupportedException;

namespace TrackPlanner.TestToolbox
{
    public  class MiniWorld : MiniWorld<WorldIdentifier, WorldIdentifier>
    {
        protected bool Manual { get; }
        protected ApproximateCalculator Calculator { get; }
        protected static bool TrueVariant => true;
        
        protected MiniWorld(ILogger? logger,bool manual) : base(logger)
        {
            this.Manual = manual;
            this.Calculator = new ApproximateCalculator();
        }

        public static MemoryStream MiniMapToStream(string filename)
        {
            return new MemoryStream(System.IO.File.ReadAllBytes(
                System.IO.Path.Combine(Navigator.GetMiniMaps(), filename)));
        }

        
        protected MiniTestSettings CreateDebugSettings(bool reverseInput = false)
        {
            var settings = new MiniTestSettings()
            {
                ComputeReverseTurns = false,
                FinderConfiguration = GetFinderPreferences(),
            };
            settings.FinderConfiguration.EnableDebugDumping = Manual;
            settings.ReverseInput = reverseInput;
            return settings;
        }

[Obsolete]
        public void ExtractMiniMapFromPointsToFile(string filename, 
            TrackPlanner.Shared.Data.Region? boundary,
            Length? extractionRange = null, params GeoPoint[] points)
        {
            new MiniExtractor(Logger).ExtractMiniMapFromDataToFile(filename,strict:false,boundary,
                extractionRange,points);
        }

    }
    
    public  class MiniWorld<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        protected const int Precision = 15;

        public static Navigator Navigator { get; } = new Navigator();
        private readonly MapMode mapMode;
        private readonly ProxySerializer serializer;
        protected ILogger Logger { get; } 

        public MiniWorld(ILogger? logger)
        {
            this.Logger = logger ?? new NoLogger();
            this.mapMode = WorldMapExtension.GetMapMode<TNodeId, TRoadId>();
            this.serializer = new ProxySerializer();
        }

       
        protected UserRouterPreferences ReadRouterPreferences(MiniTestSettings settings)
        {
            // we use serialization/deserialization in order to find quicker
            // there is something wrong in client/server setup
            return this.serializer.Deserialize<UserRouterPreferences>(
                serializer.Serialize(settings.RouterPreferences))!;
        }
        protected FinderConfiguration readFinderConfiguration(MiniTestSettings settings)
        {
            return this.serializer.Deserialize<FinderConfiguration>(
                serializer.Serialize(settings.FinderConfiguration))!;
        }
        protected FinderConfiguration readFinderConfiguration(FinderConfiguration settings)
        {
            return this.serializer.Deserialize<FinderConfiguration>(
                serializer.Serialize(settings))!;
        }

        protected void AssertTrackIndex<TNId, TRId>(int expected, IReadOnlyList<TurnInfo<TNId, TRId>> turns, Index turnIndex)
            where TNId : struct
            where TRId : struct
        {
           // if (DateTimeOffset.UtcNow >= DateTimeOffset.Parse("2022-12-27"))
             //   throw new Exception("REMOVE ME");
            Assert.Equal(expected, turns[turnIndex].TrackIndex);
        }

        public ScheduleJourney<TNodeId, TRoadId> CreateSchedule(params GeoPoint[] points)
        {
            var schedule = new ScheduleJourney<TNodeId, TRoadId>()
            {
                Settings = ScheduleSettings.Defaults(),
            };
            schedule.Settings.LoopRoute = false;
            schedule.Settings.RouterPreferences = PrefsFactory.GetRouterPreferences();

            var day = new ScheduleDay()
            {
                Anchors = points.Select(pt => new ScheduleAnchor()
                {
                    UserPoint = pt,
                    IsPinned = true
                }).ToList(),
            };
            schedule.Days.Add(day);
            return schedule;
        }

        protected static FinderConfiguration GetFinderPreferences()
        {
            return PrefsFactory.GetFinderPreferences<TNodeId,TRoadId>();
        }

        protected static void SaveRoute(RoutePlan<OsmId,OsmId> plan, string mapFilename)
        {
            var input = new TrackWriterInput() {Title = null};
            int index = -1;
            Length? prev = null;
            foreach (var step in plan.Legs.SelectMany(it => it.AllSteps()))
            {
                ++index;
                string alt_diff;
                if (prev == null)
                    alt_diff = $"::{DataFormat.FormatHeight(step.Point.Altitude, false)}";
                else
                { 
                    if (prev==step.Point.Altitude)
                        alt_diff = $"={DataFormat.FormatHeight(Length.Zero, false)}";
                    else if (prev>step.Point.Altitude)
                        alt_diff = $"v{DataFormat.FormatHeight(prev.Value-step.Point.Altitude, false)}";
                    else
                        alt_diff = $"^{DataFormat.FormatHeight(step.Point.Altitude-prev.Value, false)}";
                }
                
                input.AddPoint(step.Point, $"[{index}] #{step.NodeId}", comment: $">{DataFormat.FormatDistance(step.IncomingFlatDistance,withUnit:false)} {alt_diff}", PointIcon.DotIcon);
                prev = step.Point.Altitude;
            }

            saveToKml(input,mapFilename);
        }

        protected static void SaveRoute(VisualSchedule<TNodeId,TRoadId> schedule,string mapFilename)
        {
            var route_title = "test-route-" + System.IO.Path.GetFileName(mapFilename);
            using (var stream = new FileStream(Navigator.GetUniquePath(Navigator.GetOutputDirectory(),
                           route_title),
                       FileMode.CreateNew, FileAccess.Write))
            {
                TrackWriter.SaveAsKml(UserVisualPreferences.Defaults(), 
                    stream, route_title,
                    schedule.GetSummary().Days.SelectMany(it => it.Checkpoints),
                    schedule.Route.Legs,
                    null);
            }
        }
        
        protected static void SaveData<TMapPoint>(IEnumerable<TMapPoint> plan, string mapFilename) 
        where TMapPoint:IMapZPoint
        {
            var input = new TrackWriterInput() {Title = null};
            int index = -1;
            foreach (var place in plan)
            {
                ++index;
                input.AddPoint(place.Point, $"[{index}] {place}", comment: null, PointIcon.DotIcon);
            }
            input.AddLine(plan.Select(it => it.Point));

            saveToKml(input,mapFilename);
        }

        private static void saveToKml(TrackWriterInput input,string mapFilename)
        {
            var kml = input.BuildDecoratedKml();

            using (var stream = new FileStream(Navigator.GetUniquePath(Navigator.GetOutputDirectory(), 
                           "test-" + System.IO.Path.GetFileName(mapFilename)),
                       FileMode.CreateNew, FileAccess.Write))
            {
                kml.Save(stream);
            }
        }

        protected static void SaveData<TPlace>(IEnumerable<TPlace> plan, 
            IEnumerable<TurnInfo<OsmId,OsmId>> turns, string mapFilename)
        where TPlace:IMapZPoint
        {
            var input = new TrackWriterInput() {Title = null};
            input.AddLine(plan.Select(it => it.Point).ToArray(), name: null, 
                KmlLineDecoration.ThinBlack);
            foreach (var (place,index) in plan.ZipIndex())
            {
                string? comment = null;
#if DEBUG
                if (place is MapZPoint<OsmId,OsmId> mp)
                    comment = mp.Description;
#endif
                input.AddPoint(place.Point, $"[{index}] {place}", comment:comment,
                    PointIcon.DotIcon);
            }
            input.AddTurns(turns, PointIcon.CircleIcon);

            saveToKml(input,mapFilename);
        }

        private static WorldMapMemory loadMiniMap(ILogger logger, Stream stream,
            int gridCellSize)
        {
            var kml_track = TrackReader.ReadUnstyled(stream);

            var nodes = HashMap.Create<OsmId, (GeoZPoint point,string? description)> ();
            // coords2d -> node id
            // this is typical case, single node per point
            var rev_nodes_simple = HashMap.Create<GeoPoint, OsmId>();
            // (coords2d + road id) -> node id 
            // in case of bridges and alike we have multiple nodes per point, so when reading
            // road we need to disambiguate the data, we do it by adding a road id to coords
            // this has to define single node (unless OSM has notion of spiral roads)
            var rev_nodes_complex = HashMap.Create<(GeoPoint,OsmId), OsmId>();
            var roads = HashMap.Create<OsmId, RoadInfo<OsmId>>();
            // (footcycle) node id -> dangerous road id
            var dangerous = new Dictionary<OsmId, OsmId>();

            foreach (var waypoint in kml_track.Waypoints)
            {
                OsmId? dangerous_wpt = null;
                OsmId[]? road_ids = null;
                string? node_details = null;
                {
                    var parts = (waypoint.Description ?? "")
                        .Split(MapDataInfo.SectionSeparator)
                        .Select(it => it.Trim())
                        .ToList();
                    int idx = 0;
                    if (parts[idx] != "")
                    {
                        dangerous_wpt = OsmId.Parse(parts[idx]);
                    }
                    ++idx;

                    if (idx < parts.Count && parts[idx] != "")
                    {
                        road_ids = parts[idx].Split(MapDataInfo.ValueSeparator)
                            .Select(it => it.Trim())
                            .Select(OsmId.Parse)
                            .ToArray();
                    }
                    ++idx;

                    if (idx < parts.Count)
                    {
                        node_details = parts[idx];
                    }
                }

                var node_id = OsmId.Parse(waypoint.Name!);
                nodes.Add(node_id, (waypoint.Point,node_details));
                if (road_ids==null)
                    rev_nodes_simple.Add(waypoint.Point.Convert2d(),node_id);
                else
                {
                    foreach (var road_id in road_ids)
                    {
                        if (!rev_nodes_complex.TryAdd((waypoint.Point.Convert2d(), road_id), node_id, out var existing))
                        {
                            throw new ArgumentException($"Cannot add node {node_id} at {waypoint.Point.Convert2d()} with road {road_id}, {existing} node already exists.");
                        }
                    }
                }

                if (dangerous_wpt is {} d)
                    dangerous.Add(node_id,d);
            }

            // when binding roads to nodes we don't use elevation on purpose, but in future
            // we should use ROAD elevation as indicator which node we should use (0, 1, 2)
            // in case of bridges etc.
            foreach (var line in kml_track.Lines)
            {
                var road_id = OsmId.Parse(line.Name!);
                var info = RoadInfo<OsmId>.Parse(line.Points.Select(it =>
                {
                    OsmId node_id;
                    var pt = it.Convert2d();
                    if (rev_nodes_simple.TryGetValue(pt, out  node_id))
                        return node_id;
                    else if (rev_nodes_complex.TryGetValue((pt, road_id), out node_id))
                        return node_id;
                    throw new System.ArgumentException($"Cannot read road {road_id} because of missing node at {pt}");
                }).ToList(), line.Description!);
                roads.Add(road_id, info);
            }

            var node_to_roads_dict = NodeRoadsDictionary.Create(nodes, roads);
            var world_map = WorldMapMemory.CreateOnlyRoads(logger,                 nodes.ToHashMap(it => it.Key,
                    it => NodeInfo<OsmId>.Parse(it.Value.point,node_to_roads_dict[it.Key],it.Value.description)), 
                roads, 
                new List<(OsmId, PointOfInterest)>(),
                new List<(OsmId, CityInfo)>(),
                gridCellSize,Navigator.GetDebugDirectory());
            world_map.SetExpressNearby(dangerous);
            return world_map;
        }

        private static long parseId(string it)
        {
            return long.Parse(it, CultureInfo.InvariantCulture);
        }

        private IDisposable computePlaces(MiniTestSettings  miniTestSettings,
            string mapTitleSource, 
            Stream? stream,
            out RouteManager<TNodeId,TRoadId> manager, 
            out RouteResponse<TNodeId,TRoadId> routeResponse,
            out IReadOnlyList< FlowJump<TNodeId,TRoadId>> steps, 
            params RequestPoint<TNodeId>[] userPoints)
        {
            if (userPoints.Length < 2)
                throw new ArgumentOutOfRangeException($"{nameof(userPoints)}.Count = {userPoints.Length}");

            IDisposable result = CreateManager(miniTestSettings.FinderConfiguration, 
                mapTitleSource, stream,
                mapFolder:null, out manager);
            if (miniTestSettings.ReverseInput)
                userPoints = userPoints.Reverse().ToArray();

            var worker = new RealWorker<TNodeId, TRoadId>(Logger, manager);
            PlanRequest<TNodeId> request = PrefsFactory.PointsToRequest(ReadRouterPreferences(miniTestSettings), userPoints);
            Result<RouteResponse<TNodeId,TRoadId>> res_plan = worker.ComputeTrack(request,true,CancellationToken.None, 
                out var debug_places);
            if (!res_plan.HasValue)
                throw new Exception(res_plan.ProblemMessage.TryShortMessage()); 

            routeResponse = res_plan.Value;
            steps = debug_places!.SelectMany(it => it).ToList();
            

            if (res_plan.ProblemMessage is { } message && message.Message!=miniTestSettings.ExpectedProblem)
            {
                SaveData(convertToOsm(manager.Map, steps.Select(it => it.Place)), mapTitleSource);
                throw new Exception(message.FullMessage());
            }

            return result;
        }

        public IDisposable CreateManager(FinderConfiguration? finderConfiguration,
            string? mapTitleSource, 
            Stream? stream, string? mapFolder, out RouteManager<TNodeId, TRoadId> manager)
        {
            IDisposable result;
            if (stream == null)
                result = RouteManager<TNodeId, TRoadId>.Create(Logger, Navigator,
                    mapFolder: mapFolder?? "kujawsko-pomorskie",
                    readFinderConfiguration(finderConfiguration?? GetFinderPreferences()), out manager);
            else
                result = prepareManager(finderConfiguration, mapTitleSource!, stream, out manager);
            return result;
        }

        protected static RequestPoint<TNodeId> CreateRequestPoint(GeoPoint it)
        {
            return new RequestPoint<TNodeId>(it, allowSmoothing:true,false);
        }

        private async ValueTask processScheduleAsync( string mapTitleSource, Stream stream,
            ScheduleJourney<TNodeId,TRoadId> schedule)
        {
            using (prepareManager(new MiniTestSettings().FinderConfiguration, mapTitleSource, stream, out var manager))
            {
                var worker = new RealWorker<TNodeId, TRoadId>(Logger, manager);
                Result<RouteResponse<TNodeId,TRoadId>> res_plan = worker.ComputeTrack(schedule.BuildPlanRequest<TNodeId>(),true,CancellationToken.None,
                    out _);
                if (!res_plan.HasValue)
                    throw new Exception(res_plan.ProblemMessage.TryFullMessage() ?? "No route found.");

                schedule.Route = res_plan.Value.Route;
                await schedule.RefreshAnchorLabelsAsync(0,res_plan.Value.Names).ConfigureAwait(false);
            }
        }

        private IDisposable prepareManager(FinderConfiguration? finderConfiguration, string mapTitleSource,
            Stream stream, 
            out RouteManager<TNodeId, TRoadId> manager)
        {
            var finder_config = readFinderConfiguration(finderConfiguration ?? GetFinderPreferences());
            
            var result = CompositeDisposable.None;

            IWorldMap<TNodeId, TRoadId> mini_map;
            {
                var mem_map = loadMiniMap(Logger, stream, finder_config.MemoryParams.GridCellSize);

                if (this.mapMode != MapMode.TrueDisk)
                    throw new ArgumentException();

                result = CompositeDisposable.Stack(result, createTrueMap(Logger, mem_map, 
                    titleSource: mapTitleSource,
                    finder_config.MemoryParams, out var disk_map));
                mini_map = HACK.CastMap<TNodeId, TRoadId>(disk_map);
            }

            manager = RouteManager<TNodeId, TRoadId>.Create(Logger, Navigator, mini_map,
                finder_config);
            return result;
        }

        private IDisposable createTrueMap(ILogger logger, WorldMapMemory memMap, string titleSource,
            MemorySettings memorySettings,  out TrueGridWorldMap trueMap)
        {
            var source_nodes_trans = new IdTranslationTable(capacity:memMap.GetAllNodes().Count());
            var source_roads_trans = new IdTranslationTable(capacity:memMap.GetAllRoads().Count());
            var true_source = TrueGridWorldMap.Create(logger,  memMap,new MiniElevationMap(
                    memMap.GetAllNodes().Select(it => it.Value.Point)),
                source_nodes_trans, source_roads_trans, Navigator.GetDebugDirectory());
            Stream map_stream = new MemoryStream();
            var result = new CompositeDisposable(map_stream);
            using (Stream nodes_stream = new MemoryStream())
            {
                using (Stream roads_stream = new MemoryStream())
                {
                    TrueGridWorldMap.WriteTranslationTable(nodes_stream,source_nodes_trans);
                    nodes_stream.Position = 0;
                    TrueGridWorldMap.WriteTranslationTable(roads_stream,source_roads_trans);
                    roads_stream.Position = 0;
                    
                    true_source.WriteMap( map_stream);
                    map_stream.Position = 0;

                    var nodes_table = TrueGridWorldMap.ReadIdTranslationTable(nodes_stream);
                    var roads_table = TrueGridWorldMap.ReadIdTranslationTable(roads_stream);
                    
                    result = result.Stack(TrueGridWorldMap.ReadMap(logger, 
                        new[] {(stream: map_stream, fileName: titleSource)}.ToArray(),
                        memorySettings,
                            nodes_table,roads_table,
                        Navigator.GetDebugDirectory(),
                        out trueMap, out var invalid_files));
                    if (invalid_files.Any())
                        throw new NotSupportedException();
                    return result;
                }
            }
        }

        protected IReadOnlyList<MapZPoint<OsmId,OsmId>> ComputeRoute(string filename, 
            params GeoPoint[] userPoints)
        {
            return ComputeRoute(new MiniTestSettings(), filename, userPoints);
        }
        protected IReadOnlyList<MapZPoint<OsmId,OsmId>> ComputeRoute(MiniTestSettings miniTestSettings, string filename, 
            params GeoPoint[] userPoints)
        {
            using (var stream = new MemoryStream(System.IO.File.ReadAllBytes(
                       System.IO.Path.Combine(Navigator.GetMiniMaps(), filename))))
            using (computePlaces(miniTestSettings, filename, stream, out var manager, out _,out var steps,
                       userPoints.Select(it => CreateRequestPoint(it)).ToArray()))
            {
                return convertToOsm(manager.Map, steps.Select(it => it.Place));
            }
        }
        protected async ValueTask ProcessScheduleAsync(string filename, ScheduleJourney<TNodeId,TRoadId> schedule)
        {
            using (var stream = new MemoryStream(System.IO.File.ReadAllBytes(
                       System.IO.Path.Combine(Navigator.GetMiniMaps(), filename))))
            {
                await processScheduleAsync(filename, stream, schedule).ConfigureAwait(false);
            }
        }

        private static List<MapZPoint<OsmId,OsmId>> convertToOsm(IWorldMap<TNodeId, TRoadId> map, 
            IEnumerable<Placement<TNodeId, TRoadId>> places)
        {
            return places.Select(p => new MapZPoint<OsmId,OsmId>(p.Point, 
                p.IsNode ? map.GetOsmNodeId(p.NodeId) : null
#if DEBUG
                ,DEBUG_mapRef:map.ConvertToOsm( MapRef.Create(map,  p.GetBaseRoadIndex()))
                ,p.KindInfo
                #endif
                )
            ).ToList();
        }
        
        protected MiniResult ComputeTurns(
            string filename, params GeoPoint[] userPoints)
        {
            return ComputeTurns( CreateTestSettings(), filename, userPoints);
        }

        protected virtual MiniTestSettings CreateTestSettings()
        {
            return new MiniTestSettings();
        }

        protected MiniResult ComputeTurns(
            MiniTestSettings miniTestSettings, string filename, params RequestPoint<TNodeId>[] userPoints)
        {
            Stream? stream = miniTestSettings.MapStream;
            if (!miniTestSettings.UseFullMap && stream==null)
                stream = MiniWorld.MiniMapToStream(filename);
            using (stream)
            {
                return ComputeTurns(miniTestSettings, filename, stream, userPoints);
            }
        }

        protected MiniResult ComputeTurns(
            MiniTestSettings miniTestSettings,string filename,  params GeoPoint[] userPoints)
        {
            return ComputeTurns(miniTestSettings, filename,CreateRequestPoints(userPoints));
        }

        protected static RequestPoint<TNodeId>[] CreateRequestPoints(params GeoPoint[] userPoints)
        {
            return userPoints.Select(CreateRequestPoint).ToArray();
        }

        protected MiniResult ComputeTurns(
            MiniTestSettings miniTestSettings,
            string title, Stream? stream, params RequestPoint<TNodeId>[] userPoints)
        {
            using (computePlaces(miniTestSettings, title, stream, out var manager,
                       out var response, 
                       out var plan_nodes, userPoints))
            {
                List<TurnInfo<OsmId, OsmId>> regular = manager.Map
                    .TurnsToOsm(response.Route.DailyTurns.SelectMany(it => it)).ToList();

                IReadOnlyList<TurnInfo<OsmId, OsmId>>? reversed;

                if (!miniTestSettings.ComputeReverseTurns)
                    reversed = null;
                else
                {
                    var turner_preferences = new UserTurnerPreferences();

                    var turner = new NodeTurnWorker<TNodeId, TRoadId>(Logger, manager.Map,
                        new SystemTurnerConfig() {DebugDirectory = manager.DebugDirectory},
                        turner_preferences);

                    // checking reversal as well
                    reversed = computeTurnPoints($"REV-{title}",
                            turner, FinderHelper.ReverseJumps( plan_nodes))
                        .AsEnumerable()
                        .Reverse()
                        // reversing internal data
                        // quality ignores  track index and we couldn't simply mirror it because internally some of the points can be initially removed
                        .Select(it => new TurnInfo<OsmId, OsmId>(it.Entity, it.AtNode() ? it.NodeId : null,
                            it.AtRoundabout() ? it.RoadId : null, it.Point, trackIndex: -1,
                            it.RoundaboutCounter, it.Backward, it.Forward, reason: it.Reason))
                        .ToList();

                    if (miniTestSettings.MirrorTurns)
                    {
                        Assert.Equal(expected: regular.Count, actual: reversed.Count);
                        Assert.Equal(expected: regular.Count(it => it.RoundaboutCounter.HasValue),
                            actual: reversed.Count(it => it.RoundaboutCounter.HasValue));
                        foreach (var (reg, rev) in regular.Zip(reversed, (reg, rev) => (reg, rev)))
                        {
                            Assert.Equal(reg.Point.Latitude.Degrees, rev.Point.Latitude.Degrees, Precision);
                            Assert.Equal(reg.Point.Longitude.Degrees, rev.Point.Longitude.Degrees, Precision);
                            //    Assert.Equal(reg.RoundaboutCounter, rev.RoundaboutCounter);
                            Assert.Equal(reg.Forward, rev.Forward);
                            Assert.Equal(reg.Backward, rev.Backward);
                        }
                    }
                }
                
                return new MiniResult (
                    MapTranslator.ConvertToOsm(manager.Map, response.Route),
                    convertToOsm(manager.Map, plan_nodes.Select(it => it.Place)),
                    regular,reversed);
            }
        }

        private static List<TurnInfo<OsmId,OsmId>> computeTurnPoints(string caseMapFilename,
            NodeTurnWorker<TNodeId,TRoadId> turner, 
            IReadOnlyList<FlowJump<TNodeId,TRoadId>> plan)
        {
            var turns = turner.ComputeTurnPoints( plan, CancellationToken.None);
            if (turns.ProblemMessage != null)
            {
                SaveData(convertToOsm(turner.Map, plan.Select(it => it.Place)),caseMapFilename);
                throw new Exception(turns.ProblemMessage.EnsureFullMessage());
            }

            return turner.Map.TurnsToOsm( turns.Value).ToList();
        }
    }
}