using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Backend.Stored;
using TrackPlanner.PathFinder;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.RestClient;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.TestToolbox
{
    public class LocalPlanClient<TNodeId,TRoadId> :  PlanClient<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        public static LocalPlanClient<TNodeId, TRoadId> Create(ILogger logger, 
            RouteManager<TNodeId, TRoadId>? manager, 
            DraftOptions draftRealOptionses,
            bool alwaysReal)
        {
            var finder_config = FinderConfiguration.Defaults();

            IWorker<TNodeId, TRoadId> real_worker;

            if (manager==null)
            {
                real_worker = new DummyWorker<TNodeId, TRoadId>(logger, finder_config,draftRealOptionses);
            }
            else
            {
                real_worker = new RealWorker<TNodeId, TRoadId>(logger, manager);
            }

            return new LocalPlanClient<TNodeId, TRoadId>(logger,realWorker: real_worker,
                draftWorker:new DummyWorker<TNodeId, TRoadId>(logger, finder_config,
                    DraftOptions.GetLegacyTests()),
                alwaysReal);
        }

        private readonly bool alwaysReal;
        private readonly IWorker<TNodeId, TRoadId> realWorker;
        private readonly IWorker<TNodeId, TRoadId> draftWorker;
        private readonly StorageManager<TNodeId,TRoadId> storageManager;
        private readonly MemoryStreamProvider memoryStreamProvider;

        public IEnumerable<string> ProjectFiles => this.memoryStreamProvider.ProjectFiles;
        
        public string? GetMockPlanFailureReason { get; set; }
        
        public Func<PlanRequest<TNodeId>,string?>? GetRealPlanFailurePredicate { get; set; }

        private LocalPlanClient(ILogger logger,
            IWorker<TNodeId, TRoadId> realWorker, 
            IWorker<TNodeId, TRoadId> draftWorker, 
            bool alwaysReal)
        {

            // this flag is for some initial visual tests which assumed draft routes
            // will be split into segments right away during the tests. This is not in sync
            // how program really works, so to avoid deleting them, special flag was added
            this.alwaysReal = alwaysReal;
              // it does not have to be true-real worker, only the worker for actual computation
              // which could have be mock
            this.realWorker = realWorker;
            this.draftWorker = draftWorker;
            this.memoryStreamProvider = new MemoryStreamProvider();
            this.storageManager = new StorageManager<TNodeId, TRoadId>(this.memoryStreamProvider, 
                RestServiceConfig.Defaults());
        }

        public override async ValueTask<Result<RouteResponse<TNodeId, TRoadId>>> GetPlanAsync(PlanRequest<TNodeId> request,
            bool calcReal, CancellationToken token)
        {
            await ValueTask.CompletedTask.ConfigureAwait(false);

            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            if (calcReal && GetRealPlanFailurePredicate?.Invoke(request) is { } message)
                return Result<RouteResponse<TNodeId, TRoadId>>.Fail(message);
            else if (!calcReal && GetMockPlanFailureReason != null)
                return Result<RouteResponse<TNodeId, TRoadId>>.Fail(GetMockPlanFailureReason );

            var worker = calcReal ? this.realWorker : draftWorker;
            Result<RouteResponse<TNodeId,TRoadId>> plan = worker
                .ComputeTrack(request, this.alwaysReal || calcReal, token, out _);

            return plan;
        }

        protected override ValueTask<Result<ValueTuple>> DoSaveScheduleAsync(ScheduleJourney<TNodeId, TRoadId> schedule, 
            string path, CancellationToken token)
        {
            this.storageManager.SaveAllSchedule(schedule,
                stores:null, 
                legCities:null, 
                path);
            return ValueTask.FromResult(Result<ValueTuple>.Valid(new ValueTuple()));
        }

        protected override ValueTask<Result<ScheduleJourney<TNodeId, TRoadId>>> DoLoadScheduleAsync(string path, CancellationToken token)
        {
            if (!this.ProjectFiles.Contains(path))
                return ValueTask.FromResult(Result<ScheduleJourney<TNodeId, TRoadId>>.Fail($"File {path} does not exist."));
            else if (!this.storageManager.TryLoadSchedule(path,out var schedule))
                return ValueTask.FromResult(Result<ScheduleJourney<TNodeId, TRoadId>>.Fail($"Corrupted file {path}."));
            else
                return ValueTask.FromResult(Result<ScheduleJourney<TNodeId, TRoadId>>.Valid(schedule));
        }

        public override ValueTask<Result<DirectoryData>> GetScheduleDirectoryAsync(string directory, CancellationToken token)
        {
            return ValueTask.FromResult(Result<DirectoryData>.Valid(
                this.memoryStreamProvider.GetDirectoryEntries(directory)));
        }

        public override ValueTask<Result<DirectoryData>> GetLayersDirectoryAsync(string? directory, CancellationToken token)
        {
            return ValueTask.FromResult<Result<DirectoryData>>(Result.Valid(new DirectoryData()
            {
                Files = new string[] { },
                Directories = new string[] { }
            }));
        }

        public override ValueTask<Result<AttractionsResponse>> FindAttractionsAsync(InterestRequest request, CancellationToken token)
        {
            var response = new AttractionsResponse()
            {
                Attractions = this.realWorker.GetAttractions(request.CheckPoints, 
                    request.Range, request.ExcludeFeatures).ToList()
            };
           
            return ValueTask.FromResult(Result<AttractionsResponse>.Valid(response));
        }
        
        public override ValueTask<Result<PeaksResponse<TNodeId, TRoadId>>> GetPeaksAsync(PeaksRequest request, CancellationToken token)
        {
            return ValueTask.FromResult(Result.Valid(new PeaksResponse<TNodeId, TRoadId>()
                {
                    BaseHeight = Length.Zero,
                    Peaks = Array.Empty<MapZPoint<TNodeId, TRoadId>>(),
                },
                "No peaks", MessageLevel.Info));
        }

    }
}