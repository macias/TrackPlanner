using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Backend;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.TestToolbox
{
    public sealed class MemoryStreamProvider : IStorageProvider
    {
        private readonly Dictionary<string, MemoryStream> files;
        public IEnumerable<string> ProjectFiles => this.files.Keys
            .Where(it => System.IO.Path.GetExtension(it)==SystemCommons.ProjectFileExtension);

        public MemoryStreamProvider()
        {
            this.files = new Dictionary<string, MemoryStream>();
        }
        
        public DirectoryData GetDirectoryEntries(string? directory)
        {
            if (string.IsNullOrEmpty(directory))
                return new DirectoryData() {Directories = new string[] { }, Files = ProjectFiles.ToArray()};
            else
                throw new ArgumentException($"Directory {directory} does not exist.");
        }
        

        public IDisposable CreateWrite(string descriptor, out Stream stream)
        {
            var mem_stream = new MemoryStream();
            this.files[descriptor] = mem_stream;
            stream = mem_stream;
            return CompositeDisposable.None;
        }

        public IDisposable CreateRead(string descriptor, out Stream stream)
        {
            var mem_stream = this.files[descriptor];
            mem_stream.Position = 0;
            stream = mem_stream;
            return CompositeDisposable.None;
        }

        public bool Delete(string descriptor)
        {
            return this.files.Remove(descriptor);
        }
    }
}