using TrackPlanner.Shared;

namespace TrackPlanner.TestToolbox
{
    public sealed class ThreadedLogger : ILogger
    {
        private readonly ILogger inner;
        private readonly bool isEnabled;
        private readonly object threadLock;

        public ThreadedLogger(ILogger inner,SinkSettings sinkSettings) : this(inner,sinkSettings,new object())
        {
        }
        private ThreadedLogger(ILogger inner,SinkSettings sinkSettings, object threadLock)
        {
            this.inner = inner;
            this.isEnabled = sinkSettings.IsEnabled;
            this.threadLock = threadLock;
        }
        public void Log(MessageLevel level, string? message)
        {
            if (!this.isEnabled)
                return;
            
            // we cannot lock on inner logger because it could be changed via setting new settings
            lock (this.threadLock)  
            {
                this.inner.Log(level,message);
            }
        }

        public ILogger With(SinkSettings settings)
        {
            return new ThreadedLogger(inner.With(settings),settings, this.threadLock);
        }
    }
}