using System;
using Geo;
using TrackPlanner.RestClient;

namespace TrackPlanner.TestToolbox
{
  
    public sealed class TinyCoordRandomizer : CoordRandomizer
    {
        public static CoordRandomizer Instance { get; } = new TinyCoordRandomizer();

        // suitable for real router
        public override GeoPoint RandomizePoint(Random rnd)
        {
            double north = 53.3574;
            double south = 53.11322;
            double west = 18.42096;
            double east = 18.8881;

            return GeoPoint.FromDegrees(randomDouble(rnd, south, north),
                randomDouble(rnd, east, west));
        }
    }
}