using System;
using TrackPlanner.Shared;

namespace TrackPlanner.TestToolbox
{
    public sealed class NoLogger : ILogger
    {
        public NoLogger()
        {
            Console.WriteLine("Logging is DISABLED");
        }
        
        public void Log(MessageLevel level,string? message)
        {
         //   Console.WriteLine($"[{level}] {message}");
        }

        public ILogger With(SinkSettings settings)
        {
            return this;
        }
    }
}