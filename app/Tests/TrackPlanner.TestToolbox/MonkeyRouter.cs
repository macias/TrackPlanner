using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Mapping.Data;
using TrackPlanner.PathFinder;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TimeSpan = System.TimeSpan;

namespace TrackPlanner.TestToolbox
{
    public sealed class MonkeyRouter<TNodeId, TRoadId> : IDisposable
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private static readonly Navigator navigator = new Navigator();

        private readonly Random rnd;
        private readonly ILogger mainLogger;
        private readonly List<IDisposable> disposables;
        private readonly RouteManager<TNodeId, TRoadId> manager;
        private readonly UserRouterPreferences user_configuration;
        private readonly RealWorker<TNodeId, TRoadId> worker;
        private readonly ILogger compLogger;
        private readonly ILogger progressLogger;

        public MonkeyRouter(ILogger mainLogger, SinkSettings? compSinkSettings = null)
        {
            this.disposables = new List<IDisposable>();
            this.mainLogger = mainLogger;
            this.rnd = new Random();

            //   this.user_configuration = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds();
            // user_configuration.CompactingDistanceDeviation = Length.Zero;

            this.user_configuration = PrefsFactory.GetRouterPreferences();

            FinderConfiguration finder_config = PrefsFactory.GetFinderPreferences<TNodeId, TRoadId>();
            {
            }

            finder_config.EnableDebugDumping = false;
            finder_config.FinalSnapProximityLimit = Length.FromKilometers(2);

            this.progressLogger = this.mainLogger.With(compSinkSettings ?? SinkSettings.CreateDisabled());
            this.compLogger = this.mainLogger.With(SinkSettings.CreateDisabled());
            this.disposables.Add(RouteManager<TNodeId, TRoadId>.Create(this.compLogger,
                navigator, mapFolder: "kujawsko-pomorskie",
                finder_config,
                out this.manager));

            this.worker = new RealWorker<TNodeId, TRoadId>(this.compLogger, manager);
        }


        public void Dispose()
        {
            foreach (var disp in this.disposables.AsEnumerable().Reverse())
                disp.Dispose();
        }

        public void ComputeMany(TimeSpan timeout, MonkeyStats<RequestPoint<TNodeId>> stats)
        {
            while (true)
            {
                for (int points_count = 3; points_count <= 4; ++points_count)
                {
                    var points = Enumerable.Range(0, points_count)
                        .Select(_ => BigCoordRandomizer.Instance.RandomizePoint(rnd))
                        .Select(it => CreateRequestPoint(it) with
                        {
                            EnforcePoint = this.rnd.Next(2) == 0
                        })
                        .ToList();

                    stats.Start(timeout, points.ToArray());
                    var outcome = compute(timeout, points, simplify: false);
                    stats.Complete(outcome);
                }
            }
        }

        public void Compute(TimeSpan timeout, bool simplify, params GeoPoint[] points)
        {
            compute(timeout, points.Select(it => CreateRequestPoint(it)).ToList(), simplify);
        }

        public void Simplify(TimeSpan timeout, IEnumerable<RequestPoint<TNodeId>> points)
        {
            double start = Stopwatch.GetTimestamp();
            compute(timeout, points.ToList(), simplify: true);
            var passed = TimeSpan.FromSeconds((Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency);
            this.mainLogger.Info($"Simplification done in {passed}");
        }

        public RequestPoint<TNodeId> CreateRequestPoint(GeoPoint point)
        {
            return new RequestPoint<TNodeId>(point, allowSmoothing: true, findLabel: false);
        }

        private RouteOutcome compute(TimeSpan timeout, List<RequestPoint<TNodeId>> points,
            bool simplify)
        {
            RouteOutcome total_outcome = RouteOutcome.Success;

            {
                while (true)
                {
                    var request = PrefsFactory.PointsToRequest(this.user_configuration,
                        points);

                    bool is_inner = false;

                    try
                    {
                        var outcome = computeRoute(request, timeout,
                            out List<List<FlowJump<TNodeId, TRoadId>>>? debug_steps);
                        if (outcome == RouteOutcome.Success)
                            return total_outcome;

                        total_outcome = outcome;

                        is_inner = true;

                        if (!simplify)
                            return total_outcome;

                        bool was_simplified = rawMinimizeScenario(ref points, debug_steps!, timeout);
                        if (was_simplified)
                        {
                            string message = $"Simplified, coords  {(String.Join("; ", points))}";
                            this.mainLogger.Info(message);
                        }

                        //if (!was_simplified)
                        return total_outcome;
                    }
                    catch (OperationCanceledException)
                    {
                        this.mainLogger.Error($"{(is_inner ? "Inner " : "")}Cancellation for {(String.Join("; ", points))}");

                        return RouteOutcome.Problems;
                    }
                    catch (Exception ex)
                    {
                        ex.LogDetails(this.mainLogger, MessageLevel.Error, $"{(is_inner ? "Inner " : "")}Exception for {(String.Join("; ", points))}");

                        return RouteOutcome.Problems;
                    }
                }
            }
        }

        private RouteOutcome computeRoute(PlanRequest<TNodeId> request,
            TimeSpan timeout,
            out List<List<FlowJump<TNodeId, TRoadId>>>? debug_steps)
        {
            using (var cts = new CancellationTokenSource(timeout))
            {
                var points_string = (String.Join("; ", request.DailyPoints.SelectMany(it => it)));
                try
                {
                    var response = this.worker.ComputeTrack(request, true, cts.Token, out debug_steps);
                    if (response.ProblemMessage.TryShortMessage()==RouteFinding.RouteNotFound.ToString())
                        return TestToolbox.RouteOutcome.NotFound;

                    if (response.HasValue && response.ProblemMessage == null)
                        return TestToolbox.RouteOutcome.Success;

                    this.mainLogger.Error($"Problem for {points_string}: {response.ProblemMessage.TryFullMessage()}");
                    return RouteOutcome.Problems;
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    ex.LogDetails(this.mainLogger, MessageLevel.Error, $"Exception for {points_string}");
                    debug_steps = null;

                    return RouteOutcome.Problems;
                }
            }
        }

        bool shorten_leg_by_points(ref List<RequestPoint<TNodeId>> base_points,
            IReadOnlyList<IReadOnlyList<FlowJump<TNodeId, TRoadId>>> baseRoute,
            Direction short_direction, TimeSpan timeout)
        {
            bool changed = false;
            // we can only shorten first leg (from the start) or last one (from the end)

            // DO NOT create variable for given leg, because it will be changed within the loop
            var leg_index = short_direction == Direction.Forward ? new Index(0) : new Index(1, fromEnd: true);

            int start_idx = short_direction == Direction.Forward ? 1 : 0;
            int end_idx = baseRoute[leg_index].Count - 1 - (short_direction == Direction.Backward ? 1 : 0);
            while (start_idx < end_idx)
            {
                int i = (start_idx + end_idx) / 2;
                {
                    var base_points_store = base_points.ToList();
                    base_points[leg_index] = CreateRequestPoint(baseRoute[leg_index][i].Place.Point.Convert2d());

                    try
                    {
                        if (computeRoute(PrefsFactory.PointsToRequest(this.user_configuration, base_points),
                                timeout, out _) == RouteOutcome.Problems)
                        {
                            this.progressLogger.Verbose($"Shortened at {i} point within {start_idx}:{end_idx}.");
                            // baseRoute = debug_places!;
                            changed = true;
                            if (short_direction == Direction.Forward)
                                start_idx = i + 1;
                            else
                                end_idx = i - 1;

                            //start_idx = short_direction==Direction.Forward?1:0;
                            //end_idx = baseRoute[leg_index].Count - 1-(short_direction== Direction.Backward?1:0);
                        }
                        else
                        {
                            base_points = base_points_store;
                            if (short_direction == Direction.Forward)
                                end_idx = i - 1;
                            else
                                start_idx = i + 1;
                        }
                    }
                    catch
                    {
                        base_points = base_points_store;
                        throw;
                    }
                }
            }

            return changed;
        }


        bool shorten_leg_by_distance(ref List<RequestPoint<TNodeId>> base_points,
            Direction short_direction, TimeSpan timeout)
        {
            bool changed = false;

            var start_index = short_direction == Direction.Forward ? new Index(0) : new Index(1, fromEnd: true);
            var adj_index = short_direction == Direction.Forward ? new Index(1) : new Index(2, fromEnd: true);

            while (true)
            {
                var start_pt = base_points[start_index].UserPoint3d;
                var adj_pt = base_points[adj_index].UserPoint3d;
                if (this.manager.Calculator.GetFlatDistance(start_pt.Convert2d(), adj_pt.Convert2d()) < Length.FromMeters(100))
                    break;
                var mid = this.manager.Calculator.GetMidPoint(start_pt, adj_pt);
                var base_points_store = base_points.ToList();
                base_points[start_index] = CreateRequestPoint(mid.Convert2d());

                try
                {
                    if (computeRoute(PrefsFactory.PointsToRequest(this.user_configuration, base_points),
                            timeout, out _) == RouteOutcome.Problems)
                    {
                        this.progressLogger.Verbose($"Shortened at mid point.");
                        // baseRoute = debug_places!;
                        changed = true;
                    }
                    else
                    {
                        base_points = base_points_store;
                        break;
                    }
                }
                catch
                {
                    base_points = base_points_store;
                    throw;
                }
            }

            return changed;
        }

        private bool rawMinimizeScenario(ref List<RequestPoint<TNodeId>> basePoints,
            IReadOnlyList<IReadOnlyList<FlowJump<TNodeId, TRoadId>>>? baseRoute,
            TimeSpan timeout)
        {
            // base points are altered here non-stop -- when the exception occurs we will have
            // the points for which exception was thrown as a nice side-effect

            bool changed = false;

            // reducing number of legs
            for (int i = 0; basePoints.Count > 2 && i < basePoints.Count;)
            {
                List<RequestPoint<TNodeId>> base_points_store = basePoints.ToList();
                basePoints.RemoveAt(i);

                try
                {
                    if (computeRoute(PrefsFactory.PointsToRequest(this.user_configuration, basePoints),
                            timeout, out var debug_places) == RouteOutcome.Problems)
                    {
                        baseRoute = debug_places;
                        changed = true;
                        i = 0;
                        this.progressLogger.Verbose($"Reduced to {basePoints.Count} points.");
                    }
                    else
                    {
                        basePoints = base_points_store;
                        ++i;
                    }
                }
                catch
                {
                    basePoints = base_points_store;
                    throw;
                }
            }


            if (baseRoute == null)
            {
                if (shorten_leg_by_distance(ref basePoints, Direction.Forward, timeout))
                {
                    changed = true;
                }

                if (shorten_leg_by_distance(ref basePoints, Direction.Backward, timeout))
                {
                    changed = true;
                }
            }
            else
            {
                if (shorten_leg_by_points(ref basePoints, baseRoute, Direction.Forward, timeout))
                {
                    changed = true;
                }

                if (shorten_leg_by_points(ref basePoints, baseRoute, Direction.Backward, timeout))
                {
                    changed = true;
                }
            }

            return changed;
        }
    }
}