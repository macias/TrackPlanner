using System.Collections.Generic;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Elevation;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.TestToolbox.Implementation
{
    internal sealed class MiniElevationMap : IElevationMap
    {
        private readonly Dictionary<GeoPoint,Length> data;

        public MiniElevationMap(IEnumerable<GeoZPoint> points)
        {
            this.data = points.ToDictionary(it => it.Convert2d(), it => it.Altitude);
        }
        public bool TryGetHeight(GeoPoint point, out Length height)
        {
            return this.data.TryGetValue(point, out height);
        }
    }
}