using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Mapping.Data;
using TrackPlanner.PathFinder;
using TrackPlanner.RestClient;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TimeSpan = System.TimeSpan;

namespace TrackPlanner.TestToolbox
{
    public enum RouteOutcome
    {
        Success,
        Problems,
        NotFound
    }
}