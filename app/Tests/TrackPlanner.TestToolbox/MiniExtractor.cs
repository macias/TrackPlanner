using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.TestToolbox
{
    public sealed class MiniExtractor : MiniExtractor<WorldIdentifier, WorldIdentifier>
    {
        public MiniExtractor(ILogger? logger = null) : base(logger)
        {
        }

       
        public void ExtractMiniMapFromBoundaryToFile(string filename,
            TrackPlanner.Shared.Data.Region? boundary,bool strict = true)
        {
            ExtractMiniMapFromDataToFile(filename,strict, boundary);
        }
        
        public void ExtractMiniMapFromSourceToFile(string filename,
            Length? extractionRange = null, bool strict = true)
        {
            var track = TrackReader.ReadUnstyled(
                System.IO.Path.Combine(Navigator.GetSourceTracks(), 
                    System.IO.Path.ChangeExtension(filename,".source.kml") ));
            GeoPoint[] pointed_lines = track.Lines
                .SelectMany(it => it.Points.Select(pt => pt.Convert2d()))
                .ToArray();

            ExtractMiniMapFromDataToFile(filename,strict,
                null,extractionRange ?? Length.FromMeters(50), pointed_lines);
        }
        public void ExtractMiniMapFromPointsToFile(string filename,
            Length? extractionRange, bool strict, params GeoPoint[] points)
        {
            ExtractMiniMapFromDataToFile(filename,strict,
                null,extractionRange, points);
        }
        
        public void ExtractMiniMapFromDataToFile(string filename,bool strict,
            TrackPlanner.Shared.Data.Region? boundary,
            Length? extractionRange = null, params GeoPoint[] points)
        {
            var title = System.IO.Path.GetFileNameWithoutExtension(filename);

            using (var mem_stream = new MemoryStream())
            {
                ExtractMiniMapFromPoints(Logger, title, mem_stream, boundary,strict,
                    extractionRange, points);
                var target_path = Navigator.GetUniquePath(Navigator.GetOutputDirectory(), filename);
                using (var fs_stream = new FileStream(target_path, FileMode.CreateNew, FileAccess.Write))
                {
                    mem_stream.CopyTo(fs_stream);
                }

            }
        }
    }

    public abstract class MiniExtractor<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        protected const int Precision = 15;

        internal static Navigator Navigator { get; } = new Navigator();
        private readonly MapMode mapMode;
        protected ILogger Logger { get; }

        protected MiniExtractor(ILogger? logger = null)
        {
            this.Logger = logger ?? new NoLogger();
            this.mapMode = WorldMapExtension.GetMapMode<TNodeId, TRoadId>();
        }

        protected static FinderConfiguration GetFinderPreferences()
        {
            return PrefsFactory.GetFinderPreferences<TNodeId,TRoadId>();
        }
        public static void ExtractMiniMapFromPoints(ILogger logger, string title, Stream stream,
            TrackPlanner.Shared.Data.Region? boundary,bool strict,
            Length? extractionRange, params GeoPoint[] points)
        {
            var visual_prefs =  UserVisualPreferences.Defaults();

            var sys_config = GetFinderPreferences();

            using (RouteManager<TNodeId, TRoadId>.Create(logger, Navigator, mapFolder: "kujawsko-pomorskie",
                       sys_config, out var manager))
            {
                {
                    HashSet<TNodeId> nodes = new HashSet<TNodeId>();
                    if (points.Length>0)
                        nodes.AddRange(manager.Map.GetNodesAlongLine(manager.Calculator,
                            extractionRange ?? Length.Zero, points));
                    if (boundary.HasValue)
                        nodes.AddRange(manager.Map.GetNodesWithin(boundary.Value)
                            .Select(it => it.nodeId));
                    var mini_map = manager.Map.ExtractMiniMap(logger,
                        sys_config.MemoryParams.GridCellSize, Navigator.GetDebugDirectory(),
                        onlyRoadBased: true,
                        strict:strict,
                        nodes);
                    mini_map.SaveAsKml(visual_prefs, title, stream, flatRoads: true);
                }
            }
            
            stream.Position = 0;
        }
    }
}