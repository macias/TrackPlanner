using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace TrackPlanner.TestToolbox
{
    public sealed class MonkeyStats<TPayload>
    {
        private readonly TimeSpan gracePeriod;
        private readonly object threadLock = new object();
        private  int sucessCount;
        private  int problemsCount;
        private  int notFoundCount;
        private List<TPayload> payload;
        private DateTimeOffset expiration;
        private int runCount;
        public bool Alive { get; set; }

        public int SucessCount
        {
            get { lock (this.threadLock) return this.sucessCount; }
        }

        public int ProblemsCount
        {
            get { lock (this.threadLock) return this.problemsCount; }
        }

        public int NotFoundCount
        {
            get { lock (this.threadLock) return this.notFoundCount; }
        }

        public MonkeyStats(TimeSpan gracePeriod)
        {
            this.gracePeriod = gracePeriod;
            this.expiration = DateTimeOffset.UtcNow + gracePeriod;
            Alive = true;
            this.runCount = -1;
            this.payload = new List<TPayload>();
        }

        public bool IsExpired(out int runCount,
            [MaybeNullWhen(false)] out TPayload[] payload)
        {
            lock (this.threadLock)
            {
                bool expired = DateTimeOffset.UtcNow > this.expiration;
                payload = this.payload.ToArray();
                runCount = this.runCount;
                return expired;
            }
        }
        public void Start(TimeSpan timeout,params TPayload[] payload)
        {
            lock (this.threadLock)
            {
                ++this.runCount;
                this.payload = payload.ToList();
                this.expiration = DateTimeOffset.UtcNow + timeout+this.gracePeriod;
            }
        }
        public void Update(TimeSpan timeout,params TPayload[] payload)
        {
            lock (this.threadLock)
            {
                this.payload.AddRange(payload);
                this.expiration = DateTimeOffset.UtcNow + timeout+this.gracePeriod;
            }
        }

        public void Complete(RouteOutcome outcome)
        {
            lock (this.threadLock)
            {
                this.payload.Clear();
                if (outcome == RouteOutcome.Success)
                    ++sucessCount;
                else if (outcome == RouteOutcome.NotFound)
                    ++notFoundCount;
                else if (outcome == RouteOutcome.Problems)
                    ++problemsCount;
                else
                    throw new NotImplementedException(outcome.ToString());
            }
        }
    }
}