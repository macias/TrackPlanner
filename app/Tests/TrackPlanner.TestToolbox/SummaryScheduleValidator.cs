using System;
using System.Collections.Generic;
using System.Linq;
using Geo;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.TestToolbox
{
    public static class SummaryScheduleValidator
    {
        public static IEnumerable<CheckpointTrait> GetCheckpointTraits(this ISummarySchedule schedule)
        {
            return schedule.GetSummary().Days.ZipIndex()
                .SelectMany(day_it => Enumerable.Range(0, day_it.item.Checkpoints.Count)
                    .Select(idx => new CheckpointTrait(schedule, day_it.index, idx)));
        }
    }

}