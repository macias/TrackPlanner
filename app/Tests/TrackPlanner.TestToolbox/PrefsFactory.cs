using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.TestToolbox
{
    public static class PrefsFactory
    { 
        public static PlanRequest<TNodeId> PointsToRequest<TNodeId>(UserRouterPreferences routerPreferences,
            IEnumerable<RequestPoint<TNodeId>> req_points)
            where TNodeId:struct
        {
            var request = new PlanRequest<TNodeId>()
            {
                DailyPoints = new List<List<RequestPoint<TNodeId>>>()
                {
                    req_points.ToList()
                },
                TurnerPreferences = UserTurnerPreferences.Defaults(),
                RouterPreferences = routerPreferences,
            };
            return request;
        }
        public static UserRouterPreferences GetRouterPreferences()
        {
            var prefs = UserRouterPreferencesHelper.CreateBikeOriented().SetCustomSpeeds();
            // no compacting
            prefs.CompactingDistanceDeviation = Length.Zero;
            prefs.FastModeLimit = null;
            return prefs;
        }

        public static FinderConfiguration GetFinderPreferences<TNodeId,TRoadId>()
        {
            var prefs = FinderConfiguration.Defaults() with
            {
                CompactPreservesRoads = true
            };
            prefs.MemoryParams.SetEnableOsmId(true);
            prefs.MemoryParams.SetMapMode(WorldMapExtension.GetMapMode<TNodeId, TRoadId>());
            //prefs.MemoryParams.GridCellSize = 50;
            prefs.MemoryParams.WorldCacheCellsLimit = 4_000;

            return prefs;
        }
    }
}
