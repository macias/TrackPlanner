﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TrackPlanner.Backend;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using TrackPlanner.VisualMonkey;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace VisualRunner
{
    class Program
    {
        private static readonly Navigator navigator = new Navigator();
        private const bool mock_worker = false;

        static async Task Main(string[] args)
        {
            var timeout = TimeSpan.FromSeconds(mock_worker?10:2*60);
            var grace_period = TimeSpan.FromSeconds(5);
            const int workers_count = 1;

            var main_sink_settings = new SinkSettings()
            {
                ConsoleLevel = MessageLevel.Debug,
                FileLevel = MessageLevel.Info,
            };
            using (Logger.Create(new LoggerSettings()
                   {
                       LogPath = navigator.GetLogPath("monkey-visual.txt"),
                       Sink = main_sink_settings
                   }, out var main_logger))
            {
                var threaded_logger = new ThreadedLogger(main_logger, main_sink_settings);
                try
                {
                    //if (!DevelModes.True)
                    {
                        main_logger.Info("Dry run first");
                        
                        // dry run
                        using (var monkey = new MonkeyScheduler<TNodeId, TRoadId>(threaded_logger, 
                                   navigator,
                                   mockWorker: mock_worker, calcReal: false))
                        {
                            await monkey.EvaluateAsync(timeout, 
                                new MonkeyStats<VisualCommand>(grace_period), dryRun: true).ConfigureAwait(false);
                        }
                    }

                    var stats = new List<MonkeyStats<VisualCommand>>();
                    var alive_count = workers_count;
                    for (int __i = 0; __i < workers_count; ++__i)
                    {
                        int i = __i;
                        var s = new MonkeyStats<VisualCommand>(grace_period);
                        stats.Add(s);
                        _ = Task.Factory.StartNew(async () =>
                        {
                            threaded_logger.Verbose($"Starting {i} worker");
                            using (var monkey = new MonkeyScheduler<TNodeId, TRoadId>(threaded_logger, navigator,
                                       mockWorker:mock_worker, calcReal: false))
                            {
                                await monkey.EvaluateAsync(timeout, s).ConfigureAwait(false);
                            }
                        },TaskCreationOptions.LongRunning);
                    }

                    await Task.Factory.StartNew(async () =>
                    {
                        long start = Stopwatch.GetTimestamp();

                        threaded_logger.Verbose("Starting reporter");
                        while (true)
                        {
                            int sucess_count = 0;
                            int problems_count = 0;
                            int not_found_count = 0;
                            foreach (var s in stats)
                            {
                                sucess_count += s.SucessCount;
                                problems_count += s.ProblemsCount;
                                not_found_count += s.NotFoundCount;

                                if (s.Alive)
                                {
                                    if (s.IsExpired(out var run_count, out var payload))
                                    {
                                        threaded_logger.Error($"Stuck at {run_count} with:{Environment.NewLine} {(String.Join(Environment.NewLine, payload.Select(it => it.SerializeAsCode())))}");
                                        s.Alive = false;
                                        --alive_count;
                                    }
                                }
                            }
                            
                            if (alive_count == 0)
                                return;

                            var seconds = (Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency;

                            var iter_count = sucess_count + problems_count + not_found_count;

                            var message = $"{alive_count}/{workers_count} workers, try {iter_count} ({sucess_count}:{not_found_count})";
                            if (problems_count > 0)
                                message += $" PROBLEMS: {problems_count}";
                            message += $" in {DataFormat.Format(TimeSpan.FromSeconds(seconds))}/{seconds / (iter_count + 1)}";

                            threaded_logger.Verbose(message);

                            await Task.Delay(TimeSpan.FromMinutes(1)).ConfigureAwait(false);
                        }
                    },TaskCreationOptions.LongRunning).Unwrap().ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    ex.LogDetails(threaded_logger,MessageLevel.Error, "Program crashed");
                }
            }
        }
    }
}