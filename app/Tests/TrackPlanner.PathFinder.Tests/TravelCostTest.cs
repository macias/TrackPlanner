using System;
using TrackPlanner.Structures;
using Xunit;

namespace TrackPlanner.PathFinder.Tests;

public class TravelCostTest
{
    [Fact]
    public void ComparisonTest()
    {
        var a = Weight.FromCost(TimeSpan.FromMinutes(2), 2);
        var b = Weight.FromCost(TimeSpan.FromMinutes(3), 1);
        Assert.True(WeightComparer.Instance.IsGreater(a , b));
        Assert.True(!WeightComparer.Instance.IsLess( a , b));
        Assert.True(WeightComparer.Instance.IsLess( b , a));
        Assert.True(!WeightComparer.Instance.IsGreater(b , a));
        Assert.True(b != a);
        Assert.True(a != b);
        Assert.False(b == a);
    }
}