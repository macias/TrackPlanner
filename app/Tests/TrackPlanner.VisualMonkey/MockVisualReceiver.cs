using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using TrackPlanner.Backend;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.RestClient;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;

namespace TrackPlanner.VisualMonkey
{
    public sealed class MockVisualReceiver<TNodeId, TRoadId> : IVisualReceiver, IVisualActor
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        public static IDisposable? Create(ILogger logger, Navigator navigator,
            IGeoCalculator calculator, VisualTestSettings testSettings, 
            out MockVisualReceiver<TNodeId, TRoadId> visualReceiver)
        {
            if (testSettings.MockWorker)
            {
                visualReceiver = new MockVisualReceiver<TNodeId, TRoadId>(logger, navigator,
                    routeManager: null,
                    calculator,testSettings.DraftRealOptions,testSettings.SettingsCustomizer,
                    testSettings.AlwaysReal);
                return null;
            }
            else
            {
                IDisposable result = CompositeDisposable.None;
                var mini = new MiniWorld<TNodeId, TRoadId>(logger);
                Stream? stream = null;
                if (testSettings.Filename is { } fn)
                {
                    stream = MiniWorld.MiniMapToStream(fn);
                    result = CompositeDisposable.Stack(result, stream);
                }

                result = CompositeDisposable.Stack(result, mini.CreateManager(testSettings.FinderConfiguration,
                    testSettings.Filename, stream, testSettings.MapFolder, out var manager));
                visualReceiver = new MockVisualReceiver<TNodeId, TRoadId>(logger, navigator,manager,
                    calculator, 
                    DraftOptions.GetLegacyTests(),
                    testSettings.SettingsCustomizer,
                    testSettings.AlwaysReal);

                return result;
            }
        }

        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly RouteManager<TNodeId, TRoadId>? routeManager;
        public IWorldMap<TNodeId, TRoadId> Map => this.routeManager?.Map ?? throw new InvalidOperationException("Map not available in draft/mock mode");
        private bool trueRouting => this.routeManager != null;
        private readonly IGeoCalculator calculator;
        private readonly DraftOptions draftRealOptions;
        private readonly Action<ScheduleSettings> settingsCustomizer;
        private readonly bool alwaysReal;
        private readonly HashSet<LegPlan<TNodeId, TRoadId>> legs;
        private readonly Dictionary<string, int> hitCounters;
        private LocalPlanClient<TNodeId, TRoadId> planClient = default!;
        public LocalPlanClient<TNodeId, TRoadId> PlanClient => this.planClient;
        private readonly List<string> problemMessages;
        private readonly HashSet<PlacedAttraction> routeAttractions;
        public (int DayIndex, int AnchorIndex)? PlaceholderNotification { get; private set; }
        public IReadOnlyList<string> ProblemMessages => this.problemMessages;
        public ScheduleManager<TNodeId, TRoadId> Manager { get; private set; } = default!;
        public DispatchRecorder<TNodeId, TRoadId> Dispatcher => Manager.Dispatcher;

        public int LegCount => this.legs.Count;
        private IVisualReceiver asReceiver => this;

        public int OnSummarySetCount => this.getHitCount(nameof(asReceiver.OnSummarySetAsync));

        private MockVisualReceiver(ILogger logger, Navigator navigator, RouteManager<TNodeId, TRoadId>? routeManager,
            IGeoCalculator calculator,
            DraftOptions draftRealOptions,
            Action<ScheduleSettings>? settingsCustomizer,
            bool alwaysReal)
        {
            this.logger = logger;
            this.navigator = navigator;
            this.routeManager = routeManager;
            this.calculator = calculator;
            this.draftRealOptions = draftRealOptions;
            this.settingsCustomizer = settingsCustomizer ?? new Action<ScheduleSettings>( _ => { });
            this.alwaysReal = alwaysReal;
            this.legs = new HashSet<LegPlan<TNodeId, TRoadId>>();
            this.hitCounters = new Dictionary<string, int>();
            this.problemMessages = new List<string>();
            this.routeAttractions = new HashSet<PlacedAttraction>();

            Reset();
        }

        public MockVisualReceiver(ILogger logger,Navigator navigator, IGeoCalculator calculator,
            bool alwaysReal = false) : this(logger,navigator, routeManager: null, calculator,
            DraftOptions.GetLegacyTests(),settingsCustomizer:null, alwaysReal)
        {
        }

        public void Reset()
        {
            this.PlaceholderNotification = null;
            this.legs.Clear();
            this.routeAttractions.Clear();
            this.problemMessages.Clear();
            this.hitCounters.Clear();
            this.planClient = LocalPlanClient<TNodeId, TRoadId>.Create(this.logger, this.routeManager,
                this.draftRealOptions,
                alwaysReal: alwaysReal);
            var schedule_settings = ScheduleSettings.Defaults();
            this.settingsCustomizer(schedule_settings);
            this.Manager = new ScheduleManager<TNodeId, TRoadId>(logger, this,
                calculator,
                schedule_settings,
                UserVisualPreferences.Defaults(),
                this.PlanClient,
                this,
                GlobalSettings.Defaults().UndoHistory,
                GlobalSettings.Defaults().AutoBuild);


            _ =this.Manager.Schedule.GetSummary();
        }

        IDisposable IDataContext.NotifyOnChanging(object sender, string propertyName)
        {
            throw new NotImplementedException();
        }

        ValueTask IDataContext.NotifyOnChangedAsync(object sender, string propertyName)
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnAnchorChangedAsync(int dayIndex, int anchorIndex)
        {
            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }
            _ = this.Manager.Schedule.IndexedAnchors(dayIndex, anchorIndex).ToArray();
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnAnchorAddedAsync(VisualAnchor anchor, int dayIndex, int anchorIndex)
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnAllAnchorsRemovingAsync()
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnAnchorRemovingAsync(VisualAnchor anchor)
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnAnchorMovedAsync(VisualAnchor anchor)
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnPlanChangedAsync()
        {
            return ValueTask.CompletedTask;
        }

        void IVisualReceiver.OnDaySplit(int dayIndex)
        {
        }

        void IVisualReceiver.OnDayMerged(int dayIndex)
        {
        }

        ValueTask IVisualReceiver.OnLegRemovingAsync(int legIndex)
        {
            LegPlan<TNodeId, TRoadId> leg = this.Manager.Schedule.Route.Legs[legIndex];
            if (!this.legs.Remove(leg))
            {
                ;
                throw new ArgumentException($"Unable to find visual leg {legIndex}");
            }

            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnLegsAddedAsync(int firstLegIndex, int legCount)
        {
            foreach (var leg in this.Manager.Schedule.Route.Legs.Skip(firstLegIndex).Take(legCount))
                if (!this.legs.Add(leg))
                    throw new ArgumentException();
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnAllLegsRemovingAsync()
        {
            this.legs.Clear();
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnAttractionsAddedAsync(IEnumerable<PlacedAttraction> attractions)
        {
            foreach (var attr in attractions)
            {
                if (!this.routeAttractions.Add(attr))
                    throw new ArgumentException("Cannot add route attraction");
            }
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnSummarySetAsync()
        {
            this.countHit();
            _ = this.Manager.Schedule.GetSummary();
            return ValueTask.CompletedTask;
        }

        private void countHit([CallerMemberName] string? caller = null)
        {
            if (this.hitCounters.TryGetValue(caller!, out var counter))
                this.hitCounters[caller!] = counter + 1;
            else
                this.hitCounters.Add(caller!, 1);
        }

        private int getHitCount(string callName)
        {
            if (this.hitCounters.TryGetValue(callName, out var counter))
                return counter;
            else
                return 0;
        }

        ValueTask IVisualReceiver.OnPlaceholderChangedAsync()
        {
            this.PlaceholderNotification = this.Manager.Schedule.AnchorPlaceholder;
            _ = this.Manager.Schedule.GetSummary();
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnLoopChangedAsync()
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualReceiver.OnScheduleSetAsync()
        {
            return ValueTask.CompletedTask;
        }

        public ValueTask OnPeakAddedAsync(GeoZPoint peak)
        {
            return ValueTask.CompletedTask;
        }

        public ValueTask OnPeaksClearedAsync()
        {
            return ValueTask.CompletedTask;
        }

        ValueTask IVisualActor.AlertAsync(string? message, MessageLevel severity)
        {
            if (isValidProblem(message) && severity<MessageLevel.Info)
                this.problemMessages.Add(message!);
            return ValueTask.CompletedTask;
        }

        ValueTask<bool> IVisualActor.ConfirmAsync(string message)
        {
            return ValueTask.FromResult<bool>(true);
        }

        IDisposable IVisualActor.GetConcurrentLock(string message)
        {
            return CompositeDisposable.None;
        }

        public async ValueTask ValidateExecutionAsync(VisualCommand command, CancellationToken token)
        {
            var before_state = this.GetState();
            var result = await this.Dispatcher.ExecuteAsync(command, token).ConfigureAwait(false);
            this.CommandCheck(command, before_state, result,expectedErrorMessage:null);
        }

        public void CommandCheck(VisualCommand command, ReceiverState beforeState, Result<ValueTuple> commandResult,
            string? expectedErrorMessage)
        {
            if (commandResult.ProblemMessage is { } pm1
                // simply in case of real routing, not found route is not an error
                && isValidProblem(pm1.Message)
                && pm1.Message!=expectedErrorMessage)
                throw new Exception(pm1.FullMessage());

            _ = this.Manager.Schedule.GetSummary();
            
            var traits = this.Manager.Schedule.GetCheckpointTraits().ToArray();

            {
                if (this.ProblemMessages.FirstOrDefault() is { } problem
                    && problem!=expectedErrorMessage)
                    throw new Exception(problem);
            }

            if (expectedErrorMessage != null)
            {
                var error_message = commandResult.ProblemMessage?.Message ?? this.ProblemMessages.FirstOrDefault();
                if ( error_message != expectedErrorMessage)
                    throw new Exception($"Expected error {expectedErrorMessage}, but got {error_message}.");
                this.problemMessages.Clear();
            }

            if (this.Manager.Schedule.AnchorPlaceholder is { } placeholder) // todo: pretty lame, involve this properly
            {
                this.Manager.Schedule.GetLivePlaceholderLine();
                var day = this.Manager.Schedule.Days[placeholder.DayIndex];
                if (placeholder.AnchorIndex < 0 || placeholder.AnchorIndex > day.Anchors.Count)
                    throw new ArgumentOutOfRangeException($"Placeholder is out of range: {placeholder.AnchorIndex}/{day.Anchors.Count}");
            }

            if (PlaceholderNotification != this.Manager.Schedule.AnchorPlaceholder)
                throw new Exception($"Anchor placehold mismatch after {command.Name}: notification {PlaceholderNotification} vs data {this.Manager.Schedule.AnchorPlaceholder}.");

            if (this.Manager.Schedule.Days.Count != this.Manager.UiState.CollapsedDays.Count)
                throw new Exception($"Days mismatch: schedule {this.Manager.Schedule.Days.Count}, UI {this.Manager.UiState.CollapsedDays.Count}");

            {
                var schedule_count = this.Manager.Schedule.Route.Legs.Sum(it => it.Attractions.Count);
                if (this.routeAttractions.Count != schedule_count)
                    throw new Exception($"Route attractions count mismatch visual:{this.routeAttractions.Count} vs schedule:{schedule_count} after {command.Name}");
                if (command is AddRouteAttraction && schedule_count != beforeState.RouteAttractionCount - 1)
                    throw new Exception($"Only one attraction should be removed on {command.Name}");
            }
            if (!trueRouting && !this.draftRealOptions.OffsetRealPoints )
            {
                foreach (var trait in traits)
                {
                    var checkpoint = this.Manager.Schedule.GetSummary().Days[trait.CheckpointDayIndex].Checkpoints[trait.CheckpointIndex];
                    // in general, checkpoint coordinates are taken from the route, so they can deviate from the user request
                    // but here we are using mock/draft planner, and it uses the same coordinates
                    var chk_coords = checkpoint.UserPoint.Convert2d();
                    // convert back and forth to get the same precision as checkpoint coordinates
                    var anchor_coords = trait.Anchor.UserPoint.Convert3d().Convert2d();
                    if (chk_coords != anchor_coords)
                    {
                        var message = $"Coordinates mismatch {chk_coords} vs {anchor_coords}";
                        throw new ArgumentException(message);
                    }
                }
            }

            if (!this.Manager.Schedule.HasEnoughAnchorsForBuild())
            {
                if (this.Manager.Schedule.Days.Count > 1)
                    throw new Exception($"We have multiple days, but single anchor after {command.Name}");
                
                if (!this.Manager.Schedule.Route.IsEmpty())
                    throw new Exception($"Remaining of the route after {command.Name}");
            }
            else
            {
                if (this.Manager.Schedule.Route.Validate() is { } problem)
                {
                    throw new Exception(problem.FullMessage());
                }

                var empty_day = this.Manager.Schedule.Days.ZipIndex()
                    .SkipLast(this.Manager.Schedule.Settings.LoopRoute ? 1 : 0)
                    .FirstOrNone(it => it.item.Anchors.Count == 0);
                if (empty_day.HasValue)
                    throw new Exception($"Day {empty_day.Value.index} is empty after {command.Name}");
                    
                var active_anchors = this.Manager.Schedule.Days
                    .SelectMany(it => it.Anchors)
                    .SkipLast(this.Manager.Schedule.IsLoopActivated() ? 0 : 1)
                    .ToList();
                if (active_anchors.Count != this.Manager.Schedule.Route.Legs.Count)
                    throw new Exception($"Active anchors count {active_anchors}, legs count {this.Manager.Schedule.Route.Legs.Count}");
                foreach (var (anchor, idx) in active_anchors.ZipIndex())
                {
                    if (anchor.IsPinned == this.Manager.Schedule.Route.Legs[idx].AutoAnchored)
                        throw new Exception("Anchor-leg mode out of sync");
                    if (this.Manager.Schedule.Route.Legs[idx].IsMissing)
                        throw new Exception("Used leg is missing");
                    //if (this.Manager.Schedule.Route.Legs[idx].Fragments.Count == 0)
                    //  throw new Exception("Empty leg");
                }
            }
        }

        private bool isValidProblem(string? message)
        {
            return message != null
                   && (!trueRouting || message != RouteFinding.RouteNotFound.ToString());
        }

        public async ValueTask StoreRealFileAsAsync(string realFilename, string localFilename)
        {
            if (!StorageHelper.TryDeserializeSchedule<TNodeId, TRoadId>(
                    System.IO.File.ReadAllText(System.IO.Path.Combine( this.navigator.GetSchedules(),realFilename)),
                    out var schedule))
                throw new Exception();
            var res = await PlanClient.SaveScheduleAsync(schedule, localFilename, CancellationToken.None).ConfigureAwait(false);
            if (!res.HasValue || res.ProblemMessage != null)
                throw new Exception();
        }

        ValueTask IVisualReceiver.OnAttractionsRemovingAsync(IEnumerable< PlacedAttraction> attractions)
        {
            if (DEBUG_SWITCH.Enabled && attractions.Any())
            {
                ;
            }
            foreach (var attr in attractions)
            {
                if (!this.routeAttractions.Remove(attr))
                    throw new ArgumentException("Cannot find given attraction");
            }

            return ValueTask.CompletedTask;
        }


        public ReceiverState GetState()
        {
            return new ReceiverState()
            {
                RouteAttractionCount = this.routeAttractions.Count
            };
        }
    }
}