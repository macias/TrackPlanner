using System;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.Shared.Stored;
using TrackPlanner.TestToolbox;

namespace TrackPlanner.VisualMonkey
{
    public sealed record class VisualTestSettings
    {
        public bool MockWorker { get; set; }
        public DraftOptions DraftRealOptions { get; set; }
        public bool AlwaysReal { get; set; }
        public string? Filename { get; set; }
        public Action<ScheduleSettings>? SettingsCustomizer { get; set; }
        public string MapFolder { get; set; }
        public FinderConfiguration? FinderConfiguration { get; set; }

        public VisualTestSettings()
        {
            this.DraftRealOptions = DraftOptions.GetLegacyTests();
            MapFolder = "kujawsko-pomorskie";
        }

    }
}