using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MathUnit;
using Newtonsoft.Json;
using TrackPlanner.Backend;
using TrackPlanner.RestClient;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;

namespace TrackPlanner.VisualMonkey
{
    public sealed class MonkeyScheduler<TNodeId, TRoadId> : IDisposable
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly Navigator navigator;
        private readonly bool mockWorker;

        private const int maxRealAnchors = 4;

        private readonly MockVisualReceiver<TNodeId, TRoadId> visual;
        private ScheduleManager<TNodeId, TRoadId> manager => this.visual.Manager;

        private readonly Random rnd;
        private readonly ApproximateCalculator calculator;
        private readonly ProxySerializer serializer;
        private readonly CoordRandomizer coordRandomizer;
        private readonly IDisposable? disposable;
        private readonly IReadOnlyList<Func<IEnumerable<VisualCommand>>> builders;
        private DispatchRecorder<TNodeId, TRoadId> dispatcher => this.visual.Dispatcher;

        public MonkeyScheduler(ILogger logger, Navigator navigator, bool mockWorker, bool calcReal)
        {
            this.rnd = new Random();
            this.navigator = navigator;
            this.mockWorker = mockWorker;
            this.calculator = new ApproximateCalculator();
            this.coordRandomizer = mockWorker ? BigCoordRandomizer.Instance : TinyCoordRandomizer.Instance;
            this.disposable = MockVisualReceiver<TNodeId, TRoadId>.Create(
                logger.With(SinkSettings.CreateDisabled()), navigator, this.calculator,
                new VisualTestSettings()
                {
                    MockWorker = mockWorker,
                    AlwaysReal = calcReal,
                    SettingsCustomizer = settingsCustomizer,
                    FinderConfiguration = FinderConfiguration.Defaults() with
                    {
                        FinalSnapProximityLimit = Length.FromKilometers(2)
                    }
                },
                out this.visual);
            var options = NewtonOptionsFactory.BuildJsonOptions(compact: false);
            options.TypeNameHandling = TypeNameHandling.Auto;
            this.serializer = new ProxySerializer(options);
            this.builders = new Func<IEnumerable<VisualCommand>>[]
            {
                // only stateless commands can be created directly, anything else
                // only through factory functions
                () => new[] {new NewProject()},
                () => new[] {new Undo()},
                () => new[] {new Redo()},
                createAutoBuildSwitch,
                createLoopSwitch,
                createChangeAnchorPosition,
                createAddAnchor,
                createClearAttractions,
                createAddExternalAttraction,
                createSnapToPeak,
                createSnapToLine,
                createAddRouteAttraction,
                createGetAttractions,
                createRemovePlaceholder,
                createMouseOverLine,
                createSetPlaceholder,
                createPrepareAnchorAppend,
                createDayMerge,
                createOrderPrevious,
                createOrderNext,
                createBuild,
                createDaySplit,
                createDeleteAnchor,
                createFindPeaks,
                createClearPeaks,
                createPinAutoAnchor,
                createSave,
                createLoad,
            };
        }

        public void Dispose()
        {
            this.disposable?.Dispose();
        }

        private void settingsCustomizer(ScheduleSettings cfg)
        {
            cfg.PlannerPreferences.StartWithInsert = this.rnd.Next(2) == 0;
            if (this.rnd.Next(2) == 0)
                cfg.PlannerPreferences.Home = coordRandomizer.RandomizePoint(rnd);
        }

        public async ValueTask EvaluateAsync(TimeSpan timeout, MonkeyStats<VisualCommand> stats,
            bool dryRun = false)
        {
            do
            {
                this.visual.Reset();

                try
                {
                    stats.Start(timeout);

                    if (!dryRun)
                    {
                        await this.visual.Manager.InitializeDefaultAsync().ConfigureAwait(false);

                        while (this.dispatcher.HistoryCount < 30)
                        {
                            var actions = builders[this.rnd.Next(builders.Count)]().ToArray();

                            foreach (VisualCommand action in actions)
                            {
                                stats.Update(timeout, action);
                                await this.visual.ValidateExecutionAsync(action, CancellationToken.None).ConfigureAwait(false);
                            }
                        }
                    }

                    stats.Complete(RouteOutcome.Success);
                }
                catch (Exception ex)
                {
                    handleError(ex.ExtractDetails());

                    if (DevelModes.True)
                        return;
                    else
                        stats.Complete(RouteOutcome.Problems);
                }
            }
            while (!dryRun);
        }


        private IEnumerable<Save> createSave()
        {
            var files = this.visual.PlanClient.ProjectFiles.ToList();
            var idx = this.rnd.Next(3);
            string name;
            if (idx < files.Count)
                name = files[idx]; // overwrite
            else
                name = $"{files.Count}{SystemCommons.ProjectFileExtension}"; // new save
            yield return new Save(name);
        }

        private IEnumerable<Load> createLoad()
        {
            var files = this.visual.PlanClient.ProjectFiles.ToList();
            if (files.Count != 0)
            {
                yield return new Load(files[this.rnd.Next(files.Count)]);
            }
        }

        private IEnumerable<LoopSwitch> createLoopSwitch()
        {
            yield return new LoopSwitch(IsLooped: !this.manager.Schedule.Settings.LoopRoute);
        }

        private IEnumerable<VisualCommand> createAutoBuildSwitch()
        {
            yield return new AutoBuildSwitch(Enable: !this.manager.AutoBuild);
        }

        private void handleError(string extractDetails)
        {
            List<string> historyAsCode = this.dispatcher.HistoryAsCode().ToList();
            string path = Navigator.UniqueSaveText(this.navigator.GetOutputDirectory(), "visual-case.code",
                historyAsCode);
            System.IO.File.WriteAllText(System.IO.Path.ChangeExtension(path, ".error"),
                extractDetails);
            System.IO.File.WriteAllText(System.IO.Path.ChangeExtension(path, ".route"),
                dispatcher.StringifySchedule());
        }


        private IEnumerable<VisualCommand> createAddAnchor()
        {
            if (!canAddAnchors())
                yield break;

            if (!this.manager.Schedule.AnchorPlaceholder.HasValue)
                yield return createSetPlaceholder().Single();
            yield return new AddAnchorAtPlaceholder(Point: coordRandomizer.RandomizePoint(rnd));
        }

        private bool canAddAnchors()
        {
            return this.mockWorker || this.manager.Schedule.IndexedReadOnlyAnchors().Count() < maxRealAnchors;
        }

        private IEnumerable<AddExternalAttraction> createAddExternalAttraction()
        {
            if (!canAddAnchors())
                yield break;

            var place = new MapZPoint<OsmId, OsmId>(coordRandomizer.RandomizePoint(this.rnd).Convert3d(), nodeId: null
#if DEBUG
                , DEBUG_mapRef: null
#endif
            );
            yield return new AddExternalAttraction(new PlacedAttraction(place.Point,
                new PointOfInterest("monkey-attr", null, PointOfInterest.Feature.None)));
        }

        private IEnumerable<SnapToPeak> createSnapToPeak()
        {
            if (!canAddAnchors() || this.manager.AuxState.Peaks.Count == 0)
                yield break;

            var idx = this.rnd.Next(this.manager.AuxState.Peaks.Count);
            yield return new SnapToPeak(this.manager.AuxState.Peaks[idx]);
        }

        private IEnumerable<SnapToLineAt> createSnapToLine()
        {
            yield return new SnapToLineAt(coordRandomizer.RandomizePoint(this.rnd));
        }

        private IEnumerable<FindRouteAttractions> createGetAttractions()
        {
            if (this.manager.Schedule.HasEnoughAnchorsForBuild())
                yield return new FindRouteAttractions(this.rnd.Next(this.manager.Schedule.Days.Count), PointOfInterest.Category.Historic);
        }

        private IEnumerable<AddRouteAttraction> createAddRouteAttraction()
        {
            if (!canAddAnchors())
                yield break;

            var route_attrs = this.manager.Schedule.Route.Legs.ZipIndex()
                .SelectMany(it => it.item.Attractions.ZipIndex()
                    .Select(a => (leg_idx: it.index, attr_idx: a.index)))
                .ToList();
            if (route_attrs.Count != 0)
            {
                var index = this.rnd.Next(route_attrs.Count);
                var attr_idx = route_attrs[index].attr_idx;
                var leg_idx = route_attrs[index].leg_idx;
                yield return new AddRouteAttraction(leg_idx, attr_idx,
                    this.manager.Schedule.Route.Legs[leg_idx].Attractions[attr_idx]);
            }
        }

        private IEnumerable<VisualCommand> createBuild()
        {
            if (!this.manager.Schedule.HasEnoughAnchorsForBuild())
                yield break;
            if (this.rnd.Next(2) == 0)
                yield return new CompleteRebuild();
            else
                yield return new PartialBuild();
        }


        private IEnumerable<PrepareAnchorAppend> createPrepareAnchorAppend()
        {
            var data = this.manager.Schedule.IndexedReadOnlyAnchors().ToList();
            if (data.Count > 0)
            {
                var idx = this.rnd.Next(data.Count);
                yield return new PrepareAnchorAppend(DayIndex: data[idx].dayIndex,
                    AnchorIndex: data[idx].anchorIndex);
            }
        }

        private IEnumerable<SetPlaceholder> createSetPlaceholder()
        {
            var day_idx = this.rnd.Next(this.manager.Schedule.Days.Count);
            var anchor_idx = this.rnd.Next(this.manager.Schedule.Days[day_idx].Anchors.Count + 1);
            yield return new SetPlaceholder(DayIndex: day_idx, AnchorIndex: anchor_idx);
        }

        private IEnumerable<ChangeAnchorPosition> createChangeAnchorPosition()
        {
            if (tryRandomCheckpoint(it => it.CanMove) is { } trait)
                yield return new ChangeAnchorPosition(coordRandomizer.RandomizePoint(rnd), DayIndex: trait.AnchorDayIndex,
                    AnchorIndex: trait.AnchorIndex);
        }

        private IEnumerable<DeleteAnchor> createDeleteAnchor()
        {
            if (tryRandomCheckpoint(it => it.CanDelete) is { } trait)
                yield return new DeleteAnchor(DayIndex: trait.AnchorDayIndex, AnchorIndex: trait.AnchorIndex);
        }

        private IEnumerable<FindPeaks> createFindPeaks()
        {
            if (tryRandomCheckpoint(it => true) is { } trait)
                yield return new FindPeaks(trait.AnchorDayIndex,trait.AnchorIndex);
        }

        private IEnumerable<ClearPeaks> createClearPeaks()
        {
                yield return new ClearPeaks();
        }

        private IEnumerable<DaySplit> createDaySplit()
        {
            if (tryRandomCheckpoint(it => it.CanSplit) is { } trait)
                yield return new DaySplit(CheckpointDayIndex: trait.CheckpointDayIndex,
                    CheckpointIndex: trait.CheckpointIndex);
        }

        private IEnumerable<PinAutoAnchor> createPinAutoAnchor()
        {
            if (!canAddAnchors())
                yield break;

            if (tryRandomCheckpoint(it => it.CanPin) is { } trait)
                yield return new PinAutoAnchor(DayIndex: trait.AnchorDayIndex, AnchorIndex: trait.AnchorIndex);
        }

        private CheckpointTrait? tryRandomCheckpoint(Func<CheckpointTrait, bool> predicate)
        {
            var traits = this.manager.Schedule.GetCheckpointTraits()
                .Where(predicate)
                .ToArray();

            if (traits.Length == 0)
            {
                return null;
            }

            var idx = this.rnd.Next(traits.Length);
            return traits[idx];
        }

        private IEnumerable<DaySplitRemove> createDayMerge()
        {
            if (tryRandomCheckpoint(it => it.CanMerge) is { } trait)
                yield return new DaySplitRemove(AnchorDayIndex: trait.AnchorDayIndex);
        }

        private IEnumerable<OrderAnchor> createOrderNext()
        {
            if (tryRandomCheckpoint(it => it.CanOrderNext) is { } trait)
                yield return OrderAnchor.Next(trait.AnchorDayIndex,trait.AnchorIndex);
        }

        private IEnumerable<OrderAnchor> createOrderPrevious()
        {
            if (tryRandomCheckpoint(it => it.CanOrderPrevious) is { } trait)
                yield return OrderAnchor.Previous(trait.AnchorDayIndex,trait.AnchorIndex);
        }

        private IEnumerable<ClearAttractions> createClearAttractions()
        {
            if (this.rnd.Next(2) == 0)
                yield return new ClearAttractions(null);
            else
                yield return new ClearAttractions(this.rnd.Next(this.manager.Schedule.Days.Count));
        }

        private IEnumerable<VisualCommand> createRemovePlaceholder()
        {
            if (!this.visual.Manager.Schedule.AnchorPlaceholder.HasValue)
                yield return createSetPlaceholder().Single();
            yield return new DeletePlaceholder();
        }

        private IEnumerable<FragmentInformationGetter> createMouseOverLine()
        {
            var non_zero_legs = this.manager.Schedule.Route.Legs.ZipIndex()
                .Where(it => it.item.Fragments.Count > 0)
                .ToArray();
            if (non_zero_legs.Length == 0)
                yield break;

            var leg_idx = non_zero_legs[this.rnd.Next(non_zero_legs.Length)].index;
            var frag_idx = this.rnd.Next(this.manager.Schedule.Route.Legs[leg_idx].Fragments.Count);
            var fragment = this.manager.Schedule.Route.Legs[leg_idx].Fragments[frag_idx];
            var pt = this.calculator.GetMidPoint(fragment.GetSteps().First().Point, fragment.GetSteps()[1].Point);
            yield return new FragmentInformationGetter(pt.Convert2d(), leg_idx, frag_idx);
        }
    }
}