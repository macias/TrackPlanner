namespace TrackPlanner.VisualMonkey
{
    public sealed record class ReceiverState
    {
        public int RouteAttractionCount { get; set; }
    }
}