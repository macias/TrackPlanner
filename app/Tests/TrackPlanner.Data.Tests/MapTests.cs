using System.Linq;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Data.Tests;

using Xunit;


public class MapTests
{
    [Fact]
    public void ComputingCellTargetsTest()
    {
      var indices =  RoadGridMemoryBuilder.GetCellIndices(new ApproximateCalculator(),
            GeoZPoint.FromDegreesMeters( 53.1422827, 18.5022558,0),
            GeoZPoint.FromDegreesMeters( 53.1396416, 18.4880871,0),
            MemorySettings.Defaults().GridCellSize)
          .ToList();
      Assert.Equal(4,indices.Count());
      Assert.Contains(new CellIndex(9671,3364),indices);
      Assert.Contains(new CellIndex(9671,3365),indices);
      Assert.Contains(new CellIndex(9671,3366),indices);
      Assert.Contains(new CellIndex(9671,3367),indices);
      
        {
            ;
        }
    }
}