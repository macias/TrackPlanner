using System;
using System.Text.Json.Serialization;
using MathUnit;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Structures;
using Xunit;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace TrackPlanner.Data.Tests
{
    public readonly struct Pair
    {
        public int X { get; }
        public int Y { get; }

        [JsonConstructor]
        public Pair(int x,int y)
        {
            X = x;
            Y = y;
        }
    }


    public class TextSerializationTests
    {
        [Fact]
        public void PairSerializationTest()
        {
            var input = new Pair(3,5);
            var options = TextOptionsFactory.BuildJsonOptions(compact: false);

            var json_string = JsonSerializer.Serialize(input, options);

            var output = JsonSerializer.Deserialize<Pair>(json_string, options);

            Assert.Equal(input, output);

        }

        [Fact]
        public void AngleSerializationTest()
        {
            var input = Angle.FromRadians(Math.PI);
            var options = TextOptionsFactory.BuildJsonOptions(compact:false);

            var json_string = JsonSerializer.Serialize(input, options);
            Assert.Contains("180", json_string);

            var output = JsonSerializer.Deserialize<Angle>(json_string, options);

            Assert.Equal(input, output);

        }

        [Fact]
        public void GeoZPointSerializationTest()
        {
            if (DevelModes.True)
            return; // it looks Text.Json does not respect ISerializable
            
            {
                var input = GeoZPoint.Create(Angle.FromDegrees(90), Angle.FromDegrees(180), null);
                var options = TextOptionsFactory.BuildJsonOptions(compact:false);
                var json_string = JsonSerializer.Serialize(input, options);
                var output = JsonSerializer.Deserialize<GeoZPoint>(json_string, options);

                Assert.Equal(input, output);
            }

            {
                var input = GeoZPoint.Create(Angle.FromDegrees(90), Angle.FromDegrees(180), Length.FromMeters(100));
                var options = TextOptionsFactory.BuildJsonOptions(compact:false);
                var json_string = JsonSerializer.Serialize(input, options);
                var output = JsonSerializer.Deserialize<GeoZPoint>(json_string, options);

                Assert.Equal(input, output);
            }
        }

    }
}
