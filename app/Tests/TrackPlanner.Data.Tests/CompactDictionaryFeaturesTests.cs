using System;
using System.Linq;
using TrackPlanner.Structures;
using Xunit;

namespace TrackPlanner.Data.Tests
{
    public class CompactDictionaryFeaturesTests
    {
        [Fact]
        public void LiveChangingValuesTest()
        {
            var dict = new CompactDictionaryShift<string, int>
            {
                {"hello", 1},
                {"world", 2}
            };

            foreach (var (key,val) in dict)
            {
                dict[key] = val+1;
            }
            foreach (var (key,val) in dict.Keys.Zip(dict.Values))
            {
                dict[key] = val+1;
            }

        }

        [Fact]
        public void LiveChangingKeysRemoveTest()
        {
            static void iterate()
            {
                var dict = new CompactDictionaryShift<string, int>
                {
                    {"hello", 1},
                    {"world", 1}
                };

                foreach (var key in dict.Keys)
                {
                   Assert.True(dict.TryRemove(key, out _));
                }
            }

            var ex = Assert.Throws<InvalidOperationException>(iterate);
            Assert.Equal("Collection was modified; enumeration operation may not execute.", ex.Message);
        }
        
        [Fact]
        public void LiveChangingKeysAddTest()
        {
            static void iterate()
            {
                var dict = new CompactDictionaryShift<string, int>
                {
                    {"hello", 1},
                    {"world", 1}
                };

                foreach (var key in dict.Keys)
                {
                    Assert.True(dict.TryAdd($"{key}.", 22,out _));
                }
            }

            var ex = Assert.Throws<InvalidOperationException>(iterate);
            Assert.Equal("Collection was modified; enumeration operation may not execute.", ex.Message);
        }

    }
}