using TrackPlanner.RestClient;
using Xunit;

namespace TrackPlanner.Data.Tests
{

    public class DataFunctionsTests
    {
        [Fact]
        public void DebugStateEqualityTest()
        {
            var a = new DebugState(new int[] {3, 5}, new int[] {2},(3,4),7);
            var b = new DebugState(new int[] {3, 5},new int[] {2},(3,4), 7);
            Assert.True(a.AnchorCount!=b.AnchorCount);
            Assert.Equal(a, b);
        }
    }
}