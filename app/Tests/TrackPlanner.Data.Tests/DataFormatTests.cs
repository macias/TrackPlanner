using System;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using Xunit;

namespace TrackPlanner.Data.Tests
{

    public class DataFormatTests
    {
        [Fact]
        public void TimeSpanTest()
        {
            Assert.Equal("14:00",DataFormat.Format(TimeSpan.FromHours(14)));
            Assert.Equal("2.00:00",DataFormat.Format(TimeSpan.FromDays(2)));
        }
        [Fact]
        public void RawAngleTest()
        {
            Assert.Equal("3.5",DataFormat.RawFormat(Angle.FromDegrees(3.5)));
            Assert.Equal("2.48",DataFormat.RawFormat(Angle.FromDegrees(2.48)));
        }

        [Fact]
        public void FAILING_UserInputStabilityTest()
        {
            string input = "53.64512, 18.68615";
            var point = DataFormat.TryParseUserPoint(input);
            Assert.NotNull(point);

            var calc = new ApproximateCalculator();
            Assert.True(calc.TryStabilizeUserData(point.Value, out _, out var textual));
            Assert.Equal(input,textual);
        }

        [Fact]
        public void AdjustingNumbersTest()
        {
            int x;
            x = 0;
            Assert.Equal("0", DataFormat.Adjust(x,0));
            Assert.Equal("0", DataFormat.Adjust(x,1));
            Assert.Equal("0", DataFormat.Adjust(x,9));
            Assert.Equal("00", DataFormat.Adjust(x,10));
            Assert.Equal("00", DataFormat.Adjust(x,99));
            Assert.Equal("000", DataFormat.Adjust(x,100));
            Assert.Equal("000", DataFormat.Adjust(x,999));

            x = 1;
            Assert.Equal("1", DataFormat.Adjust(x,1));
            Assert.Equal("1", DataFormat.Adjust(x,9));
            Assert.Equal("01", DataFormat.Adjust(x,10));
            Assert.Equal("01", DataFormat.Adjust(x,99));
            Assert.Equal("001", DataFormat.Adjust(x,100));
            Assert.Equal("001", DataFormat.Adjust(x,999));

            x = 9;
            Assert.Equal("9", DataFormat.Adjust(x,9));
            Assert.Equal("09", DataFormat.Adjust(x,10));
            Assert.Equal("09", DataFormat.Adjust(x,99));
            Assert.Equal("009", DataFormat.Adjust(x,100));
            Assert.Equal("009", DataFormat.Adjust(x,999));
            
            x = 12;
            Assert.Throws<ArgumentOutOfRangeException>(() => DataFormat.Adjust(x, 10));
            Assert.Equal("12", DataFormat.Adjust(x,99));
            Assert.Equal("012", DataFormat.Adjust(x,100));
            Assert.Equal("012", DataFormat.Adjust(x,999));
        }
    }
}