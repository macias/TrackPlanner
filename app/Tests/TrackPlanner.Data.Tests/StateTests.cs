using System.Threading.Tasks;
using TrackPlanner.Shared.Visual;
using Xunit;

namespace TrackPlanner.Data.Tests
{
    public class StateTests
    {
        public class DummyDataState: IDataState
        {
            public string Label { get; set; } = default!;
            public int Value { get; set; }
        }

        [Fact]
        public async Task FastRedoTestAsync()
        {
            int x = 33;
            var manager = new StateManager<DummyDataState>((name) => new DummyDataState() {Label = name, Value = x}, s =>
            {
                x = s.Value;
                return ValueTask.CompletedTask;
            }, 100);

            using (manager.OpenScope("adding"))
            {
                x = 4;
                Assert.True(manager.CanUndo);
                Assert.False(manager.CanRedo);
            }
            using (manager.OpenScope("adding 2"))
            {
                x = 6;
                Assert.True(manager.CanUndo);
                Assert.False(manager.CanRedo);
            }

            await manager.TryUndoAsync().ConfigureAwait(false);

            Assert.True(manager.CanUndo);
            Assert.True(manager.CanRedo);

            await manager.TryUndoAsync().ConfigureAwait(false);

            Assert.False(manager.CanUndo);
            Assert.True(manager.CanRedo);
            
            using (manager.OpenScope("new"))
            {
                x = 5;
                Assert.True(manager.CanUndo);
                Assert.False(manager.CanRedo);
            }
        }
    }
    
}