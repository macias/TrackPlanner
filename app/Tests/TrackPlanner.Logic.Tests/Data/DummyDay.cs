using System;
using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Logic.Tests.Data
{
    public class DummyDay : IDay<IAnchor>
    {
        public string? Note { get; set; }
        public TimeSpan Start { get; set; }
        IReadOnlyList<IReadOnlyAnchor> IReadOnlyDay.Anchors => this.Anchors;
        IReadOnlyList<IAnchor> IDay<IAnchor>.Anchors => this.Anchors;
        public List<IAnchor> Anchors { get; set; }

        public DummyDay()
        {
            Anchors = new List<IAnchor>();
        }
    }
}