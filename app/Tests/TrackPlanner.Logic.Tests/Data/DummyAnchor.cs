using System;
using System.Threading.Tasks;
using Geo;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Logic.Tests.Data
{
    public sealed class DummyAnchor : IAnchor
    {
        public TimeSpan UserBreak { get; set; }
        public string Label { get; set; } = default!;
        public LabelSource LabelSource { get; set; }
        public bool IsPinned { get; set; }
        public bool AllowGap { get; set; }
        public bool HasMap { get; set; }
        public GeoPoint UserPoint { get; set; }
#if DEBUG
        public bool? DEBUG_Smoothing { get; set; }
        public bool? DEBUG_Enforce { get; set; }
#endif

        public DummyAnchor()
        {
            IsPinned = true;
        }
        public ValueTask SetLabelAsync(string label, LabelSource source)
        {
            this.Label = label;
            this.LabelSource = source;
            return ValueTask.CompletedTask;
        }
    }
}