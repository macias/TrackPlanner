using System.Collections.Generic;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Logic.Tests.Data
{
    public class DummySchedule : ISchedule<DummyDay,IAnchor,long,long>
    {
        public ScheduleSettings Settings { get; set; }
        IReadOnlyList<IReadOnlyDay> IReadOnlySchedule.Days => this.Days;

        public List<DummyDay> Days { get; set; }
        public RoutePlan<long, long> Route { get; set; }


        public DummySchedule()
        {
            this.Days = new List<DummyDay>();
            this.Route = new RoutePlan<long, long>();
            this.Settings = ScheduleSettings.Defaults();
            this.Settings.RouterPreferences = new UserRouterPreferences().SetCustomSpeeds();
        }
    }
}