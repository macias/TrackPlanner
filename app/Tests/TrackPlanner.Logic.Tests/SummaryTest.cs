using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Logic.Tests.Data;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackPlanner.TestToolbox;
using Xunit;

namespace TrackPlanner.Logic.Tests;

public class SummaryTest
{
    private static DummySchedule createDummySchedule()
    {
        var schedule = new DummySchedule()
        {
            Days = new List<DummyDay>()
            {
                new DummyDay()
                {
                    Anchors = new List<IAnchor>()
                    {
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(10, 10)},
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(10, 11)},
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(11, 11)}
                    }
                },
                new DummyDay()
                {
                    Anchors = new List<IAnchor>()
                    {
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(12, 11)},
                        new DummyAnchor() {UserPoint = GeoPoint.FromDegrees(12, 10)}
                    }
                },
            }
        };
        schedule.Settings.RouterPreferences.CheckpointIntervalLimit = TimeSpan.Zero;
        schedule.Settings.LoopRoute = true;

        if (!DevelModes.True)
        {
            schedule.Route = new RoutePlan<long, long>()
            {
                Legs = new List<LegPlan<long, long>>()
                {
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(100)},
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(200)},

                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(400)},
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(800)},
                    // and looped leg
                    new LegPlan<long, long>() {UnsimplifiedDistance = Length.FromMeters(20)},
                }
            };
        }
        else
        {
            var helper = new DraftRouter<long, long>(new NoLogger(),new ApproximateCalculator(),
                DraftOptions.GetLegacyTests(),
                compactPreservesRoads:FinderConfiguration.Defaults().CompactPreservesRoads);
            var response = helper.BuildPlan(schedule.BuildPlanRequest<long>(),mockReal:true, CancellationToken.None);
            schedule.Route = response.Route;
        }

        return schedule;
    }

    [Fact]
    public void SingleAnchorSummaryTest()
    {
        var schedule = new DummySchedule()
        {
            Days = new List<DummyDay>()
            {
                new DummyDay() {Anchors = new List<IAnchor>() {new DummyAnchor()}},
            },
            Route = new RoutePlan<long, long>(),
        };
        schedule.Settings.LoopRoute = true;

        var summary = schedule.CreateSummary();

        // nothing crashed = success
    }

    [Fact]
    public void EventsSummaryTest()
    {
        var schedule = new DummySchedule()
        {
            Days = new List<DummyDay>()
            {
                new DummyDay() {Anchors = new List<IAnchor>()
                {
                    new DummyAnchor(){UserPoint = GeoPoint.FromDegrees(53.21331787109375, 19.1162109375)},
                    new DummyAnchor(){UserPoint = GeoPoint.FromDegrees(52.89656448364258, 14.611816406250002)},
                }},
            },
        };
        schedule.Settings.LoopRoute = true;

        var helper = new DraftRouter<long, long>(new NoLogger(),new ApproximateCalculator(),
            DraftOptions.GetLegacyTests(),
            compactPreservesRoads:FinderConfiguration.Defaults().CompactPreservesRoads);
        var response = helper.BuildPlan(schedule.BuildPlanRequest<long>(),mockReal:false,CancellationToken.None);
        schedule.Route = response.Route;
        
        var summary = schedule.CreateSummary();
        
        Assert.Single(summary.Days);
        var day = summary.Days.Single();
        Assert.Equal(3,day.Checkpoints.Count);
        Assert.False(day.Checkpoints[0].GetEvents(summary).Any());
        Assert.False(day.Checkpoints[2].GetEvents(summary).Any());
        var atomic_events = day.Checkpoints[1].GetEvents(summary).ToArray();
        var atomic_lunch = atomic_events.Single(it => it.label == "lunch");
        var atomic_snacks = atomic_events.Single(it => it.label == "snacks");
        var total_events = ScheduleSummaryExtension.GetEventStats(day.GetEventCounters(), schedule.Settings.PlannerPreferences).ToArray();
        var total_lunch = total_events.Single(it => it.label == "lunch");
        var total_snacks = total_events.Single(it => it.label == "snacks");
        Assert.Equal(atomic_lunch,total_lunch);
        Assert.Equal(atomic_snacks,total_snacks);
        
        Assert.True(atomic_snacks.count>100);
        // currently it is 1, but maybe in a future we could handle multiday stages and assign several lunches
        Assert.True(atomic_lunch.count>0);
    }

    [Fact]
    public void SummaryDayDistancesTest()
    {
        var schedule = createDummySchedule();

        var summary = schedule.CreateSummary();

        Assert.Equal(220701, summary.Days[0].Distance.Meters,0);
        Assert.Equal(442350, summary.Days[1].Distance.Meters,0);

        Assert.Equal(663050, summary.Distance.Meters,0);
    }
    
    [Fact]
    public void SingleAnchorPointSummaryTest()
    {
        var schedule = new DummySchedule();
        schedule.Settings.LoopRoute = true;
        schedule.Days.Add(new DummyDay());
        schedule.Days[0].Anchors.Add(new DummyAnchor(){ UserPoint = GeoPoint.FromDegrees(10,20)});

        var summary = schedule.CreateSummary();

        var point = summary.Days[0].Checkpoints[0].UserPoint;
        Assert.Equal(10,point.Latitude.Degrees);
        Assert.Equal(20,point.Longitude.Degrees);
    }

    [Fact]
    public void DayDistancesByLegsTest()
    {
        var schedule = createDummySchedule();

        var legs_0 = schedule.GetDayLegs(0);
        var legs_1 = schedule.GetDayLegs(1);

        Assert.Equal(220701, legs_0.Select(it => it.UnsimplifiedDistance).Sum().Meters,0);
        Assert.Equal(442350, legs_1.Select(it => it.UnsimplifiedDistance).Sum().Meters,0);
    }

    [Fact]
    public void LastCheckpointPerDayTest()
    {
        var schedule = createDummySchedule();

        var summary = schedule.CreateSummary();

        Assert.False(summary.Days[0].Checkpoints.Last().GetEvents(summary).Any());
        Assert.False(summary.Days[1].Checkpoints.Last().GetEvents(summary).Any());
    }
}