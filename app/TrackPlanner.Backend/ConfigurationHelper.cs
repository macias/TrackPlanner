using Microsoft.Extensions.Configuration;
using TrackPlanner.Backend.Stored;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Backend
{
    public static class ConfigurationHelper
    {
        public static FinderConfiguration ReadFinderConfig(this Navigator navigator)
        {
            FinderConfiguration finder_config;
            var builder = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile(navigator.GetConfigPath("restservice_settings.json"), optional: false);

            IConfiguration config = builder.Build();

            var rest_config = new RestServiceConfig();
            config.GetSection(RestServiceConfig.SectionName).Bind(rest_config);

            finder_config = rest_config.FinderConfiguration;
            return finder_config;
        }
    }
}