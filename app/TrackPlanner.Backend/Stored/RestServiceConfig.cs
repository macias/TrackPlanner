using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Backend.Stored
{
    public sealed class RestServiceConfig
    {
        public static string SectionName { get; } = "Rest";
        public static string CorsPolicyName { get; } = "CorsPolicy";

        public string[] CorsOrigins { get; set; } = default!;

        public bool DummyRouting { get; set; }
        public string MapFolder { get; set; } = default!;
        public bool UseSummaryLightTheme { get; set; }
        public bool UseDirectionsLightTheme { get; set; }
        public SummaryTheme SummaryLightTheme { get; set; } = default!;
        public SummaryTheme SummaryDarkTheme { get; set; } = default!;

        public FinderConfiguration FinderConfiguration { get; set; } = default!;

        public SummaryTheme GetSummaryActiveTheme() => UseSummaryLightTheme ? SummaryLightTheme : SummaryDarkTheme;
        public SummaryTheme GetDirectionsActiveTheme() => UseDirectionsLightTheme ? SummaryLightTheme : SummaryDarkTheme;

        public RestServiceConfig()
        {
            // do NOT put any compound data here (like lists, arrays), ConfigurationBinder is buggy

            UseSummaryLightTheme = false;
            UseDirectionsLightTheme = true;
        }

        public static RestServiceConfig Defaults()
        {
            return new RestServiceConfig()
            {
                FinderConfiguration = FinderConfiguration.Defaults(),

                CorsOrigins = new[]
                {
                    "http://localhost:5200",
                },
                MapFolder = "poland",
                SummaryLightTheme = new SummaryTheme()
                {
                    BackgroundColor = "white",
                    TextColor = "black",
                    WarningTextColor = "red"
                },
                SummaryDarkTheme = new SummaryTheme()
                {
                    BackgroundColor = "black",
                    TextColor = "white",
                    WarningTextColor = "yellow"
                },
            };
        }
    }
}