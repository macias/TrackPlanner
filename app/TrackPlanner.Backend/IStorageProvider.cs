namespace TrackPlanner.Backend
{
    public interface IStorageProvider
    {
        IDisposable  CreateWrite(string descriptor,out Stream stream);
        IDisposable CreateRead(string descriptor,out Stream stream);
        bool Delete(string descriptor);
    }
}