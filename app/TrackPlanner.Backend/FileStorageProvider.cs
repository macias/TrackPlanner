﻿using System.IO;

namespace TrackPlanner.Backend
{
    public sealed class FileStorageProvider : IStorageProvider
    {
        private readonly string basePath;

        public FileStorageProvider(string basePath)
        {
            this.basePath = basePath;
        }

        public IDisposable CreateWrite(string descriptor,out Stream stream)
        {
            stream = new FileStream(System.IO.Path.Combine(this.basePath, descriptor), FileMode.Create, FileAccess.Write); // allowing overwrite
            return stream;
        }

        public IDisposable CreateRead(string descriptor,out Stream stream)
        {
            stream = new FileStream(System.IO.Path.Combine(this.basePath, descriptor), FileMode.Open, FileAccess.Read);
            return stream;
        }

        public bool Delete(string descriptor)
        {
            var path = System.IO.Path.Combine(this.basePath,descriptor);
            if (!System.IO.File.Exists(path))
                return false;
            System.IO.File.Delete(path);
            return true;
        }
    }
}