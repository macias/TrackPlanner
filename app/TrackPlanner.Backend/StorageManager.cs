﻿using System.Diagnostics.CodeAnalysis;
using System.Text;
using TrackPlanner.Backend.Stored;
using TrackPlanner.Structures;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;
using TimeSpan = System.TimeSpan;

namespace TrackPlanner.Backend
{
    public sealed class StorageManager<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        const int saneDayCount = 99;

        private readonly IStorageProvider streamProvider;
        private readonly RestServiceConfig serviceConfig;

        public StorageManager(IStorageProvider streamProvider,
            RestServiceConfig serviceConfig)
        {
            this.streamProvider = streamProvider;
            this.serviceConfig = serviceConfig;
        }

        public bool TryLoadSchedule(string path,
            [MaybeNullWhen(false)] out ScheduleJourney<TNodeId, TRoadId> schedule)
        {
            using (streamProvider.CreateRead(path, out var stream))
            {
                using (var reader = new StreamReader(stream, leaveOpen: true))
                {
                    var json = reader.ReadToEnd();
                    return StorageHelper.TryDeserializeSchedule(json, out schedule);
                }
            }
        }


        public void SaveAllSchedule(ScheduleJourney<TNodeId, TRoadId> schedule,
            IReadOnlyList<IReadOnlyList<PlacedAttraction>>? stores,
            IReadOnlyList<IReadOnlyList<string>>? legCities,
            string path)
        {
            saveSchedule(schedule, path);

            ConvertRouteToKml(path,schedule);
            ConvertDayRouteToKml(path, schedule, turnsMode: false);
            if (schedule.Route.DailyTurns.Any()) // checking whether turns were computed
                ConvertDayRouteToKml(path, schedule, turnsMode: true);
            
            ConvertStoresToKml(path,stores);

            SaveSummary(path, schedule,legCities,fullMode:true);
            if (!DevelModes.True)
            {
                // todo: remove this functionality 2024-09-01
                // in Poland those tips have limited use because in too many places there are zero (!) roadsigns on
                // tertiary roads, so you have to have working GPS to find out your position and direction
                SaveSummary(path, schedule, legCities, fullMode: false);
            }
        }

        private void saveSchedule(ScheduleJourney<TNodeId, TRoadId> schedule, string path)
        {
            using (streamProvider.CreateWrite(path, out var stream))
            using (TextWriter sw = new StreamWriter(stream, leaveOpen: true))
            {
                sw.Write(schedule.SaveString());
            }
        }

        internal void ConvertStoresToKml(string path, IReadOnlyList<IReadOnlyList<PlacedAttraction>>? stores)
        {
            const string file_suffix = "stores";

            var core_path = System.IO.Path.ChangeExtension(path, null);

            if (stores == null)
            {
                for (int day_idx = 0;; ++day_idx)
                {
                    if (!streamProvider.Delete(getDayFileName(core_path, day_idx,
                            Math.Max(saneDayCount, day_idx + 1), file_suffix)))
                        break;
                }
            }
            else
            {
                for (int day_idx = 0; day_idx < stores.Count; ++day_idx)
                {
                    using (streamProvider.CreateWrite(getDayFileName(core_path, day_idx,
                               Math.Max(saneDayCount, stores.Count), file_suffix), out var stream))
                    {
                        var file_title = $"Day-{day_idx + 1}";

                        var input = new TrackWriterInput() {Title = file_title};
                        input.Waypoints.AddRange(stores[day_idx]
                            .Select(it =>
                            {
                                var (attr_title, attr_desc) = it.Attraction.GetDisplay();
                                return new WaypointDefinition(it.Point, attr_title,
                                    description: attr_desc, PointIcon.StarIcon);
                            }));

                        input.SaveDecorated(stream);

                    }
                }
            }
        }

        internal void ConvertDayRouteToKml(string path, ScheduleJourney<TNodeId, TRoadId>? schedule,
            bool turnsMode)
        {
            if (schedule == null)
            {
                if (!TryLoadSchedule(path, out schedule))
                    throw new Exception("Loading schedule failed.");
            }

            var core_path = System.IO.Path.ChangeExtension(path, null);

            if (turnsMode && schedule.Days.Count != schedule.Route.DailyTurns.Count)
            {
                // this would be recipe for problems, so we just clean the files
                for (int day_idx = 0;; ++day_idx)
                {
                    if (!streamProvider.Delete(getDayFileName(core_path, day_idx,
                            Math.Max(saneDayCount, day_idx + 1), turnsMode?"turns":"route")))
                        break;
                }
            }
            else
                for (int day_idx = 0; day_idx < schedule.Days.Count; ++day_idx)
                {
                    var legs = schedule.GetDayLegs(day_idx).ToList();

                    using (streamProvider.CreateWrite(getDayFileName(core_path, day_idx,
                               Math.Max(saneDayCount, schedule.Days.Count), turnsMode?"turns":"route"), out var stream))
                    {
                        var title = $"Day-{day_idx + 1} {DataFormat.Format(legs.Select(it => it.RawTime).Sum())}, {DataFormat.FormatDistance(legs.Select(it => it.UnsimplifiedDistance).Sum(), withUnit: true)}";
                        List<TurnInfo<TNodeId, TRoadId>>? turns = null;
                        if (turnsMode)
                            turns = schedule.Route.DailyTurns[day_idx];
                        TrackWriter.SaveAsKml(schedule.VisualPreferences, stream, 
                            title,schedule.GetSummary().Days[day_idx].Checkpoints, legs, turns);
                    }
                }
        }

        internal void ConvertRouteToKml(string path, ScheduleJourney<TNodeId, TRoadId>? schedule)
        {
            if (schedule == null)
            {
                if (!TryLoadSchedule(path, out schedule))
                    throw new Exception("Loading schedule failed.");
            }

            var core_path = System.IO.Path.ChangeExtension(path, null);

            var legs = schedule.Route.Legs;

            using (streamProvider.CreateWrite(
                       $"{core_path}-all-route.kml", out var stream))
            {
                var title = $"{core_path} {DataFormat.FormatDistance(legs.Select(it => it.UnsimplifiedDistance).Sum(), withUnit: true)}";
                TrackWriter.SaveAsKml(schedule.VisualPreferences, stream,
                    title, schedule.GetSummary().Days.ZipIndex()
                        .SelectMany(it => it.item.Checkpoints.ZipIndex().Select(x => (dayIndex: it.index, ptIndex: x.index, pt: x.item)))
                        // skip starting checkpoints on the second (and later) days
                        .Where(it => it.dayIndex == 0 || it.ptIndex > 0)
                        .Select(it => it.pt),
                    legs, turns: null);
            }

        }

        private static string getDayFileName(string name, int dayIndex, int daysCount, string suffix)
        {
            // we cannot use ".day.kml" (i.e. with dot) pattern because Google Maps fails with "not supported format" (2022-05-31)
            return $"{name}-day-{DataFormat.Adjust(dayIndex + 1, daysCount)}-{suffix}.kml";
        }


        internal void SaveSummary(string path, ScheduleJourney<TNodeId, TRoadId> schedule,
            IReadOnlyList<IReadOnlyList<string>>? legCities,bool fullMode)
        {
            var theme = fullMode ? this.serviceConfig.GetSummaryActiveTheme():this.serviceConfig.GetDirectionsActiveTheme();

            var core_path = System.IO.Path.ChangeExtension(path, null);

            SummaryJourney summary = schedule.CreateSummary();

            foreach (var (day,pt_day_idx) in summary.Days.ZipIndex())
            {
                using (streamProvider.CreateWrite($"{core_path}-{DataFormat.Adjust(pt_day_idx + 1, summary.Days.Count)}-{(fullMode?"summary":"directions")}.html", out var stream))
                {
                    using (var writer = new StreamWriter(stream, leaveOpen: true))
                    {
                        writer.WriteLine(@$"<!DOCTYPE html>
<html>
                            <head>
                        <meta charset='utf-8'>
                        <title>Day {pt_day_idx + 1} summary</title>
                        </head>
<body style='background-color: {theme.BackgroundColor}; color:{theme.TextColor}'>");

                        writer.WriteLine($"<h3>Day {pt_day_idx + 1} {day.Note}</h3>");

                        foreach (var (checkpoint,pt_idx) in day.Checkpoints.ZipIndex())
                        {
                            var (anchor_day_idx,anchor_idx ) = schedule.CheckpointIndexToAnchor(pt_day_idx, pt_idx);
                            var anchor = schedule.Days[anchor_day_idx].Anchors[anchor_idx];
                            
                            // writing surfaces
                            if (checkpoint.IncomingLegIndex is { } leg_idx)
                            {
                                if (fullMode)
                                {
                                var leg = schedule.Route.Legs[leg_idx];
                                writer.Write("<small><i>");
                                {
                                    var ele = checkpoint.ElevationStats;
                                    if (DataFormat.FormatNonZeroDistance(ele.ClimbDistance, withUnit: false, out var climb_dist))
                                    {
                                        writer.Write("<div>");
                                        writer.Write($"<b>Climbs</b> {climb_dist} / {DataFormat.FormatHeight(ele.ClimbHeight, withUnit: true)}, {ele.ClimbSlope}");
                                        writer.Write("</div>");
                                    }
                                }

                                writer.Write("<div>");
                                // surface info
                                writer.Write(String.Join(", ", leg.Fragments.Partition(it => it.SpeedStyling)
                                    .Select(it =>
                                    {
                                        if (DataFormat.FormatNonZeroDistance(it.Select(x => x.UnsimplifiedFlatDistance).Sum(),
                                                withUnit: true, out var dist_info))
                                            return $"{it.First().SpeedStyling.Label.ToString().ToLowerInvariant()} {dist_info}";
                                        else
                                            return (string?) null;
                                    })
                                    .Where(it => it != null)));
                                writer.WriteLine("</div>");
                                writer.WriteLine("</i></small>");
                            }


                            if (legCities != null)
                                {
                                    var cities = legCities[leg_idx];
                                    if (cities.Any())
                                    {
                                        writer.WriteLine("<div>");
                                        writer.WriteLine($"<b><i><small>{(String.Join(", ", cities))}</small></i></b>");
                                        writer.WriteLine("</div>");
                                    }
                                }
                            }

                            writer.Write($"<div><b>{pt_idx + 1}. {(anchor.HasMap?"&#x21B1; ":"")}{checkpoint.Label}</b> ");
                            {
                                var buffer = new List<string>();
                                if (pt_idx != 0)
                                    buffer.Add(DataFormat.FormatDistance(checkpoint.IncomingDistance, true));
                                if (fullMode)
                                    buffer.Add(DataFormat.DecoFormatHeight(checkpoint.UserPoint.Altitude));

                                buffer.Add($"at {DataFormat.Format(checkpoint.Arrival)}");
                                writer.Write(String.Join(" ", buffer));
                            }

                            if (checkpoint.TotalBreakTime != TimeSpan.Zero)
                                writer.Write($" &gt;&gt; {DataFormat.Format(checkpoint.Departure)}");
                            writer.WriteLine("</div>");
                            
                            if (fullMode)
                            {
                                var events = String.Join(", ",
                                    ScheduleSummaryExtension.GetEventStats(checkpoint.EventCounters, summary.PlannerPreferences)
                                        .Select(it => $"{it.label}: {DataFormat.FormatEvent(it.count, it.duration)}"));

                                if (events != "")
                                {
                                    writer.WriteLine("<div>");
                                    writer.Write($"<i>{events}</i>");
                                    writer.WriteLine("</div>");
                                }
                            }
                        }

                        writer.Write($"<div><b>In total:</b> {DataFormat.FormatDistance(day.Distance, true)}");
                        if (day.LateCampingBy is { } late)
                            writer.Write($", <span style='color:{theme.WarningTextColor}'><b>running late by {late}</b></span>");
                        writer.Write($"</div>");
                        writer.WriteLine(@" </body>
</html>");
                    }
                }
            }
        }
    }
}