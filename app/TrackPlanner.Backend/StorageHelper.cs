using System.Diagnostics.CodeAnalysis;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Backend
{
    public static class StorageHelper
    {
        public static string SaveString<TNodeId, TRoadId>(this ScheduleJourney<TNodeId, TRoadId> schedule)
            where TNodeId : struct
            where TRoadId : struct
        {
            return ProxySerializer.Instance.Serialize(schedule);
        }

        public static bool TryDeserializeSchedule<TNodeId, TRoadId>(string json, 
            [MaybeNullWhen(false)] out ScheduleJourney<TNodeId, TRoadId> schedule)
            where TNodeId : struct
            where TRoadId : struct
        {
            schedule = ProxySerializer.Instance.Deserialize<ScheduleJourney<TNodeId, TRoadId>>(json);

            if (schedule == null)
                return false;
            
            if (schedule.Settings == null)
            {
                schedule.Settings = ScheduleSettings.Defaults();
            }

            if (schedule.Settings.RouterPreferences == null)
            {
                schedule.Settings.RouterPreferences = ScheduleSettings.Defaults().RouterPreferences;
            }

            schedule.Settings.RouterPreferences.Complete();
            
            return true;
        }


    }
}