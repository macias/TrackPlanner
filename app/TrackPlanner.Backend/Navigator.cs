﻿using System.Diagnostics.CodeAnalysis;

namespace TrackPlanner.Backend
{
    public readonly record struct Navigator
    {
        public string BaseDirectory { get; }

        public Navigator()
        {
           // string dir = Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!;
           string dir = Environment.CurrentDirectory;
            while (true)
            {
                bool app_ended = dir.EndsWith("app");
              dir =  System.IO.Path.GetDirectoryName(dir)!;
                if (app_ended)
                    break;
            }

            this.BaseDirectory = System.IO.Path.GetFullPath(dir);
        }

        public static string UniqueSaveText(string directory, string filename, IEnumerable<string> content)
        {
            string ext = System.IO.Path.GetExtension(filename);
            filename = System.IO.Path.GetFileNameWithoutExtension(filename);

            for (int i = -1;; ++i)
            {
                var path = System.IO.Path.Combine(directory, 
                    i == -1 ? $"{filename}{ext}" : $"{filename}-{i}{ext}");
                if (!tryCreateFileStream(path, FileMode.CreateNew, FileAccess.Write, FileShare.None, out var stream))
                    continue;

                using (StreamWriter writer = new StreamWriter(stream))
                {
                    foreach (var line in content)
                        writer.WriteLine(line);
                }

                return path;
            }
        }

        private static bool tryCreateFileStream(string path, FileMode mode, FileAccess access, FileShare share, 
            [MaybeNullWhen(false)] out FileStream stream)
        {
            try
            {
                stream = new FileStream(path, mode,access, share);
                return true;
            }
            catch
            {
                stream = default;
                return false;
            }
        }

        public static string GetUniquePath(string directory, string filename)
        {
            string ext = System.IO.Path.GetExtension(filename);
            string result = System.IO.Path.Combine(directory, filename);

            filename = System.IO.Path.GetFileNameWithoutExtension(filename);
            int count = 0;
            while (System.IO.File.Exists(result))
            {
                result = System.IO.Path.Combine(directory, $"{filename}-{count}{ext}");
                ++count;

            }

            return result;
        }

        public string GetMiniMaps()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "app/mini-maps");
        }

        public string GetSourceTracks()
        {
            return System.IO.Path.Combine(this.GetMiniMaps(), "source-tracks");
        }

        public string GetLegacyTracks()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "app/tracks");
        }

        public string GetBdl()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "bdl");
        }

        public string GetCustomMapsDirectory(string mapFolder)
        {
            return System.IO.Path.Combine(this.BaseDirectory, "maps",mapFolder);
        }

        public string GetOsmMapsDirectory(string mapFolder)
        {
            return System.IO.Path.Combine(this.BaseDirectory, "osm",mapFolder);
        }

        public string GetSrtmMaps()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "srtm");
        }

        public string GetOutputDirectory()
        {
            return System.IO.Path.Combine(this.BaseDirectory, "output");
        }

        public string GetOutputPath(string filename)
        {
            return  GetUniquePath(this.GetOutputDirectory(), filename);
        }

        public string GetDebugDirectory()
        {
            return GetOutputDirectory();
        }

        public void CreateSchedules()
        {
            System.IO.Directory.CreateDirectory(GetSchedules());
        }

        public string GetSchedules()
        {
            var schedules = System.IO.Path.Combine(this.BaseDirectory, "schedules");
            return schedules;
        }
        public string GetConfigPath(string configFileName)
        {
            var schedules = System.IO.Path.Combine(this.BaseDirectory, "config",configFileName);
            return schedules;
        }

        public string GetLayers()
        {
            var schedules = System.IO.Path.Combine(this.BaseDirectory, "layers");
            return schedules;
        }

        public string? GetDebugDirectory(bool enabled)
        {
            return enabled?GetDebugDirectory():null;
        }
        
        public string GetLogPath(string? logFilename = null)
        {
            return System.IO.Path.Combine(GetOutputDirectory(), logFilename?? "log.txt");
        }

    }
}