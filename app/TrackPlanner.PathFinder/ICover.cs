using System;
using System.Collections.Generic;
using TrackPlanner.Mapping;

namespace TrackPlanner.PathFinder
{
    public interface ICommonCover<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        IReadOnlyList<StepRun<TNodeId, TRoadId>> GetSteps(TNodeId sourceNodeId, TNodeId targetNodeId, bool isReversed);
    }
    public interface IBasicCover<TNodeId, TRoadId> : ICommonCover<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        bool CanUseShortcuts(TNodeId current);

        IEnumerable<(TNodeId target, Weight weight)> GetShortcuts(TNodeId nodeId, bool isReversed);

        bool CanUseDirectAdjacent(TNodeId current, TNodeId adjacent);

    }
    public interface ICover<TNodeId, TRoadId>:ICommonCover<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        bool CanUseShortcuts(TNodeId current,
            RoadBucket<TNodeId, TRoadId> startBucket, RoadBucket<TNodeId, TRoadId> endBucket);

        IEnumerable<(TNodeId target, Weight weight)> GetShortcuts(TNodeId nodeId, bool isReversed);

        bool CanUseDirectAdjacent(TNodeId current, TNodeId adjacent,
            RoadBucket<TNodeId, TRoadId> startBucket, RoadBucket<TNodeId, TRoadId> endBucket);

    }

}