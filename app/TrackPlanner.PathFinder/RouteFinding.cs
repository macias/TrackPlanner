﻿namespace TrackPlanner.PathFinder
{
    public enum RouteFinding
    {
        RouteNotFound,
        Same, // source=target
        InsufficientData
    }
    public enum LegacyRouteFinding
    {
        NotFound,
        Same, // source=target
    }
}