﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.ContractionHierarchies;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{
    internal readonly record struct MultiRoute<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public Result<(StageRun<TNodeId, TRoadId> leg,Weight weight), LegacyRouteFinding>[,] Routes { get; }

        public MultiRoute(int sources, int targets)
        {
            Routes= new Result<(StageRun<TNodeId, TRoadId> leg, Weight weight), LegacyRouteFinding>[sources, targets];
        }

        public Result<bool> Validate(IWorldMap<TNodeId,TRoadId> map,
            int sourceIdx,TNodeId source_node,
             int targetIdx, TNodeId target_node,string message, out StageRun<TNodeId, TRoadId> stage,
            out Weight weight)
        {
            message += $"-{sourceIdx}-{targetIdx}";
            
            var res_route = Routes[sourceIdx, targetIdx];
            if (!res_route.HasValue)
            {
                stage = default;
                weight = default;
                if (res_route.Failure == LegacyRouteFinding.Same)
                {
                    return Result<bool>.Valid(false);
                }

                return Result<bool>.Fail($"{message}: could not find a shortcut route {res_route.Failure}");
            }

            if (res_route.ProblemMessage != null)
            {
                stage = default;
                weight = default;
                return Result<bool>.Fail($"{message}: #{map.GetOsmNodeId(source_node)} to #{map.GetOsmNodeId(target_node)} {res_route.ProblemMessage.EnsureFullMessage()}");
            }

            (stage, weight) = res_route.Value;
            return Result<bool>.Valid(true);
        }
    }
    // A* algorithm with pairing heap
    // https://en.wikipedia.org/wiki/A*_search_algorithm
    // https://brilliant.org/wiki/pairing-heap/
    // https://en.wikipedia.org/wiki/Pairing_heap
    // http://www.brouter.de/brouter/algorithm.html

    public sealed class MultiRouteFinder<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
      


        internal static MultiRoute<TNodeId, TRoadId> TryFindRouteByNodes(ILogger logger, Navigator navigator, 
            IWorldMap<TNodeId, TRoadId> map,
            IBasicCover<TNodeId,TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            MultiWeight? weightLimit,
            FinderConfiguration sysConfig,
            UserRouterPreferences userConfig, IEnumerable<TNodeId> startNodes,
            IEnumerable<TNodeId> endNodes,
            ValidBucketPredicate<TNodeId,TRoadId> bucketPredicate,
            bool allowSmoothing,bool allowGap,
            CancellationToken cancellationToken)
        {
            var start_buckets = startNodes.Select(it =>  RoadBucket.CreateBucket(0,it,  map,bucketPredicate,
                map.Grid.Calc, 
                allowSmoothing: allowSmoothing,
                allowGap:allowGap)).ToArray();
            var end_buckets = endNodes.Select(it =>  RoadBucket.CreateBucket(1,it,  map,bucketPredicate,
                map.Grid.Calc, 
                allowSmoothing: allowSmoothing,
                allowGap:allowGap)).ToArray();

            var finder = new MultiRouteFinder<TNodeId, TRoadId>(logger, navigator, map, 
                coverGrid,connectionPredicate, sysConfig, userConfig,
                 cancellationToken);
            var result = finder.TryFindRoute(weightLimit, start_buckets, end_buckets);
            return result;
        }


        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly IBasicCover<TNodeId, TRoadId>? coverGrid;
        private readonly ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate;
        private IGeoCalculator calc => this.map.Grid.Calc;
        private readonly FinderConfiguration sysConfig;
        private readonly UserRouterPreferences userConfig;
        private readonly CancellationToken cancellationToken;

        private string? debugDirectory => this.navigator.GetDebugDirectory(this.sysConfig.EnableDebugDumping);

        private readonly HashSet<TNodeId> DEBUG_lowCostNodes;
        private readonly Dictionary<TNodeId, string> DEBUG_suppressTooFar;
        private readonly Dictionary<TNodeId, string> DEBUG_suppressInRange;
        private readonly HashSet<TNodeId> DEBUG_dangerousNodes;
        private readonly Dictionary<TNodeId, string> DEBUG_hotNodes;

        private readonly RouteLogic<TNodeId, TRoadId> logic;

//        private readonly LegFinder<TNodeId, TRoadId> finder;
        private readonly MultiStageFinder<TNodeId, TRoadId> finder;
        private readonly TotalWeightComparer preciseComparer;
        private readonly RouteShaper<TNodeId,TRoadId> shaper;

        internal MultiRouteFinder(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            IBasicCover<TNodeId,TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            FinderConfiguration sysConfig,
            UserRouterPreferences userConfig,
            CancellationToken cancellationToken)
        {
            if (userConfig.TrafficSuppression != Length.Zero)
                throw new ArgumentException();
            
            this.logger = logger;
            this.shaper = new RouteShaper<TNodeId, TRoadId>(logger, map);
            this.navigator = navigator;
            this.map = map;
            this.coverGrid = coverGrid;
            this.connectionPredicate = connectionPredicate;
            this.sysConfig = sysConfig;
            this.userConfig = userConfig;
            this.cancellationToken = cancellationToken;

            this.DEBUG_lowCostNodes = new HashSet<TNodeId>();
            this.DEBUG_suppressTooFar = new Dictionary<TNodeId, string>();
            this.DEBUG_suppressInRange = new Dictionary<TNodeId, string>();
            this.DEBUG_dangerousNodes = new HashSet<TNodeId>();
            this.DEBUG_hotNodes = new Dictionary<TNodeId, string>()
            {
            };

            var empty_supressed = new HashSet<TNodeId>();
            this.logic = new RouteLogic<TNodeId, TRoadId>(map, calc, userConfig, empty_supressed);
            this.finder = new MultiStageFinder<TNodeId, TRoadId>(logger, navigator, map,
                sysConfig,
                userConfig,
                empty_supressed);
            this.preciseComparer =  TotalWeightComparer.BothInstance;
        }

    

        internal MultiRoute<TNodeId, TRoadId> TryFindRoute(
            MultiWeight? weightLimit,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> startBuckets,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> endBuckets)
        {
            if (startBuckets.Count ==0 || endBuckets.Count==0)
                return new MultiRoute<TNodeId, TRoadId>(startBuckets.Count, endBuckets.Count);

            if (this.userConfig.SnapCyclewaysToRoads != Length.Zero)
                throw new NotSupportedException();


#if DEBUG
            //   logger.Info($"bucket[0] {buckets[0].OsmString()}");
#endif

            var stages = this.finder.TryFindStageRoute(null,
                null,
                this.coverGrid,
                connectionPredicate,
                this.preciseComparer,
                weightLimit,
                startBuckets, endBuckets,
                "proper-find",
                this.cancellationToken);

            var result = new MultiRoute<TNodeId, TRoadId>(startBuckets.Count, endBuckets.Count);
            
            for (int s =0;s<startBuckets.Count;++s)
            for (int t=0;t<endBuckets.Count;++t)
            {
                Result<WeightSteps<TNodeId,TRoadId>,LegacyRouteFinding> stage = stages.Stages[s, t]!.Value;
                
                if (!stage.HasValue)
                    result.Routes[s, t] = stage.FailAs<(StageRun<TNodeId, TRoadId> leg, Weight weight)>();
                else
                {
                    var route_leg = new StageRun<TNodeId, TRoadId>(stage.Value.Steps.ToList(),
                        isDraft: stage.Value.IsDraft);
                    Result<ValueTuple> shape_res = this.shaper.ShapeRouteLegs(
                        new[] {startBuckets[s], endBuckets[t]},
                        new List<StageRun<TNodeId, TRoadId>>() {route_leg});
                    if (shape_res.HasValue)
                    {
                        result.Routes[s, t] = Result<(StageRun<TNodeId, TRoadId> leg, Weight weight), LegacyRouteFinding>.Valid((route_leg, stage.Value.JoinedWeight))
                            .WithMessage(stage)
                            .WithMessage(shape_res,force:false);
                    }
                    else
                    {
                        result.Routes[s, t] = Result<(StageRun<TNodeId, TRoadId> leg, Weight weight),
                            LegacyRouteFinding>.Fail(LegacyRouteFinding.NotFound,
                            stage.ProblemMessage ?? shape_res.ProblemMessage);
                    }
                }
            }

            dumpPostDebug();

            return result;
        }

        private void dumpPostDebug()
        {
            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpLowCost)
            {
                var input = new TrackWriterInput();
                foreach (var node in DEBUG_lowCostNodes)
                    input.AddPoint(map.GetPoint(node), icon: PointIcon.DotIcon);

                string filename = Navigator.GetUniquePath(this.navigator.GetDebugDirectory(), $"low-cost.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpTooFar)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_suppressTooFar)
                {
                    if (!this.DEBUG_suppressInRange.ContainsKey(entry.Key))
                        input.AddPoint(map.GetPoint(entry.Key), label: entry.Value, icon: PointIcon.CircleIcon);
                }

                string filename = Navigator.GetUniquePath(this.debugDirectory!, $"too-far.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpInRange)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_suppressInRange)
                    input.AddPoint(map.GetPoint(entry.Key), label: entry.Value, icon: PointIcon.DotIcon);

                string filename = Navigator.GetUniquePath(this.debugDirectory!, $"in-range.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (sysConfig.EnableDebugDumping && this.sysConfig.DumpDangerous)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_dangerousNodes)
                    input.AddPoint(map.GetPoint(entry), label: "noname", icon: PointIcon.DotIcon);

                string filename = Navigator.GetUniquePath(this.debugDirectory!, $"dangerous.kml");
                input.BuildDecoratedKml().Save(filename);
            }
        }


       
    }
}