﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.PathFinder.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    internal sealed class RouteShaper<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly ILogger logger;
        private readonly IWorldMap<TNodeId, TRoadId> map;

        internal RouteShaper(ILogger logger,IWorldMap<TNodeId,TRoadId> map)
        {
            this.logger = logger;
            this.map = map;
        }


        public ContextMessage? FlattenRoundabouts(List<LegFlow<TNodeId, TRoadId>> legs)
        {
            // keep in mind this code should be removed really. It is all done for the sake
            // of old TrackRadar, once we improve it, this code is gone
            
            ContextMessage? problem = null;
            foreach (var leg in legs)
            {
                if (leg.IsDraft)
                    continue;
                
                try
                {
                        flattenRoundaboutLinks(leg.Flow);
                        this.ValidateFlowSteps(leg.Flow);
                        smoothRoundaboutCrosspoints(leg.Flow);
                        this.ValidateFlowSteps(leg.Flow);
                }
                catch (Exception ex)
                {
                    ex.LogDetails(this.logger,MessageLevel.Error);
                    problem = problem.Override(  ContextMessage.Create( ex));
                }
            }

            return problem;
        }
        
        internal static void ClearSingularLegs(List<StageRun<TNodeId, TRoadId>> stages)
        {
            for (int i = stages.Count - 1; i >= 0; --i)
                // when the leg is so short is has only single point, replace it with empty leg
                if (stages[i].Steps.Count == 1)
                    stages[i] = new StageRun<TNodeId, TRoadId>(
                        new List<StepRun<TNodeId, TRoadId>>(), isDraft: stages[i].IsDraft);
        }

        

        private void smoothRoundaboutCrosspoints(List<FlowJump<TNodeId, TRoadId>> pathSteps)
        {
            RouteHelper<TNodeId,TRoadId>.LegCheck(pathSteps);
            
            for (int center_idx = pathSteps.Count - 1; center_idx >= 0;)
            {
                var path_step = pathSteps[center_idx];

                if (!path_step.Place.IsRoundaboutCenter)
                {
                    --center_idx;
                    continue;
                }

                if (!COMP_MODE.PhantomRounabouts)
                {
                    // (around 2023-04-01)
                    // after introducing concept of phantom roundabout centers AND trying to keep
                    // map compact by making connections only from crossroads on roundabouts
                    // we cannot longer jump into center from arbitrary node on the roundabout.
                    // Solutions:
                    // a) build all connections for roundabouts
                    // b) disable turn notifications when going around roundabout
                    // But since this is the problem only for start/end of the leg (i.e. quite rare)
                    // let's wait for better ideas. After all maybe we remove phantom roundabouts
                    // at all
                    tryReduceRoundaboutCrossPoint(pathSteps, center_idx, Direction.Forward);
                    center_idx -= tryReduceRoundaboutCrossPoint(pathSteps, center_idx, Direction.Backward);
                }

                if (center_idx == 1)
                {
                    pathSteps[0] = pathSteps[0].MirrorRoadsForInitial(pathSteps[1]);
                }

                --center_idx;
            }
            RouteHelper<TNodeId,TRoadId>.LegCheck(pathSteps);
        }

        private void flattenRoundaboutLinks(List<FlowJump<TNodeId, TRoadId>> pathSteps)
        {
            RouteHelper<TNodeId,TRoadId>.LegCheck(pathSteps);

#if DEBUG
            var osm = pathSteps.Select(_ => -1L).ToList();
#endif
            for (int step_idx = pathSteps.Count - 1; step_idx >= 0;)
            {
                var path_step = pathSteps[step_idx];

                if (!path_step.Place.IsRoundaboutModel)
                {
                    --step_idx;
                    continue;
                }

                var info = this.map.GetRoadInfo(path_step.Place.RoundaboutCenterRoadId!.Value);
                // we had a case in Torun/Poland at 53.0118, 18.56459 and going east
                // via Bydgoska, program assumed it is super long exit and removed
                // all the nodes, so we have some stupid limit -- why not roundabout itself?
                // If the link contains more nodes than roundabout it looks like it is not a link.
                int lame_link_limit = info.Nodes.Count;
                var center_index_offset = (path_step.Place.IsRoundaboutCenter?1:0);
                
                tryReduceRoundaboutLink(pathSteps, step_idx+center_index_offset, Direction.Forward,lame_link_limit);
                int entry_idx = step_idx - center_index_offset;
                entry_idx -= tryReduceRoundaboutLink(pathSteps, entry_idx, 
                    Direction.Backward,lame_link_limit);

                if (entry_idx <= 0)
                {
                    pathSteps[0] = pathSteps[0].MirrorRoadsForInitial(pathSteps[1]);
                    
                    // maybe it should be set to all (roundabout center)->(link)
                    // cases because the roads will be different (guaranteed) so then
                    // route will be split between center and its link
                    
                    // this condition is in order to avoid splitting center/link just at the start
                    // because it would create 1-point fragment (with center). So we override
                    // road for the link to incorrect one, but working
                    if (pathSteps[1].Place.IsRoundaboutLink)
                        pathSteps[1] = pathSteps[1] with {IncomingRoadId = pathSteps[0].IncomingRoadId};
                }

                step_idx = entry_idx - 1;
            }

            RouteHelper<TNodeId,TRoadId>.LegCheck(pathSteps);
        }

       

        public void legCheck(IReadOnlyList<StepRun<TNodeId, TRoadId>> pathSteps)
        {
            if (pathSteps.Count == 1)
                throw new ArgumentException($"Only single step for leg");
            if (pathSteps.Count == 0)
                return;

            var init_step = pathSteps.First();
            if (init_step.IncomingDistance != Length.Zero)
                throw new ArgumentOutOfRangeException($"Initial step is expected to be zero length, it is {init_step.IncomingDistance}.");
        }

        private int tryReduceRoundaboutCrossPoint(List<FlowJump<TNodeId, TRoadId>> steps,
            int roundaboutIndex,
            Direction dir)
        {
            // if we have scenario like this
            // segment crosspoint - node - roundabout center
            // and the crosspoint lies on the rounadbout, then cut out the node and go
            // directly to the center:
            // crosspoint - roundabout center
            
            var roundabout = steps[roundaboutIndex];

            var node_idx = roundaboutIndex + dir.AsChange();
            if (!steps.IndexInRange(node_idx))
                return 0;
            FlowJump<TNodeId, TRoadId> node = steps[node_idx];
            if (!node.Place.IsNode)
                return 0;

            var cx_idx = node_idx + dir.AsChange();
            if (!steps.IndexInRange(cx_idx))
                return 0;
            if (!steps[cx_idx].Place.IsCrossSegment)
                return 0;

            if (!EqualityComparer<TRoadId>.Default.Equals(roundabout.Place.RoundaboutCenterRoadId!.Value,
                    steps[cx_idx].Place.BaseRoadIndex.RoadId))
                return 0;

            steps.RemoveAt(node_idx);

            var hot_step = steps[roundaboutIndex + dir.AsChange()];
            steps[roundaboutIndex + dir.AsChange()] = hot_step with
            {
                IncomingDistance = hot_step.IncomingDistance + node.IncomingDistance,
                IncomingTime = hot_step.IncomingTime + node.IncomingTime,
                Place = hot_step.Place.AddRoundaboutLink()
            };

            return 1;
        }

        private int tryReduceRoundaboutLink(List<FlowJump<TNodeId, TRoadId>> steps, int startIndex,
            Direction dir,int nodeCountLimit)
        {
            if ( startIndex<0 || startIndex>=steps.Count || !steps[startIndex].Place.IsNode)
                return 0;

            int idx = startIndex;
            Length total_dist = Length.Zero;
            TimeSpan total_time = TimeSpan.Zero;

            while (true)
            {
                var read_index = idx;
                if (dir == Direction.Forward)
                    ++read_index;

                // todo: we need to tell the difference between trying finding a link in a roundabout
                // O
                // V ___ here, the tip is the link to roundabout
                // and infinity searching because the road is parallel
                // O
                // ||
                // there is no link, and those two roads could not be connected in shared node
                
                // if we didn't stop so far there is no link
                if (read_index <= 0 || read_index >= steps.Count)
                {
                    return 0;
                }

                var s = steps[read_index];
                if (!s.IncomingRelativeTraffic.IsOneWay)
                    break;
                /*var serious = this.map.GetAdjacentRoads(s.Place.NodeId)
                    .Select(it => this.map.GetRoad(it.RoadId))
                    .Where(it => it.Kind.IsSerious()).ToArray();
                if (serious.Count()==2 )
                  */  

                total_dist += s.IncomingDistance;
                total_time += s.IncomingTime;

                idx += dir.AsChange();
            }

            int count = Math.Abs(startIndex - idx);
            // if we exceeded the limit it means it wasn't short link, but rather regular road
            if (count == 0 || count>nodeCountLimit)
                return 0;

            // if our start point is roundabout model as well, it means we just bounced/touched
            // roundabout, so we didn't create true center of it
            var has_true_roundabout_center = !steps[startIndex].Place.IsRoundaboutBounce;

            if (dir == Direction.Forward)
            {
                var hot_step = steps[idx];
                steps[idx] = hot_step with
                {
                    IncomingDistance = total_dist,
                    IncomingTime = total_time, 
                    Place = hot_step.Place.AddRoundaboutLink()
                };
                if (!has_true_roundabout_center)
                {
                    --count;
                    ++startIndex;
                }

                steps.RemoveRange(startIndex, count);
            }
            else
            {
                steps[idx] = steps[idx] with
                {
                    Place = steps[idx].Place.AddRoundaboutLink()
                };
                if (has_true_roundabout_center)
                    ++startIndex;
                else
                    --count;
                // we are updating info at roundabout center (or "touch point")
                steps[startIndex] = steps[startIndex] with
                {
                    IncomingDistance = total_dist, 
                    IncomingTime = total_time
                };
                steps.RemoveRange(idx + 1, count);
            }

            return count;
        }
        
        public Result<ValueTuple> ShapeRouteLegs(IReadOnlyList<RoadBucket<TNodeId, TRoadId>> buckets,
            List<StageRun<TNodeId, TRoadId>> routeLegs)
        {
            ContextMessage? problem = null;
            
            // note: adjacent legs have shared place
            problem=problem.Override( SmoothLegJoints(buckets, routeLegs));
            
            foreach (var leg in routeLegs)
            {
                if (leg.Steps.Count != 0)
                {
                    if (!dropPointCrosspoints(leg.Steps, out var p))
                        return Result.FailVoid(p);

                    problem = problem.Override( p);
                }
            }
            #if DEBUG
            problem = problem.Override( validateLegs(routeLegs));
            #endif

            return Result.ValidVoid(problem);
        }

        public ContextMessage? SmoothLegJoints(IReadOnlyList<RoadBucket<TNodeId, TRoadId>> buckets,
            List<StageRun<TNodeId, TRoadId>> rawLegs)
        {
            ContextMessage? problem = null;

            // for looped request do not smooth out start/end and do not force start/end point to be exactly the same
            // NOTE: in case of single leg this loop won't run at all...
            foreach (var (prev_idx, next_idx, prev_leg, next_leg) in rawLegs.ZipIndex()
                         .Where(it => it.item.Steps.Count > 0)
                         .Slide()
                         .Select(it => (it.prev.index, it.next.index, it.prev.item, it.next.item)))
            {

                int bucket_idx = prev_idx + 1; // the shared bucket between legs
                // the buckets are in between, so no +1 at end
                FinderHelper.IsSmoothingJointAllowed(prev_idx, next_idx, buckets,
                    out bool smoothing_allowed,out bool gap_allowed);
                if (smoothing_allowed && !prev_leg.IsDraft && !next_leg.IsDraft) 
                    problem = problem.Override(smoothLegJoints(buckets[bucket_idx], prev_leg, next_leg));

                if (!gap_allowed && !prev_leg.Steps.Last().Place.MatchesCoordinates(next_leg.Steps.First().Place))
                    problem = problem.Override( $"Legs {prev_idx}-{next_idx} are not connected");
            }

            return problem;
        }


        private ContextMessage? smoothLegJoints(RoadBucket<TNodeId, TRoadId> sharedBucket,
            in StageRun<TNodeId, TRoadId> previousLeg,
            in StageRun<TNodeId, TRoadId> nextLeg)
        {
            // each legs is stripped from start/end user points already

            // check if we have joint in the first place
            if (previousLeg.Steps[^1].Place.Point != nextLeg.Steps[0].Place.Point)
                return null;
            
            // smoothing connections between legs

            // the first and last points are segment crosspoint (or sometimes NODES),
            // the second to them are real nodes
            // if we share the same node skip both crosspoints and starting (!) shared nodes,
            // to avoid diagrams like this
            //           | 
            //           |
            //  ---------o--* U
            // o node
            // * crosspoint
            // U user point
            // we don't check more shared nodes, because this removal is neglible,
            // but in general user could go to some place and return
            // using partially of the same path

            bool first_step = true; // nextLeg.Steps[0].Place.IsCrossPoint;

            while (true)
            {
                if (previousLeg.Steps.Count < 2 || nextLeg.Steps.Count < 2)
                    break;

                var same_followers = sameNode(previousLeg.Steps[^2].Place,
                    nextLeg.Steps[1].Place);

                if (!first_step && !same_followers)
                    break;

                first_step = false;

                if (same_followers)
                {
                    previousLeg.Steps.RemoveLast();
                    nextLeg.Steps.RemoveFirst();
                    nextLeg.Steps[0] = nextLeg.Steps[0].MirrorRoadsForInitial(nextLeg.Steps[1]);
                }
                else
                {
                    // if the nodes next to crosspoint are not the same it means, we remove
                    // crosspoint, and "stretch" given leg
                    // ----o----*----o-----
                    // the split between legs was at "*" (crosspoint) but now we shift it to adjacent node
                    // note, the case like
                    // ----o----@----o-----
                    // is also possible (@ segment crosspoint, which lies exactly at the node)

                    var is_prev_covered = nextLeg.Steps[0].Place.IsCrosspointAdjacentToNode( this.map,
                        previousLeg.Steps[^2].Place.NodeId);
                    var is_next_covered = previousLeg.Steps[^1].Place.IsCrosspointAdjacentToNode(this.map,
                            nextLeg.Steps[1].Place.NodeId);
                    
                    if (is_prev_covered && is_next_covered) // crosspoint between nodes
                    {
                        nextLeg.Steps[0] = previousLeg.Steps[^2]
                          .MirrorRoadsForInitial(previousLeg.Steps[^1]);
                        // this placement is not changed, but since step 0 is moved
                        // it could be the case we now use different road, so we have to set it
                        nextLeg.Steps[1] = nextLeg.Steps[1] with
                        {
                            IncomingRoadId = nextLeg.Steps[0].IncomingRoadId,
                            IncomingCondition = nextLeg.Steps[0].IncomingCondition
                        };
                        nextLeg.Steps[1] = nextLeg.Steps[1].AddTravel(previousLeg.Steps.Last());
                        previousLeg.Steps.RemoveLast();
                    }
                    else  
                    {
                        // only crosspoint POINT (at node) is shared
                        
                        // possible case
                        //    X----@
                        //    |
                        //    |
                        //    o
                        // X -- last NODE and crosspoint at previous leg
                        // @ -- first NODE of the next leg
                        // | -- the cross segment of the previous leg
                        // - -- the cross segment of the next leg
                        // NOTE: cross point has to be shared. Cross-segment can be shared but does not have to
                        
                        if (!nextLeg.Steps[0].Place.TryGetSharedCrosspointNode2(this.map, previousLeg.Steps[^1].Place,
                                out var common_node))
                            return new ContextMessage("Unable to find shared crosspoint", MessageLevel.Error);
                        
                        // the segment crosspoint lies exactly at the node, so we cannot remove it
                        // only convert it
                        nextLeg.Steps[0] = nextLeg.Steps[0] with
                        {
                            Place = nextLeg.Steps[0].Place.AsNode(this.map, common_node)
                        };
                        previousLeg.Steps[^1] = previousLeg.Steps[^1] with
                        {
                            Place = previousLeg.Steps[^1].Place.AsNode(this.map, common_node)
                        };

                        // now remove duplicated place
                        if (previousLeg.Steps[^1].Place.MatchesCoordinates(previousLeg.Steps[^2].Place))
                            previousLeg.Steps.RemoveLast();
                        if (nextLeg.Steps[0].Place.MatchesCoordinates(nextLeg.Steps[1].Place))
                        {
                            nextLeg.Steps.RemoveFirst();
                            // we have to mirror road because for the next place we could end up with mismatch
                            nextLeg.Steps[0] = nextLeg.Steps[0]
                                .MirrorRoadsForInitial(nextLeg.Steps[1]);
                        }
                    }
                }

                // keep removing same nodes as long the nodes were in snap-range 
                if (previousLeg.Steps.Count < 2 || nextLeg.Steps.Count < 2
                                                  // the current one has to be within snap
                                                  || !sharedBucket.ReachableNodes.Contains(previousLeg.Steps[^1].Place.NodeId))
                    break;
            }

            return null;
        }

        private static bool sameNode(Placement<TNodeId, TRoadId> place, Placement<TNodeId, TRoadId> other)
        {
            if (place.IsNode && other.IsNode &&
                EqualityComparer<TNodeId>.Default.Equals(place.NodeId,
                    other.NodeId))
                return true;
            else
                return false;
        }

         private bool dropPointCrosspoints(List<StepRun<TNodeId, TRoadId>> legs, 
             out ContextMessage? problem)
        {
            problem = null;

            if (legs[0].Place.IsCrossPoint)
            {
                if (legs[0].Place.IsCrossSegment)
                {
                    if (legs.Count > 1 && !legs[0].Place.IsCrossPointAdjacent(this.map, legs[1].Place))
                        problem = problem.Override( $"Start crosspoint {legs[0].Place.CrossPointSpanningNodesInfo(this.map)} is not adjacent to {legs[1].Place.OsmString(map)}");
                    else if (legs.Count > 1 && legs[0].Place.Point == legs[1].Place.Point)
                     {
                         if (legs.Count == 2)
                         {
                             legs.Clear();
                             return true;
                         }
                         else
                         {
                             legs.RemoveFirst();
                             legs[0] = legs[0].MirrorRoadsForInitial(legs[1]);
                         }
                     }
                }
                else
                {
                    if (legs[0].Place.Point != legs[1].Place.Point)
                        problem = problem.Override( $"Start pointy-crosspoint {legs[0].Place.OsmString(map)} does not overlap with {legs[1].Place.OsmString(map)}");
                    else
                    {
                        legs.RemoveFirst();
                        legs[0] = legs[0].MirrorRoadsForInitial(legs[1]);
                    }
                }
            }


            if (legs[^1].Place.IsCrossPoint)
            {
                if (legs[^1].Place.IsCrossSegment)
                { 
                    if (legs.Count > 1 && !legs[^1].Place.IsCrossPointAdjacent(this.map, legs[^2].Place))
                        problem = problem.Override( $"End crosspoint {legs[^1].Place.CrossPointSpanningNodesInfo(this.map)} is not adjacent to {legs[^2].Place.OsmString(this.map)}");
                    else if (legs.Count > 1 && legs[^1].Place.Point == legs[^2].Place.Point)
                    {
                        legs.RemoveLast();
                    }
                }
                else{
                    if (legs[^1].Place.Point != legs[^2].Place.Point)
                        problem = problem.Override( $"End pointy-crosspoint {legs[^1].Place.OsmString(map)} does not overlap with {legs[^2].Place.OsmString(map)}");
                    else
                    {
                        legs.RemoveLast();
                    }
                }
               }

            // maybe not perfect but if all we have are two crosspoints (so user points were super close to each
            // other) basically it is empty route
            if (legs.Count <= 2 && legs[0].Place.IsCrossPoint && legs[^1].Place.IsCrossPoint)
            {
                    legs.Clear();
                    return true;
            }

            return true;
        }
       
         public ContextMessage? validateLegs(IReadOnlyList<StageRun<TNodeId, TRoadId>> legs)
         {
             ContextMessage? problem = null;
             // ... so we have to have separate loop just for validation
             foreach (var raw_leg in legs)
             {
                 if (raw_leg.Steps.Count==0)
                     continue;
                 
                 if (raw_leg.Steps.First().IncomingDistance != Length.Zero 
                     || raw_leg.Steps.First().IncomingTime != TimeSpan.Zero)
                     problem = problem.Override( $"Initial step should be zero, it is {raw_leg.Steps.First().IncomingDistance} in {raw_leg.Steps.First().IncomingTime}");
             }

             return problem;
         }

        public Result<ValueTuple> RemoveUserPoints(
            RoadBucket<TNodeId, TRoadId> startBucket, RoadBucket<TNodeId, TRoadId> endBucket,
            List<StepRun<TNodeId, TRoadId>> steps)
        {
            ContextMessage? problem = null;

            if (steps.Count != 0)
            {
                if (!steps[0].Place.IsUserPoint)
                    problem = problem.Override( $"Path does not start with user point {steps[0].Place}.");
                else
                {
                    if (steps[0].Place.Point.Convert2d() != startBucket.UserPoint)
                        problem = problem.Override($"Path start out of sync with bucket {steps[0].Place.Point} != {startBucket.UserPoint}.");
                    steps.RemoveFirst();
                    steps[0] = steps[0].MirrorRoadsForInitial(steps[1]);
                }

                if (!steps[0].Place.IsCrossPoint)
                    return Result.FailVoid($"Path start does not follow with cross point {steps[0].Place}.");

                if (!steps[^1].Place.IsUserPoint)
                    problem = problem.Override( $"Path does not end with user point {steps[^1].Place}.");
                else
                {
                    if (steps[^1].Place.Point.Convert2d() != endBucket.UserPoint)
                        problem = problem.Override( $"Path end out of sync with bucket {steps[^1].Place.Point} != {endBucket.UserPoint}.");
                    steps.RemoveLast();
                }

                if (!steps[^1].Place.IsCrossPoint)
                    return Result.FailVoid($"Path end does not follow with cross point {steps[^1].Place}.");
                ;

                if (steps.First().IncomingDistance != Length.Zero || steps.First().IncomingTime != TimeSpan.Zero)
                    problem = problem.Override( $"Initial step should be zero, it is {steps.First().IncomingDistance} in {steps.First().IncomingTime}");
            }
            
            if (steps.ZipIndex().Skip(1).SkipLast(1)
                    .FirstOrNone(it => it.item.Place.IsCrossPoint) is {HasValue: true} cx_opt)
                problem = problem.Override( $"{cx_opt.Value.index} step is crosspoint");


            return Result.ValidVoid(problem);
        }

        public void ValidateFlowSteps(List<LegFlow<TNodeId, TRoadId>> legs)
        {
            foreach (var leg in legs)
            {
                if (!leg.IsDraft)
                    ValidateFlowSteps(leg.Flow);
            }
        }

        public void ValidateFlowSteps(List<FlowJump<TNodeId, TRoadId>> steps)
        {
            for(int i=0;i<steps.Count;++i)
            {
                var step = steps[i];
                    this.map.GetNode(step.FacingBackwardIndex); // just checking if indices are sane
                    this.map.GetNode(step.FacingForwardIndex);
            }
        }


    }
}