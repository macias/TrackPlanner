﻿using MathUnit;
using System;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    internal readonly record struct BacktrackInfo<TNodeId,TRoadId>
        where TNodeId: struct
        where TRoadId : struct
    {
        // for prestart and for shortcuts these are null
        public TRoadId? IncomingRoadId { get; }
        public RoadCondition? IncomingCondition { get; }

        // for shortcuts these are null
        public Length? IncomingDistance { get; }
        public TimeSpan? IncomingTime { get; }
        
        public Edge<TNodeId,TRoadId> Source { get; }
        public bool IsShortcut { get; }
        
        public int Timestamp { get; init; }
        
#if DEBUG
        #endif

        public BacktrackInfo(Edge<TNodeId,TRoadId> source,
            TRoadId? incomingRoadId, RoadCondition? incomingCondition, 
            Length? incomingDistance, TimeSpan? incomingTime,
            bool isShortcut)
        {
            IncomingCondition = incomingCondition;
            Source = source;
            IncomingRoadId = incomingRoadId;
            IncomingDistance = incomingDistance;
            IncomingTime = incomingTime;
            IsShortcut = isShortcut;
            Timestamp = 0;
        }
    }




}