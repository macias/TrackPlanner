﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    [StructLayout(LayoutKind.Auto)]
    public readonly struct Placement<TNodeId, TRoadId> : IEquatable<Placement<TNodeId, TRoadId>>, IMapZPoint
        where TNodeId : struct
        where TRoadId : struct
    {
#if DEBUG
        private static long DEBUG_idCounter;
        public long DEBUG_Identifier { get; } = getIdentifier();

        private static long getIdentifier()
        {
            var result = DEBUG_idCounter++;
            if (result == 52)
            {
                ;
            }
            return result;
        }
#endif
        public static Placement<TNodeId, TRoadId> Node(IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<TRoadId> roadIndex)
        {
            var node_id = map.GetNode(roadIndex);
            var kind = PlaceKind.Node;
            TRoadId? roundabout_road_id = null;
            {
                var is_center = map.GetNodeInfo(node_id).IsRoundaboutCenter;
                if (is_center)
                {
                    kind |= PlaceKind.RoundaboutCenter;
                    // get complementary index of the star center (those segments are always
                    // with 2 nodes)
                    var adj_star_idx = map.GetNode(new RoadIndex<TRoadId>(roadIndex.RoadId,
                        1 - roadIndex.IndexAlongRoad));
                    // get the road which is an actual roundabout
                    roundabout_road_id = map.GetRoadsAtNode(adj_star_idx)
                        .First(it => map.GetRoadInfo(it.RoadId).IsRoundabout)
                        .RoadId;
                }
            }

            return new Placement<TNodeId, TRoadId>(node_id, map.GetPoint(node_id), baseRoadIndex: roadIndex,
                roundabout_road_id,kind);
        }

        public static Placement<TNodeId, TRoadId> Prestart(GeoZPoint point)
        {
            return new Placement<TNodeId, TRoadId>(null, point, baseRoadIndex: null, 
                roundaboutCenterRoadId: null,
                PlaceKind.Prestart);
        }

        public static Placement<TNodeId, TRoadId> Draft(GeoZPoint point)
        {
            return new Placement<TNodeId, TRoadId>(null, point, baseRoadIndex: null, 
                roundaboutCenterRoadId: null, PlaceKind.Draft);
        }

        public static Placement<TNodeId, TRoadId> UserPoint(RoadBucket<TNodeId, TRoadId> bucket)
        {
            return new Placement<TNodeId, TRoadId>(null, bucket.UserPoint.Convert3d(), baseRoadIndex: null,
                roundaboutCenterRoadId: null, PlaceKind.UserPoint);
        }

        public static Placement<TNodeId, TRoadId> Crosspoint(IWorldMap<TNodeId, TRoadId> map,
            GeoZPoint point, RoadIndex<TRoadId> baseRoadIndex, SnapKind atSegment)
        {
            var flags = PlaceKind.CrossPoint;
            if (atSegment == SnapKind.Segment)
                flags |= PlaceKind.Segment;
        

        return new Placement<TNodeId, TRoadId>(map.GetNode(baseRoadIndex), point,
                baseRoadIndex: baseRoadIndex,
                roundaboutCenterRoadId:null,
                flags);
        }

        public GeoZPoint Point { get; }
        private readonly PlaceKind kind;
        public bool IsUserPoint => kind.HasFlag(PlaceKind.UserPoint);
        public bool IsRoundaboutCenter => kind.HasFlag(PlaceKind.RoundaboutCenter);
        public bool IsRoundaboutBounce => kind.HasFlag(PlaceKind.RoundaboutBounce);

        public bool IsRoundaboutModel => this.IsRoundaboutBounce || this.IsRoundaboutCenter;

        //  public bool IsRoundaboutArm => kind.HasFlag(PlaceKind.RoundaboutCenter) && this.kind.HasFlag(PlaceKind.Auxiliary);
        public bool IsCrossPoint => kind.HasFlag(PlaceKind.CrossPoint);
        public bool IsCrossSegment => IsCrossPoint && kind.HasFlag(PlaceKind.Segment);

        public bool IsNode => HasNodeFlag; 

        private bool HasNodeFlag => this.kind.HasFlag(PlaceKind.Node);

        public bool IsRoundaboutLink => kind.HasFlag(PlaceKind.RoundaboutLink);
        public bool IsPrestart => kind.HasFlag(PlaceKind.Prestart);

        private readonly TNodeId? nodeId;
        public TRoadId? RoundaboutCenterRoadId { get; }

        // consumer should check it via IsNode, Crosspoint can set node at different place than point 
        public TNodeId NodeId
        {
            get
            {
                if (this.nodeId is not { } id)
                    throw new Exception($"Not a node: {this.kind}");
                return id;
            }
        }

        public RoadIndex<TRoadId>? NodeIndexOrNull => IsNode ? BaseRoadIndex : null;
        
        public string KindInfo => this.kind.ToString();

        // in case of segment crosspoints, the spanning node is simply +1, but we cannot tell
        // if the crosspoint is closer to this index or the next one
        private readonly RoadIndex<TRoadId>? baseRoadIndex;
        // applies only to any crosspoint (single node, or between two nodes) and nodes
        public RoadIndex<TRoadId> BaseRoadIndex => this.baseRoadIndex!.Value;

        // applies only to crosssegment (crosspoint between two nodes)
        public RoadIndex<TRoadId>? NextRoadIndex => IsCrossSegment ? BaseRoadIndex.Next() : null;

        public Placement()
        {
            throw new InvalidOperationException();
        }

       
        private Placement(TNodeId? nodeId, GeoZPoint point, 
            RoadIndex<TRoadId>? baseRoadIndex,TRoadId? roundaboutCenterRoadId, PlaceKind kind)
        {
            if (this.DEBUG_Identifier == 57)
            {
                ;
            }
            this.kind = kind;
            this.Point = point;
            this.nodeId = nodeId;
            this.RoundaboutCenterRoadId = roundaboutCenterRoadId;
            this.baseRoadIndex = baseRoadIndex;
        }

        /*private Placement(TNodeId nodeId, GeoZPoint point) : this(nodeId,point, associatedRoadId:null,
            associatedIndexAlongRoad:null,PlaceKind.Node)
        {
        }*/

        public override int GetHashCode()
        {
            return HashCode.Combine(Point, kind, this.nodeId);
        }

        public override bool Equals(object? obj)
        {
            return obj is Placement<TNodeId, TRoadId> place && Equals(place);
        }

        public bool Equals(Placement<TNodeId, TRoadId> other)
        {
            return MatchesCoordinates(other)
                   && kind == other.kind;
        }

        public bool MatchesCoordinates(Placement<TNodeId, TRoadId> other)
        {
            return Point == other.Point
                   // we cannot compare here base index, because we can have multiple indices
                   // for single node
                   && EqualityComparer<TNodeId?>.Default.Equals(this.nodeId, other.nodeId);
        }

        public static bool operator ==(Placement<TNodeId, TRoadId> left, Placement<TNodeId, TRoadId> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Placement<TNodeId, TRoadId> left, Placement<TNodeId, TRoadId> right)
        {
            return !(left == right);
        }

        public (TNodeId, TNodeId?) CrossPointSpanningNodes(IWorldMap<TNodeId, TRoadId> map)
        {
            if (!this.IsCrossPoint)
                throw new ArgumentException();

            if (this.NextRoadIndex is { } next_idx)
                return (this.NodeId, map.GetNode(next_idx));
            else
                return (this.NodeId, null);
        }

        public bool TryGetSharedCrosspointNode(IWorldMap<TNodeId, TRoadId> map,
            in Placement<TNodeId, TRoadId> other,
            out TNodeId shared)
        {
            var (this_n1, this_n2) = CrossPointSpanningNodes(map);
            var (other_n1, other_n2) = other.CrossPointSpanningNodes(map);
            {
                if (EqualityComparer<TNodeId>.Default.Equals(this_n1, other_n1)
                    || (other_n2 is { } on2 && EqualityComparer<TNodeId>.Default.Equals(this_n1, on2)))
                {
                    shared = this_n1;
                    return true;
                }
            }

            {
                if (this_n2 is { } tn2 && (EqualityComparer<TNodeId>.Default.Equals(tn2, other_n1)
                                           || (other_n2 is { } on2 && EqualityComparer<TNodeId>.Default.Equals(tn2, on2))))
                {
                    shared = tn2;
                    return true;
                }
            }

            shared = default;
            return false;
        }

        public bool TryGetSharedCrosspointNode2(IWorldMap<TNodeId, TRoadId> map,
            in Placement<TNodeId, TRoadId> other,
            out RoadIndex<TRoadId> shared)
        {
            var (this_n1, this_n2) = (this.BaseRoadIndex,NextRoadIndex);
            var (other_n1, other_n2) = (other.BaseRoadIndex,other.NextRoadIndex);
            {
                if (this_n1== other_n1
                    || (other_n2 is { } on2 && this_n1== on2))
                {
                    shared = this_n1;
                    return true;
                }
            }

            {
                if (this_n2 is { } tn2 && (tn2== other_n1
                                           || (other_n2 is { } on2 && tn2== on2)))
                {
                    shared = tn2;
                    return true;
                }
            }

            shared = default;
            return false;
        }

        public string CrossPointSpanningNodesInfo(IWorldMap<TNodeId, TRoadId> map)
        {
            var (n1, n2) = CrossPointSpanningNodes(map);
            var result = $"{osmNodeCoordsInfo(map, n1)}" + (n2 is { } nn2 ? $"-{osmNodeCoordsInfo(map, nn2)}" : "");
#if DEBUG
            result = $"{debugInfo()} {result}";
#endif
            return result;
        }
#if DEBUG
        private string debugInfo()
        {
            return $"DBG[{this.DEBUG_Identifier}]";
        }
#endif

        public bool IsCrossPointAdjacent(IWorldMap<TNodeId, TRoadId> map,
            Placement<TNodeId, TRoadId> adjPlace)
        {
            if (!this.IsCrossPoint || !adjPlace.nodeId.HasValue)
                return false;

            return IsCrosspointAdjacentToNode(map, adjPlace.NodeId);
        }

        private static string coordsInfo<TId>(TId? id, GeoZPoint point)
            where TId : struct
        {
            if (id is { } nid)
                return $"#{nid}({point.Convert2d()})";
            else
                return $"{point.Convert2d()}";
        }

        private static string osmNodeCoordsInfo(IWorldMap<TNodeId, TRoadId> map, TNodeId id)
        {
            return coordsInfo<OsmId>(map.GetOsmNodeId(id), map.GetPoint(id));
        }

        private string getFullInfo<TId>(TId? node)
            where TId : struct
        {
            var result = $"{coordsInfo(node, this.Point)} {{{this.kind}}}";
#if DEBUG
            result = $"{debugInfo()} {result}";
#endif
            return result;
        }

        public string OsmString(IWorldMap<TNodeId, TRoadId> map)
        {
            OsmId? osm_id;
            if (this.IsNode)
                osm_id = map.GetOsmNodeId(this.NodeId);
            else
                osm_id = null;
            return getFullInfo(osm_id);
        }

        public override string ToString()
        {
            return getFullInfo<TNodeId>(this.IsNode? this.NodeId:null);
        }

     /*   public Placement<TNodeId, TRoadId> StripToRaw()
        {
            return new Placement<TNodeId, TRoadId>(nodeId: null, this.Point, baseRoadId: null,
                baseIndexAlongRoad: null, PlaceKind.None);
        }
*/
        public bool IsCrosspointAdjacentToNode(IWorldMap<TNodeId, TRoadId> map, TNodeId adjNodeId)
        {
            if (!this.IsCrossPoint)
                return false;

            var (n1, n2) = CrossPointSpanningNodes(map);

            if (EqualityComparer<TNodeId>.Default.Equals(n1, adjNodeId))
                return true;
            if (n2 is { } nn2 && EqualityComparer<TNodeId>.Default.Equals(nn2, adjNodeId))
                return true;

            return false;
        }

        public Placement<TNodeId, TRoadId> StripSegment(IWorldMap<TNodeId, TRoadId> map, RoadIndex<TRoadId> rebase)
        {
            return new Placement<TNodeId, TRoadId>(nodeId: this.nodeId, this.Point,
                baseRoadIndex: rebase,
                roundaboutCenterRoadId: null,
                (kind | PlaceKind.Segment) ^ PlaceKind.Segment);
        }

        public Placement<TNodeId, TRoadId> AddRoundaboutLink()
        {
            return new Placement<TNodeId, TRoadId>(nodeId: this.nodeId, this.Point, 
                baseRoadIndex: this.baseRoadIndex,
                roundaboutCenterRoadId: this.RoundaboutCenterRoadId, 
                (kind | PlaceKind.RoundaboutLink));
        }

        public Placement<TNodeId, TRoadId> AddRoundaboutBounce(RoadIndex<TRoadId> roundabotIndex)
        {
            return new Placement<TNodeId, TRoadId>(nodeId: this.nodeId, this.Point,
                baseRoadIndex: roundabotIndex,
                roundaboutCenterRoadId: roundabotIndex.RoadId, 
                (kind | PlaceKind.RoundaboutBounce));
        }

        public Placement<TNodeId, TRoadId> AsNode(IWorldMap<TNodeId, TRoadId> map, RoadIndex<TRoadId> nodeIndex)
        {
            return new Placement<TNodeId, TRoadId>(nodeId: map.GetNode(nodeIndex), map.GetPoint(nodeIndex),
                baseRoadIndex: nodeIndex,
                roundaboutCenterRoadId:  null,
                PlaceKind.Node | (this.IsRoundaboutLink ? PlaceKind.RoundaboutLink : PlaceKind.None));
        }

        public RoadIndex<TRoadId>? GetBaseRoadIndex()
        {
            return this.baseRoadIndex;
        }

        public MapZPoint<TNodeId,TRoadId> GetCoords(IWorldMap<TNodeId,TRoadId>? map)
        {
            #if DEBUG
            MapRef<TNodeId,TRoadId>? map_ref = MapRef.Create(map, GetBaseRoadIndex());
            #endif
            return new MapZPoint<TNodeId,TRoadId>(
                Point, this.IsNode ? NodeId : null
#if DEBUG
            , map_ref
#endif
                );
        }

#if DEBUG
        public bool DEBUG_IsConnectedWith(Placement<TNodeId, TRoadId> other, int dbgId1, int dbgId2)
        {
            return (this.DEBUG_Identifier == dbgId1 || this.DEBUG_Identifier == dbgId2)
                   && (other.DEBUG_Identifier == dbgId1 || other.DEBUG_Identifier == dbgId2);
        }
#endif
    }
}
