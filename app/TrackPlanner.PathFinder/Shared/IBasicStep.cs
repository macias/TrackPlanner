﻿using MathUnit;
using System;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder.Shared
{
    public interface IBasicStep<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        TimeSpan IncomingTime { get; }
        RoadCondition IncomingCondition { get; }
        Length IncomingDistance { get; }

        TRoadId IncomingRoadId { get; }

        MapZPoint<TNodeId, TRoadId> GetRefCoords(IWorldMap<TNodeId, TRoadId>? map);
    }
}