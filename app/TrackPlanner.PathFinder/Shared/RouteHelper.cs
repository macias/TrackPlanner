﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.Shared
{
    public sealed class RouteHelper<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly ILogger logger;
        private readonly IWorldMap<TNodeId, TRoadId>? map;
        private readonly TimeSpan checkpointIntervalLimit;
        private readonly bool compactPreservesRoads;

        public RouteHelper(ILogger logger,
            IWorldMap<TNodeId, TRoadId>? map,
            TimeSpan checkpointIntervalLimit,
            bool compactPreservesRoads
            //IWorldMap<TNodeId, TRoadId> map, 
        )
        {
            this.logger = logger;
            this.map = map;
            this.checkpointIntervalLimit = checkpointIntervalLimit;
            this.compactPreservesRoads = compactPreservesRoads;
            /*
            this.map = map;
            this.calc = new ApproximateCalculator();
            this.shaper = new RouteShaper<TNodeId, TRoadId>(logger, map);
            */
        }

        private ContextMessage? splitSteps(LegFlow<TNodeId,TRoadId> sourceLeg,
            CancellationToken token, out List<LegPlan<TNodeId, TRoadId>> planLegs)
        {
            ContextMessage? problem = null;
            planLegs = new List<LegPlan<TNodeId, TRoadId>>();

            void add_leg(List<LegPlan<TNodeId, TRoadId>> list, LegPlan<TNodeId, TRoadId> leg)
            {
                leg.IsDrafted = sourceLeg.IsDraft;
                list.Add(leg);
                var p = leg.Validate();
                if (problem == null && p != null)
                    problem = p;
            }

            TimeSpan leg_time_limit;
            int expected_leg_pieces;
            {
                var time_total = sourceLeg.Flow.Select(it => it.IncomingTime).Sum();
                if (sourceLeg.IsDraft 
                    || time_total == TimeSpan.Zero || this.checkpointIntervalLimit == TimeSpan.Zero)
                {
                    leg_time_limit = TimeSpan.Zero;
                    expected_leg_pieces = 0;
                }
                else
                {
                    expected_leg_pieces = (int) Math.Ceiling(time_total / this.checkpointIntervalLimit);
                    leg_time_limit = time_total / expected_leg_pieces;

                    this.logger.Info($"Leg total time {time_total} with limit {this.checkpointIntervalLimit} split into {expected_leg_pieces} pieces, each {leg_time_limit}");
                }
            }

            var leg = new LegPlan<TNodeId, TRoadId>() {AutoAnchored = false};
            TimeSpan running_time = TimeSpan.Zero;

            MapZPoint<TNodeId, TRoadId>? last_point = null;

            foreach (var (step_run,step_idx) in sourceLeg.Flow.ZipIndex())
            {
                token.ThrowIfCancellationRequested();

                MapZPoint<TNodeId, TRoadId> curr_coords = step_run.GetRefCoords(this.map);
                GeoZPoint current_point = curr_coords.Point;

                if (leg.Fragments.Any())
                {
                    // if the current leg part is too long, we have to return it and create another leg
                    if (expected_leg_pieces > 1 // remember, we will return tail leg at the end (out of the loop)
                        && running_time + step_run.IncomingTime > leg_time_limit)
                    {
                        --expected_leg_pieces;
                        leg.ComputeLegTotals();
                        //logger.Info($"Returning split leg {leg.RawTime}");
                        add_leg(planLegs, purgeLeg(leg));

                        leg = new LegPlan<TNodeId, TRoadId>() {AutoAnchored = true};
                        running_time = TimeSpan.Zero;
                    }
                }

                var speed_mode = step_run.IncomingCondition.SpeedStyling;

                {
                    bool start_new_fragment = !leg.Fragments.Any();
                    if (!start_new_fragment)
                    {
                        LegFragment<TNodeId, TRoadId> last_fragment = leg.Fragments.Last();
                        if (this.compactPreservesRoads)
                            start_new_fragment = !EqualityComparer<TRoadId>.Default.Equals(last_fragment.RoadIds.Single(),
                                step_run.IncomingRoadId)
                                // todo: previous is center and current is the link
                                                 //&& !(step_idx==1 && pathSteps[step_idx-1].IncomingCondition)
                                                 ;
                        else
                            start_new_fragment = last_fragment.SpeedStyling != speed_mode 
                                                 || last_fragment.IsForbidden!=step_run.IncomingCondition.IsForbidden;
                    }

                    if (start_new_fragment)
                    {
                        var new_fragment = new LegFragment<TNodeId, TRoadId>()
                                {
                                    CostInfo = step_run.IncomingCondition.CostInfo,
                                    IsForbidden = step_run.IncomingCondition.IsForbidden,
                                }
                                .SetSpeedMode(speed_mode)
                            ;

                        if (last_point is { } last_pt)
                        {
                            // we need to draw it, so the beginning of the new segment has to start where the last one ended
                            new_fragment.Steps.Add(new FragmentStep<TNodeId, TRoadId>(
#if DEBUG
                                last_pt.DEBUG_MapRef,
#endif
                                last_pt, Length.Zero));
                        }

                        leg.Fragments.Add(new_fragment);
                    }
                }

                var fragment = leg.Fragments.Last();

                fragment.Steps.Add(new FragmentStep<TNodeId, TRoadId>(
#if DEBUG
                    curr_coords.DEBUG_MapRef,
#endif
                    current_point,
                    curr_coords.NodeId,
                    step_run.IncomingDistance));

                fragment.RoadIds.Add(step_run.IncomingRoadId);
                fragment.RawTime += step_run.IncomingTime;
                fragment.UnsimplifiedFlatDistance += step_run.IncomingDistance;

                running_time += step_run.IncomingTime;

                last_point = fragment.GetSteps().Last().GetMapPoint();
            }


            if (leg.Fragments.Any())
            {
                leg.ComputeLegTotals();
                //logger.Info($"Returning last leg, auto = {leg.AutoAnchored}, time {leg.RawTime}");
                add_leg(planLegs, purgeLeg(leg));
            }

            return problem;
        }


        public RoutePlan<TNodeId, TRoadId> SplitLegs<TReqPoint>(
            IReadOnlyList<TReqPoint> requestPoints,
            IEnumerable<LegFlow<TNodeId,TRoadId>> pathLegs,
            CancellationToken token)
            where TReqPoint:IRequestPoint
        {
            var route = new RoutePlan<TNodeId, TRoadId>();

            ContextMessage? problem = null;
            foreach (var (leg_steps, idx) in pathLegs.ZipIndex())
            {
                LegCheck(leg_steps.Flow);

                if (DEBUG_SWITCH.Enabled && idx == 1)
                {
                    ;
                }

                if (leg_steps.Flow.Count == 0)
                {
                    var leg = new LegPlan<TNodeId, TRoadId>();
                    leg.AllowGap = requestPoints[idx].AllowGap;
                    leg.IsDrafted = leg_steps.IsDraft;
                    route.Legs.Add(leg);
                }
                else
                {
                    problem = problem.Override(splitSteps( leg_steps, token, out var plan_legs));
                    if (plan_legs.Count>0)
                        plan_legs[0].AllowGap = requestPoints[idx].AllowGap;
                   // plan_legs.ForEach(it => it.IsDrafted = leg_steps.IsDraft);

                    int count = route.Legs.Count;
                    route.Legs.AddRange(plan_legs);
                    if (count == route.Legs.Count)
                        problem = problem.Override($"Splitting {idx} leg did not produce anything.");
                }
            }

#if DEBUG
            problem = route.Validate();
            if (problem != null)
            {
                ;
            }
#endif

            return route;
        }


        private LegPlan<TNodeId, TRoadId> purgeLeg(LegPlan<TNodeId, TRoadId> leg)
        {
            // removing duplicate points, and then removing entire fragments
            // if there is only one point
            // typical scenario is when user put two points close to each other
            // with only one road, those points could be snapped into same node
            // resulting in zero-segment
            for (int i = leg.Fragments.Count - 1; i >= 0; --i)
            {
                if (tryRemoveDuplicatePoints(leg.Fragments[i])
                    && leg.Fragments[i].GetSteps().Count == 1)
                {
                    leg.Fragments.RemoveAt(i);
                }
            }

            return leg;
        }


        private bool tryRemoveDuplicatePoints(LegFragment<TNodeId, TRoadId> fragment)
        {
            var count = fragment.GetSteps().Count;
            if (count < 2)
                return false;

            // we need to keep at least two points, even the same -- this is for computing route
            // summary, otherwise we wouldn't be able to create checkpoints (checkpoint uses 3D points
            // from route, not 2D ones from the request/plan)
            for (int i = count - 1; i > 0 && fragment.GetSteps().Count > 2; --i)
                if (fragment.GetSteps()[i].Point == fragment.GetSteps()[i - 1].Point)
                {
                    fragment.Steps.RemoveAt(i);
                }

            return count != fragment.GetSteps().Count;
        }

        public static void LegCheck<TStep>(IReadOnlyList<TStep> pathSteps)
            where TStep : IBasicStep<TNodeId, TRoadId>
        {
            if (pathSteps.Count == 1)
                throw new ArgumentException($"Only single step for leg");
            if (pathSteps.Count == 0)
                return;

            var init_step = pathSteps.First();
            if (init_step.IncomingDistance != Length.Zero)
                throw new ArgumentOutOfRangeException($"Initial step is expected to be zero length, it is {init_step.IncomingDistance}.");
        }
    }
}