﻿using System;
using System.Runtime.InteropServices;

namespace TrackPlanner.PathFinder
{
    [StructLayout(LayoutKind.Auto)]
    public readonly struct TotalWeight : IEquatable<TotalWeight>
    {
        public static TotalWeight Zero { get; } = new TotalWeight(Weight.Zero, Weight.Zero);

        public Weight Current { get; }
        public Weight Remaining { get; }

       // public TravelCost TotalTimeCost=>this.Current.TravelCost + Remaining.TravelCost;

        public TotalWeight(Weight current, Weight remaining)
        {
            Current = current;
            this.Remaining = remaining;
        }

        public Weight Combine()
        {
            return Current + Remaining;
        }
        public Weight CostlessAndRemaining()
        {
            return  Current.GetCostless()+Remaining;
        }
        public static TotalWeight OBSOLETE_JoinFinal(in TotalWeight a, in TotalWeight b)
        {
            return new TotalWeight(( a.Current+b.Current), Weight.Zero);
        }
        public static Weight JoinFinal(in TotalWeight a, in TotalWeight b)
        {
            return a.Current+b.Current;
        }

       /* public TravelCost GetRemainingTimeCost(Speed estimatedSpeed)
        {
            return TravelCost.Create(RemainingCost/estimatedSpeed,costScale: 1.0) ;
        }*/

        public override string ToString()
        {
            return $"{Current}; Remaining {Remaining}; Total {Current+Remaining}";
        }

        public override bool Equals(object? obj)
        {
            return obj is TotalWeight weight && Equals(weight);
        }

        public bool Equals(TotalWeight other)
        {
            return Remaining == other.Remaining
                   && Current == other.Current;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Remaining, Current);
        }

        public static bool operator ==(TotalWeight left, TotalWeight right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(TotalWeight left, TotalWeight right)
        {
            return !(left == right);
        }
    }
}