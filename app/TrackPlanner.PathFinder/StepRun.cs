﻿using MathUnit;
using System;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    
    public readonly record struct StepRun<TNodeId, TRoadId> : IBasicStep<TNodeId,TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public TRoadId IncomingRoadId { get; init; } // in case of starting point it is actually the outgoing road (because there is no incoming one)

        public RoadCondition IncomingCondition { get; init; }
        public Length IncomingDistance { get; init; }
        public TimeSpan IncomingTime { get; init; }

        public Placement<TNodeId, TRoadId> Place { get; init; }


        public StepRun(Placement<TNodeId, TRoadId> place, TRoadId incomingRoadId,
            RoadCondition incomingCondition,
            Length incomingDistance, TimeSpan incomingTime)
        {
            Place = place;
            IncomingRoadId = incomingRoadId;
            IncomingCondition = incomingCondition;
            IncomingDistance = incomingDistance;
            IncomingTime = incomingTime;
        }

        public StepRun<TNodeId, TRoadId> MirrorRoadsForInitial(in StepRun<TNodeId, TRoadId> nextStep)
        {
            return new StepRun<TNodeId, TRoadId>(this.Place, nextStep.IncomingRoadId,
                nextStep.IncomingCondition,
                Length.Zero, TimeSpan.Zero);
        }

        /*public StepRun<TNodeId, TRoadId> WithRawPlace()
        {
            return this with {Place = Place.StripToRaw()};
        }
*/
        /*public StepRun<TNodeId, TRoadId> WithRegularCrossPoint()
        {
            return this with {Place = Place.StripSegment()};
        }
*/
        public StepRun<TNodeId, TRoadId> AddTravel(StepRun<TNodeId, TRoadId> extra)
        {
            return this with
            {
                IncomingDistance = this.IncomingDistance + extra.IncomingDistance,
                IncomingTime = this.IncomingTime + extra.IncomingTime
            };
        }
        public MapZPoint<TNodeId, TRoadId> GetRefCoords(IWorldMap<TNodeId, TRoadId>? map)
        {
            return Place.GetCoords(map);
        }
    }
}