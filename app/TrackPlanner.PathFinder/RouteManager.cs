﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Structures;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;


namespace TrackPlanner.PathFinder
{
    public readonly record struct RouteManagerData<TNodeId, TRoadId>(
        List<LegFlow<TNodeId, TRoadId>> routeFlow,
        Weight routeWeight)
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
    }

    public sealed class RouteManager<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private static readonly IGeoCalculator calculator = new ApproximateCalculator();

        public static IDisposable Create(ILogger logger, Navigator navigator,
            string mapFolder, FinderConfiguration finderConfiguration,
            out RouteManager<TNodeId, TRoadId> manager)
        {
            if (finderConfiguration == null)
                throw new ArgumentNullException(nameof(finderConfiguration));

            var osm_reader = new MapReader(logger, calculator, finderConfiguration.MemoryParams,
                finderConfiguration.MapSettings,
                getDebugDirectory(navigator, finderConfiguration));

            double start = Stopwatch.GetTimestamp();

            var disposable = osm_reader.ReadMap<TNodeId, TRoadId>(
                navigator.GetSrtmMaps(),
                navigator.GetOsmMapsDirectory(mapFolder),
                navigator.GetCustomMapsDirectory(mapFolder),
                onlyRoads: true, out var out_map) ?? CompositeDisposable.None;

            disposable = CompositeDisposable.Stack(disposable,
                () => { logger.Info($"STATS {nameof(out_map)} {out_map.GetStats()}"); });
            logger.Info($"Loading map in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");


            manager = new RouteManager<TNodeId, TRoadId>(logger, navigator, worldMap: out_map,
                finderConfiguration);
            return disposable;
        }

        private static string? getDebugDirectory(Navigator navigator, FinderConfiguration finderConfiguration)
        {
            return navigator.GetDebugDirectory(finderConfiguration.EnableDebugDumping);
        }

        public static RouteManager<TNodeId, TRoadId> Create(ILogger logger, Navigator navigator,
            IWorldMap<TNodeId, TRoadId> worldMap, FinderConfiguration finderConfiguration)
        {
            return new RouteManager<TNodeId, TRoadId>(logger, navigator, worldMap, finderConfiguration);
        }

        private readonly ILogger logger;
        public IWorldMap<TNodeId, TRoadId> Map { get; }
        private readonly Navigator navigator;
        private CoverGrid<TNodeId, TRoadId>? coverGrid;
        private readonly RouteShaper<TNodeId, TRoadId> shaper;
        public FinderConfiguration FinderConfig { get; }
        public IGeoCalculator Calculator => calculator;

        public string? DebugDirectory { get; }

        private RouteManager(ILogger logger, Navigator navigator,
            IWorldMap<TNodeId, TRoadId> worldMap,
            FinderConfiguration finderConfiguration)
        {
            this.logger = logger;

            this.navigator = navigator;
            logger.Info($"{this} with baseDirectory: {navigator.BaseDirectory}");

            this.FinderConfig = finderConfiguration;

            this.DebugDirectory = getDebugDirectory(navigator, this.FinderConfig);

            this.Map = worldMap;

            this.shaper = new RouteShaper<TNodeId, TRoadId>(logger, Map);
        }


        public Result<RouteManagerData<TNodeId, TRoadId>> TryFindFlattenRoute(UserRouterPreferences userConfig,
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken token)
        {
            // the last point of given leg is repeated as the first point of the following leg
            var result = tryFindVanillaRoute(userConfig, userPoints, token);

            if (!result.HasValue)
                return result.FailAs<RouteManagerData<TNodeId, TRoadId>>();

            var problem = result.ProblemMessage;
            var routeFlow = convertSteps(userPoints, result.Value.RouteLegs,
                out ContextMessage? p);
            problem = problem.Override(p);

            shaper.ValidateFlowSteps(routeFlow);

            if (problem == null)
            {
                // we flatten roundabouts only if there was no previous problem
                problem = shaper.FlattenRoundabouts(routeFlow!);
            }

            return Result<RouteManagerData<TNodeId, TRoadId>>.Valid(new RouteManagerData<TNodeId, TRoadId>(routeFlow,
                result.Value.RouteWeight), problem);
        }

        private List<LegFlow<TNodeId, TRoadId>> convertSteps(
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            List<StageRun<TNodeId, TRoadId>> legs,
            out ContextMessage? problem)
        {
            problem = validateCrosspoints(userPoints, legs);

            foreach (var leg in legs)
            {
                if (leg.IsDraft)
                    continue;

                for (int i = 0; i < leg.Steps.Count; ++i)
                {
                    var step = leg.Steps[i];
                    if (!step.Place.IsNode)
                        continue;
                    if ((i > 0 && leg.Steps[i - 1].Place.IsRoundaboutCenter)
                        || (i < leg.Steps.Count - 1 && leg.Steps[i + 1].Place.IsRoundaboutCenter))
                        continue;
                    Option<RoadIndex<TRoadId>> roundabout_idx = Map.GetRoadsAtNode(step.Place.NodeId)
                        .FirstOrNone(it => Map.GetRoadInfo(it.RoadId).IsRoundabout);
                    if (!roundabout_idx.HasValue)
                        continue;

                    leg.Steps[i] = step with {Place = step.Place.AddRoundaboutBounce(roundabout_idx.Value)};
                }
            }

            return legs.ZipIndex().Select(it =>
            {
                if (it.item.IsDraft)
                    return new LegFlow<TNodeId, TRoadId>(
                        it.item.Steps.Select(FlowJump<TNodeId, TRoadId>.Create).ToList(),
                        isDraft: true);
                else
                    return convertRealSteps(this.Map,it.index, it.item.Steps);
            }).ToList();
        }

        private static ContextMessage? validateCrosspoints(IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            IReadOnlyList<StageRun<TNodeId, TRoadId>> legs)
        {
            var first_leg_idx = legs.FindIndex(it => it.Steps.Count != 0);
            var last_leg_idx = legs.FindLastIndex(it => it.Steps.Count != 0);

            var prev_leg_idx = -1;
            bool check_prev_crosspoint = false;
            for (int leg_idx = first_leg_idx; leg_idx != -1;)
            {
                var next_leg_idx = legs.FindIndex(leg_idx + 1, it => it.Steps.Count != 0);
                bool check_next_crosspoint;
                if (next_leg_idx == -1)
                    check_next_crosspoint = false;
                else
                {
                    FinderHelper.IsSmoothingJointAllowed(leg_idx, next_leg_idx, userPoints,
                        out bool smoothing_allowed, out bool gap_allowed);
                    check_next_crosspoint = smoothing_allowed && !gap_allowed;
                }

                if (!legs[leg_idx].IsDraft)
                    for (int step_idx = 0; step_idx < legs[leg_idx].Steps.Count; ++step_idx)
                    {
                        // absolute first/last place can be crosspoint
                        if ((leg_idx == first_leg_idx && step_idx == 0)
                            || (step_idx == 0 && leg_idx != first_leg_idx && legs[prev_leg_idx].IsDraft)
                            || (step_idx == legs[leg_idx].Steps.Count - 1 && leg_idx != last_leg_idx && legs[next_leg_idx].IsDraft)
                            || (leg_idx == last_leg_idx && step_idx == legs[leg_idx].Steps.Count - 1)
                            || (step_idx == 0 && !check_prev_crosspoint)
                            || (step_idx == legs[leg_idx].Steps.Count - 1 && !check_next_crosspoint))
                            continue;

                        StepRun<TNodeId, TRoadId> step = legs[leg_idx].Steps[step_idx];
                        if (step.Place.IsCrossPoint)
                        {
                            return new ContextMessage($"Leg {leg_idx}/{last_leg_idx + 1}, at step {step_idx}/{legs[leg_idx].Steps.Count} contains {(step.Place.IsCrossSegment ? "segment" : "point")} crosspoint");
                        }
                    }

                check_prev_crosspoint = check_next_crosspoint;
                prev_leg_idx = leg_idx;
                leg_idx = next_leg_idx;
            }

            return null;
        }

        private static LegFlow<TNodeId, TRoadId> convertRealSteps(IWorldMap<TNodeId, TRoadId> map,
            int DEBUG_legIndex,
            IReadOnlyList<StepRun<TNodeId, TRoadId>> steps)
        {
            var result = new List<FlowJump<TNodeId, TRoadId>>(capacity: steps.Count);
            if (steps.Count == 0)
                return new LegFlow<TNodeId, TRoadId>(new List<FlowJump<TNodeId, TRoadId>>(),
                    isDraft: false);

            for (int i = 0; i < steps.Count; ++i)
            {
                var s = steps[i];
                result.Add(FlowJump<TNodeId, TRoadId>.Create(s));
            }

            for (int i = 0; i < result.Count; ++i)
            {
                var flow = result[i];

                if (flow.Place.IsNode)
                {
                    var prev_valid = i > 0;
                    var next_valid = i < result.Count - 1;

                    bool prev_computed = false;
                    bool next_computed = false;

                    foreach (var adj_idx in map.GetAdjacentRoads(flow.Place.NodeId))
                    {
                        var adj_node = map.GetNode(adj_idx);


                        if (prev_valid && EqualityComparer<TRoadId>.Default.Equals(steps[i].IncomingRoadId, adj_idx.RoadId)
                                       && map.IsCoveredWith(adj_node, result[i - 1].Place))
                        {
                            prev_computed = true;
                            result[i - 1] = result[i - 1] with
                            {
                                FacingForwardIndex = adj_idx
                            };
                        }

                        if (next_valid && EqualityComparer<TRoadId>.Default.Equals(steps[i + 1].IncomingRoadId, adj_idx.RoadId)
                                       && map.IsCoveredWith(adj_node, result[i + 1].Place))
                        {
                            next_computed = true;
                            result[i + 1] = result[i + 1] with
                            {
                                FacingBackwardIndex = adj_idx
                            };
                        }
                    }

                    if (prev_valid && !prev_computed)
                        throw new InvalidOperationException("Could not find previous adjacent");
                    if (next_valid && !next_computed)
                        throw new InvalidOperationException($"Could not find next adjacent {steps[i + 1].Place.OsmString(map)} from {DEBUG_legIndex}:{i} at {flow.Place.OsmString(map)} by {map.GetOsmRoadId(steps[i + 1].IncomingRoadId)}");
                }
                else if (flow.Place.IsCrossSegment)
                {
                    var cx_index = flow.Place.BaseRoadIndex;
                    // we add end node of the segment, because we would like to spread first and last
                    // entry, in order to have space to calculate turn (if any)
                    var cx_node = map.GetNode(cx_index);
                    if (i > 0)
                    {
                        RoadIndex<TRoadId> prev;
                        if (EqualityComparer<TNodeId>.Default.Equals(cx_node, result[i - 1].Place.NodeId))
                            prev = cx_index;
                        else
                            prev = cx_index.Next();

                        result[i - 1] = result[i - 1] with {FacingForwardIndex = prev};
                    }

                    if (i < result.Count - 1)
                    {
                        RoadIndex<TRoadId> next;
                        if (EqualityComparer<TNodeId>.Default.Equals(cx_node, result[i + 1].Place.NodeId))
                            next = cx_index;
                        else
                            next = cx_index.Next();

                        result[i + 1] = result[i + 1] with {FacingBackwardIndex = next};
                    }
                }
                else
                    throw new ArgumentException($"Unexpected place kind {flow.Place.KindInfo}");
            }

            result[0] = result[0]with {FacingBackwardIndex = result[0].FacingForwardIndex};
            result[^1] = result[^1]with {FacingForwardIndex = result[^1].FacingBackwardIndex};

            // computing flow/traffic direction
            for (int i = 1; i < result.Count; ++i)
            {
                var from_index = result[i - 1].FacingForwardIndex;
                var dest_index = result[i].FacingBackwardIndex;
                Result<TrafficDirection<TRoadId>> dir
                    = map.GetTrafficDirection(from: from_index, dest: dest_index);

                if (!dir.HasValue)
                {
                    throw new Exception($"Index: {i}, {dir.ProblemMessage.TryFullMessage()}");
                }

                result[i] = result[i] with
                {
                    IncomingRelativeTraffic = dir.Value
                };
            }

            result[0] = result[0] with {IncomingRelativeTraffic = result[1].IncomingRelativeTraffic};

            return new LegFlow<TNodeId, TRoadId>(result, isDraft: false);
        }


        private Result<RouteFinderData<TNodeId, TRoadId>> tryFindVanillaRoute(UserRouterPreferences routerConfig,
            IReadOnlyList<RequestPoint<TNodeId>> userPoints, CancellationToken token)
        {
            var result = RouteFinder<TNodeId, TRoadId>.TryFindRouteByPoints(this.logger, this.navigator,
                this.Map, this.coverGrid, this.FinderConfig, routerConfig,
                userPoints, token);

            if (!result.HasValue)
                return result.FailTrim();

            var stages = result.Value.RouteLegs;
            if (FinderConfig.EnableDebugDumping && FinderConfig.DumpRawRoute)
            {
                var input = new TrackWriterInput();
                foreach (var (stage, stage_idx) in stages.ZipIndex())
                {
                    foreach (var (place, place_idx) in stage.Steps.Select(it => it.Place).ZipIndex())
                        input.AddPoint(place.Point, icon: PointIcon.DotIcon, label: $"[{stage_idx}:{place_idx}] {place.OsmString(Map)}");
                }

                string filename = Navigator.GetUniquePath(this.navigator.GetDebugDirectory(),
                    $"raw-route.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            RouteShaper<TNodeId,TRoadId>.ClearSingularLegs(stages);
            
            return result.FailTrim();
        }

        internal Result<RouteFinderData<TNodeId, TRoadId>> TryFindVanillaRoute(UserRouterPreferences routerConfig,
            IReadOnlyList<TNodeId> userPoints, bool allowSmoothing, bool allowGap, CancellationToken token)
        {
            var result = RouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                this.Map, this.coverGrid, connectionPredicate: null, weightLimit: null, this.FinderConfig, routerConfig,
                userPoints, allowSmoothing,
                allowGap: allowGap, token);
            return result.FailTrim();
        }

        public void EnableShortcuts(CoverGrid<TNodeId, TRoadId> coverGrid)
        {
            this.coverGrid = coverGrid;
        }

        public Result<RoutePlan<TNodeId, TRoadId>> TryFindCompactRoute(UserRouterPreferences userConfig,
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken token)
        {
            return TryFindCompactRoute(userConfig, userPoints, token, out _);
        }

        internal Result<RoutePlan<TNodeId, TRoadId>> TryFindCompactRoute(UserRouterPreferences userConfig,
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            CancellationToken token,
            out Weight? routeWeight)
        {
            var result = TryFindFlattenRoute(userConfig, userPoints, token);
            if (!result.HasValue)
            {
                routeWeight = default;
                return result.FailAs<RoutePlan<TNodeId, TRoadId>>();
            }

            var problem = result.ProblemMessage;
            var route = CompactFlattenRoute(userConfig, userPoints, result.Value.routeFlow,
                token, out var p);
            problem = problem.Override(p);

            routeWeight = result.Value.routeWeight;
            return Result<RoutePlan<TNodeId, TRoadId>>.Valid(route, problem);
        }

        public RoutePlan<TNodeId, TRoadId> CompactFlattenRoute<TReqPoint>(UserRouterPreferences userConfig,
            IReadOnlyList<TReqPoint> requestPoints,
            List<LegFlow<TNodeId, TRoadId>> legs,
            CancellationToken token, out ContextMessage? problem)
            where TReqPoint : IRequestPoint
        {
            var compactor = new RouteCompactor<TNodeId, TRoadId>(this.logger, this.Map, userConfig,
                this.FinderConfig.CompactPreservesRoads);

            RoutePlan<TNodeId, TRoadId> compacted = compactor.Compact(requestPoints, legs, token, out problem);

            if (compacted.Legs.Count < legs.Count)
                problem = problem.Override($"During compacting the legs were removed {legs.Count}->{compacted.Legs.Count}.");

            return compacted;
        }

        public Result<RoutePlan<TNodeId, TRoadId>> TryFindRoute(UserRouterPreferences userConfig,
            IReadOnlyList<RequestPoint<TNodeId>> userPlaces,
            bool allowSmoothing,
            bool allowGap,
            CancellationToken token)
        {
            var finding = RouteFinder<TNodeId, TRoadId>.TryFindRouteByMixed(this.logger, this.navigator, this.Map, this.coverGrid,
                this.FinderConfig,
                userConfig, userPlaces, allowSmoothing: allowSmoothing,
                allowGap: allowGap,
                token);
            if (!finding.HasValue)
            {
                return finding.FailWith<RoutePlan<TNodeId, TRoadId>>();
            }

            ContextMessage? problem = null;
            ContextMessage? p;
            var flow = convertSteps(userPlaces, finding.Value.RouteLegs, out p);
            problem = problem.Override(p);
            var compactor = new RouteCompactor<TNodeId, TRoadId>(logger, this.Map, userConfig,
                this.FinderConfig.CompactPreservesRoads);
            // flatten roundabouts only if there was no previous problem
            p = shaper.FlattenRoundabouts(flow);
            problem = problem.Override(p);
            var route = compactor.Compact(userPlaces, flow, token, out p);
            problem = problem.Override(p);
            return Result<RoutePlan<TNodeId, TRoadId>>.Valid(route, problem);
        }


        public IEnumerable<(GeoPoint point, CityInfo info, Length distance)> GetCities(GeoPoint point, Length range)
        {
            var boundary = Calculator.GetBoundary(range, point);

            foreach (var (_, pt, city) in Map.GetCitiesWithin(boundary))
            {
                if (city.Name == null)
                    continue;

                var dist = Calculator.GetDistance(pt.Convert3d(), point.Convert3d());
                if (dist <= range)
                    yield return (pt, city, dist);
            }
        }

        public List<string?> FindNames(PlanRequest<TNodeId> request, RoutePlan<TNodeId, TRoadId> plan)
        {
            var names = new List<string?>();
            var range = request.RouterPreferences.NamesSearchRange;

            foreach (var req_point in request.GetSequenceWithAutoPoints(plan))
            {
                if (!req_point.FindLabel)
                    names.Add(null);
                else
                {
                    names.Add(GetClosestCityName(req_point.UserPointFlat, range));
                }
            }

            return names;
        }

        public string? GetClosestCityName(GeoPoint point, Length range)
        {
            string? best_name = null;
            Length best_dist = Length.MaxValue;

            foreach (var (_, city, dist) in this.GetCities(point, range))
            {
                if (dist < best_dist)
                {
                    best_dist = dist;
                    best_name = city.Name;
                }
            }

            return best_name;
        }

        public PeaksResponse<TNodeId,TRoadId> FindPeaks(GeoPoint focusPoint, Length searchRange, Length separationDistance, int count)
        {
            var boundary = Calculator.GetBoundary(searchRange, focusPoint);

            var peaks = new List<(TNodeId node_id, GeoZPoint point)>();
            foreach (var (node_id, node_point, _) in Map.GetNodesWithin(boundary)
                         .Where(it => Map.IsPeak(it.nodeId, it.nodeInfo.Point.Altitude))
                         .OrderByDescending(it => it.nodeInfo.Point.Altitude)
                         .Select(it => (it.nodeId, it.nodeInfo.Point, dist: Calculator.GetFlatDistance(focusPoint, it.nodeInfo.Point.Convert2d())))
                         .Where(it => it.dist <= searchRange))
            {
                if (peaks.Any(it => Calculator.GetFlatDistance(it.point.Convert2d(), node_point.Convert2d()) <= separationDistance))
                    continue;
                peaks.Add((node_id, node_point));
                if (peaks.Count == count)
                    break;
            }

            return new PeaksResponse<TNodeId, TRoadId>()
            {
                // todo: compute base height of focus point
                
                Peaks = peaks.Select(it => new MapZPoint<TNodeId, TRoadId>(it.point,
                    it.node_id
#if DEBUG
                    , DEBUG_mapRef: null
#endif
                )).ToArray(),
            };
        }
    }
}