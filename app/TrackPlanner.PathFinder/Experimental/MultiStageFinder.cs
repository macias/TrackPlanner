﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackRadar.Collections;

namespace TrackPlanner.PathFinder
{
    // development stopped because I realized that even with shortcuts approach there is a design error
    // when you compute A-B route program will try to skip high traffic roads, but when you route it like
    // A-B...Y-Z program will assume that for B..Y (any middle fixed point) any nearby road is OK to be
    // used even if it high-traffic (because user pointed at such place). So if we compute shortcuts
    // using them in scenarios with 3 or more user points would give us incorrect results, because for shorcuts
    // there is always only A-B (two user points)
    
    // there is also related issue, in multi finder let's say we search for A-B and A-C. But what if A-C goes
    // through B? From A-B perspective cost of high-traffic should be supressed, but from A-C not, because user points
    // are A and C, not B.
    
    // SOLUTION: compute shortcuts with supression disabled. Then when computing real route, compute it as usual
    // when there is possibility we are within supression region. Once we are free on BOTH sides, use precomputed
    // shortcuts
    
//#if EXPERIMENTAL
    public sealed class NOT_FINISHED_MultiStageFinder<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly ILogger logger;
        private readonly RouteLogic<TNodeId, TRoadId> logic;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly CoverGrid<TNodeId, TRoadId>? coverGrid;
        private readonly Weight? weightLimit;
        private readonly Dictionary<TNodeId, string> DEBUG_hotNodes;
        private readonly HashSet<TNodeId> DEBUG_dangerousNodes;

        private IGeoCalculator calc => this.map.Grid.Calc;
        private readonly UserRouterPreferences userConfig;
        private readonly SystemConfiguration sysConfig;
        private readonly FinderConfiguration finderConfiguration;

        private readonly Navigator navigator;
        private readonly Speed fastest;

        internal NOT_FINISHED_MultiStageFinder(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            CoverGrid<TNodeId, TRoadId>? coverGrid,
            Weight? weightLimit,
            SystemConfiguration sysConfig,
            FinderConfiguration finderConfiguration,
            UserRouterPreferences userConfig,
            IReadOnlySet<TNodeId> suppressedHighTraffic)
        {
            this.logger = logger;
            this.navigator = navigator;
            this.map = map;
            this.coverGrid = coverGrid;
            this.weightLimit = weightLimit;
            this.sysConfig = sysConfig;
            this.finderConfiguration = finderConfiguration;
            this.userConfig = userConfig;
            if (!userConfig.Speeds.Values.SelectMany(it => new[] {it.Max, it.Regular}).TryMinMax(out var slowest,
                    out this.fastest))
                throw new ArgumentException("There are no speeds given.");

            this.DEBUG_dangerousNodes = new HashSet<TNodeId>();
            this.DEBUG_hotNodes = new Dictionary<TNodeId, string>()
            {
            };

            this.logic = new RouteLogic<TNodeId, TRoadId>(map, calc, userConfig, suppressedHighTraffic);
        }

        internal Result<WeightSteps<TNodeId, TRoadId>> TryFindStageRoute(
            DebugFinderHistory<TNodeId, TRoadId>? forwardDebugHistory,
            DebugFinderHistory<TNodeId, TRoadId>? backwardDebugHistory,
            TotalWeightComparer weightComparer,
            IReadOnlyList< RoadBucket<TNodeId, TRoadId>> startBuckets, IReadOnlyList< RoadBucket<TNodeId, TRoadId>> endBuckets,
            string modeLabel,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            CancellationToken cancellationToken,
            out CompStatistics stats)
        {
            RoadBucket<TNodeId, TRoadId> START_WRONG_BUCKET = null!;
            RoadBucket<TNodeId, TRoadId> END_WRONG_BUCKET = null!;
            var result = tryFindRawRoute(forwardDebugHistory, backwardDebugHistory,
                weightComparer,
                startBuckets, endBuckets,
                modeLabel,
                connectionPredicate, cancellationToken,
                out stats);

            if (!result.HasValue)
                return Result<WeightSteps<TNodeId, TRoadId>>.Fail("Unable to compute stage.");

            string? problem = null;
            // this is wrong, just to make it compile
            var stage_steps = result.Value[10000,10000].Steps.ToList();
            var stage_weight = result.Value[10000,10000].JoinedWeight;

            // remove user points
            if (!stage_steps[0].Place.IsUserPoint)
                problem ??= $"Path does not start with user point {stage_steps[0].Place}.";
            else
            {
                if (stage_steps[0].Place.Point.Convert2d() != START_WRONG_BUCKET.UserPoint)
                    problem ??= $"Path start out of sync with bucket {stage_steps[0].Place.Point} != {START_WRONG_BUCKET.UserPoint}.";
                stage_steps.RemoveAt(0);
            }

            stage_steps[0] = stage_steps[0].MirrorRoadsForInitial( stage_steps[1]);
            if (!stage_steps[0].Place.IsCrossPoint)
                return Result<WeightSteps<TNodeId, TRoadId>>.Fail($"Path start does not follow with cross point {stage_steps[0].Place}.");
            else if (!stage_steps[0].Place.IsCrossPointAdjacent(this.map, stage_steps[1].Place))
                problem ??= $"Start crosspoint {stage_steps[0].Place.CrossPointSpanningNodesInfo(this.map)} is not adjacent to {stage_steps[1].Place.OsmString(map)}";
            
            if (!stage_steps[^1].Place.IsUserPoint)
                problem ??= $"Path does not end with user point {stage_steps[^1].Place}.";
            else
            {
                if (stage_steps[^1].Place.Point.Convert2d() != END_WRONG_BUCKET.UserPoint)
                    problem ??= $"Path end out of sync with bucket {stage_steps[^1].Place.Point} != {END_WRONG_BUCKET.UserPoint}.";
                stage_steps.RemoveLast();
            }

            if (!stage_steps[^1].Place.IsCrossPoint)
                return Result<WeightSteps<TNodeId, TRoadId>>.Fail($"Path end does not follow with cross point {stage_steps[^1].Place}.");
            else if (!stage_steps[^1].Place.IsCrossPointAdjacent(this.map, stage_steps[^2].Place))
                problem ??= $"End crosspoint {stage_steps[^1].Place.CrossPointSpanningNodesInfo(this.map)} is not adjacent to {stage_steps[^2].Place.OsmString(this.map)}";

            if (!stage_steps.Skip(1).SkipLast(1).FirstOrNone(it => it.Place.IsNode).HasValue)
            {
                this.logger.Error($"Route does not contain nodes, start at {START_WRONG_BUCKET.OsmOrigin()}, end at {END_WRONG_BUCKET.OsmOrigin()}");
                return Result<WeightSteps<TNodeId, TRoadId>>.Fail($"Path does not contain pure node, {stage_steps.Count} entries.");
            }

            if (stage_steps.First().IncomingDistance != Length.Zero || stage_steps.First().IncomingTime != TimeSpan.Zero)
                problem ??= $"Initial step should be zero, it is {stage_steps.First().IncomingDistance} in {stage_steps.First().IncomingTime}";

            var stage = new WeightSteps<TNodeId, TRoadId>(stage_steps, stage_weight,isDraft:false);

            return Result.Valid(stage, problem);
        }

        private sealed class FinderData
        {
            public RoadBucket<TNodeId, TRoadId> Bucket { get; }
            public Backtrack<TNodeId,TRoadId> Backtrack { get;  }
            public MappedPairingHeap<Placement<TNodeId,TRoadId>,TotalWeight,BacktrackInfo<TNodeId,TRoadId>> Heap { get;  }
            public bool Completed { get; set; }

            public FinderData(IWorldMap<TNodeId, TRoadId> map,  RoadBucket<TNodeId, TRoadId> bucket,          TotalWeightComparer weightComparer)
            {
                Bucket = bucket;
                // node id -> source node id
                this.Backtrack = new Backtrack<TNodeId, TRoadId>(map);
                // node id -> ESTIMATE length (i.e. current length + direct distance), info
                this.Heap = MappedPairingHeap.Create<Placement<TNodeId, TRoadId>, TotalWeight,
                    BacktrackInfo<TNodeId, TRoadId>>(keyComparer: null, weightComparer: weightComparer);

            }

        }

        private sealed class RouteMatrix
        {
            private readonly (Placement<TNodeId,TRoadId> place, TotalWeight weight)?[,] joints;
            private readonly WeightSteps<TNodeId,TRoadId>?[,] steps;


            public RouteMatrix(int sourceCount, int targetCount)
            {
                this.joints = new (Placement<TNodeId, TRoadId> place, TotalWeight weight)?[sourceCount, targetCount];
                this.steps = new WeightSteps<TNodeId, TRoadId>?[sourceCount, targetCount];
            }

            public (int, int) Normalize(int from, int to, bool isForward)
            {
                return isForward ? (from, to) : (to, from);
            }
            public bool TryGetJoint(int from, int to, bool isForward,
                out Placement<TNodeId,TRoadId> place, out TotalWeight totalWeight)
            {
                (from, to) = Normalize(from, to, isForward);
                if (this.joints[from, to] is {} joint)
                {
                    (place, totalWeight) = joint;
                    return true;
                }

                (place, totalWeight) = (default,default);
                return false;
            }

            public void SetSteps(int from, int to, bool isForward, WeightSteps<TNodeId, TRoadId> steps)
            {
                (from, to) = Normalize(from, to, isForward);
                this.steps[from, to] = steps;
            }

            public void SetJoint(int from, int to, bool isForward, 
                (Placement<TNodeId, TRoadId> place, TotalWeight weight) joint)
            {
                (from, to) = Normalize(from, to, isForward);
                this.joints[from, to] = joint;
            }
        }

        private Result<WeightSteps<TNodeId, TRoadId>[,]> tryFindRawRoute(
            DebugFinderHistory<TNodeId, TRoadId>? forwardDebugHistory,
            DebugFinderHistory<TNodeId, TRoadId>? backwardDebugHistory,
            TotalWeightComparer weightComparer,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> startBuckets, IReadOnlyList<RoadBucket<TNodeId, TRoadId>> endBuckets,
            string modeLabel,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            CancellationToken cancellationToken,
            out CompStatistics stats)
        {
            RoadBucket<TNodeId, TRoadId> END_WRONG_BUCKET = null!;
            (Placement<TNodeId, TRoadId> place, TotalWeight weight)? WRONG_joint = null!;
                Dictionary<Placement<TNodeId, TRoadId>, (BacktrackInfo<TNodeId, TRoadId> info, TotalWeight weight) > WRONG_oppositeBacktrack = null!;
                
            IEnumerable<int> iterate_opposite_indices(bool isForward)
            {
                return Enumerable.Range(0, (isForward ? endBuckets : startBuckets).Count);
            }

            var matrix = new RouteMatrix(startBuckets.Count, endBuckets.Count);

            // NOTE: even in multi finder there is a difference between going "B to A" and going to "B to A in reverse"
            // you cannot simply turn around route, simple example going uphill and downhill makes the difference
            // road is the same, yet the cost/time is not (similar thing with oneway roads)
            stats = new CompStatistics();
            //logger.Info($"Start at legal {startBucket.Any(it => this.map.GetRoad(it.BaseRoadIndex.RoadId).HasAccess)}, end at legal {endBucket.Any(it => this.map.GetRoad(it.BaseRoadIndex.RoadId).HasAccess)}");

            int rejected = 0;

            var forward_data = new FinderData[startBuckets.Count];
            var backward_data = new FinderData[endBuckets.Count];
            foreach (var is_forward in new[] {false, true})
            {
                var buckets = is_forward ? startBuckets : endBuckets;
                var data_pack = is_forward ? forward_data : backward_data;
                
                for (int idx = 0; idx < buckets.Count; ++idx)
                {
                    var data = new FinderData(this.map,buckets[idx], weightComparer);

                    Length remaining_direct_distance;
                    // if (endBuckets.Count()==1)
                    // remaining_direct_distance= calc.GetFlatDistance(startBucket.UserPoint, endBucket.UserPoint);
                    //else
                    remaining_direct_distance = Length.Zero;

                    data.Heap.TryAddOrUpdate(Placement<TNodeId, TRoadId>.UserPoint(data.Bucket),
                         new TotalWeight(Weight.Zero,
                            remaining: Weight.FromCost( remaining_direct_distance,this.fastest)),
                        new BacktrackInfo<TNodeId, TRoadId>(source: 
                            new Edge<TNodeId, TRoadId>( Placement<TNodeId, TRoadId>.Prestart(new GeoZPoint())),
                            null,
                            null,
                            Length.Zero,
                            TimeSpan.Zero,
                            isShortcut: false
                            ));
                    
                    data_pack[idx] = data;

                    stats.Updated(isForward: is_forward);
                }
            }

            bool is_forward_side = true;

            while (true)
            {
                is_forward_side = !is_forward_side;

                var data_pack = is_forward_side ? forward_data : backward_data;
                var opposite_pack = is_forward_side ?  backward_data:forward_data;
                var to_buckets = is_forward_side ? endBuckets : startBuckets;
                
                for (int from_idx = 0; from_idx < data_pack.Length; ++from_idx)
                {
                    if (forward_data[from_idx].Completed)
                        continue;

                    cancellationToken.ThrowIfCancellationRequested();

                    var backtrack = data_pack[from_idx].Backtrack;
                    var heap = data_pack[from_idx].Heap;
                    var from_bucket = data_pack[from_idx].Bucket;
                    var debug_history = is_forward_side ? forwardDebugHistory : backwardDebugHistory;

                    if (!DevelModes.True && backtrack.Count % 1_000 == 0)
                    {
                        this.logger.Verbose($"Routed through {backtrack.Count} places");
                    }

                    if (!heap.TryPop(out Placement<TNodeId, TRoadId> current_place,
                            out TotalWeight current_weight,
                            out BacktrackInfo<TNodeId, TRoadId> current_info))
                    {
                        data_pack[from_idx].Completed = true;

                        foreach (var to_idx in iterate_opposite_indices(is_forward_side))
                        {
                            if (!matrix.TryGetJoint(from_idx, to_idx, isForward: is_forward_side,
                                    out var joint_place, out var joint_weight))
                            {
                                this.logger.Info($"Finding path in {modeLabel} failed , fwd: {stats.ForwardUpdateCount}, bwd {stats.BackwardUpdateCount}");
                            }
                            else
                            {
                                var (start_idx, end_idx) = matrix.Normalize(from_idx, to_idx, is_forward_side);

                                var forward_steps = FinderHelper.RecreatePath(map:map, coverGrid, 
                                    forward_data[start_idx].Backtrack,
                                    Placement<TNodeId, TRoadId>.UserPoint(startBuckets[start_idx]),
                                    Edge.STUB(  joint_place),isReversed:false);
                                var backward_steps = FinderHelper.RecreatePath(this.map,coverGrid, 
                                    backward_data[end_idx].Backtrack,
                                    Placement<TNodeId, TRoadId>.UserPoint(endBuckets[end_idx]), Edge.STUB( joint_place),
                                    isReversed:true);
                                // skip first place, because it is shared with the result
                                forward_steps.AddRange(FinderHelper.ReverseSteps(backward_steps).Skip(1));

                                matrix.SetSteps(from_idx, to_idx, isForward: is_forward_side,
                                    new WeightSteps<TNodeId, TRoadId>(forward_steps,
                                        joinedWeight: joint_weight.Current,isDraft:false));

                               // this.logger.Info($"We have joint in {modeLabel}");
                            }
                        }
                    }

                    debug_history?.Add(Edge.STUB(current_place), current_weight, current_info);

                    backtrack.Add(Edge.STUB(current_place), current_info, current_weight);

                    /*if (current_place.IsNode && map.GetRoad(current_info.IncomingRoadId!.Value).IsDangerous)
                    {
                        this.DEBUG_dangerousNodes.Add(current_place.NodeId);
                    }/*
    
                    /*{
                        if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                        {
                            logger.Info($"Coming to hot node {current_place.NodeId}/{comment} using road {current_info.IncomingRoadId}");
                        }
                    }*/

                    // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point

                    foreach (var to_idx in iterate_opposite_indices(is_forward_side))
                    {
                        var to_bucket = opposite_pack[to_idx].Bucket;
                        var opposite_backtrack = opposite_pack[to_idx].Backtrack;

                        if (current_place.IsUserPoint && current_place.Point.Convert2d() == to_bucket.UserPoint)
                        {
                            // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point
                            var route_steps = FinderHelper.RecreatePath(this.map,coverGrid, backtrack,
                                Placement<TNodeId, TRoadId>.UserPoint(from_bucket),
                                Edge.STUB( current_place),isReversed:!is_forward_side);
                            if (!is_forward_side)
                                route_steps = FinderHelper.ReverseSteps(route_steps);
                            matrix.SetSteps(from_idx, to_idx, is_forward_side,
                                new WeightSteps<TNodeId, TRoadId>(route_steps, 
                                    joinedWeight: current_weight.Current,isDraft:false));

                            logger.Info($"BOOM, direct hit with weight {current_weight} in {modeLabel}");
                            forwardDebugHistory?.DumpLastData();
                            backwardDebugHistory?.DumpLastData();
                            stats.RejectedNodes = rejected;
                        }
                        else if (opposite_backtrack.TryGetValue(Edge.STUB(current_place), out var opposite_info,out var opposite_weight))
                        {
                            this.logger.Info("Joint point found");
                            var new_joined_weight = TotalWeight.OBSOLETE_JoinFinal(opposite_weight, current_weight);
                            if (!matrix.TryGetJoint(from_idx, to_idx, is_forward_side, out _, out var joint_weight)
                                || weightComparer.Compare(new_joined_weight, joint_weight) < 0)
                                matrix.SetJoint(from_idx, to_idx, is_forward_side, (current_place, new_joined_weight));
                        }
                    }

                    int adjacent_count = 0;

                    if (connectionPredicate == null
                        && coverGrid != null
                        && current_place.IsNode
                        && coverGrid.CanUseShortcuts(current_place.NodeId, from_bucket, to_buckets))
                        foreach (var (target, short_weight) in coverGrid.GetShortcuts(current_place.NodeId,
                                     !is_forward_side))
                        {
                            var target_place = Placement<TNodeId, TRoadId>.Node(this.map,Edge.INDEX<TNodeId,TRoadId>( target));

                            ++adjacent_count;

                            if (backtrack.ContainsKey(Edge.STUB(target_place)))
                            {
                                if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, 
                                        out string? comment))
                                {
                                    logger.Info($"Adjacent to hot node {current_place.NodeId}/{comment} is already used by outgoing road ? @{target_place.NodeId}");
                                }

                                continue;
                            }

                            Weight remaining_direct_cost = Weight.Zero;
                            if (DevelModes.True)
                            {
                            }
                            else
                            {
                                if (!computeRemainingDistance(stats, heap, target_place, WRONG_joint, WRONG_oppositeBacktrack,
                                        END_WRONG_BUCKET, cancellationToken, out remaining_direct_cost))
                                    continue;
                            }


                            TotalWeight outgoingTotalWeight = new TotalWeight((current_weight.Current+
                                    short_weight),
                                remaining: remaining_direct_cost);

                            {
                                var outgoing_info = new BacktrackInfo<TNodeId, TRoadId>(Edge.STUB(  current_place),
                                    incomingRoadId: null,
                                    incomingCondition: null,
                                    incomingDistance: null,
                                    incomingTime: null,
                                    isShortcut:true
                                    );

                                bool updated = heap.TryAddOrUpdate(target_place, outgoingTotalWeight, outgoing_info);
                                stats.Updated(is_forward_side);

                                {
                                    if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                                    {
                                        //logger.Info($"Adjacent to hot node {current_place.NodeId}/{comment} is by outgoing road ? @ {target_place.NodeId}, {(step_info.IsForbidden ? "forbidden" : "")}, weight {outgoing_weight}, updated {updated}");
                                    }
                                }
                            }
                            stats.AddNode(degree: adjacent_count);
                        }


                    var adj_predicate = connectionPredicate;
                    if (adj_predicate == null && coverGrid != null)
                        adj_predicate = (curr, adj, _) => coverGrid.CanUseAdjacent(curr, adj, from_bucket, to_buckets);


                    foreach ((var target_place, TRoadId connecting_road_map_index)
                             in this.map.GetAdjacent(calc,current_place, from_bucket, to_buckets,
                                 adj_predicate,this.finderConfiguration.PreserveRoundabouts))
                    {
                        ++adjacent_count;

                        if (backtrack.ContainsKey(target_place))
                        {
                            if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                            {
                                logger.Info(
                                    @$"Adjacent to hot node {current_place.NodeId}/{comment} is already 
used by outgoing road {connecting_road_map_index}@{target_place.Place.NodeId}");
                            }

                            continue;
                        }

                        if (!computeRemainingDistance(stats, heap, target_place.Place, WRONG_joint, WRONG_oppositeBacktrack,
                                END_WRONG_BUCKET, cancellationToken, out var remaining_direct_cost))
                            continue;

                        MoveInfo step_info = this.logic.GetFullStepInfo(connecting_road_map_index, 
                            Edge.INDEX<TNodeId,TRoadId>( Edge.SOURCE<TNodeId>()),
                            current_place, 
                            target_place.Place, reversed: !is_forward_side);

                        TotalWeight outgoingTotalWeight = new TotalWeight(current_weight.Current+ step_info.Weight,
                            remaining:  remaining_direct_cost);

                        {
                            var outgoing_info = new BacktrackInfo<TNodeId, TRoadId>(Edge.STUB(  current_place),
                                connecting_road_map_index,
                                step_info.GetCondition(),
                                step_info.SegmentLength,
                                step_info.Time,
                                isShortcut: false
                                );

                            bool updated = heap.TryAddOrUpdate(target_place.Place, outgoingTotalWeight, outgoing_info);
                            if (is_forward_side)
                                ++stats.ForwardUpdateCount;
                            else
                                ++stats.BackwardUpdateCount;

                            {
                                if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                                {
                                    logger.Info(
                                        @$"Adjacent to hot node {current_place.NodeId}/{comment} is by 
outgoing road {connecting_road_map_index}@{target_place.Place.NodeId}, {(step_info.IsForbidden ? "forbidden" : "")}, weight {outgoingTotalWeight}, updated {updated}");
                                }
                            }
                        }
                    }

                    stats.AddNode(degree: adjacent_count);
                }
            }
        }

        private bool computeRemainingDistance(CompStatistics stats,
            MappedPairingHeap<Placement<TNodeId, TRoadId>, TotalWeight, BacktrackInfo<TNodeId, TRoadId>> heap,
            Placement<TNodeId, TRoadId> focusPlace,
            (Placement<TNodeId, TRoadId> place, TotalWeight weight)? joint,
            Dictionary<Placement<TNodeId, TRoadId>, (BacktrackInfo<TNodeId, TRoadId> info, TotalWeight weight)> oppositeBacktrack,
            RoadBucket<TNodeId, TRoadId> endBucket,
            CancellationToken cancellationToken,
            out Weight remainingDirectCost)
        {
            Weight?  remaining_weight = null;
            {
                if (heap.TryGetData(focusPlace, out var adj_weight, out var adj_info))
                {
                    remaining_weight = adj_weight.Remaining;
                }
            }
            // after initial join, we work only in join mode, meaning we accepts only updates on our side, and we
            // need to hit the opposite already fixed places
            if (joint != null && (remaining_weight == null
                                  || !oppositeBacktrack.ContainsKey(focusPlace)))
            {
                remainingDirectCost = default;
                return false;
            }

            if (remaining_weight == null)
            {
                if (focusPlace.IsNode && this.userConfig.HACK_ExactToTarget)
                {
                    var adj_bucket = RoadBucket.GetRoadBuckets(new[] {focusPlace.NodeId}, 
                        this.map, this.map.Grid.Calc, allowSmoothing: false,allowGap:false)
                        .Single();

                    var sub_buckets = new List<RoadBucket<TNodeId, TRoadId>>() {adj_bucket, endBucket};
                    var worker = new RouteFinder<TNodeId, TRoadId>(this.logger, this.navigator,
                        this.map, this.coverGrid,connectionPredicate:null,
                        weightLimit,
                        this.finderConfiguration with {DumpProgress = false},
                        new UserRouterPreferences() {HACK_ExactToTarget = false}.SetUniformSpeeds(),
                        sub_buckets, cancellationToken);
                    if (worker.TryFindRoute(sub_buckets) is {HasValue: true} remaining)
                    {
                        ++stats.SuccessExactTarget;
                        remaining_weight = Weight.FromCost( Length.FromMeters(remaining.Value.RouteLegs
                            .SelectMany(x => x.Steps)
                            .Sum(it => it.IncomingDistance.Meters)), this.fastest);
                    }
                    else
                    {
                        //throw new InvalidOperationException($"Sub-path failed from n#{adj_place.NodeId}@{this.map.Nodes[adj_place.NodeId.Value]} to {end}");
                        ++stats.FailedExactTarget;
                    }
                }

                if (remaining_weight == null)
                    remaining_weight = Weight.FromCost( calc.GetFlatDistance(focusPlace.Point.Convert2d(),
                        endBucket.UserPoint), this.fastest);
            }

            remainingDirectCost = remaining_weight.Value;
            return true;
        }
    }
//#endif
}