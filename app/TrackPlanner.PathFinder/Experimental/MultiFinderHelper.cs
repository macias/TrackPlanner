using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;

namespace TrackPlanner.PathFinder
{
   // #if EXPERIMENTAL
    public static class NOT_FINISHED_MultiFinderHelper
    {
        internal static IEnumerable<(Edge<TNodeId, TRoadId> place, TRoadId roadId)> GetAdjacent<TNodeId, TRoadId>(
            this IWorldMap<TNodeId, TRoadId> map,
            IGeoCalculator calc,
            Placement<TNodeId, TRoadId> current,
            RoadBucket<TNodeId, TRoadId> startBucket, IEnumerable< RoadBucket<TNodeId, TRoadId>> endBuckets,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            bool preserveRoundabouts)
            where TNodeId : struct
            where TRoadId : struct
        {
            // point -> incoming road id
            var adjacent = new Dictionary<Edge<TNodeId, TRoadId>, TRoadId>();
            if (current.IsNode)
            {
                FinderHelper.AddAdjacentCrosspointsToNode(map, adjacent, current, startBucket);
                foreach (var end_bucket in endBuckets)
                    FinderHelper.AddAdjacentCrosspointsToNode(map, adjacent, current, end_bucket);

                foreach (var (place, road_id) in
                         FinderHelper.GetDirectlyAdjacent(map, calc,current, 
                             connectionPredicate,preserveRoundabouts))
                    adjacent.TryAdd(Edge.STUB( place), road_id);
            }
            else
            {
                FinderHelper.AddAdjacentToUserPoint(map, adjacent, current, startBucket);
                foreach (var end_bucket in endBuckets)
                FinderHelper.AddAdjacentToUserPoint(map, adjacent, current,end_bucket);
            }

            return adjacent.Select(it => (it.Key, it.Value));
        }  
    }
  //  #endif
}