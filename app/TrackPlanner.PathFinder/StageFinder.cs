﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackRadar.Collections;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.ContractionHierarchies;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{

    internal readonly record struct StageFinderData<TNodeId, TRoadId>(
        WeightSteps<TNodeId, TRoadId> ResultPath,
    TotalWeight ForwardWeight,
        TotalWeight BackwardWeight)
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
    
    }

  
    public sealed class StageFinder<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        enum SearchAlgorithm
        {
            FastForward,
            AStar,
            Dijkstra, 
        }
        
        private readonly ILogger logger;
        private readonly RouteLogic<TNodeId, TRoadId> logic;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly FinderConfiguration finderConfig;
        private readonly Dictionary<TNodeId, string> DEBUG_hotNodes;
        private readonly HashSet<TNodeId> DEBUG_dangerousNodes;

        private string? debugDirectory => this.navigator.GetDebugDirectory(this.finderConfig.EnableDebugDumping);

        private IGeoCalculator calc => this.map.Grid.Calc;
        private readonly UserRouterPreferences userConfig;

        private readonly Navigator navigator;
        private readonly RouteShaper<TNodeId, TRoadId> shaper;
        private readonly Speed fastest;

        internal StageFinder(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            FinderConfiguration finderConfig,
            UserRouterPreferences userConfig,
            IReadOnlySet<TNodeId> suppressedHighTraffic)
        {
            this.logger = logger;
            this.shaper = new RouteShaper<TNodeId, TRoadId>(logger, map);
            this.navigator = navigator;
            this.map = map;
            this.finderConfig = finderConfig;
            this.userConfig = userConfig;

            this.DEBUG_dangerousNodes = new HashSet<TNodeId>();
            this.DEBUG_hotNodes = new Dictionary<TNodeId, string>()
            {
            };

            userConfig.CalculateSpeedLimits(out _, out this.fastest);
            this.logic = new RouteLogic<TNodeId, TRoadId>(map, calc, userConfig, suppressedHighTraffic);
        }

        internal Result<WeightSteps<TNodeId, TRoadId>, RouteFinding> TryFindStageRoute(
            RouteMode routeMode,
            ICover<TNodeId, TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            Weight? weightLimit,
            RoadBucket<TNodeId, TRoadId> startBucket, RoadBucket<TNodeId, TRoadId> endBucket,
            CancellationToken cancellationToken,
            out CompStatistics stats)
        {
            using (var forward_debug_history = this.finderConfig.DumpProgress
                       ? new DebugFinderHistory<TNodeId, TRoadId>(this.logger, this.map,
                           $"fwd-{routeMode}", this.debugDirectory)
                       : null)
            using (var backward_debug_history = this.finderConfig.DumpProgress
                       ? new DebugFinderHistory<TNodeId, TRoadId>(this.logger, this.map,
                           $"bwd-{routeMode}", this.debugDirectory)
                       : null)
            {
                stats = new CompStatistics();
                TotalWeight? forwardLimit = null;
                TotalWeight? backwardLimit = null;
                WeightSteps<TNodeId, TRoadId> stage = new WeightSteps<TNodeId, TRoadId>();
                bool ff_suffice = this.userConfig.FastModeLimit is { } limit
                                  && calc.GetFlatDistance(startBucket.UserPoint, endBucket.UserPoint) > limit;
                if (routeMode == RouteMode.General && (finderConfig.TwoPassStage || ff_suffice))
                {
                    this.logger.Info("Using fast forward");
                    var result = tryFindRawRoute(routeMode,
                        SearchAlgorithm.FastForward,
                        forward_debug_history, backward_debug_history,
                        coverGrid,
                        connectionPredicate,
                        weightLimit,
                        forwardLimit: null,
                        backwardLimit: null,
                        startBucket, endBucket,
                        cancellationToken,
                        out stats);
                    if (!result.HasValue)
                        return result.FailAs<WeightSteps<TNodeId, TRoadId>>();
                    stage = result.Value.ResultPath;

                    if (weightLimit == null || WeightComparer.Instance.IsLess(stage.JoinedWeight, weightLimit.Value))
                        weightLimit = stage.JoinedWeight;
                    forwardLimit = result.Value.ForwardWeight;
                    backwardLimit = result.Value.BackwardWeight;

                    this.logger.Info($"Forward weight limit {forwardLimit}, backward weight limit {backwardLimit}");
                }

                if (routeMode == RouteMode.CycleSnap || !ff_suffice)
                {
                    this.logger.Info("Using proper route");

                    SearchAlgorithm search_mode = finderConfig.AStarMode
                        ? SearchAlgorithm.AStar
                        : SearchAlgorithm.Dijkstra;
                    var result = tryFindRawRoute(routeMode,
                        search_mode,
                        forward_debug_history, backward_debug_history,
                        coverGrid,
                        connectionPredicate,
                        weightLimit,
                        forwardLimit,
                        backwardLimit,
                        startBucket, endBucket,
                        cancellationToken,
                        out stats);
                    if (!result.HasValue)
                        return result.FailAs<WeightSteps<TNodeId, TRoadId>>();

                    stage = result.Value.ResultPath;
                }

                var stage_steps = stage.Steps.ToList();

                if (finderConfig.EnableDebugDumping && this.finderConfig.DumpRawLegs)
                {
                    var input = new TrackWriterInput();
                    foreach (var (place, i) in stage_steps.Select(it => it.Place).ZipIndex())
                        input.AddPoint(place.Point, icon: PointIcon.DotIcon, label: $"[{i}] {place.OsmString(map)}");
                    input.AddLine(stage_steps.Select(it => it.Place.Point));
                    string filename = Navigator.GetUniquePath(this.navigator.GetDebugDirectory(),
                        $"raw-leg.kml");
                    input.BuildDecoratedKml().Save(filename);
                }

                var res = this.shaper.RemoveUserPoints(startBucket, endBucket, stage_steps);
                if (!res.HasValue)
                    return res.FailAs<WeightSteps<TNodeId, TRoadId>, RouteFinding>(RouteFinding.RouteNotFound);

                stage = new WeightSteps<TNodeId, TRoadId>(stage_steps, stage.JoinedWeight, isDraft: false);

                return Result<WeightSteps<TNodeId, TRoadId>, RouteFinding>.Valid(stage, res.ProblemMessage);
            }
        }

        private Result<StageFinderData<TNodeId,TRoadId>,RouteFinding> tryFindRawRoute(
            RouteMode routeMode,
            SearchAlgorithm searchAlgorithm,
            DebugFinderHistory<TNodeId, TRoadId>? forwardDebugHistory,
            DebugFinderHistory<TNodeId, TRoadId>? backwardDebugHistory,
            ICover<TNodeId, TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            Weight? weightLimit,
            TotalWeight? forwardLimit,
            TotalWeight? backwardLimit,
            RoadBucket<TNodeId, TRoadId> stageStart, RoadBucket<TNodeId, TRoadId> stageEnd,
            CancellationToken cancellationToken,
            out CompStatistics stats)
        {
            stats = new CompStatistics();
            //  logger.Info($"Start at legal {stageStart.Any(it => this.map.GetRoad(it.BaseRoadIndex.RoadId).HasAccess)}, end at legal {stageEnd.Any(it => this.map.GetRoad(it.BaseRoadIndex.RoadId).HasAccess)}");

            int rejected = 0;

            SearchData<TNodeId, TRoadId> forward_data;
            SearchData<TNodeId, TRoadId> backward_data;

            TotalWeightComparer weight_comparer = searchAlgorithm switch
            {
                SearchAlgorithm.Dijkstra => TotalWeightComparer.CurrentInstance,
                SearchAlgorithm.FastForward=> TotalWeightComparer.CostlessAndRemainingInstance,
                SearchAlgorithm.AStar => TotalWeightComparer.BothInstance,
                _ => throw new NotImplementedException($"{searchAlgorithm}")
            };
            
            {
                forward_data = new SearchData<TNodeId, TRoadId>(this.map,weightComparer: weight_comparer,
                    stageStart);
                backward_data = new SearchData<TNodeId, TRoadId>(this.map,weightComparer: weight_comparer,
                    stageEnd);
            }

            stats.ForwardUpdateCount += 1;
            stats.BackwardUpdateCount += 1;

            bool is_forward_side = true;

            JointInfo<TNodeId, TRoadId>? joint = null;

            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                    cancellationToken.ThrowCanceledException("");

                if (forward_data.IsEmpty)
                    forwardDebugHistory?.DumpLastData();
                if (backward_data.IsEmpty)
                    backwardDebugHistory?.DumpLastData();

                // pick either side with not depleted heap, or simply switch side on each step
                is_forward_side = forward_data.ProgressSize - backward_data.ProgressSize > 1 || forward_data.IsEmpty
                    ? false
                    : backward_data.ProgressSize - forward_data.ProgressSize > 1 || backward_data.IsEmpty
                        ? true
                        : !is_forward_side;

                var backtrack = (is_forward_side ? forward_data : backward_data).Backtrack;
                var opposite_backtrack = (is_forward_side ? backward_data : forward_data).Backtrack;
                var heap = (is_forward_side ? forward_data : backward_data).Heap;
                var start_bucket = is_forward_side ? stageStart : stageEnd;
                var end_bucket = is_forward_side ? stageEnd : stageStart;
                var debug_history = is_forward_side ? forwardDebugHistory : backwardDebugHistory;
                var side_limit = is_forward_side ? forwardLimit : backwardLimit;

                if (!DevelModes.True && backtrack.Count % 1_000 == 0)
                {
                    this.logger.Verbose($"Routed through {backtrack.Count} places");
                }

                // if the sides are not balanced stop fetching new data, it means only
                // that one end is in disconnected area and there is no sense to continue
                // searching
                if (Math.Abs(forward_data.ProgressSize - backward_data.ProgressSize) > 2
                    || !heap.TryPop(out Edge<TNodeId, TRoadId> current_edge,
                        out TotalWeight current_weight,
                        out BacktrackInfo<TNodeId, TRoadId> current_info))
                {
                    if (joint is { } j)
                    {
                        prepareResult(coverGrid, stageStart, stageEnd,
                            forward_data, backward_data, j,
                            out var forwardWeight, out var backwardWeight, out var resultPath);

                        //this.logger.Info($"We have joint in {modeLabel}");
                        if (DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }

                        return Result<StageFinderData<TNodeId, TRoadId>, RouteFinding>.Valid(
                            new StageFinderData<TNodeId, TRoadId>(
                                resultPath, forwardWeight, backwardWeight));
                    }
                    else
                    {
                        // the fact that, for example, active side now is going from the end
                        // does not mean it is backward side needed to be expanded, because
                        // forward size could be already exhausted for some time
                        var is_forward_exhausted = forward_data.Heap.Count == 0;

                        if ((is_forward_exhausted ? stageStart : stageEnd).IsExpandable)
                        {
                            if (is_forward_exhausted)
                            {
                                forward_data.Charge();
                                stageStart.Expand();
                            }
                            else
                            {
                                backward_data.Charge();
                                stageEnd.Expand();
                            }

                            continue;
                        }
                        else
                        {
                            this.logger.Info($"Finding path in {routeMode} failed , fwd: {stats.ForwardUpdateCount}, bwd {stats.BackwardUpdateCount}");
                            if (DEBUG_SWITCH.Enabled)
                            {
                                ;
                            }

                            return Result<StageFinderData<TNodeId, TRoadId>, RouteFinding>.Fail(RouteFinding.RouteNotFound);
                        }
                    }
                }

                debug_history?.Add(current_edge, current_weight, current_info);

                current_info = backtrack.Add(current_edge, current_info, current_weight);

                /*if (current_place.IsNode && map.GetRoad(current_info.IncomingRoadId!.Value).IsDangerous)
                {
                    this.DEBUG_dangerousNodes.Add(current_place.NodeId);
                }/*

                /*{
                    if (current_place.IsNode && DEBUG_hotNodes.TryGetValue(current_place.NodeId, out string? comment))
                    {
                        logger.Info($"Coming to hot node {current_place.NodeId}/{comment} using road {current_info.IncomingRoadId}");
                    }
                }*/

                // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point

                if (current_edge.Place.IsUserPoint
                    && current_edge.Place.Point.Convert2d() == end_bucket.UserPoint
                    && (weightLimit == null || !WeightComparer.Instance.IsLess(weightLimit.Value, current_weight.Current)))
                {
                    // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point
                    var route_steps = FinderHelper.RecreatePath(this.map, coverGrid,
                        backtrack,
                        Placement<TNodeId, TRoadId>.UserPoint(start_bucket),
                        current_edge, isReversed: !is_forward_side);
                    if (!is_forward_side)
                        route_steps = FinderHelper.ReverseSteps(route_steps);
                    var (forwardWeight, backwardWeight) = is_forward_side
                        ? (current_weight, TotalWeight.Zero)
                        : (TotalWeight.Zero, current_weight);
                    var resultPath = new WeightSteps<TNodeId, TRoadId>(route_steps, joinedWeight: current_weight.Current,
                        isDraft: false);

                    logger.Info($"BOOM, direct hit with weight {current_weight} in {routeMode}");
                    forwardDebugHistory?.DumpLastData();
                    backwardDebugHistory?.DumpLastData();
                    stats.RejectedNodes = rejected;
                    if (DEBUG_SWITCH.Enabled)
                    {
                        ;
                    }

                    return Result<StageFinderData<TNodeId, TRoadId>, RouteFinding>.Valid(
                        new StageFinderData<TNodeId, TRoadId>(
                            resultPath, forwardWeight, backwardWeight));

                }
                else
                {
                    foreach (var (opposite_edge, opposite_info, opposite_weight) in opposite_backtrack.TryGetValues(current_edge.Place))
                    {
                       Weight curr_new_joined_weight = this.logic.SumTotalJointWeight(opposite_edge, opposite_weight, current_edge, current_weight);
                        var passes_weight = weightLimit == null || !WeightComparer.Instance.IsLess(weightLimit.Value, curr_new_joined_weight);
                        if (passes_weight)
                        {
                            var (fwd_weight, bwd_weight) = is_forward_side
                                ? (current_weight, opposite_weight)
                                : (opposite_weight, current_weight);
                            var (fwd_edge, bwd_edge) = is_forward_side
                                ? (current_edge, opposite_edge)
                                : (opposite_edge, current_edge);
                            if (joint is not { } j)
                            {
                                if (DEBUG_SWITCH.Enabled)
                                {
                                    ;
                                }

                                joint = new JointInfo<TNodeId, TRoadId>(fwd_edge,bwd_edge,
                                    fwd_weight, bwd_weight,
                                    forward_data.Backtrack.Timestamp, backward_data.Backtrack.Timestamp);

                                if (searchAlgorithm == SearchAlgorithm.FastForward)
                                {
                                    prepareResult(coverGrid, stageStart, stageEnd,
                                        forward_data, backward_data, joint.Value,
                                        out var forwardWeight, out var backwardWeight, out var resultPath);

                                    return Result<StageFinderData<TNodeId, TRoadId>, RouteFinding>.Valid(
                                        new StageFinderData<TNodeId, TRoadId>(
                                            resultPath, forwardWeight, backwardWeight));

                                }
                            }
                            else if (WeightComparer.Instance.IsLess(curr_new_joined_weight, j.JoinedWeight))
                            {
                                if (DEBUG_SWITCH.Enabled)
                                {
                                    ;
                                }

                                joint = j with
                                {
                                    ForwardEdge = fwd_edge,
                                    BackwardEdge = bwd_edge,
                                    ForwardWeight = fwd_weight,
                                    BackwardWeight = bwd_weight
                                };
                            }

                            if (side_limit == null
                                || WeightComparer.Instance.IsLess(joint.Value.JoinedWeight,
                                    TotalWeight.JoinFinal(forwardLimit!.Value, backwardLimit!.Value)))
                            {
                                forwardLimit = joint.Value.ForwardWeight;
                                backwardLimit = joint.Value.BackwardWeight;
                            }
                        }
                    }
                }

                if (side_limit is { } s_limit 
                    && TotalWeightComparer.BothInstance.IsGreater(current_weight, s_limit))
                {
                    // side limit has to be able to make one step more than limit, this is because we need to "borrow"
                    // one move in order to connect with the other side despite from our side it looks like exceeding
                    // the limit
                    // consider scenario
                    // computing the limit o----o---X---o----o
                    // for X the side limit was computed
                    // run with the limit  o----O-------O----o
                    // asuming the second run is cheaper it still would not be able to connect O's 
                    // if we not let each side make extra move
                    continue;
                }

                int adjacent_count = 0;

                if (coverGrid != null
                    && current_edge.Place.IsNode
                    && coverGrid.CanUseShortcuts(current_edge.Place.NodeId, start_bucket, end_bucket))
                    foreach (var (target, short_weight) in coverGrid.GetShortcuts(current_edge.Place.NodeId,
                                 !is_forward_side))
                    {
                        if (!(connectionPredicate?.Invoke(current_edge.Place.NodeId, target, null) ?? true))
                            continue;

                        var target_place = Edge.STUB( Placement<TNodeId, TRoadId>.Node(this.map,
                            Edge.INDEX<TNodeId,TRoadId>( target)));

                        ++adjacent_count;

                        if (backtrack.ContainsKey(target_place))
                        {
                            if (current_edge.Place.IsNode && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId, out string? comment))
                            {
                                logger.Info(
                                    @$"Adjacent to hot node {current_edge.Place.NodeId}/{comment} is 
already used by outgoing road ? @{target_place.Place.NodeId}");
                            }

                            continue;
                        }

                        if (!canExpandOverJoint(is_forward_side,searchAlgorithm, 
                                current_edge, target_place, joint, opposite_backtrack))
                            continue;
                        var remaining_direct_cost = computeRemainingDistance(
                            stats, heap, coverGrid,
                            connectionPredicate,
                             target_place,
                            end_bucket, cancellationToken);

                        TotalWeight outgoingTotalWeight = new TotalWeight((current_weight.Current +
                                                                           short_weight),
                            remaining: remaining_direct_cost);

                        var passes_weight = passesWeight(weight_comparer, weightLimit,
                            outgoingTotalWeight,target_place,opposite_backtrack,joined:joint!=null);
                        if (!passes_weight && DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }

                        if (passes_weight)
                        {
                            var outgoing_info = new BacktrackInfo<TNodeId, TRoadId>(current_edge,
                                incomingRoadId: null,
                                incomingCondition: null,
                                incomingDistance: null,
                                incomingTime: null,
                                isShortcut:true
                                );

                            bool updated = heap.TryAddOrUpdate(target_place, outgoingTotalWeight, outgoing_info);
                            stats.Updated(is_forward_side);

                            {
                                if (current_edge.Place.IsNode 
                                    && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId, out string? comment))
                                {
                                    //logger.Info($"Adjacent to hot node {current_place.NodeId}/{comment} is by outgoing road ? @ {target_place.NodeId}, {(step_info.IsForbidden ? "forbidden" : "")}, weight {outgoing_weight}, updated {updated}");
                                }
                            }
                        }

                        stats.AddNode(degree: adjacent_count);
                    }


                var adj_predicate = connectionPredicate;
                if (adj_predicate == null && coverGrid != null)
                    adj_predicate = (curr, adj, _) => coverGrid.CanUseDirectAdjacent(curr, adj, start_bucket, end_bucket);


                foreach ((var target_edge, TRoadId connecting_road_map_id)
                         in this.map.GetAllAdjacent(calc, current_edge.Place, start_bucket, end_bucket,
                             adj_predicate, finderConfig.PreserveRoundabouts))
                {
                    ++adjacent_count;

                    // checking against going directly back
                    if (target_edge.Place.IsNode 
                        && current_edge.ProjectedSource is {} ret_index
                        && EqualityComparer<TNodeId>.Default.Equals(this.map.GetNode(ret_index),
                            target_edge.Place.NodeId))
                        continue;
                    
                    if (backtrack.ContainsKey(target_edge))
                    {
                        if (current_edge.Place.IsNode && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId, out string? comment))
                        {
                            logger.Info(
                                @$"Adjacent to hot node {current_edge.Place.NodeId}/{comment} is already 
used by outgoing road {connecting_road_map_id}@{target_edge.Place.NodeId}");
                        }

                        continue;
                    }

                    if (!canExpandOverJoint(is_forward_side, searchAlgorithm,current_edge,
                            target_edge, joint, 
                            opposite_backtrack))
                        continue;
                   var remaining_direct_cost = computeRemainingDistance( stats, heap,
                       coverGrid, connectionPredicate,
                         target_edge, end_bucket, cancellationToken);

                   #if DEBUG
                    {
                        var osm_id = map.GetOsmRoadId(connecting_road_map_id);
                        if (osm_id.Identifier ==983379789L)
                        {
                            ;
                        }
                    }
                    #endif
                    MoveInfo step_info = this.logic.GetFullStepInfo(
                        connecting_road_map_id, current_edge.ProjectedSource, current_edge.Place,
                        target_edge.Place, reversed: !is_forward_side);

                    TotalWeight outgoingTotalWeight = new TotalWeight(
                        (current_weight.Current + step_info.Weight),
                        remaining: remaining_direct_cost);

                    var passes_weight = passesWeight(weight_comparer,weightLimit, 
                        outgoingTotalWeight,target_edge,opposite_backtrack,
                        joined:joint!=null);
                    if (!passes_weight && DEBUG_SWITCH.Enabled)
                    {
                        ;
                    }

                    if (passes_weight)
                    {
                        var outgoing_info = new BacktrackInfo<TNodeId, TRoadId>(current_edge,
                            connecting_road_map_id,
                            step_info.GetCondition(),
                            step_info.SegmentLength,
                            step_info.Time,
                            isShortcut: false
                            );

                        bool updated = heap.TryAddOrUpdate(target_edge, outgoingTotalWeight, outgoing_info);
                        stats.Updated(is_forward_side);

                        {
                            if (current_edge.Place.IsNode && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId, out string? comment))
                            {
                                logger.Info(
                                    @$"Adjacent to hot node {current_edge.Place.NodeId}/{comment} is by 
outgoing road {connecting_road_map_id}@{target_edge.Place.NodeId}, {(step_info.IsForbidden ? "forbidden" : "")}, weight {outgoingTotalWeight}, updated {updated}");
                            }
                        }
                    }
                }

                stats.AddNode(degree: adjacent_count);
            }
        }

        private void prepareResult(ICover<TNodeId, TRoadId>? coverGrid, 
            RoadBucket<TNodeId, TRoadId> stageStart, 
            RoadBucket<TNodeId, TRoadId> stageEnd,
            SearchData<TNodeId, TRoadId> forwardData, 
            SearchData<TNodeId, TRoadId> backwardData,
            JointInfo<TNodeId, TRoadId> joint, 
            out TotalWeight forwardWeight, out TotalWeight backwardWeight, out WeightSteps<TNodeId, TRoadId> resultPath)
        {
            List<StepRun<TNodeId, TRoadId>> forward_steps = FinderHelper.RecreatePath(this.map, coverGrid,
                forwardData.Backtrack,
                Placement<TNodeId, TRoadId>.UserPoint(stageStart), joint.ForwardEdge, isReversed: false);
            var backward_steps = FinderHelper.RecreatePath(this.map, coverGrid,
                backwardData.Backtrack,
                Placement<TNodeId, TRoadId>.UserPoint(stageEnd), joint.BackwardEdge,
                isReversed: true);
            // skip first place, because it is shared with the result
            forward_steps.AddRange(FinderHelper.ReverseSteps(backward_steps).Skip(1));

            forwardWeight = joint.ForwardWeight;
            backwardWeight = joint.BackwardWeight;
            resultPath = new WeightSteps<TNodeId, TRoadId>(forward_steps,
                joinedWeight: joint.JoinedWeight,isDraft:false);
        }

        private static bool passesWeight(TotalWeightComparer weightComparer,
            Weight? weightLimit, TotalWeight weight,in Edge<TNodeId,TRoadId> expandPlace,
            Backtrack<TNodeId, TRoadId> oppositeBacktrack,bool joined)
        {
            if (weightLimit == null)
                return true;

            var opposite_boundary = Weight.Zero;
            if (!joined && !oppositeBacktrack.ContainsKey(expandPlace))
                opposite_boundary = oppositeBacktrack.MinReachWeight;
            TotalWeight min_weight = new TotalWeight(weight.Current,
                weight.Remaining + opposite_boundary);

            var passed = !WeightComparer.Instance.IsLess(weightLimit.Value, weightComparer.Combine( min_weight));
            return passed;
        }

        private Weight computeRemainingDistance(
            CompStatistics stats,
            MappedPairingHeap<Edge<TNodeId, TRoadId>, TotalWeight, BacktrackInfo<TNodeId, TRoadId>> heap,
            ICover<TNodeId, TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            in Edge<TNodeId, TRoadId> expansionPlace,
            RoadBucket<TNodeId, TRoadId> endBucket,
            CancellationToken cancellationToken)
        {
           Weight? remaining_weight = null;

            {
                if (heap.TryGetData(expansionPlace, out var adj_weight, out var adj_info))
                {
                    remaining_weight = adj_weight.Remaining;
                }
            }

            if (remaining_weight == null)
            {
                if (expansionPlace.Place.IsNode && this.userConfig.HACK_ExactToTarget)
                {
                    var adj_bucket = RoadBucket.GetRoadBuckets(new[] {expansionPlace.Place.NodeId},
                        this.map, this.map.Grid.Calc, allowSmoothing: false,allowGap:false)
                        .Single();

                    var sub_buckets = new List<RoadBucket<TNodeId, TRoadId>>() {adj_bucket, endBucket};
                    var worker = new RouteFinder<TNodeId, TRoadId>(this.logger, this.navigator,
                        this.map, coverGrid,
                        connectionPredicate,
                        weightLimit: null,
                        finderConfig with {DumpProgress = false},
                        new UserRouterPreferences() {HACK_ExactToTarget = false}.SetUniformSpeeds(),
                        sub_buckets, cancellationToken);
                    if (worker.TryFindRoute(sub_buckets) is {HasValue:true} remaining) 
                    {
                        ++stats.SuccessExactTarget;
                        remaining_weight = Weight.FromCost(Length.FromMeters(remaining.Value.RouteLegs
                            .SelectMany(x => x.Steps)
                            .Sum(it => it.IncomingDistance.Meters)), this.fastest);
                    }
                    else
                    {
                        //throw new InvalidOperationException($"Sub-path failed from n#{adj_place.NodeId}@{this.map.Nodes[adj_place.NodeId.Value]} to {end}");
                        ++stats.FailedExactTarget;
                    }
                }

                if (remaining_weight == null)
                    remaining_weight = Weight.FromCost(calc.GetFlatDistance(expansionPlace.Place.Point.Convert2d(),
                        endBucket.UserPoint), this.fastest);
                
            }

            return remaining_weight.Value;
        }


        private bool canExpandOverJoint(
            bool isForward,
            SearchAlgorithm searchAlgorithm,
            in Edge<TNodeId, TRoadId> currentPlace,
            in Edge<TNodeId, TRoadId> expansionPlace,
            in JointInfo<TNodeId, TRoadId>? joint,
            IReadOnlyBacktrack<TNodeId, TRoadId> oppositeBacktrack
        )
        {
            if (joint is { } j)
            {
                // after initial join, we work only in join mode, meaning we accepts
                // only updates on our side, and we
                // need to hit the opposite already fixed places

                if (searchAlgorithm == SearchAlgorithm.FastForward)
                    throw new NotSupportedException();

                if (!oppositeBacktrack.TryGetValue(expansionPlace, out var opposite_info, out _))
                {
                    // with A* approach it is OK to reach for new nodes, because there can still
                    // be optimal one, unknown yet (our weight is current+remaining, thus it
                    // optimizes SUM, not current). It is opposite in Dijkstra case, since
                    // it optimizes only current, thus we have to fail immediately
                    return searchAlgorithm == SearchAlgorithm.AStar;
                }

                if (!oppositeBacktrack.IsWithinJoinedOldBacktrack(
                        jointTimestamp: j.GetTimestamp(forward: !isForward),
                        placeTimestamp: opposite_info.Timestamp))
                {
                    // as comment above describes, in A* algorithm we can legally wander around
                    if (searchAlgorithm == SearchAlgorithm.Dijkstra)
                        return false;
                }

                if (!oppositeBacktrack.IsAtBacktrackSurface(
                        jointTimestamp: j.GetTimestamp(forward: !isForward),
                        oppositeOrigin: currentPlace))
                {
                    // in both algorithms we cannot go deep inside (canibalize) the opposite
                    // backtrack
                    return false;
                }

            }

            return true;
        }

      
    }
}