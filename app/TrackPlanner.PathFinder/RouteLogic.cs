using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.PathFinder.ContractionHierarchies;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder
{
    public sealed class RouteLogic<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly UserRouterPreferences userConfig;
        private readonly Speed fastest;
        private readonly IReadOnlySet<TNodeId> suppressedTraffic;
        private readonly HashSet<TNodeId> DEBUG_lowCostNodes;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly IGeoCalculator calc;

        public RouteLogic(IWorldMap<TNodeId, TRoadId> map, IGeoCalculator calc,
            UserRouterPreferences userConfig,
            IReadOnlySet<TNodeId> suppressedTraffic)
        {
            this.map = map;
            this.calc = calc;
            this.userConfig = userConfig;
            this.suppressedTraffic = suppressedTraffic;
            this.DEBUG_lowCostNodes = new HashSet<TNodeId>();
            userConfig.CalculateSpeedLimits(out _, out this.fastest);
        }



        private IEnumerable<TRoadId> getRoadsAt(in Placement<TNodeId, TRoadId> current)
        {
            if (current.IsCrossPoint)
            {
                return new[] {current.BaseRoadIndex.RoadId};
            }
            else if (current.IsNode)
            {
                return map.GetRoadsAtNode(current.NodeId).Select(it => it.RoadId);
            }
            else if (current.IsUserPoint || current.IsPrestart)
            {
                return ArraySegment<TRoadId>.Empty;
            }

            throw new InvalidOperationException($"Not possible {current.KindInfo}");
        }

        private bool isSuppressed(Placement<TNodeId, TRoadId> place)
        {
            return place.IsNode && this.suppressedTraffic.Contains(place.NodeId);
        }

        internal MoveInfo GetFullStepInfo(TRoadId connectingRoadId,
            RoadIndex<TRoadId>? projectedSource,
            Placement<TNodeId, TRoadId> currentPlace, Placement<TNodeId, TRoadId> targetPlace,
            bool reversed)
        {
#if DEBUG
#endif
            // this function has to be symmetric regarding current/target places, so it would be
            // arc dependent, not place dependent. The reason for it is split-computing
            //  o-o-o-O-o
            // here O is a joint place. We should get get the same sum of weight of left and right
            // side no matter when the joint lies. If we were focusing on places weight, it would 
            // mean joint was counted twice (bad).
            var target_point = targetPlace.Point;

            Length flat_step_length = calc.GetFlatDistance(currentPlace.Point.Convert2d(),
                target_point.Convert2d());

            var connecting_road_info = this.map.GetRoadInfo(connectingRoadId);

            RoadSpeedStyling connectingRoadSpeedStyling = connecting_road_info.GetRoadSpeedMode();

            double cost_scale_factor = 1.0 + this.userConfig.GetSurfaceSpeed(connectingRoadSpeedStyling.Mode).AddedCostFactor;

            CostInfo cost_info = CostInfo.None;

            {
                // it can be negative
                var cycleway_factor = this.userConfig.AddedCyclewayCostFactor;
                if (connecting_road_info.Kind == WayKind.Cycleway)
                {
                    if (cycleway_factor > 0) // only add this if is positive
                    {
                        cost_scale_factor += cycleway_factor;
                        cost_info |= CostInfo.Cycleway;
                    }
                }
                // if we would like to promote cycleway we do this by adding penalties
                // to non-cycleways
                else if (cycleway_factor < 0)
                {
                    cost_scale_factor -= cycleway_factor;
                    cost_info |= CostInfo.Cycleway;
                }
            }

            if (connecting_road_info.Kind == WayKind.Auxiliary)
            {
                cost_scale_factor += this.userConfig.AddedAuxiliaryRoadCostFactor;
                cost_info |= CostInfo.Auxiliary;
            }

            if (connecting_road_info.IsExpressTraffic)
            {
                if (isSuppressed(currentPlace) && isSuppressed(targetPlace))
                {
                    ; // default cost
                    if (currentPlace.IsNode)
                        this.DEBUG_lowCostNodes.Add(currentPlace.NodeId);
                    if (targetPlace.IsNode)
                        this.DEBUG_lowCostNodes.Add(targetPlace.NodeId);

                    cost_info |= CostInfo.Suppressed;
                }
                else
                {
                    var vol_level = connecting_road_info.VolumeTrafficLevel;
                    if (vol_level > 0)
                    {
                        cost_scale_factor += vol_level * this.userConfig.AddedHighVolumeTrafficFactor;
                        cost_info |= CostInfo.HighVolume;
                    }
                }
            }

            {

                if (connecting_road_info.IsDangerous)
                {

                    if (isSuppressed(currentPlace) && isSuppressed(targetPlace))
                    {
                        ; // default cost
                        if (currentPlace.IsNode)
                            this.DEBUG_lowCostNodes.Add(currentPlace.NodeId);
                        if (targetPlace.IsNode)
                            this.DEBUG_lowCostNodes.Add(targetPlace.NodeId);

                        cost_info |= CostInfo.Suppressed;
                    }
                    else
                    {
                        cost_scale_factor += connecting_road_info.SpeedLimitLevel
                                             * this.userConfig.AddedMotorDangerousTrafficFactor;
                        cost_info |= CostInfo.DangerousSpeed;
                    }
                }
                else if (connecting_road_info.IsUncomfortable)
                {
                    if (isSuppressed(currentPlace) && isSuppressed(targetPlace))
                    {
                        ; // default cost
                        cost_info |= CostInfo.Suppressed;
                    }
                    else
                    {
                        cost_scale_factor += connecting_road_info.SpeedLimitLevel
                                             * this.userConfig.AddedMotorUncomfortableTrafficFactor;
                        cost_info |= CostInfo.UncomfortableSpeed;

                    }
                }
                else if (currentPlace.IsNode
                         && targetPlace.IsNode
                         && this.map.GetBikeFootRoadExpressNearby(nodeId: currentPlace.NodeId) is { } nearby_id1
                         && this.map.GetBikeFootRoadExpressNearby(nodeId: targetPlace.NodeId) is { } nearby_id2)
                {
                    if (isSuppressed(currentPlace) && isSuppressed(targetPlace))
                    {
                        ; // default cost
                        cost_info |= CostInfo.Suppressed;
                    }
                    else
                    {
                        RoadInfo<TNodeId> danger = this.map.GetRoadInfo(nearby_id1);
                        {
                            var r2 = this.map.GetRoadInfo(nearby_id2);
                            if (r2.Traffic.MoreRiskyThan(danger.Traffic))
                                danger = r2;
                        }
                        double added = 0;
                        var vol_level = danger.VolumeTrafficLevel;
                        if (vol_level > 0)
                        {
                            added += vol_level * this.userConfig.AddedHighVolumeTrafficFactor;
                            cost_info |= CostInfo.HighVolume;
                        }

                        var speed_level = danger.SpeedLimitLevel;
                        if (speed_level > 0)
                        {
                            added += speed_level
                                     * this.userConfig.AddedMotorDangerousTrafficFactor;
                            cost_info |= CostInfo.DangerousSpeed;
                        }

                        cost_info |= CostInfo.HighTrafficBikeLane;
                        cost_scale_factor += added * this.userConfig.BikeFootHighTrafficScaleFactor;
                        //logger.Info($"Higher cost {cost_factor} for way {incoming_road_id}");
                    }
                }
            }

            if (this.userConfig.UseStableRoads && !connecting_road_info.Kind.IsStable())
            {
                cost_scale_factor += this.userConfig.AddedUnstableRoadCostFactor;
                cost_info |= CostInfo.UnstableRoad;
            }

            TimeSpan added_run_time;

            // segment between user point and cross point
            bool is_snap = currentPlace.IsUserPoint || targetPlace.IsUserPoint;

            if (is_snap)
            {
                // do not mark snap as forbidden because it would start counting its length
                //    is_forbidden = false;
                // use top speed to prefer slightly longer snap to some node, instead snapping right  to the closest road
                // and then moving by it 1-2 meters, which is absurd
                added_run_time = flat_step_length / this.fastest;
                cost_scale_factor = 1.0;
                cost_info = CostInfo.None;
            }
            else
            {
                added_run_time = this.userConfig.GetRideTime(flat_step_length, connectingRoadSpeedStyling.Mode,
                    currentPlace.Point.Altitude * (reversed ? -1 : +1), target_point.Altitude * (reversed ? -1 : +1),
                    out _);
            }

            var added_run_cost = Weight.FromCost(added_run_time, cost_scale_factor);

            if (!is_snap)
            {
                added_run_cost += CalculateJointWeight(projectedSource, currentPlace,
                    connectingRoadId,
                    targetPlace.NodeIndexOrNull);
                
                // crossing or joining high-traffic road
                if (this.userConfig.JoiningHighTraffic != TimeSpan.Zero
                    && !connecting_road_info.IsExpressTraffic)
                {
                    // we are hitting high-traffic road
                    Option<RoadInfo<TNodeId>> high_traffic = getRoadsAt(targetPlace)
                        .Concat(getRoadsAt(currentPlace))
                        .Select(it => this.map.GetRoadInfo(it))
                        .FirstOrNone(it => it.IsExpressTraffic);
                    if (high_traffic.HasValue)
                    {
                        TimeSpan join_traffic = this.userConfig.JoiningHighTraffic;
                        // most likely there is parallel road, going the other way
                        // so we would have double penalty in comparison to two-way road
                        // with the same traffic
                        if (high_traffic.Value.OneWay)
                            join_traffic /= 2;
                        added_run_time += join_traffic;
                        // we add it in separate step, because cost factor of crossing is constant
                        added_run_cost += Weight.FromCost(join_traffic, 1.0);
                        cost_info |= CostInfo.JoiningHighTraffic;
                    }
                }
            }


            return (MoveInfo.Create(added_run_cost, flat_step_length,
                        isForbidden: connecting_road_info.IsCyclingForbidden(),
                        isSnap: is_snap) with
                    {
                        CostInfo = cost_info,
                        Time = added_run_time,
                    })
                .WithSpeedMode(connectingRoadSpeedStyling)
                ;
        }

        internal Weight SumTotalJointWeight(Edge<TNodeId, TRoadId> oppositeEdge, 
            TotalWeight oppositeWeight,
            Edge<TNodeId, TRoadId> currentEdge, TotalWeight currentWeight)
        {
            if (oppositeEdge.Place != currentEdge.Place)
                throw new ArgumentException("Joint place is not shared");
            
            return TotalWeight.JoinFinal(oppositeWeight, currentWeight)
                +CalculateJointWeight(currentEdge.ProjectedSource,currentEdge.Place,
                    oppositeEdge.Place.BaseRoadIndex.RoadId,
                    oppositeEdge.ProjectedSource);
        }

        internal Weight CalculateJointWeight(RoadIndex<TRoadId>? projectedSource,
            Placement<TNodeId,TRoadId> currentPlace,
            TRoadId? outgoingRoadId,
            RoadIndex<TRoadId>? target)
        {
            if (projectedSource is not { } proj_source 
                || target is not {} target_idx 
                || this.userConfig.Turning==TimeSpan.Zero)
                return Weight.Zero;

            var current = currentPlace.NodeId;
            if (!this.map.IsRoadCrossroad(current))
                return Weight.Zero;

            if (COMP_MODE.WeightedTurns)
            {
                var incoming_road = this.map.GetRoadInfo(currentPlace.BaseRoadIndex.RoadId);
                var outgoing_road = this.map.GetRoadInfo(outgoingRoadId!.Value); 
                
                // we are supposed to ride on the cycleway, so do not add penalties here
                if (outgoing_road.Kind== WayKind.Cycleway || incoming_road.Kind== WayKind.Cycleway)
                    return Weight.Zero;
                
                var current_point = this.map.GetPoint(current);
                var source_point = this.map.GetPoint(proj_source);
                var incoming_bearing = this.calc.GetBearing(source_point, current_point);
                var angle_diff = this.calc.GetAbsoluteBearingDifference(incoming_bearing,
                    this.calc.GetBearing(current_point, this.map.GetPoint(target_idx)));

                RoadIndex<TRoadId>[] adjacents = this.map.GetAdjacentRoads(current).ToArray();

                foreach (var alt_road_idx in adjacents)
                {
                    var alt_node = this.map.GetNode(alt_road_idx);
                    if (alt_road_idx== proj_source || alt_road_idx== target_idx)
                        continue;

                    var alt_road = this.map.GetRoadInfo(alt_road_idx.RoadId);

                    if (outgoing_road.IsSignificantlyMoreObvious(alt_road) 
                        && incoming_road.IsSignificantlyMoreObvious(alt_road)
                        )
                    {
                        continue;
                    }
                    
                    var alt_diff = this.calc.GetAbsoluteBearingDifference(incoming_bearing,
                        this.calc.GetBearing(current_point, this.map.GetPoint(alt_node)));
                    if (alt_diff < angle_diff)
                    {
                        // found a road which takes smaller turn than the current one
                        return Weight.FromCost(this.userConfig.Turning, 1.0);
                    }
                }
            }

            return Weight.Zero;
        }
    }
}