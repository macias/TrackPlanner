﻿using System.Collections.Generic;
using System.Linq;
using Geo;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder
{
    public sealed class AttractionsWorker<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly record struct AttractionSnap(GeoPoint point, int legIndex,
            // for ordering
            Length distanceAlongLeg,Length distanceFromLeg)
        {
            
        }
        private readonly ILogger logger;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly IGeoCalculator calc;

        public AttractionsWorker(ILogger logger, IWorldMap<TNodeId, TRoadId> map, IGeoCalculator calc)
        {
            this.logger = logger;
            this.map = map;
            this.calc = calc;
        }

        public IEnumerable<List<(MapZPoint<TNodeId,TRoadId> place, PointOfInterest attraction)>> FindAttractions(IReadOnlyList<GeoPoint> checkPoints,
            Length range, PointOfInterest.Feature excludeFeatures)
        {
            var attractions = new Dictionary<TNodeId,AttractionSnap>();

                int leg_idx = 0;
                foreach (var (prev, next) in checkPoints.Slide())
                {
                    findAttractions(attractions, leg_idx, prev.Convert3d(), next.Convert3d(), range, excludeFeatures);
                    ++leg_idx;
                }

                for (int i = 0; i < checkPoints.Count - 1; ++i) // 2 checkpoints make 1 leg, thus -1
                    yield return attractions.Where(it => it.Value.legIndex == i)
                        .OrderBy(it => it.Value.distanceAlongLeg).ThenBy(it => it.Value.distanceFromLeg)
                        .SelectMany(it => this.map.GetAttractions(it.Key)
                            .Select(attr => (new MapZPoint<TNodeId,TRoadId>(it.Value.point.Convert3d(), it.Key
                            #if DEBUG
                                ,DEBUG_mapRef:null
                            #endif
                            ), attr)))
                        .ToList();
        }

        private void findAttractions(Dictionary<TNodeId,AttractionSnap> attractions, int legIndex, 
            GeoZPoint prev, GeoZPoint next,
            Length range, PointOfInterest.Feature excludeFeatures)
        {
            var boundary = this.calc.GetBoundary(range, prev.Convert2d(), next.Convert2d());
            Length total_range = range+ this.calc.GetFlatDistance(prev.Convert2d(), next.Convert2d());

            foreach (var (node_id, pt, attr) in this.map.GetAttractionsWithin(boundary, excludeFeatures))
            {
                var area_range = this.calc.GetFlatDistance(pt, prev.Convert2d());
                if (area_range > range)
                {
                    area_range += this.calc.GetFlatDistance(pt, next.Convert2d());
                    if (area_range > total_range)
                        continue;
                }

                var (dist, _, along_seg) = this.calc.GetFlatDistanceToArcSegment(pt.Convert3d(), prev, next,stable:false);
                if (attractions.TryGetValue(node_id,out var snap) && snap.distanceFromLeg<dist)
                    continue;
                attractions[node_id] = new AttractionSnap(pt,legIndex, along_seg, dist);
            }
        }
    }
}