﻿namespace TrackPlanner.PathFinder
{
    public delegate bool ValidConnectionPredicate<in TNodeId, TRoadId>(TNodeId currentId, TNodeId targetId,
        TRoadId? connectingRoad)
        where TNodeId : struct
        where TRoadId : struct;

   
}