﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TrackPlanner.Shared;
using TrackRadar.Collections;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.ContractionHierarchies;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{
    internal enum RouteMode
    {
        General,
        CycleSnap,
    }
}