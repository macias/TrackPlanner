using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder.Drafter
{
    public sealed record class DraftOptions
    {
        public static DraftOptions GetLegacyTests()
        {
            return new DraftOptions()
            {
                SpeedStyling = new RoadSpeedStyling("draft",RoadSpeeding.UnknownLoose, RoadStyling.Unknown)
            };
        }

        public bool OffsetRealPoints { get; set; }
        public RoadSpeedStyling SpeedStyling { get; set; }

        public DraftOptions()
        {
            this.SpeedStyling = new RoadSpeedStyling("draft",RoadSpeeding.UnknownStructured,
                RoadStyling.Unknown);
        }
    }
}