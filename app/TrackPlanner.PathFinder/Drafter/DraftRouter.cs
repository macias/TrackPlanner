﻿namespace TrackPlanner.PathFinder.Drafter;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.PathFinder.Shared;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

public sealed class DraftRouter<TNodeId, TRoadId>
    where TNodeId : struct
    where TRoadId : struct
{
    private readonly ILogger logger;
    private readonly IGeoCalculator calc;
    private readonly DraftOptions draftOptions;
    private readonly bool compactPreservesRoads;

    public DraftRouter(ILogger logger,
        IGeoCalculator calc,
        DraftOptions draftOptions,
        bool compactPreservesRoads)
    {
        this.logger = logger;
        this.calc = calc;
        this.draftOptions = draftOptions;
        this.compactPreservesRoads = compactPreservesRoads;
    }


    private void populatePoints(List<GeoPoint> points, int limit)
    {
        while (points.Count < limit)
        {
            for (int i = points.Count - 1; i > 0; --i)
            {
                points.Insert(i, this.calc.GetMidPoint(points[i - 1].Convert3d(), points[i].Convert3d()).Convert2d());
            }
        }
    }

    private IEnumerable<LegPlan<TNodeId, TRoadId>> buildDraftLegs(GeoPoint start,
        GeoPoint end,
        UserRouterPreferences userPrefs, bool mockReal, CancellationToken token)
    {
        var steps = FindStageRoute(start, end, userPrefs, mockReal)
            .Select(it => FlowJump<TNodeId,TRoadId>.Create(it))
            .ToList();

        var helper = new RouteHelper<TNodeId, TRoadId>(logger, map: null,
            mockReal ? userPrefs.CheckpointIntervalLimit : TimeSpan.Zero,
            compactPreservesRoads);
        var route = helper.SplitLegs(new IRequestPoint[]
            {
                new RequestPoint<TNodeId>(start),
                new RequestPoint<TNodeId>(end)
            },
            new List<LegFlow<TNodeId, TRoadId>>() {new LegFlow<TNodeId, TRoadId>(steps, isDraft: !mockReal)}, token);
            /*if (!mockReal)
            foreach (var leg in route.Legs)
                leg.IsDrafted = true;*/
        return route.Legs;
    }

    public List<StepRun<TNodeId, TRoadId>> FindStageRoute(GeoPoint start, GeoPoint end, 
        UserRouterPreferences userPrefs, bool splitLeg)
    {
        var points = new List<GeoPoint>() {start, end};
        if (splitLeg)
            populatePoints(points, limit: 128);

        var condition = new RoadCondition(this.draftOptions.SpeedStyling,
            CostInfo.None, isForbidden: false);
        var steps = new List<StepRun<TNodeId, TRoadId>>(capacity: points.Count);
        steps.Add(new StepRun<TNodeId, TRoadId>()
        {
         Place =   Placement<TNodeId, TRoadId>.Draft(points[0].Convert3d()),
         IncomingRoadId = new TRoadId(),
           IncomingCondition  = condition,
            IncomingDistance = Length.Zero, 
            IncomingTime = TimeSpan.Zero
        });
        
        for (int i = 1; i < points.Count; ++i)
        {
            var dist = this.calc.GetDistance(points[i - 1].Convert3d(), points[i].Convert3d());
            var time = userPrefs.GetRideTime(dist, condition.SpeedStyling.Mode, Length.Zero, Length.Zero, out _);
            var coords = new MapZPoint<TNodeId, TRoadId>(points[i].Convert3d(), null
#if DEBUG
                , DEBUG_mapRef: null
#endif
            );
            steps.Add(new StepRun<TNodeId, TRoadId>()
                    {Place = Placement<TNodeId, TRoadId>.Draft(coords.Point),
                IncomingRoadId = new TRoadId(), 
                IncomingCondition = condition, 
                IncomingDistance = dist,
                IncomingTime = time});
        }

        return steps;
    }


    public RouteResponse<TNodeId, TRoadId> BuildPlan(PlanRequest<TNodeId> request,
        bool mockReal, CancellationToken token)
    {
        var plan = new RoutePlan<TNodeId, TRoadId>()
        {
            Legs = new List<LegPlan<TNodeId, TRoadId>>(),
        };
        RequestPoint<TNodeId>[] points = request.GetPointsSequence().ToArray();
        if (mockReal && this.draftOptions.OffsetRealPoints)
        {
            {
                if (!points[0].EnforcePoint)
                {
                    var p = points[0].UserPoint;
                    points[0] = points[0] with
                    {
                        UserPoint = GeoZPoint.FromDegreesMeters(
                            p.Latitude.Degrees + 0.0002,
                            p.Longitude.Degrees - 0.0006,
                            0)
                    };
                }

                if (!points[^1].EnforcePoint)
                {
                    var p = points[^1].UserPoint;
                    points[^1] = points[^1] with
                    {
                        UserPoint = GeoZPoint.FromDegreesMeters(
                            p.Latitude.Degrees + 0.0004,
                            p.Longitude.Degrees - 0.0004,
                            0)
                    };
                }
            }
        }

        foreach (var (prev, next) in points.Slide())
        {
            IEnumerable<LegPlan<TNodeId, TRoadId>> draft_legs = buildDraftLegs(prev.UserPointFlat,
                next.UserPointFlat,
                request.RouterPreferences, mockReal, token);
            plan.Legs.AddRange(draft_legs);
        }

        return new RouteResponse<TNodeId, TRoadId>()
        {
            Route = plan,
            Names = request.GetSequenceWithAutoPoints(plan).Select(_ => (string?) null).ToList(),
        };
    }
}