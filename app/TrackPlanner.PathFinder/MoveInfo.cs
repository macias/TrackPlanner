﻿using MathUnit;
using System;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;


namespace TrackPlanner.PathFinder
{
    public readonly record struct MoveInfo
    {
        public static MoveInfo Create( Weight addedCost, Length segmentLength, bool isForbidden, 
            bool isSnap)
        {
            return new MoveInfo()
            {
                IsForbidden = isForbidden,
                SegmentLength = segmentLength,
                Weight = Weight.CreateForbidden(segmentLength,isForbidden,isSnap)+addedCost,
            };
        }

        public bool IsForbidden { get; private init; }
        public RoadSpeedStyling RoadSpeedStyling { get; private init; }
        public CostInfo CostInfo { get; init; }
        public Weight Weight { get; init; }
        public TimeSpan Time { get; init; }
        public Length SegmentLength { get; private init; }
        //public Length ForbiddenLength { get; private init;}

        
      /*  public MoveInfo ExtendWith(MoveInfo extension)
        {
            return extension with
            {
                Weight = this.Weight + extension.Weight,
                Time = this.Time + extension.Time,
                SegmentLength = this.SegmentLength + extension.SegmentLength,
                ForbiddenLength = this.ForbiddenLength+extension.ForbiddenLength,
            };
        }*/

        public RoadCondition GetCondition()
        {
            return new RoadCondition(RoadSpeedStyling, CostInfo, IsForbidden);
        }
        public MoveInfo WithSpeedMode(RoadSpeedStyling styling)
        {
            return this with {RoadSpeedStyling = styling};
        }
        
     /*   public  Weight GetWeight()
        {
            return PathFinder.Weight.Create(ForbiddenLength, Weight);
        }
*/
    }
}