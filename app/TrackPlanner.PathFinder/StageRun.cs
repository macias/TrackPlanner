﻿using System.Collections.Generic;

namespace TrackPlanner.PathFinder
{
    public readonly record  struct StageRun<TNodeId,TRoadId>
        where TNodeId: struct
        where TRoadId : struct
    {
        public List<StepRun<TNodeId,TRoadId>> Steps { get; }
        public bool IsDraft { get; }

        public StageRun(List<StepRun<TNodeId,TRoadId>> steps,bool isDraft)
        {
            Steps = steps;
            IsDraft = isDraft;
        }
    }
    
}