﻿using MathUnit;
using System;
using System.Collections.Generic;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    public interface IBasicLeg<TNodeId, TRoadId,out TStep> 
        where TNodeId : struct
        where TRoadId : struct
        where TStep : IBasicStep<TNodeId, TRoadId>
    {
        IReadOnlyList<TStep> Steps { get; }
        bool IsDraft { get; }
    }

    public sealed class LegFlow<TNodeId, TRoadId> : IBasicLeg<TNodeId,TRoadId,FlowJump<TNodeId,TRoadId>>
        where TNodeId : struct
        where TRoadId : struct
    {
        public List<FlowJump<TNodeId,TRoadId>> Flow { get; }
        IReadOnlyList<FlowJump<TNodeId, TRoadId>> IBasicLeg<TNodeId, TRoadId,FlowJump<TNodeId,TRoadId>>.Steps => this.Flow;
        public bool IsDraft { get; }

        public LegFlow(List<FlowJump<TNodeId,TRoadId>> flow,bool isDraft)
        {
            Flow = flow;
            IsDraft = isDraft;
        }
      
    }

    // because we alter route (we flatten roundabouts) before computing turn notification we
    // need richer info to preserve important info (like traffic flow/direction)
    public readonly record struct FlowJump<TNodeId, TRoadId> : IBasicStep<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public static FlowJump<TNodeId, TRoadId> Create(StepRun<TNodeId, TRoadId> step)
        {
            return new FlowJump<TNodeId, TRoadId>()
            {
                Place = step.Place,
                IncomingCondition = step.IncomingCondition,
                IncomingDistance = step.IncomingDistance,
                IncomingTime = step.IncomingTime,
                FacingForwardIndex = RoadIndex<TRoadId>.InvalidIndex(),
                FacingBackwardIndex = RoadIndex<TRoadId>.InvalidIndex(),
            };
        }
        
        public Placement<TNodeId, TRoadId> Place { get; init; }

        // for continuous road this will be the same index, but on the road switch -- no
        // for start/end of the leg those indices will be the same (strictly speaking the invalid one should be null)
        private readonly RoadIndex<TRoadId> facingBackwardIndex;
        public RoadIndex<TRoadId> FacingBackwardIndex
        {
            // i.e. facing backward
            get { return this.facingBackwardIndex;}
            init
            {
                this.facingBackwardIndex = value;
                this.IncomingRoadId = value.RoadId;
            }
        } 

        public RoadIndex<TRoadId> FacingForwardIndex { get; init; } // i.e. facing forward

        // it does NOT describe the road, but rider against the road
        public TrafficDirection<TRoadId> IncomingRelativeTraffic { get; init; }
        public RoadCondition IncomingCondition { get; init; }
        public Length IncomingDistance { get; init; }
        public TimeSpan IncomingTime { get; init; }

        public TRoadId IncomingRoadId { get; init; }

        public FlowJump<TNodeId, TRoadId> MirrorRoadsForInitial(in FlowJump<TNodeId, TRoadId> next)
        {
            return this with
            {
                IncomingCondition = next.IncomingCondition,
                IncomingDistance = Length.Zero,
                IncomingTime = TimeSpan.Zero
            };
        }
        
        public MapZPoint<TNodeId, TRoadId> GetRefCoords(IWorldMap<TNodeId, TRoadId>? map)
        {
            return this.Place.GetCoords(map);
        }
  
    }
    
}