﻿using System;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    public static class PlaceEntry
    {
        public static PlaceEntry<TNodeId, TRoadId> Create<TNodeId, TRoadId>(Placement<TNodeId, TRoadId> current,
            Placement<TNodeId, TRoadId> entryPlace,
            TRoadId entryRoad)
            where TNodeId : struct
            where TRoadId : struct
        {
            return new PlaceEntry<TNodeId, TRoadId>(current, entryPlace, entryRoad);
        }
    }

    public readonly record struct PlaceEntry<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public static PlaceEntry<TNodeId, TRoadId> UserPoint(RoadBucket<TNodeId, TRoadId> bucket)
        {
            return new PlaceEntry<TNodeId, TRoadId>(Placement<TNodeId, TRoadId>.UserPoint(bucket), null, null);
        }

        public static PlaceEntry<TNodeId, TRoadId> Prestart(GeoZPoint point)
        {
            return new PlaceEntry<TNodeId, TRoadId>(Placement<TNodeId, TRoadId>.Prestart(point), null, null);
        }

        public Placement<TNodeId, TRoadId> Current { get; }
        public Placement<TNodeId, TRoadId>? EntryPlace { get; }
        public TRoadId? EntryRoad { get; }

        public PlaceEntry(Placement<TNodeId, TRoadId> current, Placement<TNodeId, TRoadId>? entryPlace, TRoadId? entryRoad)
        {
            Current = current;
            EntryPlace = entryPlace;
            EntryRoad = entryRoad;
        }

        public bool TryReverse(out PlaceEntry<TNodeId, TRoadId> reversed)
        {
            if (EntryRoad == null)
            {
                reversed = default;
                return false;
            }

            reversed = new PlaceEntry<TNodeId, TRoadId>(EntryPlace!.Value, Current, EntryRoad);
            return true;
        }

        public PlaceEntry<TNodeId, TRoadId> Reverse()
        {
            if (!TryReverse(out var reversed))
                throw new InvalidOperationException();

            return reversed;
        }
    }

}