﻿using System;


namespace TrackPlanner.PathFinder
{
   
    internal sealed class LeastSquares
    {
        // https://www.mathsisfun.com/data/least-squares-regression.html
        private double xSum;
        private double ySum;
        private double xySum;
        private double x2Sum;
        private int count;

        [Obsolete]
        public LeastSquares()
        {
            
        }

        public void Add(double x, double y)
        {
            this.xSum += x;
            this.ySum += y;
            this.xySum += x * y;
            this.x2Sum += x * x;
            ++this.count;
        }

        public void Compute(out double m, out double b)
        {
            m = (this.count * this.xySum - this.xSum * this.ySum) / (this.count * this.x2Sum - this.xSum * this.xSum);
            b = (this.ySum - m * this.xSum) / this.count;
        }
    }
  

}