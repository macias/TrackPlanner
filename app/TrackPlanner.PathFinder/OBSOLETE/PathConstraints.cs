﻿using System;
using System.Collections.Generic;

namespace TrackPlanner.PathFinder
{
    [Obsolete]
    public readonly record struct PathConstraints<TNodeId>
    {
        public TotalWeight? WeightLessThan { get; init; }
        public IReadOnlySet<TNodeId>? ExcludedNodes { get; init; }

        [Obsolete]
        public PathConstraints()
        {
            this.WeightLessThan = null;
            ExcludedNodes = null;
        }

        public bool IsAcceptable(TNodeId nodeId)
        {
            return this.ExcludedNodes == null || !this.ExcludedNodes.Contains(nodeId);
        }
    }
}