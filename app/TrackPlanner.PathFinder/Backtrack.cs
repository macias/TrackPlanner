﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder
{
    internal interface IReadOnlyBacktrack<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public Weight MinReachWeight { get; }
        int Count { get; }
        (BacktrackInfo<TNodeId, TRoadId> info,
            TotalWeight weight) this[in Edge<TNodeId, TRoadId> place] { get; }

        bool TryGetValue(Edge<TNodeId, TRoadId> place,
            out BacktrackInfo<TNodeId, TRoadId> info,
            out TotalWeight totalWeight);

        bool ContainsKey(in Edge<TNodeId, TRoadId> place);

        bool IsAtJointBoundary(int jointTimestamp, int placeTimestamp,
            in Edge<TNodeId, TRoadId> oppositeOrigin);

        bool IsWithinJoinedOldBacktrack(int jointTimestamp, int placeTimestamp);
        bool IsAtBacktrackSurface(int jointTimestamp, in Edge<TNodeId, TRoadId> oppositeOrigin);
    }

    internal sealed class Backtrack<TNodeId, TRoadId> : IReadOnlyBacktrack<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly IWorldMap<TNodeId, TRoadId> map;

        // node id -> source node id
        private readonly Dictionary<Placement<TNodeId, TRoadId>,
            CompactDictionaryFill<RoadIndex<TRoadId>?, (BacktrackInfo<TNodeId, TRoadId> info, TotalWeight weight)>> data;

        private int count;
        public int Count => this.count;
        public int Timestamp => this.count;
        // Consider the scenario
        // x 10
        // |
        // A---------------------y 100
        // A is current node with weight 0, the next node that will be
        // processed is the upper "x" with weight 10. But at this point
        // what is reachability weight of A for real? It is 10, not 0,
        // because if opposite side would go through "x" it would be exactly
        // 10, if through y it would be at least 10 (it cannot be less
        // thanks to Dijsktra guarantee, if it was less we would pick
        // y first, not x)
        //
        // the only downside to this is the opposite side has to check
        // if its move/expansion is to free node, or already within
        // backtrack of the other side -- if the latter it cannot use this
        // weight
        // o----(--->o----------o
        // ( boundary of reachable weight
        // we are moving from left to right, so the segment (---> would be
        // added twice
        public Weight MinReachWeight { get; private set; }


        public (BacktrackInfo<TNodeId, TRoadId> info, TotalWeight weight) this[in Edge<TNodeId, TRoadId> place]
            => this.data[place.Place][place.ProjectedSource];

        public Backtrack(IWorldMap<TNodeId, TRoadId> map)
        {
            this.map = map;
            this.data = new Dictionary<Placement<TNodeId, TRoadId>, CompactDictionaryFill<RoadIndex<TRoadId>?, 
                (BacktrackInfo<TNodeId, TRoadId> info, TotalWeight weight)>>();
        }

        #if DEBUG
        public int DEBUG_GetDepth( Edge<TNodeId,TRoadId> current)
        {
            int depth = -1;
            while (TryGetValue(current, out var info, out _))
            {
                current =  info.Source;
                ++depth;
            }

            return depth;
        }
        #endif

        public BacktrackInfo<TNodeId, TRoadId> Add(Edge<TNodeId, TRoadId> edge,
            BacktrackInfo<TNodeId, TRoadId> info, 
            TotalWeight weight)
        {
            info = info with {Timestamp = this.Timestamp};
            
            bool changed = false;
            if (!this.data.TryGetValue(edge.Place, out var existing_sub))
            {
                existing_sub = new CompactDictionaryFill<RoadIndex<TRoadId>?, 
                    (BacktrackInfo<TNodeId, TRoadId> info, TotalWeight weight)>();
                this.data.Add(edge.Place,existing_sub);
                changed = true;
            }

            if (existing_sub.TryAdd(edge.ProjectedSource, (info, weight), out _))
                changed = true;
            
            if (changed)
                ++count;
            else
            {
                // user point could be added twice because of the bucket expansion
                if (!edge.Place.IsUserPoint)
                    throw new ArgumentException($"Cannot add {edge} to backtrack.");
            }

            this.MinReachWeight = weight.Current;
            return info;
        }

        public bool TryGetValue(Edge<TNodeId,TRoadId> place,
            out BacktrackInfo<TNodeId, TRoadId> info, 
            out TotalWeight totalWeight)
        {
            if (!this.data.TryGetValue(place.Place, out var sub_dict))
            {
                info = default;
                totalWeight = default;
                return false;
            }

            var result = sub_dict.TryGetValue(place.ProjectedSource, out var value);
            (info, totalWeight) = value;
            return result;
        }

        public IEnumerable<(Edge<TNodeId, TRoadId> edge, BacktrackInfo<TNodeId, TRoadId> info, TotalWeight totalWeight)>
            TryGetValues(Placement<TNodeId, TRoadId> place)
        {
            if (!this.data.TryGetValue(place, out var sub_dict))
                return Array.Empty<(Edge<TNodeId, TRoadId> edge, BacktrackInfo<TNodeId, TRoadId> info, TotalWeight totalWeight)>();

            return sub_dict.Select(it => (new Edge<TNodeId, TRoadId>(map,place, it.Key),
                it.Value.info, it.Value.weight));
        }

        public bool ContainsKey(in Edge<TNodeId,TRoadId> place)
        {
            return this.TryGetValue(place,out _,out _);
        }

        public bool IsWithinJoinedOldBacktrack(int jointTimestamp, int placeTimestamp)
        {
            // we are checking both time and "position" -- we need to verify we are hitting opposite boundary
            // from the moment joint between start and end was created
            
            // if the entry is newer than timestamp of the joint it is not old
            return placeTimestamp <= jointTimestamp;
        }

        public bool IsAtBacktrackSurface(int jointTimestamp, in Edge<TNodeId, TRoadId> oppositeOrigin)
        {
            // if the origin of our current move is already in the backtrack and it is older one
            // it means we are trying to cannibalize the data, i.e. we are going deeper than just boundary
            // O--->X--->
            // we are at X, we went from O, but the opposite side have both O and X in its backtrack
            // thus we conclude X is not boundary for opposite side
            if (TryGetValue(oppositeOrigin, out var origin_info, out _)
                && origin_info.Timestamp <= jointTimestamp)
                return false;

            return true;
        }
        public bool IsAtJointBoundary(int jointTimestamp, int placeTimestamp, 
            in Edge<TNodeId, TRoadId> oppositeOrigin)
        {
            // we are checking both time and "position" -- we need to verify we are hitting opposite boundary
            // from the moment joint between start and end was created
            
            // if the entry is newer than timestamp of the joint it is not old
            if (!IsWithinJoinedOldBacktrack(jointTimestamp, placeTimestamp ))
                return false;
            // if the origin of our current move is already in the backtrack and it is older one
            // it means we are trying to cannibalize the data, i.e. we are going deeper than just boundary
            // O--->X--->
            // we are at X, we went from O, but the opposite side have both O and X in its backtrack
            // thus we conclude X is not boundary for opposite side
            if (!IsAtBacktrackSurface(jointTimestamp, oppositeOrigin))
                return false;

            return true;
        }

        public void DumpKml(string path)
        {
            var input = new TrackWriterInput() {Title = null};
            foreach (var (place,value) in this.data
                         .SelectMany(it => it.Value
                             .Select(s => KeyValuePair.Create(new Edge<TNodeId,TRoadId>(map,it.Key,s.Key),s.Value)))
                         .OrderBy(it => it.Value.info.Timestamp))
            {
                var (info, weight) = value;
                var label = $"{info.Timestamp} {weight}";
                input.AddPoint(place.Place.Point, label, comment: null, info.Timestamp==0 ? PointIcon.ParkingIcon: PointIcon.DotIcon);
            }

            var kml = input.BuildDecoratedKml();

            using (var stream = new FileStream(path, FileMode.CreateNew, FileAccess.Write))
            {
                kml.Save(stream);
            }
        }
        
    }
}