﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Backend;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{
    // A* algorithm with pairing heap
    // https://en.wikipedia.org/wiki/A*_search_algorithm
    // https://brilliant.org/wiki/pairing-heap/
    // https://en.wikipedia.org/wiki/Pairing_heap

    internal sealed class DebugFinderHistory<TNodeId,TRoadId> : IDisposable
        where TNodeId: struct
        where TRoadId : struct
    {
        private readonly record struct HistoryEntry(int index, 
            TotalWeight weight, BacktrackInfo<TNodeId,TRoadId> info)
        {
            
        }
        private readonly ILogger logger;
        private IWorldMap<TNodeId,TRoadId> map;
        private readonly string label;
        private int lastIndexSaved;
        private readonly string? debugDirectory;
        private readonly Dictionary<Edge<TNodeId,TRoadId>, HistoryEntry> historyData;
        private bool modified;

        public DebugFinderHistory(ILogger logger, IWorldMap<TNodeId,TRoadId> map,string label, string? debugDirectory)
        {
            this.logger = logger;
            this.map = map;
            this.label = label;
            this.debugDirectory = debugDirectory;
            this.historyData = new Dictionary<Edge<TNodeId,TRoadId>,HistoryEntry>();
            this.lastIndexSaved = -1;
        }

        public void Dispose()
        {
            DumpLastData();
        }
        
        internal void Add(Edge<TNodeId,TRoadId> edge, TotalWeight totalWeight,
            BacktrackInfo<TNodeId,TRoadId> info)
        {
            this.modified = true;
            if (!this.historyData.TryAdd(edge, new HistoryEntry(this.historyData.Count,
                    totalWeight, info)) && !edge.Place.IsUserPoint)
            {
                throw new ArgumentException($"Place {edge} is already added");
            }

            if (this.historyData.Count % 1000 == 0)
                DumpLastData();
        }

        public void DumpLastData()
        {
            if (debugDirectory == null || !this.modified)
                return;

            var input = new TrackWriterInput();
            var last = lastIndexSaved;
            foreach (var (edge,entry) in this.historyData
                         .Where(it => it.Value.index > last)
                         .OrderBy(it => it.Value.index))
            {
                HistoryEntry? source = entry.index == 0 ? null : this.historyData[ entry.info.Source];
                string source_str = source is {} s1 ?  s1.index.ToString():"@";

                var node_id = (edge.Place.IsNode?$"#{this.map.GetOsmNodeId(edge.Place.NodeId)}":"");
                var cost_info = entry.info.IncomingCondition?.CostInfo ?? CostInfo.None;
                var road_mode = entry.info.IncomingCondition?.SpeedStyling.Mode;
                var diff = source is { } s2 ? Weight.Diff(entry.weight.Current, s2.weight.Current) : Weight.Zero;
                var icon = entry.index == 0 
                    ? PointIcon.ParkingIcon 
                    : ( cost_info.Penalized()
                        ? PointIcon.StarIcon : PointIcon.DotIcon);
                input.AddPoint(edge.Place.Point, $"[{entry.index}] {node_id} {entry.weight} from {source_str}",
                    comment:
                    $"{road_mode}; {cost_info}; +{diff.TimeCost}", 
                    icon);

                this.lastIndexSaved = entry.index;
                this.modified = false;
            }

            {
                var visited = new HashSet<int>();
                foreach (var (seed_place, seed_entry) in this.historyData
                             .Where(it => it.Value.index > last)
                             .OrderByDescending(it => it.Value.index))
                {
                    var line = new List<GeoZPoint>();
                    var place = seed_place;
                    var entry = seed_entry;
                    while (true)
                    {
                        if (entry.index <= last)
                            break;
                        line.Add(place.Place.Point);
                        if (!visited.Add(entry.index) || entry.index == 0)
                            break;
                        place =  entry.info.Source;
                        entry = this.historyData[place];
                    }

                    if (line.Count > 1)
                        input.AddLine(line);
                }

            }

            string filename = Navigator.GetUniquePath(debugDirectory, $"trace-{label}-{this.historyData.Count:D10}.kml");
            input.BuildDecoratedKml().Save(filename);
        }
    }

  
}
