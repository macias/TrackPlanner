﻿using System;
using System.Collections.Generic;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder
{
    internal static class Edge
    {
        internal static Edge<TNodeId, TRoadId> STUB<TNodeId, TRoadId>(in Placement<TNodeId, TRoadId> Place)
            where TNodeId : struct
            where TRoadId : struct
        {
            // this method is just for the sake of compilation, I don't have time for covering all the code
            // (i.e. including non-used, now, parts)
            throw new NotImplementedException();
        }

        public static TNodeId SOURCE<TNodeId>()
            where TNodeId : struct
        {
            throw new NotImplementedException();
        }

        public static RoadIndex<TRoadId> INDEX<TNodeId, TRoadId>(TNodeId target)
        {
            throw new NotImplementedException();
        }
    }

    internal readonly record struct Edge<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public Placement<TNodeId, TRoadId> Place { get; }
        // user point does not create source, as well as pointy crosspoint, and node creates itself.
        // The only usual one is the crosspoint segment -- for it source is the opposite node of the move.
        // For example for crosspoint segment A-B, if the place is A, then source is B.

        // we cannot use node, because of OSM errors. There are duplicate (overlapping roads), so we have to use some
        // data which expresses node plus used road
        public RoadIndex<TRoadId>? ProjectedSource { get; }

        public Edge(in Placement<TNodeId, TRoadId> place)
        {
            Place = place;
            ProjectedSource = null;
        }

        public Edge(IWorldMap<TNodeId, TRoadId> map, in Placement<TNodeId, TRoadId> place, RoadIndex<TRoadId>? projectedSource)
        {
            Place = place;
            ProjectedSource = projectedSource;
        }
    }
}