﻿using System.Collections.Generic;

namespace TrackPlanner.PathFinder
{
    internal readonly record struct WeightSteps< TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public IReadOnlyList<StepRun<TNodeId, TRoadId>> Steps { get; }
        public Weight JoinedWeight { get; }
        public bool IsDraft { get; }

        public WeightSteps(IReadOnlyList<StepRun<TNodeId, TRoadId>> steps, in Weight joinedWeight,
            bool isDraft)
        {
            Steps = steps;
            JoinedWeight = joinedWeight;
            IsDraft = isDraft;
        }

        public void Deconstruct(out IReadOnlyList<StepRun<TNodeId, TRoadId>> steps, out Weight weight)
        {
            steps = Steps;
            weight = JoinedWeight;
        }
    }
}