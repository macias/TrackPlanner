﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackRadar.Collections;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{
    public readonly record struct RouteFinderData<TNodeId, TRoadId>(
        List<StageRun<TNodeId, TRoadId>> RouteLegs,
        Weight RouteWeight)
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
    }
    // A* algorithm with pairing heap
    // https://en.wikipedia.org/wiki/A*_search_algorithm
    // https://brilliant.org/wiki/pairing-heap/
    // https://en.wikipedia.org/wiki/Pairing_heap
    // http://www.brouter.de/brouter/algorithm.html

    public sealed class RouteFinder<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        public static Result<RouteFinderData<TNodeId, TRoadId>, RouteFinding>
            TryFindRouteByPoints(ILogger logger, Navigator navigator,
                IWorldMap<TNodeId, TRoadId> map,
                CoverGrid<TNodeId, TRoadId>? coverGrid,
                FinderConfiguration sysConfig,
                UserRouterPreferences userConfig,
                IReadOnlyList<RequestPoint<TNodeId>> userPoints,
                CancellationToken cancellationToken)
        {
            var buckets = map.Grid.GetRoadBuckets(userPoints, sysConfig.InitSnapProximityLimit,
                sysConfig.FinalSnapProximityLimit);
            var finder = new RouteFinder<TNodeId, TRoadId>(logger, navigator, map,
                coverGrid, connectionPredicate: null, weightLimit: null,
                sysConfig, userConfig, buckets, cancellationToken);
            var result = finder.TryFindRoute(buckets);
            return result;
        }


        public static Result<RouteFinderData<TNodeId, TRoadId>, RouteFinding> TryFindRouteByMixed(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            CoverGrid<TNodeId, TRoadId>? coverGrid,
            FinderConfiguration sysConfig,
            UserRouterPreferences userConfig, IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            bool allowSmoothing,
            bool allowGap,
            CancellationToken cancellationToken)
        {
            var buckets = new List<RoadBucket<TNodeId, TRoadId>>();
            for (int i = 0; i < userPoints.Count; ++i)
            {
                if (userPoints[i].IsNode())
                    buckets.Add(RoadBucket.CreateBucket(i,
                        userPoints[i].NodeId!.Value,
                        map, predicate: null,
                        map.Grid.Calc,
                        allowSmoothing: allowSmoothing,
                        allowGap:allowGap));
                else
                    buckets.Add(map.Grid.CreateBucket(i, userPoints[i].UserPoint3d, sysConfig.InitSnapProximityLimit,
                        sysConfig.FinalSnapProximityLimit,
                        forceClosest: false,
                        allowSmoothing: allowSmoothing,
                        allowGap:allowGap));
            }

            var finder = new RouteFinder<TNodeId, TRoadId>(logger, navigator, map,
                coverGrid, connectionPredicate: null,
                weightLimit: null,
                sysConfig,
                userConfig, buckets, cancellationToken);
            var result = finder.TryFindRoute(buckets);
            return result;
        }

        public static Result<RouteFinderData<TNodeId, TRoadId>, RouteFinding> TryFindRouteByNodes(ILogger logger, Navigator navigator,
            IWorldMap<TNodeId, TRoadId> map,
            ICover<TNodeId, TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            Weight? weightLimit,
            FinderConfiguration sysConfig,
            UserRouterPreferences userConfig, IReadOnlyList<TNodeId> mapNodes,
            bool allowSmoothing,
            bool allowGap,
            CancellationToken cancellationToken)
        {
            var buckets = RoadBucket.GetRoadBuckets(mapNodes, map, map.Grid.Calc, 
                allowSmoothing: allowSmoothing,
                allowGap:allowGap);

            var finder = new RouteFinder<TNodeId, TRoadId>(logger, navigator, map,
                coverGrid, connectionPredicate, weightLimit, sysConfig, userConfig,
                buckets, cancellationToken);
            var result = finder.TryFindRoute(buckets);
            return result;
        }


        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly ICover<TNodeId, TRoadId>? coverGrid;
        private readonly ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate;
        private readonly Weight? weightLimit;
        private IGeoCalculator calc => this.map.Grid.Calc;
        private readonly FinderConfiguration finderConfig;
        private readonly UserRouterPreferences userConfig;
        private readonly IReadOnlySet<TNodeId> suppressedHighTraffic;
        private readonly CancellationToken cancellationToken;
        private ContextMessage? problemMessage;

        private string? debugDirectory => this.navigator.GetDebugDirectory(this.finderConfig.EnableDebugDumping);

        private readonly HashSet<TNodeId> DEBUG_lowCostNodes;
        private readonly Dictionary<TNodeId, string> DEBUG_suppressTooFar;
        private readonly Dictionary<TNodeId, string> DEBUG_suppressInRange;
        private readonly HashSet<TNodeId> DEBUG_dangerousNodes;
        private readonly Dictionary<TNodeId, string> DEBUG_hotNodes;

        private readonly RouteLogic<TNodeId, TRoadId> logic;

        private readonly StageFinder<TNodeId, TRoadId> finder;
        private readonly RouteShaper<TNodeId, TRoadId> shaper;
        private readonly DraftRouter<TNodeId, TRoadId> drafter;

        internal RouteFinder(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            ICover<TNodeId, TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            Weight? weightLimit,
            FinderConfiguration finderConfig,
            UserRouterPreferences userConfig,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> buckets,
            CancellationToken cancellationToken)
        {
            this.logger = logger;
            this.shaper = new RouteShaper<TNodeId, TRoadId>(logger, map);
            this.navigator = navigator;
            this.map = map;
            this.coverGrid = coverGrid;
            this.connectionPredicate = connectionPredicate;
            this.weightLimit = weightLimit;
            this.finderConfig = finderConfig;
            this.userConfig = userConfig;
            this.cancellationToken = cancellationToken;

            this.DEBUG_lowCostNodes = new HashSet<TNodeId>();
            this.DEBUG_suppressTooFar = new Dictionary<TNodeId, string>();
            this.DEBUG_suppressInRange = new Dictionary<TNodeId, string>();
            this.DEBUG_dangerousNodes = new HashSet<TNodeId>();
            this.DEBUG_hotNodes = new Dictionary<TNodeId, string>()
            {
            };

            this.suppressedHighTraffic = suppressTrafficCost(buckets, userConfig.TrafficSuppression);

            this.logic = new RouteLogic<TNodeId, TRoadId>(map, calc, userConfig, this.suppressedHighTraffic);
            this.finder = new StageFinder<TNodeId, TRoadId>(logger, navigator, map,
                finderConfig,
                userConfig,
                suppressedHighTraffic);
            this.drafter = new DraftRouter<TNodeId, TRoadId>(logger, calc,
                new DraftOptions(), compactPreservesRoads: false);
        }


        internal Result<RouteFinderData<TNodeId, TRoadId>, RouteFinding> TryFindRoute(List<RoadBucket<TNodeId, TRoadId>> buckets)
        {
            if (buckets.Count < 2)
            {
                logger.Error($"Insufficient number of anchors were given: {buckets.Count}");
                return Result<RouteFinderData<TNodeId, TRoadId>, RouteFinding>.Fail(RouteFinding.InsufficientData);
            }

#if DEBUG
            //   logger.Info($"bucket[0] {buckets[0].OsmString()}");
#endif


            var route_legs = new List<StageRun<TNodeId, TRoadId>>();
            var route_weight = Weight.Zero;

            for (int pt_index = 1; pt_index < buckets.Count; ++pt_index)
            {
                WeightSteps<TNodeId, TRoadId> best_path;

                RoadBucket<TNodeId, TRoadId> start_bucket = buckets[pt_index - 1];
                RoadBucket<TNodeId, TRoadId> end_bucket = buckets[pt_index];
                if (DEBUG_SWITCH.Enabled && pt_index == 2)
                {
                    ;
                }

                {
                    var cost_path_result = this.finder.TryFindStageRoute(
                        RouteMode.General,
                        this.coverGrid,
                        connectionPredicate,
                        this.weightLimit,
                        start_bucket, end_bucket,
                        this.cancellationToken,
                        out CompStatistics cost_stats);

                    overrideProblem(cost_path_result.ProblemMessage);

                    if (cost_path_result.HasValue)
                    {
                        var cost_path = cost_path_result.Value;

                        if (this.userConfig.SnapCyclewaysToRoads != Length.Zero)
                        {
                            try
                            {
                                tryStraightenCycleways(start_bucket, end_bucket, ref cost_path);
                            }
                            catch (Exception ex)
                            {
                                ex.LogDetails(this.logger, MessageLevel.Error);
                                overrideProblem(new ContextMessage("Unable to smooth out cycleway"));
                            }
                        }

                        best_path = cost_path;
                        // 652_998
                        // this.logger.Info($"Proper path cost {best_path.Value.Weight.TravelCost.EquivalentInMinutes} with {cost_stats.ForwardUpdateCount + cost_stats.BackwardUpdateCount} updates, sub-success {cost_stats.SuccessExactTarget}, sub-fails {cost_stats.FailedExactTarget}");
                    }
                    else 
                    {
                        if (DevelModes.True)
                        {
                            var steps = drafter.FindStageRoute(start_bucket.UserPoint, end_bucket.UserPoint,
                                this.userConfig, splitLeg: false);
                            best_path = new WeightSteps<TNodeId, TRoadId>(steps, Weight.Zero, isDraft: true);
                            overrideProblem(new ContextMessage($"Unable to find route between {start_bucket.UserPoint} and {end_bucket.UserPoint}",
                                MessageLevel.Error));
                        }
                        else
                        {
                            return cost_path_result.FailAs<RouteFinderData<TNodeId, TRoadId>>();
                        }
                    }
                }


                var route_leg = best_path.Steps.ToList();

                route_legs.Add(new StageRun<TNodeId, TRoadId>(route_leg,
                    isDraft:best_path.IsDraft));
                route_weight = (route_weight + best_path.JoinedWeight);

                if (DEBUG_SWITCH.Enabled)
                {
                    ;
                }

                if (best_path.IsDraft)
                {
                    ;
                }
                // todo: INCORRECT, it won't give optimal path, fix it 
                // to avoid gaps at the anchors, like this
                // X----xo x----X
                // where X -- user point and cross point at the same time, x -- crosspoint, o -- user point
                // this hack was added, we simply use single snap which was used when finding current path leg
                else if (route_leg.Count > 1)
                {
                    // we cannot accept rebuilding bucket based on segment with single step because then
                    // we don't know where from this step comes -- from start or end bucket. And for sure we
                    // cannot rebuild end bucket based on start input
                    
                    if (!end_bucket.AllowGap)
                    {
                        Placement<TNodeId,TRoadId> last_place = route_leg[^1].Place;
                        if (end_bucket.TryRebuildWithSnap(this.logger, last_place.Point.Convert2d(), 
                                last_place.BaseRoadIndex,last_place.NextRoadIndex,
                                out var bucket) is { } problem)
                            overrideProblem(problem);
                        else
                            buckets[pt_index] = bucket!;
                    }
                }
                else 
                {
                    // if we "found" the route but it is empty it means we have
                    // duplicate points in request, so now we simply copy bucket
                    // as starting point

                    buckets[pt_index] = buckets[pt_index - 1];
                }
            }

            RouteShaper<TNodeId,TRoadId>.ClearSingularLegs(route_legs);

            Result<ValueTuple> shape_res = this.shaper.ShapeRouteLegs(buckets, route_legs);
            overrideProblem(shape_res.ProblemMessage);
            if (!shape_res.HasValue)
                return shape_res.FailAs<RouteFinderData<TNodeId, TRoadId>, RouteFinding>(RouteFinding.RouteNotFound);

            dumpPostDebug();

            return Result<RouteFinderData<TNodeId, TRoadId>, RouteFinding>.Valid(
                new RouteFinderData<TNodeId, TRoadId>(route_legs, route_weight), this.problemMessage);
        }

        private void overrideProblem(ContextMessage? otherProblem)
        {
            this.problemMessage = this.problemMessage.Override(otherProblem);
        }

        private bool tryStraightenCycleways(RoadBucket<TNodeId, TRoadId> startBucket,
            RoadBucket<TNodeId, TRoadId> endBucket,
            ref WeightSteps<TNodeId, TRoadId> legSteps)
        {
            // [polish][smooth][straighten]
            
            // cycleway too often are too complex, instead running in straight line they are jumping
            // left and right around main road. Thus here we try to snap cycleway fragments into
            // main road

            List<StepRun<TNodeId, TRoadId>>? result_steps = null;

            int start_cyc = legSteps.Steps.Count;
            while (true)
            {
                var end_cyc = legSteps.Steps.LastIndexOf(start_cyc - 1,
                    it => this.map.GetRoadInfo(it.IncomingRoadId).Kind.IsWinding());
                if (end_cyc == -1)
                {
                    if (result_steps != null)
                        legSteps = new WeightSteps<TNodeId, TRoadId>(result_steps,
                            reweightSteps(startBucket, endBucket, result_steps), legSteps.IsDraft);

                    return true;
                }

                start_cyc = legSteps.Steps.LastIndexOf(end_cyc, it => !this.map.GetRoadInfo(it.IncomingRoadId).Kind.IsWinding());

                start_cyc = Math.Max(0, start_cyc);

                // we don't compute over excessive region, so we pick nodes close to the cycleway
                var focus_nodes = new HashSet<TNodeId>();

                foreach (var (prev, next) in legSteps.Steps.Skip(start_cyc).Take(end_cyc - start_cyc).Slide())
                {
                    var nodes_extract = this.map.GetNodesAlongLine(calc, this.userConfig.SnapCyclewaysToRoads,
                        prev.Place.Point.Convert2d(), next.Place.Point.Convert2d());

                    focus_nodes.AddRange(nodes_extract);
                }

                RoadBucket<TNodeId, TRoadId> sub_start;
                if (start_cyc == 0)
                    sub_start = startBucket;
                else
                    sub_start = RoadBucket.CreateBucket(0, legSteps.Steps[start_cyc].Place.NodeId,
                        this.map, predicate: null,
                        calc, allowSmoothing: false,
                        allowGap:false);

                RoadBucket<TNodeId, TRoadId> sub_end;
                if (end_cyc == legSteps.Steps.Count - 1)
                    sub_end = endBucket;
                else
                    sub_end = RoadBucket.CreateBucket(0, legSteps.Steps[end_cyc].Place.NodeId,
                        this.map, predicate: null,
                        calc,
                        allowSmoothing: false,
                        allowGap:false);

                var cost_path_result = this.finder.TryFindStageRoute(
                    RouteMode.CycleSnap,
                    coverGrid: null,
                    (node_id, _, road_id) =>
                    {
                        if (road_id is not { } r_id)
                            throw new ArgumentNullException();

                        RoadInfo<TNodeId> road_info = this.map.GetRoadInfo(r_id);
                        return focus_nodes.Contains(node_id)
                               // this time we have to exclude forbidden roads completely, since
                               // we have more or less correct way, we cannot change it to completely
                               // invalid one (e.g. instead of cycleway we go with highway)
                               && !road_info.IsCyclingForbidden()
                               && !road_info.Kind.IsWinding();
                    },
                    weightLimit: null,
                    sub_start,
                    sub_end,
                    this.cancellationToken,
                    out _);

                if (!cost_path_result.HasValue || cost_path_result.Value.Steps.Count == 0)
                    continue;

                // propage problem ONLY if the polishing was possible, otherwise we would
                // report back that route was not found in many cases
                    overrideProblem(cost_path_result.ProblemMessage);

                var sub_replacement_steps = cost_path_result.Value.Steps.ToList();
                if (sub_start != startBucket)
                {
                    // so we remove crosspoint from replacement
                    if (sub_replacement_steps[0].Place.IsCrossPoint)
                        sub_replacement_steps.RemoveFirst();
                }

                if (sub_end != endBucket)
                {
                    if (sub_replacement_steps[^1].Place.IsCrossPoint)
                        sub_replacement_steps.RemoveLast();
                }

                if (sub_replacement_steps.Where(it => it.Place.IsNode).Slide()
                    .Any(it => EqualityComparer<TNodeId>.Default.Equals(it.prev.Place.NodeId, it.next.Place.NodeId)))
                {
                    overrideProblem(new ContextMessage("Duplicated nodes in sub path"));
                    continue;
                }

                if (result_steps == null)
                    result_steps = legSteps.Steps.ToList();

                // removing cycleway part including start and end of it
                result_steps.RemoveRange(start_cyc, end_cyc - start_cyc + 1);
                // if we made a turn specially for the cycleway and in this version we are turning back
                // then remove the common fragment, like in:
                //   =====================+---------------+===========
                //                        ||              ||
                //                        ||              ||
                //                        *~~~~~~~~~~~~~~~*
                // = and || road we ride, ~ cycleway we ride
                // at * program will remove the cycleway and return to main road, meaning it will ride
                // back and forth on || vertical segment -- this we are removing here

                // note we already deleted fragment, so we are point after its end

                // hiting the shared entry
                if (start_cyc < result_steps.Count
                    && sub_replacement_steps.Count > 1
                    && result_steps[start_cyc].Place == sub_replacement_steps[^1].Place)
                {
                    result_steps.RemoveAt(start_cyc);
                }

                // going through shared entry
                while (start_cyc < result_steps.Count
                       && sub_replacement_steps.Count > 1
                       && result_steps[start_cyc].Place == sub_replacement_steps[^2].Place)
                {
                    sub_replacement_steps.RemoveLast();
                    result_steps.RemoveAt(start_cyc);
                }


                if (start_cyc > 0
                    && sub_replacement_steps.Count > 1
                    && result_steps[start_cyc - 1].Place == sub_replacement_steps[0].Place)
                {
                    sub_replacement_steps.RemoveFirst();
                    --start_cyc;
                }

                while (start_cyc > 0
                       && sub_replacement_steps.Count > 1
                       && result_steps[start_cyc - 1].Place == sub_replacement_steps[1].Place)
                {
                    sub_replacement_steps.RemoveFirst();
                    result_steps.RemoveAt(start_cyc - 1);
                    --start_cyc;
                }


                if (start_cyc > 0)
                {
                    RoadIndex<TRoadId>? proj_source = start_cyc > 1 
                        ?result_steps[start_cyc - 2].Place.NodeIndexOrNull:null;
                    Placement<TNodeId, TRoadId> prev = result_steps[start_cyc - 1].Place;
                    Placement<TNodeId, TRoadId> next = sub_replacement_steps[0].Place;
                    if (tryCreateStep(startBucket, endBucket,proj_source, prev, next, out var conn_step))
                        sub_replacement_steps[0] = conn_step;
                    else
                    {
                        overrideProblem(new ContextMessage($"Cannot create starting connection {prev.OsmString(map)} to {next.OsmString(map)}"));
                        return false;
                    }
                }

                if (start_cyc < result_steps.Count)
                {
                    RoadIndex<TRoadId>? proj_source = sub_replacement_steps.Count > 1 
                        ?sub_replacement_steps[^2].Place.NodeIndexOrNull:null;
                    Placement<TNodeId, TRoadId> prev = sub_replacement_steps[^1].Place;
                    Placement<TNodeId, TRoadId> next = result_steps[start_cyc].Place;
                    if (tryCreateStep(startBucket, endBucket,proj_source, prev, next, out var conn_step))
                        result_steps[start_cyc] = conn_step;
                    else
                    {
                        overrideProblem(new ContextMessage($"Cannot create ending connection {prev.OsmString(map)} to {next.OsmString(map)}"));
                        return false;
                    }
                }

                result_steps.InsertRange(start_cyc, sub_replacement_steps);

                if (result_steps.Where(it => it.Place.IsNode).Slide()
                    .Any(it => EqualityComparer<TNodeId>.Default.Equals(it.prev.Place.NodeId, it.next.Place.NodeId)))
                {
                    overrideProblem(new ContextMessage("Duplicated nodes after inserting sub path"));
                    continue;
                }
            }
        }

        private Weight reweightSteps(RoadBucket<TNodeId, TRoadId> startBucket,
            RoadBucket<TNodeId, TRoadId> endBucket, IReadOnlyList<StepRun<TNodeId, TRoadId>> steps)
        {
            Weight weight = Weight.Zero;

            TRoadId incoming_road_id = steps[0].IncomingRoadId;
            RoadIndex<TRoadId>? source_index = null;

            foreach (var (prev, next) in steps.Slide())
            {
                var step_info = this.logic.GetFullStepInfo(next.IncomingRoadId, source_index, prev.Place, 
                    next.Place, reversed: false);

                weight = (weight + step_info.Weight);

                incoming_road_id = next.IncomingRoadId;

                source_index = prev.Place.NodeIndexOrNull;
            }

            return weight;
        }

        private bool tryCreateStep(RoadBucket<TNodeId, TRoadId> startBucket,
            RoadBucket<TNodeId, TRoadId> endBucket,
            RoadIndex<TRoadId>? projectedSource,
            Placement<TNodeId, TRoadId> prev,
            Placement<TNodeId, TRoadId> next,
            out StepRun<TNodeId, TRoadId> step)
        {
            IEnumerable<(Placement<TNodeId, TRoadId> place, TRoadId roadId)> adjacents
                = FinderHelper.GetAllAdjacent(this.map, calc, prev, startBucket, endBucket,
                    connectionPredicate: null, this.finderConfig.PreserveRoundabouts)
                    .Select(it => (it.place.Place,it.roadId))
                    .Distinct();
            // we need to differentiate between nodes and crosspoints here
            adjacents = adjacents.Where(it => it.place.MatchesCoordinates(next)
                                              && it.place.IsCrossSegment == next.IsCrossSegment
                                              && it.place.IsCrossPoint == next.IsCrossPoint);
#if DEBUG
            if (next.DEBUG_Identifier == 250)
            {
                ;
            }
#endif
            var adj = adjacents.SingleOrNone();

            if (!adj.HasValue)
            {
                step = default;
                return false;
            }

            var road_id = adj.Value.roadId;
            var info = this.logic.GetFullStepInfo(connectingRoadId: road_id,projectedSource,
                prev, next, reversed: false);
            step = new StepRun<TNodeId, TRoadId>(next, road_id, info.GetCondition(), info.SegmentLength, info.Time);
            return true;
        }


        private void dumpPostDebug()
        {
            if (this.finderConfig.EnableDebugDumping && this.finderConfig.DumpLowCost)
            {
                var input = new TrackWriterInput();
                foreach (var node in DEBUG_lowCostNodes)
                    input.AddPoint(map.GetPoint(node), icon: PointIcon.DotIcon);

                string filename = Navigator.GetUniquePath(this.navigator.GetDebugDirectory(), $"low-cost.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (this.finderConfig.EnableDebugDumping && this.finderConfig.DumpTooFar)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_suppressTooFar)
                {
                    if (!this.DEBUG_suppressInRange.ContainsKey(entry.Key))
                        input.AddPoint(map.GetPoint(entry.Key), label: entry.Value, icon: PointIcon.CircleIcon);
                }

                string filename = Navigator.GetUniquePath(this.debugDirectory!, $"too-far.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (this.finderConfig.EnableDebugDumping && this.finderConfig.DumpInRange)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_suppressInRange)
                    input.AddPoint(map.GetPoint(entry.Key), label: entry.Value, icon: PointIcon.DotIcon);

                string filename = Navigator.GetUniquePath(this.debugDirectory!, $"in-range.kml");
                input.BuildDecoratedKml().Save(filename);
            }

            if (this.finderConfig.EnableDebugDumping && this.finderConfig.DumpDangerous)
            {
                var input = new TrackWriterInput();
                foreach (var entry in this.DEBUG_dangerousNodes)
                    input.AddPoint(map.GetPoint(entry), label: "noname", icon: PointIcon.DotIcon);

                string filename = Navigator.GetUniquePath(this.debugDirectory!, $"dangerous.kml");
                input.BuildDecoratedKml().Save(filename);
            }
        }


        private IReadOnlySet<TNodeId> suppressTrafficCost(IEnumerable<RoadBucket<TNodeId, TRoadId>> buckets, Length suppressionRange)
        {
            var suppressed = new HashSet<TNodeId>();

            if (suppressionRange != Length.Zero)
            {
                // assume that for all mid-points user selected, she/he is aware that she/he places point on a high-traffic road
                // in such case assume that for given distance user is OK to ride on such rode, so do not add any "penalties"
                // while searching for a route
                foreach (var bucket in buckets.Skip(1).SkipLast(1))
                {
                    foreach (var snap in bucket.AllSnaps)
                    {
                        var road = map.GetRoadInfo(snap.BaseRoadIndex.RoadId);
                        if (road.IsExpressTraffic || road.IsSignificantTraffic)
                        {
                            suppressed.AddRange(suppressTrafficCost(map.GetNode(snap.BaseRoadIndex), suppressionRange));
                        }
                    }
                }
            }

            return suppressed;
        }

        private IEnumerable<TNodeId> suppressTrafficCost(TNodeId startNodeId, Length suppressionRange)
        {
            var suppressed = new HashSet<TNodeId>();
            // node id -> distance -> incoming road
            var heap = MappedPairingHeap.Create<TNodeId, Length, TRoadId>();

            heap.TryAddOrUpdate(startNodeId, Length.Zero, default);

            while (heap.TryPop(out TNodeId curr_node_id, out Length curr_dist, out TRoadId incoming_road_id))
            {
                // please note this collection is not "polluted" with data coming from other mid-points selected by user
                suppressed.Add(curr_node_id);
                this.DEBUG_suppressInRange.TryAdd(curr_node_id, $"#{incoming_road_id} {curr_dist}");

                GeoZPoint current_point = map.GetPoint(curr_node_id);

                foreach (var adj_idx in map.GetAdjacentRoads(curr_node_id))
                {
                    var adj_node_id = map.GetNode(adj_idx);

                    if (suppressed.Contains(adj_node_id))
                        continue;

                    var adj_road_info = this.map.GetRoadInfo(adj_idx.RoadId);
                    if (adj_road_info.IsExpressTraffic || adj_road_info.IsSignificantTraffic)
                    {
                        Length adj_total_dist = curr_dist + calc.GetDistance(current_point, map.GetPoint(adj_node_id));
                        if (adj_total_dist <= suppressionRange)
                        {
                            heap.TryAddOrUpdate(adj_node_id, adj_total_dist, adj_idx.RoadId);
                        }
                        else
                            this.DEBUG_suppressTooFar.TryAdd(adj_node_id, $"#{adj_idx.RoadId} {adj_total_dist}");
                    }
                }
            }

            return suppressed;
        }
    }
}