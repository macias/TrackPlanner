﻿using System;
using System.Collections.Generic;

namespace TrackPlanner.PathFinder
{
    public sealed class TotalWeightComparer : IComparer<TotalWeight>
    {
        public static TotalWeightComparer CurrentInstance { get; } = new TotalWeightComparer(WeightComparer.Instance, Mode.Current);
        public static TotalWeightComparer BothInstance { get; } = new TotalWeightComparer(WeightComparer.Instance, Mode.Both);
        public static TotalWeightComparer CostlessAndRemainingInstance { get; } = new TotalWeightComparer(WeightComparer.Instance, Mode.CostlessAndRemaining);
        
        public enum Mode
        {
            Current = 1,
            CostlessAndRemaining = 2,
            Both = 4
        }

        public WeightComparer Comparer { get; }
        private readonly Mode mode;

        private TotalWeightComparer(WeightComparer comparer, Mode mode)
        {
            this.mode = mode;
            this.Comparer = comparer;
        }

        public int Compare(TotalWeight x, TotalWeight y)
        {
            return Comparer.Compare(Combine(x), Combine(y));
        }
        public Weight Combine(TotalWeight x)
        {
            switch (mode)
            {
                case Mode.Current: return x.Current;
                case Mode.CostlessAndRemaining: return x.CostlessAndRemaining();
                case Mode.Both: return x.Combine();
                default: throw new NotSupportedException($"{nameof(mode)}={mode}");
            }
        }
    }

    public sealed class WeightComparer : IComparer<Weight>
    {
        public static WeightComparer Instance { get; } = new WeightComparer();
        
        private WeightComparer()
        {
        }

        public int Compare(Weight x, Weight y)
        {
            int comp;
            comp = x.ForbiddenDistance.CompareTo(y.ForbiddenDistance);
            if (comp != 0)
                return comp;

            comp = x.TimeCost.CompareTo(y.TimeCost);
            if (comp != 0)
                return comp;

            return 0;
        }
    }
}