﻿using MathUnit;
using System;
using System.Collections.Generic;
using TrackRadar.Collections;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    internal readonly record struct SearchData<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly Placement<TNodeId,TRoadId> initPoint;
        public Backtrack<TNodeId, TRoadId> Backtrack { get; }
        public MappedPairingHeap<Edge<TNodeId, TRoadId>,
            TotalWeight, BacktrackInfo<TNodeId, TRoadId>> Heap { get; }

        public bool IsEmpty => Heap.Count == 0;

        // we cannot add entire heap size, because such metric would be useless when comparing
        // forward and backward progress (for example somewhere in the middle when hitting densely
        // populated area we could get 200 entries against 50, which would trigger lagging
        // alert)
        public int ProgressSize => Backtrack.Count + (Heap.Count>0?1:0);

        public SearchData(IWorldMap<TNodeId, TRoadId> map,IComparer<TotalWeight> weightComparer,
            RoadBucket<TNodeId, TRoadId> initBucket)
        {
            this.Backtrack = new Backtrack<TNodeId, TRoadId>(map);

            // node id -> ESTIMATE length (i.e. current length + direct distance), info
            this.Heap = MappedPairingHeap.Create<Edge<TNodeId, TRoadId>, TotalWeight,
                BacktrackInfo<TNodeId, TRoadId>>(keyComparer: null, weightComparer: weightComparer);

            this.initPoint = Placement<TNodeId, TRoadId>.UserPoint(initBucket);
            Charge();
        }

        public void Charge()
        {
            Heap.TryAddOrUpdate(new Edge<TNodeId, TRoadId>(  this.initPoint),
                TotalWeight.Zero,
                new BacktrackInfo<TNodeId, TRoadId>(
                    source: new Edge<TNodeId, TRoadId>( Placement<TNodeId, TRoadId>.Prestart(new GeoZPoint())),
                    null,
                    null,
                    Length.Zero,
                    TimeSpan.Zero,
                    isShortcut: false));
        }
    }
}