﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder
{
    public static class FinderHelper
    {
        public static bool IsCoveredWith<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, TNodeId node,
            in Placement<TNodeId, TRoadId> adjacentPlace)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (adjacentPlace.IsNode)
            {
                return EqualityComparer<TNodeId>.Default.Equals(node, adjacentPlace.NodeId);
            }
            else if (adjacentPlace.IsCrossSegment)
            {
                return adjacentPlace.IsCrosspointAdjacentToNode(map, node);
            }
            else
                throw new NotSupportedException($"Target place is neither node nor cross segment {adjacentPlace}");
        }

        public static void IsSmoothingJointAllowed<TRequestPoint>(int prevLegIndex, int nextLegIndex,
            IReadOnlyList<TRequestPoint> requests, out bool smoothingAllowed, out bool gapAllowed)
            where TRequestPoint : IRequestPoint
        {
            // note, there could indices in between, because of the empty legs

            // our arguments are LEG indices, but we use them against POINTS, thus we have to add
            // +1 to the start, and keep the end index
            //     0       1
            // *-------*-------*
            // 0       1       2

            smoothingAllowed = true;
            gapAllowed = false;
            for (int i = prevLegIndex + 1; i <= nextLegIndex; ++i)
            {
                if (!requests[i].AllowSmoothing)
                {
                    smoothingAllowed = false;
                }

                if (requests[i].AllowGap)
                {
                    gapAllowed = true;
                }
            }
        }


#if DEBUG
        private sealed class OsmIdComparer : IComparer<OsmId>
        {
            public static IComparer<OsmId> Instance { get; } = new OsmIdComparer();

            public int Compare(OsmId x, OsmId y)
            {
                var comp = x.Identifier.CompareTo(y.Identifier);
                if (comp != 0)
                    return comp;
                return x.Phantom.CompareTo(y.Phantom);
            }
        }
#endif
        internal static IEnumerable<(Edge<TNodeId, TRoadId> place, TRoadId roadId)>
            GetAllAdjacent<TNodeId, TRoadId>(
                this IWorldMap<TNodeId, TRoadId> map,
                IGeoCalculator calc,
                Placement<TNodeId, TRoadId> current,
                RoadBucket<TNodeId, TRoadId> startBucket,
                IEnumerable<RoadBucket<TNodeId, TRoadId>> endBuckets,
                ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
                bool preserveRoundabouts)
            where TNodeId : struct
            where TRoadId : struct
        {
            // point -> incoming road id
            var adjacent = new Dictionary<Edge<TNodeId, TRoadId>, TRoadId>();
            if (current.IsNode)
            {
                AddAdjacentCrosspointsToNode(map, adjacent, current, startBucket);
                foreach (var bucket in endBuckets)
                    AddAdjacentCrosspointsToNode(map, adjacent, current, bucket);

                foreach (var (place, road_id) in GetDirectlyAdjacent(map, calc, current,
                             connectionPredicate, preserveRoundabouts))
                    adjacent.TryAdd(new Edge<TNodeId, TRoadId>(map,place,current.BaseRoadIndex),
                        road_id);
            }
            else if (current.IsCrossPoint)
            {
                AddAdjacentToCrossPoint(map, calc, adjacent, current, startBucket, isStarting: true);
                foreach (var bucket in endBuckets)
                    AddAdjacentToCrossPoint(map, calc, adjacent, current, bucket, isStarting: false);
            }
            else if (current.IsUserPoint)
            {
                AddAdjacentToUserPoint(map, adjacent, current, startBucket);
                foreach (var bucket in endBuckets)
                    AddAdjacentToUserPoint(map, adjacent, current, bucket);
            }

            return adjacent.Select(it => (it.Key, it.Value));
        }

        internal static void AddAdjacentCrosspointsToRoundabout<TNodeId, TRoadId>(
            IWorldMap<TNodeId, TRoadId> map,
            Dictionary<Placement<TNodeId, TRoadId>, TRoadId> adjacent,
            Placement<TNodeId, TRoadId> place,
            RoadBucket<TNodeId, TRoadId> bucket)
            where TNodeId : struct
            where TRoadId : struct
        {
            foreach (var snap in bucket.ActiveSnaps)
            {
                if (map.IsRedundant(snap.BaseRoadIndex.RoadId)
                    // if snap lies right at roundabout it means it and center are adjacent
                    || EqualityComparer<TRoadId>.Default.Equals(snap.BaseRoadIndex.RoadId,
                        place.BaseRoadIndex.RoadId))
                    continue;

                Placement<TNodeId, TRoadId> crosspoint = Placement<TNodeId, TRoadId>.Crosspoint(map,
                    snap.TrackCrosspoint,
                    snap.BaseRoadIndex, snap.SnapKind);

                adjacent.TryAdd(crosspoint, snap.BaseRoadIndex.RoadId);
#if DEBUG
                if (crosspoint.DEBUG_Identifier == 4 || place.DEBUG_Identifier == 15)
                {
                    ;
                }
#endif
            }
        }

        internal static void AddAdjacentCrosspointsToNode<TNodeId, TRoadId>(
            IWorldMap<TNodeId, TRoadId> map,
            Dictionary<Edge<TNodeId, TRoadId>, TRoadId> adjacent,
            Placement<TNodeId, TRoadId> current,
            RoadBucket<TNodeId, TRoadId> bucket)
            where TNodeId : struct
            where TRoadId : struct
        {
            foreach (var snap in bucket.ActiveSnaps)
            {
                if (!map.IsRedundant(snap.BaseRoadIndex.RoadId)
                    && snap.IsAdjacentToNode(map, current.NodeId))
                {
                    Placement<TNodeId, TRoadId> crosspoint = Placement<TNodeId, TRoadId>.Crosspoint(map,
                        snap.TrackCrosspoint,
                        snap.BaseRoadIndex, snap.SnapKind);

                    adjacent.TryAdd(new Edge<TNodeId, TRoadId>(map, crosspoint,current.BaseRoadIndex), snap.BaseRoadIndex.RoadId);
#if DEBUG
                    if (crosspoint.DEBUG_Identifier == 4 || current.DEBUG_Identifier == 15)
                    {
                        ;
                    }
#endif
                }
            }
        }


        internal static IEnumerable<(Edge<TNodeId, TRoadId> place, TRoadId roadId)>
            GetAllAdjacent<TNodeId, TRoadId>(
                this IWorldMap<TNodeId, TRoadId> map,
                IGeoCalculator calc,
                Placement<TNodeId, TRoadId> current,
                RoadBucket<TNodeId, TRoadId> startBucket, RoadBucket<TNodeId, TRoadId> endBucket,
                ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
                bool preserveRoundabouts)
            where TNodeId : struct
            where TRoadId : struct
        {
            // point -> incoming road id
            var adjacent = new Dictionary<Edge<TNodeId, TRoadId>, TRoadId>();
            if (current.IsNode)
            {
                AddAdjacentCrosspointsToNode(map, adjacent, current, endBucket);

                foreach (var (place, road_id) in GetDirectlyAdjacent(map, calc, current,
                             connectionPredicate, preserveRoundabouts))
                    {
                        adjacent.TryAdd(new Edge<TNodeId, TRoadId>(map,place, current.BaseRoadIndex), road_id);
                    }
            }
            else if (current.IsCrossPoint)
            {
                AddAdjacentToCrossPoint(map, calc, adjacent, current, startBucket,
                    isStarting: true);
                AddAdjacentToCrossPoint(map, calc, adjacent, current, endBucket,
                    isStarting: false);
            }
            else if (current.IsUserPoint)
            {
                AddAdjacentToUserPoint(map, adjacent, current, startBucket);
            }

            return adjacent.Select(it => (it.Key, it.Value));
        }

        internal static IEnumerable<KeyValuePair<Placement<TNodeId, TRoadId>, TRoadId>>
            GetDirectlyAdjacent<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
                IGeoCalculator calc,
                Placement<TNodeId, TRoadId> current,
                ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
                bool preserveRoundabouts)
            where TNodeId : struct
            where TRoadId : struct
        {
            foreach (var adj_road_idx in map.GetAdjacentRoads(current.NodeId))
            {
                if (map.IsRedundant(adj_road_idx.RoadId))
                    continue;

                var adj_node_id = map.GetNode(adj_road_idx);

                if (connectionPredicate != null &&
                    !connectionPredicate(current.NodeId, adj_node_id, adj_road_idx.RoadId))
                    continue;

                if (!preserveRoundabouts && map.GetRoadInfo(adj_road_idx.RoadId).IsRoundabout)
                {
                    continue;
                }

                /*if (!has_adjacent_crosspoints && !is_snapped)
                    {
                        // todo: jak tylko bedziemy mieli pamiec, po prostu zwracac tu ireadonlylist i sprawdz count,
                        // chodzi o to, ze musimy miec wylacznie jedna droge
                        if (!this.map.GetRoads(this.map.GetNode(adj_road_idx)).Skip(1).Any())
                        {
                            var current_road_indices = this.map.GetRoads(current.NodeId.Value).Where(it => it.RoadMapIndex == adj_road_idx.RoadMapIndex).ToArray();
                            if (current_road_indices.Length == 1)
                            {
                                
                            }
                        }
                    }*/
                Placement<TNodeId, TRoadId> adj_place = Placement<TNodeId, TRoadId>.Node(map, adj_road_idx);

                yield return KeyValuePair.Create(adj_place, adj_road_idx.RoadId);
            }
        }

        internal static void AddAdjacentToUserPoint<TNodeId, TRoadId>(
            IWorldMap<TNodeId, TRoadId> map,
            Dictionary<Edge<TNodeId, TRoadId>, TRoadId> adjacent,
            Placement<TNodeId, TRoadId> place, RoadBucket<TNodeId, TRoadId> bucket)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (place.Point.Convert2d() != bucket.UserPoint)
                return;

            foreach (var snap in bucket.ActiveSnaps
                         // during the debug it is critical to have the same results when using
                         // true/big map and mini map used for tests. Thus we have to sort
                         // the adjacent crosspoints to get the same execution
#if DEBUG
                         .OrderBy(it => map.GetOsmRoadId(it.BaseRoadIndex.RoadId),
                             OsmIdComparer.Instance)
                         .ThenBy(it => it.BaseRoadIndex.IndexAlongRoad)
#endif
                    )
            {
                if (map.IsRedundant(snap.BaseRoadIndex.RoadId))
                    continue;

                Placement<TNodeId,TRoadId> crosspoint = Placement<TNodeId, TRoadId>.Crosspoint(map,
                    snap.TrackCrosspoint,
                    snap.BaseRoadIndex, snap.SnapKind);
                adjacent.TryAdd(new Edge<TNodeId, TRoadId>( crosspoint), snap.BaseRoadIndex.RoadId);
            }
        }

        internal static void AddAdjacentToCrossPoint<TNodeId, TRoadId>(
            IWorldMap<TNodeId, TRoadId> map,
            IGeoCalculator calc,
            Dictionary<Edge<TNodeId, TRoadId>, TRoadId> adjacent,
            Placement<TNodeId, TRoadId> place, RoadBucket<TNodeId, TRoadId> bucket,
            bool isStarting)
            where TNodeId : struct
            where TRoadId : struct
        {
            foreach (var snap in bucket.ActiveSnaps)
            {
                if (map.IsRedundant(snap.BaseRoadIndex.RoadId) 
                    || !snap.Matches(place.Point.Convert2d(), place.BaseRoadIndex, place.NextRoadIndex))
                    continue;

                if (isStarting)
                {
                    {
                        Placement<TNodeId, TRoadId> adj_place = Placement<TNodeId, TRoadId>.Node(map,
                            snap.BaseRoadIndex);
                        adjacent.TryAdd(new Edge<TNodeId, TRoadId>(map,adj_place,snap.NextRoadIndex),
                            snap.BaseRoadIndex.RoadId);
#if DEBUG
                        if (place.DEBUG_Identifier == 4 || adj_place.DEBUG_Identifier == 15)
                        {
                            ;
                        }
#endif
                    }

                    if (snap.NextRoadIndex is { } next_idx)
                    {
                        Placement<TNodeId, TRoadId> adj_place = Placement<TNodeId, TRoadId>.Node(map,
                            next_idx);
                        adjacent.TryAdd(new Edge<TNodeId, TRoadId>(map,adj_place,snap.BaseRoadIndex),
                            snap.BaseRoadIndex.RoadId);
#if DEBUG
                        if (place.DEBUG_Identifier == 4 || adj_place.DEBUG_Identifier == 15)
                        {
                            ;
                        }
#endif
                    }
                }

                if (!isStarting)
                {
                    Placement<TNodeId, TRoadId> adj_place = Placement<TNodeId, TRoadId>.UserPoint(bucket);
                    adjacent.TryAdd(new Edge<TNodeId, TRoadId>(adj_place), snap.BaseRoadIndex.RoadId);
                }
            }
        }

        public static List<StepRun<TNodeId, TRoadId>> ReverseSteps<TNodeId, TRoadId>(
            IReadOnlyList<StepRun<TNodeId, TRoadId>> steps)
            where TNodeId : struct
            where TRoadId : struct
        {
            var result = new List<StepRun<TNodeId, TRoadId>>(capacity: steps.Count);
            if (steps.Count == 0)
                return result;

            Length incoming_distance = Length.Zero;
            TimeSpan incoming_time = TimeSpan.Zero;

            // the first point does not have any incoming road so we copy data for this from the next point
            var incoming_road_id = steps.Last().IncomingRoadId;
            RoadCondition incoming_condition = steps.Last().IncomingCondition;

            for (int i = steps.Count - 1; i >= 0; --i)
            {
                result.Add(new StepRun<TNodeId, TRoadId>(steps[i].Place, incoming_road_id, incoming_condition,
                    incoming_distance, incoming_time));

                incoming_distance = steps[i].IncomingDistance;
                incoming_time = steps[i].IncomingTime;
                incoming_road_id = steps[i].IncomingRoadId;
                incoming_condition = steps[i].IncomingCondition;
            }

            if (result.Count != steps.Count)
                throw new InvalidOperationException();

            return result;
        }

        public static IReadOnlyList<FlowJump<TNodeId, TRoadId>> ReverseJumps<TNodeId, TRoadId>(IReadOnlyList<FlowJump<TNodeId, TRoadId>> steps)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (steps.Count == 0)
                return steps;

            var result = new List<FlowJump<TNodeId, TRoadId>>(capacity: steps.Count);

            Length incoming_distance = Length.Zero;
            TimeSpan incoming_time = TimeSpan.Zero;

            // the first point does not have any incoming road so we copy data for this from the next point
            var incoming_traffic = steps.Last().IncomingRelativeTraffic;
            RoadCondition incoming_condition = steps.Last().IncomingCondition;

            for (int i = steps.Count - 1; i >= 0; --i)
            {
                result.Add(new FlowJump<TNodeId, TRoadId>()
                {
                    Place = steps[i].Place,
                    FacingBackwardIndex = steps[i].FacingForwardIndex,
                    FacingForwardIndex = steps[i].FacingBackwardIndex,
                    IncomingRelativeTraffic = incoming_traffic.Reverse(),
                    IncomingCondition = incoming_condition,
                    IncomingDistance = incoming_distance,
                    IncomingTime = incoming_time
                });

                incoming_traffic = steps[i].IncomingRelativeTraffic;
                incoming_distance = steps[i].IncomingDistance;
                incoming_time = steps[i].IncomingTime;
                incoming_condition = steps[i].IncomingCondition;
            }

            return result;
        }

        internal static List<StepRun<TNodeId, TRoadId>> RecreatePath<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            ICommonCover<TNodeId, TRoadId>? coverGrid,
            IReadOnlyBacktrack<TNodeId, TRoadId> backtrack,
            Placement<TNodeId, TRoadId> startPlace, Edge<TNodeId, TRoadId> endPlace, bool isReversed)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            var result_path = new List<StepRun<TNodeId, TRoadId>>();

            Edge<TNodeId, TRoadId> current_place = endPlace;

            while (true)
            {
                var (back_info, _) = backtrack[current_place];
                if (back_info.IsShortcut)
                {
                    // we have to skip first step of the shortcut because we don't have valid info there,
                    // we get it from the next iteration of entire recreating loop
                    var shortcut_steps = coverGrid!
                            .GetSteps(back_info.Source.Place.NodeId, current_place.Place.NodeId, isReversed)
                        ;
#if DEBUG
                    if (result_path.Count > 0 && shortcut_steps.Last().Place
                            .DEBUG_IsConnectedWith(result_path[^1].Place, 1057, 740))
                    {
                        ;
                    }
#endif

                    // o regular place, O shortcut boundaries, * place within shortcut

                    // forward building -->
                    // start o--o--O==*==*==O--o--o end
                    // <-- backtracking
                    // in this approach we have forward shortcut, and since we are backtracking
                    // from the right we need to just put elements of shortcut in reverse order
                    // ignoring head of the shortcut

                    // <-- backward building
                    // start o--o--O==*==*==O--o--o end
                    // backtracking -->

                    // in this case we still have the forward shortcut (after all, we move
                    // from start to end). But we have to "shift" all information shortcut
                    // elements have, so the first/left O-place would know where it is going
                    // an so on until last/right O-place which would be stripped from this
                    // information. It is "borrow-info-from-right" and drop the last element.
                    // To achieve this we use already existing functions: reverse steps
                    // (which transfers info), drop the (now) first element, and reorder elements back

                    if (isReversed)
                        shortcut_steps = ReverseSteps(shortcut_steps);
                    result_path.AddRange(shortcut_steps.Skip(1).Reverse());
                }
                else if (!back_info.Source.Place.IsPrestart)
                {
#if DEBUG
                    if (result_path.Count > 0 && current_place.Place.DEBUG_IsConnectedWith(result_path[^1].Place, 1057, 740))
                    {
                        ;
                    }
#endif

                    result_path.Add(new StepRun<TNodeId, TRoadId>(current_place.Place,
                        back_info.IncomingRoadId!.Value,
                        back_info.IncomingCondition!.Value,
                        back_info.IncomingDistance!.Value,
                        back_info.IncomingTime!.Value));
                }
                else
                {
                    // the end, we backtracked all steps

                    if (result_path.Count > 0)
                    {
                        result_path.Add(new StepRun<TNodeId, TRoadId>(current_place.Place,
                            result_path[^1].IncomingRoadId,
                            result_path[^1].IncomingCondition,
                            Length.Zero,
                            TimeSpan.Zero));

                        result_path.Reverse();
                        var init_step_distance = result_path.First().IncomingDistance;
                        var init_step_time = result_path.First().IncomingTime;
                        if (init_step_distance != Length.Zero || init_step_time != TimeSpan.Zero)
                            throw new ArgumentOutOfRangeException($"Initial step {TrackPlanner.Shared.Data.DataFormat.FormatDistance(init_step_distance, true)} in {TrackPlanner.Shared.Data.DataFormat.Format(init_step_time)}, expected zero.");
                    }

                    return result_path;
                }

                current_place = back_info.Source;
            }
        }
    }
}