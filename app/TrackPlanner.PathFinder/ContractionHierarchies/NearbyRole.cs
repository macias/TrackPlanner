using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    [Flags]
    internal enum NearbyRole
    {
        Source = 1,
        Target = 2
    }
}