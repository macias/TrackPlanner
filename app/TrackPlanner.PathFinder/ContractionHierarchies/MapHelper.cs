using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    internal readonly record struct HubConnection<TRoadId>(RoadIndex<TRoadId> RoadIndex,
        Weight ForwardWeight, // going to hub
        Weight BackwardWeight) // coming back from hub
    {
    }

    internal readonly record struct CrossroadTrace<TNodeId, TRoadId>(RoadIndex<TRoadId> RoadIndex,
        IReadOnlyList<TNodeId> Trace, // going to crossroad
    bool ThroughForbidden)
    {
    }

    internal readonly record struct CrossroadSteps<TNodeId, TRoadId>(RoadIndex<TRoadId> RoadIndex,
        WeightSteps<TNodeId, TRoadId> Forward, // going to crossroad
        WeightSteps<TNodeId, TRoadId> Backward, // going to crossroad
        bool ThroughForbidden)
        where TNodeId : struct
        where TRoadId : struct
    {
    }

    internal static class MapHelper
    {
        private enum Connectivity
        {
            Isolated,
            End,
            PassThrough,
            Crossroad,
        }

        private static Connectivity getConnectivity<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TNodeId nodeId)
            where TNodeId : struct
            where TRoadId : struct
        {
            var count = map.GetAdjacentRoads(nodeId).CountAtLeast(3);
            return count switch
            {
                0 => Connectivity.Isolated,
                1 => Connectivity.End,
                2 => Connectivity.PassThrough,
                3 => Connectivity.Crossroad,
                _ => throw new NotSupportedException()
            };
        }

        public static bool IsRoadCrossroad<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TNodeId nodeId)
            where TNodeId : struct
            where TRoadId : struct
        {
            var connectivity = map.getConnectivity(nodeId);
            return connectivity == Connectivity.Crossroad;
        }

        public static IEnumerable<CrossroadTrace<TNodeId, TRoadId>> GetNearbyCrossroads<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TNodeId nodeId)
            where TNodeId : struct
            where TRoadId : struct
        {
            // get either ends or crossroads

            foreach (var road_idx in map.GetRoadsAtNode(nodeId))
            {
                {
                    if (map.tryGetNearbyCrossroad(road_idx, Direction.Backward, out var conn))
                        yield return conn;
                }
                {
                    if (map.tryGetNearbyCrossroad(road_idx, Direction.Forward, out var conn))
                        yield return conn;
                }
            }
        }

        public static IEnumerable<CrossroadSteps<TNodeId, TRoadId>> CH_GetNearbyCrossroads2<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RouteLogic<TNodeId, TRoadId> logic,
            TNodeId nodeId)
            where TNodeId : struct
            where TRoadId : struct
        {
            // get either ends or crossroads

            foreach (var road_idx in map.GetRoadsAtNode(nodeId))
            {
                {
                    if (map.CH_tryGetNearbyCrossroad2(logic, road_idx, Direction.Backward, out var conn))
                        yield return conn;
                }
                {
                    if (map.CH_tryGetNearbyCrossroad2(logic, road_idx, Direction.Forward, out var conn))
                        yield return conn;
                }
            }
        }

        public static IEnumerable<HubConnection<TRoadId>> EXPERIMENTAL_GetNearbyHubs<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RouteLogic<TNodeId, TRoadId> logic,
            TNodeId nodeId)
            where TNodeId : struct
            where TRoadId : struct
        {
            // get either ends or crossroads

            foreach (var road_idx in map.GetRoadsAtNode(nodeId))
            {
                {
                    if (map.EXPERIMENTAL_tryGetInterconnection(logic, road_idx, Direction.Backward,
                            out var conn))
                        yield return conn;
                }
                {
                    if (map.EXPERIMENTAL_tryGetInterconnection(logic, road_idx, Direction.Forward,
                            out var conn))
                        yield return conn;
                }
            }
        }

        private static bool EXPERIMENTAL_tryGetInterconnection<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RouteLogic<TNodeId, TRoadId> logic,
            RoadIndex<TRoadId> current, Direction direction, out HubConnection<TRoadId> connection)
            where TNodeId : struct
            where TRoadId : struct
        {
            var last_node = map.GetNode(current);
            var last_road_id = current.RoadId;
            int last_index = current.IndexAlongRoad;
            var last_road_count = map.GetRoadInfo(last_road_id).Nodes.Count;
            var forward_weight = Weight.Zero;
            var backward_weight = Weight.Zero;
            while (true)
            {
                var target_index = last_index + direction.AsChange();
                if (target_index < 0 || target_index == last_road_count)
                {
                    var temp = new RoadIndex<TRoadId>(last_road_id, last_index);
                    if (temp == current)
                        break;
                    (last_road_id, last_index) = map.GetRoadsAtNode(last_node)
                        .Single(it => it != temp)
                        .AsTuple();

                    direction = last_index == 0 ? Direction.Forward : Direction.Backward;
                    last_road_count = map.GetRoadInfo(last_road_id).Nodes.Count;
                    continue;
                }

                var target = new RoadIndex<TRoadId>(last_road_id, target_index);
                if (target == current)
                    break;

                var from_place = Placement<TNodeId, TRoadId>.Node(map,Edge.INDEX<TNodeId,TRoadId>( last_node));
                var to_place = Placement<TNodeId, TRoadId>.Node(map, target);
                forward_weight = (forward_weight + logic.GetFullStepInfo(connectingRoadId: last_road_id,
                    Edge.INDEX<TNodeId,TRoadId>( Edge.SOURCE<TNodeId>()),
                    from_place,
                    to_place, reversed: false).Weight);
                backward_weight = (backward_weight + logic.GetFullStepInfo(connectingRoadId: last_road_id,
                    Edge.INDEX<TNodeId,TRoadId>( Edge.SOURCE<TNodeId>()),
                    from_place, 
                    to_place, reversed: true).Weight);


                if (map.IsRoadCrossroad(to_place.NodeId))
                {
                    connection = new HubConnection<TRoadId>(target, forward_weight, backward_weight);
                    return true;
                }

                last_index = target_index;
                last_node = to_place.NodeId;
            }

            connection = default;
            return false;
        }

        private static bool tryGetNearbyCrossroad<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<TRoadId> initial, Direction direction, out CrossroadTrace<TNodeId, TRoadId> connection)
            where TNodeId : struct
            where TRoadId : struct
        {
            bool through_forbidden = false;
            var last_node = map.GetNode(initial);
            int last_road_idx = initial.IndexAlongRoad;
            var last_road_id = initial.RoadId;
            int last_road_count;
            {
                RoadInfo<TNodeId> road_info = map.GetRoadInfo(last_road_id);
                if (road_info.IsCyclingForbidden())
                    through_forbidden = true;
                last_road_count = road_info.Nodes.Count;
            }

            var trace = new List<TNodeId>();
            while (true)
            {
                var target_road_idx = last_road_idx + direction.AsChange();
                if (target_road_idx < 0 || target_road_idx == last_road_count)
                {
                    var last_index = new RoadIndex<TRoadId>(last_road_id, last_road_idx);
                    if (last_index == initial)
                        break;
                    // we cannot continue using current road, so then find out
                    // what the other road is used on the given node and then continue
                    var other_index = map.GetRoadsAtNode(last_node)
                        .SingleOrNone(it => it != last_index);
                    if (!other_index.HasValue)
                        break;
                    (last_road_id, last_road_idx) = other_index.Value.AsTuple();

                    direction = last_road_idx == 0 ? Direction.Forward : Direction.Backward;
                    {
                        RoadInfo<TNodeId> road_info = map.GetRoadInfo(last_road_id);
                        last_road_count = road_info.Nodes.Count;
                        if (road_info.IsCyclingForbidden())
                            through_forbidden = true;
                    }
                    continue;
                }

                var target = new RoadIndex<TRoadId>(last_road_id, target_road_idx);
                if (target == initial)
                    break;

                last_node = map.GetNode(target);

                if (map.IsRoadCrossroad(last_node))
                {
                    connection = new CrossroadTrace<TNodeId, TRoadId>(target, trace,through_forbidden);
                    return true;
                }

                trace.Add(last_node);

                last_road_idx = target_road_idx;
            }

            connection = default;
            return false;
        }
        
        private static bool CH_tryGetNearbyCrossroad2<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RouteLogic<TNodeId, TRoadId> logic,
            RoadIndex<TRoadId> initial, Direction direction, out CrossroadSteps<TNodeId, TRoadId> connection)
            where TNodeId : struct
            where TRoadId : struct
        {
            bool through_forbidden = false;
            var last_node = map.GetNode(initial);
            int last_road_idx = initial.IndexAlongRoad;
            var last_road_id = initial.RoadId;
            int last_road_count;
            {
                RoadInfo<TNodeId> road_info = map.GetRoadInfo(last_road_id);
                if (road_info.IsCyclingForbidden())
                    through_forbidden = true;
                last_road_count = road_info.Nodes.Count;
            }

            while (true)
            {
                var target_road_idx = last_road_idx + direction.AsChange();
                if (target_road_idx < 0 || target_road_idx == last_road_count)
                {
                    var last_index = new RoadIndex<TRoadId>(last_road_id, last_road_idx);
                    if (last_index == initial)
                        break;
                    // we cannot continue using current road, so then find out
                    // what the other road is used on the given node and then continue
                    var other_index = map.GetRoadsAtNode(last_node)
                        .SingleOrNone(it => it != last_index);
                    if (!other_index.HasValue)
                        break;
                    (last_road_id, last_road_idx) = other_index.Value.AsTuple();

                    direction = last_road_idx == 0 ? Direction.Forward : Direction.Backward;
                    {
                        RoadInfo<TNodeId> road_info = map.GetRoadInfo(last_road_id);
                        last_road_count = road_info.Nodes.Count;
                        if (road_info.IsCyclingForbidden())
                            through_forbidden = true;
                    }
                    continue;
                }

                var target = new RoadIndex<TRoadId>(last_road_id, target_road_idx);
                if (target == initial)
                    break;

                Placement<TNodeId,TRoadId> current_place 
                    = Placement<TNodeId, TRoadId>.Node(map,Edge.INDEX<TNodeId,TRoadId>( last_node));
                last_node = map.GetNode(target);
                Placement<TNodeId,TRoadId> target_place 
                    = Placement<TNodeId, TRoadId>.Node(map,Edge.INDEX<TNodeId,TRoadId>(last_node));
                var fwd_info = logic.GetFullStepInfo(last_road_id,Edge.INDEX<TNodeId,TRoadId>( Edge.SOURCE<TNodeId>()), 
                    current_place, target_place, reversed:false);
                var bwd_info = logic.GetFullStepInfo(last_road_id,Edge.INDEX<TNodeId,TRoadId>( Edge.SOURCE<TNodeId>()),
                    current_place, target_place, reversed:true);

                if (map.IsRoadCrossroad(last_node))
                {
                    if (DevelModes.True)
                    throw new NotImplementedException();
                    else
                    {
                        connection = new CrossroadSteps<TNodeId, TRoadId>(target,
                            new WeightSteps<TNodeId, TRoadId>(),
                            new WeightSteps<TNodeId, TRoadId>(), through_forbidden);
                        return true;
                    }
                }

                last_road_idx = target_road_idx;
            }

            connection = default;
            return false;
        }
        
    }
}