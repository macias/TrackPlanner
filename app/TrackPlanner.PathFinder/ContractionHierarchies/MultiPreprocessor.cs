using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;
using TrackRadar.Collections;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    internal sealed class PreNodeInfoComparer : IComparer<PreNodeInfo>
    {
        public int Compare(PreNodeInfo x, PreNodeInfo y)
        {
            return -x.Connections.CompareTo(y.Connections);
        }
    }
    internal readonly record struct PreNodeInfo(int Connections) 
    {
        public int CompareTo(PreNodeInfo other)
        {
            return Connections.CompareTo(other.Connections);
        }

        public PreNodeInfo ChangeConnection(int change)
        {
            return this with {Connections = Connections + change};
        }
    }

    public sealed class MultiPreprocessor<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private static readonly IReadOnlySet<TNodeId> emptySuppressedTraffic = new HashSet<TNodeId>();

        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly IGeoCalculator calc;
        private readonly FinderConfiguration finderConfig;
        private readonly UserRouterPreferences userConfig;
        private readonly WeightComparer weightComparer;
        private readonly Dictionary<TNodeId, int> removed_nodes;
        private readonly ChCover<TNodeId, TRoadId> ch_cover;
        private readonly Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>> shortcuts;
        private readonly int allNodesCount;
        private readonly double startTime;
        private double limitTime;
        private double actualTime;
        private double maxLimitTime;
        private double maxActualTime;
        private readonly MappedPairingHeap<TNodeId, PreNodeInfo, ValueTuple> heap;
        private readonly IReadOnlySet<TNodeId> crossroads;
        private readonly RouteLogic<TNodeId,TRoadId> logic;

        public MultiPreprocessor(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            IGeoCalculator calc,
            FinderConfiguration finderConfig, UserRouterPreferences userConfig)
        {
            this.logger = logger;
            this.navigator = navigator;
            this.map = map;
            this.calc = calc;
            this.finderConfig = finderConfig;

            this.userConfig = userConfig.DeepClone();
            this.userConfig.TrafficSuppression = Length.Zero;
            this.userConfig.SnapCyclewaysToRoads = Length.Zero;

            this.weightComparer = WeightComparer.Instance;

            this.removed_nodes = new Dictionary<TNodeId, int>();
            this.shortcuts = new Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>>();

            this.ch_cover = new ChCover<TNodeId, TRoadId>(this.map, removed_nodes, shortcuts);

            this.crossroads = this.map.GetAllNodes()
                .Select(it => it.Key)
                .Where(it => this.map.IsRoadCrossroad(it))
                .ToHashSet();
            this.allNodesCount = crossroads.Count();

            this.startTime = Stopwatch.GetTimestamp();

            this.logic = new RouteLogic<TNodeId, TRoadId>(map, calc,
                userConfig,
                emptySuppressedTraffic);
            this.heap = MappedPairingHeap.Create<TNodeId, PreNodeInfo, ValueTuple>( new PreNodeInfoComparer() );
            foreach (var node in crossroads)
            {
                heap.TryAddOrUpdate(node, new PreNodeInfo()
                {
                    Connections = map.GetAdjacentRoads(node).Count(),
                }, new ValueTuple());
            }

            int idx = 0;
            while (heap.TryPop(out var node, out _, out _))
            {
                calculate(idx, node);
                ++idx;
            }
        }

        private bool updateShortcuts(TNodeId sourceNodeId, TNodeId targetNodeId,
            in WeightSteps<TNodeId, TRoadId> info, bool backReference)
        {
            if (backReference)
                (sourceNodeId, targetNodeId) = (targetNodeId, sourceNodeId);

            if (!shortcuts.TryGetValue(sourceNodeId, out var connection))
            {
                connection = new Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>();
                shortcuts.Add(sourceNodeId, connection);
            }

            if (!connection.TryGetValue(targetNodeId, out var existing))
                existing = new BiShortcutInfo<TNodeId, TRoadId>(null, null);

            if (existing.GetInfo(backReference) is not { } curr_info
                || weightComparer.IsLess(info.JoinedWeight, curr_info.JoinedWeight))
            {
                connection[targetNodeId] = existing.With(info, backReference);
                return true;
            }
            else
                return false;
        }

        MultiWeight computeMultiWeight(int iterationIndex, 
            TNodeId focusNode,
            IReadOnlyList<TNodeId> src, IReadOnlyList<TNodeId> dst,
            IReadOnlySet<TNodeId> allowed)
        {
            if (DEBUG_SWITCH.Enabled)
            {
               // dumpKml(allowed,"allowed");
            }

            MultiRoute<TNodeId, TRoadId> multi_res;
            using (var cts = new CancellationTokenSource(DEBUG_SWITCH.Timeout))
            {
                multi_res = MultiRouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger,
                    this.navigator,
                    this.map,
                    coverGrid: ch_cover,
                    connectionPredicate: (_, adj, _) => allowed.Contains(adj),
                    weightLimit: null,
                    this.finderConfig,
                    this.userConfig, src, dst,
                    bucketPredicate,
                    allowSmoothing: false,
                    allowGap:false,
                    cts.Token);
            }

            var result = new MultiWeight(src.Count, dst.Count);
            var errors = new List<ContextMessage>();
            foreach (var (source_node, source_idx) in src.ZipIndex())
            {
                foreach (var (target_node, target_idx) in dst.ZipIndex())
                {
                    Result<bool> validate = multi_res.Validate(this.map, source_idx, source_node,
                        target_idx, target_node, $"Init iteration {iterationIndex}",
                        out var leg, out var weight);
                    if (validate.HasValue)
                    {
                        if (!validate.Value)
                            continue;

                        result.Limits[source_idx, target_idx] = new TotalWeight(weight, Weight.Zero);
                    }
                    else
                    {
                        errors.Add(validate.ProblemMessage!.Value);
                    }
                }
            }

            if (errors.Any())
            {
                dumpKml(focusNode, src, dst);
                throw new Exception(String.Join(Environment.NewLine, errors));
            }

            return result;
        }

        private static bool bucketPredicate(RoadInfo<TNodeId> roadInfo)
        {
            return !roadInfo.IsCyclingForbidden();
        }

        Weight computeWeight(TNodeId src, TNodeId dst, IReadOnlySet<TNodeId> allowed)
        {
            var finding = RouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                this.map,
                coverGrid: ch_cover,
                connectionPredicate: (_, adj, _) => allowed.Contains(adj),
                weightLimit: null,
                this.finderConfig,
                this.userConfig, new[] {src, dst},
                allowSmoothing: false,
                allowGap:false,
                CancellationToken.None);
            if (!finding.HasValue)
                throw new Exception($"Could not find a model route {finding}. {shortcuts!.Count} shortcuts");

            if (finding.ProblemMessage is {} problem)
                throw new Exception($"{problem} for #{this.map.GetOsmNodeId(src)} to #{this.map.GetOsmNodeId(dst)}");


            return finding.Value.RouteWeight;
        }

        private void calculate(int idx, TNodeId focusNode)
        {
            if (idx % 10 == 0)
            {
                TimeSpan eta = TimeSpan.Zero;
                double limit_speed_ms = 0;
                double actual_speed_ms = 0;
                var passed = (Stopwatch.GetTimestamp() - startTime) / Stopwatch.Frequency;
                if (idx > 0)
                {
                    eta = TimeSpan.FromSeconds((allNodesCount - idx) * passed / idx);
                    limit_speed_ms = 1000 * this.limitTime / (idx * Stopwatch.Frequency);
                    actual_speed_ms = 1000 * this.actualTime / (idx * Stopwatch.Frequency);
                }

                this.logger.Info($"Preprocessed {idx}/{allNodesCount} nodes in {DataFormat.Format(TimeSpan.FromSeconds(passed))} ({limit_speed_ms:0.##}:{actual_speed_ms:0.##}ms), max {this.maxLimitTime*1000/Stopwatch.Frequency:0.##}:{this.maxActualTime*1000/Stopwatch.Frequency:0.##}ms, ETA {DataFormat.Format(eta)}");
            }

            var nearby = new Dictionary<TNodeId, NearbyInfo2<TNodeId>>();
            {
                foreach (var conn in map.CH_GetNearbyCrossroads2(this.logic,focusNode))
                {
                    var id = this.map.GetNode(conn.RoadIndex);
                    if (removed_nodes.ContainsKey(id) 
                        || EqualityComparer<TNodeId>.Default.Equals(id, focusNode)
                        || conn.ThroughForbidden)
                        continue;

                    if (!nearby.TryGetValue(id, out var info))
                    {
                        info = new NearbyInfo2<TNodeId>()
                        {
                            Allowed = new HashSet<TNodeId>() {id, focusNode},
                            Role = NearbyRole.Source | NearbyRole.Target,
                        };
                        nearby.Add(id, info);
                    }

                    throw new NotImplementedException();
                    //info.Allowed.AddRange(conn.Trace);
                }

                if (shortcuts.TryGetValue(focusNode, out var sc))
                    foreach (var (id, conn) in sc)
                    {
                        if (removed_nodes.ContainsKey(id) 
                            || EqualityComparer<TNodeId>.Default.Equals(id, focusNode))
                            continue;

                        if (!nearby.TryGetValue(id, out var info))
                        {
                            info = new NearbyInfo2<TNodeId>()
                            {
                                Allowed = new HashSet<TNodeId>() {focusNode, id},
                            };
                        }

                        if (conn.Forward.HasValue)
                            info = info with {Role = info.Role | NearbyRole.Target};
                        if (conn.Backward.HasValue)
                            info = info with {Role = info.Role | NearbyRole.Source};

                        nearby[id] = info;
                        if (DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }
                    }
            }

            var source_nodes = nearby
                .Where(it => it.Value.Role.HasFlag(NearbyRole.Source))
                .Select(it => it.Key)
                .ToArray();
            var target_nodes = nearby
                .Where(it => it.Value.Role.HasFlag(NearbyRole.Target))
                .Select(it => it.Key)
                .ToArray();

            if (source_nodes.Length != 0 && target_nodes.Length != 0)
            {
                DEBUG_SWITCH.Enabled = idx == DEBUG_SWITCH.Iteration;
                MultiWeight? weight_limit = null;
                {
                    double start = Stopwatch.GetTimestamp();
                    weight_limit = computeMultiWeight(idx,focusNode, source_nodes, target_nodes,
                        nearby.Values.SelectMany(it => it.Allowed).ToHashSet());
                    var passed = Stopwatch.GetTimestamp() - start;
                    if (this.maxLimitTime < passed)
                        this.maxLimitTime = passed;
                    this.limitTime += passed;
                }

                MultiRoute<TNodeId, TRoadId> multi_res;
                if (DEBUG_SWITCH.Enabled)
                {
                    ;
                }

                using (var cts = new CancellationTokenSource(DEBUG_SWITCH.Timeout))
                    try
                    {
                        double start = Stopwatch.GetTimestamp();
                        multi_res = MultiRouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                            this.map,
                            coverGrid: ch_cover,
                            connectionPredicate: null,
                            weight_limit,
                            this.finderConfig,
                            this.userConfig, source_nodes, target_nodes,
                            bucketPredicate,
                            allowSmoothing: false,
                            allowGap:false,
                            cts.Token);
                        var passed = Stopwatch.GetTimestamp() - start;
                        if (this.maxActualTime < passed)
                            this.maxActualTime = passed;
                        this.actualTime += passed;
                    }
                    catch
                    {
                        dumpKml(focusNode, source_nodes, target_nodes);

                        throw;
                    }

                var errors = new List<ContextMessage>();
                foreach (var (source_node, source_idx) in source_nodes.ZipIndex())
                {
                    foreach (var (target_node, target_idx) in target_nodes.ZipIndex())
                    {
                        if (DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }

                        Result<bool> validate = multi_res.Validate(this.map, source_idx, source_node,
                            target_idx, target_node, $"Main iteration {idx}", out var leg, out var weight);
                        if (validate.HasValue)
                        {
                            if (!validate.Value)
                                continue;

                            if (leg.Steps
                                .Where(it => it.Place.IsNode)
                                .Select(it => it.Place.NodeId)
                                .Any(it => EqualityComparer<TNodeId>.Default.Equals(focusNode, it)))
                            {
                                var info = new WeightSteps<TNodeId, TRoadId>(leg.Steps.ToList(), weight,isDraft:false);

                                Option<StepRun<TNodeId, TRoadId>> invalid_step = info.Steps
                                    .FirstOrNone(it => !it.Place.IsNode);
                                if (invalid_step.HasValue)
                                    throw new Exception($"Only nodes can create a shortcut {invalid_step.Value.Place}");
                                if (!EqualityComparer<TNodeId>.Default.Equals(source_node, info.Steps.First().Place.NodeId))
                                    throw new Exception("Shortcut does not reach start");
                                if (!EqualityComparer<TNodeId>.Default.Equals(target_node, info.Steps.Last().Place.NodeId))
                                    throw new Exception("Shortcut does not reach end");
                                if (updateShortcuts(source_node, target_node, info, backReference: false))
                                {
                                    var steps = this.ch_cover.GetSteps(source_node, target_node, isReversed: false);
                                    if (!EqualityComparer<TNodeId>.Default.Equals(source_node, steps.First().Place.NodeId))
                                        throw new Exception("Shortcut does not reach start");
                                    if (!EqualityComparer<TNodeId>.Default.Equals(target_node, steps.Last().Place.NodeId))
                                        throw new Exception("Shortcut does not reach end");
                                }

                                updateHeap(focusNode, source_node, +1);
                                updateHeap(focusNode, target_node, +1);

                                if (updateShortcuts(source_node, target_node, info, backReference: true))
                                {
                                    var steps = this.ch_cover.GetSteps(target_node, source_node, isReversed: true);
                                    if (!EqualityComparer<TNodeId>.Default.Equals(source_node, steps.First().Place.NodeId))
                                        throw new Exception("Shortcut does not reach start");
                                    if (!EqualityComparer<TNodeId>.Default.Equals(target_node, steps.Last().Place.NodeId))
                                        throw new Exception("Shortcut does not reach end");
                                }
                            }
                        }
                        else
                        {
                            errors.Add(validate.ProblemMessage!.Value);
                        }
                    }
                }

                if (errors.Any())
                {
                    dumpKml(focusNode, source_nodes, target_nodes);
                    throw new Exception(String.Join(Environment.NewLine, errors));
                }
            }

            foreach (var node in nearby.Keys)
            {
                updateHeap(focusNode, node, -1);
            }

            removed_nodes.Add(focusNode, removed_nodes.Count);
        }

        private void dumpKml(TNodeId focusNode, IReadOnlyList<TNodeId> startNodes,
            IReadOnlyList<TNodeId> endNodes)
        {
            var input = new TrackWriterInput() {Title = null};
            input.AddPoint(this.map.GetPoint(focusNode), $"{this.map.GetOsmNodeId(focusNode)}", comment: null, PointIcon.StarIcon);
            foreach (var node in startNodes.Concat(endNodes).Distinct())
            {
                var start_idx = startNodes.IndexOf(node);
                var end_idx = endNodes.IndexOf(node);
                var label = $"{this.map.GetOsmNodeId(node)} {(start_idx==-1 ? "":$"S-{start_idx}")} {(end_idx==-1 ? "":$"E-{end_idx}" )}";
                input.AddPoint(this.map.GetPoint(node), label, comment: null, PointIcon.DotIcon);
            }

            var kml = input.BuildDecoratedKml();

            using (var stream = new FileStream(Navigator.GetUniquePath(new Navigator().GetOutputDirectory(), "dump-multi-search.kml"),
                       FileMode.CreateNew, FileAccess.Write))
            {
                kml.Save(stream);
            }
        }
        private void dumpKml(IEnumerable<TNodeId> nodes,string filename)
        {
            var input = new TrackWriterInput() {Title = null};
            foreach (var node in nodes)
            {
                var label = $"{this.map.GetOsmNodeId(node)}";
                input.AddPoint(this.map.GetPoint(node), label, comment: null, PointIcon.DotIcon);
            }

            var kml = input.BuildDecoratedKml();

            using (var stream = new FileStream(Navigator.GetUniquePath(
                           new Navigator().GetOutputDirectory(), $"{filename}.kml"),
                       FileMode.CreateNew, FileAccess.Write))
            {
                kml.Save(stream);
            }
        }

        private void updateHeap(TNodeId DEBUG_focusNode, TNodeId node, int change)
        {
            if (!this.heap.TryGetData(node, out var info, out _))
                throw new Exception($"{EqualityComparer<TNodeId>.Default.Equals(DEBUG_focusNode, node)}, {this.crossroads.Contains(node)}");
            var new_info = info.ChangeConnection(change);

            /*   if (info.CompareTo( new_info) <= 0)
               {
                   heap.
                   special
               }
               else
                   if (!this.heap.TryAddOrUpdate(node, new_info, new ValueTuple()))
                       throw new Exception();
            */
        }
    }
}