﻿
using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using TrackPlanner.Shared;
using TrackRadar.Collections;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    public readonly record struct MultiWeight
    {
        public TotalWeight[,] Limits { get; }

        public MultiWeight(int sources, int targets)
        {
            this.Limits = new TotalWeight[sources, targets];
        }
       
    }
   
}