Main executors:

* Preprocessor -- single node to other single node, this type is for removal
* Preprocessor2 -- same as above
* MultiPreprocessor -- it computes many-to-many routes, and here is the main focus of work

The code seems correct, it has following steps:
* pick node for removal
* get nearby crossroads (not just OSM node, too much computation)
* ignore crossroad if there is forbidden segment for it
(maybe in future we could drop this requirement)
* compute route between crossroads and check if the shortest route
goes through picked node -- if yes, create new shortcut

The problem is program is way too slow, it has something like 
250 000 nodes to process and after 8 hours it processed 70 000
with ETA 1 day more and at this point it took more than 5 minutes
for a step and 9 seconds to compute routes through known in advance
nodes (this is speed-up step).

In theory I could add another speed-up -- giving up if the route
already went through N nodes and didn't find a winner, but this
would mean suboptimal solution.

So far I didn't have any better ideas, and time ran out, so I am
stopping further tests.