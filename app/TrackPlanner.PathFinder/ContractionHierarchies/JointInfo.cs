﻿using System;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    internal readonly record struct JointInfo<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public Edge<TNodeId, TRoadId> ForwardEdge { get; init; }
        public Edge<TNodeId, TRoadId> BackwardEdge { get; init; }
        public TotalWeight ForwardWeight { get; init; }
        public TotalWeight BackwardWeight { get; init; }
        public int ForwardTimestamp { get; init; }
        public int BackwardTimestamp { get; init; }
        
        public TotalWeight OBSOLETE_JoinedWeight => TotalWeight.OBSOLETE_JoinFinal(ForwardWeight, BackwardWeight);
        public Weight JoinedWeight => TotalWeight.JoinFinal(ForwardWeight, BackwardWeight);

        public JointInfo(Edge<TNodeId, TRoadId> forwardEdge,
            Edge<TNodeId, TRoadId> backwardEdge,
            TotalWeight forwardWeight,
            TotalWeight backwardWeight,
            int forwardTimestamp,
            int backwardTimestamp)
        {
            if (forwardEdge.Place != backwardEdge.Place)
                throw new ArgumentException("Not really a joint.");
            
            ForwardEdge = forwardEdge;
            BackwardEdge = backwardEdge;
            ForwardWeight = forwardWeight;
            BackwardWeight = backwardWeight;
            ForwardTimestamp = forwardTimestamp;
            BackwardTimestamp = backwardTimestamp;
        }

        public int GetTimestamp(bool forward) => forward ? ForwardTimestamp : BackwardTimestamp;

    }
}