﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Backend;
using TrackRadar.Collections;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    internal sealed class MultiSearch<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly TotalWeightComparer weightComparer;
        private readonly MultiWeight? weightLimit;
        public bool IsForwardSide { get; private set; }
        private readonly JointInfo<TNodeId, TRoadId>?[,] joints;
        public MultiStage<TNodeId, TRoadId> Result { get; }
        private readonly SearchSide<TNodeId, TRoadId> start;
        private readonly SearchSide<TNodeId, TRoadId> end;

        public bool IsCompleted => this.start.IsCompleted && this.end.IsCompleted;

        public MultiSearch(IWorldMap<TNodeId, TRoadId> map,TotalWeightComparer weightComparer,
            MultiWeight? weightLimit,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> startBuckets,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> endBuckets)
        {
            this.weightComparer = weightComparer;
            this.weightLimit = weightLimit;
            this.Result = new MultiStage<TNodeId, TRoadId>(startBuckets.Count, endBuckets.Count);

            this.start = new SearchSide<TNodeId, TRoadId>(map,weightComparer, startBuckets, endBuckets.Count);
            this.end = new SearchSide<TNodeId, TRoadId>(map,weightComparer, endBuckets, startBuckets.Count);
            this.joints = new JointInfo<TNodeId, TRoadId>?[startBuckets.Count, endBuckets.Count];

            // keep forward side to true in order resolve call work correctly
            this.IsForwardSide = true;
            for (int s = 0; s < startBuckets.Count; ++s)
            for (int t = 0; t < endBuckets.Count; ++t)
            {
                if (startBuckets[s].UserPoint == endBuckets[t].UserPoint)
                {
                    resolve(s, t,
                        Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding>
                            .Fail(LegacyRouteFinding.Same));
                }
            }
        }

        public void SwitchSide()
        {
            // pick either side with not depleted heap, or simply switch side on each step
            var switched_side = this.start.IsCompleted
                ? false
                : this.end.IsCompleted
                    ? true
                    : !this.IsForwardSide;

            this.IsForwardSide = switched_side;
        }

        public IEnumerable<(int index, RoadBucket<TNodeId, TRoadId> bucket,
            MappedPairingHeap<Edge<TNodeId, TRoadId>, TotalWeight, BacktrackInfo<TNodeId, TRoadId>> heap,
            Backtrack<TNodeId, TRoadId> backtrack)> Iterate()
        {
            return (IsForwardSide ? this.start : this.end).Iterate();
        }

        public IEnumerable<(int index, RoadBucket<TNodeId, TRoadId> bucket,
            MappedPairingHeap<Edge<TNodeId, TRoadId>, TotalWeight, BacktrackInfo<TNodeId, TRoadId>> heap,
            Backtrack<TNodeId, TRoadId> backtrack)> OppositeFor(int fromIndex)
        {
            return (IsForwardSide ? this.end : this.start).Iterate()
                .Where(it =>
                {
                    var (from, to) = AbsoluteIndices(fromIndex, it.index);
                    // return only those to-data that have not found route yet
                    return Result.Stages[from, to] == null;
                });
        }

        internal (int, int) AbsoluteIndices(int fromIndex, int toIndex)
        {
            return IsForwardSide ? (fromIndex, toIndex) : (toIndex, fromIndex);
        }

        public JointInfo<TNodeId, TRoadId>? GetJoint(int fromIndex, int toIndex)
        {
            (fromIndex, toIndex) = AbsoluteIndices(fromIndex, toIndex);
            return this.joints[fromIndex, toIndex];
        }

        public void FailFor(int fromIndex, int toIndex)
        {
            resolve(fromIndex, toIndex, Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding>
                .Fail(LegacyRouteFinding.NotFound));
        }

        public void SuccessFor(int fromIndex, int toIndex, List<StepRun<TNodeId, TRoadId>> steps,
            Weight weight)
        {
            resolve(fromIndex, toIndex, Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding>
                .Valid(new PathFinder.WeightSteps<TNodeId, TRoadId>(steps, weight,isDraft:false)));
        }

        private void resolve(int fromIndex, int toIndex, in Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding> stage)
        {
            (fromIndex, toIndex) = AbsoluteIndices(fromIndex, toIndex);

            if (this.Result.Stages[fromIndex, toIndex] != null)
                throw new ArgumentException("Stage is already resolved");
            this.Result.Stages[fromIndex, toIndex] = stage;

            ackJoint(fromIndex, toIndex);

            this.start.DropReceiverFor(fromIndex);
            this.end.DropReceiverFor(toIndex);
        }

        public bool UpdateJoint(int fromIndex, int toIndex,
            BacktrackInfo<TNodeId, TRoadId> fromTrackInfo,
            BacktrackInfo<TNodeId, TRoadId> toTrackInfo,
            Placement<TNodeId, TRoadId> place, 
            TotalWeight fromWeight,
            TotalWeight toWeight)
        {
            if (!IsForwardSide)
                (fromWeight, toWeight) = (toWeight, fromWeight);
            
            var totalWeight = TotalWeight.OBSOLETE_JoinFinal(fromWeight, toWeight);
            if (!weightPasses(fromIndex, toIndex, totalWeight,oppositeBoundary:Weight.Zero))
                return false;

            (fromIndex, toIndex) = AbsoluteIndices(fromIndex, toIndex);

            ackJoint(fromIndex, toIndex);

            JointInfo<TNodeId, TRoadId>? joint = this.joints[fromIndex, toIndex];

            if (joint is { } j)
            {
                if (!weightComparer.IsLess(totalWeight, j.OBSOLETE_JoinedWeight))
                    return false;

                // updating joints is a bit tricky, due to numeric weight rounding, typical case is having
                // both sides A-X-B join at point X and then one side proceed further "eating" already
                // visited places, just because from its side there is some weight saving. It is only because
                // of the numeric errors, not because there is real saving

                // in weight limit mode it is a big problem, because we could "optimize" weight in such way
                // then the second pass with this limit will report than first joint has bigger weight and
                // reject such route

                // to avoid this we timestamp joint, and then check if we are updating joint with bridge-data
                // i.e. connections between boundaries from each side from the time the joint was created

                var opposite_backtrack = IsForwardSide 
                    ? this.end.GetBacktrack(toIndex) : this.start.GetBacktrack(fromIndex);
                
                if (!opposite_backtrack
                             .IsAtJointBoundary(j.GetTimestamp(forward:!IsForwardSide),
                                 // not a typo, when we are going from end
                                 // "from" means "backwards", and "to" forward
                                 toTrackInfo.Timestamp,  fromTrackInfo.Source))
                    return false;

                if (DEBUG_SWITCH.Enabled && (fromIndex, toIndex) == DEBUG_SWITCH.ElemIndex)
                {
                    var fd = this.start.GetBacktrack(fromIndex).DEBUG_GetDepth(Edge.STUB(place));
                    var bd = this.end.GetBacktrack(toIndex).DEBUG_GetDepth(Edge.STUB(place));
                    ;
                }

                joints[fromIndex, toIndex] = j with {ForwardEdge = Edge.STUB(place), BackwardEdge = Edge.STUB(place),
                    ForwardWeight = fromWeight, BackwardWeight = toWeight};
            }
            else
            {
                if (DEBUG_SWITCH.Enabled && (fromIndex, toIndex) == DEBUG_SWITCH.ElemIndex)
                {
                    var fd = this.start.GetBacktrack(fromIndex).DEBUG_GetDepth(Edge.STUB(place));
                    var bd = this.end.GetBacktrack(toIndex).DEBUG_GetDepth(Edge.STUB(place));
                    ;
                }

                joints[fromIndex, toIndex] = new JointInfo<TNodeId, TRoadId>(Edge.STUB(place),Edge.STUB(place),
                fromWeight,
                    toWeight,
                    this.start.GetBacktrack(fromIndex).Timestamp,
                    this.end.GetBacktrack(toIndex).Timestamp);
            }

            return true;
        }

        private void ackJoint(int fromIndex, int toIndex)
        {
            if (this.joints[fromIndex, toIndex] == null)
            {
                this.start.JointFound(fromIndex);
                this.end.JointFound(toIndex);
            }
        }

        public string BuildSummary()
        {
            var summary = new List<string>();
            int solved_count = 0;
            int extra_joints = 0;
            var hot_entries = new HashSet<(int, bool)>();

            for (int s = 0; s < this.start.Count; ++s)
            for (int t = 0; t < this.end.Count; ++t)
            {
                if (this.Result.Stages[s, t] != null)
                    ++solved_count;
                else
                {
                    var has_joint = this.joints[s, t] != null;
                    if (has_joint)
                        ++extra_joints;
                    else
                    {
                        hot_entries.Add((s, true));
                        hot_entries.Add((t, false));
                    }

                    if (this.weightLimit != null)
                    {
                        summary.Add($"{(has_joint ? "J " : "")}{s}->{t} limit {this.weightLimit.Value.Limits[s, t]}; at start reach weight {this.start.GetBacktrack(s).MinReachWeight}; at end reach weight {this.end.GetBacktrack(t).MinReachWeight}");
                    }
                }
            }

            for (int s = 0; s < this.start.Count; ++s)
                if (hot_entries.Contains((s, true)))
                    this.start.GetBacktrack(s).DumpKml(Navigator.GetUniquePath(new Navigator().GetOutputDirectory(), $"dump-S-{s}.kml"));
            for (int t = 0; t < this.end.Count; ++t)
                if (hot_entries.Contains((t, false)))
                    this.end.GetBacktrack(t).DumpKml(Navigator.GetUniquePath(new Navigator().GetOutputDirectory(), $"dump-E-{t}.kml"));

            summary.Insert(0, $"{this.start.Count}x{this.end.Count} {(IsCompleted ? "" : "NOT ")}completed, {solved_count}/{this.start.Count * this.end.Count} stages resolved, {extra_joints} extra joints, source {this.start.GetSummary()}, target {this.end.GetSummary()}");

            return String.Join(Environment.NewLine, summary);
        }

        public bool CanExpandTo(int fromIndex,in Placement<TNodeId, TRoadId> currentPlace,
            in Placement<TNodeId, TRoadId> expandPlace, TotalWeight totalWeight)
        {
            bool fully_joined = (IsForwardSide ? this.start : this.end).IsFullyJoined(fromIndex);
            if (this.weightLimit == null && !fully_joined)
                return true;

            foreach (var (to_index, to_bucket, to_heap, to_backtrack) in OppositeFor(fromIndex))
            {
                JointInfo<TNodeId,TRoadId>? curr_joint = GetJoint(fromIndex, to_index);

                bool expand_found = to_backtrack.TryGetValue(Edge.STUB(expandPlace), out var to_info, out _);
                var joint_passes = curr_joint is not {} j 
                                   || (expand_found
                                   && to_backtrack.IsAtJointBoundary(
                                       // since we are hitting opposite backtrack, we need to get opposite timestamp
                                       j.GetTimestamp(forward: !IsForwardSide),
                                       to_info.Timestamp, Edge.STUB(currentPlace)));

                var weight_passes = weightPasses(fromIndex, to_index,
                    totalWeight,
                    oppositeBoundary: curr_joint == null && !expand_found
                        ? to_backtrack.MinReachWeight:Weight.Zero);

                if (weight_passes && joint_passes)
                    return true;
                else
                {
                    var (src_idx, dst_idx) = AbsoluteIndices(fromIndex, to_index);
#if DEBUG
                    if (DEBUG_SWITCH.Enabled && (src_idx, dst_idx) == DEBUG_SWITCH.ElemIndex)
                    {
                        if (this.weightLimit != null)
                        {
                            var limit = this.weightLimit.Value.Limits[src_idx, dst_idx];
                        }
                    }
#endif
                }
            }

            return false;
        }
        
        private bool weightPasses(int fromIndex, int toIndex,
            TotalWeight weight,
            Weight oppositeBoundary)
        {
            if (this.weightLimit == null)
                return true;
            (fromIndex, toIndex) = AbsoluteIndices(fromIndex, toIndex);
            var limit = this.weightLimit.Value.Limits[fromIndex, toIndex];
            TotalWeight min_weight = new TotalWeight(weight.Current,
                    weight.Remaining + oppositeBoundary);

            var passed = !this.weightComparer.IsLess(limit, min_weight);
            if (DEBUG_SWITCH.Enabled && (fromIndex, toIndex) == DEBUG_SWITCH.ElemIndex && !passed)
            {
            }

            return passed;
        }

    }
}