using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Mapping;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    public sealed class ChCover<TNodeId, TRoadId> : ICover<TNodeId, TRoadId>,IBasicCover<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly Dictionary<TNodeId, int> removed;
        private readonly Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>> shortcuts;

        internal ChCover(IWorldMap<TNodeId, TRoadId> map,
            Dictionary<TNodeId, int> removed,
            Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>> shortcuts)
        {
            this.map = map;
            this.removed = removed;
            this.shortcuts = shortcuts;
        }

        public bool CanUseShortcuts(TNodeId current, RoadBucket<TNodeId, TRoadId> startBucket, RoadBucket<TNodeId, TRoadId> endBucket)
        {
            return true;
        }

        public bool CanUseShortcuts(TNodeId current)
        {
            return true;
        }

        public IEnumerable<(TNodeId target, Weight weight)> GetShortcuts(TNodeId nodeId,
            bool isReversed)
        {
            if (this.shortcuts.TryGetValue(nodeId, out var connection))
            {
                return connection
                    .Where(it => it.Value.GetInfo(backReference: isReversed).HasValue)
                    .Select(it => (it.Key, Weight: it.Value.GetInfo(backReference: isReversed)!.Value.JoinedWeight));
            }
            else
                return ArraySegment<(TNodeId target, Weight weight)>.Empty;
        }

        public bool CanUseDirectAdjacent(TNodeId current, TNodeId adjacent)
        {
            return !this.removed.ContainsKey(adjacent);
        }

        public IReadOnlyList<StepRun<TNodeId, TRoadId>> GetSteps(TNodeId sourceNodeId, 
            TNodeId targetNodeId, bool isReversed)
        {
            BiShortcutInfo<TNodeId, TRoadId> bi_info = this.shortcuts[sourceNodeId][targetNodeId];
            
            if (bi_info.GetInfo(backReference:isReversed) is { } info)
                return info.Steps;
            else
                throw new ArgumentException($"There is no {(isReversed ? "reversed " : "")}shortcut between {map.GetOsmNodeId(sourceNodeId)} and {map.GetOsmNodeId(targetNodeId)}");
        }

        public bool CanUseDirectAdjacent(TNodeId current, TNodeId adjacent,
            RoadBucket<TNodeId, TRoadId> startBucket, RoadBucket<TNodeId, TRoadId> endBucket)
        {
            return CanUseDirectAdjacent(current, adjacent);
        }
    }
}