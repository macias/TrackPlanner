using System;
using System.Collections.Generic;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    internal sealed class BoundaryRegistry<TNodeId,TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly WeightComparer weightComparer;
        private readonly IReadOnlyBacktrack<TNodeId, TRoadId> backtrack;
        private readonly Dictionary<Placement<TNodeId,TRoadId>,int> outConnections;
        // target -> sources
        private readonly Dictionary<Placement<TNodeId,TRoadId>,
            HashSet<Placement<TNodeId,TRoadId>>> sources;

        public  Weight MinWeight { get; private set; }
        private Placement<TNodeId, TRoadId>? minPlace;

        public IEnumerable<Placement<TNodeId, TRoadId>> EdgePlaces => this.outConnections.Keys;
        
        public BoundaryRegistry(WeightComparer weightComparer,
            IReadOnlyBacktrack<TNodeId, TRoadId> backtrack)
        {
            this.weightComparer = weightComparer;
            this.backtrack = backtrack;
            this.MinWeight = Weight.Zero;
            this.minPlace = null;
            this.outConnections = new Dictionary<Placement<TNodeId,TRoadId>, int>();
            this.sources =  new Dictionary<Placement<TNodeId, TRoadId>, 
                HashSet<Placement<TNodeId, TRoadId>>>();
        }

        public void Register(in Placement<TNodeId,TRoadId> place, int oppositeReachableCount) 
        {
            this.outConnections.Add(place,oppositeReachableCount);
            if (!place.IsUserPoint)
            {
                if (!this.sources.TryGetValue(place, out var ss))
                    throw new Exception();
                foreach (var s in ss)//this.sources[place])
                {
                    if (--this.outConnections[s] == 0)
                    {
                        this.outConnections.Remove(s);
                        if (this.minPlace == s)
                        {
                            this.minPlace = null;
                        }
                    }
                }

                this.sources.Remove(place);
            }

            if (this.minPlace == null)
            {
                ;
                foreach (var edge in EdgePlaces)
                {
                    var (_,total_weight) = backtrack[Edge.STUB( edge)];
                    if (this.minPlace == null 
                        || this.weightComparer.IsLess(total_weight.Current, this.MinWeight))
                    {
                        this.minPlace = edge;
                        this.MinWeight = total_weight.Current;
                    }
                }

                ;
            }

            ;
        }

        public void AddSource(in Placement<TNodeId, TRoadId> targetPlace, 
            in Placement<TNodeId, TRoadId> currentPlace) 
        {
            if (!this.sources.TryGetValue(targetPlace, out var s))
            {
                s = new HashSet<Placement<TNodeId, TRoadId>>();
                this.sources.Add(targetPlace,s);
            }

            s.Add(currentPlace);
        }
    }
}