﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using TrackPlanner.Backend;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    public sealed class MultiStageFinder<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly ILogger logger;
        private readonly RouteLogic<TNodeId, TRoadId> logic;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly Dictionary<TNodeId, string> DEBUG_hotNodes;
        private readonly HashSet<TNodeId> DEBUG_dangerousNodes;

        private IGeoCalculator calc => this.map.Grid.Calc;
        private readonly UserRouterPreferences userConfig;
        private readonly FinderConfiguration finderConfig;

        private readonly Navigator navigator;
        private readonly RouteShaper<TNodeId, TRoadId> shaper;
        private readonly Speed fastest;

        internal MultiStageFinder(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            FinderConfiguration finderConfig,
            UserRouterPreferences userConfig,
            IReadOnlySet<TNodeId> suppressedHighTraffic)
        {
            this.logger = logger;
            this.shaper = new RouteShaper<TNodeId, TRoadId>(logger, map);
            this.navigator = navigator;
            this.map = map;
            this.finderConfig = finderConfig;
            this.userConfig = userConfig;

            this.DEBUG_dangerousNodes = new HashSet<TNodeId>();
            this.DEBUG_hotNodes = new Dictionary<TNodeId, string>()
            {
            };

            userConfig.CalculateSpeedLimits(out _, out this.fastest);
            this.logic = new RouteLogic<TNodeId, TRoadId>(map, calc, userConfig, suppressedHighTraffic);
        }

        internal MultiStage<TNodeId, TRoadId> TryFindStageRoute(
            DebugFinderHistory<TNodeId, TRoadId>? forwardDebugHistory,
            DebugFinderHistory<TNodeId, TRoadId>? backwardDebugHistory,
            IBasicCover<TNodeId, TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            TotalWeightComparer weightComparer,
            MultiWeight? weightLimit,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> startBuckets,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> endBuckets,
            string modeLabel,
            CancellationToken cancellationToken)
        {
            var result = tryFindRawRoute(forwardDebugHistory, backwardDebugHistory,
                coverGrid,
                connectionPredicate,
                weightComparer,
                weightLimit,
                startBuckets, endBuckets,
                modeLabel,
                cancellationToken);

            for (int s = 0; s < startBuckets.Count; ++s)
            for (int t = 0; t < endBuckets.Count; ++t)
            {
                var elem = result.Stages[s, t]!.Value;
                if (!elem.HasValue)
                    continue;

                var stage_steps = elem.Value.Steps.ToList();

                var shape_res = this.shaper.RemoveUserPoints(startBuckets[s], endBuckets[t], stage_steps);
                if (!shape_res.HasValue)
                    result.Stages[s, t] = Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding>
                        .Fail(LegacyRouteFinding.NotFound, shape_res.ProblemMessage);
                else
                {
                    var shape = new PathFinder.WeightSteps<TNodeId, TRoadId>(stage_steps, 
                        elem.Value.JoinedWeight,isDraft:false);

                    result.Stages[s, t] = Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding>
                        .Valid(shape, shape_res.ProblemMessage);
                }
            }

            return result;
        }

        private MultiStage<TNodeId, TRoadId> tryFindRawRoute(
            DebugFinderHistory<TNodeId, TRoadId>? forwardDebugHistory,
            DebugFinderHistory<TNodeId, TRoadId>? backwardDebugHistory,
            IBasicCover<TNodeId, TRoadId>? coverGrid,
            ValidConnectionPredicate<TNodeId, TRoadId>? connectionPredicate,
            TotalWeightComparer weightComparer,
            MultiWeight? weightLimit,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> startBuckets,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> endBuckets,
            string modeLabel,
            CancellationToken cancellationToken)
        {
            //  logger.Info($"Start at legal {stageStart.Any(it => this.map.GetRoad(it.BaseRoadIndex.RoadId).HasAccess)}, end at legal {stageEnd.Any(it => this.map.GetRoad(it.BaseRoadIndex.RoadId).HasAccess)}");

            var search_data = new MultiSearch<TNodeId, TRoadId>(this.map,weightComparer, weightLimit,
                startBuckets, endBuckets);


            while (!search_data.IsCompleted)
            {
                if (cancellationToken.IsCancellationRequested)
                    cancellationToken.ThrowCanceledException(search_data.BuildSummary());

                search_data.SwitchSide();

                foreach (var (from_index, from_bucket, from_heap, from_backtrack)
                         in search_data.Iterate())
                {
                    var popped = from_heap.TryPop(out Edge<TNodeId, TRoadId> current_edge,
                        out TotalWeight current_weight,
                        out BacktrackInfo<TNodeId, TRoadId> current_info);
                    if (!popped)
                    {
                        foreach (var (to_index, to_bucket, to_heap, to_backtrack)
                                 in search_data.OppositeFor(from_index))
                        {
                            var joint = search_data.GetJoint(from_index, to_index);
                            if (joint == null)
                            {
                                // this.logger.Info($"Finding path in {modeLabel} failed , fwd: {stats.ForwardUpdateCount}, bwd {stats.BackwardUpdateCount}");
                                search_data.FailFor(from_index, to_index);
                            }
                            else
                            {
                                var from_steps = FinderHelper.RecreatePath(this.map, coverGrid,
                                    from_backtrack,
                                    Placement<TNodeId, TRoadId>.UserPoint(from_bucket),
                                    joint.Value.ForwardEdge,
                                    isReversed: !search_data.IsForwardSide);
                                var to_steps = FinderHelper.RecreatePath(this.map, coverGrid,
                                    to_backtrack,
                                    Placement<TNodeId, TRoadId>.UserPoint(to_bucket),
                                    joint.Value.BackwardEdge,
                                    isReversed: search_data.IsForwardSide);
                                if (!search_data.IsForwardSide)
                                    (from_steps, to_steps) = (to_steps, from_steps);

                                if (DEBUG_SWITCH.Enabled)
                                {
                                    if (search_data.AbsoluteIndices(from_index, to_index) == DEBUG_SWITCH.ElemIndex)
                                    {
                                        ;
                                    }
                                }

                                // skip first place, because it is shared with the result
                                from_steps.AddRange(FinderHelper.ReverseSteps(to_steps).Skip(1));
                                if (from_steps.Count == 0)
                                    throw new ArgumentException("No steps computed");

                                search_data.SuccessFor(from_index, to_index, from_steps,
                                    weight: joint.Value.OBSOLETE_JoinedWeight.Current);
                            }

                            if (search_data.IsCompleted)
                                return search_data.Result;
                        }

                        continue;
                    }

                    current_info = from_backtrack.Add(current_edge, current_info, current_weight);

                    // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point

                    foreach (var (to_index, to_bucket, to_heap, to_backtrack)
                             in search_data.OppositeFor(from_index))
                    {
                        if (current_edge.Place.IsUserPoint
                            && current_edge.Place.Point.Convert2d() == to_bucket.UserPoint)
                        {
                            // we add user point flag to be sure we have such sequence -- user point, cross point, nodes...., cross point, user point
                            var route_steps = FinderHelper.RecreatePath(this.map, coverGrid,
                                from_backtrack,
                                Placement<TNodeId, TRoadId>.UserPoint(from_bucket),
                                current_edge, isReversed: !search_data.IsForwardSide);
                            if (route_steps.Count == 0)
                                throw new ArgumentException("No steps computed");
                            if (!search_data.IsForwardSide)
                                route_steps = FinderHelper.ReverseSteps(route_steps);

                            if (DEBUG_SWITCH.Enabled)
                            {
                                if (search_data.AbsoluteIndices(from_index, to_index) == DEBUG_SWITCH.ElemIndex)
                                {
                                    ;
                                }
                            }

                            search_data.SuccessFor(from_index, to_index, route_steps, current_weight.Current);

                            forwardDebugHistory?.DumpLastData();
                            backwardDebugHistory?.DumpLastData();

                            if (search_data.IsCompleted)
                                return search_data.Result;
                        }
                        else if (to_backtrack.TryGetValue(current_edge, out var opposite_info, out var opposite_weight))
                        {
                            var joint = search_data.GetJoint(from_index, to_index);

                            bool updated = search_data.UpdateJoint(from_index, to_index,
                                current_info, opposite_info,
                                current_edge.Place, current_weight,opposite_weight);

                            if (DEBUG_SWITCH.Enabled && joint is { } j)
                            {
                            }
                        }
                    }

                    if (coverGrid != null
                        && current_edge.Place.IsNode
                        && coverGrid.CanUseShortcuts(current_edge.Place.NodeId))
                        foreach (var (target, short_weight) in coverGrid
                                     .GetShortcuts(current_edge.Place.NodeId,
                                         isReversed: !search_data.IsForwardSide))
                        {
                            if (!(connectionPredicate?.Invoke(current_edge.Place.NodeId, target, null) ?? true))
                                continue;

                            var target_place = Edge.STUB( Placement<TNodeId, TRoadId>.Node(this.map,
                                Edge.INDEX<TNodeId,TRoadId>( target)));

                            if (from_backtrack.ContainsKey(target_place))
                            {
                                if (current_edge.Place.IsNode && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId,
                                        out string? comment))
                                {
                                    logger.Info(
                                        @$"Adjacent to hot node {current_edge.Place.NodeId}/{comment} 
is already used by outgoing road ? @{target_place.Place.NodeId}");
                                }

                                continue;
                            }

                            TotalWeight outgoingTotalWeight = new TotalWeight((current_weight.Current +
                                                                               short_weight),
                                remaining: Weight.Zero);

                            var passes = search_data.CanExpandTo(from_index,current_edge.Place,
                                target_place.Place,
                                outgoingTotalWeight);
                            if (!passes && DEBUG_SWITCH.Enabled)
                            {
                                ;
                            }

                            if (!passes)
                                continue;

                            {
                                var outgoing_info = new BacktrackInfo<TNodeId, TRoadId>(current_edge,
                                    incomingRoadId: null,
                                    incomingCondition: null,
                                    incomingDistance: null,
                                    incomingTime: null,
                                    isShortcut: true
                                    );

#if DEBUG
                                if (DEBUG_SWITCH.Enabled)
                                {
                                    var DEBUG_steps = coverGrid.GetSteps(current_edge.Place.NodeId,
                                        target_place.Place.NodeId, isReversed: !search_data.IsForwardSide);
                                }

#endif
                                bool updated = from_heap.TryAddOrUpdate(target_place,
                                    outgoingTotalWeight,
                                    outgoing_info);

                                {
                                    if (current_edge.Place.IsNode && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId, out string? comment))
                                    {
                                        //logger.Info($"Adjacent to hot node {current_place.NodeId}/{comment} is by outgoing road ? @ {target_place.NodeId}, {(step_info.IsForbidden ? "forbidden" : "")}, weight {outgoing_weight}, updated {updated}");
                                    }
                                }
                            }
                        }


                    var adj_predicate = connectionPredicate;
                    if (adj_predicate == null && coverGrid != null)
                    {
                        if (DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }

                        adj_predicate = (curr, adj, _) => coverGrid.CanUseDirectAdjacent(curr, adj);
                    }


                    foreach ((var target_edge, TRoadId connecting_road_id)
                             in this.map.GetAllAdjacent(calc, current_edge.Place, from_bucket,
                                 search_data.OppositeFor(from_index).Select(it => it.bucket),
                                 adj_predicate, preserveRoundabouts: this.finderConfig.PreserveRoundabouts))
                    {
                        {
                            RoadInfo<TNodeId> road_info = this.map.GetRoadInfo(connecting_road_id);
                            if (road_info.IsCyclingForbidden())
                            {
                                if (DEBUG_SWITCH.Enabled)
                                {
                                    //var osm = this.map.GetOsmRoadId(connecting_road_id);
                                    //var foo = (osm, this.map.GetOsmNodeId(target_place.NodeId));
                                    ;
                                }

                                continue;
                            }
                        }
#if DEBUG
                        if (current_edge.Place.DEBUG_Identifier == 1057)
                        {
                            ;
                        }
#endif

                        if (from_backtrack.TryGetValue(target_edge, out var debug_info, out _))
                        {
                            if (current_edge.Place.IsNode 
                                && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId, out string? comment))
                            {
                                logger.Info(
                                    @$"Adjacent to hot node {current_edge.Place.NodeId}/{comment} 
is already used by outgoing road {connecting_road_id}@{target_edge.Place.NodeId}");
                            }

                            continue;
                        }

                        MoveInfo step_info = this.logic.GetFullStepInfo(connecting_road_id,
                            current_edge.ProjectedSource,
                            current_edge.Place, target_edge.Place, reversed: !search_data.IsForwardSide);

                        TotalWeight outgoingTotalWeight = new TotalWeight(
                            (current_weight.Current + step_info.Weight),
                            remaining: Weight.Zero);

                        var passes = search_data.CanExpandTo(from_index,current_edge.Place, 
                            target_edge.Place,
                            outgoingTotalWeight);
                        if (!passes && DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }

                        if (!passes)
                            continue;
                        {
                            var outgoing_info = new BacktrackInfo<TNodeId, TRoadId>(current_edge,
                                connecting_road_id,
                                step_info.GetCondition(),
                                step_info.SegmentLength,
                                step_info.Time,
                                isShortcut: false
                                );

                            bool updated = from_heap.TryAddOrUpdate(target_edge, outgoingTotalWeight, outgoing_info);

                            {
                                if (current_edge.Place.IsNode 
                                    && DEBUG_hotNodes.TryGetValue(current_edge.Place.NodeId, out string? comment))
                                {
                                    logger.Info(
                                        @$"Adjacent to hot node {current_edge.Place.NodeId}/{comment} is 
by outgoing road {connecting_road_id}@{target_edge.Place.NodeId}, {(step_info.IsForbidden ? "forbidden" : "")}, weight {outgoingTotalWeight}, updated {updated}");
                                }
                            }
                        }
                    }

                }
            }

            return search_data.Result;
        }
    }
}