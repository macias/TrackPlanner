using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    public sealed class Preprocessor2<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private static readonly IReadOnlySet<TNodeId> emptySuppressedTraffic = new HashSet<TNodeId>();

        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly IGeoCalculator calc;
        private readonly FinderConfiguration finderConfig;
        private readonly UserRouterPreferences userConfig;
        private readonly WeightComparer weightComparer;
        private readonly Dictionary<TNodeId, int> removed_nodes;
        private readonly ChCover<TNodeId, TRoadId> ch_cover;
        private readonly Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>> shortcuts;
        private readonly int allNodesCount;
        private readonly double startTime;

        public Preprocessor2(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            IGeoCalculator calc,
            FinderConfiguration finderConfig, UserRouterPreferences userConfig)
        {
            this.logger = logger;
            this.navigator = navigator;
            this.map = map;
            this.calc = calc;
            this.finderConfig = finderConfig;

            this.userConfig = userConfig.DeepClone();
            this.userConfig.TrafficSuppression = Length.Zero;
            this.userConfig.SnapCyclewaysToRoads = Length.Zero;

            this.weightComparer =  WeightComparer.Instance;

            this.removed_nodes = new Dictionary<TNodeId, int>();
            this.shortcuts = new Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>>();

            this.ch_cover = new ChCover<TNodeId, TRoadId>(this.map, removed_nodes, shortcuts);

            this.allNodesCount = this.map.GetAllNodes().Count();

            this.startTime = Stopwatch.GetTimestamp();

            foreach (var ((node_id, point), idx) in this.map.GetAllNodes().ZipIndex())
                calculate(idx, node_id);
        }

        private void updateShortcuts(TNodeId sourceNodeId, TNodeId targetNodeId,
            in WeightSteps<TNodeId, TRoadId> info, bool backReference)
        {
            if (backReference)
                (sourceNodeId, targetNodeId) = (targetNodeId, sourceNodeId);

            if (!shortcuts.TryGetValue(sourceNodeId, out var connection))
            {
                connection = new Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>();
                shortcuts.Add(sourceNodeId, connection);
            }

            if (!connection.TryGetValue(targetNodeId, out var existing))
                existing = new BiShortcutInfo<TNodeId, TRoadId>(null, null);

            if (existing.GetInfo(backReference) is not { } curr_info
                || weightComparer.IsLess(info.JoinedWeight, curr_info.JoinedWeight))
            {
                connection[targetNodeId] = existing.With(info, backReference);
            }
        }


        Weight computeWeight(TNodeId src, TNodeId dst, IReadOnlySet<TNodeId> allowed)
        {
            var finding = RouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                this.map,
                coverGrid: ch_cover,
                connectionPredicate: (_, adj, _) => allowed.Contains(adj),
                weightLimit: null,
                this.finderConfig,
                this.userConfig, new[] {src, dst},
                allowSmoothing: false,
                allowGap:false,
                CancellationToken.None);
            if (!finding.HasValue)
                throw new Exception($"Could not find a model route {finding}. {shortcuts!.Count} shortcuts");

            if (finding.ProblemMessage is {} problem)
                throw new Exception($"{problem} for #{this.map.GetOsmNodeId(src)} to #{this.map.GetOsmNodeId(dst)}");

            
            return finding.Value.RouteWeight;
        }


        private void calculate(int idx, TNodeId node_id)
        {
            if (!this.map.IsRoadCrossroad(node_id))
                return;

            if (idx % 10 == 0)
            {
                TimeSpan eta = TimeSpan.Zero;
                double speed_ms = 0;
                var passed = (Stopwatch.GetTimestamp() - startTime) / Stopwatch.Frequency;
                if (idx > 0)
                {
                    eta = TimeSpan.FromSeconds((allNodesCount - idx) * passed / idx);
                    speed_ms = (passed / idx)*1000;
                }

                this.logger.Info($"Preprocessed {idx}/{allNodesCount} nodes in {DataFormat.Format(TimeSpan.FromSeconds(passed))} ({speed_ms:0.###}ms), ETA {DataFormat.Format(eta)}");
            }

            var nearby = new Dictionary<TNodeId, NearbyInfo2<TNodeId>>();
            {
                foreach (var conn in map.GetNearbyCrossroads(node_id))
                {
                    var id = this.map.GetNode(conn.RoadIndex);
                    if (removed_nodes.ContainsKey(id))
                        continue;

                    if (!nearby.TryGetValue(id, out var info))
                    {
                        info = new NearbyInfo2<TNodeId>()
                        {
                            Allowed = new HashSet<TNodeId>() {id, node_id},
                            Role = NearbyRole.Source | NearbyRole.Target,
                        };
                        nearby.Add(id, info);
                    }

                    info.Allowed.AddRange(conn.Trace);
                }

                if (shortcuts.TryGetValue(node_id, out var sc))
                    foreach (var (id, conn) in sc)
                    {
                        if (removed_nodes.ContainsKey(id))
                            continue;

                        if (!nearby.TryGetValue(id, out var info))
                        {
                            info = new NearbyInfo2<TNodeId>()
                            {
                                Allowed = new HashSet<TNodeId>() {node_id, id},
                            };
                            nearby.Add(id, info);
                        }

                        if (conn.Forward.HasValue)
                            info = info with {Role = info.Role | NearbyRole.Target};
                        if (conn.Backward.HasValue)
                            info = info with {Role = info.Role | NearbyRole.Source};

                        nearby[id] = info;
                    }
            }

            var nodes = nearby.Keys.ToArray();
            foreach (var (source_node,source_idx) in nodes.ZipIndex())
            {
                var source_info = nearby[source_node];
                foreach (var (target_node,target_idx) in nodes.ZipIndex())
                {
                    DEBUG_SWITCH.Enabled = idx == 34 && source_idx == 0 && target_idx == 1;
                    if (DEBUG_SWITCH.Enabled)
                    {
                        ;
                    }

                    var target_info = nearby[target_node];

                    if (EqualityComparer<TNodeId>.Default.Equals(source_node, target_node))
                        continue;
                    if (!source_info.Role.HasFlag(NearbyRole.Source) || !target_info.Role.HasFlag(NearbyRole.Target))
                        continue;

                    var limit = computeWeight(source_node, target_node, source_info.Allowed.Concat(target_info.Allowed).ToHashSet());

                    if (DEBUG_SWITCH.Enabled)
                    {
                        ;
                    }
                    var finding = RouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                        this.map,
                        coverGrid: ch_cover,
                        connectionPredicate: null,
                        weightLimit: limit,
                        this.finderConfig,
                        this.userConfig, new[] {source_node, target_node},
                        allowSmoothing: false,
                        allowGap:false,
                        CancellationToken.None);
                    if (!finding.HasValue)
                        throw new Exception($"Iteration {idx}-{source_idx}-{target_idx}: could not find a shortcut route {finding}. {shortcuts.Count} shortcuts");

                    if (finding.ProblemMessage is {} problem)
                        throw new Exception($"{idx}-{source_idx}-{target_idx}: {problem} for #{this.map.GetOsmNodeId(source_node)} to #{this.map.GetOsmNodeId(target_node)}");

                    if (finding.Value.RouteLegs.SelectMany(it => it.Steps)
                        .Where(it => it.Place.IsNode)
                        .Select(it => it.Place.NodeId)
                        .Any(it => EqualityComparer<TNodeId>.Default.Equals(node_id, it)))
                    {
                        var info = new WeightSteps<TNodeId, TRoadId>(
                            finding.Value.RouteLegs.SelectMany(it => it.Steps).ToList(),
                            finding.Value.RouteWeight,
                            isDraft:false
                        );

                        updateShortcuts(source_node, target_node, info, backReference: false);
                        updateShortcuts(source_node, target_node, info, backReference: true);
                    }
                }
            }

            removed_nodes.Add(node_id, removed_nodes.Count);
        }
    }
}