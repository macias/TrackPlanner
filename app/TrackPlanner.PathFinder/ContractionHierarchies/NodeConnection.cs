using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{



    internal readonly record struct NodeConnection<TNodeId>(TNodeId NodeId,
        Weight? ForwardWeight, // going to node
        Weight? BackwardWeight) // coming back from node
        where TNodeId : struct
    {
        public static NodeConnection<TNodeId> Create<TRoadId>(IWorldMap<TNodeId, TRoadId> map, HubConnection<TRoadId> hub)
            where TRoadId : struct
        {
            return new NodeConnection<TNodeId>(map.GetNode(hub.RoadIndex), hub.ForwardWeight, hub.BackwardWeight);
        }

        public static NodeConnection<TNodeId> Create<TRoadId>(in KeyValuePair<TNodeId, BiShortcutInfo<TNodeId, TRoadId>> conn)
            where TRoadId : struct
        {
            Weight? forward_weight = null;
            if (conn.Value.Forward is { } f)
                forward_weight = f.JoinedWeight;
            Weight? backward_weight = null;
            if (conn.Value.Backward is { } b)
                backward_weight = b.JoinedWeight;
            return new NodeConnection<TNodeId>(conn.Key, forward_weight, backward_weight);
        }
    }
}