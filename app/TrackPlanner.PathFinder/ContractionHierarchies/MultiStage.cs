﻿using TrackPlanner.Shared;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    
    internal readonly record struct MultiStage<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding>?[,] Stages { get; }

        public MultiStage(int sources, int targets)
        {
            Stages = new Result<PathFinder.WeightSteps<TNodeId, TRoadId>, LegacyRouteFinding>?[sources, targets];
        }
    }
}