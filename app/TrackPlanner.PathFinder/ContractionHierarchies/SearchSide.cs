﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackRadar.Collections;
using TrackPlanner.Mapping;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    internal sealed class SearchSide<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        
        private readonly IReadOnlyList<RoadBucket<TNodeId, TRoadId>> buckets;
        private readonly SearchData<TNodeId, TRoadId>[] searchData;
        private readonly int[] receivers;
        private readonly int[] joints;

        private bool isElementCompleted(int index)
        {
            return  this.receivers[index] == 0;
        }
        private bool isEmpty(int index)
        {
            return this.searchData[index].IsEmpty || isElementCompleted(index);
        }

        public bool IsFullyJoined(int index)
        {
            return this.joints[index] == 0;
        }

        public bool IsCompleted => Enumerable.Range(0, buckets.Count).All(isElementCompleted);
        public bool IsEmpty => Enumerable.Range(0, buckets.Count).All(isEmpty);
        public int Count => this.buckets.Count;

        public SearchSide(IWorldMap<TNodeId, TRoadId> map,TotalWeightComparer weightComparer,
            IReadOnlyList<RoadBucket<TNodeId, TRoadId>> buckets,
            int oppositeCount)
        {
            this.buckets = buckets;
            this.searchData = new SearchData<TNodeId, TRoadId>[buckets.Count];
            this.receivers = new int[buckets.Count];
            this.joints = new int[buckets.Count];
            for (int i = 0; i < buckets.Count; ++i)
            {
                this.searchData[i] = new SearchData<TNodeId, TRoadId>(map,weightComparer, buckets[i]);
                this.receivers[i] = oppositeCount;
                this.joints[i] = oppositeCount;
            }
        }

        public IEnumerable<(int index, RoadBucket<TNodeId, TRoadId> bucket,
                MappedPairingHeap<Edge<TNodeId, TRoadId>, TotalWeight, BacktrackInfo<TNodeId, TRoadId>> heap,
                Backtrack<TNodeId, TRoadId> backtrack)>
            Iterate()
        {
            for (int i = 0; i < this.buckets.Count; ++i)
            {
                if (isElementCompleted(i))
                    continue;

                var d = this.searchData[i];
                yield return (index: i, this.buckets[i], d.Heap, d.Backtrack);
            }
        }
        
        public Backtrack<TNodeId, TRoadId> GetBacktrack(int index)
        {
            return this.searchData[index].Backtrack;
        }

        public void DropReceiverFor(int index)
        {
            if (--this.receivers[index] < 0)
                throw new InvalidOperationException();
        }

        public void JointFound(int index)
        {
            if (--this.joints[index] < 0)
                throw new InvalidOperationException();
        }

        public string GetSummary()
        {
            return $"{this.receivers.Sum()} receivers, {this.joints.Sum()} joints, {this.searchData.Count(it => it.IsEmpty)} exhausted, {this.searchData.Sum(it =>it.Heap.Count)} heaps, {this.searchData.Sum(it => it.Backtrack.Count)} backtracks";
        }
    }
}