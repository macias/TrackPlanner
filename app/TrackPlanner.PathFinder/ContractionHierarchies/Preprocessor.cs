using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Backend;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Overlay;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    public sealed class Preprocessor<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly ILogger logger;
        private readonly Navigator navigator;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly IGeoCalculator calc;
        private readonly FinderConfiguration finderConfig;
        private readonly UserRouterPreferences userConfig;
        private readonly WeightComparer weightComparer;

        public Preprocessor(ILogger logger, Navigator navigator, IWorldMap<TNodeId, TRoadId> map,
            IGeoCalculator calc,
            FinderConfiguration finderConfig,UserRouterPreferences userConfig)
        {
            this.logger = logger;
            this.navigator = navigator;
            this.map = map;
            this.calc = calc;
            this.finderConfig = finderConfig;
            
            this.userConfig = userConfig.DeepClone();
            this.userConfig.TrafficSuppression= Length.Zero;
            this.userConfig.SnapCyclewaysToRoads = Length.Zero;

          this.weightComparer =  WeightComparer.Instance;
        }

        private void updateShortcuts(Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>> shortcuts,
            TNodeId sourceNodeId,TNodeId targetNodeId,
            in WeightSteps<TNodeId, TRoadId> info,bool backReference)
        {
            if (backReference)
                (sourceNodeId, targetNodeId) = (targetNodeId, sourceNodeId);
                
            if (!shortcuts.TryGetValue(sourceNodeId, out var connection))
            {
                connection = new Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>();
                shortcuts.Add(sourceNodeId, connection);
            }

            if (!connection.TryGetValue(targetNodeId, out var existing))
                existing = new BiShortcutInfo<TNodeId, TRoadId>(null, null);
                            
            if (existing.GetInfo(backReference) is not {} curr_info 
                || weightComparer.IsLess(info.JoinedWeight, curr_info.JoinedWeight))
            {
                connection[targetNodeId] = existing.With(info, backReference);
            }
        }
        
        public void Calculate()
        {
            IReadOnlySet<TNodeId> empty_suppressed_traffic = new HashSet<TNodeId>();

            var removed_nodes = new Dictionary<TNodeId,int>();
            var shortcuts = new Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>>();
            
            var ch_cover = new ChCover<TNodeId, TRoadId>(this.map,removed_nodes, shortcuts);
            var logic = new RouteLogic<TNodeId, TRoadId>(this.map, this.calc, this.userConfig, 
                empty_suppressed_traffic);

            var count = this.map.GetAllNodes().Count();

            double start = Stopwatch.GetTimestamp();
            
            foreach (var ((node_id, point),idx) in this.map.GetAllNodes().ZipIndex())
            {
                if (!this.map.IsRoadCrossroad(node_id))
                    continue;
                
                if (idx % 10 == 0)
                {
                    TimeSpan eta = TimeSpan.Zero;
                    double speed = 0;
                    var passed = (Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency;
                    if (idx > 0)
                    {
                        eta = TimeSpan.FromSeconds( (count - idx) * passed / idx);
                        speed = passed / idx;
                    }

                    this.logger.Info($"Preprocessed {idx}/{count} nodes in {DataFormat.Format(TimeSpan.FromSeconds(passed))} ({speed:0.#}s), ETA {DataFormat.Format(eta)}");
                }

                NodeConnection<TNodeId>[] adjacent;
                {
                    var dict = new Dictionary<TNodeId, NodeConnection<TNodeId>>();
                    foreach (var conn in map.EXPERIMENTAL_GetNearbyHubs(logic, node_id)
                                 .Select(it => NodeConnection<TNodeId>.Create(this.map, it))
                                 .Concat(shortcuts.TryGetValue(node_id, out var shortcut)
                                     ? shortcut.Select(it => NodeConnection<TNodeId>.Create(it))
                                     : ArraySegment<NodeConnection<TNodeId>>.Empty)
                                 .Where(it => !removed_nodes.ContainsKey(it.NodeId)))
                    {
                        if (!dict.TryGetValue(conn.NodeId,out var existing))
                            dict.Add(conn.NodeId,conn);
                        else
                        {
                            if (existing.ForwardWeight == null ||
                                (conn.ForwardWeight.HasValue && this.weightComparer.IsLess(conn.ForwardWeight.Value, existing.ForwardWeight.Value)))
                                existing = existing with {ForwardWeight = conn.ForwardWeight};
                            if (existing.BackwardWeight == null ||
                                (conn.BackwardWeight.HasValue && this.weightComparer.IsLess(conn.BackwardWeight.Value, existing.BackwardWeight.Value)))
                                existing = existing with {BackwardWeight = conn.BackwardWeight};
                            
                            dict[conn.NodeId] = existing;
                        }
                    }

                    adjacent = dict.Values.ToArray();
                }

                for (int s=0;s<adjacent.Length;++s)
                for (int t = 0; t < adjacent.Length; ++t)
                {
                    if (s==t)
                        continue;
                    
                    var source = adjacent[s];
                    var target = adjacent[t];

                    if (EqualityComparer<TNodeId>.Default.Equals(source.NodeId, target.NodeId))
                    {
                        throw new ArgumentException($"We have double connection from {this.map.GetOsmNodeId(node_id)} to {this.map.GetOsmNodeId(target.NodeId)}");
                    }

                    var finding = RouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                        this.map,
                        coverGrid: ch_cover, 
                        connectionPredicate:null,
                        weightLimit:
                        (adjacent[s].BackwardWeight!.Value+ adjacent[t].ForwardWeight!.Value),
                        this.finderConfig,
                        this.userConfig, new[] {source.NodeId, target.NodeId},
                        allowSmoothing: false,
                        allowGap:false,
                        CancellationToken.None);
                    if (!finding.HasValue)
                        throw new Exception($"Could not find a route {finding}. {shortcuts.Count} shortcuts");

                    if (finding.ProblemMessage is {} problem)
                        throw new Exception($"{problem} for #{this.map.GetOsmNodeId(source.NodeId)} to #{this.map.GetOsmNodeId(target.NodeId)}");

                    if (finding.Value.RouteLegs.SelectMany(it => it.Steps)
                        .Where(it => it.Place.IsNode)
                        .Select(it => it.Place.NodeId)
                        .Any(it => EqualityComparer<TNodeId>.Default.Equals(node_id, it)))
                    {
                        var info = new WeightSteps<TNodeId, TRoadId>(
                            finding.Value.RouteLegs.SelectMany(it => it.Steps).ToList(),
                            finding.Value.RouteWeight,
                            isDraft:false
                        );
                        
                        updateShortcuts(shortcuts, source.NodeId,target.NodeId,info,backReference:false);
                        updateShortcuts(shortcuts, source.NodeId,target.NodeId,info,backReference:true);
                    }
                    
                    
                }
                
                removed_nodes.Add(node_id,removed_nodes.Count);
            }
        }
        
        
        
            public void Calculate2()
        {
            IReadOnlySet<TNodeId> empty_suppressed_traffic = new HashSet<TNodeId>();

            var removed_nodes = new Dictionary<TNodeId,int>();
            var shortcuts = new Dictionary<TNodeId, Dictionary<TNodeId, BiShortcutInfo<TNodeId, TRoadId>>>();
            
            var ch_cover = new ChCover<TNodeId, TRoadId>(this.map,removed_nodes, shortcuts);

            var count = this.map.GetAllNodes().Count();

            double start = Stopwatch.GetTimestamp();

            Weight compute_weight(TNodeId src,TNodeId dst, IReadOnlySet<TNodeId> allowed)
            {
                var finding = RouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                    this.map,
                    coverGrid: ch_cover, 
                    connectionPredicate: (_,adj,_)=>allowed.Contains(adj),
                    weightLimit:null,
                    this.finderConfig,
                    this.userConfig, new[] {src, dst},
                    allowSmoothing: false,
                    allowGap:false,
                    CancellationToken.None);
                if (!finding.HasValue)
                    throw new Exception($"Could not find a model route {finding}. {shortcuts!.Count} shortcuts");

                if (finding.ProblemMessage is {} problem)
                    throw new Exception($"{problem} for #{this.map.GetOsmNodeId(src)} to #{this.map.GetOsmNodeId(dst)}");

                return finding.Value.RouteWeight;
            }


            foreach (var ((node_id, point), idx) in this.map.GetAllNodes().ZipIndex())
            {
                if (!this.map.IsRoadCrossroad(node_id))
                    continue;

                if (idx % 10 == 0)
                {
                    TimeSpan eta = TimeSpan.Zero;
                    double speed = 0;
                    var passed = (Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency;
                    if (idx > 0)
                    {
                        eta = TimeSpan.FromSeconds((count - idx) * passed / idx);
                        speed = passed / idx;
                    }

                    this.logger.Info($"Preprocessed {idx}/{count} nodes in {DataFormat.Format(TimeSpan.FromSeconds(passed))} ({speed:0.#}s), ETA {DataFormat.Format(eta)}");
                }

                var nearby = new Dictionary<TNodeId, NearbyInfo<TNodeId>>();
                {
                    foreach (var conn in map.GetNearbyCrossroads(node_id))
                    {
                        var id = this.map.GetNode(conn.RoadIndex);
                        if (removed_nodes.ContainsKey(id))
                            continue;

                        if (!nearby.TryGetValue(id, out var info))
                        {
                            info = new NearbyInfo<TNodeId>()
                            {
                                Allowed = new HashSet<TNodeId>(){id,node_id},
                                Role = NearbyRole.Source | NearbyRole.Target,
                            };
                            nearby.Add(id, info);
                        }

                        info.Allowed.AddRange(conn.Trace);
                    }

                    if (shortcuts.TryGetValue(node_id, out var sc))
                        foreach (var (id, conn) in sc!)
                        {
                            if (removed_nodes.ContainsKey(id))
                                continue;

                            if (!nearby.TryGetValue(id, out var info))
                            {
                                info = new NearbyInfo<TNodeId>()
                                {
                                    Allowed = new HashSet<TNodeId>(){node_id,id},
                                };
                                nearby.Add(id, info);
                            }

                            if (conn.Forward.HasValue)
                                info = info with {Role = info.Role | NearbyRole.Target};
                            if (conn.Backward.HasValue)
                                info = info with {Role = info.Role | NearbyRole.Source};
                            
                            nearby[id] = info;
                        }
                }

                var nodes = nearby.Keys.ToArray();
                foreach (var s in nodes)
                {
                    var source = nearby[s];
                foreach (var t in nodes)
                {
                    var target = nearby[t];
                    
                    if (EqualityComparer<TNodeId>.Default.Equals(s, t))
                        continue;
                    if (!source.Role.HasFlag(NearbyRole.Source) || !target.Role.HasFlag(NearbyRole.Target))
                        continue;

                    /*if (!source.BackwardWeight.HasValue)
                    {
                        source = source with {BackwardWeight = compute_weight(s, node_id, source.Allowed)};
                        nearby[s] = source;
                    }
                    if (!target.ForwardWeight.HasValue)
                    {
                        target= target with {ForwardWeight = compute_weight(node_id, t, target.Allowed)};
                        nearby[t] = target;
                    }*/
                    var limit = compute_weight(s, t,source.Allowed.Concat( target.Allowed).ToHashSet());

                        var finding = RouteFinder<TNodeId, TRoadId>.TryFindRouteByNodes(this.logger, this.navigator,
                            this.map,
                            coverGrid: ch_cover,
                            connectionPredicate: null,
                            weightLimit: limit,
                            this.finderConfig,
                            this.userConfig, new[] {s, t},
                            allowSmoothing: false,
                            allowGap:false,
                            CancellationToken.None);
                    if (!finding.HasValue)
                        throw new Exception($"Iteration {idx}: could not find a shortcut route {finding}. {shortcuts.Count} shortcuts");

                    if (finding.ProblemMessage is {} problem)
                        throw new Exception($"{problem} for #{this.map.GetOsmNodeId(s)} to #{this.map.GetOsmNodeId(t)}");

                    if (finding.Value.RouteLegs.SelectMany(it => it.Steps)
                        .Where(it => it.Place.IsNode)
                        .Select(it => it.Place.NodeId)
                        .Any(it => EqualityComparer<TNodeId>.Default.Equals(node_id, it)))
                    {
                        var info = new WeightSteps<TNodeId, TRoadId>(
                        
                            finding.Value.RouteLegs.SelectMany(it => it.Steps).ToList(),
                            finding.Value.RouteWeight,
                             isDraft:false
                        );

                        updateShortcuts(shortcuts, s, t, info, backReference: false);
                        updateShortcuts(shortcuts, s, t, info, backReference: true);
                    }


                }
            }

            removed_nodes.Add(node_id,removed_nodes.Count);
            }
        }
    }
}