namespace TrackPlanner.PathFinder.ContractionHierarchies
{
    internal readonly record struct BiShortcutInfo<TNodeId, TRoadId>(WeightSteps<TNodeId, TRoadId>? Backward, WeightSteps<TNodeId, TRoadId>? Forward)
        where TNodeId : struct
        where TRoadId : struct
    {
        public WeightSteps<TNodeId, TRoadId>? GetInfo(bool backReference) => backReference ? Backward : Forward;

        public BiShortcutInfo<TNodeId, TRoadId> With(WeightSteps<TNodeId, TRoadId>? info, 
            bool backReference)
        {
            if (backReference)
                return this with {Backward = info};
            else
                return this with {Forward = info};
        }
    }
}