using System.Collections.Generic;

namespace TrackPlanner.PathFinder.ContractionHierarchies
{


    internal readonly record struct NearbyInfo<TNodeId>(NearbyRole Role,
        HashSet<TNodeId> Allowed,
        Weight? ForwardWeight, // going to node
        Weight? BackwardWeight) // coming back from node
    {
    }
    internal readonly record struct NearbyInfo2<TNodeId>(NearbyRole Role,
        HashSet<TNodeId> Allowed) 
    {
    }
}