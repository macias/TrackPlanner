﻿using System;

namespace TrackPlanner.PathFinder
{
    [Flags]
    public enum PlaceKind : ushort
    {
        None = 0,
        Prestart = 1,
        UserPoint = 2,
        CrossPoint = 4,

        Node = 8,

        RoundaboutCenter = 16,
        
        Segment = 32, // apply to crosspoints,
        RoundaboutLink =64,
        RoundaboutBounce = 128,
        
        Draft = 256,
    }


}
