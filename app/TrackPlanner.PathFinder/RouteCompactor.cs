using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder
{
    public partial class RouteCompactor<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly ILogger logger;
        private readonly IWorldMap<TNodeId, TRoadId> map;
        private readonly UserRouterPreferences userPrefs;
        private readonly bool compactPreservesRoads;
        private readonly ApproximateCalculator calc;
        private readonly RouteShaper<TNodeId,TRoadId> shaper;
        private readonly RouteHelper<TNodeId,TRoadId> helper;

        public RouteCompactor(ILogger logger, IWorldMap<TNodeId, TRoadId> map, UserRouterPreferences userPrefs,
            bool compactPreservesRoads)
        {
            this.logger = logger;
            this.map = map;
            this.userPrefs = userPrefs;
            this.compactPreservesRoads = compactPreservesRoads;
            this.calc = new ApproximateCalculator();
            this.shaper = new RouteShaper<TNodeId, TRoadId>(logger, map);

            this.helper = new RouteHelper<TNodeId, TRoadId>(logger, map,userPrefs.CheckpointIntervalLimit, compactPreservesRoads);
        }

        public RoutePlan<TNodeId, TRoadId> Compact<TReqPoint>(IReadOnlyList<TReqPoint> requestPoints,
            List<LegFlow<TNodeId, TRoadId>> legs,
            CancellationToken token,out ContextMessage? problem)
            where TReqPoint:IRequestPoint
        {
            problem = null;
            var plan = this.helper.SplitLegs(requestPoints, legs,
                token);

            if (this.userPrefs.CompactingDistanceDeviation == Length.Zero
                || this.userPrefs.CompactingAngleDeviation == Angle.Zero)
                this.logger.Verbose("Skipping simplification");
            else
            {
                int removed = 0;
                foreach (var leg in plan.Legs)
                {
                    foreach (var fragment in leg.Fragments)
                    {
                        if (!leg.IsDrafted)
                            removed += simplifySegment(fragment);
                        problem ??= fragment.Validate();
                    }
                }

                //this.logger.Verbose($"{removed} nodes removed when simplifying route with {this.userPrefs.CompactingAngleDeviation}, {this.userPrefs.CompactingDistanceDeviation}m.");
            }

            //this.logger.Info($"{plan.Legs.SelectMany(l => l.AllSteps()).Count()} points on the track.");

            return plan;
        }

        private int simplifySegment(LegFragment<TNodeId, TRoadId> fragment)
        {
            int removed_count = 0;
            for (int start_idx = 0; start_idx < fragment.GetSteps().Count - 2; ++start_idx)
            {
                int end_idx;
                // we cannot remove the ending point
                for (end_idx = start_idx + 2; end_idx < fragment.GetSteps().Count - 1; ++end_idx)
                {
                    Angle base_bearing = this.calc.GetBearing(fragment.GetSteps()[start_idx].Point,
                        fragment.GetSteps()[end_idx].Point);

                    for (int i = start_idx; i < end_idx; ++i)
                    {
                        Angle curr_bearing = this.calc.GetBearing(fragment.GetSteps()[i].Point,
                            fragment.GetSteps()[i + 1].Point);

                        if (this.calc.GetAbsoluteBearingDifference(curr_bearing, base_bearing) > this.userPrefs.CompactingAngleDeviation)
                        {
                            goto END_IDX_LOOP;
                        }
                    }

                    for (int i = start_idx + 1; i < end_idx; ++i) // the loop ranges are different than above
                    {
                        (Length dist, _, _) = this.calc.GetFlatDistanceToArcSegment(fragment.GetSteps()[i].Point, 
                            fragment.GetSteps()[end_idx].Point,
                            fragment.GetSteps()[start_idx].Point,stable:false);

                        if (dist > this.userPrefs.CompactingDistanceDeviation)
                        {
                            goto END_IDX_LOOP;
                        }
                    }
                }

                END_IDX_LOOP: ;

                // in both cases we have to decrease end_idx -- either we broke through the Count limit or we failed
                // to stick within the limits for it (but previous value was OK)
                --end_idx;

                int count = end_idx - start_idx - 1;
                if (count > 0)
                {
                    var dist_replacement = fragment.GetSteps().Skip(start_idx + 1).Take(count + 1).Select(it => it.IncomingFlatDistance).Sum();
                    fragment.Steps.RemoveRange(start_idx + 1, count);
                    fragment.Steps[start_idx + 1] = fragment.GetSteps()[start_idx + 1] with
                    {
                        IncomingFlatDistance = dist_replacement
                    };
                    removed_count += count;
                }
            }

            return removed_count;
        }



    }
}