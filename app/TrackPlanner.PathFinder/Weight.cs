﻿using MathUnit;
using System;

namespace TrackPlanner.PathFinder
{
    public readonly record struct Weight// : IEquatable<Weight>
    {
        public static Weight CreateForbidden(Length segmentLength, bool isForbidden, bool isSnap)
        {
            return Create( isForbidden && !isSnap ? segmentLength : Length.Zero,
                TimeSpan.Zero, costScale:1);
        }

        public static Weight FromCost(TimeSpan time, double costScale)
        {
            return Create(Length.Zero, time, costScale);
        }
        public static Weight FromCost(Length distance, Speed speed)
        {
            return Create(Length.Zero, distance, speed);
        }
        public static Weight Create(Length forbiddenDistance,Length distance, Speed speed)
        {
            return Create(forbiddenDistance, distance / speed, 1.0);
        }
        public static Weight Create(Length forbiddenDistance, TimeSpan time, double costScale)
        {
            if (time < TimeSpan.Zero)
                throw new ArgumentOutOfRangeException($"{nameof(time)} = {time}");
            if (costScale < 1)
                throw new ArgumentOutOfRangeException($"{nameof(costScale)} = {costScale}");

            return new Weight((uint) Math.Round(forbiddenDistance.Millimeters),
                (uint)Math.Round(time.TotalMilliseconds * costScale));
        }

        public static Weight Zero { get; } = new Weight(0, 0);
        public static Weight MaxValue { get; } = new Weight(uint.MaxValue, uint.MaxValue);

        private readonly uint timeCostMs;
        public double TimeCost => this.timeCostMs;
        
        // for forbidden parts it is better to use length instead of time, because if we pick such route it is most likely we need to
        // ride around it, or walk through, so then length and not speed (asphalt/sand) is important
        private readonly uint forbiddenDistanceMm;
        public Length ForbiddenDistance => Length.FromMillimeters(this.forbiddenDistanceMm);

        private Weight(uint forbiddenDistance, uint timeCostMs)
        {
            this.timeCostMs = timeCostMs;
            this.forbiddenDistanceMm = forbiddenDistance;
        }

        public static Weight operator +(in Weight a, in Weight b)
        {
            return new Weight(a.forbiddenDistanceMm + b.forbiddenDistanceMm,
                a.timeCostMs + b.timeCostMs);
        }

        public static Weight Diff(in Weight a, in Weight b)
        {
            // since this is only for debugging it is better to have more distinct name than just "-"
            return new Weight(a.forbiddenDistanceMm - b.forbiddenDistanceMm,
                a.timeCostMs - b.timeCostMs);
        }

        public Weight GetCostless()
        {
            return new Weight(this.forbiddenDistanceMm, 0);
        }
        
        public override string ToString()
        {
            string s = $"{this.timeCostMs}";
            if (!ForbiddenDistance.IsZero)
                s += $" (forbidden {(int)ForbiddenDistance.Meters})";
            return s;
        }

    }
}