using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Mapping;

namespace TrackPlanner.PathFinder.Overlay
{
    internal static class FlowFactory
    {
        public static IEnumerable<CoverFlow<TNodeId>> GetGridFlows<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            // how many cells (horizontally, and vertically) to create meta/shortcut cell
            int coverSize)
            where TNodeId : struct
            where TRoadId : struct
        {
            foreach (var cover in map.Grid.EnumerateAllCellIndices()
                         .Select(it => CoverIndex.Get(it, coverSize))
                         .Distinct())
            {
                yield return getGridBoundaryNodes(map, cover);
            }
        }

        private static CoverFlow<TNodeId> getGridBoundaryNodes<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            CoverIndex coverIndex)
            where TNodeId : struct
            where TRoadId : struct
        {
            var sources = new List<TNodeId>();
            var sinks = new HashSet<TNodeId>();
            var stats_nodes_count = 0;

            foreach (var cell_idx in coverIndex.GetCellIndices())
            {
                if (!map.Grid.TryGetAllNodes(cell_idx, out var nodes))
                    continue;

                foreach (var node_id in nodes)
                {
                    ++stats_nodes_count;
                
                    bool is_boundary = false;

                    foreach (var adj in map.GetAdjacentRoads(node_id))
                    {
                        var adj_node = map.GetNode(adj);
                        var adj_idx = map.Grid.GetIndexOfNode(adj_node);
                        if (!coverIndex.IsCoverOf(adj_idx))
                        {
                            sinks.Add(adj_node);
                            is_boundary = true;
                        }
                    }

                    if (is_boundary)
                        sources.Add(node_id);
                }
            }

            return new CoverFlow<TNodeId>(coverIndex, sources,stats_nodes_count);
        }
    }
}