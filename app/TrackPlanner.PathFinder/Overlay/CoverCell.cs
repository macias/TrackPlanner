using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.PathFinder.Overlay
{
    public sealed class CoverCell<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly Dictionary<TNodeId, int> boundaryIndices;
        private readonly IReadOnlyList<TNodeId> boundaryNodes; // back-reference
        private readonly WeightSteps<TNodeId, TRoadId>?[,] weights;

        public CoverCell(RouteManager<TNodeId, TRoadId> manager, UserRouterPreferences routerConfig,
            IReadOnlyList<TNodeId> boundaryNodes, CancellationToken token)
        {
            this.boundaryNodes = boundaryNodes;
            this.boundaryIndices = new Dictionary<TNodeId, int>(capacity: boundaryNodes.Count);
            for (int i = 0; i < boundaryNodes.Count; ++i)
                this.boundaryIndices.Add(boundaryNodes[i], i);

            this.weights = new WeightSteps<TNodeId, TRoadId>?[boundaryNodes.Count, boundaryNodes.Count];

            for (int start_idx = 0; start_idx < boundaryNodes.Count; ++start_idx)
            {
                var start_node = boundaryNodes[start_idx];
              //  var start_req = new RequestPoint<TNodeId>(manager.Map.GetPoint(start_node).Convert(),
              //      allowSmoothing: false, findLabel: false);
                for (int end_idx = 0; end_idx < boundaryNodes.Count; ++end_idx)
                {
                    var end_node = boundaryNodes[end_idx];

                    if (EqualityComparer<TNodeId>.Default.Equals(start_node, end_node))
                        continue;

                   // var end_req = new RequestPoint<TNodeId>(manager.Map.GetPoint(end_node).Convert(),
                     //   allowSmoothing: false, findLabel: false);

                    if (manager.TryFindVanillaRoute(routerConfig, new[] {start_node, end_node},
                            allowSmoothing:false,
                            allowGap:false,
                            token) is var  route_res && route_res.HasValue)
                        this.weights[start_idx, end_idx] = new WeightSteps<TNodeId, TRoadId>(
                            route_res.Value.RouteLegs.Single().Steps,
                            route_res.Value.RouteWeight,isDraft:false);
                    else
                        this.weights[start_idx, end_idx] = null;

                    if (route_res.ProblemMessage is {} problem)
                        throw new Exception($"{problem} while computing shortcut {manager.Map.GetOsmNodeId(start_node)}-{manager.Map.GetOsmNodeId(end_node)}");
                }
            }
        }

        /*  internal bool TryFindRoute(TNodeId start,TNodeId end, 
              [MaybeNullWhen(false)] out List<LegRun<TNodeId, TRoadId>> route,
              out RunningWeight routeWeight)
          {
              if (!this.boundaryIndices.TryGetValue(start, out var start_idx)
                  || !this.sinkIndices.TryGetValue(end, out var end_idx))
              {
                  route = null;
                  routeWeight =  RunningWeight.Infinity;
                  return false;
              }
  
              (var steps,routeWeight) = this.weights[start_idx, end_idx];
              if (routeWeight.IsInfinite)
              {
                  route = null;
                  return false;
              }
  
              route = new List<LegRun<TNodeId, TRoadId>>() { new LegRun<TNodeId, TRoadId>(steps.ToList())};
              return true;
          }
  */
        public bool IsAtBoundary(TNodeId nodeId)
        {
            return this.boundaryIndices.ContainsKey(nodeId);
        }

        internal IEnumerable<(TNodeId target, Weight weight)> GetShortcuts(TNodeId nodeId,
            bool isReversed)
        {
            if (this.boundaryIndices.TryGetValue(nodeId, out var index))
                yield break;
            for (int other_idx = 0; other_idx < this.boundaryIndices.Count; ++other_idx)
            {
                if (index == other_idx)
                    continue;
                var (src_idx, dst_idx) = isReversed ? (other_idx, index) : (index, other_idx);
                WeightSteps<TNodeId, TRoadId>? weight_steps = this.weights[src_idx, dst_idx];
                if (weight_steps is {} ws)
                {
                    yield return (this.boundaryNodes[dst_idx], ws.JoinedWeight);
                }
            }
        }

        public IReadOnlyList<StepRun<TNodeId,TRoadId>> GetSteps(TNodeId sourceNodeId, TNodeId targetNodeId)
        {
            var source_idx = this.boundaryIndices[sourceNodeId];
            var target_idx = this.boundaryIndices[targetNodeId];
            var weight = this.weights[source_idx, target_idx];
            if (weight==null)
                throw new Exception($"No shortcut");
            return weight.Value.Steps;
        }
    }
}