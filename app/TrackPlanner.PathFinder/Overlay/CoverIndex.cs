using System;
using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.PathFinder.Overlay
{
    internal readonly record struct CoverIndex
    {
        public static CoverIndex Get(CellIndex cellIndex, 
            // how many cells (horizontally, and vertically) to create meta/shortcut cell
            int coverSize)
        {
            return new CoverIndex((cellIndex.LatitudeGridIndex/coverSize), 
                (cellIndex.LongitudeGridIndex/coverSize), 
                coverSize);
        }

        private readonly short latitudeCoverIndex;
        private readonly  short longitudeCoverIndex;
        private readonly  short coverSize;

        private CoverIndex(int latitudeGridIndex, int longitudeGridIndex, int coverSize)
        {
            if (latitudeGridIndex < short.MinValue || latitudeGridIndex > short.MaxValue)
                throw new ArgumentOutOfRangeException($"{nameof(latitudeGridIndex)} = {latitudeGridIndex}");
            if (longitudeGridIndex < short.MinValue || longitudeGridIndex > short.MaxValue)
                throw new ArgumentOutOfRangeException($"{nameof(longitudeGridIndex)} = {longitudeGridIndex}");

            this.latitudeCoverIndex = (short) latitudeGridIndex;
            this.longitudeCoverIndex = (short) longitudeGridIndex;
            this.coverSize = (short) coverSize;
        }

        public IEnumerable<CellIndex> GetCellIndices()
        {
            for (int lat_idx = 0; lat_idx < coverSize ; ++lat_idx)
            for (int lon_idx = 0; lon_idx <  coverSize ; ++lon_idx)
                yield return new CellIndex(latitudeCoverIndex*this.coverSize+lat_idx,
                    longitudeCoverIndex*this.coverSize+ lon_idx);
        }

        public bool IsCoverOf(CellIndex cellIndex)
        {
            return cellIndex.LatitudeGridIndex >= this.latitudeCoverIndex*this.coverSize
                   && cellIndex.LatitudeGridIndex < latitudeCoverIndex*this.coverSize + this.coverSize
                   && cellIndex.LongitudeGridIndex >= this.longitudeCoverIndex*this.coverSize
                   && cellIndex.LongitudeGridIndex < longitudeCoverIndex*this.coverSize + this.coverSize;
        }

/*        public bool IsAdjacent(CoverIndex other)
        {
            if (this.coverSize != other.coverSize)
                throw new ArgumentException();
            
            if (this == other)
                return false;

            return Math.Abs(this.latitudeCoverIndex - other.latitudeCoverIndex) <= 1
                   && Math.Abs(this.longitudeCoverIndex - other.longitudeCoverIndex) <= 1;
        }*/
    }
    
}