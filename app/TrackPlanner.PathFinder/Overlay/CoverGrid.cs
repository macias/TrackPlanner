using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.PathFinder.Overlay
{
    public sealed class CoverGrid<TNodeId,TRoadId> : ICover<TNodeId,TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly Dictionary<CoverIndex, CoverCell<TNodeId, TRoadId>> covers;
        private readonly IWorldMap<TNodeId,TRoadId> map;
        private readonly int coverSize;

        public CoverGrid(ILogger logger, RouteManager<TNodeId,TRoadId> manager,UserRouterPreferences routerConfig,
            CancellationToken token)
        {
            routerConfig = routerConfig.DeepClone();
            routerConfig.SnapCyclewaysToRoads = Length.Zero;
            
            this.map = manager.Map;
            this.coverSize = 2;
            var flows = FlowFactory.GetGridFlows(manager.Map,coverSize:coverSize).ToList();
            this.covers = new Dictionary<CoverIndex, CoverCell<TNodeId, TRoadId>>();
            foreach (var (flow,i) in flows.ZipIndex())
            {
                logger.Info($"Computing {i}/{flows.Count} cover cell with {flow.BoundaryNodes.Count}/{flow.StatsNodesCount}");
             this.covers.Add(flow.CoverIndex,   new CoverCell<TNodeId, TRoadId>(manager, routerConfig,
                    flow.BoundaryNodes,  token));
            }
        }

        public bool CanUseDirectAdjacent(TNodeId current, TNodeId adjacent,
            RoadBucket<TNodeId,TRoadId> startBucket,RoadBucket<TNodeId,TRoadId> endBucket)
        {
            if (!CanUseShortcuts(current, startBucket,endBucket))
                return true;
            if (getCoverIndex(current) != getCoverIndex(adjacent))
                return true;
            
            return false;
        }
        public bool CanUseAdjacent(TNodeId current, TNodeId adjacent,
            RoadBucket<TNodeId,TRoadId> startBucket,IEnumerable< RoadBucket<TNodeId,TRoadId>> endBuckets)
        {
            if (!CanUseShortcuts(current, startBucket,endBuckets))
                return true;
            if (getCoverIndex(current) != getCoverIndex(adjacent))
                return true;
            
            return false;
        }

        private CoverIndex getCoverIndex(TNodeId nodeIde)
        {
            return CoverIndex.Get(this.map.Grid.GetIndexOfNode(nodeIde), this.coverSize);
        }

        public bool CanUseShortcuts(TNodeId current,
            RoadBucket<TNodeId,TRoadId> startBucket,RoadBucket<TNodeId,TRoadId> endBucket)
        {
            var curr_idx = getCoverIndex(current);
            if (!this.covers.TryGetValue(curr_idx, out var curr_cover))
                return false;
            if (startBucket.PrimarySnaps.Any(it => getCoverIndex(this.map.GetNode( it.BaseRoadIndex))==curr_idx
                                                || (it.NextRoadIndex is {} next_idx && getCoverIndex(this.map.GetNode( next_idx))==curr_idx))
             || endBucket.PrimarySnaps.Any(it => getCoverIndex(this.map.GetNode( it.BaseRoadIndex))==curr_idx 
                                              || (it.NextRoadIndex is {} next_idx && getCoverIndex(this.map.GetNode( next_idx))==curr_idx)))
                return false;
            if (!curr_cover.IsAtBoundary(current))
                return false;
            
            return true;
        }
        public bool CanUseShortcuts(TNodeId current,
            RoadBucket<TNodeId,TRoadId> startBucket,
            IEnumerable< RoadBucket<TNodeId,TRoadId>> endBuckets)
        {
            var curr_idx = getCoverIndex(current);
            if (!this.covers.TryGetValue(curr_idx, out var curr_cover))
                return false;
            if (startBucket.PrimarySnaps.Any(it => getCoverIndex(this.map.GetNode( it.BaseRoadIndex))==curr_idx
                                                || (it.NextRoadIndex is {} next_idx && getCoverIndex(this.map.GetNode( next_idx))==curr_idx))
                || endBuckets.SelectMany(it => it.PrimarySnaps)
                    .Any(it => getCoverIndex(this.map.GetNode( it.BaseRoadIndex))==curr_idx 
                                                 || (it.NextRoadIndex is {} next_idx && getCoverIndex(this.map.GetNode( next_idx))==curr_idx)))
                return false;
            if (!curr_cover.IsAtBoundary(current))
                return false;
            
            return true;
        }

        public IEnumerable<(TNodeId target, Weight weight)> GetShortcuts(TNodeId nodeId,bool isReversed) 
        {
            var cover_idx = getCoverIndex(nodeId);
            if (!this.covers.TryGetValue(cover_idx, out var cover))
                return ArraySegment<(TNodeId target, Weight weight)>.Empty;
            
            return cover.GetShortcuts(nodeId,isReversed);
        }

        public IReadOnlyList<StepRun<TNodeId,TRoadId>> GetSteps(TNodeId sourceNodeId, TNodeId targetNodeId, bool isReversed) 
        {
            var cover_idx = getCoverIndex(sourceNodeId);
          return this.covers[cover_idx].GetSteps(sourceNodeId, targetNodeId);
        }
    }
}
