The idea is to divide entire map graph into clusters, and then preprocess cluster boundaries computing
shortest paths between boundary nodes.

Later, when computing "real" route we would compute starting and ending segments the usual way, but
once we hit boundary we use precomputed shortcuts.

Current approach, super naive, groups KxK tiles/cells as cluster. In dense areas even for 2x2 size it means
something like 200 boundary nodes, meaning computing 40 000 routes just for single cluster. Even for small
map like kuyavian-pomorskie county it would take around 2 days for precomputing.

Possible solutions:
* when computing shortcuts, use multi-route search
* compute more optimal clustering (this requires fast indexing node-cluster and cluster-node)