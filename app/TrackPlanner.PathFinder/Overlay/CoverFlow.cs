using System.Collections.Generic;

namespace TrackPlanner.PathFinder.Overlay
{
    internal readonly  record struct CoverFlow<TNodeId>
    {
        public CoverIndex CoverIndex { get; }
        public IReadOnlyList<TNodeId> BoundaryNodes { get; }
        public int StatsNodesCount { get; }

        public CoverFlow(CoverIndex coverIndex, IReadOnlyList<TNodeId> boundaryNodes,int statsNodesCount)
        {
            CoverIndex = coverIndex;
            BoundaryNodes = boundaryNodes;
            StatsNodesCount = statsNodesCount;
        }
    }
}