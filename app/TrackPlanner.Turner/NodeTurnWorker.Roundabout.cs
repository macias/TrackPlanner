﻿using System.Collections.Generic;
using TrackPlanner.Mapping;
using TrackPlanner.Shared;
using TrackPlanner.Turner.Implementation;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Turner
{
    public sealed partial class NodeTurnWorker<TNodeId, TRoadId>
    {
        private IEnumerable<int> computeRoundaboutTurnNotifications(IReadOnlyList<TrackStepContext<TNodeId, TRoadId>> track,
            List<TurnInfo<TNodeId, TRoadId>> turns)
        {
            int roundabout_counter = 0;
            for (int i = 0; i < track.Count; ++i)
            {
                if (!track[i].Place.IsRoundaboutCenter)
                    continue;
                var has_node_entrance = i > 0 && track[i-1].IsNode;
                if (has_node_entrance)
                    yield return i - 1;
                var has_node_exit = i < track.Count - 1 && track[i+1].IsNode;
                if (has_node_exit)
                    yield return i + 1;
                
                if (i == 0 || i == track.Count - 1
                           // if the roundabout is right at start/end do not add turn notification 
                           || (i==1 && !track[i-1].Place.IsRoundaboutLink)
                    || (i==track.Count-2 && !track[i+1].Place.IsRoundaboutLink))
                    continue;

                var track_curved_message = isTrackCurved(track[i].Point, track[i - 1].Point, track[i + 1].Point,
                    out _);
                if (track_curved_message != null)
                {
                    turns.Add(TurnInfo<TNodeId, TRoadId>.CreateRoundabout(track[i].IsNode ? track[i].NodeId : null,
                        track[i].RoundaboutId!.Value,
                        track[i].Point.Convert2d(), i, roundabout_counter,
                        reason: track_curved_message));

                    ++roundabout_counter;
                }
            }
        }
        
    }
}