﻿namespace TrackPlanner.Turner.Implementation
{
    internal record struct TurnNotification
    {
        public static TurnNotification Alert(string? description)
        {
            return new TurnNotification(true, description);
        }
        public static TurnNotification Pass(string? description)
        {
            return new TurnNotification(false, description);
        }

        public bool Enabled { get; }
        public string? Description { get; }
        public string? TurnReason => this.Enabled ? this.Description : null;
        
        public TurnNotification(bool enabled, string? description)
        {
            Enabled = enabled;
            Description = description;
        }
    }
    
}
