﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Structures;
using TrackPlanner.Mapping;
using TrackPlanner.Mapping.Data;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Turner.Implementation
{
    internal sealed class TrackStepContext <TNodeId,TRoadId>
        where TNodeId:struct
        where TRoadId:struct
    {
        public static TrackStepContext<TNodeId, TRoadId> Create(
            #if DEBUG
            int originIndex,
            #endif
            IWorldMap<TNodeId, TRoadId> map, 
            Placement<TNodeId, TRoadId> place)
        {
            Dictionary<TRoadId, IReadOnlyList<ushort>> road_indicies;
            if (!place.IsNode)
                road_indicies = new Dictionary<TRoadId, IReadOnlyList<ushort>>();
            else
            {
                road_indicies = map.GetRoadsAtNode(place.NodeId)
                    .GroupBy(it => it.RoadId)
                    // remove all point-roads (like crossings)
                    .Where(it => map.GetRoadInfo(it.Key).Nodes.Count > 1)
                    .ToDictionary(it => it.Key, it => it.Select(it => it.IndexAlongRoad).ToList().ReadOnlyList());
                if (road_indicies.Values.Any(it => it.Count < 1 || it.Count > 2))
                    // 1 shared point within the road is a crossing
                    // 2 points are loops (roundabouts)
                    // 3 would be "8" figures and other weirdos
                    throw new ArgumentException($"We have weird number of shared points within single road");
            }

            return new TrackStepContext<TNodeId, TRoadId>(
                #if DEBUG
                originIndex,
                #endif
                map, place, road_indicies);
        }

        #if DEBUG
        public int OriginIndex { get; }
        #endif
        private readonly IWorldMap<TNodeId,TRoadId> map;

        // road id -> indices along road nodes (1 or -- in case of roundabout start/end -- 2)
        private readonly IReadOnlyDictionary<TRoadId, IReadOnlyList<ushort>> roadIndicesMap;

        public IEnumerable<RoadIndex<TRoadId>> RoadIndices => this.roadIndicesMap
            .SelectMany(it => it.Value.Select(v => new RoadIndex<TRoadId>(it.Key,v) ));
        
        public TRoadId? RoundaboutId { get; }

        public int RoadCount => this.roadIndicesMap.Count;

        public bool CycleWayExit { get; set; }
        public bool ForwardCycleWayCorrected { get; set; }
        public bool BackwardCycleWayCorrected { get; set; }
        public bool CyclewaySwitch { get; set; }
        public bool BackwardCyclewayUncertain { get; set; }
        public bool ForwardCyclewayUncertain { get; set; }
        public DirectionsInfo<RoadIndex<TRoadId>> RoadsInfo { get; }

        private TrackStepContext(
            #if DEBUG
            int originIndex,
            #endif
            IWorldMap<TNodeId, TRoadId> map, Placement<TNodeId, TRoadId> place,
            // road id -> indices along road
            IReadOnlyDictionary<TRoadId, IReadOnlyList<ushort>> roadIndicesMap)
        {
            this.OriginIndex = originIndex;
            this.map = map;
            this.roadIndicesMap = roadIndicesMap;
            this.Place = place;

            if (place.IsRoundaboutCenter)
                RoundaboutId = place.RoundaboutCenterRoadId!.Value;
            else
                foreach (var entry in this.roadIndicesMap)
                {
                    if (map.GetRoadInfo(entry.Key).IsRoundabout)
                    {
                        RoundaboutId = entry.Key;
                        break;
                    }
                }

            this.RoadsInfo = new DirectionsInfo<RoadIndex<TRoadId>>();
        }

        public Placement<TNodeId,TRoadId> Place { get; }
        public GeoZPoint Point => Place.Point;
        public TNodeId NodeId => Place.NodeId;
        public bool IsNode => Place.IsNode;

        private TrafficDirection<TRoadId> incomingRelativeTraffic;

        public TrafficDirection<TRoadId> IncomingRelativeTraffic
        {
            get => this.incomingRelativeTraffic;
            set { this.incomingRelativeTraffic = value;
                if (value == default)
                    throw new ArgumentException();
            }
        }

        private TrafficDirection<TRoadId> outgoingRelativeTraffic;
        public TrafficDirection<TRoadId> OutgoingRelativeTraffic
        {
            get => this.outgoingRelativeTraffic;
            set
            {
                this.outgoingRelativeTraffic = value;
                if (value == default)
                    throw new ArgumentException();
            }
        }

        public IReadOnlyDictionary<TRoadId, (ushort currentIndex, ushort nextIndex)> GetSharedRoads(TrackStepContext<TNodeId,TRoadId> other)
        {
            var intersect = Linqer.Intersect(this.roadIndicesMap, other.roadIndicesMap, (a, b) => (a, b));
            // this is naive, we assume the highest index from current node and the lowest index from the next node
            // are the closest ones
            // but roads can have loops and knots so this is VERY shaky

            // todo: compute all permutations and return the actual closests one, by computing segment distance
            // todo: this is wrong also because we cannot assume we are along (and not in reverse) of given road
            return intersect.ToDictionary(it => it.Key, it => (it.Value.a.OrderByDescending(x => x).First(), 
                it.Value.b.OrderBy(x => x).First()));
        }

        internal RoadIndex<TRoadId> GetRoadIndex(TRoadId roadId)
        {
            return new RoadIndex<TRoadId>(roadId,this.roadIndicesMap[roadId].First());
        }

        public string OsmString()
        {
            return this.Place.OsmString(map);
        }
    }
}
