﻿using System;

namespace TrackPlanner.Turner.Implementation
{
    internal sealed class DirectionsInfo<T>
    where T : struct
    {
        private T? tryBackward;
        public T FacingBackward
        {
            get { return this.tryBackward ?? throw new NullReferenceException("Backward face is not set"); }
            set { this.tryBackward = value; }
        }

        private T? tryForward;
        public T FacingForward
        {
            get { return this.tryForward ?? throw new NullReferenceException("Forward face is not set"); }
            set { this.tryForward = value; }
        }

        public DirectionsInfo()
        {
        }

    }
}