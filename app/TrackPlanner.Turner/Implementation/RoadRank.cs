﻿using System;
using TrackPlanner.Mapping.Data;

namespace TrackPlanner.Turner.Implementation
{
    internal readonly struct RoadRank<TNodeId>
    {
       public enum SimpleSurface
        {
            Paved,
            Unpaved,
            Unknown
        }
        enum Perception
        {
            Highway,
            Trunk,
            Primary,
            Secondary,
            Tertiary,
            Cycleway,
            Ferry,
            Path,
        }

        public static RoadRank<TNodeId> CyclewayLink(in RoadInfo<TNodeId> info) 
            => new RoadRank<TNodeId>(info.Kind, Perception.Cycleway, DEPRECATED_isSolid(info), simplifySurface(info),
                forced: true);
        public RoadRank<TNodeId> CyclewayLink() => new RoadRank<TNodeId>(original, Perception.Cycleway,
            DEPRECATED_IsSolid,SurfaceCategory, 
            forced: true);

        public bool IsCycleway => this.simplified == Perception.Cycleway;
        public bool IsMapPath => this.original == WayKind.Path;
        public bool DEPRECATED_IsSolid { get; }
        public SimpleSurface SurfaceCategory { get; }

        private readonly WayKind original;
        private readonly Perception simplified;
        private readonly bool forced;

        public RoadRank(in RoadInfo<TNodeId> info) : this(info.Kind, simplifyRoadImportance(info), 
            DEPRECATED_isSolid(info),simplifySurface(info), false)
        {
        }
        private RoadRank(WayKind original, Perception kind, bool deprecatedIsSolid,SimpleSurface solid, bool forced)
        {
            this.original = original;
            this.simplified = kind;
            this.forced = forced;
            this.DEPRECATED_IsSolid = deprecatedIsSolid;
            SurfaceCategory = solid;
        }

        private static bool DEPRECATED_isSolid(in RoadInfo<TNodeId> info)
        {
            if (info.Kind <= WayKind.TertiaryLink)
                return true;

            switch (info.Kind)
            {
                case WayKind.Auxiliary:
                case WayKind.Unclassified:
                    if (info.Surface <= RoadSurface.Paved || info.IsLikelyPaved() || info.HasName)
                        return true;
                    break;

                case WayKind.Path:
                    if (info.Surface <= RoadSurface.Paved)
                        return true;
                    break;
            }

            return false;
        }

        private static SimpleSurface simplifySurface(in RoadInfo<TNodeId> info)
        {
            if (info.Kind <= WayKind.TertiaryLink
                || info.Surface <= RoadSurface.Paved)
                return SimpleSurface.Paved;

            if (info.Surface != RoadSurface.Unknown)
                return SimpleSurface.Unpaved;
            if (info.IsLikelyPaved())
                return SimpleSurface.Paved;

            return SimpleSurface.Unknown;
        }

        private static Perception simplifyRoadImportance(in RoadInfo<TNodeId> info)
        {
            // here we convert the kinds of the roads to more likely how the cyclist will "feel" the road and most importantly the changes between roads

            switch (info.Kind)
            {
                // for turns treat links and given road the same
                case WayKind.Highway:
                case WayKind.HighwayLink: return Perception.Highway;
                case WayKind.Primary:
                case WayKind.PrimaryLink: return Perception.Primary;
                case WayKind.Secondary:
                case WayKind.SecondaryLink: return Perception.Secondary;
                case WayKind.Trunk:
                case WayKind.TrunkLink: return Perception.Trunk;
                case WayKind.Tertiary:
                case WayKind.TertiaryLink: return Perception.Tertiary;

                case WayKind.Ferry: return Perception.Ferry;
                case WayKind.Cycleway: return Perception.Cycleway;

                case WayKind.Footway: return Perception.Path;
                case WayKind.Steps: return Perception.Path;

                // it is unlikely secondary road will remain unclassified, but residential or tertiary yes -- so, same bucket
                // we can upgrade this type of road only if we don't know the surface or if we know it well -- because it is better to add turn-notification than not
                case WayKind.Unclassified:
                case WayKind.Auxiliary:
                    if (info.Surface<= RoadSurface.Paved || info.IsLikelyPaved() || info.HasName)
                        return Perception.Tertiary;
                    else
                        return Perception.Path;

                case WayKind.Path:
                    if (info.Surface == RoadSurface.AsphaltLike || info.HasName)
                        return Perception.Tertiary;
                    else
                        return Perception.Path;

            }

            throw new NotImplementedException($"Add case for {info.Kind}");
        }

        public bool IsMoreImportantThan(in RoadRank<TNodeId> other)
        {
            if (this.simplified == Perception.Tertiary && other.simplified == Perception.Cycleway)
                return false;
            else
                return this.simplified < other.simplified;
        }

        public bool IsSignificantlyMoreImportantThan(in RoadRank<TNodeId> other)
        {
            if (this.simplified == Perception.Tertiary && other.simplified == Perception.Cycleway)
                return false;
            else if (this.simplified <= Perception.Secondary && this.simplified < other.simplified)
                return true;
            else if (this.simplified == Perception.Tertiary
                     && this.simplified < other.simplified
                     && this.SurfaceCategory == SimpleSurface.Paved
                     && other.SurfaceCategory == SimpleSurface.Unpaved)
                return true;

            return false;
        }

        public int DifferenceLevel(in RoadRank<TNodeId> other)
        {
            if (this.simplified < other.simplified)
                return other.DifferenceLevel(this);

            int diff = this.simplified - other.simplified;
            // switching from (let's say) cycleway to path or to ferry is the the same difference, so we have to correct our enum "gaps"
            if (this.simplified == Perception.Path && other.simplified < Perception.Ferry)
                --diff;
            return diff;
        }


        public override string ToString()
        {
            return $"{(forced ? "^^" : "")}{simplified}";
        }
    }
}
