﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using TrackPlanner.Backend;
using TrackPlanner.Turner.Implementation;
using TrackPlanner.Mapping;
using TrackPlanner.Structures;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Turner
{
    public sealed partial class NodeTurnWorker<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct
    {
        private readonly ILogger logger;
        private readonly SystemTurnerConfig turnerConfig;
        public IWorldMap<TNodeId, TRoadId> Map { get; }
        private readonly UserTurnerPreferences userPreferences;
        private readonly ApproximateCalculator calc;
        private readonly List<(GeoZPoint, string)> DEBUG_points;
        private static readonly IReadOnlySet<int> emptyIntSet = new HashSet<int>();
        private ContextMessage? problemMessage;


        public NodeTurnWorker(ILogger logger, IWorldMap<TNodeId, TRoadId> map,
            SystemTurnerConfig turnerConfig, UserTurnerPreferences userPreferences)
        {
            this.logger = logger;
            this.Map = map;
            this.turnerConfig = turnerConfig;
            this.userPreferences = userPreferences;
            this.calc = new ApproximateCalculator();
            this.DEBUG_points = new List<(GeoZPoint, string)>();
        }

        private string stringify(in RoadInfo<TNodeId> info)
        {
            return $"{info.Kind}"; //:{info.Identifier}";
        }

  
        public List<FlowJump<TNodeId, TRoadId>> MergeDailySteps(IReadOnlyList<LegFlow<TNodeId, TRoadId>> legs)
        {
            int last_index = -1;
            int end_index = legs.LastIndexOf(it => it.Flow.Count > 0);
            List<FlowJump<TNodeId, TRoadId>> merged = new List<FlowJump<TNodeId, TRoadId>>();
            for (int i = 0; i <= end_index; ++i)
            {
                if (legs[i].Flow.Count == 0)
                    continue;
                
                if (last_index != -1)
                {
                    merged.Add(legs[last_index].Flow.Last() with
                    {
                        FacingForwardIndex = legs[i].Flow.First().FacingForwardIndex
                    });
                }

                // we will merge legs by dropping first element of the "next" leg
                int skip_first = last_index == -1 ? 0 : 1;
                int skip_last = i == end_index ? 0 : 1;
                merged.AddRange(legs[i].Flow.Skip(skip_first)
                    .Take(legs[i].Flow.Count - skip_first - skip_last));
                last_index = i;
            }


            return merged;
        }
        
        public Result<List<TurnInfo<TNodeId, TRoadId>>> ComputeTurnPoints(
            IReadOnlyList<FlowJump<TNodeId, TRoadId>> steps, CancellationToken token)
        {
            try
            {
                return internalComputeTurnPoints(steps);
            }
            catch (Exception ex)
            {
                ex.LogDetails(this.logger, MessageLevel.Error);
                string message = "Unable to compute turns";
#if DEBUG
                message += $": {ex.Message}";
#endif
                return Result<List<TurnInfo<TNodeId, TRoadId>>>.Fail(message);
            }
        }

        private Result<List<TurnInfo<TNodeId, TRoadId>>> internalComputeTurnPoints(IReadOnlyList<FlowJump<TNodeId, TRoadId>> steps)
        {
            this.problemMessage = null;

            IEnumerable<TrackStepContext<TNodeId, TRoadId>> stretch_route()
            {
                for (int i = 0; i < steps.Count; ++i)
                {
                    var curr = steps[i].Place;
                    TrackStepContext<TNodeId, TRoadId> ctx;
                    // involve crosspoints as well, because right on next node we could have a turn
                    var backward_idx =     steps[i].FacingBackwardIndex;
                    var forward_idx =steps[i].FacingForwardIndex;
                    #if DEBUG
                    if (forward_idx.IndexAlongRoad == 4)
                    {
                        var road = Map.GetRoadInfo(forward_idx.RoadId);
                        if (road.Nodes.Count == 4)
                        {
                            ;
                        }
                    }
                    #endif
                    if (curr.IsCrossSegment)
                    {
                            ctx = TrackStepContext<TNodeId, TRoadId>.Create(
#if DEBUG
                                i,
#endif
                                this.Map,
                                curr.StripSegment(this.Map,i == 0 ? forward_idx : backward_idx));
                    }
                    else 
                    {
                        ctx = TrackStepContext<TNodeId, TRoadId>.Create(
#if DEBUG
                            i,
#endif
                            this.Map, curr);
                    }
                    
                    ctx.RoadsInfo.FacingForward = forward_idx;
                    ctx.RoadsInfo.FacingBackward = backward_idx;
                    ctx.OutgoingRelativeTraffic = steps[i<steps.Count-1?i+1:i].IncomingRelativeTraffic;
                    ctx.IncomingRelativeTraffic = steps[i].IncomingRelativeTraffic;
                    yield return ctx;
                }
            }

            var track = stretch_route()
                .ConsecutiveDistinctBy((a, b) => a.Place.Point == b.Place.Point)
                .ToList();

            if (false && this.turnerConfig.DebugDirectory != null)
            {
                TrackWriter.Build(null, track.Select(it => it.Place.Point)).Save(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory, "orig-points.kml"));
                TrackWriter.Build(null, new[] {track.First().Place.Point, track.Last().Place.Point}, PointIcon.ParkingIcon)
                    .Save(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory, "orig-anchors.kml"));
            }

            // remove same CONSECUTIVE (because the track could be in shape of "8") points
            //  track = track.ConsecutiveDistinctBy(dict => dict.NodeId).ToList();

            logger.Verbose($"Recreated track has {track.Count()} points");

            dumpRecreatedTrack(track, "dups");

            var turn_alerts = new List<TurnInfo<TNodeId, TRoadId>>();
/*            #if DEBUG
            var turn_ignores = new List<TurnInfo<TNodeId, TRoadId>>();
            #endif
*/
            // add pairs enter-exit turns (if needed) for passed roundabouts
            HashSet<int> roundabout_exits = computeRoundaboutTurnNotifications(track, turn_alerts)
                    .ToHashSet();

            if (this.turnerConfig.DebugDirectory != null && this.turnerConfig.DumpRoundaboutInfo)
                TrackWriter.WriteLabeled(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory,
                        "roundabout-exits.kml"), null,
                    roundabout_exits.Select(it => (track[it].Point, $"[{it}]")));

           // markCyclewayExits(track, roundabout_exits);

            IReadOnlySet<int> potential_turn_indices;
            {
                var indices = computeCrossroadsIndices(track, roundabout_exits).ToList();
                if (this.turnerConfig.DumpPossibleTurns && this.turnerConfig.DebugDirectory != null)
                    TrackWriter.WriteLabeled(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory,
                            "possible-turns.kml"), null,
                        indices.OrderBy(x => x.index).Select(x => (track[x.index].Point, $"[{x.index}] {x.reason}")));
                potential_turn_indices = indices.Select(it => it.index).ToHashSet();
            }

            foreach (int turn_pt_idx in potential_turn_indices.OrderBy(it => it)) // ordering makes debugging easier
            {
                // compute road kinds only at given points, this way we avoid problems with computing along entire track (at some point computing can be shaky)
                TrackStepContext<TNodeId, TRoadId> track_incoming_ctx = track[turn_pt_idx - 1];
                TrackStepContext<TNodeId, TRoadId> track_outgoing_ctx = track[turn_pt_idx + 1];
                var track_node_ctx = track[turn_pt_idx];

                var incoming_road_idx = track_incoming_ctx.RoadsInfo.FacingForward;
                var outgoing_road_idx = track_outgoing_ctx.RoadsInfo.FacingBackward;

               // var incoming_rank = new RoadRank<TNodeId>(Map.GetRoad(incoming_road_idx.RoadId));
                //var outgoing_rank = new RoadRank<TNodeId>(Map.GetRoad(outgoing_road_idx.RoadId));

                TNodeId incoming_arm_node_id = track_incoming_ctx.NodeId;
                //GeoZPoint incoming_arm_pt = track_incoming_ctx.Point;
                TNodeId outgoing_arm_node_id = track_outgoing_ctx.NodeId;
                //GeoZPoint outgoing_arm_pt = track_outgoing_ctx.Point;

                GeoZPoint turn_point = track_node_ctx.Point;

                // Console.WriteLine($"{Map.GetOsmNodeId(track_node.NodeId)} {turn_point}");

                IReadOnlyList<(RoadIndex<TRoadId> turn, RoadIndex<TRoadId> adj_arm)> alt_arms
                    = getAlternateArms(track_node_ctx, incoming_road_idx, outgoing_road_idx).ToList();
                logger.Verbose($"Track index {turn_pt_idx}, arms {alt_arms.Count}, {(String.Join(", ", alt_arms.Select(it => stringify(Map.GetRoadInfo(it.turn.RoadId)))))}");

                bool is_cross_intersection = false;
                if (alt_arms.Count == 2)
                {
                    // we calculate more distant points from the track to avoid reporting cross intersection for figures like
                    //   |  / track here
                    //  -+-/
                    //   |
                    //  with - segment being very short. For the rider in real life it will be angled interesection
                    if (Map.GetRoadInfo(alt_arms[0].turn.RoadId).Kind 
                        == Map.GetRoadInfo(alt_arms[1].turn.RoadId).Kind
                        // go over other turns to get proper length of the arm
                        && tryGetPointAlongTrack(track, emptyIntSet, turn_pt_idx, -1, this.userPreferences.MinimalCrossIntersection, out GeoZPoint distant_incoming_pt)
                        && tryGetPointAlongTrack(track, emptyIntSet, turn_pt_idx, +1, this.userPreferences.MinimalCrossIntersection, out GeoZPoint distant_outgoing_pt)
                        // for detecting true cross intersection we don't simplify arm kinds, because the arms have to be identical
                        //&& incoming_kind == outgoing_kind
                       )
                    {
                        GeoZPoint left_arm_pt = Map.GetPoint(alt_arms[0].adj_arm);
                        GeoZPoint right_arm_pt = Map.GetPoint(alt_arms[1].adj_arm);
                        if (calc.IsCrossIntersection(turn_point, distant_incoming_pt, distant_outgoing_pt, left_arm_pt, right_arm_pt, this.userPreferences.CrossIntersectionAngleSeparation,
                                out Angle in_left_angle, out Angle in_right_angle, out Angle out_left_angle, out Angle out_right_angle))
                        {
                            is_cross_intersection = true;
                            if (this.turnerConfig.DumpCrossIntersections && this.turnerConfig.DebugDirectory != null)
                            {
                                TrackWriter.WriteLabeled(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory,
                                        $"cross_interesection-{turn_pt_idx}.kml"),
                                    new[] {distant_incoming_pt, turn_point, distant_outgoing_pt},
                                    new[]
                                    {
                                        (left_arm_pt, $"{(in_left_angle.Degrees.ToString("0.#"))} {(out_left_angle.Degrees.ToString("0.#"))} "),
                                        (turn_point, $"{turn_pt_idx}"),
                                        (right_arm_pt, $"{(in_right_angle.Degrees.ToString("0.#"))} {(out_right_angle.Degrees.ToString("0.#"))} ")
                                    });
                            }
                        }
                    }
                }

                if (is_cross_intersection)
                {
                    //logger.Verbose($"Skipping cross intersection at track node {turn_pt_idx}: {incoming_road_idx}-{outgoing_road_idx} vs {alt_arms[0].adj_arm}-{alt_arms[1].adj_arm}");
                }
                else
                {
                    TurnNotification forward = TurnNotification.Pass("Initial straight");
                    TurnNotification backward = forward;

                    {
                        foreach (var (alt_road_idx, alt_sibling_idx) in alt_arms)
                        {
                            var alt_info = Map.GetRoadInfo(alt_road_idx.RoadId);
                            //RoadRank alt_importance = simplifyRoadImportance(alt_info);
                            TNodeId alt_sibling_node_id = alt_info.Nodes[alt_sibling_idx.IndexAlongRoad];

                            (forward, backward) = isTurnNeeded(track, potential_turn_indices, turn_pt_idx,
                                track_node_ctx.IncomingRelativeTraffic,
                                track_node_ctx.OutgoingRelativeTraffic,
                                alt_road_idx, alt_sibling_idx,
                                turn_point,
                                Map.GetRoadInfo(incoming_road_idx.RoadId), // road kind "to" turn point
                                incoming_arm_node_id,
                                Map.GetRoadInfo(outgoing_road_idx.RoadId), // road kind "from" turn point
                                outgoing_arm_node_id,
                                alt_info, alt_sibling_node_id);

                            if (forward.Enabled || backward.Enabled)
                            {
                                //logger.Verbose($"Turn {turn_pt_idx} on roads {incoming_road_idx.RoadId} {outgoing_road_idx.RoadId} {alt_road_idx.RoadId}, all present {(String.Join(", ", alt_arms.Select(it => stringify(Map.GetRoad(it.turn.RoadId)))))}");
                                string reason;
                                if (forward.TurnReason == backward.TurnReason)
                                    reason = "* " + forward.TurnReason;
                                else if (forward.Enabled && backward.Enabled)
                                    reason = $"F:{forward.TurnReason}; B:{backward.TurnReason}";
                                else
                                    reason = (forward.TurnReason ?? backward.TurnReason)!;

                                turn_alerts.Add(TurnInfo<TNodeId, TRoadId>.CreateRegular(track_node_ctx.NodeId, turn_point.Convert2d(),
                                    turn_pt_idx,
                                    forward.Enabled, backward.Enabled, reason));

                                break;
                            }
                            else
                            {
                                logger.Verbose($"So far no turn at {turn_pt_idx}, forward reason {forward.Description}, backward {backward.Description} ");
                            }
                        }
                    }

                    if (!forward.Enabled && !backward.Enabled)
                    {
                        logger.Verbose($"NO turn {turn_pt_idx} on roads {incoming_road_idx.RoadId} {outgoing_road_idx.RoadId}, all present {(String.Join(", ", alt_arms.Select(it => stringify(Map.GetRoadInfo(it.turn.RoadId)))))}");
                        /* #if DEBUG
                         turn_ignores.Add(TurnInfo<TNodeId, TRoadId>.CreateRegular(track_node.NodeId, turn_point.Convert2d(),
                             turn_pt_idx,
                             forward.Enable, backward.Enable, reason));
                         #endif*/
                    }
                }
            }

            if (this.turnerConfig.DebugDirectory != null && this.turnerConfig.DumpPoints)
            {
                TrackWriter.WriteLabeled(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory, 
                        "debug.kml"), null,
                    DEBUG_points);
            }
            if (this.turnerConfig.DebugDirectory != null && this.turnerConfig.DumpTurns)
            {
                TrackWriter.WriteLabeled(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory, 
                        "turn-points.kml"),
                    null, turn_alerts.Select(it => (it.Point.Convert3d(), it.GetLabel())));
            }

            turn_alerts = turn_alerts.OrderBy(it => it.TrackIndex).ToList();
            return Result.Valid(turn_alerts, this.problemMessage);
        }


        private void markCyclewayExits(List<TrackStepContext<TNodeId, TRoadId>> track, 
            IReadOnlySet<int> roundaboutExits)
        {
            IReadOnlySet<int> potential_turn_indices = computeCrossroadsIndices(track, roundaboutExits)
                .Select(it => it.index)
                .ToHashSet();
            foreach (int i in potential_turn_indices.OrderBy(x => x))
            {
                var incoming_road_idx = track[i - 1].RoadsInfo.FacingForward;
                var outgoing_road_idx = track[i + 1].RoadsInfo.FacingBackward;

                var incoming_rank = new RoadRank<TNodeId>(Map.GetRoadInfo(incoming_road_idx.RoadId));
                var outgoing_rank = new RoadRank<TNodeId>(Map.GetRoadInfo(outgoing_road_idx.RoadId));

                bool incoming_fixed = fixPathCycleWayLink(track, i - 1, +1, ref incoming_rank);
                bool outgoing_fixed = fixPathCycleWayLink(track, i + 1, -1, ref outgoing_rank);
                // if we detected a path, which can be treated as cycleway it is better to keep this info,
                // because further turn-nofication depends on such shaky situation
                if (incoming_fixed)
                {
                    track[i - 1].ForwardCyclewayUncertain = true;
                }

                if (outgoing_fixed)
                    track[i + 1].BackwardCyclewayUncertain = true;

                GeoZPoint incoming_arm_pt = track[i - 1].Point;
                GeoZPoint outgoing_arm_pt = track[i + 1].Point;

                GeoZPoint turn_point = track[i].Point;

                // if we go via road and we simply "turn" into parallel cycleway then ignore such turn,
                // because road signs will tell all the instructions

                // we try to detect here if our track looks like this
                // |_
                //   |
                // in theory there are two turns, in fact it can be road-to-cycleway switch and in such case
                // there should be no turn notifications

                bool from_cycleway = incoming_rank.IsCycleway;
                bool to_cycleway = outgoing_rank.IsCycleway;
                if ((from_cycleway && outgoing_rank.DEPRECATED_IsSolid) || (to_cycleway && incoming_rank.DEPRECATED_IsSolid))
                {
                    // ok, so we know we have change from/to cycleway, this coresponds to the short "link" segment
                    // so now we have to check if our track "behind" the cycle-link is also cycleway
                    if (from_cycleway && (i < 2 || Map.GetRoadInfo(track[i - 2].RoadsInfo.FacingForward.RoadId).Kind != WayKind.Cycleway))
                    {
                        logger.Verbose($"Giving up on cycle way exit at track index {i}, no further cycleway");
                        continue;
                    }
                    else if (to_cycleway && (i >= track.Count - 2 || Map.GetRoadInfo(track[i + 2].RoadsInfo.FacingBackward.RoadId).Kind != WayKind.Cycleway))
                    {
                        logger.Verbose($"Giving up on cycle way exit at track index {i}, no further cycleway");
                        continue;
                    }

                    GeoZPoint cycleway_point = from_cycleway ? incoming_arm_pt : outgoing_arm_pt;
                    // first segment of the cycleway (counting from road usually is orthogonal and very short
                    Length cycle_next_dist = calc.GetDistance(turn_point, cycleway_point);

                    logger.Verbose($"Potential cycle way exit at track index {i}, dist {cycle_next_dist}");
                    DEBUG_points.Add((turn_point, $"Cycle way exit {i} turn"));
                    DEBUG_points.Add((cycleway_point, $"Cycle way point {i} exit"));

                    if (cycle_next_dist <= this.userPreferences.CyclewayExitDistanceLimit)
                    {
                        /*int regular_cycle_track_idx = i + (from_cycleway ? -2 : +2);
                        if (regular_cycle_track_idx < 0 || regular_cycle_track_idx >= track.Count)
                        {
                            logger.Warning($"Cannot decide whether we have cycleway exit, because index is off the track {regular_cycle_track_idx}/{track.Count}");
                            continue;
                        }
                        */

                        Angle road_bearing = calc.GetBearing(from_cycleway ? outgoing_arm_pt : incoming_arm_pt, turn_point);
                        int parallel_index = i + (from_cycleway ? -1 : +1);
                        if (!tryGetPointAlongTrack(track, potential_turn_indices, parallel_index, from_cycleway ? -1 : +1, this.userPreferences.CyclewayRoadParallelLength, out GeoZPoint parallel_cycle_point))
                        {
                            logger.Warning($"Cannot get enough parallel segment {this.userPreferences.CyclewayRoadParallelLength} to the road starting from {parallel_index}");
                            continue;
                        }

                        Angle cycle_bearing = calc.GetBearing(from_cycleway ? incoming_arm_pt : outgoing_arm_pt, parallel_cycle_point);
                        //                        Angle cycle_bearing = calc.GetBearing(from_cycleway ? incoming_arm_pt : outgoing_arm_pt, track[regular_cycle_track_idx].Point);
                        // compute absolute value in range (0,180) with 180 meaning we go straight ahead, 0 we going back
                        Angle bearing_diff = calc.GetAbsoluteBearingDifference(road_bearing, cycle_bearing);
                        bool marking_exit = bearing_diff >= this.userPreferences.CyclewayExitAngleLimit;
                        logger.Verbose($"Exit cycleway {i} = {marking_exit}. At angle {bearing_diff}, limit {this.userPreferences.CyclewayExitAngleLimit}");
                        if (marking_exit)
                        {
                            track[i].CycleWayExit = true;
                            if (from_cycleway)
                            {
                                track[i - 1].CyclewaySwitch = true;
                                logger.Verbose($"Marking track index {i - 1} as cycleway switch");
                            }

                            if (to_cycleway)
                            {
                                track[i + 1].CyclewaySwitch = true;
                                logger.Verbose($"Marking track index {i + 1} as cycleway switch");
                            }

                            track[i - 1].ForwardCycleWayCorrected = incoming_fixed;
                            track[i + 1].BackwardCycleWayCorrected = outgoing_fixed;
                        }
                    }
                }
            }

            foreach (var node in track)
            {
                // if we know for sure we need to correct cycleway exit link part then this segment is no longer uncertain
                if (node.ForwardCycleWayCorrected)
                    node.ForwardCyclewayUncertain = false;
                if (node.BackwardCycleWayCorrected)
                    node.BackwardCyclewayUncertain = false;
            }
        }

        private bool tryGetPointAlongTrack(IReadOnlyList<TrackStepContext<TNodeId, TRoadId>> track,
            IReadOnlySet<int> turnIndices, int startIndex, int direction, Length length, out GeoZPoint point)
        {
            for (int curr = startIndex;; curr += direction)
            {
                int next = curr + direction;
                if (next < 0 || next >= track.Count)
                {
                    if (curr == startIndex)
                    {
                        point = default;
                        return false;
                    }
                    else
                    {
                        point = track[curr].Point;
                        return true;
                    }
                }

                if (length == Length.Zero)
                {
                    point = track[curr].Point;
                    return true;
                }

                Length dist = calc.GetDistance(track[curr].Point, track[next].Point);
                if (length >= dist)
                    length -= dist;
                else
                {
                    point = calc.PointAlongSegment(track[curr].Point, track[next].Point, length);
                    return true;
                }

                if (turnIndices.Contains(next))
                {
                    point = track[next].Point;
                    return true;
                }
            }
        }

        private bool fixPathCycleWayLink(List<TrackStepContext<TNodeId, TRoadId>> track, int i, int direction,
            ref RoadRank<TNodeId> segmentKind)
        {
            // there some cases when cycleways don't end up with regular roads, but in pathes, which then go into regular roads, 
            // we would like to treat such short path-cycleway exits like regular cycleways

            // please note we deal here with segments, and we can have two cases
            // o----O====o-----o
            // or
            // o----o====O-----o
            // = segment we would like to fix, O active track node
            // in first case left is next-left, while in the second it is next-next-left
            // thanks to "direction" passed we can select appropriate segments with the same code

            if (!segmentKind.IsMapPath)
            {
                logger.Verbose($"Skipping cycleway link fix for {i}/{direction}, not map path");
                return false;
            }

            // we could rely only on track segments which would be maybe more accurate, but until it is not needed let's base just on map info

            bool? is_cycleway_category(TrackStepContext<TNodeId, TRoadId> node)
            {
                var road_indices_at_node = this.Map.GetRoadsAtNode(node.NodeId);
                bool has_cycleway = road_indices_at_node.Any(it => Map.GetRoadInfo(it.RoadId).Kind == WayKind.Cycleway);
                bool has_road = road_indices_at_node.Any(it => new RoadRank<TNodeId>(Map.GetRoadInfo(it.RoadId)).DEPRECATED_IsSolid);

                if (has_cycleway && !has_road)
                    return true;
                else if (has_road && !has_cycleway)
                    return false;
                else
                    return null; // hard to tell
            }

            if (!(is_cycleway_category(track[i]) is bool this_cycle))
                return false;
            if (!(is_cycleway_category(track[i + direction]) is bool other_cycle))
                return false;

            bool making_link = this_cycle != other_cycle;
            logger.Verbose($"Cycle way link fixed = {making_link} track {i}/{direction} with this cycle {this_cycle} and other cycle {other_cycle}");
            if (making_link) // to have LINK one has to be cycle way, while the other cannot be cycle way
            {
                segmentKind = segmentKind.CyclewayLink();
                return true;
            }

            return false;
        }

        private void dumpRecreatedTrack(IReadOnlyList<TrackStepContext<TNodeId, TRoadId>?> road_assignments, string label)
        {
            if (this.turnerConfig.DebugDirectory != null)
            {
                var points = road_assignments.ZipIndex()
                    .Where(it => it.item != null)
                    .Select(entry =>
                    {
                        var i = entry.index;
                        var ctx = entry.item!;
                        string label;
                        if (ctx.IsNode)
                        {
                            (TRoadId road_id, ushort idx) = this.Map.GetRoadsAtNode(ctx.NodeId).First();
                            label = ctx.RoadCount == 1 ? $"[{i}]={road_id}" : $"[{i}] : {ctx.RoadCount}";
                        }
                        else
                            label = $"[{i}]";

                        return (ctx.Point, label);
                    });
                if (this.turnerConfig.DumpRestoredData)
                {
                    TrackWriter.WriteLabeled(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory,
                        $"recreated-line-{label}.kml"), points.Select(it => it.Item1).ToList(), null);
                    TrackWriter.WriteLabeled(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory,
                        $"recreated-points-{label}.kml"), null, points);
                }
            }
        }

        private IEnumerable<(RoadIndex<TRoadId> turn, RoadIndex<TRoadId> adj_arm)> getAlternateArms(TrackStepContext<TNodeId, TRoadId> trackStepContext,
            RoadIndex<TRoadId> incoming, RoadIndex<TRoadId> outgoing)
        {
            foreach (var road_idx in this.Map.GetRoadsAtNode(trackStepContext.NodeId))
            {
                bool is_same_arm(in RoadIndex<TRoadId> a, in RoadIndex<TRoadId> b)
                {
                    var a_node = this.Map.GetNode(a);
                    var b_node = this.Map.GetNode(b);
                    return EqualityComparer<TNodeId>.Default.Equals(a_node, b_node);
                };
                /*EqualityComparer<TRoadId>.Default.Equals(a.RoadId, road_idx.RoadId)
                       && EqualityComparer<TRoadId>.Default.Equals(a.RoadId, b.RoadId) // same road
                       // we cannot compare direct indices or nodes, because our track does not
                       // have to contain all the nodes from the given map road
                       && Math.Sign(road_idx.IndexAlongRoad - a.IndexAlongRoad) == Math.Sign(road_idx.IndexAlongRoad - b.IndexAlongRoad); // same side of the turn point*/

                // alternate road can have two arms
                if (Map.TryGetPrevious(road_idx, out RoadIndex<TRoadId> alt_prev_sibling))
                {
                    if (!is_same_arm(alt_prev_sibling, incoming) 
                        && !is_same_arm(alt_prev_sibling, outgoing))
                        yield return (road_idx, alt_prev_sibling);
                }

                if (Map.TryGetNext(road_idx, out RoadIndex<TRoadId> alt_next_sibling))
                {
                    if (!is_same_arm(alt_next_sibling, incoming) 
                        && !is_same_arm(alt_next_sibling, outgoing))
                        yield return (road_idx, alt_next_sibling);
                }
            }
        }


        private IEnumerable<(int index, string reason)> computeCrossroadsIndices(IReadOnlyList<TrackStepContext<TNodeId, TRoadId>> roadAssignments,
            IReadOnlySet<int> roundaboutExits)
        {
            for (int i = 1; i < roadAssignments.Count - 1; ++i)
            {
                TrackStepContext<TNodeId, TRoadId> trackStepContext = roadAssignments[i];

                if (trackStepContext.CycleWayExit || trackStepContext.CyclewaySwitch)
                {
                    ;
                }
                else if (trackStepContext.Place.IsRoundaboutCenter)
                {
                    ; // for roundabouts we have special treatment
                }
                else if ( trackStepContext.Place.IsRoundaboutLink)
                {
                    ; // assumption that right on roundabout link we do not make turn
                }
                else if (roundaboutExits.Contains(i))
                {
                    // currently we are reporting turns on roundabout centers, not entrances/exits
                    // this is also for such cases as split entrance/exit
                    // O>-
                    // without this rule program would report turn on the split point
                    ;
                }
                else if (trackStepContext.RoadCount == 1)
                {
                    ;
                }
                else if (trackStepContext.RoadCount > 2)
                {
                    yield return (i, "multi-road");
                }
                else
                {
                    var road_indices_at_node = this.Map.GetRoadsAtNode(trackStepContext.NodeId);
                    if (isEndRoad(road_indices_at_node.First()) && isEndRoad(road_indices_at_node.Last()))
                    {
                        // just an extension of the roads
                        ;
                    }
                    else
                    {
                        // T-juction of two roads, thus it is a turn here
                        yield return (i, "T-junction");
                    }
                }
            }
        }

        private bool isEndRoad(in RoadIndex<TRoadId> roadIdx)
        {
            var road_info = this.Map.GetRoadInfo(roadIdx.RoadId);
            return roadIdx.IndexAlongRoad == 0 || roadIdx.IndexAlongRoad == road_info.Nodes.Count - 1;
        }
        
        private bool tryGetRoundabout(IReadOnlyList<TrackStepContext<TNodeId, TRoadId>> track, int trackIndex,
            int direction, [MaybeNullWhen(false)] out TRoadId? roundaboutId)
        {
            if (direction != -1 && direction != 1)
                throw new ArgumentOutOfRangeException($"Invalid direction {direction}");

            for (; trackIndex >= 0 && trackIndex < track.Count; trackIndex += direction)
                if (track[trackIndex].RoundaboutId is { } road_id)
                {
                    roundaboutId = road_id;
                    return true;
                }

            roundaboutId = null;
            return false;
        }

        private bool reachesRoundabout(RoadIndex<TRoadId> roadIndex, int direction, TRoadId roundaboutId)
        {
            if (direction != -1 && direction != 1)
                throw new ArgumentOutOfRangeException($"Invalid direction {direction}");

            IReadOnlySet<TNodeId> roundabout_nodes = Map.GetRoadInfo(roundaboutId).Nodes.ToHashSet();

            var road_info = Map.GetRoadInfo(roadIndex.RoadId);
            for (int idx = roadIndex.IndexAlongRoad; idx >= 0 && idx < road_info.Nodes.Count; idx += direction)
                if (roundabout_nodes.Contains(road_info.Nodes[idx]))
                {
                    return true;
                }

            return false;
        }

        private (TurnNotification forward, TurnNotification backward) isTurnNeeded(IReadOnlyList<TrackStepContext<TNodeId, TRoadId>> track,
            IReadOnlySet<int> turnIndices,
            int nodeIndex,
            in TrafficDirection<TRoadId> incomingDirection,
            in TrafficDirection<TRoadId> outgoingDirection,
            in RoadIndex<TRoadId> altTurn, in RoadIndex<TRoadId> altSibling,
            in GeoZPoint turnPoint,
            in RoadInfo<TNodeId> incomingSegmentInfo, TNodeId incomingNode,
            in RoadInfo<TNodeId> outgoingSegmentInfo, TNodeId outgoingNode,
            in RoadInfo<TNodeId> altInfo, TNodeId altSiblingNode)
        {
            TurnNotification? forward_resolved = null;
            TurnNotification? backward_resolved = null;

            // general assumption is we can go both ways along the track no matter if this is one way or not
            // but we have to ride according to the rules when considering alternative
            var alt_direction = Map.GetTrafficDirection(altTurn, altSibling);
            if (!alt_direction.HasValue)
            {
                this.problemMessage ??= alt_direction.ProblemMessage;
                var turn_note = TurnNotification.Pass("Unknown road direction");
                return (turn_note, turn_note);
            }
            else if (!alt_direction.Value.Forward)
            {
                if (incomingDirection.Forward && outgoingDirection.Forward)
                {
                    // we can reject alternative road only if our own route is legitimate
                    logger.Verbose($"One direction, rejecting forward");
                    forward_resolved ??= TurnNotification.Pass("One way forward");
                }

                if (incomingDirection.Backward && outgoingDirection.Backward)
                {
                    // we can reject alternative road only if our own route is legitimate
                    logger.Verbose($"One direction, rejecting backward");
                    backward_resolved ??= TurnNotification.Pass("One way backward");
                }
            }

            if (forward_resolved.HasValue && backward_resolved.HasValue)
                return (forward_resolved.Value, backward_resolved.Value);

            // if we are on cycle way and we continue to ride on cycleway, then we don't need any turn notification
            if (incomingSegmentInfo.Kind == WayKind.Cycleway
                && outgoingSegmentInfo.Kind == WayKind.Cycleway
                // surface continuation is important visual cue
                && incomingSegmentInfo.Surface == outgoingSegmentInfo.Surface
                && altInfo.Kind != WayKind.Cycleway)
            {
                var turn_note = TurnNotification.Pass("Split to cycleway");
                return (forward_resolved ?? turn_note, backward_resolved ?? turn_note);
            }

            var alt_rank = new RoadRank<TNodeId>(altInfo);

            //logger.Verbose($"Incoming corrected = {track[nodeIndex].BackwardCycleWayCorrected}, outgoing corrected = {track[nodeIndex].ForwardCycleWayCorrected}");

            var incoming_rank = track[nodeIndex].BackwardCycleWayCorrected ? RoadRank<TNodeId>.CyclewayLink(incomingSegmentInfo) : new RoadRank<TNodeId>(incomingSegmentInfo);
            var outgoing_rank = track[nodeIndex].ForwardCycleWayCorrected ? RoadRank<TNodeId>.CyclewayLink(outgoingSegmentInfo) : new RoadRank<TNodeId>(outgoingSegmentInfo);

            GeoZPoint current_point = Map.GetPoint(incomingNode);
            GeoZPoint next_point = Map.GetPoint(outgoingNode);
            GeoZPoint alt_sibling_point = Map.GetPoint(altSiblingNode);

            // consider we are coming from right
            // _L 
            // if L is primary road and we ride along it -- no turn is need
            // if we go in horizontal line -- turn is needed
            var is_more_important = incoming_rank.IsMoreImportantThan(alt_rank) && outgoing_rank.IsMoreImportantThan(alt_rank);
            var is_significantly_more_important = incoming_rank.IsSignificantlyMoreImportantThan(alt_rank) 
                                                  && outgoing_rank.IsSignificantlyMoreImportantThan(alt_rank);
            if (is_significantly_more_important)
            {
                // ashpalted tertiary roads are too similar to lower ranked roads (when also asphalted) so we need
                // more significant difference. 
                
                logger.Verbose($"Alt arm has lower priority {alt_rank} than both arms {incoming_rank}, {outgoing_rank} of our track");
                var turn_note = TurnNotification.Pass("Alt road low rank");
                return (forward_resolved ?? turn_note, backward_resolved ?? turn_note);
            }

            // exceptions:
            // _|_                
            // let's say | is primary but we go in horizontal line, despite priorities of the road there should be no turn (because we maintain course)

            {
                int debug_id = DEBUG_points.Count;
                DEBUG_points.Add((current_point, $"c {nodeIndex - 1} {debug_id} {incoming_rank} {incomingNode}"));
                DEBUG_points.Add((next_point, $"n {nodeIndex + 1} {debug_id} {outgoing_rank} {outgoingNode}"));
                DEBUG_points.Add((turnPoint, $"t {nodeIndex} {debug_id}"));
                DEBUG_points.Add((alt_sibling_point, $"a {debug_id} {alt_rank} {altSiblingNode}"));
            }

            bool incoming_uncertain = track[nodeIndex - 1].ForwardCyclewayUncertain;
            bool outgoing_uncertain = track[nodeIndex + 1].BackwardCyclewayUncertain;

            logger.Verbose($"Checking transition {(incoming_uncertain ? "?" : "")}{incoming_rank} -> {(outgoing_uncertain ? "?" : "")}{outgoing_rank}");

            if (incoming_uncertain || outgoing_uncertain)
            {
                var turn_note = TurnNotification.Alert($"Uncertain roads incoming:{incoming_uncertain}, outgoing:{outgoing_uncertain}");
                return (forward_resolved ?? turn_note, backward_resolved ?? turn_note);
            }

            // we can go without turn notification (for example) from path to highway, but not from highway to path
            //                bool forward = incoming_segment_kind < outgoing_segment_kind;
            //              bool backward = incoming_segment_kind > outgoing_segment_kind;
            //TurnNotification forward = TurnNotification.Ignore("Initial whether needed");
            // TurnNotification backward = forward;

            if (incoming_rank.IsMoreImportantThan(outgoing_rank))
                forward_resolved ??= TurnNotification.Alert("Coming into lesser road");
            if (outgoing_rank.IsMoreImportantThan(incoming_rank))
                backward_resolved ??= TurnNotification.Alert("Returning into lesser road");

            // if (forward.Enable && backward.Enable) // this case should not ever happen
            //   return (forward, backward);

            // logger.Verbose($"Initial turn notifications forward {forward}, backward {backward}");

            // if any priority difference suggests no need for notification, let's check if our track is pretty straight at this point

            GeoZPoint incoming_point;
            if (!tryGetPointAlongTrack(track, turnIndices, nodeIndex, -1, this.userPreferences.TurnArmLength,
                    out incoming_point))
                incoming_point = current_point;
            GeoZPoint outgoing_point;
            if (!tryGetPointAlongTrack(track, turnIndices, nodeIndex, +1, this.userPreferences.TurnArmLength, out outgoing_point))
                outgoing_point = next_point;

            isTurnNeededOnCurvedTrack(nodeIndex,
                turnPoint, incoming_point,
                outgoing_point,
                alt_sibling_point,
                incomingSegmentInfo,
                outgoingSegmentInfo,
                new[] {altInfo},
                isRouteRoadImportant:is_more_important,
                isAltMinor: altInfo.Kind > incomingSegmentInfo.Kind && altInfo.Kind > outgoingSegmentInfo.Kind,
                ref forward_resolved, ref backward_resolved);

            if ((forward_resolved ?? default).Enabled)
            {
                // consider road around roundabout like this
                // O>
                // so if we detect our track is against the traffic and our track leads to roundabout and the alternate road leads to the same roundabout
                // are not at the "real" crossroad but at the split roads leading to the same roundabout, so there should be no turn-notification, because
                // you cannot choose the road, one way is FROM roundabout, the other is TO roundabout, you ride the one you has to ride
                if (!outgoingDirection.Forward
                    && tryGetRoundabout(track, nodeIndex, +1, out var roundabout_id)
                    && reachesRoundabout(altTurn, Math.Sign(altSibling.IndexAlongRoad - altTurn.IndexAlongRoad), roundabout_id!.Value))
                {
                    forward_resolved ??= TurnNotification.Pass("Forward rounabout link");
                    logger.Verbose("Rejecting alternate road because both track and alt are links to roundabout");
                }
            }

            if ((backward_resolved ?? default).Enabled)
            {
                if (!incomingDirection.Forward
                    && tryGetRoundabout(track, nodeIndex, -1, out var roundabout_id)
                    && reachesRoundabout(altTurn, Math.Sign(altSibling.IndexAlongRoad - altTurn.IndexAlongRoad), roundabout_id!.Value))
                {
                    backward_resolved ??= TurnNotification.Pass("Backward rounabout link");
                    logger.Verbose("Rejecting alternate road because both track and alt are links to roundabout");
                }
            }

            return (forward_resolved ?? TurnNotification.Pass("No need"),
                backward_resolved ?? TurnNotification.Pass("No need"));
        }

        // this recomputes angle distance from range (0,360) to (-180,+180) which is easier to say what side it is, -180/+180 angle means dead ahead
        private Angle signedAngleDistance(in GeoZPoint center, in GeoZPoint start, in GeoZPoint end)
        {
            Angle dist = calc.AngleDistance(center, start, end);
            return dist <= Angle.PI ? dist : dist - Angle.FullCircle;
        }

        private void isTurnNeededOnCurvedTrack(int trackIndex,
            in GeoZPoint turnPoint, in GeoZPoint currentPoint,
            in GeoZPoint nextPoint,
            in GeoZPoint altSiblingPoint,
            in RoadInfo<TNodeId>? incomingRoadInfo,
            in RoadInfo<TNodeId>? outgoingRoadInfo,
            in IEnumerable<RoadInfo<TNodeId>> altRoadInfos,
            bool isRouteRoadImportant,
            bool isAltMinor, ref TurnNotification? forward, ref TurnNotification? backward)
        {
            #if DEBUG
            if (trackIndex == 3)
            {
                ;
            }
            #endif
            var track_curved_message = isTrackCurved(turnPoint, currentPoint, nextPoint, 
                out var track_angle);
            if (track_curved_message != null)
            {
                //logger.Verbose(track_curved_message);
                if (this.turnerConfig.DebugDirectory != null && this.turnerConfig.DumpTurnAngles)
                {
                    TrackWriter.Build(new[] {currentPoint, turnPoint, nextPoint}, new[] {turnPoint})
                        .Save(Navigator.GetUniquePath(this.turnerConfig.DebugDirectory, $"turn-{trackIndex}-angled.kml"));
                }

                var turn_note = TurnNotification.Alert(track_curved_message);
                forward ??= turn_note;
                backward ??= turn_note;
            }

            if (forward.HasValue && backward.HasValue)
                return;

            bool alt_angle_makes_turn(Angle trackAngle, Angle altAngle)
            {
                if (trackAngle.Sign() == altAngle.Sign()) // same side
                {
                    logger.Verbose($"Same sides, alt angle {altAngle}");
                    return altAngle.Abs() > trackAngle.Abs();
                }
                else // opposite sides
                {
                    Angle diff = trackAngle.Abs() - altAngle.Abs();
                    Angle diff_limit = this.userPreferences.GetAltAngleDifferenceLimit(trackAngle); 
                    if (isAltMinor)
                        diff_limit -= this.userPreferences.AltMinorAngleSlack;
                    bool turn_needed = diff <= diff_limit;
                    logger.Verbose($"Opposite sides, alt angle {altAngle}, diff {diff}, limit {diff_limit}, turn {turn_needed}");
                    return turn_needed;
                }
            }
            // ok, our track looks flat at this turn, so we could skip turn-notification, but now the question is whether alternate road is not straight line as well

            // if we use the same road, and the alternate is just cycleway, ignore it
            if (incomingRoadInfo != null
                && outgoingRoadInfo != null
                && incomingRoadInfo.Value.Kind == outgoingRoadInfo.Value.Kind
                // our route cannot be on cycleway to skip turn, because we couldn't tell the difference
                && incomingRoadInfo.Value.Kind != WayKind.Cycleway
                && altRoadInfos.All(it => it.Kind == WayKind.Cycleway))
            {
                return;
            }

            #if DEBUG
            if (trackIndex == 14)
            {
                ;
            }
            #endif
            var short_regular_reason = $"Alt road turns";
            var long_regular_reason = $"{short_regular_reason}. From {currentPoint.Convert2d()}, via {turnPoint.Convert2d()} to {nextPoint.Convert2d()}, alt {altSiblingPoint.Convert2d()}";

            // 2023-05-07: for "inversed" check the logic is as follows -- we are on important road, but it is not
            // significantly important so if our route makes sharp turn (compared to alternate one) than make a turn
            // notification however if if our route is almost straight, the user while riding should notice which way
            // to go (well, we will see this in practice)
            
            var short_inversed_reason = $"Route road sharp turn";
            var long_inversed_reason = $"{short_inversed_reason}. From {currentPoint.Convert2d()}, via {turnPoint.Convert2d()} to {nextPoint.Convert2d()}, alt {altSiblingPoint.Convert2d()}";
            
            if (!forward.HasValue)
            {
                var alt_angle = signedAngleDistance(turnPoint, currentPoint, altSiblingPoint);
                if (isRouteRoadImportant)
                {
                    // 2023-05-08: the idea is as follow, if we are going important road, but not obviously one
                    // and we got into unclear junction this should wake us up, and we will pay attention to follow
                    // the important road. But if the turn is so sharp, and the alternate road is straight
                    // it could mislead us and we follow the alternate one -- so to prevent this we add turn notification
                    if (this.userPreferences.IsObviouslyStraightInComparison(alt_angle,track_angle ))
                    {
                        logger.Verbose($"Forward: {long_inversed_reason}");
                        forward = TurnNotification.Alert(short_inversed_reason);
                    }
                }
                else if (alt_angle_makes_turn(track_angle, alt_angle))
                {
                    logger.Verbose($"Forward: {long_regular_reason}");
                    forward = TurnNotification.Alert(short_regular_reason);
                }
            }

            // we checked riding forward/along the track, now we have to check riding backwards (we don't know which direction we will be riding)

            // watch the changed sign, now we are riding back, from next to current
            if (!backward.HasValue)
            {
                var alt_angle = signedAngleDistance(turnPoint, nextPoint, altSiblingPoint);
                if (isRouteRoadImportant)
                {
                    if (this.userPreferences.IsObviouslyStraightInComparison(alt_angle,track_angle ))
                    {
                        logger.Verbose($"Backward: {long_inversed_reason}");
                        backward = TurnNotification.Alert(short_inversed_reason);
                    }
      
                }
                else if (alt_angle_makes_turn(-track_angle, alt_angle))
                {
                    logger.Verbose($"Backward: {long_regular_reason}");
                    backward = TurnNotification.Alert(short_regular_reason);
                }
            }
        }

        private string? isTrackCurved(GeoZPoint turnPoint, GeoZPoint prevPoint, GeoZPoint nextPoint,
            out Angle trackAngle)
        {
            static string format(Angle angle)
            {
                return angle.Degrees.ToString("0.#")+"°";
            }
            trackAngle = signedAngleDistance(turnPoint, prevPoint, nextPoint);
            if (trackAngle.Abs() <= this.userPreferences.StraigtLineAngleLimit)
                return $"curved track {format(trackAngle)} below limit {format(this.userPreferences.StraigtLineAngleLimit)}";
            else
                return null;
        }
    }
}