﻿namespace TrackPlanner.Turner
{
   
    public sealed class SystemTurnerConfig
    {
        public string? DebugDirectory { get; set; }
        public bool DumpCrossIntersections { get; set; }
        public bool DumpRestoredData { get; set; }
        public bool DumpPossibleTurns { get; set; }
        public bool DumpRoundaboutInfo { get; set; }
        public bool DumpTurnAngles { get; set; }
        public bool DumpTurns { get; set; }
        public bool DumpPoints { get; set; }

        public SystemTurnerConfig()
        {
        }
    }

}