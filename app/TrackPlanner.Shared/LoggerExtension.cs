﻿using System;

namespace TrackPlanner.Shared 
{
    public static class LoggerExtension
    {
        public static string ExtractDetails(this Exception ex, string? message =null)
        {
            if (message != null)
                message += Environment.NewLine;
            else
                message = "";
            message += ex.Message + Environment.NewLine + ex.StackTrace;
            return message;
        }
        public static void LogDetails(this Exception ex,ILogger logger, MessageLevel level, string? message =null)
        {
            logger.Log(level,ex.ExtractDetails(message));
        }
        public static void Info(this ILogger logger, string message)
        {
            logger.Log(MessageLevel.Info, message);
        }
        public static void Error(this ILogger logger, string? message)
        {
            logger.Log(MessageLevel.Error, message);
        }
        public static void Verbose(this ILogger logger, string message)
        {
            logger.Log(MessageLevel.Debug, message);
        }
        public static void Warning(this ILogger logger, string message)
        {
            logger.Log(MessageLevel.Warning, message);
        }
    }

}