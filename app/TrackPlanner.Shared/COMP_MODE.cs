﻿namespace TrackPlanner.Shared
{
    public static class COMP_MODE
    {
        public static bool PhantomRounabouts { get; } = true;
        public static bool WeightedTurns { get; set; } = true;

        public static bool UniTestWeightedTurns => WeightedTurns;

    }
}