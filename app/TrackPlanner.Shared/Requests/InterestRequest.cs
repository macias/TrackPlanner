﻿using System.Collections.Generic;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Requests
{
    public sealed class InterestRequest
    {
        // simplification, we only send anchor points, not actual route
        public List<GeoPoint> CheckPoints { get; set; } = default!;
        public Length Range { get; set; }
        
        // rationale for exclusion mode instead of inclusion. Let's say we are completely not interested in graves
        // with inclusion notion there is no way to express this, beause a place with "archeological site + grave"
        // would still be returned. However once we go with exclusion we can say "grave" and no matter with what it
        // is mixed, we won't see any grave
        public PointOfInterest.Feature ExcludeFeatures { get; set; }

        public InterestRequest()
        {
        }

    }
}