using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Requests
{
    public sealed class SaveRequest<TNodeId,TRoadId>
        where TNodeId:struct
        where TRoadId: struct
    {
        public ScheduleJourney<TNodeId,TRoadId> Schedule { get; set; } = default!;
        public string Path { get; set; } = default!;

        public SaveRequest()
        {
            
        }
    }

}