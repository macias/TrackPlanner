﻿using Geo;
using MathUnit;

namespace TrackPlanner.Shared.Requests
{
    public sealed class PeaksRequest
    {
        public GeoPoint FocusPoint { get; set; }
        public Length SearchRange { get; set; }
        public Length SeparationDistance { get; set; }
        public int Count { get; set; }

        public PeaksRequest()
        {
        }
    }
}