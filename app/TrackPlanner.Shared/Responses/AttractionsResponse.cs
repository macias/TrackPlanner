﻿using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Responses
{
    public sealed  class AttractionsResponse
    {
        public List<List<PlacedAttraction>> Attractions { get; set; } = default!;

        public AttractionsResponse()
        {
        }
    }
}
