﻿using System.Collections.Generic;
using MathUnit;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Responses
{
    public sealed class PeaksResponse<TNodeId,TRoadId>
        where TNodeId : struct
    {
        public Length BaseHeight { get; set; }
        public MapZPoint<TNodeId,TRoadId>[] Peaks { get; set; } = default!;

        public PeaksResponse()
        {
        }
    }
}
