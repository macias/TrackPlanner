﻿using System;

namespace TrackPlanner.Shared
{
   
    
    #if DEBUG
    public static class DEBUG_SWITCH
    {
        public static TimeSpan Timeout => TimeSpan.FromSeconds(DEBUG_SWITCH.Enabled ? 5000 *60: 1*5*60);
        public static bool Enabled { get; set; }
        public static bool SubEnabled { get; set; }
        public static bool SubSubEnabled2 { get; set; }
        public static int Iteration { get; } = 23;
        public static (int, int) ElemIndex { get; } = (0, 3);
    }
    #endif
}