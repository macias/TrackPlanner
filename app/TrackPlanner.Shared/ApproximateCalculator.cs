﻿using System;
using Geo;
using MathUnit;
using System.Runtime.CompilerServices;
using TrackPlanner.Shared.Data;

[assembly: InternalsVisibleTo("TrackTurner.Tests")] 

namespace TrackPlanner.Shared
{
    public sealed class ApproximateCalculator : IGeoCalculator
    {
        private static double dist2(double xA, double yA, double xB, double yB)
        {
            return Math.Pow(xA - xB, 2) + Math.Pow(yA - yB, 2);
        }
        
        public double DistanceToSegmentSquared(double xPoint,double yPoint,
            double xSegmentStart,double ySegmentStart, double segmentEndX,double ySegmentEnd) 
        {
            // https://stackoverflow.com/a/1501725/210342
            var seg_len = dist2(xSegmentStart,ySegmentStart, segmentEndX,ySegmentEnd);
            if (seg_len == 0) 
                return dist2(xPoint,yPoint, xSegmentStart,ySegmentStart);
            var t = ((xPoint - xSegmentStart) * (segmentEndX - xSegmentStart) 
                     + (yPoint - ySegmentStart) * (ySegmentEnd - ySegmentStart)) / seg_len;
            t = Math.Max(0, Math.Min(1, t));
            return dist2(xPoint,yPoint,  
                xSegmentStart + t * (segmentEndX - xSegmentStart), ySegmentStart + t * (ySegmentEnd - ySegmentStart) );
        }
        
        public static Length GetTrueDistance(Length distance, Length altitudeFrom, Length targetAltitude)
        {
            if (altitudeFrom == targetAltitude)
                return distance;
            else
                return Length.FromMeters(Math.Sqrt(Math.Pow(distance.Meters,2)
                                                   +Math.Pow((altitudeFrom-targetAltitude).Meters,2)));
        }
        public Length GetDistance(in GeoZPoint a, in GeoZPoint b)
        {
            return GetFlatDistance(a.Convert2d(), b.Convert2d());
        }

        public Length GetTrueDistance(in GeoZPoint a, in GeoZPoint b)
        {
            return GetTrueDistance(GetFlatDistance(a.Convert2d(), b.Convert2d()), a.Altitude, b.Altitude);
        }

        public Length GetFlatDistance(in GeoPoint a, in GeoPoint b)
        {
            return GeoCalculator.GetDistance(a, b);
        }

        public GeoZPoint OppositePoint(in GeoZPoint p)
        {
            return GeoCalculator.OppositePoint(p.Convert2d()).Convert3d(p.Altitude);
        }

        public Region GetBoundary(Length range, params GeoPoint[] points)
        {
            if (points.Length == 0)
                throw new ArgumentException();

            var region = Region.Build(points);

            var lat_dist = getLatitudeDifference(range);
            var north_dist = GeoCalculator.GetLongitudeDifference(region.North, range);
            var south_dist = GeoCalculator.GetLongitudeDifference(region.South, range);
            var lon_dist = north_dist.Max(south_dist);
            
            return region.Expand(byLatitude: lat_dist, byLongitude: lon_dist);
        }

        public bool TryStabilizeUserData(GeoPoint rawData,out GeoPoint stable,out string textual)
        {
            var result = TryStabilize(rawData, out stable);
            textual = DataFormat.FormatUserInput(stable)!;
            return result;
        }

        public bool TryStabilize(GeoPoint point, out GeoPoint stable)
        {
            for (int i = 0; i < 3; ++i)
            {
                var pt = DataFormat.TryParseUserPoint(DataFormat.FormatUserInput(point));
                if (pt == point)
                {
                    stable = point;
                    return true;
                }

                point = pt!.Value;
            }

            stable = default;
            return false;
        }


        public (Length distance, GeoZPoint crosspoint, Length distanceAlongSegment) GetFlatDistanceToArcSegment(in GeoZPoint point, 
            in GeoZPoint segmentStart, in GeoZPoint segmentEnd,bool stable)
        {
            // todo: expose modified version of GeoCalculator.GetDistanceToArcSegment 

            // this is very cheap hack in order to get stable partial rebuild
            // scenario: you build one leg, it starts and ends somewhere on the road (crosspoint or exact node)
            // then you need to build another road starting/ending _exactly_ at the point of the previous leg.
            // So in theory when we compute crosspoint from crosspoint we would get back this crosspoint, but because
            // of numerical rounding the values fluctuates slightly, and then program reports error there is gap
            // between legs
            // The (hacky) solution -- let's iterate computation until we get such crosspoint that computing crosspoint
            // from it, we will get itself. Of course there could be infinite loop, but so far it solved the problem
            // and cheaply
            
            Length distance_along_segment = Length.PositiveInfinity;
            Length? distance = null;
            GeoPoint cx = default;
            GeoZPoint init_point = point;
            for (int i = 0;; ++i)
            {
                // 2023-04-04 increased number of iterations from 3 to 5,
                // todo: this needs another approach badly
                if (i == 5)
                    break;
                var dist = GeoCalculator.GetDistanceToArcSegment(init_point.Convert2d(), segmentStart.Convert2d(), segmentEnd.Convert2d(),
                    out cx, out Length dist_along);
                if (distance == null)
                {
                    distance = dist;
                }
                distance_along_segment = dist_along;

                if (!stable || cx.Convert3d().IsSame2d(init_point))
                    break;
                else
                    init_point = cx.Convert3d();
            }

            var diff = segmentEnd.Altitude - segmentStart.Altitude;
            var total_dist = GetFlatDistance(segmentStart.Convert2d(), segmentEnd.Convert2d());
            return (distance!.Value, cx.Convert3d(segmentStart.Altitude+diff*(distance_along_segment/total_dist)),
                distance_along_segment);
        }

        public bool CheckArcSegmentIntersection(in GeoZPoint startA, in GeoZPoint endA, in GeoZPoint startB, in GeoZPoint endB, out GeoZPoint crosspoint)
        {
            GeoCalculator.GetArcSegmentIntersection(startA.Convert2d(), endA.Convert2d(), startB.Convert2d(), endB.Convert2d(),
                       out GeoPoint? cx1, out GeoPoint? cx2);
            crosspoint = cx1?.Convert3d() ?? default;
            return cx1.HasValue;
        }

        /*public double SquareDistance(in GeoZPoint a, in GeoZPoint b)
        {
            Angle shortest_distance(Angle a,Angle b)
            {
                Angle diff = (a - b).Normalize();
                return diff <= Angle.PI ? diff : Angle.FullCircle - diff;
            }
            return Math.Pow(shortest_distance(a.Latitude, b.Latitude).Radians, 2) + Math.Pow(shortest_distance(a.Longitude, b.Longitude).Radians, 2);
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>angle in (0,360) range. Note angles are computed counterclockwise</returns>
        public Angle AngleDistance(in GeoZPoint center, in GeoZPoint a, in GeoZPoint b)
        {
            // https://stackoverflow.com/questions/1211212/how-to-calculate-an-angle-from-three-points
            // https://math.stackexchange.com/questions/878785/how-to-find-an-angle-in-range0-360-between-2-vectors

            /*            var x1 = (a.Longitude - center.Longitude).Radians;
                        var y1 = (a.Latitude - center.Latitude).Radians;
                        var x2 = (b.Longitude - center.Longitude).Radians;
                        var y2 = (b.Latitude - center.Latitude).Radians;

                        // this is buggy, because of the around 0 angle issue (e.g. a is 359, b is 1 -- you either get too negative values, or too big)
                        var dot = x1 * x2 + y1 * y2;     // dot product
                        var det = x1 * y2 - y1 * x2;    // determinant
                        var angle = Math.Atan2(det, dot);  // atan2(y, x) or atan2(sin, cos)

                        return Angle.FromRadians(angle).Normalize();
            */

            var center_conv = center.Convert2d();
            Angle bearing_a = GetBearing( center,a);
            Angle bearing_b = GeoCalculator.GetBearing(center_conv, b.Convert2d());

            return (bearing_a- bearing_b).Normalize();
        }

        public Angle GetBearing(GeoZPoint from,GeoZPoint to )
        {
            return GeoCalculator.GetBearing(from.Convert2d(),to.Convert2d());
        }

        public Angle GetNormalizedBearingDistance(Angle startAngle, Angle endAngle)
        {
            return (endAngle.Normalize()-startAngle.Normalize()).Normalize();
        }

        public Angle GetAbsoluteBearingDifference(Angle bearingA, Angle bearingB)
        {
            var bearing_dist = GetNormalizedBearingDistance(bearingA, bearingB);
            return bearing_dist <= Angle.PI ? bearing_dist : Angle.FullCircle - bearing_dist;

        }

        /*  public GeoZPoint GetDestination(GeoZPoint start, Angle bearing, Length distance)
          {
              return GeoCalculator.GetDestination(start.Convert(), bearing, distance).Convert();
          }*/

        public void GetAngularDistances(GeoZPoint point, Length distance, 
            out Angle latitudeDistance, out Angle longitudeDistance)
        {
            latitudeDistance = getLatitudeDifference(distance);
            // with increasing latitude (Y) the radius of the earth cicle is smaller, thus we have to compute it
            longitudeDistance = GeoCalculator.GetLongitudeDifference(point.Latitude, distance);
        }

        private static Angle getLatitudeDifference(Length distance)
        {
            return Angle.FullCircle * (distance / GeoCalculator.EarthCircumference);
        }

        public GeoZPoint GetMidPoint(in GeoZPoint a, in GeoZPoint b)
        {
            GeoPoint pt = GeoCalculator.GetMidPoint(a.Convert2d(), b.Convert2d());
            return GeoZPoint.Create(pt.Latitude, pt.Longitude, (a.Altitude + b.Altitude) / 2);
        }

        // do our track crosses other road in shape of (more or less) like "-|-"
        // note, it is not about cross geometry
        // two roads
        // 
        // _|
        // and
        //     _
        //   |
        // would form a cross too but no of those roads CROSSES the others

        public bool IsCrossIntersection(in GeoZPoint center, in GeoZPoint incomingTrack, in GeoZPoint outgoingTrack, in GeoZPoint leftArmPoint, in GeoZPoint rightArmPoint, Angle angleSeparation,
            out Angle inLeftAngle, out Angle inRightAngle, out Angle outLeftAngle, out Angle outRightAngle)
        {
            int angle_side(Angle angle) => (angle.Normalize() - Angle.PI).Sign();

            // basically we compute direction, the arms should go -- track, other, track, other (clockwise, or counterclockwise) -- then we have crossing
            Angle outgoing_bearing = GeoCalculator.GetBearing(center.Convert2d(), outgoingTrack.Convert2d());
            Angle incoming_bearing = GeoCalculator.GetBearing(center.Convert2d(), incomingTrack.Convert2d());

            Angle right_bearing = GeoCalculator.GetBearing(center.Convert2d(), rightArmPoint.Convert2d());
            Angle left_bearing = GeoCalculator.GetBearing(center.Convert2d(), leftArmPoint.Convert2d());

            Angle bearing_diff(Angle bearing1, Angle bearing2) { var dist = (bearing1 - bearing2).Normalize(); return dist <= Angle.PI ? dist : Angle.FullCircle - dist; };

            inLeftAngle = bearing_diff(incoming_bearing, left_bearing);
            inRightAngle = bearing_diff(incoming_bearing, right_bearing);
            outLeftAngle = bearing_diff(outgoing_bearing, left_bearing);
            outRightAngle = bearing_diff(outgoing_bearing, right_bearing);
            if (inLeftAngle < angleSeparation || inRightAngle < angleSeparation || outLeftAngle < angleSeparation || outRightAngle < angleSeparation)
                return false;

            int angle_dir = angle_side(right_bearing - outgoing_bearing);

            if (angle_side(incoming_bearing - right_bearing) != angle_dir)
                return false;

            if (angle_side(left_bearing - incoming_bearing) != angle_dir)
                return false;

            if (angle_side(outgoing_bearing - left_bearing) != angle_dir)
                return false;


            return true;
        }

        public Angle GetBearing(in GeoZPoint start, in GeoZPoint dest)
        {
            return GeoCalculator.GetBearing(start.Convert2d(), dest.Convert2d());
        }

        public GeoZPoint PointAlongSegment(in GeoZPoint start, in GeoZPoint dest, Length length)
        {
            Angle bearing = this.GetBearing(start, dest);
            return GeoCalculator.GetDestination(start.Convert2d(), bearing, length).Convert3d();
        }
    }

}
