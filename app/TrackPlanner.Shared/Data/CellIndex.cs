﻿using System;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct CellIndex 
    {
        public static CellIndex Create(Angle latitude, Angle longitude,int cellSize)
        {
            return new CellIndex(latitudeGridIndex: (int) (latitude.Degrees * cellSize),
                longitudeGridIndex: (int) (longitude.Degrees * cellSize));
        }

        public static (Angle lat, Angle lon) Recreate(int latIndex, int lonIndex, int cellSize)
        {
            return (Angle.FromDegrees(latIndex * 1.0 / cellSize),
                Angle.FromDegrees(lonIndex * 1.0 / cellSize));
        }

        public short LatitudeGridIndex { get;  }
        public short LongitudeGridIndex { get; }

        public CellIndex(int latitudeGridIndex, int longitudeGridIndex)
        {
            if (latitudeGridIndex<short.MinValue || latitudeGridIndex > short.MaxValue)
                throw new ArgumentOutOfRangeException($"{nameof(latitudeGridIndex)} = {latitudeGridIndex}");
            if (longitudeGridIndex<short.MinValue || longitudeGridIndex > short.MaxValue)
                throw new ArgumentOutOfRangeException($"{nameof(longitudeGridIndex)} = {longitudeGridIndex}");

            this.LatitudeGridIndex = (short)latitudeGridIndex;
            this.LongitudeGridIndex = (short)longitudeGridIndex;
        }

        public void Deconstruct(out short latitudeGrid, out short longitudeGrid)
        {
            latitudeGrid = this.LatitudeGridIndex;
            longitudeGrid = this.LongitudeGridIndex;
        }

        public override string ToString()
        {
            return $"{LatitudeGridIndex}x{LongitudeGridIndex}";
        }
    }
}