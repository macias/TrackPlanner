﻿using MathUnit;
using Newtonsoft.Json;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct FragmentStep<TNodeId,TRoadId>
        where TNodeId : struct
    {
        #if DEBUG
        //private static int DEBUG_IdCounter = 0;
        //private readonly int DEBUG_Id = DEBUG_IdCounter++;
        [JsonIgnore]
        public MapRef<TNodeId, TRoadId>? DEBUG_MapRef { get; } 
        #endif
        public GeoZPoint Point { get; init; }
        public TNodeId? NodeId { get; init; }
        public Length IncomingFlatDistance { get; init; }

        public FragmentStep( 
#if DEBUG
         MapRef<TNodeId,TRoadId>? DEBUG_mapRef, 
#endif
            GeoZPoint point, TNodeId? nodeId,Length incomingFlatDistance)
        {
            #if DEBUG
            this.DEBUG_MapRef = DEBUG_mapRef;
            /*if (this.DEBUG_Id == 165)//166)
            {
                ;
            }*/
            #endif
            Point = point;
            NodeId = nodeId;
            IncomingFlatDistance = incomingFlatDistance;
        }

        public FragmentStep(
#if DEBUG
            MapRef<TNodeId,TRoadId>? DEBUG_mapRef, 
#endif
        MapZPoint<TNodeId,TRoadId> mapZPoint,Length incomingFlatDistance) 
            : this(
#if DEBUG
                DEBUG_mapRef, 
#endif
                mapZPoint.Point,mapZPoint.NodeId,
                incomingFlatDistance)
        {
        }

        public MapZPoint<TNodeId,TRoadId> GetMapPoint()
        {
            return new MapZPoint<TNodeId,TRoadId>(Point, NodeId
            #if DEBUG
                , DEBUG_MapRef
            #endif
            );
        }
    }
    
    
}