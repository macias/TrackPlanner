namespace TrackPlanner.Shared.Data;

public interface IRequestPoint
{
    bool AllowSmoothing { get; }
    bool AllowGap { get; }
}