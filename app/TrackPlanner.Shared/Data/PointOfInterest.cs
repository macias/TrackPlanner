﻿
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct PointOfInterest
    {
        public enum Category
        {
            Historic,
            Utility,
        }

        [Flags]
        public enum Feature
        {
            None = 0,
            Ruins = 1,
            ArchaeologicalSite = 2,
            Manor = 4,
            Castle = 8,
            Grave = 16,
            Church = 32,
            Mansion = 64,
            Lighthouse = 128,
            Ship = 256,
            Bunker = 512,
            Palace = 1 << 10,
            Tower = 1 << 11,
            Chapel = 1 << 12,
            Fortress = 1 << 13,
            Fortification = 1 << 14,
            Bridge = 1 << 15,
            Wall = 1 << 16,
            TrainStation = 1 << 17,
            Mill = 1 << 18,
            Pillory = 1 << 19,
            Mine = 1 << 20,
            Aircraft = 1 << 21,
            Aqueduct = 1 << 22,
            Train = 1 << 23,
            Tank = 1 << 24,
            Building = 1 << 25,

            FoodStore = 1 << 31,
        }

        public string? Name { get; }
        public string? Url { get; }
        public Feature Features { get; }

        private bool isUtility => Features.HasFlag(Feature.FoodStore);

        public PointOfInterest(string? name, string? url, Feature features)
        {
            Name = name;
            Url = url;
            Features = features;
        }

        public static Feature GetMask(Category category)
        {
            switch (category)
            {
                case Category.Historic: return ~Feature.FoodStore;
                case Category.Utility: return Feature.FoodStore;
                default: throw new NotImplementedException(category.ToString());
            }
        }


        [Pure]
        private IEnumerable<string> getFeatures()
        {
            var this_features = this.Features;
            return Enum.GetValues<Feature>()
                .Where(it => it != Feature.None && this_features.HasFlag(it))
                .Select(it => it.ToString());
        }

        public override string ToString()
        {
            string label = Name ?? "";
            if (Url != null)
                label += $"@{Url}";
            return String.Join("+",getFeatures())+$"({label})";
        }

        public (string Title,string? Description) GetDisplay()
        {
            if (isUtility)
                return (Name??"Food store", null);
            else
            {
                var title = String.Join(", ", getFeatures());
                string? description = Name;
                return (title, description);
            }
        }

        public string GetCompactLabel()
        {
            string? title = this.Name;
            var features = String.Join(", ", this.getFeatures());
            if (title == null)
                title = features;
            else if (!isUtility)
                title += $" ({features})";
            return title;
        }
        

       
    }
}