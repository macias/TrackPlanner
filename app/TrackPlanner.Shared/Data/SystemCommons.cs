﻿namespace TrackPlanner.Shared.Data
{
   public static class SystemCommons
   {
       public static string ProjectFileExtension = ".trproj";
       public static string LayerFileExtension = ".kml";
     //  public static double AutoCheckpointIntervalStretch = 1.5;
   }
}
