using System;
using System.Collections.Generic;

namespace TrackPlanner.Shared.Data
{
   
    public interface IReadOnlyDay
    {
        string? Note { get; }
        TimeSpan Start { get; }
        IReadOnlyList<IReadOnlyAnchor> Anchors { get; }
    }
  
    public interface IDay<TAnchor> : IReadOnlyDay
    where TAnchor : IAnchor
    {
       // new TimeSpan Start { get; set; }
        new IReadOnlyList<TAnchor> Anchors { get; }
    }
}