using System;
using Geo;

namespace TrackPlanner.Shared.Data
{
   
    public interface IReadOnlyAnchor
    {
        TimeSpan UserBreak { get; }
        string Label { get; }
        LabelSource LabelSource { get; }
        bool IsPinned { get; }
        bool AllowGap { get; }
        bool HasMap { get; }
        GeoPoint UserPoint { get;  }
        #if DEBUG
        bool? DEBUG_Smoothing { get; }
        bool? DEBUG_Enforce { get; }
        #endif
    }

    public interface IInterface
    {
        bool IsUserLabel { get; }
    }
   
    public interface IInterface2 :IInterface
    {
        new bool IsUserLabel { get; set; }
    }

 
}