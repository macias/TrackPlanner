﻿using System;

namespace TrackPlanner.Shared.Data
{
    // separate for nodes and roads
    public readonly record struct WorldIdentifier 
    {
        public static ushort CastIndex(int entityIndex)
        {
            if (entityIndex<ushort.MinValue || entityIndex > ushort.MaxValue)
                throw new ArgumentOutOfRangeException($"{nameof(entityIndex)} = {entityIndex}");

            return (ushort) entityIndex;
        }

        public CellIndex CellIndex { get;  }
        public ushort EntityIndex { get;  }

        public WorldIdentifier(CellIndex cellIndex, int entityIndex)
        {
            CellIndex = cellIndex;
            EntityIndex = CastIndex(entityIndex);
        }

        public override string ToString()
        {
            return $"{CellIndex}::{EntityIndex}";
        }

        
    }
    
    
}