﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public sealed class SummaryCheckpoint 
    {
        public string Code { get; set; } 
        public bool IsUserLabel { get; set; }
        public string Label { get; set; }
        public TimeSpan Arrival { get; set; }
        public GeoZPoint UserPoint { get; set; }
        public TimeSpan Departure { get; set; }
        public Length IncomingDistance { get; set; }
        public TimeSpan TotalBreakTime { get; set; } // including events
        public TimeSpan StaminaRollingTime { get; set; }  
        public int? IncomingLegIndex { get; set; }
        //public bool IsLooped { get; set; }
        public int[] EventCounters { get; set; }
        public ElevationStats ElevationStats { get; set; }
        public Length AggregateDistance { get; set; }

        public IEnumerable<(string label, string iconClass, int count, TimeSpan duration)> GetEvents(SummaryJourney summary) =>
            Enumerable.Range(0,EventCounters.Length)
                .Where(it => this.EventCounters[ it]>0)
                .Select(it => (  Label: summary.PlannerPreferences.TripEvents[it].Group,
                    IconClass: summary.PlannerPreferences.TripEvents[it].ClassIcon, 
                    count:this.EventCounters[ it],
                    this.EventCounters[ it]*summary.PlannerPreferences.TripEvents[it].Duration))
            .OrderBy(it => it.Label);
        
        public SummaryCheckpoint(int eventsCount)
        {
            IsUserLabel = false;
            Label = "";
            Code = "";
            this.EventCounters = new int[eventsCount];
        }
    }
}
