﻿namespace TrackPlanner.Shared.Data
{
 public readonly record struct MapZPoint<TNodeId,TRoadId> : IMapZPoint
        where TNodeId : struct
    {
        public GeoZPoint Point { get;  }
        #if DEBUG
        public TNodeId? NodeId { get; }
        private static int DEBUG_IdCounter = 0;
        private readonly int DEBUG_id = DEBUG_IdCounter++;
        public string? Description { get; }
        public MapRef<TNodeId, TRoadId>? DEBUG_MapRef { get; }
        #endif
        public bool IsNode() => NodeId.HasValue;

        public MapZPoint(GeoZPoint point
#if DEBUG
            , TNodeId? nodeId
        , MapRef<TNodeId,TRoadId>? DEBUG_mapRef =null
            , string? description = null
#endif
        )
        {
            Point = point;
            NodeId = nodeId;
            #if DEBUG
            this.DEBUG_MapRef = DEBUG_mapRef;
            Description = description;
            if (this.DEBUG_id == 288)//289)
            {
                ;
            }
            #endif
        }

        public override string ToString()
        {
            var result = $"({this.Point.Convert2d()})";
            if (this.IsNode())
                result= $"#{this.NodeId} {result}";
            return result;
        }
    }
}