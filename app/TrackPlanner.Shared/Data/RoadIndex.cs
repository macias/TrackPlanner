﻿using System.Globalization;
using System.Runtime.InteropServices;

namespace TrackPlanner.Shared.Data
{
    // https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.structlayoutattribute.pack?view=net-6.0
    // https://docs.microsoft.com/en-us/dotnet/api/system.runtime.interopservices.layoutkind?view=net-6.0

    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public readonly record struct RoadIndex<TRoadId>
    {
        private static readonly NumberFormatInfo nfi;

        static RoadIndex()
        {
            nfi = (NumberFormatInfo) CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = "_";
        }

        public static RoadIndex<TRoadId> InvalidIndex()
        {
            return new RoadIndex<TRoadId>(default!, ushort.MaxValue);
        }

        public static RoadIndex<TRoadId> InvalidIndex(TRoadId roadId)
        {
            return new RoadIndex<TRoadId>(roadId, ushort.MaxValue);
        }

        public TRoadId RoadId { get; init; }
        public ushort IndexAlongRoad { get; init; }

        public RoadIndex(TRoadId roadId, int indexAlongRoad)
        {
            RoadId = roadId;
            IndexAlongRoad = (ushort) indexAlongRoad;
        }

        public void Deconstruct(out TRoadId roadId, out ushort indexAlongRoad)
        {
            roadId = this.RoadId;
            indexAlongRoad = this.IndexAlongRoad;
        }

        public (TRoadId roadId, int indexAlongRoad) AsTuple() // for easier working with ints
        {
            return (RoadId, IndexAlongRoad);
        }

        public override string ToString()
        {
//            return $"{RoadId.ToString("#,0",nfi)} [{IndexAlongRoad}]";
            return $"{RoadId} [{IndexAlongRoad}]";
        }

        public RoadIndex<TRoadId> Next()
        {
            return new RoadIndex<TRoadId>(RoadId, IndexAlongRoad + 1);
        }

    }
}
