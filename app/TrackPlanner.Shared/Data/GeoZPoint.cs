﻿using MathUnit;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

#nullable enable

namespace TrackPlanner.Shared.Data
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public readonly struct GeoZPoint : IEquatable<GeoZPoint>, ISerializable
    {
        //  public static GeoZPoint Invalid { get; } = FromDegreesMeters(100, 0, ushort.MaxValue);

        public static GeoZPoint FromDegreesMeters(double latitude, double longitude, double? altitude)
        {
            return new GeoZPoint(scaleDegrees(latitude), scaleDegrees(longitude),
                altitude == null ? (ushort) 0 : scaleAltitude(altitude.Value));
        }

        private static int scaleDegrees(double degrees)
        {
            return (int)Math.Round(degrees*degreeScale);
            //return (float) degrees;
        }

        private static Angle rawToAngle(double rawDegress)
        {
            return Angle.FromDegrees( rawDegress/degreeScale);
//            return Angle.FromDegrees( rawDegress);
        }

        private static ushort scaleAltitude(double altitudeMeters)
        {
            return (ushort) Math.Round((altitudeMeters - altitudeOffset) * altitudeScale);
        }

        public static GeoZPoint Create(Angle latitude, Angle longitude, Length? altitude)
        {
            return FromDegreesMeters(latitude.Degrees, longitude.Degrees, altitude?.Meters);
        }

        private static readonly string angleFormat = $"0.{new string('#',degreeScalePower)}";
        public static string RawFormat(Angle angle)
        {
            return angle.Degrees.ToString(angleFormat);
        }

        private const int degreeScalePower = 7;
        private static readonly double degreeScale = Math.Pow(10, degreeScalePower);
        private readonly int scaledLatitudeDegrees;
        private readonly int scaledLongitudeDegrees;
        private const double altitudeOffset = -100;
        private const double altitudeScale = 7;
        private readonly ushort scaledAltitudeMeters;

        public Angle Latitude =>  rawToAngle(this.scaledLatitudeDegrees);
        public Angle Longitude => rawToAngle( this.scaledLongitudeDegrees);
        public Length Altitude => Length.FromMeters(this.scaledAltitudeMeters / altitudeScale + altitudeOffset);

        public bool HasAltitude => this.scaledAltitudeMeters > 0;
        
        private GeoZPoint(int scaledScaledLatitudeDegrees, int scaledLongitudeDegrees, ushort scaledAltitudeMeters)
        {
            this.scaledLatitudeDegrees = scaledScaledLatitudeDegrees;
            this.scaledLongitudeDegrees = scaledLongitudeDegrees;
            this.scaledAltitudeMeters = scaledAltitudeMeters;

        }

        public GeoZPoint WithAltitude(Length altitude)
        {
            return new GeoZPoint(this.scaledLatitudeDegrees, this.scaledLongitudeDegrees,scaleAltitude( altitude.Meters));
        }

        public GeoZPoint(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            this.scaledLatitudeDegrees = scaleDegrees( info.GetDouble(nameof(Latitude)));
            this.scaledLongitudeDegrees = scaleDegrees(info.GetDouble(nameof(Longitude)));
            this.scaledAltitudeMeters =scaleAltitude( info.GetSingle(nameof(Altitude)));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            info.AddValue(nameof(Latitude), Latitude.Degrees);
            info.AddValue(nameof(Longitude), Longitude.Degrees);
            // serialization will be used for JSON as well, it means human-readable, so let's use regular meters
            info.AddValue(nameof(Altitude), (float) this.Altitude.Meters);
        }


        public void Deconstruct(out Angle latitude, out Angle longitude, out Length? altitude)
        {
            latitude = this.Latitude;
            longitude = this.Longitude;
            altitude = this.Altitude;
        }

        public override string ToString()
        {
            string result = $"{Latitude}, {Longitude}";
            if (HasAltitude)
                result += $", {Altitude}";
            return result;
        }

        public override bool Equals(object? obj)
        {
            return obj is GeoZPoint pt && Equals(pt);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.scaledLatitudeDegrees, this.scaledLongitudeDegrees, this.scaledAltitudeMeters);
        }

        public bool Equals(GeoZPoint obj)
        {
            return Equals(obj, compareAltitude: true);
        }

        public bool Equals(GeoZPoint obj,bool compareAltitude)
        {
            return IsSame2d(obj)
                   && (!compareAltitude || this.scaledAltitudeMeters == obj.scaledAltitudeMeters);
        }
        public bool IsSame2d(in GeoZPoint other)
        {
            return this.scaledLatitudeDegrees == other.scaledLatitudeDegrees
                   && this.scaledLongitudeDegrees == other.scaledLongitudeDegrees;
        }

        public static bool operator ==(in GeoZPoint a, in GeoZPoint b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(in GeoZPoint a, in GeoZPoint b)
        {
            return !(a == b);
        }

        public static void WriteRawAngle(BinaryWriter writer, Angle angle)
        {
            writer.Write(scaleDegrees( angle.Degrees));
        }

        public static Angle ReadRawAngle(BinaryReader reader)
        {
            return rawToAngle(readAngleRawValue( reader));
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(this.scaledLatitudeDegrees);
            writer.Write(this.scaledLongitudeDegrees);
            writer.Write(this.scaledAltitudeMeters);
        }

        public static GeoZPoint Read(BinaryReader reader)
        {
            var lat = readAngleRawValue(reader);
            var lon = readAngleRawValue(reader);
            var alt = reader.ReadUInt16();

            var point = new GeoZPoint(lat, lon, alt);
            return point;
        }

        private static int readAngleRawValue(BinaryReader reader)
        {
            return reader.ReadInt32();
        }
    }
}