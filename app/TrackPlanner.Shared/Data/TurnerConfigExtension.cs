﻿using MathUnit;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Data
{
    public static class TurnerConfigExtension
    {
        public static Angle GetAltAngleDifferenceLimit(this UserTurnerPreferences preferences, Angle altAngle)
        {
            // the more straighter is our track, the more straigher the alternate has to be as well, to force turn-noficication
            double scaling = (Angle.PI - altAngle.Abs()) / (Angle.PI - preferences.StraigtLineAngleLimit);
            return preferences.AltAngleDifferenceHighLimit + 
                   (preferences.AltAngleDifferenceLowLimit - preferences.AltAngleDifferenceHighLimit) * scaling;
        }
        
        public static bool IsObviouslyStraightInComparison(this UserTurnerPreferences preferences, Angle straightAngle, 
            Angle referenceAngle)
        {
            if (preferences.StraigtLineAngleLimit > straightAngle.Abs())
                return false;
            
            var diff = preferences.GetAltAngleDifferenceLimit(referenceAngle);

            return (straightAngle.Abs() - referenceAngle.Abs()).Abs() > diff;
        }

    }

}
