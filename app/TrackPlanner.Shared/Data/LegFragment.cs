﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Data
{
    public sealed class LegFragment<TNodeId,TRoadId> 
    where TNodeId:struct
    {
        public CostInfo CostInfo { get; set; }
        public RoadSpeedStyling SpeedStyling { get; set; }
        public bool IsForbidden { get; set; }
        public List<FragmentStep<TNodeId,TRoadId>> Steps { get; set; }
        public Length UnsimplifiedFlatDistance { get; set; } // distance before points removal (simplification)
        public TimeSpan RawTime { get; set; }
        public HashSet<TRoadId> RoadIds { get; set; }

        public IReadOnlyList<FragmentStep<TNodeId, TRoadId>> GetSteps() => Steps;

        public LegFragment()
        {
            this.RoadIds = new HashSet<TRoadId>();
            this.Steps = new List<FragmentStep<TNodeId,TRoadId>>();
        }

        public LegFragment<TNodeId,TRoadId> SetSpeedMode(RoadSpeedStyling styling)
        {
            this.SpeedStyling = styling;
            return this;
        }

        public ContextMessage? Validate()
        {
            var distances = Length.Zero;
            foreach (var step_dist in GetSteps().Select(it => it.IncomingFlatDistance))
            {
                distances += step_dist;
            }

           // if ((distances - UnsimplifiedDistance).Abs() > TrackPlan.LengthErrorLimit)
             //   throw new InvalidOperationException($"{this}.{nameof(UnsimplifiedDistance)}={UnsimplifiedDistance} when sum of elements = {distances}");

            if (GetSteps().Count <= 1)
                return new ContextMessage( $"Contains {GetSteps().Count} points.");
            
            return null;
        }
    }
}