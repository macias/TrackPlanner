﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Geo;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct RequestPoint<TNodeId> : IRequestPoint 
        where TNodeId:struct
    {
        // this is lame, we use 3d point just because it is based on integers, not doubles
        // so we will get more stable requests when binding to adjacent segment
        public GeoZPoint UserPoint { get; init; }
        public TNodeId? NodeId { get; init; }
        public bool EnforcePoint { get; init; }
        public bool AllowSmoothing { get; init; }
        public bool AllowGap { get; init; }
        public bool FindLabel { get; init; }
        public GeoZPoint UserPoint3d => this.UserPoint;
        public GeoPoint UserPointFlat => this.UserPoint.Convert2d();

        public bool IsNode() => this.NodeId.HasValue;

        public RequestPoint(GeoPoint userPoint)
        {
            UserPoint = userPoint.Convert3d();
            NodeId = null;
            EnforcePoint = false;
            AllowSmoothing = false;
            AllowGap = false;
            FindLabel = false;
        }
        public RequestPoint(GeoPoint userPoint,  bool allowSmoothing, bool findLabel)
        {
            NodeId = null;
            UserPoint = userPoint.Convert3d();
            this.AllowSmoothing = allowSmoothing;
            FindLabel = findLabel;
            EnforcePoint = false;
            AllowGap = false;
        }

        public override string ToString()
        {
            return Stringify();
        }

        public string Stringify()
        {
            return $"{(AllowGap?"G":"")}{(AllowSmoothing?"S":"")}{(EnforcePoint?"E":"")}{(FindLabel?"L":"")} {UserPointFlat}";
        }

        public static IEnumerable<RequestPoint<TNodeId>> ParsePoints(string message)
        {
            static RequestPoint<TNodeId> parse_point(string s)
            {
                s = s.Trim();
                var coords = s.Split(",");
                string prefix;
                {
                    var head = coords[0].Split(" ").ToList();
                    if (head.Count == 1)
                    {
                        prefix = "";
                    }
                    else
                    {
                        prefix = head[0];
                        coords[0] = head[1];
                    }
                }
                var point = new RequestPoint<TNodeId>(GeoPoint.FromDegrees(double.Parse(coords[0], 
                        CultureInfo.InvariantCulture),
                    double.Parse(coords[1], CultureInfo.InvariantCulture)));
                return point with
                {
                    AllowSmoothing = prefix.Contains("S"),
                    EnforcePoint = prefix.Contains("E"),
                    FindLabel = prefix.Contains("L"),
                    AllowGap = prefix.Contains("G"),
                };
            }
            
            message = message.Replace("°", "");
            var parts = message.Split(";");
            return parts.Select(parse_point);
        }

    }
}