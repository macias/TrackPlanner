﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct ElevationStats
    {
        public Length ClimbDistance { get; init; }
        public Length ClimbHeight { get; init; }
        public Slope ClimbSlope { get; init; }
    }
}