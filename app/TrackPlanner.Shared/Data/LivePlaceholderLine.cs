namespace TrackPlanner.Shared.Data
{
    public readonly record struct LivePlaceholderLine
    {
        public (int dayIndex,int anchorIndex)? Start { get; }
        public (int dayIndex,int anchorIndex)? End { get; }
        public int? LegIndex { get; }

        public LivePlaceholderLine((int dayIndex,int anchorIndex)? start,
            (int dayIndex,int anchorIndex)? end, int? legIndex)
        {
            this.Start = start;
            End = end;
            LegIndex = legIndex;
        }
    }
  
}