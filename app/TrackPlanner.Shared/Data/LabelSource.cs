using System;
using Geo;

namespace TrackPlanner.Shared.Data
{
    public enum LabelSource
    {
        AutoMap,
        Attraction,
        User
    }
}