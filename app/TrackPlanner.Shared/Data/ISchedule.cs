using System.Collections.Generic;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Data
{
    public interface IReadOnlySchedule
    {
        ScheduleSettings Settings { get; }
        IReadOnlyList<IReadOnlyDay> Days { get; }

        int LegsCount(); // because I am lazy and using generic interface is tiresome
    }

    public interface IReadOnlySchedule<TNodeId, TRoadId> : IReadOnlySchedule
        where TNodeId : struct
        where TRoadId : struct
    {
        RoutePlan<TNodeId, TRoadId> Route { get; }
        int IReadOnlySchedule.LegsCount() => this.Route.Legs.Count;
    }

    public interface ISummarySchedule : IReadOnlySchedule
    {
        // method, not property to have guarantee of skipping serialization
        SummaryJourney GetSummary();
    }

    public interface IReadOnlyScheduleSummary<TNodeId,TRoadId> : ISummarySchedule
        where TNodeId:struct
        where TRoadId: struct
    {
        (int DayIndex, int AnchorIndex)? AnchorPlaceholder { get; }
        RoutePlan<TNodeId,TRoadId> Route { get; }
    }

    public interface ISchedule<TDay,TAnchor,TNodeId,TRoadId> : IReadOnlySchedule<TNodeId,TRoadId>
    where TDay:IDay<TAnchor>
    where TAnchor:IAnchor
    where TNodeId:struct
    where TRoadId: struct
    {
        new List<TDay> Days { get; }
    }
}