﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Data
{
  
    public static class RoutePlan
    {
        internal static Length ValidationLengthErrorLimit => Length.FromCentimeters(1);
    }

    public sealed class RoutePlan<TNodeId,TRoadId>
        where TNodeId:struct
    where TRoadId: struct
    {
        public List<LegPlan<TNodeId,TRoadId>> Legs { get; set; }
        // we cannot split turns and merge them with legs, because we have to compute
        // turns per day (not per leg), one of the reason is the turn could be between legs
        public List<List<TurnInfo<TNodeId,TRoadId>>> DailyTurns { get; set; } 

        public RoutePlan()
        {
            this.Legs = new List<LegPlan<TNodeId,TRoadId>>();
            this.DailyTurns = new List<List<TurnInfo<TNodeId,TRoadId>>>();
        }

        public bool IsEmpty() => this.Legs.Count == 0;

        public ContextMessage? Validate()
        {
            if (Legs == null)
                return new ContextMessage( "Route legs are set to null");
     
            for (int i=0;i<this.Legs.Count;++i)
            {
              var failure =  Legs[i].Validate();
              if (failure is {} f)
                  return new ContextMessage( $"Leg:{i} {f}",f);
            }
            foreach (var (prev,next) in this.Legs.ZipIndex()
                         .Where(it => it.item.Fragments.Any()).Slide())
            {
                if (next.item.AllowGap)
                    continue;
                        
                var prev_point = prev.item.LastStep().Point;
                var next_point = next.item.FirstStep().Point;
                // compare only legs of the same type, the reason -- we could have drafted leg
                // ending in unrealistic spot, the adjacent leg could be calculated with some
                // nearby but not the same spot
                if (prev.item.IsDrafted==next.item.IsDrafted &&
                    !prev_point.Equals( next_point,compareAltitude: !prev.item.IsDrafted && !next.item.IsDrafted))
                    return new ContextMessage( $"We have gap between legs {prev.index} and {next.index} ({prev.item.DEBUG_RequestEnd}->{prev_point} vs. {next_point}).");
            }

            return null;
        }

        public LegPlan<TNodeId,TRoadId> GetLeg(int legIndex,string? DEBUG_context = null)
        {
            if (legIndex < 0 || legIndex >= this.Legs.Count)
                throw new ArgumentOutOfRangeException($"{nameof(legIndex)}={legIndex} when having {this.Legs.Count} legs ({DEBUG_context}).");

            return this.Legs[legIndex];
        }

    }
}
