﻿using System;

namespace TrackPlanner.Shared.Data
{
    public static class CostInfoExtension
    {
        public static bool Penalized(this CostInfo info)
        {
            info &= ~(CostInfo.Suppressed);
            return info != CostInfo.None;
        }
    }
    
    [Flags]
    public enum CostInfo
    {
        None = 0,
        Suppressed = 1,

        HighVolume = 2,
        DangerousSpeed = 4,
        UncomfortableSpeed = 8,
        HighTrafficBikeLane = 16,
        UnstableRoad = 32,
        Cycleway = 64,
        Auxiliary = 128,
        JoiningHighTraffic = 256,
    }
    
}
