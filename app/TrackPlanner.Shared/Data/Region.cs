﻿using System;
using Geo;
using MathUnit;

#nullable enable

namespace TrackPlanner.Shared.Data
{
    public readonly record struct Region
    {
        // looking for version with expansion range? --> check calculator
        public static Region Build(params GeoPoint[] points)
        {
            if (points.Length == 0)
                throw new ArgumentException();
            var result = getMin();
            foreach (var p in points)
            {
                result = result.include(p);
            }
            return result;
        }
        public static Region Build(GeoPoint pointA, GeoPoint pointB)
        {
            return getMin().include(pointA).include(pointB);
        }

        private static Region getMin()
        {
            return new Region(north: -MathUnit.Angle.PI,
                 east : -MathUnit.Angle.PI,
                 south : MathUnit.Angle.PI,
                 west : MathUnit.Angle.PI);
        }
        public Angle North { get; }
        public Angle East { get;  }
        public Angle South { get; }
        public Angle West { get;  }

        public Region(Angle north, Angle east, Angle south, Angle west)
        {
            North = north;
            East = east;
            South = south;
            West = west;
        }

        public Region Expand(Angle byLatitude, Angle byLongitude)
        {
            return new Region(north: North + byLatitude,
                south: South - byLatitude,
                east: East + byLongitude,
                west: West - byLongitude);
        }

        private Region include(GeoPoint point)
        {
            return new Region(
               north: North.Max(point.Latitude),
                east: East.Max(point.Longitude),
                south: South.Min(point.Latitude),
                west:West.Min(point.Longitude));
        }

        public bool IsWithin(GeoPoint point)
        {
            return point.Latitude <= North && point.Latitude >= South 
                                           && point.Longitude <= East && point.Longitude >= West;
        }
    }
}