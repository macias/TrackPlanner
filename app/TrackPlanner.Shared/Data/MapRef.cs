﻿namespace TrackPlanner.Shared.Data
{
#if DEBUG
    public interface IMapRef
    {
        
    }
    public readonly record struct MapRef<TNodeId, TRoadId>(
        RoadIndex<TRoadId> RoadIndex,
        OsmId OsmRoadId,
        OsmId OsmNodeId) : IMapRef
    {
        private static long InstanceCounter = 0;
        public long Identifier { get; init; } = getIdentifier();

        private static long getIdentifier()
        {
            var result = InstanceCounter++;
            if (result == 36)
            {
                ;
            }

            return result;
        }
    }
#endif
}