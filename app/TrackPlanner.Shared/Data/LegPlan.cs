﻿using System;
using System.Collections.Generic;
using System.Linq;
using Geo;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    // leg are the segments from anchor to anchor
    public sealed class LegPlan<TNodeId, TRoadId>
        where TNodeId : struct
    {
#if DEBUG
        private static int DEBUG_Counter;
        private readonly int DEBUG_Id = DEBUG_Counter++;
#endif
        public static LegPlan<TNodeId, TRoadId> CreateMissing()
        {
            return new() {IsDrafted = true, IsMissing = true};
        }

        private bool isDrafted;
        public bool IsDrafted
        {
            get => this.isDrafted;
            set
            {
                this.isDrafted = value;
                if (DEBUG_SWITCH.Enabled && value)
                {
                    ;
                }
            }
        }

        public bool IsMissing { get; private init; }

        public List<LegFragment<TNodeId, TRoadId>> Fragments { get; set; }

        public Length UnsimplifiedDistance { get; set; }
        public TimeSpan RawTime { get; set; }


        public bool AutoAnchored { get; set; }
        public bool AllowGap { get; set; }
        public List<PlacedAttraction> Attractions { get; set; }

        public IEnumerable<FragmentStep<TNodeId, TRoadId>> AllSteps()
            => Fragments.SelectMany(it => it.GetSteps());

        public FragmentStep<TNodeId, TRoadId> FirstStep() => Fragments[0].GetSteps()[0];

#if DEBUG
        public GeoZPoint? DEBUG_RequestStart { get; set; }
        public GeoZPoint? DEBUG_RequestEnd { get; set; }
#endif
        public FragmentStep<TNodeId, TRoadId> LastStep()
        {
#if DEBUG
            try
            {
#endif
                return Fragments[^1].GetSteps()[^1];
#if DEBUG
            }
            catch
            {
                throw;
            }
#endif
        }

        public LegPlan()
        {
            this.Fragments = new List<LegFragment<TNodeId, TRoadId>>();
            this.Attractions = new List<PlacedAttraction>();
        }

        public ContextMessage? Validate()
        {
            if (IsMissing)
                return new ContextMessage("The leg is a stub.");

            var distances = Length.Zero;
            for (int i = 0; i < Fragments.Count; ++i)
            {
                var fragment = this.Fragments[i];

                distances += fragment.UnsimplifiedFlatDistance;
                var failure = fragment.Validate();
                if (failure is { } f)
                    return new ContextMessage($"Fragment:{i}/{Fragments.Count} {failure}", f);

                if (i > 0 && Fragments[i - 1].GetSteps().Last().Point != fragment.GetSteps().First().Point)
                    return new ContextMessage($"Gap between fragments {i - 1} and {i}.");
            }

            if ((distances - UnsimplifiedDistance).Abs() > RoutePlan.ValidationLengthErrorLimit)
                return new ContextMessage($"{this}.{nameof(UnsimplifiedDistance)}={UnsimplifiedDistance} when sum of elements = {distances}");

            return null;
        }

        public void ComputeLegTotals()
        {
            var distance = Length.Zero;
            var time = TimeSpan.Zero;

            foreach (var fragment in Fragments)
            {
                distance += fragment.UnsimplifiedFlatDistance;
                time += fragment.RawTime;
            }

            this.UnsimplifiedDistance = distance;
            this.RawTime = time;
        }

        public bool NeedsRebuild(bool calcReal)
        {
            return IsMissing || (this.IsDrafted && calcReal);
        }
    }
}