﻿using System;

namespace TrackPlanner.Shared.Data
{
    // class, not struct, because we will keep references to it in visual elements (so when for example we would like to
    // remove marker for _this_ attraction it has to be referenced one, not the one with the same data. However it would
    // be useful to allow only unique attractions, than data would become reference) 
    public sealed  class PlacedAttraction
    {
        public GeoZPoint Point { get; init; }
        public PointOfInterest Attraction { get; init; }

        // for the sake of mismatch serialization (i.e. when we have files with some legacy properties)
        public PlacedAttraction() 
        {
            
        }
        public PlacedAttraction(GeoZPoint point, PointOfInterest attraction)
        {
            Point = point;
            Attraction = attraction;
        }

        public PlacedAttraction(in MapZPoint<OsmId, OsmId> legacyPlace, PointOfInterest attraction)
            :this(legacyPlace.Point,attraction)
        {
        }
        
        public void Deconstruct(out GeoZPoint point, out PointOfInterest attraction)
        {
            point = Point;
            attraction = Attraction;
        }
        public (GeoZPoint point, PointOfInterest attraction) GetTuple() // for value equality
        {
            return (Point, Attraction);
        }
    }
    
    
}
