using System;
using System.Threading.Tasks;
using Geo;

namespace TrackPlanner.Shared.Data
{
   

    public interface IAnchor : IReadOnlyAnchor
    {
        new TimeSpan UserBreak { get;  }
        new string Label { get;  }
        new LabelSource LabelSource { get; }
        //bool IsPinned { get; }
        new GeoPoint UserPoint { get;  }
#if DEBUG
        new bool? DEBUG_Smoothing { get; set; }
        new bool? DEBUG_Enforce { get; set; }
#endif

        ValueTask SetLabelAsync(string label, LabelSource source);
    }
 
}