﻿using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct RoadCondition
    {
        public RoadSpeedStyling SpeedStyling { get; }
        public CostInfo CostInfo { get; }
        public bool IsForbidden { get; }

        public RoadCondition(RoadSpeedStyling roadSpeedSpeedStyling, CostInfo costInfo, bool isForbidden)
        {
            SpeedStyling = roadSpeedSpeedStyling;
            CostInfo = costInfo;
            this.IsForbidden = isForbidden; 
        }
    }
}