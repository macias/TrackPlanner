﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Data
{
    public static class ScheduleSummaryExtension
    {
        public static int AnchorIndexToCheckpoint(this IReadOnlySchedule schedule,
            int dayIndex, int anchorIndex)
        {
            if (dayIndex == 0)
                return anchorIndex;
            else
            {
                return anchorIndex + 1;
            }
        }

        public static bool IsCheckpointLoopEnd(this IReadOnlySchedule schedule,
            int checkpointDayIndex, int checkpointIndex)
        {
            return schedule.IsLoopedDay(checkpointDayIndex)
                && schedule.IsLastCheckpointOfDay(checkpointDayIndex, checkpointIndex) ;
        }

        public static bool IsLastCheckpointOfDay(this IReadOnlySchedule schedule,
            int checkpointDayIndex, int checkpointIndex)
        {
            var checkpoint_count = schedule.Days[checkpointDayIndex].Anchors.Count
                                   + (schedule.IsLoopedDay(checkpointDayIndex)?1:0)
                                   + (checkpointDayIndex > 0 ? 1 : 0);

            return checkpointIndex == checkpoint_count - 1;
        }

        public static bool IsAnchorReused(this IReadOnlySchedule schedule,
            int dayIndex, int anchorIndex)
        {
            // last anchor of (the multiple days) day
            if (schedule.Days.Count > dayIndex + 1 && anchorIndex == schedule.Days[dayIndex].Anchors.Count - 1)
                return true;
            
            if (dayIndex == 0 && anchorIndex == 0 && schedule.Settings.LoopRoute)//.IsLoopActivated())
                return true;

            return false;
        }

        public static (int dayIndex, int anchorIndex) CheckpointIndexToAnchor(this IReadOnlySchedule schedule,
            int checkpointDayIndex, int checkpointIndex)
        {
            if (schedule.IsCheckpointLoopEnd(checkpointDayIndex,checkpointIndex))
                return (0, 0);

            if (checkpointDayIndex == 0)
                return (checkpointDayIndex, checkpointIndex);

            if (checkpointIndex == 0) // first checkpoint of "next" day
                return (checkpointDayIndex - 1,
                    schedule.Days[checkpointDayIndex - 1].Anchors.Count - 1);

            return (checkpointDayIndex, checkpointIndex - 1);
        }

        public static string GetMarkerCode(int dayIndex, int anchorIndex)
        {
            return $"{dayIndex + 1}-{anchorIndex + 1 + (dayIndex == 0 ? 0 : 1)}";
        }

        public static IEnumerable<(string label, string classIcon, int count, TimeSpan duration)> GetEventStats(int[] eventCounters, UserPlannerPreferences preferences)
        {
            return
                Enumerable.Range(0, eventCounters.Length)
                    .GroupBy(it => preferences.TripEvents[it].Group)
                    .Select(group_it =>
                    {
                        var first_event = preferences.TripEvents[group_it.First()];
                        var total_count = group_it.Select(it => eventCounters[it]).Sum();
                        var total_duration = group_it.Select(it => preferences.TripEvents[it].Duration * eventCounters[it]).Sum();

                        return (Label: first_event.Group, first_event.ClassIcon, total_count, total_duration);
                    })
                    .Where(it => it.total_count != 0);
        }


        public static SummaryJourney CreateSummary<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> schedule)
            where TNodeId : struct
            where TRoadId : struct
        {
            var summary = new SummaryJourney() {PlannerPreferences = schedule.Settings.PlannerPreferences};
            for (int day_idx = 0; day_idx < schedule.Days.Count; ++day_idx)
            {
                var summary_day = createSummaryDay(schedule, day_idx);

                summary.Days.Add(summary_day);
            }

            return summary;
        }


        private static SummaryDay createSummaryDay<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> schedule,
            int dayIndex)
            where TNodeId : struct
            where TRoadId : struct
        {
            var day = schedule.Days[dayIndex];

            var start = day.Start;
            TimeSpan aggregate_rolling_time = TimeSpan.Zero;

            var summary_day = new SummaryDay(day) {Start = start, End = start};
            if (dayIndex == schedule.Days.Count - 1 && schedule.Settings.EndsAtHome)
                summary_day.EndsAtHome = true;
            // if we don't have anything (we just started from scratch) do not create any checkpoints as well
            // just an empty day, that's all,
            // NOTE: for last day, looped, not having any anchors is a valid scenario (it starts from the beginning
            // of the previous day and loop to the global start) 
            if (dayIndex == 0 && day.Anchors.Count == 0)
                return summary_day;

            summary_day.ElevationStats = calcElevationStats(schedule.Settings.PlannerPreferences.ReportUphillsFrom,
                schedule.GetDayLegs(dayIndex).ToArray());

            {
                var initial_point = new SummaryCheckpoint(eventsCount: schedule.Settings.PlannerPreferences.TripEvents.Length)
                {
                    Arrival = start,
                    Departure = start,
                    Code = GetMarkerCode(dayIndex, dayIndex == 0 ? 0 : -1)
                };

                if (schedule.Route.IsEmpty())
                {
                    // for example there is too little anchors to compute route
                    if (DEBUG_SWITCH.Enabled)
                    {
                        ;
                    }

                    initial_point.UserPoint = schedule.Days[dayIndex].Anchors[0].UserPoint.Convert3d(Length.Zero);
                }
                else
                {
                    var first_leg_index = schedule.GetFirstLegIndex(dayIndex);
                    if (schedule.Route.TryGetLegEdgeStep(first_leg_index, _ => true,
                            first: true, bothSides: true, out var edge_step))
                        initial_point.UserPoint = edge_step.Point;
                    else
                    {
                        if (DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }

// if we cannot grap user point from route (all empty legs) then at least
// get it from anchors
                        int i = schedule.Days.LastIndexOf(dayIndex, d => d.Anchors.Count > 0);
                        initial_point.UserPoint = schedule.Days[i]
                            .Anchors[dayIndex == i ? 0 : ^1].UserPoint.Convert3d();
                    }
                }

                summary_day.Checkpoints.Add(initial_point);
            }

            int anchor_idx = dayIndex == 0 ? 1 : 0;
            for (; anchor_idx < day.Anchors.Count; ++anchor_idx)
            {
                schedule.addSummaryPoint(summary_day,
                    ref aggregate_rolling_time, dayIndex, anchor_idx);
            }

            if (schedule.IsLoopedDay(dayIndex))
            {
                schedule.addSummaryPoint(summary_day,
                    ref aggregate_rolling_time, dayIndex, anchor_idx);
            }

            var last_events = schedule.Settings.PlannerPreferences.TripEvents.Where(it => it.Enabled)
                .Select(it => it.Category)
                .Distinct()
                .ToDictionary(it => it, _ => start);

            for (int i = 0; i < summary_day.Checkpoints.Count; ++i)
                addDayTripEvents(schedule, summary_day, i, last_events, dayIndex);


            //Console.WriteLine("DEBUG fixing last checkpoint for the day");

            var last_checkpoint = summary_day.Checkpoints.Last();

            //Console.WriteLine("DEBUG adding extra snack time");

            // let's add/replace the events set for the last moment of the day
            {
                var is_home_start = dayStartsAtHome(schedule, dayIndex);
                var is_home_end = dayEndsAtHome(schedule, dayIndex);

                for (int event_idx = 0;
                     event_idx < schedule.Settings.PlannerPreferences.TripEvents.Length;
                     ++event_idx)
                {
                    var user_event = schedule.Settings.PlannerPreferences.TripEvents[event_idx];

                    if (!user_event.Enabled
                        || !isValidEventDay(user_event, dayIndex, is_home_start, is_home_end)
                        // it is already added
                        || summary_day.Checkpoints.Any(it => it.EventCounters[event_idx] > 0))
                        continue;

                    if (last_events[user_event.Category] == start) // we need to add extra event
                    {
                        addTripEvent(schedule, summary_day, summary_day.Checkpoints.Count - 1, event_idx);
                    }
                    else // we need to replace event
                    {
                        bool replaced = false;

                        // here we have to reverse the order to go from lower priority to highest
                        for (int sub_event_idx = schedule.Settings.PlannerPreferences.TripEvents.Length - 1;
                             sub_event_idx > event_idx;
                             --sub_event_idx)
                        {
                            var sub_event = schedule.Settings.PlannerPreferences.TripEvents[sub_event_idx];
                            if (!sub_event.Enabled || sub_event.Category != user_event.Category)
                                continue;
                            var sub_point_idx = summary_day.Checkpoints.FindLastIndex(it => it.EventCounters[sub_event_idx] != 0);
                            if (sub_point_idx == -1)
                                continue;

                            removeTripEvent(schedule, summary_day, sub_point_idx, sub_event_idx);
                            addTripEvent(schedule, summary_day, sub_point_idx, event_idx);
                            replaced = true;
                            break;
                        }

                        if (!replaced)
                        {
                            addTripEvent(schedule, summary_day, summary_day.Checkpoints.Count - 1, event_idx);
                            summary_day.Problem = $"Couldn't correctly find a replacement for event [{event_idx}]{user_event.Group}";
                        }
                    }
                }
            }

            // moving all events from the last checkpoint to the previous one
            // rationale: when reading summary it is surprise effect that last checkpoint (most likely camping)
            // has snack time included, while it is not possible
            if (summary_day.Checkpoints.Count > 1)
            {
                for (int event_idx = 0; event_idx < last_checkpoint.EventCounters.Length; ++event_idx)
                    while (last_checkpoint.EventCounters[event_idx] > 0)
                    {
                        removeTripEvent(schedule, summary_day, summary_day.Checkpoints.Count - 1, event_idx);
                        addTripEvent(schedule, summary_day, summary_day.Checkpoints.Count - 2, event_idx);
                    }
            }


            //Console.WriteLine("DEBUG clearing last checkpoint for the day");

            last_checkpoint.TotalBreakTime = TimeSpan.Zero;
            // since we don't count in break for the last checkpoint we have to shift
            // arrival time to departure
            last_checkpoint.Arrival = last_checkpoint.Departure;

            // in similar fashion we "correct" the first day, it is not a break time, it is postponed start (but not for day)
            summary_day.Checkpoints[0].TotalBreakTime = TimeSpan.Zero;
            summary_day.Checkpoints[0].Arrival = summary_day.Checkpoints[0].Departure;

            summary_day.Distance = summary_day.Checkpoints.Select(it => it.IncomingDistance).Sum();

            summary_day.End = last_checkpoint.Arrival;
            var late_camping = summary_day.End - ((!schedule.Settings.EndsAtHome || dayIndex < schedule.Days.Count - 1)
                ? schedule.Settings.PlannerPreferences.CampLandingTime
                : schedule.Settings.PlannerPreferences.HomeLandingTime);
            if (late_camping > TimeSpan.Zero
                && DataFormat.Format(late_camping) is var late
                && late != DataFormat.Format(TimeSpan.Zero))
            {
                // it makes no sense to alarm user that he/she is late by "00:00"
                summary_day.LateCampingBy = late;
            }


            return summary_day;
        }

        private static ElevationStats calcElevationStats<TNodeId, TRoadId>(Slope slopeLimit, params LegPlan<TNodeId, TRoadId>[] legs)
            where TNodeId : struct
            where TRoadId : struct
        {
            Length total_climb_distance = Length.Zero;
            Length total_climb_height = Length.Zero;

            bool is_climb_started = false;
            Length climbing_height = Length.Zero;
            Length climbing_distance = Length.Zero;

            Length continuation_height = Length.Zero;
            Length continuation_distance = Length.Zero;

            // this is wrong because here we work on compacted route (but we will remove compacting)
            foreach (var (prev, next) in legs
                         .SelectMany(it => it.Fragments)
                         .SelectMany(it => it.GetSteps())
                         .Slide())
            {
                var alt_diff = next.Point.Altitude - prev.Point.Altitude;
                var curr_slope = Slope.FromData(next.IncomingFlatDistance, alt_diff);
                if (curr_slope >= slopeLimit)
                {
                    if (!is_climb_started)
                        is_climb_started = true;
                    climbing_distance += continuation_distance + next.IncomingFlatDistance;
                    climbing_height += continuation_height + alt_diff;

                    continuation_distance = Length.Zero;
                    continuation_height = Length.Zero;
                }
                else if (is_climb_started)
                {
                    // even if current piece is not that steep we will check if we can continue
                    // because we can have just around the corner good (steep) continuation
                    var total_slope = Slope.FromData(climbing_distance + continuation_distance + next.IncomingFlatDistance,
                        climbing_height + continuation_height + alt_diff);
                    if (total_slope >= slopeLimit)
                    {
                        continuation_distance += next.IncomingFlatDistance;
                        continuation_height += alt_diff;
                    }
                    else
                    {
                        total_climb_distance += climbing_distance;
                        total_climb_height += climbing_height;

                        climbing_distance = Length.Zero;
                        climbing_height = Length.Zero;

                        continuation_distance = Length.Zero;
                        continuation_height = Length.Zero;
                        is_climb_started = false;
                    }
                }
            }

            total_climb_distance += climbing_distance;
            total_climb_height += climbing_height;

            return new ElevationStats()
            {
                ClimbDistance = total_climb_distance,
                ClimbHeight = total_climb_height,
                ClimbSlope = Slope.FromData(total_climb_distance, total_climb_height)
            };
        }

        private static void addSummaryPoint<TNodeId, TRoadId>(this IReadOnlySchedule<TNodeId, TRoadId> schedule,
            SummaryDay summaryDay,
            ref TimeSpan aggregateRollingTime, int dayIndex, int anchorIndex)
            where TNodeId : struct
            where TRoadId : struct
        {
            var day = schedule.Days[dayIndex];
            schedule.GetAnchorDetails(dayIndex, anchorIndex, out var is_looped_anchor, out _);
            // we store label for the last checkpoint in the first anchor (of entire schedule)
            var anchor = is_looped_anchor ? schedule.Days[0].Anchors[0] : day.Anchors[anchorIndex];

            TimeSpan start = summaryDay.Checkpoints[^1].Departure;

            int leg_idx = schedule.GetIncomingLegIndexByAnchor(dayIndex, anchorIndex, allowWrap: false)!.Value;
            LegPlan<TNodeId, TRoadId> leg;
            {
                var DEBUG_context = $"{dayIndex}:{anchorIndex} {schedule.DEBUG_PinsToString()}";
                leg = schedule.Route.GetLeg(leg_idx, DEBUG_context);
            }
            var true_time = DataHelper.CalcTrueTime(aggregateRollingTime, leg.UnsimplifiedDistance, leg.RawTime,
                schedule.Settings.RouterPreferences.GetLowRidingSpeedLimit(),
                schedule.Settings.PlannerPreferences.HourlyStamina);
            aggregateRollingTime += true_time;
            start += true_time;


            var break_time = schedule.GetEffectiveUserBreakTime(dayIndex, anchorIndex);

            GeoZPoint user_point;
            if (schedule.Route.TryGetLegEdgeStep(leg_idx, _ => true, first: false,
                    bothSides: true, out var edge_step))
                user_point = edge_step.Point;
            else
            {
                user_point = anchor.UserPoint.Convert3d();
            }
#if DEBUG
            if (is_looped_anchor)
            {
                ;
            }
#endif

            summaryDay.Checkpoints.Add(new SummaryCheckpoint(eventsCount: schedule.Settings.PlannerPreferences.TripEvents.Length)
            {
                Arrival = start,
                IncomingDistance = leg.UnsimplifiedDistance,
                AggregateDistance = leg.UnsimplifiedDistance+summaryDay.Checkpoints[^1].AggregateDistance,
                StaminaRollingTime = true_time,
                IncomingLegIndex = leg_idx,
                //  IsLooped = is_looped_anchor,
                TotalBreakTime = break_time,
                Departure = start + break_time,
                Label = anchor.Label,
                IsUserLabel = anchor.LabelSource == LabelSource.User,
                ElevationStats = calcElevationStats(schedule.Settings.PlannerPreferences.ReportUphillsFrom, leg),
                UserPoint = user_point,
                Code = GetMarkerCode(dayIndex, anchorIndex),
            });
        }

        private static void addDayTripEvents(IReadOnlySchedule schedule, SummaryDay summaryDay, int summaryPointIndex,
            Dictionary<string, TimeSpan> lastEvents, int dayIndex)
        {
            var is_last_point = summaryPointIndex == summaryDay.Checkpoints.Count - 1;
            var current_summary_point = summaryDay.Checkpoints[summaryPointIndex];

            var is_home_start = dayStartsAtHome(schedule, dayIndex);
            var is_home_end = dayEndsAtHome(schedule, dayIndex);

            for (int event_idx = 0; event_idx < schedule.Settings.PlannerPreferences.TripEvents.Length;)
            {
                var user_event = schedule.Settings.PlannerPreferences.TripEvents[event_idx];

                if (user_event.Enabled && isValidEventDay(user_event, dayIndex, is_home_start, is_home_end))
                {
                    bool add_event;
                    if (user_event.Interval != null)
                    {
                        add_event = current_summary_point.Departure > lastEvents[user_event.Category] + user_event.Interval
                                    // it is better to play safe for the last point
                                    || (is_last_point
                                        && current_summary_point.Departure > lastEvents[user_event.Category] + user_event.Interval / 2);
                    }
                    else
                    {
                        // we treat time-clocks set to null as non-starters of the day
                        add_event = (summaryPointIndex != 0 || user_event.ClockTime != null)
                                    && current_summary_point.Departure > (user_event.ClockTime ?? TimeSpan.Zero)
                                    && summaryDay.Checkpoints.All(it => it.EventCounters[event_idx] == 0);
                    }

                    if (add_event)
                    {
                        var duration = addTripEvent(schedule, summaryDay, summaryPointIndex, event_idx);
                        if (user_event.Interval != null)
                            lastEvents[user_event.Category] += user_event.Interval.Value + duration;
                        else
                            lastEvents[user_event.Category] = (user_event.ClockTime ?? TimeSpan.Zero).Max(current_summary_point.Arrival) + duration;

                        event_idx = 0; // we changed departure time, let's check all events again
                        continue;
                    }
                }

                ++event_idx;
            }
        }

        private static bool isValidEventDay(TripEvent tripEvent, int dayIndex, bool isHomeStart, bool isHomeEnd)
        {
            return (!isHomeStart || !tripEvent.SkipAfterHome)
                   && (!isHomeEnd || !tripEvent.SkipBeforeHome)
                   && dayIndex % tripEvent.EveryDay == 0;
        }

        private static bool dayStartsAtHome(IReadOnlySchedule schedule, int dayIndex)
        {
            return (schedule.Settings.StartsAtHome && dayIndex == 0);
        }

        private static bool dayEndsAtHome(IReadOnlySchedule readOnlySchedule, int dayIndex)
        {
            return readOnlySchedule.Settings.EndsAtHome && dayIndex == readOnlySchedule.Days.Count - 1;
        }

        private static TimeSpan addTripEvent(IReadOnlySchedule schedule, SummaryDay summaryDay,
            int summaryPointIndex, int userEventIndex)
        {
            var duration = schedule.Settings.PlannerPreferences.TripEvents[userEventIndex].Duration;

            SummaryCheckpoint summary_point = summaryDay.Checkpoints[summaryPointIndex];
            ++summary_point.EventCounters[userEventIndex];

            summary_point.Departure += duration;
            summary_point.TotalBreakTime += duration;

            for (int i = summaryPointIndex + 1; i < summaryDay.Checkpoints.Count; ++i)
            {
                summaryDay.Checkpoints[i].Arrival += duration;
                summaryDay.Checkpoints[i].Departure += duration;
            }

            return duration;
        }


        private static TimeSpan removeTripEvent(IReadOnlySchedule schedule, SummaryDay summaryDay,
            int summaryPointIndex, int userEventIndex)
        {
            var duration = schedule.Settings.PlannerPreferences.TripEvents[userEventIndex].Duration;

            SummaryCheckpoint summary_point = summaryDay.Checkpoints[summaryPointIndex];
            --summary_point.EventCounters[userEventIndex];

            summary_point.Departure -= duration;
            summary_point.TotalBreakTime -= duration;

            for (int i = summaryPointIndex + 1; i < summaryDay.Checkpoints.Count; ++i)
            {
                summaryDay.Checkpoints[i].Arrival -= duration;
                summaryDay.Checkpoints[i].Departure -= duration;
            }

            return duration;
        }
    }
}