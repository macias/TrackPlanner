﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public sealed class SummaryDay
    {
        private readonly IReadOnlyDay _day;

        // we cannot use TimeOnly type because it is limited to 24 hours, and it is pretty valid case to
        // ride more in one take
        public TimeSpan Start { get; set; } // explicit data, because even without checkpoints we should know the start of the day 
        public TimeSpan End { get; set; }
        // for looped route we will get more checkpoints than anchors (by one)
        public List<SummaryCheckpoint> Checkpoints { get; set; }
        public TimeSpan TrueDuration => Checkpoints.Count==0? TimeSpan.Zero : this.Checkpoints[^1].Arrival - this.Checkpoints[0].Arrival;
        public TimeSpan RollingTime => TimeSpan.FromSeconds(Checkpoints.Sum(it => it.StaminaRollingTime.TotalSeconds));
        public Length Distance { get; set; }
        public ElevationStats ElevationStats { get; set; }
        public string? LateCampingBy { get; set; }
        public string? Problem { get; set; }
        public bool NeedsDescription => !EndsAtHome && this.Checkpoints.Count > 0 && !this.Checkpoints[^1].IsUserLabel;
        public bool EndsAtHome { get; set; }

        public string? Note => this._day.Note;
        
        public SummaryDay(IReadOnlyDay day)
        {
            this._day = day;
            this.Checkpoints = new List<SummaryCheckpoint>();
        }

        public int[] GetEventCounters()
        {
            if (Checkpoints.Count == 0)
                return Array.Empty<int>();
            
            var buffer = Checkpoints[0].EventCounters.ToArray();
            foreach (var pt in Checkpoints.Skip(1))
            {
                for (int i = 0; i < buffer.Length; ++i)
                    buffer[i] += pt.EventCounters[i];
            }

            return buffer;
        }
    }
}
