using System.Globalization;

namespace TrackPlanner.Shared.Data;

// currently we use this only for roundabout centers, in case of node (center)
// road id is used from roundabout road + phantom:1. In case of "star" roads there is N-1
// created, skipping the 0-indexed (it would be duplicated anyway by the last node of the roundabout).
// All roundabout phantom roads are 2-nodes segments, connecting given node from a roundabout and its center.
public readonly record struct OsmId(long Identifier,
    // 0 -- true osm-based entity
    byte Phantom)
{
    private const char separator = '/';
    
    public static OsmId PureOsm(long id)
    {
        return new OsmId(id, 0);
    }

    public static OsmId Parse(string s)
    {
        byte phantom = 0;
        int idx = s.IndexOf(separator);
        if (idx == -1)
            idx = s.Length;
        else
            phantom = byte.Parse(s.Substring(idx + 1),CultureInfo.InvariantCulture);
            
        long id = long.Parse(s.Substring(0,idx), CultureInfo.InvariantCulture);

        return new OsmId(id, phantom);
    }


    public static OsmId Invalid { get; } = new OsmId(-1, 0);

    public override string ToString()
    {
        if (Phantom == 0)
            return $"{Identifier}";
        else
            return $"{Identifier}{separator}{Phantom}";
    }
    
    
}
