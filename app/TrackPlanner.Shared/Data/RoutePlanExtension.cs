﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathUnit;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Data
{
    public static class RoutePlanExtension
    {
        public static bool TryGetLegEdgeStep<TNodeId, TRoadId>(this RoutePlan<TNodeId, TRoadId> route,
            int legIndex,
            Func<LegPlan<TNodeId, TRoadId>, bool> predicate,
            bool first, bool bothSides, out FragmentStep<TNodeId, TRoadId> step)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (tryGetLegEdgeStep(route, legIndex, predicate, first, out step))
                return true;
            else if (!bothSides)
                return false;
            else
                return tryGetLegEdgeStep(route, legIndex, predicate, !first, out step);
        }

        private static bool tryGetLegEdgeStep<TNodeId, TRoadId>(this RoutePlan<TNodeId, TRoadId> route,
            int legIndex,
            Func<LegPlan<TNodeId, TRoadId>, bool> predicate,
            bool first, out FragmentStep<TNodeId, TRoadId> step)
            where TNodeId : struct
            where TRoadId : struct
        {
            // some legs are empty (and this is valid) so we have to jump over them and find
            // non-empty leg to finally get its first/last point

            // when looking at adjacent legs we need to switch which element we are interested in

            int dir = first ? +1 : -1;
            var element_index = first ? 0 : ^1;
            for (int i = legIndex;
                 i < route.Legs.Count && i >= 0 && predicate(route.Legs[i]);
                 i += dir)
            {
                if (route.Legs[i].IsMissing)
                    throw new ArgumentException($"Stub leg detected at {i}");
                if (route.Legs[i].Fragments.Count != 0)
                {
                    step = route.Legs[i].Fragments[element_index].GetSteps()[element_index];
                    return true;
                }
            }

            step = default;
            return false;
        }
    }
}