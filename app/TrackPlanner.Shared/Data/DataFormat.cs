using System;
using System.Globalization;
using System.Linq;
using Geo;
using MathUnit;

namespace TrackPlanner.Shared.Data
{
    public static class DataFormat
    {
        public static GeoPoint? TryParseUserPoint(string? input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            var parts = input.Split(",").Select(it => it.Trim()).ToList();
            if (parts.Count != 2)
            {
        //        this.logger.LogError("Expected 'lat,long' input.");
                return null;
            }

            if (!float.TryParse(parts[0], out var lat))
            {
             //   this.logger.LogError($"Unable to parse latitude: {parts[0]}");
                return null;
            }

            if (!float.TryParse(parts[1], out var lon))
            {
//                this.logger.LogError($"Unable to parse longitude: {parts[1]}");
                return null;
            }

            return GeoPoint.FromDegrees(lat, lon);
        }

        public static string? FormatUserInput(GeoPoint? position)
        {
            string? input = null;
            if (position != null)
                input = $"{RawFormat( position.Value.Latitude)}, {RawFormat(position.Value.Longitude)}";
            return input;
        }
        public static string? RawFormat(GeoPoint? position)
        {
            if (position is { } pt)
                return $"{RawFormat(pt.Latitude)},{RawFormat(pt.Longitude)}";
            else
                return null;
        }
        public static string RawFormat(GeoZPoint pt)
        {
            string alt = pt.HasAltitude ? RawFormat(pt.Altitude) : "null";
                return $"{RawFormat(pt.Latitude)},{RawFormat(pt.Longitude)},{alt}";
        }
        public static string FormatEvent(int count, TimeSpan duration)
        {
            return $"{Format(duration)}{(count==1?"":$" ({count})")}";
        }

        public static string Format(TimeSpan time)
        {
            const string time_format = @"hh\:mm";
            if (time.TotalDays >= 1)
                return time.ToString(@$"d\.{time_format}");
            else
                return time.ToString(time_format);
        }

        public static string Format(DateTimeOffset dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
        }
        public static string Format(TimeOnly time)
        {
            return time.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
        }
        public static string RawFormat(Angle angle)
        {
            return GeoZPoint.RawFormat(angle);
        }
        public static string RawFormat(Length length)
        {
            return length.Meters.ToString();
        }
        public static string FormatDistance(Length distance,bool withUnit)
        {
            _ = FormatNonZeroDistance(distance, withUnit, out var info);
            return info;
        }
        public static bool FormatNonZeroDistance(Length distance,bool withUnit,out string info)
        {
            var dist = distance.Kilometers.ToString("0.#");
            info = dist+(withUnit?"km":"");
            return dist != "0";
        }
        public static string FormatHeight(Length height,bool withUnit)
        {
            return height.Meters.ToString("0")+(withUnit?"m":"");
        }
        public static string DecoFormatHeight(Length height)
        {
            // little triangle (^) character in front
            return new ( $"\u25B2 {FormatHeight(height, true)}");
        }
        public static string Format(Speed speed,bool withUnit)
        {
            return speed.KilometersPerHour.ToString("0.#")+(withUnit?"km/h":"");
        }

        public static string Adjust(int number, int range)
        {
            if (number > range)
                throw new ArgumentOutOfRangeException($"{nameof(number)}={number}, {nameof(range)}={range}.");
            int total_width =1+(range==0? 0: (int)Math.Floor( Math.Log(range, 10)));
            return number.ToString().PadLeft(total_width, '0');
        }
    }

}