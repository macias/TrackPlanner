using System.Linq;
using System.Runtime.CompilerServices;

namespace TrackPlanner.Shared.Data
{
    public readonly record struct CheckpointTrait
    {
        public int CheckpointIndex { get; }
        public int CheckpointDayIndex { get;  }
        public bool IsFirstCheckpointOfDay => this.CheckpointIndex == 0;
        public int AnchorIndex { get;  }
        public int AnchorDayIndex { get;  }
        public bool IsCheckpointLoopEnd { get;  }
        public bool IsAnchorReused { get;  }
        public IReadOnlyDay Day { get; }
        public IReadOnlyAnchor Anchor { get;  }
        public bool IsLastCheckpointOfDay { get;  }
        public bool IsLastCheckpointDayOfSchedule { get; }
        public bool IsLastPinnedAnchorDayOfSchedule { get;  }

        public bool CanPin => !Anchor.IsPinned;
        public bool CanDelete => Anchor.IsPinned;
        public bool CanHaveGap => Anchor.IsPinned;
        public bool CanSplit => !IsAnchorReused 
                                && !IsLastCheckpointOfDay 
                                && !IsFirstCheckpointOfDay;
        public bool CanHaveMap => Anchor.IsPinned
                                && !IsAnchorReused 
                                && !IsLastCheckpointOfDay 
                                && !IsFirstCheckpointOfDay;

        public bool CanMove => true;//Anchor.IsPinned;
        // actual merge point
         public bool CanMerge => (!this.IsLastCheckpointDayOfSchedule && IsLastCheckpointOfDay)
                                 // this is a useful duplicate for UI sake
         || (this.CheckpointDayIndex>0 && IsFirstCheckpointOfDay);
         public bool CanEditLabel => this.CheckpointIndex != 0 && Anchor.IsPinned;

         public bool CanOrderPrevious => Anchor.IsPinned 
                                                       && !(this.AnchorDayIndex==0 && this.AnchorIndex == 0);
         public bool CanOrderNext =>  Anchor.IsPinned && !this.IsLastPinnedAnchorDayOfSchedule;

         public CheckpointTrait(ISummarySchedule schedule,
             int checkpointDayIndex,
             int checkpointIndex)
         {
             this.CheckpointIndex = checkpointIndex;
             this.CheckpointDayIndex = checkpointDayIndex;

             (this.AnchorDayIndex, this.AnchorIndex) = schedule.CheckpointIndexToAnchor(checkpointDayIndex, checkpointIndex);
             this.IsAnchorReused = schedule.IsAnchorReused(AnchorDayIndex, AnchorIndex);

             this.IsCheckpointLoopEnd = schedule.IsCheckpointLoopEnd(checkpointDayIndex, checkpointIndex);
             this.Day = schedule.Days[AnchorDayIndex];
             this.Anchor = Day.Anchors[AnchorIndex];
             this.IsLastCheckpointOfDay = schedule.IsLastCheckpointOfDay(checkpointDayIndex, checkpointIndex);
             this.IsLastCheckpointDayOfSchedule = IsLastCheckpointOfDay && this.CheckpointDayIndex == schedule.Days.Count - 1;

             this.IsLastPinnedAnchorDayOfSchedule = this.Anchor.IsPinned 
                                                    && !schedule.IndexedReadOnlyAnchors(this.AnchorDayIndex, AnchorIndex)
                                                        .Skip(1) // skip itself
                                                        .Any(it => it.anchor.IsPinned);
         }

    }

}