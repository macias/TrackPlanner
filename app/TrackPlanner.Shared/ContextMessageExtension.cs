﻿using System.Runtime.CompilerServices;

namespace TrackPlanner.Shared;

using System;

public static class ContextMessageExtension
{
    public static ContextMessage? Override(this ContextMessage? ctx, string? message = null, MessageLevel? level = null,
        [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
    {
        return ctx.Override(ContextMessage.Create(message, level, name, path, line));
    }

    public static ContextMessage? Override(this ContextMessage? ctx, ContextMessage? other)
    {
        if (ctx == null)
            return other;
        else if (other == null)
            return ctx;
        else if (ctx.Value.Severity > other.Value.Severity)
            return other;
        else
            return ctx;
    }
    public static string? TryFullMessage(this ContextMessage? message)
    {
        if (message is { } m)
            return m.FullMessage();
        else
            return null;
    }

    public static string EnsureFullMessage(this ContextMessage? message)
    {
        return message.TryFullMessage() ?? throw new ArgumentNullException(nameof(message));
    }

    public static string FullMessage(this ContextMessage message)
    {
        return $"{message.Message} from {message.Context}";
    }

    public static string? TryShortMessage(this ContextMessage? message)
    {
        if (message is { } m)
            return m.Message;
        else
            return null;
    }
}