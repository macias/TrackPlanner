using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Geo;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Visual
{
    public sealed class VisualAnchor : IAnchor
    {
        private readonly IDataContext context;
        public LabelSource LabelSource { get; set; }
#if DEBUG
        public bool? DEBUG_Smoothing { get; set; }
        public bool? DEBUG_Enforce { get; set; }
#endif
        private bool allowGap;
        public bool AllowGap => this.allowGap;

        public async ValueTask SetAllowGapAsync(bool value)
        {
            if (this.allowGap == value)
                return;

            using (onDataChanging())
            {
                this.allowGap = value;
                await onDataChangedAsync(nameof(AllowGap)).ConfigureAwait(false);
            }
        }

        private bool _hasMap;
        public bool HasMap => this._hasMap;
        public async ValueTask SetHasMapAsync(bool value)
        {
            if (this._hasMap == value)
                return;

            using (onDataChanging())
            {
                this._hasMap = value;
                await onDataChangedAsync(nameof(HasMap)).ConfigureAwait(false);
            }
        }

        private TimeSpan userBreak;
        public TimeSpan UserBreak => this.userBreak;

        public async ValueTask SetUserBreakAsync(TimeSpan value)
        {
            if (this.userBreak == value)
                return;

            using (onDataChanging())
            {
                this.userBreak = value;
                await onDataChangedAsync(nameof(UserBreak)).ConfigureAwait(false);
            }
        }

        private string label;
        public string Label => this.label;

        public string UiOnlyLabel
        {
            get { return this.label; }
            set
            {
                #pragma warning disable CS4014
                SetLabelAsync(value,source: LabelSource.User);
                #pragma warning restore
            }
        }

        public async ValueTask SetLabelAsync(string label,LabelSource source)
        {
            if (this.label == label)
                return;

            using (source == LabelSource.User ? onDataChanging() : null)
            {
                this.label = label;
                this.LabelSource = source;
                if (source == LabelSource.User)
                    await onDataChangedAsync(nameof(Label)).ConfigureAwait(false);
            }
        }

        private bool isPinned;
        public bool IsPinned => this.isPinned;

        public async ValueTask SetIsPinnedAsync(bool value)
        {
            if (this.isPinned == value)
                return;

            using (onDataChanging())
            {
                this.isPinned = value;
                await onDataChangedAsync(nameof(IsPinned)).ConfigureAwait(false);
            }
        }

        private GeoPoint userPoint;
        public GeoPoint UserPoint => this.userPoint;

        public async ValueTask SetUserPointAsync(GeoPoint value)
        {
            if (this.userPoint == value)
                return;

            using (onDataChanging())
            {
                this.userPoint = value;
                await onDataChangedAsync(nameof(UserPoint)).ConfigureAwait(false);
            }
        }

        internal VisualAnchor(IDataContext context,GeoPoint userPoint, string label,
            TimeSpan userBreak,bool isPinned,LabelSource labelSource,bool allowGap,bool hasMap)
        {
            this.context = context;
            this.userPoint = userPoint;
            this.label = label;
            this.userBreak = userBreak;
            this.isPinned = isPinned;
            this.allowGap = allowGap;
            this.LabelSource = labelSource;
            this._hasMap = hasMap;
        }
        
        private ValueTask onDataChangedAsync([CallerMemberName] string? name = null)
        {
            return this.context.NotifyOnChangedAsync(this, name!);
        }
        private IDisposable onDataChanging([CallerMemberName] string? name = null)
        {
            return this.context.NotifyOnChanging(this, name!);
        }
    }
}