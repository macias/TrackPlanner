using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Visual
{
    public sealed class VisualDay : IDay<VisualAnchor>,IVisualDay
    {
        private readonly IDataContext context;
      
        
        private static int DEBUG_idCounter;
        private readonly int DEBUG_id = DEBUG_idCounter++;

        private readonly List<VisualAnchor> _anchors;
        public IReadOnlyList<VisualAnchor> Anchors => this._anchors;
        IReadOnlyList<VisualAnchor> IVisualDay.Anchors => this.Anchors;
        IReadOnlyList<IReadOnlyAnchor> IReadOnlyDay.Anchors => this.Anchors;
        
        private TimeSpan _start;
        public TimeSpan Start
        {
            get { return this._start; }
        }

        public async ValueTask SetStartAsync(TimeSpan value)
        {
            if (this._start == value)
                return;

            using (onDataChanging())
            {
                this._start = value;
                await onDataChangedAsync(nameof(Start)).ConfigureAwait(false);
            }
        }

        private string? _note;
        public string? Note
        {
            get { return this._note; }
            set { SetNoteAsync(value); }
        }

        public async ValueTask SetNoteAsync(string? value)
        {
            if (this._note == value)
                return;

            using (onDataChanging())
            {
                this._note = value;
                await onDataChangedAsync(nameof(Note)).ConfigureAwait(false);
            }
        }

        // internal event DataChangedEventHandler? DataChanged;
        
        internal VisualDay(IDataContext context,TimeSpan start) : this(context,start,note:null,new List<VisualAnchor>())
        {
        }
        internal VisualDay(IDataContext context, TimeSpan start,string? note, List<VisualAnchor> anchors)
        {
            this.context = context;
            this._start = start;
            this._anchors = anchors;
            this._note = note;
        }
        private ValueTask onDataChangedAsync([CallerMemberName] string? name = null)
        {
            return this.context.NotifyOnChangedAsync(this, name!);
        }
        private IDisposable onDataChanging([CallerMemberName] string? name = null)
        {
            return this.context.NotifyOnChanging(this, name!);
        }

        public void AddAnchors(IEnumerable<VisualAnchor> anchors)
        {
            this._anchors.AddRange(anchors);
        }

        public void RemoveAnchors(int index, int count)
        {
            this._anchors.RemoveRange(index,count);
        }

        public void SetAnchor(int index, VisualAnchor anchor)
        {
            this._anchors[index] = anchor;
        }

        public void InsertAnchor(int index, VisualAnchor anchor)
        {
            this._anchors.Insert(index,anchor);
        }

        public void RemoveAnchorAt(int index)
        {
            this._anchors.RemoveAt(index);
        }

        public void SwapAnchors(int aIndex, int bIndex)
        {
            (this._anchors[aIndex], this._anchors[ bIndex]) = (this._anchors[bIndex], this._anchors[aIndex]);
        }
    }
}