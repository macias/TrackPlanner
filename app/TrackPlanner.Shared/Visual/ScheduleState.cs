using System.Collections.Generic;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared.Visual
{
    public sealed class AuxiliarySchedule : IAuxiliarySchedule
    {
        // "funny" thing but we keep track of peaks only to make undo/redo consistent for the user
        // peaks are example of calculated data so it has little sense to undo/redo any action on them
        // but skipping undo/redo for them would be surprise for the user
        
        private readonly List<GeoZPoint> peaks;
        public IReadOnlyList<GeoZPoint> Peaks => this.peaks;
        
        public AuxiliarySchedule()
        {
            this.peaks = new List<GeoZPoint>();
        }

        public void ClearPeaks()
        {
            this.peaks.Clear();
        }

        public void AddPeak(GeoZPoint peakPoint)
        {
            this.peaks.Add(peakPoint);
        }

        public void RemovePeak(int index)
        {
            this.peaks.RemoveAt(index);
        }
    }
    
    public readonly record struct ScheduleState<TVolatileState,TNodeId,TRoadId>(string Label,
        TVolatileState uiState, 
        string? filename,
        ScheduleJourney<TNodeId,TRoadId> schedule, 
        AuxiliarySchedule auxiliary,
        bool isModified) : IDataState
        where TNodeId:struct
        where TRoadId: struct
    {

    }
}