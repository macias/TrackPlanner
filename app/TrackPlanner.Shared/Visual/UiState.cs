using System;
using System.Collections.Generic;

namespace TrackPlanner.Shared.Visual
{
    public sealed class UiState
    {
        private readonly List<bool> collapsedDays;
        public IReadOnlyList<bool> CollapsedDays => this.collapsedDays;

        public bool this[Index index]
        {
            get { return this.collapsedDays[index]; }
            set { this.collapsedDays[index] = value; }
        }


        public UiState()
        {
            this.collapsedDays = new List<bool>();
        }

        public void CopyFrom(UiState state)
        {
            CopyFrom(state.CollapsedDays);
        }
        public void CopyFrom(IEnumerable<bool> days)
        {
            this.collapsedDays.Clear();
            this.collapsedDays.AddRange(days);
        }

        public void RemoveAt(int index)
        {
            this.collapsedDays.RemoveAt(index);
        }

        public void Insert(int index, bool value)
        {
        this.collapsedDays.Insert(index,value);
        }

        public void Add(bool value)
        {
            this.collapsedDays.Add(value);
        }

        public void Clear()
        {
            this.collapsedDays.Clear();
        }
    }
}