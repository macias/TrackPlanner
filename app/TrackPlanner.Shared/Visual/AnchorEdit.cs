using System;
using Geo;

namespace TrackPlanner.Shared.Visual
{
    public readonly record struct  AnchorEdit
    {
        public static AnchorEdit DaySplit => new AnchorEdit(AnchorAction.DaySplit);
        public static AnchorEdit DayMergePrevious => new AnchorEdit(AnchorAction.DayMerge);
        public static AnchorEdit AnchorDelete=> new AnchorEdit(AnchorAction.AnchorDelete);
        public static AnchorEdit AnchorPin=> new AnchorEdit(AnchorAction.AnchorPin);
        public static AnchorEdit FindPeaks=> new AnchorEdit(AnchorAction.FindPeaks);
        public static AnchorEdit OrderPrevious=> new AnchorEdit(AnchorAction.OrderPrevious);
        public static AnchorEdit OrderNext=> new AnchorEdit(AnchorAction.OrderNext);

        private enum AnchorAction
        {   
            DaySplit,
            DayMerge,
            AnchorDelete,
            AnchorPin,
            AnchorChange,
            FindPeaks,
            OrderPrevious,
            OrderNext,
        }

        private readonly AnchorAction action;
        public TimeSpan? StartTime { get; }
        public TimeSpan? BreakTime { get; }
        public GeoPoint? Point { get; }
        public string? Label { get; }
        public bool? AllowGap { get; }
        public bool? HasMap { get; }

        private AnchorEdit(AnchorAction action)
        {
            if (action == AnchorAction.AnchorChange)
                throw new ArgumentException($"{nameof(action)} = {action}");
            this.action = action;
            this.StartTime = null;
            this.BreakTime = null;
            this.Point = null;
            this.Label = null;
            this.AllowGap = null;
            this.HasMap = null;
        }
        public AnchorEdit(string? label, TimeSpan ?startTime, TimeSpan? breakTime,
            GeoPoint? point,bool? allowGap,bool? hasMap)
        {
            this.action = AnchorAction.AnchorChange;
            Label = label;
            this.StartTime = startTime;
            BreakTime = breakTime;
            Point = point;
            AllowGap = allowGap;
            HasMap = hasMap;
        }
    }
}