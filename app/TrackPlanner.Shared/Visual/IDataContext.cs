using System;
using System.Threading.Tasks;

namespace TrackPlanner.Shared.Visual
{
    //public delegate void DataChangedEventHandler(object sender, string propertyName);
    public interface IDataContext
    {
        IDisposable NotifyOnChanging(object sender, string propertyName);
        ValueTask NotifyOnChangedAsync(object sender, string propertyName);
    }

}