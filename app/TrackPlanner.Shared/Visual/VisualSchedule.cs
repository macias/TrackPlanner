using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Geo;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Visual
{
    public class VisualSchedule<TNodeId, TRoadId> :
        ISchedule<VisualDay, VisualAnchor, TNodeId, TRoadId>,
        IReadOnlyScheduleSummary<TNodeId, TRoadId>, 
        IDataContext
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly record struct ResetAutoAnchors(bool Success, int AnchorIndex,
            int IncomingLegIndex)
        {
        }

        private readonly CacheGetter<SummaryJourney> _summary;

        private readonly ILogger logger;
        private readonly IVisualReceiver visualReceiver;
        public ScheduleSettings Settings { get; private set; }

        private (int day, Index index)? flexAnchorPlaceholder;

        public (int DayIndex, int AnchorIndex)? AnchorPlaceholder
        {
            get
            {
                if (this.flexAnchorPlaceholder is var (day_idx, anchor_idx))
                    return (day_idx, anchor_idx.GetOffset(Days[day_idx].Anchors.Count));
                else
                    return null;
            }
        }

        // if the anchor index is set to beginning of the day, or at the end we consider it as relative
        // anything else is absolute
        private bool isPlaceholderAbsolute => this.flexAnchorPlaceholder is var (_, anchor_idx)
                                              && (!anchor_idx.IsFromEnd && anchor_idx.Value != 0);


        public List<VisualDay> Days { get; }
        //public int LegsCount() => this.Route.Legs.Count;

        IReadOnlyList<IReadOnlyDay> IReadOnlySchedule.Days => this.Days;

        public RoutePlan<TNodeId, TRoadId> Route { get; set; } = default!;

        public string? Filename { get; private set; }
        public string UserFriendlyProjectName => System.IO.Path.GetFileNameWithoutExtension(Filename) ?? "«not set»";

        public bool IsModified { get; set; }

        public VisualSchedule(ILogger logger, IVisualReceiver visualReceiver,
            ScheduleSettings settings)
        {
            this.logger = logger;
            this.visualReceiver = visualReceiver;
            this._summary = new CacheGetter<SummaryJourney>(this.CreateSummary);
            Settings = settings;

            this.Days = new List<VisualDay>();

            initData();
        }


        /*private ValueTask onPropertyChangedAsync([CallerMemberName] string? name = null)
        {
            return this.visualController.OnPropertyChangedAsync(name!);
        }*/

        public async ValueTask NewProjectAsync()
        {
            await sendClearAllAsync().ConfigureAwait(false);

            this.Filename = null;

            initData();

            if (this.Settings.PlannerPreferences.StartWithInsert)
            {
                await SetAnchorPlaceholderAsync(0, 0).ConfigureAwait(false);
            }
            else
            {
                await ResetAnchorPlaceholderAsync().ConfigureAwait(false);
            }

            if (this.Settings.PlannerPreferences.Home is { } home)
            {
                await AddAnchorAsync(0, 0, home, "Home", LabelSource.User).ConfigureAwait(false);
            }

            this.IsModified = false;

            await this.visualReceiver.OnScheduleSetAsync().ConfigureAwait(false);
            await this.ResetSummaryAsync().ConfigureAwait(false);
        }

        private async ValueTask sendClearAllAsync()
        {
            await notifyRemovingLegAttractionsAsync(0, this.Route.Legs.Count).ConfigureAwait(false);
            await this.visualReceiver.OnAllLegsRemovingAsync().ConfigureAwait(false);
            await this.visualReceiver.OnAllAnchorsRemovingAsync().ConfigureAwait(false);
        }

        private void initData()
        {
            this.Route = new RoutePlan<TNodeId, TRoadId>();

            this.Days.Clear();
            this.Days.Add(new VisualDay(this, Settings.PlannerPreferences.JourneyStart));
        }

        private async ValueTask notifyRemovingLegAttractionsAsync(int firstLegIndex, int lastLegIndex)
        {
            int count = lastLegIndex - firstLegIndex;
            await this.visualReceiver.OnAttractionsRemovingAsync(this.Route.Legs.Skip(firstLegIndex)
                    .Take(count).SelectMany(it => it.Attractions))
                .ConfigureAwait(false);
        }

        private ValueTask refreshAnchorLabelsAsync(int legIndex, IReadOnlyList<string?> names)
        {
            return ScheduleExtension.RefreshAnchorLabelsAsync(this, legIndex, names);
        }

        public async ValueTask SetRouteAsync(RoutePlan<TNodeId, TRoadId> route, IReadOnlyList<string?> labels)
        {
            await notifyRemovingLegAttractionsAsync(0, this.Route.Legs.Count).ConfigureAwait(false);
            await this.visualReceiver.OnAllLegsRemovingAsync().ConfigureAwait(false);

            this.Route = route;
            await this.visualReceiver.OnLegsAddedAsync(0, Route.Legs.Count);

            // because of rebuilding auto anchors placehold could be out of sync with new data
            // it is safer to reset it
            await ResetAnchorPlaceholderAsync().ConfigureAwait(false);

            await rebuildAutoAnchorsAsync().ConfigureAwait(false);
            await this.refreshAnchorLabelsAsync(0, labels).ConfigureAwait(false);
        }


        public async ValueTask AddAttractionsAsync(int dayIndex, IReadOnlyList<List<PlacedAttraction>> attractions)
        {
            var leg_idx = this.GetFirstLegIndex(dayIndex);
            var DEBUG_leg_count = this.GetLegCount(dayIndex);
            if (DEBUG_leg_count != attractions.Count)
                throw new ArgumentException($"Legs attractions mismatch {DEBUG_leg_count} vs. {attractions.Count}");
            for (int i = 0; i < attractions.Count; ++i)
                this.Route.Legs![leg_idx + i].Attractions.AddRange(attractions[i]);

            await this.visualReceiver.OnAttractionsAddedAsync(attractions.SelectMany(it => it)).ConfigureAwait(false);
        }

        public async ValueTask SplitDayAsync(int dayIndex, int anchorIndex)
        {
            this.IsModified = true;

            await rawPinAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);

            await ResetAnchorPlaceholderAsync().ConfigureAwait(false);

            var curr_day = this.Days[dayIndex];
            var new_day = new VisualDay(this, dayIndex == 0 ? this.Settings.PlannerPreferences.NextDayStart : curr_day.Start);
            var split_idx = anchorIndex + 1;
            new_day.AddAnchors(curr_day.Anchors.Skip(split_idx));
            curr_day.RemoveAnchors(split_idx, curr_day.Anchors.Count - split_idx);

            await curr_day.Anchors.Last().SetIsPinnedAsync(true).ConfigureAwait(false);

            this.Days.Insert(dayIndex + 1, new_day);

            //return (curr_day, new_day);

            this.visualReceiver.OnDaySplit(dayIndex);

            // start from the end of the day
            await this.visualReceiver.OnAnchorChangedAsync(dayIndex, curr_day.Anchors.Count - 1).ConfigureAwait(false);
            await this.ResetSummaryAsync().ConfigureAwait(false);
        }

        public async ValueTask MergeDayToPreviousAsync(int dayIndex)
        {
            await rawMergeDayToPreviousAsync(dayIndex).ConfigureAwait(false);

            await this.ResetSummaryAsync().ConfigureAwait(false);
        }

        private async ValueTask rawMergeDayToPreviousAsync(int dayIndex)
        {
            this.IsModified = true;

            var prev_day = this.Days[dayIndex - 1];
            var prev_count = prev_day.Anchors.Count;
            var cur_day = this.Days[dayIndex];
            prev_day.AddAnchors(cur_day.Anchors);
            this.Days.RemoveAt(dayIndex);

            await ResetAnchorPlaceholderAsync().ConfigureAwait(false);

            this.visualReceiver.OnDayMerged(dayIndex);

            // start from the last anchor that day
            await this.visualReceiver.OnAnchorChangedAsync(dayIndex - 1,
                // previous day could have zero anchors (because it was just deleted)
                Math.Max(0, prev_count - 1)).ConfigureAwait(false);
        }

        public async ValueTask SetScheduleAsync(IReadOnlySchedule<TNodeId, TRoadId> source,
            string? schedulePath, bool modified)
        {
            await this.sendClearAllAsync().ConfigureAwait(false);
            await ResetAnchorPlaceholderAsync().ConfigureAwait(false);

            this.Filename = schedulePath;

            this.Settings = source.Settings;
            this.Route = source.Route;

            this.Days.Clear();

            // we have to create a empty skeleton first so creating marker will know if the marker is important or not
            this.Days.AddRange(source.Days.Select(d =>
                new VisualDay(this, d.Start,d.Note, d.Anchors.Select(_ =>
                    default(VisualAnchor)!).ToList())));

            int day_idx = -1;
            foreach (var day_src in source.Days)
            {
                ++day_idx;
                var day = this.Days[day_idx];
                int anchor_idx = -1;
                foreach (var anchor_src in day_src.Anchors)
                {
                    if (!string.IsNullOrEmpty(anchor_src.Label))
                    {
                        //logger.Verbose($"DEBUG Loaded non-empty label {anchor_src.Label}");
                    }

                    var vis_anchor = new VisualAnchor(this,
                        anchor_src.UserPoint,
                        anchor_src.Label,
                        anchor_src.UserBreak,
                        isPinned: anchor_src.IsPinned,
                        labelSource: anchor_src.LabelSource,
                        allowGap:anchor_src.AllowGap,
                        hasMap:anchor_src.HasMap);

                    ++anchor_idx;
                    day.SetAnchor(anchor_idx, vis_anchor);

                    await this.visualReceiver.OnAnchorAddedAsync(vis_anchor, day_idx, anchor_idx).ConfigureAwait(false);
                }
            }

            this.IsModified = modified;

            await this.visualReceiver.OnAttractionsAddedAsync(this.Route.Legs.SelectMany(it => it.Attractions)).ConfigureAwait(false);

            await visualReceiver.OnLegsAddedAsync(0, this.Route.Legs.Count).ConfigureAwait(false);

            await this.visualReceiver.OnScheduleSetAsync().ConfigureAwait(false);

            await this.ResetSummaryAsync().ConfigureAwait(false);
        }


        private void addRawStubLegForAnchor(int dayIndex, int anchorIndex)
        {
            if (!this.HasEnoughAnchorsForBuild())
                return;

            if (this.Route.IsEmpty()) // when starting with plan go easy and just remember to add loop leg
            {
                this.Route.Legs.Insert(0, LegPlan<TNodeId, TRoadId>.CreateMissing());
                if (this.Settings.LoopRoute)
                    this.Route.Legs.Insert(0, LegPlan<TNodeId, TRoadId>.CreateMissing());
            }
            else
            {
                int leg_idx = this.GetIncomingLegIndexByAnchor(dayIndex, anchorIndex, allowWrap: false) ?? 0;
                this.Route.Legs.Insert(leg_idx, LegPlan<TNodeId, TRoadId>.CreateMissing());
            }

            logger.Verbose($"DEBUG AddMarkerAsync legs count {this.Route.Legs.Count}");
        }

        private async ValueTask rebuildAutoAnchorsAsync()
        {
            // here we remove all existing auto anchors and recreate them
            // according to currently computed plan

            //  logger.LogDebug($"before RebuildAutoAnchors {this.DEBUG_PinsToString()}");

            for (int day_idx = 0; day_idx < this.Days.Count; ++day_idx)
            {
                var day = this.Days[day_idx];
                for (int anchor_idx = day.Anchors.Count - 1; anchor_idx >= 0; --anchor_idx)
                    if (!day.Anchors[anchor_idx].IsPinned)
                        await removeRawAnchorAsync(day_idx, anchor_idx).ConfigureAwait(false);
            }

            int leg_idx = 0;
            for (int day_idx = 0; day_idx < this.Days.Count; ++day_idx)
            {
                int anchor_starting_leg_idx = day_idx == 0 ? 0 : -1; // only first day has its own starting anchor

                // we need to iterate over fixed+1 legs because we don't know in advance how many auto legs
                // were added after last (valid for current day) fixed leg. So we will iterate into the first
                // leg of the next day
                int fixed_leg_count = this.GetLegCount(day_idx);
                // logger.LogDebug($"RebuildAutoAnchors Day {day_idx} with {fixed_leg_count} fixed legs, starting at {leg_idx}/{route.Legs.Count} leg");
                int DEBUG_iter = 0;
                for (; fixed_leg_count >= 0 && leg_idx < Route.Legs.Count; ++anchor_starting_leg_idx, ++DEBUG_iter)
                {
                    if (this.Route.Legs[leg_idx].AutoAnchored)
                    {
                        //logger.LogDebug($"Inserting auto anchors at {day_idx}/{anchor_starting_leg_idx}");
                        await insertAutoAnchorOnLegStartAsync(Route.Legs[leg_idx], day_idx,
                            anchor_starting_leg_idx).ConfigureAwait(false);
                    }
                    else
                    {
                        --fixed_leg_count;
                    }

                    ++leg_idx;
                }

                //  logger.LogDebug($"loop ended after {DEBUG_iter} iterations with leg_idx {leg_idx} and fixed len {fixed_leg_count}");
                --leg_idx; // on every day we go one (fixed) leg too far, so we need to go back one leg
            }

            //  logger.LogDebug($"after RebuildAutoAnchors {this.DEBUG_PinsToString()}");

            // 0,0 -- it is a lie, we simply need to recreate all marker titles
            await this.visualReceiver.OnAnchorChangedAsync(0, 0).ConfigureAwait(false);
            await this.ResetSummaryAsync().ConfigureAwait(false);
        }

        public async ValueTask ReplaceLegAsync(int legIndex,
            // first one is fixed, the following (if any) are auto-legs
            IReadOnlyList<LegPlan<TNodeId, TRoadId>> replacementLegs,
            IReadOnlyList<string?> names)
        {
            (int day_idx, int anchor_idx, _) = this.LegIndexToVirtualDayAnchor(legIndex);

            if (isPlaceholderAbsolute)
            {
                // reset placeholder if we would be out of sync because of auto anchors
                int existing_auto = this.Days[day_idx].Anchors
                    .Skip(anchor_idx + 1)
                    .TakeWhile(it => !it.IsPinned).Count();
                if (existing_auto != replacementLegs.Count - 1)
                {
                    await ResetAnchorPlaceholderAsync().ConfigureAwait(false);
                }
            }

            var abs_placeholder = this.AnchorPlaceholder;

            // first add auto-anchors
            foreach (var auto_leg in replacementLegs.Skip(1))
            {
                ++anchor_idx;
                await insertAutoAnchorOnLegStartAsync(auto_leg, day_idx, anchor_idx).ConfigureAwait(false);
            }


            transferAttractions(this.Route.Legs[legIndex], replacementLegs.First(), atStart: true);
            await removeLegAsync(legIndex).ConfigureAwait(false);

            this.Route.Legs.InsertRange(legIndex, replacementLegs);
            await this.visualReceiver.OnLegsAddedAsync(legIndex, replacementLegs.Count).ConfigureAwait(false);

            await refreshAnchorLabelsAsync(legIndex, names).ConfigureAwait(false);

            if (abs_placeholder != this.AnchorPlaceholder)
            {
                // placeholder in absolute sense changed, thus notification
                await this.visualReceiver.OnPlaceholderChangedAsync().ConfigureAwait(false);
            }
        }

        private ValueTask insertAutoAnchorOnLegStartAsync(LegPlan<TNodeId, TRoadId> leg,
            int dayIndex, int anchorIndex)
        {
            var coords = leg.FirstStep().Point.Convert2d();
            VisualAnchor anchor = insertRawAnchor(coords, "", LabelSource.AutoMap,
                Settings.PlannerPreferences.DefaultAnchorBreak,
                dayIndex, anchorIndex, isPinned: false);

            return this.visualReceiver.OnAnchorAddedAsync(anchor, dayIndex, anchorIndex);
        }

        private VisualAnchor insertRawAnchor(GeoPoint coords, string title, LabelSource source, TimeSpan breakTime,
            int dayIndex, int anchorIndex, bool isPinned)
        {
            var day = this.Days[dayIndex];
            if (anchorIndex < 0 || anchorIndex > day.Anchors.Count)
                throw new ArgumentOutOfRangeException($"{nameof(anchorIndex)}={anchorIndex} out of range {day.Anchors.Count}.");

            this.IsModified = true;

            var anchor = new VisualAnchor(this,
                coords,
                title,
                breakTime,
                isPinned: isPinned,
                labelSource: source,
                allowGap:false,
                hasMap:false);


            day.InsertAnchor(anchorIndex, anchor);

            return anchor;
        }

        public async ValueTask ClearAttractionsAsync(int? dayIndex)
        {
            int first_leg_idx, last_leg_idx;
            if (dayIndex is { } day_idx)
            {
                first_leg_idx = this.GetFirstLegIndex(day_idx);
                last_leg_idx = first_leg_idx + this.GetLegCount(day_idx);
            }
            else
            {
                first_leg_idx = 0;
                last_leg_idx = this.Route.Legs.Count;
            }

            await notifyRemovingLegAttractionsAsync(first_leg_idx, last_leg_idx).ConfigureAwait(false);

            for (int leg_idx = first_leg_idx; leg_idx < last_leg_idx; ++leg_idx)
            {
                this.Route.Legs[leg_idx].Attractions.Clear();
            }
        }

        public async ValueTask PinAnchorAsync(int dayIndex, int anchorIndex)
        {
            await rawPinAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);

            await this.visualReceiver.OnAnchorChangedAsync(dayIndex, anchorIndex).ConfigureAwait(false);
            await this.ResetSummaryAsync().ConfigureAwait(false);
        }

        private async ValueTask rawPinAnchorAsync(int dayIndex, int anchorIndex)
        {
            var anchor = this.Days[dayIndex].Anchors[anchorIndex];
            if (anchor.IsPinned)
                return;

            await anchor.SetIsPinnedAsync(true).ConfigureAwait(false);
            int leg_idx = (this.GetIncomingLegIndexByAnchor(dayIndex, anchorIndex, allowWrap: false) ?? -1) + 1;
            this.Route.Legs[leg_idx].AutoAnchored = false;
        }

        public async ValueTask DeleteAnchorAsync(int dayIndex, int anchorIndex)
        {
            int DEBUG_leg_count = this.Route.Legs.Count;

            bool placeholder_set = AnchorPlaceholder.HasValue;

            var day = this.Days[dayIndex];
            bool is_last_of_day = anchorIndex == day.Anchors.Count - 1;
            var previous_pinned = this.IndexedReadOnlyAnchors()
                .TakeWhile(it => it.anchor != day.Anchors[anchorIndex])
                .LastOrNone(it => it.anchor.IsPinned);

            (bool changed, anchorIndex, int incoming_leg_idx) = await resetAnchorAutoSurroundingsAsync(dayIndex,
                anchorIndex, removeAttractions: true).ConfigureAwait(false);
            if (changed)
            {
                // O--x---O
                // since we are about to remove one anchor we need to remove also one leg
                // so there would be one leg between the adjacent anchors

                // for the deleting starting anchor we need to bump up leg index
                await removeLegAsync(Math.Max(0, incoming_leg_idx)).ConfigureAwait(false);

                if (this.Route.Legs.Count > 0 && !this.HasEnoughAnchorsForBuild(3))
                {
                    // we have 2 points and we are about to delete one of them
                    // delete the last leg as well
                    await removeLegAsync(0).ConfigureAwait(false);
                }
            }

            await removeRawAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);

            await this.visualReceiver.OnPlanChangedAsync().ConfigureAwait(false);

            if (placeholder_set)
            {
                if (!previous_pinned.HasValue)
                    await SetAnchorPlaceholderAsync(0, 0).ConfigureAwait(false);
                else
                {
                    var (_, prev_day_idx, prev_anchor_idx) = previous_pinned.Value;
                    var prev_day = this.Days[prev_day_idx];
                    // if our previous anchor is the last of the day, but we didn't were last of the day
                    // place placeholder at the beginning of the next day
                    if (prev_day_idx < this.Days.Count - 1
                        && !is_last_of_day
                        && prev_day.Anchors.Count - 1 == prev_anchor_idx)
                        await SetAnchorPlaceholderAsync(prev_day_idx + 1, 0).ConfigureAwait(false);
                    else
                        await SetAnchorPlaceholderAsync(prev_day_idx, prev_anchor_idx + 1).ConfigureAwait(false);
                }
            }

            await ResetSummaryAsync().ConfigureAwait(false);
        }

        private async ValueTask removeRawAnchorAsync(int dayIndex, int anchorIndex)
        {
            this.IsModified = true;

            var day = this.Days[dayIndex];
            var anchor = day.Anchors[anchorIndex];

            await this.visualReceiver.OnAnchorRemovingAsync(anchor).ConfigureAwait(false);
            day.RemoveAnchorAt(anchorIndex);


            if (this.Days.Count > 1)
            {
                // here we have case when we removed the last/only
                // anchor from the day and the day now is empty
                // (in some cases the day can be legally empty)
                if (day.Anchors.Count == 0
                    && dayIndex > 0
                    && (dayIndex < this.Days.Count - 1 || !this.Settings.LoopRoute))
                {
                    await rawMergeDayToPreviousAsync(dayIndex).ConfigureAwait(false);
                }
                // this is different case -- we deleted last anchor of the day
                // so we merge the following/next day into it, because we no longer
                // have camp-anchor
                else if ((anchorIndex == day.Anchors.Count
                          && dayIndex < this.Days.Count - 1)
                         ||
                         (dayIndex == 0 && day.Anchors.Count == 1) // the first day has to have start and end
                        )
                {
                    // we cannot remove "empty" days when they are looped, for example legal case
                    // (1)---(2)|----(*1)|
                    // we have only two anchors, second day have no anchors at all, but this day
                    // is looped so we know how to route such day
                    //if (dayIndex < this.Days.Count - 1)
                    //  ++dayIndex;
                    await rawMergeDayToPreviousAsync(dayIndex + 1).ConfigureAwait(false);
                }
            }
        }

        public ValueTask ResetSummaryAsync()
        {
            if (!this._summary.Reset())
                // if it was already reset, return
                return ValueTask.CompletedTask;

            return this.visualReceiver.OnSummarySetAsync();
        }

        public async ValueTask SnapToLegAsync(int? legIndex, GeoPoint point, string title, LabelSource source,
            TimeSpan breakTime)
        {
            this.IsModified = true;

            int? day_idx = null;
            int? anchor_idx = null;
            if (legIndex is {} leg_idx)
            {
                (day_idx, anchor_idx) = this.LegIndexToActualDayAnchor(leg_idx);
                // this was the starter of the leg, so we have to insert anchor after that
                ++anchor_idx;

                (day_idx, anchor_idx) = this.CampingAdjustInsertionAnchorIndices(day_idx.Value, anchor_idx!.Value);
            }

            if (day_idx == null)
            {
                if (AnchorPlaceholder.HasValue)
                    (day_idx, anchor_idx) = AnchorPlaceholder.Value;
                else
                    (day_idx, anchor_idx) = (0, Days[0].Anchors.Count);
            }

            if (isPlaceholderAbsolute && AnchorPlaceholder != (day_idx.Value, anchor_idx!.Value))
                await ResetAnchorPlaceholderAsync().ConfigureAwait(false);
            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            await addAnchorAsync(day_idx.Value, anchor_idx!.Value,
                point, title, source, breakTime, removeAttractions: false).ConfigureAwait(false);
        }

        public async ValueTask AddRouteAttraction(int legIndex, int attrIndex,
            PlacedAttraction attraction)
        {
            // we could either accept indices, or attraction alone, but providing
            // all data is better for testing/restoring user cases when the map
            // data changes
            this.IsModified = true;

            List<PlacedAttraction> leg_attr = this.Route.Legs[legIndex].Attractions;
            //PlacedAttraction attraction = leg_attr[attrIndex];
            var before_attr = leg_attr.Take(attrIndex).ToList();
            var after_attr = leg_attr.Skip(attrIndex + 1).ToList();
            await this.visualReceiver.OnAttractionsRemovingAsync(new[] {leg_attr[attrIndex]}).ConfigureAwait(false);
            this.Route.Legs[legIndex].Attractions.Clear();

            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            var (day_idx, anchor_idx, _) = this.LegIndexToVirtualDayAnchor(legIndex);
            ++anchor_idx;
            //if (false && isPlaceholderAbsolute && AnchorPlaceholder != (day_idx, anchor_idx))
            //  rawResetAnchorPlaceholder();
            var anchor = await this.addAnchorAsync(day_idx, anchor_idx, attraction.Point.Convert2d(),
                attraction.Attraction.GetCompactLabel(),
                LabelSource.Attraction, Settings.PlannerPreferences.DefaultAttractionBreak,
                removeAttractions: false).ConfigureAwait(false);
            anchor_idx = this.Days[day_idx].Anchors.IndexOf(anchor);
            legIndex = this.GetIncomingLegIndexByAnchor(day_idx, anchor_idx, allowWrap: false)!.Value;
            this.Route.Legs[legIndex].Attractions.AddRange(before_attr);
            this.Route.Legs[legIndex + 1].Attractions.InsertRange(0, after_attr);
        }

        private async ValueTask resetLegAsync(int legIndex, bool removeAttractions)
        {
            if (this.Route.Legs[legIndex].IsMissing)
                return;

            if (removeAttractions)
            {
                await notifyRemovingLegAttractionsAsync(legIndex, legIndex + 1).ConfigureAwait(false);
            }

            await this.visualReceiver.OnLegRemovingAsync(legIndex).ConfigureAwait(false);
            var missing = LegPlan<TNodeId, TRoadId>.CreateMissing();
            if (!removeAttractions)
                transferAttractions(this.Route.Legs[legIndex], missing, atStart: false);
            this.Route.Legs[legIndex] = missing;
        }

        private async ValueTask<ResetAutoAnchors> resetAnchorAutoSurroundingsAsync(int dayIndex,
            int anchorIndex, bool removeAttractions)
        {
            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            if (this.Route.IsEmpty())
            {
                return new ResetAutoAnchors(false, anchorIndex, -1);
            }

            const int incoming_pre_leg = -1;

            var incomingLegIndex = this.GetIncomingLegIndexByAnchor(dayIndex, anchorIndex, allowWrap: false) ?? incoming_pre_leg;
            //logger.LogDebug($"Reset adjacent legs leg:{incomingLegIndex}/{this.Route.Legs.Count} anchor:{anchorIndex}");
            if (incomingLegIndex >= 0) // incoming
                await resetLegAsync(incomingLegIndex, removeAttractions).ConfigureAwait(false);
            if (incomingLegIndex < this.Route.Legs.Count - 1) // outgoing
                await resetLegAsync(incomingLegIndex + 1, removeAttractions).ConfigureAwait(false);

            if (dayIndex == 0 && anchorIndex == 0 && this.Settings.LoopRoute)
                await resetLegAsync(this.Route.Legs.Count - 1, removeAttractions).ConfigureAwait(false);


            // removing auto elements

            {
                var anchor_idx_next = anchorIndex + 1;
                var day_idx_next = dayIndex;
                // if the current anchor is last of the day we have to remove adjacent auto-anchors
                // from the next day
                if (anchor_idx_next == this.Days[day_idx_next].Anchors.Count && day_idx_next < this.Days.Count - 1)
                {
                    ++day_idx_next;
                    anchor_idx_next = 0;
                }

                var anchors = this.Days[day_idx_next].Anchors;
                // logger.LogDebug($"Reset auto anchors {anchors.Count} {String.Join(",", anchors.Select(it => it.IsPinned))}");
                while (anchor_idx_next < anchors.Count && !anchors[anchor_idx_next].IsPinned) // removing next auto-anchors
                {
                    //   logger.LogDebug($"Removing next auto anchor at {day_idx_next}:{anchor_idx_next} with leg {incomingLegIndex + 2}");
                    await removeRawAnchorAsync(day_idx_next, anchor_idx_next).ConfigureAwait(false);
                    if (!removeAttractions)
                    {
                        transferAttractions(this.Route.Legs[incomingLegIndex + 2],
                            this.Route.Legs[incomingLegIndex + 1], atStart: false);
                    }

                    await removeLegAsync(incomingLegIndex + 2);
                }
            }


            {
                var is_loop_start = dayIndex == 0 && anchorIndex == 0 && this.Settings.LoopRoute;

                var anchors = this.Days[dayIndex].Anchors;
                if (is_loop_start)
                {
                    dayIndex = this.Days.Count - 1;
                    anchors = this.Days[dayIndex].Anchors;

                    anchorIndex = anchors.Count; // set anchor as next to last
                    incomingLegIndex = this.Route.Legs.Count;
                    // logger.LogDebug($"switch to the end of anchors {dayIndex}:{anchorIndex}");
                }

                bool removed = false;
                while (anchorIndex > 0 && !anchors[anchorIndex - 1].IsPinned) // removing previous auto-anchors
                {
                    removed = true;
                    --incomingLegIndex;
                    --anchorIndex;
                    // logger.LogDebug($"Removing prev auto anchor at {anchorIndex - 1} with leg {incomingLegIndex - 1}");
                    await removeRawAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);
                    if (!removeAttractions)
                    {
                        if (DEBUG_SWITCH.Enabled)
                        {
                            ;
                        }

                        var transfer_to_idx = incomingLegIndex + 1;
                        if (transfer_to_idx == this.Route.Legs.Count)
                            transfer_to_idx = 0;
                        transferAttractions(this.Route.Legs[incomingLegIndex], this.Route.Legs[transfer_to_idx], atStart: true);
                    }

                    await removeLegAsync(incomingLegIndex).ConfigureAwait(false);
                }

                // when we removed the last auto legs, we need to reset current last leg, because it is connection
                // between real-last anchor and the ending (looped) anchor
                if (removed && is_loop_start && this.Route.Legs.Count > 0)
                    await resetLegAsync(this.Route.Legs.Count - 1, removeAttractions).ConfigureAwait(false);

                if (is_loop_start)
                {
                    anchorIndex = 0;
                    incomingLegIndex = incoming_pre_leg;
                }
            }

            // logger.LogDebug($"resetAnchorSurroundings finished with {this.DEBUG_PinsToString()}");

            return new ResetAutoAnchors(true, anchorIndex, incomingLegIndex);
        }

        private void transferAttractions(LegPlan<TNodeId, TRoadId> sourceLeg, LegPlan<TNodeId, TRoadId> targetLeg,
            bool atStart)
        {
            targetLeg.Attractions.InsertRange(atStart ? 0 : ^0, sourceLeg.Attractions);
            sourceLeg.Attractions.Clear();
        }

        private async ValueTask removeLegAsync(int legIndex)
        {
            this.IsModified = true;
            await resetLegAsync(legIndex, removeAttractions: true).ConfigureAwait(false);
            this.Route.Legs.RemoveAt(legIndex);
        }

        public ValueTask<VisualAnchor> AddAnchorAsync(int dayIndex, int anchorIndex,
            GeoPoint coords, string title, LabelSource source)
        {
            return addAnchorAsync(dayIndex, anchorIndex, coords, title, source,
                Settings.PlannerPreferences.DefaultAnchorBreak,
                removeAttractions: true);
        }

        private async ValueTask<VisualAnchor> addAnchorAsync(int dayIndex, int anchorIndex,
            GeoPoint coords, string title, LabelSource source, TimeSpan breakTime, bool removeAttractions)
        {
            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            var abs_placeholder = this.AnchorPlaceholder;
            bool placeholder_in_sync = this.AnchorPlaceholder == (DayIndex: dayIndex, AnchorIndex: anchorIndex);

            VisualAnchor anchor = insertRawAnchor(coords, title, source,
                breakTime,
                dayIndex, anchorIndex, isPinned: true);

            await this.visualReceiver.OnAnchorAddedAsync(anchor, dayIndex, anchorIndex).ConfigureAwait(false);

            addRawStubLegForAnchor(dayIndex, anchorIndex);

            (_, anchorIndex, _) = await resetAnchorAutoSurroundingsAsync(dayIndex, anchorIndex,
                removeAttractions).ConfigureAwait(false);

            await this.visualReceiver.OnPlanChangedAsync().ConfigureAwait(false);

            if (placeholder_in_sync)
            {
                // after rebuild we could have more or less anchors so we have to refresh the index
                anchorIndex = this.Days[dayIndex].Anchors.IndexOf(anchor);
                // change the placeholder after sending notification about entire plan, because it triggers
                // newly added leg refreshed, and we might need to get reference to obtained leg

                // using new updated anchor index
                await this.SetAnchorPlaceholderAsync(dayIndex, anchorIndex + 1).ConfigureAwait(false);
            }
            else if (abs_placeholder != this.AnchorPlaceholder)
            {
                // relative placeholder didn't change, but in terms of absolute values -- yes
                // thus we need to notify receivers about it
                await this.visualReceiver.OnPlaceholderChangedAsync().ConfigureAwait(false);
            }
            //    await ResetSummaryAsync().ConfigureAwait(false);

            return anchor;
        }

        public ValueTask SetAnchorPlaceholderAsync(int dayIndex, int anchorIndex)
        {
            rawSetAnchorPlaceholder(dayIndex, anchorIndex);
            //logger.LogDebug($"DEBUG setting insert at {this.VisualSchedule.anchorPlaceholder}");

            return this.visualReceiver.OnPlaceholderChangedAsync();
        }

        private void rawSetAnchorPlaceholder(int dayIndex, int anchorIndex)
        {
            if (Days[dayIndex].Anchors.Count == anchorIndex) // for placeholder at the end set it to relative index from the end
                this.flexAnchorPlaceholder = (dayIndex, Index.FromEnd(0));
            else
                this.flexAnchorPlaceholder = (dayIndex, anchorIndex);
        }

        public ValueTask ResetAnchorPlaceholderAsync()
        {
            rawResetAnchorPlaceholder();
            return this.visualReceiver.OnPlaceholderChangedAsync();
        }

        private void rawResetAnchorPlaceholder()
        {
            this.flexAnchorPlaceholder = null;
        }

        private async Task handleLoopedChangedAsync(bool isLooped)
        {
            int anchor_count = this.Days.Sum(it => it.Anchors.Count);

            if (!this.Route.IsEmpty())
            {
                if (isLooped)
                {
                    this.Route.Legs.Add(LegPlan<TNodeId, TRoadId>.CreateMissing());
                }
                else
                {
                    {
                        // this variable is valid only in this scope, because we could end up
                        // deleting entire last day
                        var anchors = this.Days[^1].Anchors;
                        while (true)
                        {
                            // last day can have legally no pinned anchors, because entire day
                            // could be from the last camp (previous day) to the starting point
                            if (anchors.Count == 0 || anchors[^1].IsPinned)
                                break;
                            await this.removeRawAnchorAsync(this.Days.Count - 1, anchors.Count - 1).ConfigureAwait(false);
                        }
                    }

                    if (this.Days[^1].Anchors.Count == 0 && this.Days.Count > 1)
                    {
                        // previously the last day was between camp previous day and starting point
                        // so it didn't have any anchors on its own, but when loop is switched off
                        // it cannot exists any longer
                        await this.rawMergeDayToPreviousAsync(this.Days.Count - 1).ConfigureAwait(false);
                    }

                    // all those legs are covered by current anchors...
                    var valid_leg_count = 1 + this.GetIncomingLegIndexByAnchor(this.Days.Count - 1,
                        this.Days[^1].Anchors.Count - 1, allowWrap: false);
                    // ...anything more we remove, because those are the parts which made the loop
                    while (this.Route.Legs.Count > valid_leg_count)
                    {
                        await this.removeLegAsync(this.Route.Legs.Count - 1).ConfigureAwait(false);
                    }
                }
            }


            if (this.Days[0].Anchors.Count > 0)
            {
                var (d_idx, a_idx) = this.GetPreviousAnchorIndices(this.Days.Count - 1, this.Days[^1].Anchors.Count)!.Value;
                await this.visualReceiver.OnAnchorChangedAsync(d_idx, a_idx).ConfigureAwait(false);
            }

            if (this.Days.Sum(it => it.Anchors.Count) != anchor_count)
            {
                // if we removed some anchors let's delete placeholder
                await ResetAnchorPlaceholderAsync().ConfigureAwait(false);
            }

            await this.visualReceiver.OnLoopChangedAsync().ConfigureAwait(false);
        }


        public async ValueTask SetSettingsAsync(ScheduleSettings prefs)
        {
            this.IsModified = true;

            var abs_placeholder = this.AnchorPlaceholder;

            var previous = this.Settings;
            this.Settings = prefs;
            if (previous.LoopRoute != prefs.LoopRoute)
                await handleLoopedChangedAsync(prefs.LoopRoute).ConfigureAwait(false);
            if (this.Settings.RouterPreferences.UseStableRoads != previous.RouterPreferences.UseStableRoads)
                await this.visualReceiver.OnPlanChangedAsync().ConfigureAwait(false);

            await ResetSummaryAsync().ConfigureAwait(false);

            if (abs_placeholder != this.AnchorPlaceholder)
            {
                // placeholder in absolute sense changed, thus notification
                await this.visualReceiver.OnPlaceholderChangedAsync().ConfigureAwait(false);
            }
        }

        public async ValueTask NotifyOnChangedAsync(object sender, string propertyName)
        {
            if (sender is VisualAnchor anchor)
            {
                if (propertyName is nameof(anchor.UserBreak) or nameof(anchor.Label))
                {
                    await ResetSummaryAsync().ConfigureAwait(false);
                }
                else if (propertyName is nameof(anchor.AllowGap))
                    await this.onAnchorAllowGapChangedAsync(anchor).ConfigureAwait(false);
            }
            else if (sender is VisualDay day && propertyName == nameof(day.Start))
            {
                await ResetSummaryAsync().ConfigureAwait(false);
            }

            await this.visualReceiver.NotifyOnChangedAsync(sender, propertyName).ConfigureAwait(false);
        }


        public void DataSaved(string schedulePath)
        {
            if (this.Filename == null)
                this.Filename = schedulePath;
            IsModified = false;
        }

        private ValueTask onDataChangedAsync([CallerMemberName] string? name = null)
        {
            return this.NotifyOnChangedAsync(this, name!);
        }

        public IDisposable NotifyOnChanging(object sender, string propertyName)
        {
            IsModified = true;

            return this.visualReceiver.NotifyOnChanging(sender, propertyName);
        }

        public async ValueTask ChangeAnchorPositionAsync(GeoPoint position, int dayIndex, int anchorIndex)
        {
            this.IsModified = true;

            var abs_placeholder = this.AnchorPlaceholder;

            await rawPinAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);

            var anchor = this.Days[dayIndex].Anchors[anchorIndex];

            await anchor.SetUserPointAsync(position).ConfigureAwait(false);
            // 2023-05-18: even user labels should be reset when changing position, example: user sets camping,
            // moves it, the label sticks despite the new position could have quite different conditions, so this
            // would be actually dangerous
            //if (anchor.LabelSource != LabelSource.User)
            {
                await anchor.SetLabelAsync("", LabelSource.AutoMap).ConfigureAwait(false);
            }

            (bool changed, _, _) = await resetAnchorAutoSurroundingsAsync(dayIndex, anchorIndex, removeAttractions: true).ConfigureAwait(false);

            if (changed && isPlaceholderAbsolute)
                await ResetAnchorPlaceholderAsync().ConfigureAwait(false);
            else if (abs_placeholder != this.AnchorPlaceholder)
            {
                // placeholder in absolute sense changed, thus notification
                await this.visualReceiver.OnPlaceholderChangedAsync().ConfigureAwait(false);
            }

            await this.visualReceiver.OnAnchorMovedAsync(anchor).ConfigureAwait(false);
        }


        private async ValueTask onAnchorAllowGapChangedAsync(VisualAnchor anchor)
        {
            this.IsModified = true;

            var (day_idx, anchor_idx) = this.IndexOf(anchor);
            
            var abs_placeholder = this.AnchorPlaceholder;

            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }
            (bool changed, _, _) = await resetAnchorAutoSurroundingsAsync(day_idx, anchor_idx, 
                removeAttractions: true).ConfigureAwait(false);

            if (changed && isPlaceholderAbsolute)
                await ResetAnchorPlaceholderAsync().ConfigureAwait(false);
            else if (abs_placeholder != this.AnchorPlaceholder)
            {
                // placeholder in absolute sense changed, thus notification
                await this.visualReceiver.OnPlaceholderChangedAsync().ConfigureAwait(false);
            }

            await this.visualReceiver.OnPlanChangedAsync().ConfigureAwait(false);
        }
        
        public SummaryJourney GetSummary()
        {
            return this._summary.Value; 
        }

        public async ValueTask OrderAnchorAsync(int dayIndex, int anchorIndex, int change)
        {
            this.IsModified = true;

            // this will clear auto-anchors around
            (_, anchorIndex, _) = await resetAnchorAutoSurroundingsAsync(dayIndex, anchorIndex, removeAttractions: false).ConfigureAwait(false);
            // so now the next/previous anchor is pinned one for sure
            var adj_day_idx = dayIndex;
            var adj_anchor_idx = anchorIndex + change;
            if (adj_anchor_idx < 0)
            {
                --adj_day_idx;
                adj_anchor_idx = Days[adj_day_idx].Anchors.Count - 1;
            }
            else if (adj_anchor_idx >= Days[adj_day_idx].Anchors.Count)
            {
                ++adj_day_idx;
                adj_anchor_idx = 0;
            }

            if (adj_day_idx == dayIndex)
                this.Days[dayIndex].SwapAnchors(anchorIndex, adj_anchor_idx);
            else
            {
                var anchor = this.Days[dayIndex].Anchors[anchorIndex];
                this.Days[dayIndex].SetAnchor(anchorIndex, this.Days[adj_day_idx].Anchors[adj_anchor_idx]);
                this.Days[adj_day_idx].SetAnchor(adj_anchor_idx, anchor);
            }

            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            await resetAnchorAutoSurroundingsAsync(adj_day_idx, adj_anchor_idx, removeAttractions: false).ConfigureAwait(false);

            await this.visualReceiver.OnPlanChangedAsync().ConfigureAwait(false);

            // in theory we could introduce complex check if placeholder was affected
            // but KISS
            await ResetAnchorPlaceholderAsync().ConfigureAwait(false);
        }
    }
}