using System;
using System.Threading.Tasks;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Visual
{
    public sealed class StateManager<TState>
    where TState:IDataState
    {
        private sealed class HistoryScope : IDisposable
        {
            private readonly StateManager<TState> manager;

            public HistoryScope(StateManager<TState> manager)
            {
                this.manager = manager;
            }
            public void Dispose()
            {
#if DEBUG
                if (DEBUG_SWITCH.Enabled)
                {
                    ;
                }
                #endif
                this.manager.stateHistory.Add(this.manager.stateGetter(this.manager.stateLabel));
                this.manager.alreadyExecuting = false;
            }
        }
        
        private bool alreadyExecuting;
        private readonly StateHistory<TState> stateHistory;
        private readonly Func<string,TState> stateGetter;
        private readonly Func<TState,ValueTask> stateAsyncSetter;
        private readonly HistoryScope scopeOpener;
        private string stateLabel;


        public bool CanUndo => this.stateHistory.CanUndo || this.alreadyExecuting;
        public bool CanRedo => this.stateHistory.CanRedo && !this.alreadyExecuting;
        public string? UndoLabel => CanUndo ? (this.alreadyExecuting? this.stateLabel: this.stateHistory.PeekCurrent().Label) : null;
        public string? RedoLabel => CanRedo ? this.stateHistory.PeekRedo().Label : null;


        public StateManager(Func<string,TState> stateGetter, 
            Func<TState,ValueTask> stateAsyncSetter,
            int stateHistoryLimit)
        {
            this.stateLabel = "";
            this.stateGetter = stateGetter;
            this.stateAsyncSetter = stateAsyncSetter;
            this.scopeOpener = new HistoryScope(this);
            
            this.stateHistory = new StateHistory<TState>(stateGetter(this.stateLabel),
                stateHistoryLimit);
        }

        public void Reset()
        {
            this.stateLabel = "";
            this.stateHistory.Reset(stateGetter(this.stateLabel));
        }
        
        public IDisposable OpenScope(string label)
        {
            #if DEBUG
            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }
            #endif
            if (this.alreadyExecuting)
                return CompositeDisposable.None;
            else
            {
                this.stateLabel = label;
                this.alreadyExecuting = true;
                return this.scopeOpener;
            }
        }
        
        public ValueTask TryUndoAsync()
        {
            if (this.alreadyExecuting)
                throw new InvalidOperationException("Cannot undo while running");
            
            if (!this.stateHistory.CanUndo)
                return ValueTask.CompletedTask;

            return this.stateAsyncSetter(this.stateHistory.Undo());
        }

        public ValueTask TryRedoAsync()
        {
            if (this.alreadyExecuting)
                throw new InvalidOperationException("Cannot redo while running");

            if (!this.stateHistory.CanRedo)
                return ValueTask.CompletedTask;

            return this.stateAsyncSetter(this.stateHistory.Redo());
        }


    }
}