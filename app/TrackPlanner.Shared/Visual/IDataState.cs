namespace TrackPlanner.Shared.Visual
{
    public interface IDataState
    {
        string Label { get; }
    }
  
}