using System.Collections.Generic;

namespace TrackPlanner.Shared.Visual
{
    public sealed class StateHistory<T>
    {
        private readonly int limit;
        private readonly List<T> states;
        private int nextIndex;

        public bool CanUndo => this.nextIndex > 1;
        public bool CanRedo => this.nextIndex < this.states.Count;

        public StateHistory(T initState, int limit)
        {
            this.limit = limit;
            this.states = new List<T>();
            Reset(initState);
        }

        public void Reset(T initState)
        {
            this.states.Clear();
            Add(initState);
        }
        public void Add(T state)
        {
            if (this.nextIndex < this.states.Count)
                this.states.RemoveRange(this.nextIndex, this.states.Count - this.nextIndex);
            if (this.states.Count == limit)
                this.states.RemoveAt(0);

            this.states.Add(state);
            this.nextIndex = this.states.Count;
        }

        public T Undo()
        {
            var state = this.states[this.nextIndex - 2];
            --this.nextIndex;
            return state;
        }

        public T Redo()
        {
            var state = PeekRedo();
            ++this.nextIndex;
            return state;
        }
        
        public T PeekCurrent()
        {
            return this.states[this.nextIndex - 1];
        }

        public T PeekRedo()
        {
            return this.states[this.nextIndex];
        }
    }
}