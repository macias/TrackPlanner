using System.Collections.Generic;
using System.Threading.Tasks;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Visual
{
    public interface IVisualReceiver :IDataContext
    {

        ValueTask OnAnchorChangedAsync(int dayIndex, int anchorIndex);
        ValueTask OnAnchorAddedAsync(VisualAnchor anchor, int dayIndex, int anchorIndex);
        ValueTask OnAllAnchorsRemovingAsync();
        ValueTask OnAnchorRemovingAsync(VisualAnchor anchor);
        ValueTask OnAnchorMovedAsync(VisualAnchor anchor);

        ValueTask OnPlanChangedAsync();

        void OnDaySplit(int dayIndex);
        void OnDayMerged(int dayIndex);

        ValueTask OnLegRemovingAsync(int legIndex);
        ValueTask OnLegsAddedAsync(int firstLegIndex,int legCount);
        ValueTask OnAllLegsRemovingAsync();

        ValueTask OnAttractionsRemovingAsync(IEnumerable<PlacedAttraction> attractions);
        ValueTask OnAttractionsAddedAsync(IEnumerable<PlacedAttraction> attractions);
        
        ValueTask OnSummarySetAsync();

        ValueTask OnPlaceholderChangedAsync();
        ValueTask OnLoopChangedAsync();

        ValueTask OnScheduleSetAsync();
        
        ValueTask OnPeakAddedAsync(GeoZPoint peak);
        ValueTask OnPeaksClearedAsync();
    }
}