using System;
using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Visual
{
    public interface IVisualDay : IReadOnlyDay
    {
        new IReadOnlyList<VisualAnchor> Anchors { get; }
        //new TimeSpan Start { get; set; }
    }

    
}