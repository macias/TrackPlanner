using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Visual
{
    public interface IAuxiliarySchedule
    {
        IReadOnlyList<GeoZPoint> Peaks { get; }
    }
}