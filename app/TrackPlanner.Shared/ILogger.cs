﻿namespace TrackPlanner.Shared
{
    public interface ILogger
    {
        void Log(MessageLevel level, string? message);
        ILogger With(SinkSettings settings);
    }
}