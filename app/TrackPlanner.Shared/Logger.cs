﻿using System;
using System.IO;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared 
{
    public static class Logger
    {
        public static IDisposable Create(LoggerSettings settings, out ILogger logger)
        {
            FileStream? stream = null;
            StreamWriter? writer = null;
            if (settings.LogPath != null)
            {
                var log_dir = System.IO.Path.GetDirectoryName(settings.LogPath);
                if (log_dir != null)
                    System.IO.Directory.CreateDirectory(log_dir);

                stream = new FileStream(settings.LogPath, FileMode.Append,
                    FileAccess.Write, 
                    FileShare.None);
                writer = new StreamWriter(stream);
            }

            var logger_impl = new LoggerImpl(writer, settings.Sink);
                           
            logger = logger_impl;
            
            logger.Info($"Using {Environment.Version} runtime");
            
            IDisposable result = CompositeDisposable.None;
            if (writer != null)
              result =  new CompositeDisposable(stream!, writer).Stack(
                  () =>
                  {
                      logger_impl.Info($"Logging ended.");
                  });
            return result;
        }

        public static ILogger Create()
        {
            return new LoggerImpl(null,SinkSettings.CreateEnabled());
        }


        private sealed class LoggerImpl : ILogger
        {
            private readonly TextWriter? writer;
            private readonly SinkSettings settings;

            public LoggerImpl(TextWriter? writer,SinkSettings settings)
            {
                this.writer = writer;
                this.settings = settings with {};
            }

            public void Log(MessageLevel level, string? message)
            {
                message = $"[{level.ToString().ToUpperInvariant()}] {message}";
                if (this.settings.ConsoleEnabled && level <= this.settings.ConsoleLevel)
                    Console.WriteLine($"[{DataFormat.Format(TimeOnly.FromDateTime(DateTime.UtcNow))}]{message}");
                if (this.settings.FileEnabled && level <= this.settings.FileLevel)
                {
                    writer?.WriteLine($"[{DataFormat.Format(DateTimeOffset.UtcNow)}]{message}");
                    if (level <= MessageLevel.Warning)
                        writer?.Flush();
                }
            }

            public ILogger With(SinkSettings settings)
            {
                return new LoggerImpl(this.writer, settings);
            }
        }
    }
}