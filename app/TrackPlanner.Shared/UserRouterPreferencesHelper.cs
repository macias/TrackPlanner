﻿using MathUnit;
using System;
using System.Collections.Generic;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Shared
{
    public static class UserRouterPreferencesHelper
    {
        public static UserRouterPreferences CreateBikeOriented()
        {
            return new UserRouterPreferences()
            {
                AddedMotorDangerousTrafficFactor = 0.40,
                BikeFootHighTrafficScaleFactor = 0.2,//75,
                AddedMotorUncomfortableTrafficFactor = 0.1,
                AddedHighVolumeTrafficFactor = 0.75,
                TrafficSuppression = Length.FromKilometers(3),

                // this is actual help to avoid high-traffic road crossing,
                // but works also well to avoid "shortcuts" while planning
                // without it program woud prefer to make "shortcut" (avoiding high-traffic)
                // on short service road just to return to the main road after 30 meters or so
                JoiningHighTraffic = TimeSpan.FromSeconds(90),
            };
        }

        public static UserRouterPreferences SetCustomSpeeds(this UserRouterPreferences prefs)
        {
            prefs.Speeds = new Dictionary<RoadSpeeding, SpeedInfo>()
            {
                [RoadSpeeding.Asphalt] = SpeedInfo.FromKilometersPerHour(16.5,35),
                [RoadSpeeding.HardBlocks] = SpeedInfo.FromKilometersPerHour(7.5,12),

                [RoadSpeeding.Ground] = SpeedInfo.FromKilometersPerHour(13,20),
                [RoadSpeeding.Sand] = SpeedInfo.FromKilometersPerHour(6,8),

                [RoadSpeeding.CableFerry] = SpeedInfo.Constant(MathUnit.Speed.FromKilometersPerHour(4.5)),

                [RoadSpeeding.CarryBike] = SpeedInfo.Constant(Speed.FromKilometersPerHour(1.6)),
                [RoadSpeeding.Walk] = SpeedInfo.Constant(MathUnit.Speed.FromKilometersPerHour(5.5)),
            };

            return prefs.Complete();
        }

        public static UserRouterPreferences SetUniformSpeeds(this UserRouterPreferences prefs)
        {
            foreach (var mode in Enum.GetValues<RoadSpeeding>())
            {
                prefs.Speeds[mode] = SpeedInfo.Constant( MathUnit.Speed.FromKilometersPerHour(13));
            }

            return prefs.Complete();
        }

    }

}