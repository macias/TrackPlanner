﻿namespace TrackPlanner.Shared
{
    public enum MessageLevel
    {
        Error,
        Warning,
        Info,
        Debug,
    }
}