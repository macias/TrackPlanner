﻿using System;
using System.Runtime.CompilerServices;

namespace TrackPlanner.Shared
{
    public readonly record struct ContextMessage
    {
        public static ContextMessage? Create(string? message,MessageLevel? severity = null,
            [CallerMemberName] string? function = null,
            [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            if (message == null)
                return null;
            else
                return new ContextMessage(message,severity, function, path, line);
        }

        public static ContextMessage Create(Exception exception,MessageLevel? severity = null,
            [CallerMemberName] string? function = null,
            [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            return new ContextMessage(exception.Message, severity?? MessageLevel.Error, function, path, line);
        }

        public string Message { get; init; }
        public string Context { get; init; }
        public MessageLevel Severity { get; init; }


        public ContextMessage(string message,MessageLevel? severity = null,
            [CallerMemberName] string? function = null,
            [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            this.Message = message;
            this.Severity = severity ?? MessageLevel.Warning;
            this.Context = createContext(function, path, line);
        }

        public ContextMessage(string message, ContextMessage inner)
        {
            this.Message = message;
            this.Context = inner.Context;
            this.Severity = inner.Severity;
        }


        private static string createContext(string? function, string? path, int line)
        {
            return $"{function} in {path} at line {line}";
        }

        public override string ToString()
        {
            return Message;
        }
    }

}