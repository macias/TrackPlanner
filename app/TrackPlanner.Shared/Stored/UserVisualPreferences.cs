using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared.Serialization;

namespace TrackPlanner.Shared.Stored
{
    public sealed class UserVisualPreferences
    {
        public static UserVisualPreferences Defaults() => new UserVisualPreferences();
        
        public Dictionary<RoadStyling, LineDecoration> SpeedStyles { get; set; }
        public LineDecoration ForbiddenStyle { get; set; } 

        public UserVisualPreferences()
        {
            ForbiddenStyle = new LineDecoration()
            {
                Label = "FBD",
                AbgrColor = "0xff0051e6",
                Width = 7,
            };
            SpeedStyles = new Dictionary<RoadStyling, LineDecoration>()
            {
                {
                    RoadStyling.CableFerry, new LineDecoration()
                    {
                        Label = "fer",
                        AbgrColor = "0xfffac2a1",
                        Width = 9
                    }
                },
                {
                    RoadStyling.Walk, new LineDecoration()
                    {
                        Label = "wlk",
                        AbgrColor = "0xff2dc0fb",
                        Width = 6
                    }
                },
                {
                    RoadStyling.UrbanSidewalk, new LineDecoration()
                    {
                        Label = "urb",
                        AbgrColor = "0xffd18802",
                        Width = 6
                    }
                },
                {
                    RoadStyling.CarryBike, new LineDecoration()
                    {
                        Label = "crr",
                        AbgrColor = "0xffbdbdbd",
                        Width = 20
                    }
                },
                {
                    RoadStyling.Unknown, new LineDecoration()
                    {
                        Label = "unk",
                        AbgrColor = "0xff2bb4af",
                        Width = 4
                    }
                }, 
                {
                    RoadStyling.Paved, new LineDecoration()
                    {
                        Label = "pvd",
                        AbgrColor = "0xff757575",
                        Width = 5
                    }
                },
                {
                    RoadStyling.Sand, new LineDecoration()
                    {
                        Label = "snd",
                        AbgrColor = "0xff80dafa",
                        Width = 16
                    }
                },
                {
                    RoadStyling.Ground, new LineDecoration()
                    {
                        Label = "gnd",
                        AbgrColor = "0xffa4aabc",
                        Width = 7
                    }
                },
                {
                    RoadStyling.Asphalt, new LineDecoration()
                    {
                        Label = "asp",
                        AbgrColor = "0xff000000",
                        Width = 1
                    }
                },
                {
                    RoadStyling.HardBlocks, new LineDecoration()
                    {
                        Label = "blk",
                        AbgrColor = "0xffb0279c",
                        Width = 11
                    }
                }

            };
        }

        public void Check()
        {
            if (SpeedStyles == null)
                throw new ArgumentNullException(nameof(SpeedStyles));

            string missing = String.Join(", ", Enum.GetValues<RoadStyling>().Where(it => !SpeedStyles.ContainsKey(it)));
            if (missing != "")
                throw new ArgumentOutOfRangeException($"Missing styles for: {missing}.");
        }

        public override string ToString()
        {
            return new ProxySerializer().Serialize(this);
        }
    }
}

