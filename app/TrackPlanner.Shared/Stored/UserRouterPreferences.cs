﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared.Serialization;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.Stored
{
    public sealed class UserRouterPreferences
    {
        public Dictionary<RoadSpeeding, SpeedInfo> Speeds { get; set; }
        public double AddedMotorDangerousTrafficFactor { get; set; }
        public double AddedMotorUncomfortableTrafficFactor { get; set; }

        public double BikeFootHighTrafficScaleFactor { get; set; }
        /// <summary>
        /// if it is negative we add as positive value to the non-cycleway roads
        /// </summary>
        public double AddedCyclewayCostFactor { get; set; }
        // for segments travelled on unofficial/unstable roads (e.g. forest tracks, wild paths, etc)
        public double AddedUnstableRoadCostFactor { get; set; }

        // if the user places out middle point (not start/end) on some highway assume before and after user is comfy with it
        public Length TrafficSuppression { get; set; }
        public TimeSpan JoiningHighTraffic { get; set; }
        public TimeSpan Turning { get; set; }
        public bool HACK_ExactToTarget { get; set; }

        public Angle CompactingAngleDeviation { get; set; }
        public Length CompactingDistanceDeviation { get; set; }
        public bool UseStableRoads { get; set; }
        public TimeSpan CheckpointIntervalLimit { get; set; }
        public Length NamesSearchRange { get; set; }
        public Length GetRouteDirectionsSearchRange_RETIRED() => Length.FromMeters(500);
        
        public Length SnapCyclewaysToRoads { get; set; } // after computing route try to snap cycleways to roads
        public double AddedAuxiliaryRoadCostFactor { get; set; }
        public Length? FastModeLimit { get; set; }
        public double AddedHighVolumeTrafficFactor { get; set; }

        public UserRouterPreferences()
        {
            this.FastModeLimit = null;//Length.FromKilometers(40);
            this.AddedUnstableRoadCostFactor = 2.0;
            SnapCyclewaysToRoads = Length.FromMeters(50);
            this.NamesSearchRange = Length.FromKilometers(2.5);
            this.Speeds = new Dictionary<RoadSpeeding, SpeedInfo>();
            CheckpointIntervalLimit = TimeSpan.FromMinutes(100);
            AddedCyclewayCostFactor = 0.02;
            // we should not go through some crazy shortcuts, like gas stations, hotel parkings, etc.
            // thus a little penalty
            AddedAuxiliaryRoadCostFactor = 0.12;

            CompactingAngleDeviation = Angle.FromDegrees(12);
            CompactingDistanceDeviation = Length.FromMeters(15);
            //AddedRoadSwitchingCostValue = TimeSpan.FromSeconds( 18); // a little something, but still we prefer continuous ride
            Turning = TimeSpan.FromSeconds(10);
        }

        public UserRouterPreferences Complete()
        {
            var prefs = this;

            if (CompactingAngleDeviation < Angle.Zero || CompactingAngleDeviation >= (Angle.PI / 4))
                throw new ArgumentOutOfRangeException($"{nameof(CompactingAngleDeviation)} = {CompactingAngleDeviation}");
            if (CompactingDistanceDeviation < Length.Zero || CompactingDistanceDeviation.Meters >= 100)
                throw new ArgumentOutOfRangeException($"{nameof(CompactingDistanceDeviation)} = {CompactingDistanceDeviation}");
            if (!prefs.Speeds.ContainsKey(RoadSpeeding.Paved))
                prefs.Speeds[RoadSpeeding.Paved] = prefs.Speeds[RoadSpeeding.HardBlocks];
            if (!prefs.Speeds.ContainsKey(RoadSpeeding.UnknownStructured))
                prefs.Speeds[RoadSpeeding.UnknownStructured] = prefs.Speeds[RoadSpeeding.Ground];
            if (!prefs.Speeds.ContainsKey(RoadSpeeding.UnknownLoose))
                prefs.Speeds[RoadSpeeding.UnknownLoose] = prefs.Speeds[RoadSpeeding.Sand];
            if (!prefs.Speeds.ContainsKey(RoadSpeeding.UrbanSidewalk))
                // we expect asphalt or paving stones, so the ride should be good, but on the other hand we cannot go fast because of the people
                prefs.Speeds[RoadSpeeding.UrbanSidewalk] = prefs.Speeds[RoadSpeeding.Ground];

            return this;
        }

        public void CalculateSpeedLimits(out Speed slowest, out Speed fastest)
        {
            if (!Speeds.Values.SelectMany(it => new[] {it.Max, it.Regular})
                    .TryMinMax(out slowest, out fastest))
                throw new ArgumentException("There are no speeds given.");
        }

        public override string ToString()
        {
            return new ProxySerializer().Serialize(this);
        }
 
        // it makes not sense riding at lower speeds than carrying bike
        public Speed GetLowRidingSpeedLimit()
        {
            return this.GetSurfaceSpeed(RoadSpeeding.CarryBike).Regular;
        }
        
        public SpeedInfo GetSurfaceSpeed(RoadSpeeding styling)
        {
            return this.Speeds[styling];
        }
        
    }
}