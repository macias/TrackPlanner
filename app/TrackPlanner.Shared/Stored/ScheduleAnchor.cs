using System;
using System.Threading.Tasks;
using Geo;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Stored
{
    public sealed class ScheduleAnchor : IAnchor
    {
        public bool HasMap { get; set; }
        public GeoPoint UserPoint { get; set; }
        public TimeSpan UserBreak { get; set; }
        public string Label { get; set; }
        public LabelSource LabelSource { get; set; }
        public bool IsPinned { get; set; }
        public bool AllowGap { get; set; }
#if DEBUG
       public bool? DEBUG_Smoothing { get; set; }
        public bool? DEBUG_Enforce { get; set; }
#endif

        public ScheduleAnchor()
        {
            Label = "";
        }
        public ValueTask SetLabelAsync(string label, LabelSource source)
        {
            this.Label = label;
            this.LabelSource = source;
            return ValueTask.CompletedTask;
        }

    }
}