using System;
using TrackPlanner.Shared.Serialization;

namespace TrackPlanner.Shared.Stored
{
    public sealed class GlobalSettings
    {
        public static string SectionName => "GlobalSettings";


        public TimeSpan PopupTimeout { get; set; }
        public UserVisualPreferences VisualPreferences { get; set; } = default!;
        public bool AutoBuild { get; set; }
        public int UndoHistory { get; set; }

        public GlobalSettings()
        {
            // do NOT put any defaults here, ConfigurationBinder is buggy
        }
        
        public static GlobalSettings Defaults()
        {
            return new GlobalSettings()
            {
                PopupTimeout = TimeSpan.FromSeconds(10),
                VisualPreferences =  UserVisualPreferences.Defaults(),
                AutoBuild = false,
                UndoHistory = 50,
            };
        }

        public void Check()
        {
            if (VisualPreferences == null)
                throw new ArgumentNullException(nameof(VisualPreferences));

            VisualPreferences.Check();
        }

        public override string ToString()
        {
            return new ProxySerializer().Serialize(this);
        }
    }
}
