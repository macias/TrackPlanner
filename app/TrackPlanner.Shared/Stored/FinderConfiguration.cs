﻿using System;
using MathUnit;
using TrackPlanner.Shared.Serialization;

namespace TrackPlanner.Shared.Stored
{
    public sealed record class FinderConfiguration
    {
        public MapSettings MapSettings { get; set; }
        public Length InitSnapProximityLimit { get; set; }
        public Length FinalSnapProximityLimit { get; set; }
        public bool EnableDebugDumping { get; set; }
        
        private bool dumpProgress;
        public bool DumpProgress
        {
            get
            {
                if (!this.dumpProgress)
                    return false;
                else if (!this.EnableDebugDumping)
                    throw new Exception("Incorrectly set flags.");
                else
                    return true;
            }
            set { this.dumpProgress = value; }
        }

        public bool CompactPreservesRoads { get; set; }
        public MemorySettings MemoryParams { get; set; }
        public bool DumpLowCost { get; set; }
        public bool DumpTooFar { get; set; }
        public bool DumpInRange { get; set; }
        public bool DumpDangerous { get; set; }
        public bool PreserveRoundabouts { get; set; }
        public bool TwoPassStage { get; set; }
        // lame bi-A-star is faster than bidirectional Dijkstra, computing route across kujawsko-pomorskie
        // bi-Dijkstra: 76 seconds
        // bi-A-star: 63 seconds
        // as for the algorithm: A-star has to use more relaxed rules when expanding over joint
        public bool AStarMode { get; set; }
        public bool DumpRawLegs { get; set; }
        public bool DumpRawRoute { get; set; }

        public FinderConfiguration()
        {
            this.MapSettings = new MapSettings();
            this.AStarMode = true;
            InitSnapProximityLimit = Length.FromMeters(25);
            FinalSnapProximityLimit = Length.FromKilometers(2);
            DumpProgress = false;
            MemoryParams = MemorySettings.Defaults();
#if DEBUG
            MemoryParams.SetEnableOsmId(true);
#endif
        }

        public static FinderConfiguration Defaults()
        {
            return new FinderConfiguration();
        }

        public override string ToString()
        {
            return new ProxySerializer().Serialize(this);
        }
    }
}