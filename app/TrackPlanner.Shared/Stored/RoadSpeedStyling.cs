﻿namespace TrackPlanner.Shared.Stored
{
    public readonly record struct RoadSpeedStyling
    {
        private readonly string? label;
        public string Label
        {
            get { return this.label ?? Mode.ToString(); }
            init { this.label = value; }
        }
        public RoadSpeeding Mode { get; init; }
        public RoadStyling Style { get; init; }

        public static RoadSpeedStyling Asphalt() 
            => new RoadSpeedStyling("Asphalt",RoadSpeeding.Asphalt, RoadStyling.Asphalt);
        public static RoadSpeedStyling Walk() 
            => new RoadSpeedStyling("Walk",RoadSpeeding.Walk, RoadStyling.Walk);
        public static RoadSpeedStyling CableFerry() 
            => new RoadSpeedStyling("Cable ferry",RoadSpeeding.CableFerry, RoadStyling.CableFerry);
        public static RoadSpeedStyling Paved() 
            => new RoadSpeedStyling("Paved",RoadSpeeding.Paved, RoadStyling.Paved);
        public static RoadSpeedStyling UrbanSidewalk() 
            => new RoadSpeedStyling("Sidewalk",RoadSpeeding.UrbanSidewalk, RoadStyling.UrbanSidewalk);
        public static RoadSpeedStyling Sand() 
            => new RoadSpeedStyling("Sand",RoadSpeeding.Sand, RoadStyling.Sand);
        public static RoadSpeedStyling Ground() 
            => new RoadSpeedStyling("Ground",RoadSpeeding.Ground, RoadStyling.Ground);
        public static RoadSpeedStyling HardBlocks() 
            => new RoadSpeedStyling("Hard blocks",RoadSpeeding.HardBlocks, RoadStyling.HardBlocks);
        public static RoadSpeedStyling CarryBike() 
            => new RoadSpeedStyling("Carry bike",RoadSpeeding.CarryBike, RoadStyling.CarryBike);

        public RoadSpeedStyling(string label, RoadSpeeding mode, RoadStyling style)
        {
            this.label = label;
            Mode = mode;
            Style = style;
        }
    }
    
    public enum RoadStyling 
    {
        Asphalt,
        HardBlocks, // concrete plates or cobblestones
        Ground,
        Sand,

        Paved,
        Unknown,
        
        UrbanSidewalk, // it is either "pure" sidewalk (bicycles are forbidden) or it is mixed (pedestrians and cyclists are not separated)
        
        Walk, // walking on unrideable surface (like wood)

        CarryBike, // steps, or walking over some objects

        CableFerry
    }
    public enum RoadSpeeding 
    {
        Asphalt,
        HardBlocks, // concrete plates or cobblestones
        Ground,
        Sand,

        Paved,
        UnknownStructured,
        UnknownLoose,
        
        UrbanSidewalk, // it is either "pure" sidewalk (bicycles are forbidden) or it is mixed (pedestrians and cyclists are not separated)
        
        Walk, // walking on unrideable surface (like wood)

        CarryBike, // steps, or walking over some objects

        CableFerry
    }

}