namespace TrackPlanner.Shared.Stored
{
    public sealed class SystemConfiguration
    {
        public static string SectionName => "SystemConfiguration";

        public string TileServer { get; set; }  = default!;
        public string PlannerServer { get; set; } = default!;
        public bool CalcReal { get; set; }

        public SystemConfiguration()
        {
            // do NOT put any defaults here, ConfigurationBinder is buggy
        }

        public static SystemConfiguration Defaults()
        {
            return new SystemConfiguration()
            {
                TileServer = "http://localhost:8600/tile/",
                PlannerServer = "http://localhost:5200/",//"8700/",
                CalcReal = true,
            };
        }


        public void Check()
        {
        }
    }
}