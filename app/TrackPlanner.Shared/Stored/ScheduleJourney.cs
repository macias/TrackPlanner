using System.Collections.Generic;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Stored
{
    public sealed class ScheduleJourney<TNodeId,TRoadId> 
        : ISchedule<ScheduleDay,ScheduleAnchor,TNodeId,TRoadId>,ISummarySchedule
        where TNodeId:struct
        where TRoadId: struct
    {
        public string Comment { get; set; } = "";
        public RoutePlan<TNodeId,TRoadId> Route { get; set; } = default!;
        IReadOnlyList<IReadOnlyDay> IReadOnlySchedule.Days => this.Days;
        //public int LegsCount() => this.Route.Legs.Count;
        public List<ScheduleDay> Days { get; set; } = default!;
        public ScheduleSettings Settings { get; set; } = default!;
        // todo: remove this outside -- to the controller level
        public UserVisualPreferences VisualPreferences { get; set; } = default!;

        private SummaryJourney? _summary;

        public ScheduleJourney()
        {
            this.Days = new List<ScheduleDay>();
        }

        public SummaryJourney GetSummary()
        {
            this._summary ??= this.CreateSummary();
            return this._summary;
        }
    }
}