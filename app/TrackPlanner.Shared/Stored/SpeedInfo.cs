﻿using MathUnit;

namespace TrackPlanner.Shared.Stored
{
    public readonly record struct SpeedInfo
    {
        public static SpeedInfo Constant(Speed speed)
        {
            return new SpeedInfo() { Regular = speed, Max = speed};
        }

        public static SpeedInfo FromKilometersPerHour(double regular, double max)
        {
            return new SpeedInfo() {Regular = Speed.FromKilometersPerHour(regular), Max = Speed.FromKilometersPerHour(max)};
        }
        
        public Speed Regular { get; init; }
        public Speed Max { get; init; }
        public double AddedCostFactor { get; init; }
    }
}