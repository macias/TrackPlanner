﻿using MathUnit;

namespace TrackPlanner.Shared.Stored
{

    public sealed record class MapSettings
    {
        // if cycleway is closer than given limit to the road we consider the cycleway as high traffic
        public Length HighTrafficProximity { get; set; }
        public Length UrbanFootwayProximity { get; set; }
        public double UrbanFootwayCoverage { get; set; }
        public Length RedundantCyclewayRange { get; set; }

        public MapSettings()
        {
            HighTrafficProximity = Length.FromMeters(20);
            RedundantCyclewayRange = Length.FromMeters(50);
            UrbanFootwayProximity = Length.FromMeters(20);
            UrbanFootwayCoverage = 0.8;
        }
    }
}