﻿#nullable enable

namespace TrackPlanner.Shared.Stored
{
    public enum MapMode
    {
        MemoryOnly = 0,
        TrueDisk = 1,
    }
}