﻿using MathUnit;
using TrackPlanner.Shared.Serialization;

namespace TrackPlanner.Shared.Stored
{
    public sealed class UserTurnerPreferences
    {
        public Angle StraigtLineAngleLimit { get; set; }
        public Length InitSnapProximityLimit { get; set; }
        public Length FinalSnapProximityLimit { get; set; }
        public Angle AltAngleDifferenceLowLimit { get; set; }
        public Angle AltAngleDifferenceHighLimit { get; set; }
        public Angle AltMinorAngleSlack { get; set; }
        public Length CyclewayExitDistanceLimit { get; set; }
        public Angle CyclewayExitAngleLimit { get; set; }
        public Length CyclewayRoadParallelLength { get; set; }
        // desired distance (along the route) from turn point and "arm"/route point
        // purpose: to increase the distance from the turn point to avoid last-minute
        // shape changes, consider
        // *^-------------
        // * is turn point, if we are to close we would say we are going at 45 degrees angle
        // but when we increase distance we are approaching from 90 degrees angle
        public Length TurnArmLength { get; set; }
        public Length MinimalCrossIntersection { get; set; }
        public Angle CrossIntersectionAngleSeparation { get; set; }

        public UserTurnerPreferences()
        {
            StraigtLineAngleLimit = Angle.FromDegrees(150);
            AltAngleDifferenceLowLimit = Angle.FromDegrees(50);
            AltAngleDifferenceHighLimit = Angle.FromDegrees(30);

            InitSnapProximityLimit = Length.FromMeters(25);
            FinalSnapProximityLimit = Length.FromKilometers(10);
            AltMinorAngleSlack = Angle.FromDegrees(10);
            CyclewayExitDistanceLimit = Length.FromMeters(10);
            CyclewayExitAngleLimit = Angle.FromDegrees(165);
            CyclewayRoadParallelLength = Length.FromMeters(30);
            TurnArmLength = Length.FromMeters(20);
            MinimalCrossIntersection = Length.FromMeters(10);
            CrossIntersectionAngleSeparation = Angle.FromDegrees(45); // +  -- if any of the arms are closer, we won't treat it as proper cross intersection (it is too squeezed)
        }

        public static UserTurnerPreferences Defaults()
        {
            return new UserTurnerPreferences();
        }

        public override string ToString()
        {
            return new ProxySerializer().Serialize(this);
        }
    }

   

}