﻿namespace TrackPlanner.Shared 
{
  
    public readonly record struct SinkSettings
    {
        public static SinkSettings CreateEnabled()
        {
            return new SinkSettings()
            {
                ConsoleLevel = MessageLevel.Debug,
                FileLevel = MessageLevel.Debug,
            };
        }
        public static SinkSettings CreateDisabled()
        {
            return new SinkSettings()
            {
                ConsoleEnabled = false,
                FileEnabled = false,
            };
        }

        private readonly MessageLevel consoleLevel;
        public MessageLevel ConsoleLevel
        {
            get { return this.consoleLevel;}
            init
            {
                this.consoleLevel = value;
                if (value > MessageLevel.Error)
                    this.ConsoleEnabled = true;
            }
        }
        public bool ConsoleEnabled { get; init; }
        private  readonly MessageLevel fileLevel;
        public MessageLevel FileLevel
        {
            get { return this.fileLevel; }
            init
            {
                this.fileLevel = value;
                if (value > MessageLevel.Error)
                    this.FileEnabled = true;
            }
        }
        public bool FileEnabled { get; init; }

        public bool IsEnabled => this.ConsoleEnabled || this.FileEnabled;
    }
    
}