﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace TrackPlanner.Shared
{
    public static class Result
    {
        public static Result<T> Valid<T>(T value, ContextMessage? message, MessageLevel? severity = null)
        {
            return Result<T>.Valid(value, message, severity);
        }
        public static Result<T> Valid<T>(T value, string? message = null, MessageLevel? severity = null, [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            return Result<T>.Valid(value, message, severity, name, path, line);
        }

        public static Result<ValueTuple> ValidVoid(string? message = null, MessageLevel? problem = null, [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            return Result<ValueTuple>.Valid(default!, message, problem, name, path, line);
        }

        public static Result<ValueTuple> ValidVoid(ContextMessage? message)
        {
            return Result<ValueTuple>.Valid(default!, message);
        }

        public static Result<TResult> Fail<TResult>(string? message, MessageLevel? problem = null, [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            return Fail<TResult>(ContextMessage.Create( message, problem, name, path, line));
        }

        public static Result<TResult> Fail<TResult>(ContextMessage? message)
        {
            return Result<TResult>.Fail(message);
        }


        public static Result<ValueTuple> FailVoid(string? message, MessageLevel? problem = null, [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            return FailVoid(ContextMessage.Create( message, problem, name, path, line));
        }
        public static Result<ValueTuple> FailVoid(ContextMessage? message)
        {
            return Fail<ValueTuple>(message);
        }
    }

    [Serializable]
    public readonly record struct Result<TResult> : ISerializable
    {
        public static Result<TResult> Valid(TResult value, ContextMessage? message ,
            MessageLevel? severity = null)
        {
            Result<TResult, ValueTuple> inner = Result<TResult, ValueTuple>.Valid(value, message, severity);
            return new Result<TResult>(inner);
        }
        public static Result<TResult> Valid(TResult value, string? message = null, 
            MessageLevel? severity = null, [CallerMemberName] string? name = null,
            [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            Result<TResult, ValueTuple> inner = Result<TResult, ValueTuple>.Valid(value, message, 
                severity, name, path, line);
            return new Result<TResult>(inner);
        }

        public static Result<TResult> Fail(string? message, MessageLevel? severity = null, 
            [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            if (message == null)
                throw new ArgumentException("No reason for failure given");
            Result<TResult, ValueTuple> inner = Result<TResult, ValueTuple>.Fail(default, message, severity, name, path, line);
            return new Result<TResult>(inner);
        }
        public static Result<TResult> Fail(ContextMessage? message, MessageLevel? severity = null)
        {
            if (message == null)
                throw new ArgumentException("No reason for failure given");
            Result<TResult, ValueTuple> inner = Result<TResult, ValueTuple>.Fail(default, message, severity);
            return new Result<TResult>(inner);
        }

        private readonly Result<TResult, ValueTuple> inner;
        public TResult Value => this.inner.Value;
        public TResult? ValueOrNull => this.inner.ValueOrNull;
        public ContextMessage? ProblemMessage => this.inner.ProblemMessage;
        public bool HasValue => this.inner.HasValue;

        private Result(Result<TResult, ValueTuple> inner)
        {
            this.inner = inner;
        }
        
        public Result(SerializationInfo info, StreamingContext context)
        {
            this.inner = new Result<TResult, ValueTuple>(info, context);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            this.inner.GetObjectData(info,context);
        }

        

        public Result<TResult> WithMessage(ContextMessage? message)
        {
            return new Result<TResult>(this.inner.WithMessage(message,force:false));
        }
        
        public Result<TResult> WithMessage<TR>(Result<TR> other,bool force)
        {
            return new Result<TResult>(this.inner.WithMessage(other,force));
        }

        public Result<TR> FailAs<TR>()
        {
            return new Result<TR>(this.inner.FailAs<TR>());
        }
        public Result<TR,TF> FailAs<TR,TF>(TF failure)
        {
            return this.inner.FailWith<TR,TF>(failure);
        }
    }

    [Serializable]
    public readonly record struct Result<TResult, TFailure> : ISerializable
    {
        public static Result<TResult, TFailure> Valid(TResult value, ContextMessage? message, 
            MessageLevel? severity = null)
        {
            return new Result<TResult, TFailure>(value, default!, 
                message, hasValue: true);
        }
        public static Result<TResult, TFailure> Valid(TResult value, string? message = null, 
            MessageLevel? severity = null,
            [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, 
            [CallerLineNumber] int line = 0)
        {
            return new Result<TResult, TFailure>(value, default!, 
                ContextMessage.Create( message,severity ?? MessageLevel.Warning, name, path, line),
                hasValue: true);
        }

        public static Result<TResult, TFailure> Fail(TFailure failure, ContextMessage? message,
            MessageLevel? problem = null)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            
            return new Result<TResult, TFailure>(default!, failure, message,
                hasValue: false);
        }
        public static Result<TResult, TFailure> Fail(TFailure failure, string message,
            MessageLevel? problem = null,
            [CallerMemberName] string? name = null,
            [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            if (message == null) 
                throw new ArgumentNullException(nameof(message));
            
            return new Result<TResult, TFailure>(default!, failure,
                ContextMessage.Create( message,problem ?? MessageLevel.Error, name, path, line),
                hasValue: false);
        }
        public static Result<TResult, TFailure> Fail(TFailure failure, 
            MessageLevel? problem = null,
            [CallerMemberName] string? name = null, [CallerFilePath] string? path = null, [CallerLineNumber] int line = 0)
        {
            return new Result<TResult, TFailure>(default!, failure,
                ContextMessage.Create( $"{failure}", problem ?? MessageLevel.Error,name, path, line),
                hasValue: false);
        }

        private readonly TResult value;
        private readonly TFailure failure;
        public TResult Value => HasValue ? value : throw new InvalidOperationException($"Result has no value: {ProblemMessage}");
        public TFailure Failure => HasValue ? throw new InvalidOperationException($"Result has value") : failure;
        public TResult? ValueOrNull => HasValue ? value : default(TResult);
        public ContextMessage? ProblemMessage { get; private init; }
        public bool HasValue { get; }

        private Result(TResult value, TFailure failure, ContextMessage? problemMessage,
            bool hasValue)
        {
            this.value = value;
            this.failure = failure;
            this.ProblemMessage = problemMessage;
            HasValue = hasValue;
        }

        public Result(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            this.HasValue = info.GetBoolean(nameof(HasValue));
            if (HasValue)
            {
                this.value = (TResult) info.GetValue(nameof(Value), typeof(TResult))!;
                this.failure = default!;
            }
            else
            {
                this.failure = (TFailure) info.GetValue(nameof(Failure), typeof(TFailure))!;
                this.value = default!;
            }

            this.ProblemMessage = (ContextMessage?) info.GetValue(nameof(ProblemMessage), typeof(ContextMessage?));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));

            info.AddValue(nameof(HasValue), HasValue);
            if (HasValue)
                info.AddValue(nameof(this.Value), value);
            else
                info.AddValue(nameof(this.Failure), failure);
            info.AddValue(nameof(ProblemMessage), ProblemMessage);
        }

        public Result<TResult> FailTrim()
        {
            // dropping Failure type, keep only message

            if (HasValue)
                return Result<TResult>.Valid(this.Value, this.ProblemMessage);
            else
                return FailWith<TResult>();
        }

        public Result<TResult, TFailure> WithMessage(ContextMessage? message, 
            bool force)
        {
            if (!HasValue)
                throw new ArgumentException("Cannot alter messages for failure");

            if (this.ProblemMessage == null || force)
                return this with
                {
                    ProblemMessage = message,
                };
            else
                return this;
        }

        public Result<TResult, TFailure> WithMessage<TR>(Result<TR> other,bool force)
        {
            return WithMessage(other.ProblemMessage, force);
        }

        public Result<TResult, TFailure> WithMessage<TR, TF>(Result<TR, TF> innerResult)
        {
            return WithMessage(innerResult.ProblemMessage, force:false);
        }

        public Result<TR, TFailure> FailAs<TR>()
        {
            return this.FailWith<TR, TFailure>(this.failure);
        }
        public Result<TR, TF> FailWith<TR,TF>(TF failure)
        {
            if (HasValue)
                throw new ArgumentException("Cannot retype sucessful result");

            return new Result<TR, TF>(default!, failure, this.ProblemMessage, this.HasValue);
        }
        public Result<TR> FailWith<TR>()
        {
            if (HasValue)
                throw new ArgumentException("Cannot retype sucessful result");

            return Result<TR>.Fail(this.ProblemMessage);
        }
        
    }
}