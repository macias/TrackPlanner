﻿using MathUnit;
using Newtonsoft.Json;
using System;
using System.Globalization;

namespace TrackPlanner.Shared.Serialization
{
    public class NullableAngleNewtonConverter : JsonConverter<Angle?>
    {
        public override Angle? ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, Angle? existingValue, 
            bool hasExistingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            var text = serializer.Deserialize<string>(reader);
            if (text == null)
                return null;

            var angle = AngleNewtonConverter.GetAngle(text);
            return angle;
        }

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, Angle? value, 
            Newtonsoft.Json.JsonSerializer serializer)
        {
            if (value == null)
                writer.WriteNull();
            else
            {
                var angle = AngleNewtonConverter.PutAngle( value.Value);
                writer.WriteValue(angle);
            }
        }

    }
}
