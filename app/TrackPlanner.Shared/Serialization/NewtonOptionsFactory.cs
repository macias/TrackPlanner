﻿using Newtonsoft.Json;

namespace TrackPlanner.Shared.Serialization
{
    public static class NewtonOptionsFactory
    {
        public static void CustomizeJsonOptions(JsonSerializerSettings options)
        {
            // serializer is buggy, you cannot use both value converter and at the same time
            // nullable value converter, thus we register only the latter and luckily it works
            // https://github.com/JamesNK/Newtonsoft.Json/issues/2219
            
            //  options.Converters.Add(new DictionaryNewtonConverter<SpeedMode,Speed>());
            //options.Converters.Add(new AngleNewtonConverter());
            options.Converters.Add(new NullableAngleNewtonConverter());
            options.Converters.Add(new SlopeNewtonConverter());
            options.Converters.Add(new SpeedNewtonConverter());
            //  options.Converters.Add(new LengthNewtonConverter());
            options.Converters.Add(new NullableLengthNewtonConverter());
            // added to convert enum as strings, not as ints
            options.Converters.Add (new Newtonsoft.Json.Converters.StringEnumConverter());
        }
        
        public static JsonSerializerSettings BuildJsonOptions(bool compact)
        {
            var options = new Newtonsoft.Json.JsonSerializerSettings
            {
                Formatting = compact? Formatting.None: Formatting.Indented,
            };

            CustomizeJsonOptions(options);

            return options;
        }
    }
}
