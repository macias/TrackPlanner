﻿using MathUnit;
using Newtonsoft.Json;
using System;
using System.Globalization;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Shared.Serialization
{
    public class SlopeNewtonConverter : JsonConverter<Slope>
    {
        public override Slope ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType,
            Slope existingValue, bool hasExistingValue,
            Newtonsoft.Json.JsonSerializer serializer)
        {
            var text = serializer.Deserialize<string>(reader);
            if (text == null)
                return default;
            else
                return GetSlope(text);
        }

        internal static Slope GetSlope(string text)
        {
            return Slope.FromPercents(float.Parse(text, CultureInfo.InvariantCulture));
        }

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, Slope value, 
            Newtonsoft.Json.JsonSerializer serializer)
        {
            writer.WriteValue(PutSlope(value));
        }

        internal static string PutSlope(Slope value)
        {
            return value.Percents.ToString(CultureInfo.InvariantCulture);
        }
    }
}
