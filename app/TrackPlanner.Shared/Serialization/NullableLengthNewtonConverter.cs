﻿using MathUnit;
using Newtonsoft.Json;
using System;
using System.Globalization;

namespace TrackPlanner.Shared.Serialization
{
    public class NullableLengthNewtonConverter : JsonConverter<Length?>
    {
        public override Length? ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, Length? existingValue, bool hasExistingValue,
            Newtonsoft.Json.JsonSerializer serializer)
        {
            var text = serializer.Deserialize<string>(reader);
            if (text == null)
                return null;

            var length = LengthNewtonConverter.GetLength(text);
            return length;
        }

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, Length? value, Newtonsoft.Json.JsonSerializer serializer)
        {
            if (value == null)
                writer.WriteNull();
            else
            {
                var length = LengthNewtonConverter.PutLength( value.Value);
                writer.WriteValue(length);
            }
        }

    }
}
