﻿namespace TrackPlanner.Shared
{
    public sealed record class LoggerSettings
    {
        public string? LogPath { get; set; }
        public SinkSettings Sink { get; set; }

        public LoggerSettings()
        {
            this.Sink = SinkSettings.CreateEnabled();
        }
    }
}