﻿namespace TrackPlanner.Shared.RestSymbols
{
    public static class Methods
    {
        public const string Put_ComputeTrack = "plan-route";
        public const string Put_ComputeAttractions = "calc-attractions";
        public const string Get_GetLayer = "get-layer";
        public const string Put_FindPeaks = "find-peaks";
        public const string Post_SaveSchedule = "save-schedule";
        public const string Get_About = "about";
        public const string Get_LoadSchedule = "load-schedule";
        public const string Get_ScheduleDirectory = "get-schedule-dir";
        public const string Post_SaveDebug = "save-debug";
        public const string Get_LayersDirectory = "get-layers-dir";
    }
}
