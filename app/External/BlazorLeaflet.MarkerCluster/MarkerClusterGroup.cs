using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlazorLeaflet.Models;
using Microsoft.JSInterop;

namespace BlazorLeaflet.MarkerCluster
{
  
    public sealed class MarkerClusterGroup : IAsyncDisposable,ILayer
    {
        private readonly IJSRuntime jsRuntime;

        public static async ValueTask<MarkerClusterGroup> CreateAsync(IJSRuntime jsRuntime)
        {
            var js_ref = await jsRuntime.InvokeAsync<IJSObjectReference>($"{MarkerClusterInterop.BaseObjectContainer}.createGroup");
            return new MarkerClusterGroup(jsRuntime, js_ref);
        }

        public IJSObjectReference JsRef { get; }

        private readonly List<Marker> markers;

        private MarkerClusterGroup(IJSRuntime jsRuntime,IJSObjectReference jsRef)
        {
            this.jsRuntime = jsRuntime;
            this.JsRef = jsRef;
            this.markers = new List<Marker>();
        }

        public async ValueTask DisposeAsync()
        {
            await ClearAsync();
            await this.JsRef.DisposeAsync();
        }

        public async ValueTask<bool> RemoveLayerAsync(Marker marker)
        {
            if (!this.markers.Remove(marker))
                return false;
            
            await jsRuntime.InvokeVoidAsync($"{MarkerClusterInterop.BaseObjectContainer}.removeLayer", 
                this.JsRef, marker.JsRef);

            return true;

        }
        public async ValueTask ClearAsync()
        {
            while (markers.Count>0)
            {
                var marker = this.markers[^1];
                await RemoveLayerAsync(marker);
                await marker.DisposeAsync();
            }

            this.markers.Clear();
        }

        public async ValueTask AddLayerAsync( Marker marker)
        {
            if (this.JsRef == null)
            {
                throw new ArgumentNullException();
            }

            if (marker.JsRef == null)
                await marker.RegisterAsync(jsRuntime);

            await jsRuntime.InvokeVoidAsync($"{MarkerClusterInterop.BaseObjectContainer}.addLayer", 
                this.JsRef, marker.JsRef);
            this.markers.Add(marker);
        }

        public T? FindMarkerOrDefault<T>(Func<T, bool> predicate)
        where T : Marker
        {
            foreach (var marker in this.markers)
                if (marker is T m && predicate(m))
                    return m;

            return default(T);
        }
        public bool Contains(Marker marker)
        {
            return this.markers.Contains(marker);
        }

    }
}