﻿using System.Collections.Generic;
using OsmSharp;
using OsmSharp.Streams;
using System.Diagnostics;
using System.IO;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public sealed class OsmCollector
    {
        private readonly ILogger logger;
        private readonly HashSet<(OsmExtractor.Source,string)> unused;
        private CompactDictionaryShift<OsmId, GeoZPoint> nodes = default!;
        private CompactDictionaryShift<OsmId, OsmId> ways = default!;

        public IEnumerable<string> Unused => this.unused.Select(it => $"{it.Item1}: {it.Item2}");

        public OsmCollector(ILogger logger)
        {
            this.logger = logger;
            this.unused = new HashSet<(OsmExtractor.Source, string)>();

            clear();
        }

        private void clear()
        {
            this.nodes = new CompactDictionaryShift<OsmId, GeoZPoint>();
            this.ways = new CompactDictionaryShift<OsmId, OsmId>();
        }
        
        public List<(PointOfInterest attraction, MapZPoint<OsmId,OsmId> location)> ReadOsm(string filePath)
        {
            clear();
            var extractor = new OsmExtractor(this.logger, this.nodes, x => this.ways[x]);
            
            double start = Stopwatch.GetTimestamp();
            {
                using (var stream = new MemoryStream(System.IO.File.ReadAllBytes(filePath)))
                {
                    using (var source = new PBFOsmStreamSource(stream))
                    {
                        foreach (OsmGeo element in source)
                        {
                            if (!element.Id.HasValue)
                            {
                                this.logger.Warning($"No id for {element}");
                                continue;
                            }
                            
                            if (element is Way way)
                            {
                                ways.Add(OsmId.PureOsm(way.Id!.Value), OsmId.PureOsm(way.Nodes.First()));
                            }
                            else if (element is Node node)
                            {
                                OsmId node_id = OsmId.PureOsm( node.Id!.Value);

                                if (node.Latitude.HasValue && node.Longitude.HasValue)
                                {
                                    nodes.Add(node_id,  GeoZPoint.FromDegreesMeters(latitude: node.Latitude.Value, 
                                        longitude: node.Longitude.Value,
                                        altitude:null));
                                }
                            }
                            
                            extractor.Extract(element);
                        }
                    }
                }
            }

            this.unused.AddRange(extractor.Unused);
            
            return extractor.GetAttractions();
        }
    }
}