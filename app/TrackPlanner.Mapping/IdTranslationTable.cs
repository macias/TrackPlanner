﻿namespace TrackPlanner.Mapping;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;


public static class OsmIdExtension 
{
    public static void Write(this OsmId index, BinaryWriter writer)
    {
        writer.Write(index.Identifier);
        writer.Write(index.Phantom);
    }
    public static OsmId Read(BinaryReader reader)
    {
        var id = reader.ReadInt64();
        var phantom = reader.ReadByte();

        return new OsmId(id,phantom);
    }


}
// translations between OSM identifiers and grid-based indices
internal sealed class IdTranslationTable
{
    private sealed class SubListComparer : IEqualityComparer<List<OsmId>>
    {
        public static SubListComparer Instance { get; } = new SubListComparer();

        private SubListComparer()
        {
        }

        public bool Equals(List<OsmId>? x, List<OsmId>? y)
        {
            if (x == y)
                return true;
            if ((x == null) != (y == null))
                return false;

            return Enumerable.SequenceEqual(x!, y!);
        }

        public int GetHashCode(List<OsmId> obj)
        {
            return obj.Aggregate(0, (acc, val) => acc ^ val.GetHashCode());
        }
    }

    private readonly IMap<OsmId, WorldIdentifier> osmToGrid;
    private readonly IMap<CellIndex, List<OsmId>> gridToOsm; 

    public int Count => this.osmToGrid.Count;

    public IdTranslationTable(int capacity)
    {
        this.osmToGrid = HashMap.Create<OsmId, WorldIdentifier>(capacity);
        this.gridToOsm = HashMap.Create<CellIndex, List<OsmId>>();
    }

    private IdTranslationTable(IMap<OsmId, WorldIdentifier> osmToGrid,
        IMap<CellIndex, List<OsmId>> gridToOsm)
    {
        this.osmToGrid = osmToGrid;
        this.gridToOsm = gridToOsm;
    }

    public WorldIdentifier AddOrGet(OsmId osmId, CellIndex cellIndex)
    {
        if (this.osmToGrid.TryGetValue(osmId, out var world_id))
        {
            if (world_id.CellIndex != cellIndex)
                throw new InvalidDataException($"OSM id {osmId} has already translation with different cell index.");
            return world_id;
        }

        var sub_map = new List<OsmId>();
        if (!this.gridToOsm.TryAdd(cellIndex, sub_map, out var existing))
            sub_map = existing;
        world_id = new WorldIdentifier(cellIndex, sub_map.Count);
        sub_map.Add(osmId);
            this.osmToGrid.Add(osmId, world_id);
        return world_id;
    }

    public WorldIdentifier Get(OsmId osmId)
    {
        return this.osmToGrid[osmId];
    }

    public bool TryGetFromOsm(OsmId osmId, out WorldIdentifier id)
    {
        return this.osmToGrid.TryGetValue(osmId, out id);
    }

    public OsmId GetOsm(WorldIdentifier worldId)
    {
        return this.gridToOsm[worldId.CellIndex][worldId.EntityIndex];
    }


    public void Write(BinaryWriter writer)
    {
        DiskHelper.WriteMap(writer, this.gridToOsm,
            writeKey: (k) => k.Write(writer),
            writeValue: (v) => DiskHelper.WriteList(writer, v, id => id.Write(writer)));
    }

    public static IdTranslationTable Read(BinaryReader reader)
    {
        IMap<CellIndex, List<OsmId>> grid_to_osm =
            DiskHelper.ReadMap(reader, HashMap.Create<CellIndex, List<OsmId>>,
                CellIndexExtension.Read,
                r => DiskHelper.ReadList(r, subr => OsmIdExtension.Read(subr)),
                SubListComparer.Instance);

        IMap<OsmId, WorldIdentifier> osm_to_grid = HashMap.Create<OsmId, WorldIdentifier>(grid_to_osm
            .Sum(it => it.Value.Count));

        foreach (var (cell_idx, sub_list) in grid_to_osm)
            for (int entity_idx = 0; entity_idx < sub_list.Count; ++entity_idx)
            {
                OsmId osm_id = sub_list[entity_idx];
                    osm_to_grid.Add(osm_id, new WorldIdentifier(cell_idx, entity_idx));
            }

        return new IdTranslationTable(osm_to_grid, grid_to_osm);
    }
}