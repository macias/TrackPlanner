﻿using System;
using TrackPlanner.Mapping.Disk;

namespace TrackPlanner.Mapping
{
    internal static class HACK
    {
       
        public static IWorldMap<TNodeId,TRoadId> CastMap<TNodeId, TRoadId>(WorldMapMemory map) 
            where TNodeId : struct, IEquatable<TNodeId> 
            where TRoadId : struct, IEquatable<TRoadId>
        {
            if (map is IWorldMap<TNodeId, TRoadId> dest_map)
                return dest_map;
            else
            {
                throw new NotSupportedException();
            }
        }
        public static IWorldMap<TNodeId,TRoadId> CastMap<TNodeId, TRoadId>(TrueGridWorldMap map) 
            where TNodeId : struct, IEquatable<TNodeId> 
            where TRoadId : struct, IEquatable<TRoadId>
        {
            if (map is IWorldMap<TNodeId, TRoadId> dest_map)
                return dest_map;
            else
            {
                throw new NotSupportedException();
            }
        }

    }
}