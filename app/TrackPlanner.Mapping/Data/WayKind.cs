﻿namespace TrackPlanner.Mapping.Data
{
    public enum WayKind : byte
    {
        Highway = 0, // AKA motorway
        HighwayLink = 1,
        Trunk = 2,
        TrunkLink = 3,
        Primary = 4,
        PrimaryLink = 5,
        Secondary = 6,
        SecondaryLink = 7,
        Tertiary = 8,
        TertiaryLink = 9,
        Cycleway = 10,

        /// <summary>
        /// if surface is not given assume both paved and unpaved
        /// </summary>
        Auxiliary = 11,  // for example service roads
        Unclassified = 12,
        Footway = 13,
        Steps = 14,
        Ferry = 15,

        /// <summary>
        /// if surface is not given assume unpaved
        /// </summary>
        Path = 16,
        Crossing = 17,
    }

    public static class WayKindExtension
    {
        public static int IndexOf(this WayKind kind)
        {
            return (int) kind;
        }

        private static WayKind trimLink(WayKind kind)
        {
            return kind switch
            {
                WayKind.HighwayLink => WayKind.Highway,
                WayKind.TrunkLink => WayKind.Trunk,
                WayKind.PrimaryLink => WayKind.Primary,
                WayKind.SecondaryLink => WayKind.Secondary,
                WayKind.TertiaryLink => WayKind.Tertiary,
                _ => kind
            };
        }

        public static bool IsSignificantlyMoreImportant(this WayKind kind,WayKind other)
        {
            return kind <= WayKind.TertiaryLink && trimLink(other) > trimLink(kind);
        }

        public static bool IsStable(this WayKind kind)
        {
            return kind != WayKind.Path;
        }


        public static bool IsSerious(this WayKind kind)
        {
            return kind < WayKind.Cycleway;
        }
        
        public static bool IsWinding(this WayKind kind)
        {
            // those types have complex shapes which leads to too many turn notifications
            return kind == WayKind.Cycleway || kind== WayKind.Footway;
        }
    }
}