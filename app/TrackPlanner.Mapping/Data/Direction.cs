﻿namespace TrackPlanner.Mapping.Data
{
    public enum Direction : sbyte
    {
        Backward = -1,
        Forward = +1,
    }

    public static class DirectionExtension
    {
        public static int AsChange(this Direction direction)
        {
            return (int)direction;
        }
    }
  
}