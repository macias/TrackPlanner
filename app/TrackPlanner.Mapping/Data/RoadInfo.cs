﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using MathUnit;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Mapping.Data
{

    [StructLayout(LayoutKind.Auto)]
    public readonly record struct RoadInfo<TNodeId> : IEquatable<RoadInfo<TNodeId>>
    {
        [Flags]
        private enum RoadFeatures : ushort
        {
            None = 0,

            OneWay = 1 << 0,
            BikeLine = 1 << 1,
            UrbanSidewalk = 1 << 2,
            Roundabout = 1 << 3,
            Dismount = 1 << 4,
            HasAccess = 1 << 5,
            Singletrack = 1 << 6,
            SpeedLimit50 = 1 << 7,
            RoundaboutStar = 1 << 8, // phantom road, roundabout star
        }

        private readonly RoadFeatures features;
        private readonly byte kind_grade;
        public WayKind Kind => (WayKind) (this.kind_grade & 0x1f);
        public RoadGrade Grade => (RoadGrade) (this.kind_grade >> 5);
        private readonly byte surface_smoothness;
        public RoadSurface Surface => (RoadSurface) (this.surface_smoothness >> 4);
        public RoadSmoothness Smoothness => (RoadSmoothness) (this.surface_smoothness & 0x0f);
        public IReadOnlyList<TNodeId> Nodes { get; init; }
        public TrafficInfo Traffic { get; }

        public bool BikeLane => this.features.HasFlag(RoadFeatures.BikeLine);
        public bool OneWay => this.features.HasFlag(RoadFeatures.OneWay);
        public bool HasUrbanSidewalk => this.features.HasFlag(RoadFeatures.UrbanSidewalk);
        public bool IsRoundabout => this.features.HasFlag(RoadFeatures.Roundabout);
        public bool IsSingletrack => this.features.HasFlag(RoadFeatures.Singletrack);
        public bool Dismount => this.features.HasFlag(RoadFeatures.Dismount);
        public bool HasAccess => this.features.HasFlag(RoadFeatures.HasAccess);

        public bool HasSpeedLimit50 => this.Traffic.SpeedLimit <= TrafficInfo.SafeSpeed; // this.features.HasFlag(RoadFeatures.SpeedLimit50);
        // public bool HasRedundantFlag => this.features.HasFlag(RoadFeatures.IsRedundant);


        public string? RoadName { get; }

        public sbyte Layer { get; }

        public bool IsExpressTraffic => this.Kind <= WayKind.PrimaryLink;
        public bool IsSignificantTraffic => !this.IsExpressTraffic && this.Kind <= WayKind.SecondaryLink;

        public bool IsDangerous => this.IsExpressTraffic && !this.HasSpeedLimit50;
        public bool IsUncomfortable => this.IsSignificantTraffic && !this.HasSpeedLimit50;

        public bool HasName => this.RoadName != null;

        public int SpeedLimitLevel => Math.Max(0, (int) (((this.SpeedLimit?.KilometersPerHour ?? TrafficInfo.AssumedSpeedLimitKph) - TrafficInfo.SafeSpeedKph) / 10));
        public int VolumeTrafficLevel => Math.Max(1, this.Traffic.LaneCount - (OneWay ? 0 : 1));
        public Speed? SpeedLimit => this.Traffic.SpeedLimit;
        public int LaneCount => this.Traffic.LaneCount;

        private RoadInfo(WayKind kind, string? name, RoadFeatures roadFeatures, RoadSurface surface,
            RoadSmoothness smoothness, RoadGrade grade, sbyte layer, TrafficInfo traffic,
            IReadOnlyList<TNodeId> nodes)
        {
            this.features = roadFeatures;
            this.Traffic = traffic;
            this.RoadName = name == "" ? null : name;
            this.kind_grade = (byte) ((((byte) (grade)) << 5) | ((byte) kind));
            this.surface_smoothness = (byte) ((((byte) (surface)) << 4) | ((byte) smoothness));
            Nodes = nodes ?? throw new ArgumentNullException(nameof(nodes));
            Layer = layer;
        }

        public RoadInfo(WayKind kind, string? name, bool oneWay, bool roundabout, RoadSurface surface,
            RoadSmoothness smoothness, RoadGrade grade, bool hasAccess, bool speedLimit50,
            Speed? speedLimit,
            bool hasBikeLane, bool isSingletrack,
            bool urbanSidewalk, bool dismount,
            // bool isRedundant,
            sbyte layer,
            int laneCount,
            IReadOnlyList<TNodeId> nodes)

            : this(kind, name, (oneWay ? RoadFeatures.OneWay : RoadFeatures.None)
                               | (hasBikeLane ? RoadFeatures.BikeLine : RoadFeatures.None)
                               | (speedLimit50 ? RoadFeatures.SpeedLimit50 : RoadFeatures.None)
                               | (roundabout ? RoadFeatures.Roundabout : RoadFeatures.None)
                               | (dismount ? RoadFeatures.Dismount : RoadFeatures.None)
                               | (hasAccess ? RoadFeatures.HasAccess : RoadFeatures.None)
                               | (isSingletrack ? RoadFeatures.Singletrack : RoadFeatures.None)
                               | (urbanSidewalk ? RoadFeatures.UrbanSidewalk : RoadFeatures.None)
                //| (isRedundant ? RoadFeatures.IsRedundant: RoadFeatures.None)
                ,
                surface, smoothness, grade, layer,
                new TrafficInfo(speedLimit, laneCount),
                nodes)
        {
        }

        internal string GetMapDataDetails()
        {
            var name = this.RoadName
                ?.Replace(MapDataInfo.SectionSeparator, MapDataInfo.NonSeparator)
                ?.Replace(MapDataInfo.ValueSeparator, MapDataInfo.NonSeparator);

            return String.Join($"{MapDataInfo.SectionSeparator} ",
                $"{Layer}", $"{Kind}", $"{Surface}", $"{Smoothness}", $"{this.features}",
                $"{name}", $"{Grade}",
                $"{this.Traffic.GetMapDataDetails()}");
        }

        internal static RoadInfo<TNodeId> Parse(IReadOnlyList<TNodeId> nodes, string details)
        {
            var parts = details.Split(MapDataInfo.SectionSeparator).Select(it => it.Trim()).ToArray();

            int index = 0;
            var layer = sbyte.Parse(parts[index++]);
            var kind = Enum.Parse<WayKind>(parts[index++]);
            var surface = Enum.Parse<RoadSurface>(parts[index++]);
            var smoothness = Enum.Parse<RoadSmoothness>(parts[index++]);
            var features = Enum.Parse<RoadFeatures>(parts[index++]);
            var road_name = parts[index++];
            RoadGrade grade = RoadGrade.Grade1;
            if (index < parts.Length)
                grade = Enum.Parse<RoadGrade>(parts[index++]);
            var traffic_info = TrafficInfo.Parse(parts, ref index,
                features.HasFlag(RoadFeatures.SpeedLimit50) ? 50 : TrafficInfo.AssumedSpeedLimitKph);

            return new RoadInfo<TNodeId>(kind, road_name, features, surface, smoothness, grade, layer,
                traffic_info,
                nodes);
        }

        internal void Write(BinaryWriter writer)
        {
            writer.Write(Layer);
            writer.Write((byte) Kind);
            writer.Write((byte) Surface);
            writer.Write((byte) Smoothness);
            writer.Write((byte) Grade);
            writer.Write(this.RoadName ?? "");
            // squash any name identifier as invalid, because we don't store road names (so far)
            writer.Write((ushort) features);
            this.Traffic.Write(writer);

            writer.Write(Nodes.Count);
            foreach (var node_id in Nodes)
                DiskHelper.WriteId(writer, node_id);
        }

        private static RoadInfo<TNodeId> Read(BinaryReader reader, bool probing, out int nodesCount)
        {
            var layer = reader.ReadSByte();
            var kind = (WayKind) reader.ReadByte();
            var surface = (RoadSurface) reader.ReadByte();
            var smoothness = (RoadSmoothness) reader.ReadByte();
            var grade = (RoadGrade) reader.ReadByte();
            var name = reader.ReadString();
            var features = (RoadFeatures) reader.ReadUInt16();
            var speed_lanes = TrafficInfo.Read(reader);

            nodesCount = reader.ReadInt32();

            if (probing)
            {
                for (int i = 0; i < nodesCount; ++i)
                    reader.ReadInt64();

                return default;
            }
            else
            {
                var nodes = new TNodeId[nodesCount];
                for (int i = 0; i < nodesCount; ++i)
                    nodes[i] = DiskHelper.ReadId<TNodeId>(reader);

                return new RoadInfo<TNodeId>(kind, name, features, surface, smoothness, grade, layer,
                    speed_lanes, nodes);
            }
        }

        internal static RoadInfo<TNodeId> Read(BinaryReader reader)
        {
            return Read(reader, probing: false, out _);
        }

        internal RoadInfo<TNodeId> BuildWithDenyAccess()
        {
            var feat = this.features;
            if (HasAccess)
                feat ^= RoadFeatures.HasAccess;
            return new RoadInfo<TNodeId>(Kind, RoadName, feat, Surface, Smoothness, Grade, Layer,
                this.Traffic, Nodes);
        }

        internal RoadInfo<TNodeId> BuildWithUrbanSidewalkFlag()
        {
            var feat = this.features;
            if (!HasUrbanSidewalk)
                feat ^= RoadFeatures.UrbanSidewalk;
            return new RoadInfo<TNodeId>(Kind, RoadName, feat, Surface, Smoothness, Grade, Layer,
                this.Traffic, Nodes);
        }

        internal RoadInfo<TNodeId> BuildWithSpeedLimit(Speed limit)
        {
            var feat = this.features;
            if (!HasSpeedLimit50)
                feat ^= RoadFeatures.SpeedLimit50;
            return new RoadInfo<TNodeId>(Kind, RoadName, feat, Surface, Smoothness, Grade, Layer,
                new TrafficInfo(limit, this.Traffic.LaneCount),
                Nodes);
        }

        public bool Equals(RoadInfo<TNodeId> other)
        {
            return Kind == other.Kind &&
                   features == other.features &&
                   Surface == other.Surface &&
                   Smoothness == other.Smoothness &&
                   Grade == other.Grade &&
                   this.Traffic == other.Traffic &&
                   RoadName == other.RoadName &&
                   Layer == other.Layer
                   && Enumerable.SequenceEqual(Nodes, other.Nodes);
        }

        public override int GetHashCode()
        {
            HashCode hash = new HashCode();
            hash.Add(Kind);
            hash.Add(features);
            hash.Add(Surface);
            hash.Add(Smoothness);
            hash.Add(RoadName);
            hash.Add(this.Traffic);
            hash.Add(Grade);
            hash.Add(Layer);
            foreach (var n in Nodes)
                hash.Add(n);
            return hash.ToHashCode();
        }

        public override string ToString()
        {
            return $"{nameof(Kind)}: {Kind}, {nameof(features)}: {this.features}, {nameof(Surface)}: {Surface},{nameof(Smoothness)}: {Smoothness},{nameof(Grade)}: {Grade},{nameof(RoadName)}: {RoadName},{nameof(Layer)}: {Layer}, #{nameof(Nodes)}: {Nodes.Count}, Traffic: {Traffic}";
        }

        private static RoadSpeedStyling? tryGetSurfaceSpeed(RoadSurface surface)
        {
            switch (surface)
            {
                case RoadSurface.Wood: return RoadSpeedStyling.Walk();
                case RoadSurface.AsphaltLike: return RoadSpeedStyling.Asphalt();
                case RoadSurface.DirtLike: return RoadSpeedStyling.Ground();
                case RoadSurface.Ice:
                case RoadSurface.GrassLike: return RoadSpeedStyling.Ground();
                case RoadSurface.HardBlocks: return RoadSpeedStyling.HardBlocks();
                case RoadSurface.SandLike: return RoadSpeedStyling.Sand();

                case RoadSurface.Paved: return RoadSpeedStyling.Paved();
                case RoadSurface.Unpaved:
                case RoadSurface.Unknown: return null;
                default: throw new NotImplementedException($"{surface}");
            }
        }


        public RoadSpeedStyling GetRoadSpeedMode()
        {
            {
                // special cases

                if (this.Kind == WayKind.Ferry) // has to be placed before anything else, because we don't ride on the ferry, but the ferry travels with its own speed
                    return RoadSpeedStyling.CableFerry();
                if (this.Kind == WayKind.Steps)
                    return RoadSpeedStyling.CarryBike();
                if (this.Dismount || this.IsSingletrack)
                    return RoadSpeedStyling.Walk();
            }

            var mode = getStructuralMode();
            
            if ((mode == RoadSpeedStyling.Asphalt() /*|| speed_mode == SpeedMode.Paved*/)
                && this.Smoothness.EqualOrWorse(RoadSmoothness.VeryBad))
                return RoadSpeedStyling.Ground();
            else if (mode == RoadSpeedStyling.Ground() && this.Grade.EqualOrWorse(RoadGrade.Grade5))
                return RoadSpeedStyling.Sand();

            return mode;
        }

        private RoadSpeedStyling getStructuralMode()
        {
            if (this.Kind <= WayKind.SecondaryLink)
                return RoadSpeedStyling.Asphalt();


            var speed_mode = tryGetSurfaceSpeed(this.Surface);

            if (this.Kind == WayKind.Footway)
            {
                if (this.BikeLane) // footway with separated bike line has to be asphalt or paving stones
                    return speed_mode ?? RoadSpeedStyling.Asphalt();
                else if (this.HasUrbanSidewalk) // sidewalks should be rather well done, but there are pedestrians so we cannot go full speed
                    return RoadSpeedStyling.UrbanSidewalk();

                // if this is any other footway, it rather means we are outside urban area and we have to rely on surface reading,
                // and probably noboy will care about riding it
            }

            if (this.Kind <= WayKind.TertiaryLink && (speed_mode == null || speed_mode== RoadSpeedStyling.Paved())) 
                speed_mode = RoadSpeedStyling.Asphalt();

            if (speed_mode == null)
            {
                if (IsLikelyPaved())
                {
                    return new RoadSpeedStyling("?paved", RoadSpeeding.Paved,
                        RoadStyling.Unknown);
                }
                else if (this.Kind == WayKind.Auxiliary)
                {
                    return new RoadSpeedStyling("?solid", RoadSpeeding.UnknownStructured,
                        RoadStyling.Unknown);
                }
                else if (this.Kind == WayKind.Cycleway
                         || this.Kind == WayKind.Crossing)
                    return new RoadSpeedStyling("?solid",RoadSpeeding.UnknownStructured,
                        RoadStyling.Unknown);
                else
                    return new RoadSpeedStyling("?unknown",RoadSpeeding.UnknownLoose,
                        RoadStyling.Unknown);
            }

            return speed_mode.Value;
        }

        public bool IsLikelyPaved()
        {
            return this.HasUrbanSidewalk 
                   && (this.Kind == WayKind.Auxiliary || this.Kind == WayKind.Unclassified);
        }

        public bool IsCyclewayCounterpart()
        {
            // can given road can be think as related to the cycleway
            // for example: nobody will build cycleway for forest track
            // but for some state-road -- sure
            return !IsCyclingForbidden() && this.Kind < WayKind.Cycleway;
        }

        public bool IsCyclingForbidden()
        {
            return Kind <= WayKind.HighwayLink || !HasAccess;
        }

        public static (bool, RoadInfo<TNodeId>) Merge(RoadInfo<TNodeId> existing,
            RoadInfo<TNodeId> other)
        {
            // the speed limits are computed, thus only those can differ when it comes to merging
            // multiple sources
            if (existing.Traffic.SpeedLimit != other.Traffic.SpeedLimit)
            {
                // if one piece of map managed to compute the limit, we trust such computation
                var max_speed = (existing.Traffic.SpeedLimit ?? other.Traffic.SpeedLimit)!
                    .Value.Max((other.Traffic.SpeedLimit ?? existing.Traffic.SpeedLimit)!.Value);
                // we use max, not min, speed because it will affect the ride safety, i.e. it is better
                // to assume the road is high speed and avoid it, then select it and be surprised while riding
                existing = existing.BuildWithSpeedLimit(max_speed);
                other = other.BuildWithSpeedLimit(max_speed);
            }

            // urban sidewalk is extracted, but also computed
            if (existing.HasUrbanSidewalk != other.HasUrbanSidewalk)
            {
                existing = existing.BuildWithUrbanSidewalkFlag();
                other = other.BuildWithUrbanSidewalkFlag();
            }

            return (existing == other, existing);
        }


        public RoadInfo<TNodeId> RebuildAsRoundaboutStar(TNodeId centerId, TNodeId nodeId)
        {
            var features = this.features;
            features &= ~RoadFeatures.Roundabout;
            features &= ~RoadFeatures.OneWay;
            features |= RoadFeatures.RoundaboutStar;

            return new RoadInfo<TNodeId>(this.Kind, this.RoadName,
                features,
                this.Surface,
                this.Smoothness, Grade, Layer, Traffic,
                new[] { centerId, nodeId});

        }

        public bool IsSignificantlyMoreObvious(in RoadInfo<TNodeId> other)
        {
            if (this.Kind.IsSignificantlyMoreImportant(other.Kind))
                return true;
            if (other.Kind.IsSignificantlyMoreImportant(this.Kind))
                return false;

            return this.Surface == RoadSurface.AsphaltLike && !other.Surface.IsSolid();
        }

    }
}