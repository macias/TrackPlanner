﻿namespace TrackPlanner.Mapping.Data
{
    public readonly record struct CityInfo
    {
        public CityRank Rank { get; }
        public string? Name { get; }

        public CityInfo(CityRank rank, string? name)
        {
            Rank = rank;
            Name = name;
        }

        public void Deconstruct(out CityRank rank, out string? name)
        {
            rank = this.Rank;
            name = this.Name;
        }
    }
}