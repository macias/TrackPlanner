﻿#nullable enable

namespace TrackPlanner.Mapping.Data
{
    public enum RoadSurface : byte
    {
        AsphaltLike = 0,
        HardBlocks = 1,
        Paved = 2,
        DirtLike = 3,
        GrassLike = 4,
        SandLike = 5,
        Unpaved = 6,
        Ice = 7,
        Wood = 8, // making it separate because I don't ride on wood surfaces (SPLINTERS and NAILS)
        Unknown = 9, // separate value (instead of unpaved) is handy for computing turn-notifications and it is better than having nullable surface (for painting)
    }

    public static class RoadSurfaceExtension
    {
        public static int IndexOf(this RoadSurface kind)
        {
            return (int) kind;
        }
        public static bool IsSolid(this RoadSurface kind)
        {
            return kind == RoadSurface.AsphaltLike || kind == RoadSurface.HardBlocks || kind == RoadSurface.Paved;
        }
    }
}