﻿namespace TrackPlanner.Mapping.Data
{
    public enum RoadGrade
    {
        //https://wiki.openstreetmap.org/wiki/Key:tracktype
        Grade1 = 1,
        Grade2 = 2,
        Grade3 = 3,
        Grade4 = 4,
        Grade5 = 5,
    }

    public static class RoadGradeExtension
    {
        public static bool EqualOrWorse(this RoadGrade grade, RoadGrade other)
        {
            return (int) grade >= (int) other;
        }
    }
}