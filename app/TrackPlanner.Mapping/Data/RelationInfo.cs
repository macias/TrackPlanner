﻿using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared.Data;

#nullable enable

namespace TrackPlanner.Mapping.Data
{
    public readonly record struct RelationInfo
    {
        public string Name { get; }
        public OsmId Id { get; }
        public List<OsmId> WayNodes { get; }

        public RelationInfo(string name, long id, IEnumerable<long> wayNodes)
        {
            Name = name;
            Id = OsmId.PureOsm(id);
            this.WayNodes = wayNodes.Select(it =>OsmId.PureOsm(it)).ToList();
        }
    }
}