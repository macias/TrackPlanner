﻿namespace TrackPlanner.Mapping.Data
{
    public static class MapDataInfo
    {
        public static string SectionSeparator { get; } = ";";
        public static string ValueSeparator { get; } = ",";
        public static string NonSeparator { get; } = "-";
    }
}