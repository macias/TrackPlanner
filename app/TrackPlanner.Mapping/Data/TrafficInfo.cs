﻿using System;
using System.Collections.Generic;
using System.IO;
using MathUnit;

namespace TrackPlanner.Mapping.Data
{
    public readonly record struct TrafficInfo
    {
        public static int AssumedSpeedLimitKph => 70;
        public static int SafeSpeedKph => 50;
        public static Speed SafeSpeed { get; } = Speed.FromKilometersPerHour(SafeSpeedKph);


        private readonly byte speedLanePack;

        public int LaneCount => unpackSpeedLanes(this.speedLanePack).lanes;
        public Speed? SpeedLimit => unpackSpeedLanes(this.speedLanePack).speed;

        private TrafficInfo(byte rawData)
        {
            this.speedLanePack = rawData;
        }

        public TrafficInfo(Speed? speedLimit, int laneCount) : this(packSpeedLanes(speedLimit, laneCount))
        {
        }

        public override string ToString()
        {
            return $"Lanes: {LaneCount}, Speed: {SpeedLimit}";
        }

        private static byte packSpeedLanes(Speed? speed, int lanes)
        {
            if (lanes < 1)
                throw new ArgumentOutOfRangeException(nameof(lanes));

            int raw_speed = 0;
            if (speed is { } s)
                raw_speed = 1 + (int) Math.Max(0, Math.Round((s.KilometersPerHour - TrafficInfo.SafeSpeedKph) / 10));
            raw_speed &= 15;
            lanes &= 15;
            return (byte) (raw_speed | ((lanes - 1) << 4));
        }

        private static (Speed? speed, int lanes) unpackSpeedLanes(byte raw)
        {
            var lanes = 1 + (raw >> 4);
            int raw_speed = raw & 15;
            Speed? speed = null;
            if (raw_speed > 0)
                speed = Speed.FromKilometersPerHour((raw_speed - 1) * 10.0 + TrafficInfo.SafeSpeedKph);
            return (speed, lanes);
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write(this.speedLanePack);

        }

        public static TrafficInfo Read(BinaryReader reader)
        {
            return new TrafficInfo(reader.ReadByte());
        }
        
        internal string GetMapDataDetails()
        {
            return String.Join($"{MapDataInfo.SectionSeparator} ",
                $"{(this.SpeedLimit is {} s ? (int)s.KilometersPerHour:"?")}",
                $"{this.LaneCount}");
        }

        public static TrafficInfo Parse(IReadOnlyList<string> parts, ref int index, int? fallbackSpeedLimitKph)
        {
            Speed? speed = null;
            if (index < parts.Count)
            {
                var s = parts[index++];
                if (s != "?")
                    speed = Speed.FromKilometersPerHour(int.Parse(s));
            }
            else if (fallbackSpeedLimitKph is { } fallback)
                speed = Speed.FromKilometersPerHour(fallback);
            else
                throw new ArgumentException();

            int lanes = 1;
            if (index < parts.Count)
                lanes = int.Parse(parts[index++]);
            return new TrafficInfo(speed, lanes);
        }


        public bool MoreRiskyThan(TrafficInfo other)
        {
            if (SpeedLimit == null && other.SpeedLimit != null)
                return true;
            else if (SpeedLimit != null && other.SpeedLimit == null)
                return false;
            else if (SpeedLimit > other.SpeedLimit)
                return true;
            else if (this.LaneCount > other.LaneCount)
                return true;

            return false;
        }
    }
}