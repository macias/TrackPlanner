﻿using OsmId = TrackPlanner.Shared.Data.OsmId;

namespace TrackPlanner.Mapping;

using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

using TNodeId = OsmId;
using TRoadId = OsmId;

public sealed class RoadGridMemoryBuilder
{
    public int CellSize { get; }

    private readonly WorldMapMemory mapMemory;
    private readonly IGeoCalculator calc;
    private readonly ILogger logger;
    private readonly string? debugDirectory;

    public RoadGridMemoryBuilder(ILogger logger, WorldMapMemory mapMemory, IGeoCalculator calc,
        int gridCellSize, string? debugDirectory)
    {
        this.CellSize = gridCellSize;
        this.logger = logger;
        this.mapMemory = mapMemory;
        this.calc = calc;
        this.debugDirectory = debugDirectory;
    }

    private RoadGridCell<TNodeId, TRoadId> selectCell(HashMap<CellIndex, RoadGridCell<TNodeId, TRoadId>> cells,
        in GeoZPoint current, out CellIndex cellIndex)
    {
        cellIndex = CellIndex.Create(current.Latitude, current.Longitude, this.CellSize);

        if (!cells.TryGetValue(cellIndex, out var cell))
        {
            cell = new RoadGridCell<TNodeId, TRoadId>();
            cells.Add(cellIndex, cell);
        }

        return cell;
    }

    public HashMap<CellIndex, RoadGridCell<TNodeId, TRoadId>> BuildCells()
    {
        var cells = HashMap.Create<CellIndex, RoadGridCell<TNodeId, TRoadId>>();

        foreach (var (road_map_index, road_info) in this.mapMemory.GetAllRoads())
        {
            for (int i = 0; i < road_info.Nodes.Count - 1; ++i)
            {
                var curr_idx = new RoadIndex<TRoadId>(road_map_index, i);
                GeoZPoint curr_point = this.mapMemory.GetPoint(curr_idx);
                GeoZPoint next_point = this.mapMemory.GetPoint(curr_idx.Next());

                IEnumerable<CellIndex> used = GetCellIndices(this.calc,curr_point, 
                    next_point,this.CellSize);

                foreach (var cell_idx in used)
                {
                    if (!cells.TryGetValue(cell_idx, out var cell))
                    {
                        cell = new RoadGridCell<TNodeId, TRoadId>();
                        cells.Add(cell_idx, cell);
                    }
                    
                    cell.AddSegment(curr_idx);
                }

            }
        }

        {
            var debug_stats = cells.Values.Select(it => it.RoadSegments.Count).OrderBy(x => x).ToArray();
            logger.Info($"Grid cells fill stats: count = {debug_stats.Length}, min = {debug_stats.First()}, median = {debug_stats[debug_stats.Length / 2]}, max: {debug_stats.Last()}");
        }

        return cells;
    }

    public static IEnumerable<CellIndex> GetCellIndices(IGeoCalculator calc,
        GeoZPoint segmentStart, GeoZPoint segmentEnd,int cellSize)
    {
        var used = new HashSet<CellIndex>();
        var curr_cell_idx = CellIndex.Create(segmentStart.Latitude, segmentStart.Longitude, cellSize);
        used.Add(curr_cell_idx);
        var next_cell_idx = CellIndex.Create(segmentEnd.Latitude, segmentEnd.Longitude, cellSize);
        used.Add(next_cell_idx);
        if (!isSideAdjacentOrSame(curr_cell_idx, next_cell_idx))
        {
            // far from optimal -- from each corner of the cells calculate
            // projection on the segment, compute in which
            // cell the crosspoint falls and register segment there

            foreach (var pt in getLatticePoints(  curr_cell_idx, next_cell_idx,cellSize))
            {
                (_, var cx, _) = calc.GetFlatDistanceToArcSegment(pt, segmentStart,
                    segmentEnd, stable: false);
                var cx_cell_idx = CellIndex.Create(cx.Latitude, cx.Longitude, cellSize);
                used.Add(cx_cell_idx);
            }
        }

        return used;
    }

    private static IEnumerable<GeoZPoint> getLatticePoints(CellIndex currentCellIndex,
        CellIndex nextCellIndex,int cellSize)
    {
        for (int lati = Math.Min(currentCellIndex.LatitudeGridIndex,
                 nextCellIndex.LatitudeGridIndex);
             lati <= Math.Max(currentCellIndex.LatitudeGridIndex,
                 nextCellIndex.LatitudeGridIndex);
             ++lati)
        for (int loni = Math.Min(currentCellIndex.LongitudeGridIndex,
                 nextCellIndex.LongitudeGridIndex);
             loni <= Math.Max(currentCellIndex.LongitudeGridIndex,
                 nextCellIndex.LongitudeGridIndex);
             ++loni)
        {
            foreach ((Angle lat, Angle lon) in getCellCorners(lati, loni, cellSize))
            {
                yield return GeoZPoint.Create(lat, lon, null);
            }
        }
    }

    private static bool isSideAdjacentOrSame(CellIndex indexA, CellIndex indexB)
    {
        return Math.Abs(indexA.LatitudeGridIndex - indexB.LatitudeGridIndex)
            + Math.Abs(indexA.LongitudeGridIndex - indexB.LongitudeGridIndex) <= 1;
    }

    private static IEnumerable<(Angle lat, Angle lon)> getCellCorners(int latIndex, int lonIndex,
        int cellSize)
    {
        yield return CellIndex.Recreate(latIndex, lonIndex, cellSize);
        yield return CellIndex.Recreate(latIndex + 1,lonIndex + 1,cellSize);
        yield return CellIndex.Recreate(latIndex + 1, lonIndex ,cellSize);
        yield return CellIndex.Recreate(latIndex , lonIndex + 1, cellSize);
    }
}