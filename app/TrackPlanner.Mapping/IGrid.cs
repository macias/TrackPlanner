﻿using MathUnit;
using System.Collections.Generic;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public interface IGrid
    {
        int CellSize { get; }
        IGeoCalculator Calc { get; } 
        CellIndex NorthEast { get; }
        CellIndex SouthWest { get; }
        
        string GetStats();
    }
    
    public interface IGrid<TNodeId,TRoadId> : IGrid
        where TNodeId:struct
        where TRoadId : struct
    {

        IEnumerable<KeyValuePair<CellIndex,IReadOnlyGridCell<TNodeId,TRoadId>>> Cells { get; }

        List<RoadBucket<TNodeId, TRoadId>> GetRoadBuckets(IReadOnlyList<RequestPoint<TNodeId>> userPoints, 
            Length proximityLimit,
            Length upperProximityLimit);
        RoadBucket<TNodeId,TRoadId> CreateBucket(int index, GeoZPoint userPoint, Length initProximityLimit,
            Length finalProximityLimit,bool forceClosest,  
            bool allowSmoothing,bool allowGap);

        bool TryGetAllNodes(CellIndex cellIndex,out IEnumerable<TNodeId> nodes);
        CellIndex GetIndexOfNode(TNodeId nodeId);
    }

}