﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.IO;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public class RoadGridCell<TNodeId, TRoadId> : IReadOnlyGridCell<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        // todo: we could keep only those segments starters which are not in nodes-roads map
        // but watch out -- the last node of the road does not create a segment, yet this road-node is
        // present nodes-roads map

        // segment from given road index to (implicit) next road index
        // when working, we don't need hashset, so let's keep it list for lower memory
        private readonly List<RoadIndex<TRoadId>> roadRoadSegments;

        public IReadOnlyList<RoadIndex<TRoadId>> RoadSegments => this.roadRoadSegments;

        public RoadGridCell()
        {
            this.roadRoadSegments = new List<RoadIndex<TRoadId>>();
        }

        public RoadGridCell(List<RoadIndex<TRoadId>> segmets)
        {
            this.roadRoadSegments = segmets;
        }

        public virtual IEnumerable<TNodeId> GetNodes()
        {
            throw new NotSupportedException();
        }

        public virtual IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetRoads()
        {
            throw new NotSupportedException();
        }

        internal void AddSegment(RoadIndex<TRoadId> roadIdx)
        {
            this.roadRoadSegments.Add(roadIdx);
        }

        public virtual (int, int) GetStats()
        {
            return (0, 0);
        }

        public IEnumerable<TNodeId> GetSegmentNodes(IWorldMap<TNodeId, TRoadId> map)
        {
            foreach (var idx in this.roadRoadSegments)
            {
                yield return map.GetNode(idx);
                yield return map.GetNode(idx.Next());
            }
        }

        public virtual void Write(BinaryWriter writer)
        {
            DiskHelper.WriteMap(writer, this.roadRoadSegments, (w, v) => RoadIndexStorage.Write(w, v));
        }

        public IEnumerable<RoadSnapInfo<TNodeId, TRoadId>> GetSnaps(IWorldMap<TNodeId, TRoadId> map, IGeoCalculator calc,
            GeoZPoint point, Length snapLimit,
            bool enforce,
            Func<RoadInfo<TNodeId>, bool>? predicate)
        {
            foreach (var idx in this.roadRoadSegments)
            {
                RoadInfo<TNodeId> road_info = map.GetRoadInfo(idx.RoadId);
                if (predicate != null && !predicate(road_info))
                    continue;

                // because we basically look for points on mapped ways, we expect the difference to be so small that we can use plane/euclidian distance
                var start = map.GetPoint(idx);
                var end = map.GetPoint(idx.Next());
                (var snap_distance, var cx, Length distance_along_segment) = calc.GetFlatDistanceToArcSegment(point, start,
                    end, stable: true);
                if (snap_distance <= snapLimit)
                {
                    yield return new RoadSnapInfo<TNodeId, TRoadId>(
#if DEBUG
                        MapRef.Create(map, idx).Value,
#endif
                        idx, snapKind: SnapKind.Segment,
                        snap_distance, cx);
                }
            }
        }
    }
}