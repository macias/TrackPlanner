﻿using System.IO;
using System.Runtime.CompilerServices;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class WorldIdentifierExtension
    {
        public static void Write(this WorldIdentifier identifier, BinaryWriter writer)
        {
            identifier.CellIndex.Write(writer);
            writer.Write(identifier.EntityIndex);
        }
        public static WorldIdentifier Read(BinaryReader reader)
        {
            var cell_index = CellIndexExtension.Read(reader);
            var entity_index = reader.ReadUInt16();

            return new WorldIdentifier(cell_index, entity_index);
        }

    }
  
    
}