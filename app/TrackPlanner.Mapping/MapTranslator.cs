using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class MapTranslator
    {
        public static IEnumerable<TurnInfo<OsmId,OsmId>> TurnsToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,IEnumerable<TurnInfo<TNodeId,TRoadId>> turns)
            where TNodeId:struct
            where TRoadId:struct
        {
            return turns.Select(ti => new TurnInfo<OsmId, OsmId>(ti.Entity, ti.NodeId.HasValue ? map.GetOsmNodeId(ti.NodeId!.Value) : null,
                ti.AtRoundabout() ? map.GetOsmRoadId(ti.RoadId!.Value) : null,
                ti.Point, ti.TrackIndex, ti.RoundaboutCounter, ti.Forward, ti.Backward, ti.Reason));
        }

        public static RoadInfo<TTargetNodeId> ConvertRoadInfo<TSourceNodeId, TTargetNodeId>(RoadInfo<TSourceNodeId> roadInfo,
            Func<TSourceNodeId,TTargetNodeId> translateNodeId)
        {
            return new RoadInfo<TTargetNodeId>(roadInfo.Kind,
                roadInfo.HasName ? roadInfo.RoadName : null,
                roadInfo.OneWay, roadInfo.IsRoundabout, roadInfo.Surface,
                roadInfo.Smoothness, roadInfo.Grade,
                roadInfo.HasAccess, roadInfo.HasSpeedLimit50, 
                roadInfo.SpeedLimit,
                roadInfo.BikeLane, roadInfo.IsSingletrack,
                roadInfo.HasUrbanSidewalk, roadInfo.Dismount,
                //isRedundant:roadInfo.HasRedundantFlag,
                roadInfo.Layer,
                roadInfo.LaneCount,
                roadInfo.Nodes.Select(translateNodeId).ToList());
        }

        public static NodeInfo<TTargetRoadId> ConvertNodeInfo<TSourceRoadId, TTargetRoadId>(NodeInfo<TSourceRoadId> nodeInfo,
            Func<TSourceRoadId,TTargetRoadId> translateRoadId)
        {
            return new NodeInfo<TTargetRoadId>(nodeInfo.Point,
                nodeInfo.Roads
                    .Select(it => new RoadIndex<TTargetRoadId>(translateRoadId(it.RoadId), it.IndexAlongRoad))
                    .ToHashSet(),
                nodeInfo.IsRoundaboutCenter);
        }

        public static MapRef< OsmId,OsmId>? ConvertToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            MapRef<TNodeId,TRoadId>? mapRef)
            where TNodeId:struct
            where TRoadId : struct, IEquatable<TRoadId>
        {
            if (mapRef is { } mref)
                return new MapRef<OsmId, OsmId>(
                    ConvertToOsm(map,mref.RoadIndex)!.Value,
                    mref.OsmRoadId,
                    mref.OsmNodeId);
            else
                return null;
        }
        public static MapRef<TNodeId, TRoadId>? ConvertFromOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            MapRef<OsmId,OsmId>? mapRef)
            where TNodeId:struct
            where TRoadId : struct, IEquatable<TRoadId>
        {
            if (mapRef is { } mref)
                return new MapRef<TNodeId, TRoadId>(
                    ConvertFromOsm(map,mref.RoadIndex)!.Value,
                    mref.OsmRoadId,
                    mref.OsmNodeId);
            else
                return null;
        }
        public static RoadIndex< OsmId>? ConvertToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<TRoadId>? roadIndex)
            where TNodeId:struct
            where TRoadId : struct, IEquatable<TRoadId>
        {
            if (roadIndex is { } rdx)
                return new RoadIndex<OsmId>(map.GetOsmRoadId(rdx.RoadId),rdx.IndexAlongRoad);
            else
                return null;
        }
        public static RoadIndex< TRoadId>? ConvertFromOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<OsmId>? roadIndex)
            where TNodeId:struct
            where TRoadId : struct, IEquatable<TRoadId>
        {
            if (roadIndex is { } rdx)
                return new RoadIndex<TRoadId>(map.FromOsmRoadId(rdx.RoadId),rdx.IndexAlongRoad);
            else
                return null;
        }
        public static MapZPoint<TNodeId,TRoadId> ConvertFromOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            MapZPoint<OsmId,OsmId> place)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return new MapZPoint<TNodeId,TRoadId>(place.Point,
                place.NodeId == null ? null : map.FromOsmNodeId(place.NodeId.Value)
#if DEBUG
                ,ConvertFromOsm(map, place.DEBUG_MapRef)
                ,place.Description 
#endif
                );
        }
        public static MapZPoint<OsmId,OsmId> ConvertToOsm<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            MapZPoint<TNodeId,TRoadId> place)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return new MapZPoint<OsmId,OsmId>(place.Point,
                place.NodeId == null ? null : map.GetOsmNodeId(place.NodeId.Value)
#if DEBUG
                , ConvertToOsm(map,place.DEBUG_MapRef)
                ,place.Description 
#endif
                );
        }
        public static FragmentStep<OsmId,OsmId> ConvertToOsm<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            FragmentStep<TNodeId,TRoadId> step)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return new FragmentStep<OsmId,OsmId>(
                #if DEBUG
                ConvertToOsm(map,step.DEBUG_MapRef),
                #endif
                step.Point, 
                step.NodeId == null ? null : map.GetOsmNodeId(step.NodeId.Value),
                step.IncomingFlatDistance);
        }

        public static IEnumerable<MapZPoint<OsmId,OsmId>> ConvertToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
          IEnumerable<MapZPoint<TNodeId,TRoadId>> mapPoints)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return mapPoints.Select(p => ConvertToOsm(map,p));
        }

        public static IEnumerable<FragmentStep<OsmId,OsmId>> ConvertToOsm<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            IEnumerable<FragmentStep<TNodeId,TRoadId>> steps)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            return steps.Select(p => ConvertToOsm(map,p));
        }

        public static RoutePlan<OsmId, OsmId> ConvertToOsm<TNodeId,TRoadId>(IWorldMap<TNodeId,TRoadId> map,
            RoutePlan<TNodeId, TRoadId> plan)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            RoutePlan<OsmId, OsmId> osm_plan = new RoutePlan<OsmId, OsmId>()
            {
                Legs = plan.Legs.Select(l => new LegPlan<OsmId, OsmId>()
                    {
                        Fragments = l.Fragments.Select(f => convertFragmentToOsm(map, f,isDraft:l.IsDrafted))
                            .ToList(),
                        UnsimplifiedDistance = l.UnsimplifiedDistance,
                        RawTime = l.RawTime,
                        IsDrafted = l.IsDrafted,
                        AutoAnchored = l.AutoAnchored,
                        AllowGap = l.AllowGap
                    }
                ).ToList(),
                DailyTurns = plan.DailyTurns.Select(dt => map.TurnsToOsm(dt).ToList()).ToList(),
            };

            return osm_plan;
        }

        private static LegFragment<OsmId, OsmId> convertFragmentToOsm<TNodeId, TRoadId>(IWorldMap<TNodeId, TRoadId> map,
            LegFragment<TNodeId, TRoadId> fragment,
            bool isDraft) 
            where TNodeId : struct, IEquatable<TNodeId> 
            where TRoadId : struct, IEquatable<TRoadId>
        {
            HashSet<OsmId> road_ids = fragment.RoadIds
                .Select(id => isDraft?default(OsmId): map.GetOsmRoadId(id)).ToHashSet();
            
            return new LegFragment<OsmId, OsmId>()
            {
                CostInfo = fragment.CostInfo,
                SpeedStyling = fragment.SpeedStyling,
                IsForbidden = fragment.IsForbidden,
                Steps = ConvertToOsm(map,fragment.GetSteps()).ToList(),
                UnsimplifiedFlatDistance = fragment.UnsimplifiedFlatDistance,
                RawTime = fragment.RawTime,
                RoadIds = road_ids,
            };
        }
    }
}