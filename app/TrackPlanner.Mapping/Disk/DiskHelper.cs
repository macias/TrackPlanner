﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;

namespace TrackPlanner.Mapping.Disk
{
    public static class DiskHelper
    {
        internal static CompositeDisposable FilenamesToStreams(IEnumerable<string> fileNames,
            out IReadOnlyList<(Stream stream,string name)> files)
        {
            files = fileNames.Select(fn => (new FileStream(fn, FileMode.Open, FileAccess.Read).Me<Stream>(), fn)).ToArray();  
            return CompositeDisposable.Create(files.Select(it => it.Item1));
        }

        public static void WriteId<T>(BinaryWriter writer, T id)
        {
            if (id is long long_id)
                writer.Write(long_id);
            else if (id is WorldIdentifier world_id)
                world_id.Write(writer);
            else
                throw new NotSupportedException();
        }
        
        public static T ReadId<T>(BinaryReader reader)
        {
            T cast<R>(R value)
            {
                if (value is T res)
                    return res;
                else
                    throw new NotImplementedException();
            }
            if (typeof(T)==typeof( long ))
                return cast(reader.ReadInt64());
            else if (typeof(T)==typeof(WorldIdentifier))
                return cast(WorldIdentifierExtension.Read(reader));
            else
                throw new NotSupportedException();
        }

        public static void WriteMap<TKey, TValue>(BinaryWriter writer, IReadOnlyMap<TKey, TValue> data,
            Action<TKey> writeKey, Action<TValue> writeValue)
            where TKey : notnull
        {
            writer.Write(data.Count);
            foreach (var (key, value) in data)
            {
                writeKey(key);
                writeValue(value);
            }
        }

        public static void WriteMap< T>(BinaryWriter writer, IReadOnlyList< T> data,
            Action<BinaryWriter,T> writeValue)
        {
            writer.Write(data.Count);
            foreach (var  value in data)
            {
                writeValue(writer,value);
            }
        }

        public static void WriteList<T>(BinaryWriter writer, List<T> list, Action<T> writeValue)
        {
            writer.Write(list.Count);
            foreach (var elem in list)
            {
                writeValue(elem);
            }
        }

        public static void ReadList<T>(BinaryReader reader,ref List<T>? list,Func<BinaryReader,T> readValue)
        {
            list = readList(reader, list, readValue);
        }
        public static List<T> ReadList<T>(BinaryReader reader,Func<BinaryReader,T> readValue)
        {
            return readList(reader,null, readValue);
        }
        
        
        private static List<T> readList<T>(BinaryReader reader,List<T>? list,Func<BinaryReader,T> readValue)
        {
            int count = reader.ReadInt32();
            if (list == null)
                list = new List<T>(capacity: count);
            else
                list.Capacity += count;
            
            for (int i = 0; i < count; ++i)
            {
                list.Add(readValue(reader));
            }

            return list;
        }
        
        public static void ReadMap<TKey, TValue>(BinaryReader reader,
            ref IMap<TKey, TValue>? map,
            Func<int,IMap<TKey, TValue>> create,
            Func<BinaryReader,TKey> readKey, Func<BinaryReader,TValue> readValue)
            where TKey : notnull
        where TValue:IEquatable<TValue>
        {
            map = readMap(reader, map, create, readKey, readValue,EqualityComparer<TValue>.Default);
        }
        
        public static IMap<TKey, TValue> ReadMap<TKey, TValue>(BinaryReader reader,
            Func<int,IMap<TKey, TValue>> create,
            Func<BinaryReader,TKey> readKey, Func<BinaryReader,TValue> readValue)
            where TKey : notnull
            where TValue:IEquatable<TValue>
        {
            return readMap(reader, null, create, readKey, readValue,EqualityComparer<TValue>.Default);
        }
        public static IMap<TKey, TValue> ReadMap<TKey, TValue>(BinaryReader reader,
            Func<int,IMap<TKey, TValue>> create,
            Func<BinaryReader,TKey> readKey, Func<BinaryReader,TValue> readValue,
        IEqualityComparer<TValue> valueComparer)
            where TKey : notnull
        {
            return readMap(reader, null, create, readKey, readValue,valueComparer);
        }

        private static IMap<TKey, TValue> readMap<TKey, TValue>(BinaryReader reader,
            IMap<TKey, TValue>? map,
            Func<int, IMap<TKey, TValue>> create,
            Func<BinaryReader, TKey> readKey, Func<BinaryReader, TValue> readValue,
            IEqualityComparer<TValue> valueComparer)
            where TKey : notnull
        {
            mergeRead(reader, ref map, create, readKey, readValue, valueComparer,
                (_, _) => (false, default!));
            return map!;
        }

        public static void MergeRead<TKey, TValue>(BinaryReader reader,
            ref IMap<TKey, TValue>? map,
            Func<int, IMap<TKey, TValue>> create,
            Func<BinaryReader, TKey> readKey, Func<BinaryReader, TValue> readValue,
            Func<TValue, TValue, (bool done, TValue value)> mergeValue)
            where TKey : notnull
            where TValue : IEquatable<TValue>
        {
            mergeRead(reader, ref map, create, readKey, readValue,
                EqualityComparer<TValue>.Default, mergeValue);
        }

        private static void mergeRead<TKey, TValue>(BinaryReader reader,
            ref IMap<TKey, TValue>? map,
            Func<int,IMap<TKey, TValue>> create,
            Func<BinaryReader,TKey> readKey, Func<BinaryReader,TValue> readValue,
            IEqualityComparer<TValue> valueComparer,
            Func<TValue,TValue,(bool done,TValue value)> mergeValue)
            where TKey : notnull
        {
            var count = reader.ReadInt32();
            bool first_run = map == null;
            map ??= create(count);
            for (int i=0;i<count;++i)
            {
                var key = readKey(reader);
                var value = readValue(reader);
                if (!map.TryAdd(key, value, out var existing))
                {
                    if (first_run)
                        throw new ArgumentException($"Key collision {key}");
                    else if (!valueComparer.Equals(value,existing))
                    {
                        var (ok, merged) = mergeValue(value, existing);
                        if (ok)
                            map[key] = merged;
                        else
                            throw new ArgumentException($"{typeof(TValue).Name} value collision for {typeof(TKey).Name}::{key}, we read {value} while having {existing}");
                    }
                }
            }
        }
        
        public static void ReadSet<T>(BinaryReader reader,
            ref HashSet<T>? valueSet,
            Func<BinaryReader,T> readValue)
        {
            var count = reader.ReadInt32();
            valueSet ??= new HashSet<T>(count);
            for (int i=0;i<count;++i)
            {
                var value = readValue(reader);
                valueSet.Add(value);
            }
        }
        
    }
}
