﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Storage;
using TrackPlanner.Structures;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Mapping.Disk
{
    public sealed class RoadGridDisk<TCell> : RoadGrid<TNodeId,TRoadId, TCell>
        where TCell : RoadGridCell<TNodeId,TRoadId>
    {
        private readonly IReadOnlyEnumerableDictionary<CellIndex, TCell> cells;

        public RoadGridDisk(ILogger logger, IReadOnlyEnumerableDictionary<CellIndex, TCell> cells,
            IWorldMap<TNodeId,TRoadId> map, IGeoCalculator calc,
            int gridCellSize, string? debugDirectory)
            : base(logger, cells, map, calc, gridCellSize, debugDirectory)
        {
            this.cells = cells;
        }

        public override CellIndex GetIndexOfNode(TNodeId nodeId)
        {
            return nodeId.CellIndex;
        }

        public IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetAllRoads()
        {
            return this.cells.SelectMany(it => it.Value.GetRoads());
        }

        public override string GetStats()
        {
            int min_nodes = int.MaxValue;
            int min_roads = int.MaxValue;
            int max_nodes = int.MinValue;
            int max_roads = int.MinValue;
            int empty_count = 0;
            foreach (var (_,cell) in this.cells)
            {
                var (nc, rc) = cell.GetStats();
                if (nc == 0 && rc == 0)
                    ++empty_count;
                min_nodes = Math.Min(min_nodes, nc);
                max_nodes = Math.Max(max_nodes, nc);
                min_roads = Math.Min(min_roads, rc);
                max_roads = Math.Max(max_roads, rc);
            }
            
            return $"nodes: {min_nodes}--{max_nodes}, roads: {min_roads}--{max_roads}, empty: {empty_count}";
        }

        public void Write(BinaryWriter writer)
        {
            var offsets = new WriterOffsets<CellIndex>(writer);

            var stable_cells = this.cells.ToArray(); // making sure iterations go in the same order

            writer.Write(stable_cells.Count());
            foreach (var (cell_idx, _) in stable_cells)
            {
                cell_idx.Write(writer);
                offsets.Register(cell_idx);
            }

            foreach (var (cell_idx, cell) in stable_cells)
            {
                offsets.AddOffset(cell_idx);
                cell.Write(writer);
            }

            offsets.WriteBackOffsets();
        }


    }
}