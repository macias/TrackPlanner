﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Structures;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping.Disk
{

    public readonly struct NodeInfo<TRoadId> : IEquatable<NodeInfo<TRoadId>>
    {
        [Flags]
        private enum NodeFeatures : byte
        {
            None = 0,
            
            RoundaboutCenter = 1 << 0, // roundabout center
        }
        private readonly NodeFeatures features;
        
        public GeoZPoint Point { get; }
        public IReadOnlySet<RoadIndex<TRoadId>> Roads { get; }

        public bool IsRoundaboutCenter => this.features.HasFlag(NodeFeatures.RoundaboutCenter);

        private NodeInfo( GeoZPoint point, IReadOnlySet<RoadIndex<TRoadId>> roads, NodeFeatures features)
        {
            this.features = features;
            Point = point;
            Roads = roads;
        }

        public NodeInfo( GeoZPoint point, IReadOnlySet<RoadIndex<TRoadId>> roads, bool isRoundaboutCenter)
        : this(point,roads, NodeFeatures.None| (isRoundaboutCenter? NodeFeatures.RoundaboutCenter: NodeFeatures.None))
        {
        }

        public override bool Equals(object? other)
        {
            return other is NodeInfo<TRoadId> info && this.Equals(info);
        }
        public bool Equals(NodeInfo<TRoadId> other)
        {
            return Point.Equals(other.Point) 
                   && Roads.SetEquals(other.Roads)
                   && this.features==other.features;
        }

        public override int GetHashCode()
        {
            var hash_code = new HashCode();
            hash_code.Add(Point);
            hash_code.Add(this.features);
            hash_code.InvokeAddHashCode(Roads.GetUnorderedHashCode());
            return hash_code.ToHashCode();
        }

        public void Write(BinaryWriter writer)
        {
            writer.Write((byte)this.features);
            Point.Write(writer);
            writer.Write(Roads.Count);
            foreach (var road_idx in Roads)
            {
                RoadIndexStorage.Write(writer,road_idx);
            }
        }
        public static NodeInfo<TRoadId> Read(BinaryReader reader)
        {
            var features = (NodeFeatures)reader.ReadByte();
            var point = GeoZPoint.Read(reader);
            var count = reader.ReadInt32();
            var roads = new HashSet<RoadIndex<TRoadId>>(count);
            for (int i=0;i<count;++i)
                roads.Add(RoadIndexStorage.Read<TRoadId>(reader));

            return new NodeInfo<TRoadId>(point, roads,features);

        }

        public override string ToString()
        {
            return $"NodeInfo {this.features} @{Point.Convert2d()} with {Roads.Count} roads.";
        }

        public static (bool,NodeInfo<TRoadId>) Merge(NodeInfo<TRoadId> info1, NodeInfo<TRoadId> info2) 
        {
            if (info1.Point!=info2.Point)
                return (false,default);

            return (true,new NodeInfo<TRoadId>(info1.Point, 
                info1.Roads.Concat(info2.Roads).ToHashSet(),
                info1.features|info2.features));
        }
        
        internal string GetMapDataDetails()
        {
            return $"{this.features}";
        }
        
        internal static NodeInfo<TRoadId> Parse(GeoZPoint point, IReadOnlySet<RoadIndex<TRoadId>> roads, string? details)
        {
            details ??= "";
            NodeFeatures features ;
            if (details != "")
                features = Enum.Parse<NodeFeatures>(details);
            else
                features = NodeFeatures.None;

            return new NodeInfo<TRoadId>(point,roads,features);
        }

        public NodeInfo<TRoadId> WithRoad(RoadIndex<TRoadId> roadIndex)
        {
            var road_list = Roads.ToList();
            road_list.Add(roadIndex);
            return new NodeInfo<TRoadId>(Point, road_list.ToHashSet(), this.features);
        }
    }
  
}