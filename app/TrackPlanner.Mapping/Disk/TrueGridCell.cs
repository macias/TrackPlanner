﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geo;
using TrackPlanner.Elevation;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;
using TrackPlanner.Structures;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Mapping.Disk
{
    internal sealed class TrueGridCell : RoadGridCell<TNodeId,TRoadId>
    {
        private readonly CellIndex selfIndex;
        private readonly IMap<ushort,NodeInfo<TRoadId>> nodes;
        private readonly IMap<ushort,RoadInfo<TNodeId>> roads;
        private readonly IMap<ushort,TRoadId> bikeFootExpressNearbyNodes;
        private readonly List<(ushort nodeIndex, PointOfInterest attraction)> nodeAttractions;
        private readonly List<(ushort nodeIndex, CityInfo info)> cities;

        public IEnumerable<(ushort nodeIndex, PointOfInterest attraction)> NodeAttractions => this.nodeAttractions;
        public IEnumerable<(ushort nodeIndex, CityInfo info)> Cities => cities;
        public IEnumerable<(ushort nodeIndex, NodeInfo<TRoadId> info)> Nodes => nodes.Select(it => (it.Key,it.Value));
        
        public TrueGridCell(CellIndex selfIndex) : base()
        {
            this.selfIndex = selfIndex;
            this.nodes = new CompactDictionaryFill<ushort, NodeInfo<TRoadId>>();
            this.roads = new CompactDictionaryFill<ushort, RoadInfo<TNodeId>>();
            this.bikeFootExpressNearbyNodes = new CompactDictionaryFill<ushort, TRoadId>();
            this.nodeAttractions = new List<(ushort nodeIndex, PointOfInterest attraction)>();
            this.cities = new List<(ushort nodeIndex, CityInfo info)>();
        }

        public TrueGridCell(CellIndex selfIndex,
            IMap<ushort,NodeInfo<TRoadId>> nodes,
         IMap<ushort,RoadInfo<TNodeId>> roads,
         List<(ushort nodeIndex, PointOfInterest attraction)> attractions,
         List<(ushort nodeIndex, CityInfo info)> cities,
            IMap<ushort,TRoadId> bikeFootExpressNearbyNodes,
            List<RoadIndex<TRoadId>> segmets) : base( segmets)
        {
            this.selfIndex = selfIndex;
            this.nodes = nodes;
            this.roads = roads;
            this.nodeAttractions = attractions;
            this.cities = cities;
            this.bikeFootExpressNearbyNodes = bikeFootExpressNearbyNodes;
        }

        public override IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetRoads()
        {
            return this.roads.Select(it => KeyValuePair.Create(new TRoadId(this.selfIndex,it.Key),it.Value));
        }
        public override IEnumerable<TNodeId> GetNodes()
        {
            return this.nodes.Keys.Select(GetWorldIdentifier);
        }

        public WorldIdentifier GetWorldIdentifier(ushort index)
        {
            return new TNodeId(this.selfIndex,index);
        }

        public GeoZPoint GetPoint(ushort nodeIndex)
        {
            return nodes[nodeIndex].Point;
        }
        
        public IReadOnlySet<RoadIndex<TRoadId>> GetRoads(ushort nodeIndex)
        {
            return nodes[nodeIndex].Roads;
        }

        public RoadInfo<TNodeId> GetRoadInfo(ushort roadIndex)
        {
            return roads[roadIndex];
        }

        public NodeInfo<TRoadId> GetNodeInfo(ushort nodeIndex)
        {
            return nodes[nodeIndex];
        }

        public TRoadId? GetBikeFootRoadExpressNearby(ushort nodeIndex)
        {
            if (this.bikeFootExpressNearbyNodes.TryGetValue(nodeIndex, out var road_id))
                return road_id;
            else
                return null;
        }

        public void AddNode(ushort nodeIndex, NodeInfo<TRoadId> nodeInfo,
            TRoadId? bikeFootNearbyDangerousTraffic)
        {
            this.nodes.Add(nodeIndex,nodeInfo);
            if (bikeFootNearbyDangerousTraffic is {} road_id)
                this.bikeFootExpressNearbyNodes.Add(nodeIndex,road_id);
        }

        public void UpdateNodes(IElevationMap elevation, HashSet<GeoPoint> missingCoordinates)
        {
            foreach (var (idx,info) in this.nodes.ToList())
            {
                if (!elevation.TryGetHeight(info.Point.Convert2d(), out var height))
                    missingCoordinates.Add(ElevationMap.SourceCoordinate(info.Point.Convert2d()));

                this.nodes[idx] = new NodeInfo<TRoadId>(info.Point.WithAltitude(height),
                    info.Roads,info.IsRoundaboutCenter);
            }
        }

        public void AddRoad(ushort roadIndex, RoadInfo<TNodeId> roadInfo)
        {
            this.roads.Add(roadIndex,roadInfo);
        }

        public override void Write(BinaryWriter writer)
        {
            base.Write(writer);
            
            DiskHelper.WriteMap(writer,this.nodes,writer.Write, (info) => info.Write(writer));
            DiskHelper.WriteMap(writer,this.roads,writer.Write, (info) => info.Write(writer));
            
            DiskHelper.WriteList(writer,this.nodeAttractions, ( a) =>
            {
                writer.Write(a.nodeIndex);
                a.attraction.WriteAttraction(writer);
            });
            
            DiskHelper.WriteList(writer,this.cities, ( c) =>
            {
                writer.Write(c.nodeIndex);
                c.info.WriteCity(writer);
            });

            DiskHelper.WriteMap(writer,this.bikeFootExpressNearbyNodes,
                writer.Write, (nearby_id) => nearby_id.Write(writer));
        }
        
        public static TrueGridCell Load(CellIndex selfIndex, IReadOnlyList<BinaryReader> readers)
        {
            // all those data can have overlaps between readers (i.e. between map files)
            IMap<ushort, NodeInfo<TRoadId>>?  nodes = null;
            IMap<ushort, RoadInfo<TNodeId>>? roads = null;
            List<(ushort nodeIndex, PointOfInterest attraction)>? attractions = null;
            List<(ushort nodeIndex, CityInfo info)>? cities = null;
             IMap<ushort, TRoadId>? bike_foot_express = null;
             HashSet<RoadIndex<TRoadId>>? road_segments = null;
             foreach (var reader in  readers)
             {
                 DiskHelper.ReadSet(reader,ref road_segments,RoadIndexStorage.Read<TRoadId>);

                 DiskHelper.MergeRead(reader, ref nodes, 
                     capacity => new CompactDictionaryFill<ushort, NodeInfo<TRoadId>>(capacity),
                     r => r.ReadUInt16(), NodeInfo<TRoadId>.Read,NodeInfo<TRoadId>.Merge);

                 DiskHelper.MergeRead(reader, ref roads, 
                     capacity => new CompactDictionaryFill<ushort, RoadInfo<TNodeId>>(capacity),
                     r => r.ReadUInt16(), RoadInfo<TNodeId>.Read,RoadInfo<TNodeId>.Merge);
                 
                 DiskHelper.ReadList(reader,ref attractions, r =>
                 {
                     var idx = r.ReadUInt16();
                     var attr = DataDiskExtension.ReadAttraction(r);
                     return (idx,attr);
                 });

                 DiskHelper.ReadList(reader,ref cities, r =>
                 {
                     var idx = r.ReadUInt16();
                     var city = DataDiskExtension.ReadCity(r);
                     return (idx, city);
                 });

                 DiskHelper.MergeRead(reader, ref bike_foot_express, 
                     capacity => new CompactDictionaryFill<ushort, TRoadId>(capacity),
                     r => r.ReadUInt16(), DiskHelper.ReadId<TRoadId>,
                     // the better approach for merging info would be reading road and choosing more dangerous
                     // but the problem is, the road definition could be in some other cell, so let's select
                     // the first one
                     (id1,id2) => (true,id1));
             }

             return new TrueGridCell(selfIndex, nodes!,roads!,attractions!,cities!, bike_foot_express!,
                 road_segments!.ToList());
        }
        
        
        public override (int, int) GetStats()
        {
            return (nodes.Count, this.roads.Count);
        }

        public void AddAttraction(TNodeId nodeId, PointOfInterest attraction)
        {
           this.nodeAttractions.Add((nodeId.EntityIndex,attraction));
        }

        public void AddCity(TNodeId nodeId, CityInfo info)
        {
            this.cities.Add((nodeId.EntityIndex,info));
        }
    }
}