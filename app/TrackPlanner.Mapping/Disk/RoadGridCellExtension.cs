﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping.Disk
{
    public static class RoadGridCellExtension
    {
        public static void WriteMemory(this RoadGridCell<OsmId,OsmId> cell, BinaryWriter writer,
            IWorldMap<OsmId,OsmId> map,
            IReadOnlyDictionary<OsmId, long> nodeOffsets)
        {
            using (new OffsetKeeper(writer))
            {
                writer.Write(cell.RoadSegments.Count);
                foreach (var elem in cell.RoadSegments)
                    RoadIndexStorage.Write(writer,elem);
            }

            foreach (var node_id in cell.GetSegmentNodes(map).Distinct())
            {
                node_id.Write(writer);
                writer.Write(nodeOffsets[node_id]);
            }
        }
        
        public static RoadGridCell<TNodeId,TRoadId> Read<TNodeId,TRoadId>(CellIndex cellIndex,
            BinaryReader reader)
            where TNodeId:struct
        where TRoadId : struct
        {
            reader.ReadInt64(); // nodes offset
            
            var count = reader.ReadInt32();
            var segments = new HashSet<RoadIndex<TRoadId>>(capacity: count);
            for (int i = 0; i < count; ++i)
                segments.Add(RoadIndexStorage.Read<TRoadId>(reader));

            return new RoadGridCell<TNodeId,TRoadId>(segments.ToList());
        }
        
        public static unsafe RoadGridCell<TNodeId,TRoadId> Load<TNodeId,TRoadId>(CellIndex cellIndex, IReadOnlyList<BinaryReader> readers)
            where TNodeId:struct
        where TRoadId : struct
        {
            var counts = stackalloc int[readers.Count];
            int total_count = 0;
            for (int r=0;r<readers.Count;++r)
            {
                readers[r].ReadInt64(); // nodes offset
                var c = readers[r].ReadInt32();
                counts[r] = c;
                total_count += c;
            }

            var segments = new HashSet<RoadIndex<TRoadId>>(capacity: total_count);

            for (int r = 0; r < readers.Count; ++r)
            {
                for (int i = 0; i < counts[r]; ++i)
                    segments.Add(RoadIndexStorage.Read<TRoadId>(readers[r]));
            }

            return new RoadGridCell<TNodeId,TRoadId>( segments.ToList());
        }
    }

   
}
