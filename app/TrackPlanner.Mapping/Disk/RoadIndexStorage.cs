﻿using System.IO;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping.Disk
{
    public static class RoadIndexStorage
    {
        public static void Write<TRoadId>(BinaryWriter writer,RoadIndex<TRoadId> roadIndex)
        {
            DiskHelper.WriteId(writer, roadIndex.RoadId);
            writer.Write(roadIndex.IndexAlongRoad);
        }

        public static RoadIndex<TRoadId> Read<TRoadId>(BinaryReader reader)
        {
            TRoadId id = DiskHelper.ReadId<TRoadId>(reader);
            var along = reader.ReadUInt16();

            return new RoadIndex<TRoadId>(id, along);
        }

    }
}