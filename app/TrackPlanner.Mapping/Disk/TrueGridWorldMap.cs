﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Geo;
using MathUnit;
using TrackPlanner.Elevation;
using TrackPlanner.Structures;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Storage;
using TNodeId = TrackPlanner.Shared.Data.WorldIdentifier;
using TRoadId = TrackPlanner.Shared.Data.WorldIdentifier;

namespace TrackPlanner.Mapping.Disk
{
    internal sealed class TrueGridWorldMap : IWorldMap<TNodeId, TRoadId>
    {
        public static TrueGridWorldMap Create(ILogger logger,
            WorldMapMemory source,
            IElevationMap elevation,
            IdTranslationTable nodesTranslation,
            IdTranslationTable roadsTranslation,
            string? debugDirectory)
        {
            // ---- building translation tables
            {
                double start = Stopwatch.GetTimestamp();
                foreach (var (src_node_id, src_node_info) in source.GetAllNodes())
                {
                    var cell_idx = source.Grid.GetCellIndex(src_node_info.Point.Latitude, 
                        src_node_info.Point.Longitude);
                    nodesTranslation.AddOrGet(src_node_id, cell_idx);
                }

                logger.Info($"Translated nodes in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
            }

            {
                double start = Stopwatch.GetTimestamp();
                foreach (var (src_road_id, road_info) in source.GetAllRoads())
                {
                    var pt = source.GetPoint(road_info.Nodes.First());
                    var cell_idx = source.Grid.GetCellIndex(pt.Latitude, pt.Longitude);
                    roadsTranslation.AddOrGet(src_road_id, cell_idx);
                }

                logger.Info($"Translated roads in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
            }

            // --- adding info

            var cells = HashMap.Create<CellIndex, TrueGridCell>();

            {
                double start = Stopwatch.GetTimestamp();
                foreach (var (src_node_id, src_node_info) in source.GetAllNodes())
                {
                    var world_id = nodesTranslation.Get(src_node_id);
                    if (!cells.TryGetValue(world_id.CellIndex, out var cell))
                    {
                        cell = new TrueGridCell(world_id.CellIndex);
                        cells.Add(world_id.CellIndex, cell);
                    }

                    var node_info = new NodeInfo<TRoadId>(src_node_info.Point,
                        convertRoadIndices(src_node_info.Roads, roadsTranslation).ToHashSet(), 
                        src_node_info.IsRoundaboutCenter);
                    var osm_danger_nearby = source.GetBikeFootRoadExpressNearby(src_node_id);
                    WorldIdentifier? danger_nearby_id = null;
                    if (osm_danger_nearby is { } d)
                    {
                        if (roadsTranslation.TryGetFromOsm(d, out var id))
                            danger_nearby_id = id;
                        else
                            throw new ArgumentException($"Node {src_node_id} at {src_node_info} references road {d} which is not included.");
                    }

                    cell.AddNode(world_id.EntityIndex, node_info, danger_nearby_id);
                }

                logger.Info($"Added nodes in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
            }

            {
                double start = Stopwatch.GetTimestamp();
                var missing_coords = new HashSet<GeoPoint>();
                foreach (var cell in cells.Values)
                {
                    cell.UpdateNodes(elevation, missing_coords);
                }

                logger.Info($"Updated elevation in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");

                if (missing_coords.Any())
                    throw new ArgumentException($"Cannot get elevation for {(String.Join("; ", missing_coords))}");
            }

            {
                double start = Stopwatch.GetTimestamp();
                foreach (var (src_road_id, road_info) in source.GetAllRoads())
                {
                    var world_id = roadsTranslation.Get(src_road_id);
                    if (!cells.TryGetValue(world_id.CellIndex, out var cell))
                    {
                        cell = new TrueGridCell(world_id.CellIndex);
                        cells.Add(world_id.CellIndex, cell);
                    }

                    cell.AddRoad(world_id.EntityIndex, MapTranslator.ConvertRoadInfo(road_info, nodesTranslation.Get));
                }

                logger.Info($"Added roads in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
            }

            {
                // now we add road segments, so when checking cell X we would know which roads
                // are intersects it even if none of the nodes of the road lie there

                double start = Stopwatch.GetTimestamp();
                foreach (var (cell_idx, source_cell) in source.Grid.Cells)
                {
                    if (!cells.TryGetValue(cell_idx, out var cell))
                    {
                        cell = new TrueGridCell(cell_idx);
                        cells.Add(cell_idx, cell);
                    }

                    foreach (var road_seg in convertRoadIndices(source_cell.RoadSegments, roadsTranslation))
                    {
                        cell.AddSegment(road_seg);
                    }
                }

                logger.Info($"Added road segments in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
            }

            {
                double start = Stopwatch.GetTimestamp();
                foreach (var (node_id, attraction) in source.Attractions)
                {
                    var world_id = nodesTranslation.Get(node_id);
                    var cell = cells[world_id.CellIndex];
                    cell.AddAttraction(world_id, attraction);
                }

                logger.Info($"Added attractions in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
            }

            {
                double start = Stopwatch.GetTimestamp();
                foreach (var (node_id, info) in source.Cities)
                {
                    var world_id = nodesTranslation.Get(node_id);
                    var cell = cells[world_id.CellIndex];
                    cell.AddCity(world_id, info);
                }

                logger.Info($"Added cities in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
            }

            return new TrueGridWorldMap(logger,
                source.Northmost,
                source.Eastmost,
                source.Southmost,
                source.Westmost,
                nodesTranslation,
                roadsTranslation,
                cells,
                source.Grid.CellSize, debugDirectory);
        }

        private static IEnumerable<RoadIndex<TRoadId>> convertRoadIndices(IEnumerable<RoadIndex<OsmId>> osmRoadIndices,
            IdTranslationTable roadsTable)
        {
            return osmRoadIndices.Select(it => new RoadIndex<WorldIdentifier>(roadsTable.Get(it.RoadId),
                it.IndexAlongRoad));
        }

        private readonly ILogger logger;
        private readonly IdTranslationTable? nodesTranslation;
        private readonly IdTranslationTable? roadsTranslation;
        private readonly IReadOnlyEnumerableDictionary<CellIndex, TrueGridCell> cells;

        private readonly RoadGridDisk<TrueGridCell> grid;

        public Angle Eastmost { get; }
        public Angle Northmost { get; }
        public Angle Southmost { get; }

        public Angle Westmost { get; }

        //public Dictionary<WorldIdentifier, RoadExtra>? RoadExtras { get; set; }
        public IGrid<TNodeId, TRoadId> Grid => grid;

        public TrueGridWorldMap(ILogger logger,
            Angle northmost,
            Angle eastmost,
            Angle southmost,
            Angle westmost,
            IdTranslationTable? nodesTranslation,
            IdTranslationTable? roadsTranslation,
            IReadOnlyEnumerableDictionary<CellIndex, TrueGridCell> cells,
            int gridCellSize, string? debugDirectory)
        {
            this.logger = logger;
            this.nodesTranslation = nodesTranslation;
            this.roadsTranslation = roadsTranslation;
            this.cells = cells;
            Southmost = southmost;
            Northmost = northmost;
            Eastmost = eastmost;
            Westmost = westmost;


            var calc = new ApproximateCalculator();

            this.grid = new RoadGridDisk<TrueGridCell>(logger, cells, this, calc,
                gridCellSize, debugDirectory);
        }

        internal static IDisposable Read(ILogger logger, IReadOnlyList<string> fileNames, MemorySettings memSettings,
            IdTranslationTable? nodes_table,
            IdTranslationTable? roads_table,
            string? debugDirectory,
            out TrueGridWorldMap map, out List<string> invalidFiles)
        {
            var result = DiskHelper.FilenamesToStreams(fileNames, out var files);

            result.Stack(ReadMap(logger, files, memSettings,
                nodes_table, roads_table,
                debugDirectory, out map, out invalidFiles));
            return result;
        }

        internal static IDisposable ReadMap(ILogger logger,
            IReadOnlyList<(Stream stream, string name)> files,
            MemorySettings memSettings,
            IdTranslationTable? nodesTranslationTable, IdTranslationTable? roadsTranslationTable,
            string? debugDirectory,
            out TrueGridWorldMap map,
            out List<string> invalidFiles)
        {
            invalidFiles = new List<string>();

            Angle northmost = -Angle.PI;
            Angle eastmost = -Angle.PI;
            Angle southmost = Angle.PI;
            Angle westmost = Angle.PI;

            int? cell_size = null;

            var offsets = new List<ReaderOffsets<CellIndex>>();
            var disposables = new List<IDisposable>();

            foreach (var (stream, fn) in files)
            {
                stream.Position = 0;
                logger.Verbose($"Loading raw map from {fn}");

                var reader = new BinaryReader(stream, Encoding.UTF8,
                    leaveOpen: true);
                var format = reader.ReadInt32();
                if (format != StorageInfo.DataFormatVersion)
                {
                    invalidFiles.Add(fn);
                    logger.Warning($"Format {format} not supported, expected {StorageInfo.DataFormatVersion}");
                    reader.Dispose();
                    continue;
                }

                int cs = reader.ReadInt32();
                if (cell_size == null)
                    cell_size = cs;
                else if (cell_size != cs)
                {
                    invalidFiles.Add(fn);
                    logger.Warning($"Invalid cell size {cs}, expected {cell_size}");
                    reader.Dispose();
                    continue;
                }

                northmost = northmost.Max(GeoZPoint.ReadRawAngle(reader));
                eastmost = eastmost.Max(GeoZPoint.ReadRawAngle(reader));
                southmost = southmost.Min(GeoZPoint.ReadRawAngle(reader));
                westmost = westmost.Min(GeoZPoint.ReadRawAngle(reader));

                ReaderOffsets<CellIndex> reader_offsets = ReaderOffsets.Read(reader, CellIndexExtension.Read);
                logger.Info($"Cells count {reader_offsets.Count}");
                offsets.Add(reader_offsets);
                disposables.Add(reader);
            }

            var cells = DiskDictionary.Create(offsets, extraOffset: 0,
                TrueGridCell.Load, _ => { }, memSettings.WorldCacheCellsLimit);
            map = new TrueGridWorldMap(logger, northmost, eastmost, southmost, westmost,
                nodesTranslationTable, roadsTranslationTable, cells, cell_size!.Value, debugDirectory);

            return CompositeDisposable.Create(disposables);
        }

        internal void WriteMap(Stream stream)
        {
            using (var writer = new BinaryWriter(stream, Encoding.UTF8, leaveOpen: true))
            {
                writer.Write(StorageInfo.DataFormatVersion);
                writer.Write(Grid.CellSize);

                GeoZPoint.WriteRawAngle(writer, this.Northmost);
                GeoZPoint.WriteRawAngle(writer, this.Eastmost);
                GeoZPoint.WriteRawAngle(writer, this.Southmost);
                GeoZPoint.WriteRawAngle(writer, this.Westmost);

                grid.Write(writer);
                this.logger.Info($"Cells stats: {this.grid.GetStats()}");
            }
        }

        internal static void WriteTranslationTable(Stream stream, IdTranslationTable translation)
        {
            using (var writer = new BinaryWriter(stream, Encoding.UTF8, leaveOpen: true))
            {
                writer.Write(StorageInfo.DataFormatVersion);

                translation.Write(writer);
            }
        }

        internal static IdTranslationTable ReadIdTranslationTable(Stream stream)
        {
            using (var reader = new BinaryReader(stream, Encoding.UTF8, leaveOpen: true))
            {
                var format = reader.ReadInt32();
                if (format != StorageInfo.DataFormatVersion)
                    throw new NotSupportedException($"Format {format} not supported, expected {StorageInfo.DataFormatVersion}");

                return IdTranslationTable.Read(reader);
            }
        }

        public GeoZPoint GetPoint(TNodeId nodeId)
        {
            var cell = this.cells[nodeId.CellIndex];
            return cell.GetPoint(nodeId.EntityIndex);
        }

        public Result<RoadInfo<TNodeId>> TryGetRoadInfo(TRoadId roadId)
        {
            if (this.cells.TryGetValue(roadId.CellIndex, out var cell))
                return Result<RoadInfo<TNodeId>>.Valid(cell.GetRoadInfo(roadId.EntityIndex));
            else
                return Result<RoadInfo<TNodeId>>.Fail($"Cell {roadId.CellIndex} not found");
        }
        
        public Result<NodeInfo<TRoadId>> TryGetNodeInfo(TNodeId nodeId)
        {
            if (this.cells.TryGetValue(nodeId.CellIndex, out var cell))
                return Result<NodeInfo<TRoadId>>.Valid(cell.GetNodeInfo(nodeId.EntityIndex));
            else
                return Result<NodeInfo<TRoadId>>.Fail($"Cell {nodeId.CellIndex} not found");
        }


        public IEnumerable<KeyValuePair<TNodeId, NodeInfo<TRoadId>>> GetAllNodes()
        {
            return this.cells
                .Select(it => it.Value)
                .SelectMany(cell => cell.Nodes
                    .Select(it =>  KeyValuePair.Create(cell.GetWorldIdentifier(it.nodeIndex),it.info)));
        }

        public IReadOnlySet<RoadIndex<TRoadId>> GetRoadsAtNode(TNodeId nodeId)
        {
            var cell = this.cells[nodeId.CellIndex];
            return cell.GetRoads(nodeId.EntityIndex);
        }


        public OsmId GetOsmRoadId(TRoadId roadId)
        {
            return this.roadsTranslation?.GetOsm(roadId) ?? OsmId.Invalid;
        }

        public OsmId GetOsmNodeId(TNodeId nodeId)
        {
            return this.nodesTranslation?.GetOsm(nodeId) ?? OsmId.Invalid;
        }

        public IEnumerable<PointOfInterest> GetAttractions(WorldIdentifier nodeId)
        {
            return this.cells[nodeId.CellIndex].NodeAttractions.Where(it => it.nodeIndex == nodeId.EntityIndex).Select(it => it.attraction);
        }

        public IEnumerable<(WorldIdentifier, GeoPoint, PointOfInterest)> GetAttractionsWithin(Region region,
            PointOfInterest.Feature excludeFeatures)
        {
            foreach (var cell_idx in this.grid.EnumerateCellIndices(region))
            {
                if (!this.cells.TryGetValue(cell_idx, out var cell))
                    continue;

                foreach (var (node_idx, attr) in cell.NodeAttractions)
                {
                    // if current feature has any of excluded skip this spot
                    if ((attr.Features & excludeFeatures) != 0) 
                        continue;

                    var world_id = new WorldIdentifier(cell_idx, node_idx);
                    var pt = this.GetPoint(world_id).Convert2d();
                    if (region.IsWithin(pt))
                        yield return (world_id, pt, attr);
                }
            }
        }

        public IEnumerable<(WorldIdentifier, GeoPoint, CityInfo)> GetCitiesWithin(Region region)
        {
            foreach (var cell_idx in this.grid.EnumerateCellIndices(region))
            {
                if (!this.cells.TryGetValue(cell_idx, out var cell))
                    continue;

                foreach (var (node_idx, city) in cell.Cities)
                {
                    var world_id = new WorldIdentifier(cell_idx, node_idx);
                    var pt = this.GetPoint(world_id).Convert2d();
                    if (region.IsWithin(pt))
                        yield return (world_id, pt, city);
                }
            }
        }

        public IEnumerable<(WorldIdentifier, NodeInfo<WorldIdentifier>)> GetNodesWithin(Region region)
        {
            foreach (var cell_idx in this.grid.EnumerateCellIndices(region))
            {
                if (!this.cells.TryGetValue(cell_idx, out var cell))
                    continue;

                foreach (var (node_idx, node_info) in cell.Nodes)
                {
                    var world_id = new WorldIdentifier(cell_idx, node_idx);
                    var pt = this.GetPoint(world_id).Convert2d();
                    if (region.IsWithin(pt))
                        yield return (world_id, node_info);
                }
            }
        }

        public WorldIdentifier FromOsmNodeId(OsmId osmNodeId)
        {
            return this.nodesTranslation?.Get(osmNodeId) ?? default;
        }

        public WorldIdentifier FromOsmRoadId(OsmId osmRoadId)
        {
            return this.roadsTranslation?.Get(osmRoadId) ?? default;
        }

        public TRoadId? GetBikeFootRoadExpressNearby(TNodeId nodeId)
        {
            var cell = this.cells[nodeId.CellIndex];
            return cell.GetBikeFootRoadExpressNearby(nodeId.EntityIndex);
        }

        public string GetStats()
        {
            return this.cells.ToString() ?? "";
        }


        public IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetAllRoads()
        {
            return this.grid.GetAllRoads();
        }
    }
}