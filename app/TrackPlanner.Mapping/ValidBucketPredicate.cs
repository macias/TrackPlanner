﻿using TrackPlanner.Mapping.Data;

namespace TrackPlanner.Mapping
{
    public delegate bool ValidBucketPredicate<TNodeId, TRoadId>(RoadInfo<TNodeId> roadInfo)
        where TNodeId : struct
        where TRoadId : struct;

}