﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using Geo;
using TrackPlanner.Structures;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class RoadBucket
    {
        public static List<RoadBucket<TNodeId,TRoadId>> GetRoadBuckets<TNodeId,TRoadId>(IReadOnlyList<TNodeId> mapNodes,
            IWorldMap<TNodeId,TRoadId> map, IGeoCalculator calc,
            bool allowSmoothing,bool allowGap)
            where TNodeId:struct
            where TRoadId:struct
        {
            return mapNodes.ZipIndex().Select(it =>
            {
                return CreateBucket(it.index,it.item,map,predicate:null, calc, 
                    allowSmoothing:allowSmoothing,allowGap:allowGap);
            }).ToList();
        }

        public static RoadBucket<TNodeId,TRoadId> CreateBucket<TNodeId,TRoadId>(int index,
            TNodeId nodeId,
            IWorldMap<TNodeId,TRoadId> map,
            ValidBucketPredicate<TNodeId,TRoadId>? predicate,
            IGeoCalculator calc,
            bool allowSmoothing,
            bool allowGap)
        where TNodeId:struct
            where TRoadId:struct
        {
            var node_point = map.GetPoint(nodeId);
            RoadSnapInfo<TNodeId,TRoadId>[] snaps = map.GetRoadsAtNode(nodeId)
                .Where(it => predicate?.Invoke(map.GetRoadInfo(it.RoadId))??true)
                .Select(
                    // todo: maybe it would be more appropriate if we pass node in such case,
                    // and not road info
                    // because we cannot associate one road with single node 
                    idx => new RoadSnapInfo<TNodeId,TRoadId>(
                        #if DEBUG
                        MapRef.Create(map,idx).Value,
                        #endif
                        idx ,
                        // there is no ending node in case when we know node in advance,
                        // so we use the same index again
                        snapKind:  SnapKind.Node, 
                        trackSnapDistance: Length.Zero, 
                        trackCrosspoint: node_point))
                .ToArray();
            
            return new RoadBucket<TNodeId,TRoadId>(DEBUG_trackIndex: index, map, 
                userNodeId:nodeId, node_point.Convert2d(),
                calc, snaps, 
                reachableNodes: new HashSet<TNodeId>(),Length.Zero,  
                allowSmoothing,allowGap:allowGap);
        }
    }
    
    public sealed class RoadBucket<TNodeId,TRoadId> : IRequestPoint
    where TNodeId:struct
    where TRoadId : struct
    {
        private readonly IWorldMap<TNodeId,TRoadId> map;
        private readonly IGeoCalculator calc;

        // IMPORTANT: for given OSM node this type does not keep all roads (you have to fetch them from the map)
        // this is because we get the nodes by hitting segments in nearby, if some segment is too far
        // we won't register it

        private readonly IReadOnlyList<RoadSnapInfo<TNodeId,TRoadId>> snaps;

        public IEnumerable<RoadSnapInfo<TNodeId,TRoadId>> PrimarySnaps => this.snaps
            .Where(it => it.TrackSnapDistance<=this.primarySnapRange);
        public IEnumerable<RoadSnapInfo<TNodeId,TRoadId>> ActiveSnaps => getSnaps(this.expanded);

        public IEnumerable<RoadSnapInfo<TNodeId,TRoadId>> AllSnaps => this.snaps;
       
        public bool IsExpandable => !this.expanded && getSnaps(expanded:true).Any();

        public int DEBUG_TrackIndex { get; }
        public GeoPoint UserPoint { get; }
        public TNodeId? UserNodeId { get; }
        public IReadOnlySet<TNodeId> ReachableNodes { get; }
        private readonly Length primarySnapRange;
        public bool AllowSmoothing { get; }
        public bool AllowGap { get; }
        private bool expanded;

        public RoadBucket(int DEBUG_trackIndex, IWorldMap<TNodeId,TRoadId> map, TNodeId? userNodeId, 
            GeoPoint userPoint,
            IGeoCalculator calculator,
            IReadOnlyList<RoadSnapInfo<TNodeId,TRoadId>> snaps,
            IReadOnlySet<TNodeId> reachableNodes,
            Length primarySnapRange,  
            bool allowSmoothing,bool allowGap)
        {
            DEBUG_TrackIndex = DEBUG_trackIndex;
            this.map = map;
            this.UserNodeId = userNodeId;
            this.UserPoint = userPoint;
            ReachableNodes = reachableNodes;
            this.primarySnapRange = primarySnapRange;
            this.calc = calculator;
            AllowSmoothing = allowSmoothing;
            AllowGap = allowGap;
            this.snaps = snaps;
        }

        public void Expand()
        {
            this.expanded = true;
        }


        private IEnumerable<RoadSnapInfo<TNodeId, TRoadId>> getSnaps(bool expanded)
        {
            return this.snaps
                .Where(it =>  (it.TrackSnapDistance>this.primarySnapRange)==expanded);
        }


        public ContextMessage? TryRebuildWithSnap(ILogger logger, GeoPoint snapCrosspoint,
            RoadIndex<TRoadId> snapBaseIndex, RoadIndex<TRoadId>? snapNextIndex,out RoadBucket<TNodeId,TRoadId>? bucket)
        {
            // it could be the case we have several snaps at the same coords, it is when
            // we hit exactly some node, then crosspoint is exactly the node coords
            // and we have as many road hits as there are at this node
            var snaps = this.snaps
                .Where(it => it.Matches(snapCrosspoint, snapBaseIndex, snapNextIndex))
                .ToArray();

            if (!snaps.Any())
            {
                bucket = null;
                
                var short_message = $"Cannot find single entry {snapBaseIndex}, cx {snapCrosspoint} in the bucket";
                logger.Error(short_message
                                   + Environment.NewLine
                                   + String.Join(Environment.NewLine, this.snaps.Select(it => $"node {this.map.GetNode(it.BaseRoadIndex)}, cx {it.TrackCrosspoint}")));
                return new ContextMessage(short_message, MessageLevel.Error);
            }

            bucket = new RoadBucket<TNodeId,TRoadId>(DEBUG_TrackIndex,this.map,this.UserNodeId,UserPoint,calc,
                snaps,ReachableNodes,this.primarySnapRange, 
                AllowSmoothing,AllowGap);
            return null;
        }

     
        public string OsmString()
        {
            return String.Join(Environment.NewLine, new[] {OsmOrigin()}.Concat(snaps.Select(it => it.OsmString(map)))); 
        }
        public override string ToString()
        {
            if (this.UserNodeId == null)
                return this.UserPoint.ToString();
            else
                return $"#{this.UserNodeId}({UserPoint})";
        }

        public string OsmOrigin()
        {
            if (this.UserNodeId == null)
                return this.UserPoint.ToString();
            else
                return $"#{this.map.GetOsmNodeId(this.UserNodeId.Value)}({UserPoint})";
        }
    }
}
