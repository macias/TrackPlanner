﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using TrackPlanner.Elevation;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Mapping
{
    // elevation
    // https://en.wikipedia.org/wiki/Shuttle_Radar_Topography_Mission

    // https://wiki.openstreetmap.org/wiki/Map_Features
    public sealed partial class MapReader
    {
        private readonly ILogger logger;
        private readonly IGeoCalculator calc;
        private readonly MemorySettings memSettings;
        private readonly MapSettings mapSettings;
        private readonly string? debugDirectory;

        public MapReader(ILogger logger, IGeoCalculator calc, MemorySettings memSettings,
            MapSettings mapSettings, string? debugDirectory)
        {
            this.logger = logger;
            this.calc = calc;
            this.memSettings = memSettings;
            this.mapSettings = mapSettings;
            this.debugDirectory = debugDirectory;
        }

        public IDisposable? ReadMap<TNodeId, TRoadId>(
            string elevationDirectory,
            string osmDirectory, 
            string worldMapDirectory, 
            bool onlyRoads,
            out IWorldMap<TNodeId, TRoadId> map)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            System.IO.Directory.CreateDirectory(worldMapDirectory);
            
            string[] osm_files;
           // if (System.IO.File.Exists(osmDirectory))
            //{
//                osm_files = new[] {osmDirectory};
  //          }
    //        else
            {
                osm_files = System.IO.Directory.GetFiles(osmDirectory, "*.osm.pbf")
                    .OrderBy(it => it.ToLowerInvariant())
                    .ToArray();
            }

            var road_names = new Dictionary<string, int>();

            long? timestamp = null;

            if (!onlyRoads)
            {
                if (this.memSettings.GetMapMode() != MapMode.MemoryOnly)
                    throw new ArgumentException();

                var temp_map = OsmReader.ReadActualOsmMap(
                     logger,  calc,  memSettings,
                     debugDirectory,  
                     osm_files, road_names, onlyRoads);
                map = HACK.CastMap<TNodeId, TRoadId>(temp_map);
                var temp_grid = new RoadGridMemory(logger,
                    new RoadGridMemoryBuilder(logger, temp_map, new ApproximateCalculator(), this.memSettings.GridCellSize, debugDirectory).BuildCells(),
                    temp_map, new ApproximateCalculator(), this.memSettings.GridCellSize, debugDirectory);
                temp_map.ProcessRoadAssociations(temp_grid, this.mapSettings);
                return CompositeDisposable.None;
            }

            var mem_conversions = new List<string>(capacity: osm_files.Length);


            var nodes_translation_filename = System.IO.Path.Combine(worldMapDirectory,
                $"nodes_trans_{StorageInfo.DataFormatVersion}_{this.memSettings.GridCellSize}.ttb");
            var roads_translation_filename = System.IO.Path.Combine(worldMapDirectory,
                $"roads_trans_{StorageInfo.DataFormatVersion}_{this.memSettings.GridCellSize}.ttb");

            IdTranslationTable? node_translations = null;
            IdTranslationTable? road_translations = null;
            int node_trans_count = 0;
            int road_trans_count = 0;
            ElevationMap elevation_map = new ElevationMap(elevationDirectory,
                this.memSettings.ElevationCacheCellsLimit);


            foreach (string file_path in osm_files)
            {
                string get_custom_map_path(string fn)
                {
                    // removing double extension
                    fn = System.IO.Path.ChangeExtension(System.IO.Path.ChangeExtension(fn, null), null);
                    fn = System.IO.Path.GetFileName(fn);
                    return System.IO.Path.Combine(worldMapDirectory, fn);
                }

                string mem_conv_path = get_custom_map_path(file_path)
                                       + $"xtr_{StorageInfo.DataFormatVersion}";
                string disk_conv_path = get_custom_map_path(file_path)
                                        + $"_{StorageInfo.DataFormatVersion}_{this.memSettings.GridCellSize}.gm";

                mem_conversions.Add(mem_conv_path);

                WorldMapMemory? mem_map = null;
                if (!DevelModes.True && !System.IO.File.Exists(mem_conv_path))
                {
                    mem_map = OsmReader.ReadActualOsmMap(
                            logger,  calc,  memSettings,
                            debugDirectory, 
                            new[] {file_path}, road_names, onlyRoads);

                    var temp_grid = new RoadGridMemory(logger,
                        new RoadGridMemoryBuilder(logger, mem_map, new ApproximateCalculator(), this.memSettings.GridCellSize, debugDirectory).BuildCells(),
                        mem_map, new ApproximateCalculator(), this.memSettings.GridCellSize, debugDirectory);
                    mem_map.ProcessRoadAssociations(temp_grid, this.mapSettings);

                    timestamp ??= DateTimeOffset.UtcNow.ToUnixTimeSeconds();

                    using (var mem = new MemoryStream())
                    {
                        mem_map.Write(timestamp.Value, mem);
                        mem.Position = 0;
                        System.IO.File.WriteAllBytes(mem_conv_path, mem.ToArray());
                    }
                }

                if (this.memSettings.GetMapMode() == MapMode.TrueDisk
                    && !System.IO.File.Exists(disk_conv_path))
                {
                    if (node_translations == null)
                    {
                        loadTranslationTables<TNodeId, TRoadId>(nodes_translation_filename,
                            roads_translation_filename,
                            ref node_translations, ref road_translations);
                        if (node_translations != null)
                        {
                            node_trans_count = node_translations!.Count;
                            road_trans_count = road_translations!.Count;
                        }
                    }

                    if (!DevelModes.True && mem_map == null)
                    {
                        mem_map = WorldMapMemory.ReadRawArray(logger, mem_conversions,
                            this.memSettings.GridCellSize, debugDirectory!,
                            out var invalid_files);
                        if (invalid_files.Any())
                            throw new NotSupportedException();
                    }

                    if (mem_map == null)
                    {
                        elevation_map.ThrowIfEmpty();

                        mem_map = OsmReader.ReadActualOsmMap(
                            logger,  calc,  memSettings,
                            debugDirectory, 
                            new[] {file_path}, road_names, onlyRoads);

                        var temp_grid = new RoadGridMemory(logger,
                            new RoadGridMemoryBuilder(logger, mem_map, new ApproximateCalculator(), 
                                this.memSettings.GridCellSize, debugDirectory).BuildCells(),
                            mem_map, new ApproximateCalculator(), this.memSettings.GridCellSize, debugDirectory);
                        mem_map.ProcessRoadAssociations(temp_grid, this.mapSettings);
                    }

                    timestamp ??= DateTimeOffset.UtcNow.ToUnixTimeSeconds();


                    if (node_translations == null)
                    {
                        node_translations = new IdTranslationTable(capacity: mem_map.GetAllNodes().Count());
                        road_translations = new IdTranslationTable(capacity: mem_map.GetAllRoads().Count());
                    }

                    var true_map = TrueGridWorldMap.Create(this.logger, mem_map, elevation_map,
                        node_translations, road_translations!,
                        this.debugDirectory);
                    using (var mem = new MemoryStream())
                    {
                        true_map.WriteMap(mem);
                        mem.Position = 0;
                        System.IO.File.WriteAllBytes(disk_conv_path, mem.ToArray());
                    }
                }
            }

            bool map_changed = false;
            
            if (node_trans_count != (node_translations?.Count ?? 0))
            {
                map_changed = true;
                using (var mem = new MemoryStream())
                {
                    TrueGridWorldMap.WriteTranslationTable(mem, node_translations!);
                    mem.Position = 0;
                    System.IO.File.WriteAllBytes(nodes_translation_filename, mem.ToArray());
                }
            }

            if (road_trans_count != (road_translations?.Count ?? 0))
            {
                map_changed = true;
                using (var mem = new MemoryStream())
                {
                    TrueGridWorldMap.WriteTranslationTable(mem, road_translations!);
                    mem.Position = 0;
                    System.IO.File.WriteAllBytes(roads_translation_filename, mem.ToArray());
                }
            }


            if (this.memSettings.GetMapMode() == MapMode.MemoryOnly)
            {
                double start = Stopwatch.GetTimestamp();
                //var temp_map = OsmMapMemory.ReadMappedArray(logger, extracts);
                var temp_map = WorldMapMemory.ReadRawArray(logger, mem_conversions,
                    this.memSettings.GridCellSize, debugDirectory!,
                    out var invalid_files);
                if (invalid_files.Any())
                    throw new NotSupportedException();
                map = HACK.CastMap<TNodeId, TRoadId>(temp_map);
                logger.Info($"Loaded MEM in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");

                return null;
            }
            else if (this.memSettings.GetMapMode() == MapMode.TrueDisk)
            {
                if (this.memSettings.GetEnableOsmId())
                    loadTranslationTables<TNodeId, TRoadId>(nodes_translation_filename, roads_translation_filename,
                        ref node_translations, ref road_translations);

                double start = Stopwatch.GetTimestamp();
                var disp = TrueGridWorldMap.Read(logger,
                    System.IO.Directory.GetFiles(worldMapDirectory, $"*{StorageInfo.DataFormatVersion}_{this.memSettings.GridCellSize}.gm"),
                    this.memSettings,
                    this.memSettings.GetEnableOsmId() ? node_translations : null,
                    this.memSettings.GetEnableOsmId() ? road_translations : null,
                    debugDirectory,
                    out var temp_map, out var invalid_files);
                if (invalid_files.Any())
                    throw new NotSupportedException();
                map = HACK.CastMap<TNodeId, TRoadId>(temp_map);
                logger.Info($"Loaded TRUE DISK in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency} s");

                if (map_changed)
                    map.Validate();
                return disp;
            }
            else
                throw new NotImplementedException($"{nameof(this.memSettings.GetMapMode)} {this.memSettings.GetMapMode}");
            
            //this.logger.Flush();
        }

        private static void loadTranslationTables<TNodeId, TRoadId>(string nodes_translation_filename,
            string roads_translation_filename,
            ref IdTranslationTable? nodeTranslations,
            ref IdTranslationTable? roadTranslations)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            if (System.IO.File.Exists(nodes_translation_filename))
            {
                using (var fs = new FileStream(nodes_translation_filename, FileMode.Open, FileAccess.Read))
                    nodeTranslations = TrueGridWorldMap.ReadIdTranslationTable(fs);
                using (var fs = new FileStream(roads_translation_filename, FileMode.Open, FileAccess.Read))
                    roadTranslations = TrueGridWorldMap.ReadIdTranslationTable(fs);
            }
        }


        



       

       
      
    }
}