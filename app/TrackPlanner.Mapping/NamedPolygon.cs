﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    internal readonly struct NamedPolygon
    {
        public IReadOnlyList<OsmId> Nodes { get; }
        public OsmId Id { get; }
        public string Name { get; }

        public NamedPolygon(OsmId id, string name, IReadOnlyList<OsmId> nodes)
        {
            Id =  id;
            Name = name;
            this.Nodes = nodes ?? throw new ArgumentNullException(nameof(nodes));
        }
        public NamedPolygon(long id, string name, IEnumerable<long> nodes) 
            : this(OsmId.PureOsm( id), name, nodes?.Select(it => OsmId.PureOsm(it)).ToArray()!)
        {
        }
    }
}