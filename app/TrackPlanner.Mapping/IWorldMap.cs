﻿using System;
using MathUnit;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Geo;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class MapRef
    {
        [return: NotNullIfNotNull(nameof(roadIndex))]
        public static  MapRef<TNodeId, TRoadId>? Create<TNodeId, TRoadId>(
            IWorldMap<TNodeId, TRoadId>? map,
            RoadIndex<TRoadId>? roadIndex)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (roadIndex is { } rdx)
            {
                if (map == null)
                    throw new ArgumentNullException($"Map cannot be null when translating nodes.");
                return new MapRef<TNodeId, TRoadId>(rdx,
                    map.GetOsmRoadId(rdx.RoadId),
                    map.GetOsmNodeId(map.GetNode(rdx)));
            }
            else
                return null;
        }
    }
    
    public readonly record struct RoadExtra(bool HasRedundantFlag)
    {
        
    }

    public static class WorldMapDefaultExtensions
    {
        public static bool IsNetworked<TNodeId>(this RoadInfo<TNodeId> info)
            where TNodeId:struct
        {
                // in order to check connectivity we would need to actually calculate it
                // but we can make a guess using only info about road type
                return info.Kind <=WayKind.Auxiliary 
                       || info.Kind== WayKind.Ferry
                       || info.IsRoundabout
                       || info.HasName;
        }

        public static RoadInfo<TNodeId> GetRoadInfo<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, TRoadId roadId)
            where TNodeId:struct
            where TRoadId : struct
        {
            Result<RoadInfo<TNodeId>> result = map.TryGetRoadInfo(roadId);
            if (result.HasValue)
                return result.Value;
            else
                throw new ArgumentException(result.ProblemMessage.EnsureFullMessage());
        }
        public static NodeInfo<TRoadId> GetNodeInfo<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, 
            TNodeId nodeId)
            where TNodeId:struct
            where TRoadId : struct
        {
            Result<NodeInfo<TRoadId>> result = map.TryGetNodeInfo(nodeId);
            if (result.HasValue)
                return result.Value;
            else
                throw new ArgumentException(result.ProblemMessage.EnsureFullMessage());
        }
    }

    public interface IWorldMap<TNodeId, TRoadId>
        where TNodeId:struct
    where TRoadId:struct
    {
        IGrid<TNodeId,TRoadId> Grid { get; }
        
        GeoZPoint GetPoint(TNodeId nodeId);
        IEnumerable<KeyValuePair<TNodeId, NodeInfo<TRoadId>>> GetAllNodes();

        Angle Eastmost { get; }
        Angle Northmost { get; }
        Angle Southmost { get; }
        Angle Westmost { get; }
        
        IReadOnlySet<RoadIndex<TRoadId>> GetRoadsAtNode(TNodeId nodeId);
        TRoadId? GetBikeFootRoadExpressNearby(TNodeId nodeId);

        string GetStats();
        IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetAllRoads();
        IEnumerable<PointOfInterest> GetAttractions(TNodeId nodeId);
        IEnumerable<(TNodeId,GeoPoint,PointOfInterest)> GetAttractionsWithin(Region region,
            PointOfInterest.Feature excludeFeatures);
        IEnumerable<(TNodeId,GeoPoint,CityInfo)> GetCitiesWithin(Region region);
        IEnumerable<(TNodeId nodeId,NodeInfo<TRoadId> nodeInfo)> GetNodesWithin(Region region);
        TNodeId FromOsmNodeId(OsmId osmNodeId);
        TRoadId FromOsmRoadId(OsmId osmRoadId);
        OsmId GetOsmRoadId(TRoadId roadId);
        OsmId GetOsmNodeId(TNodeId nodeId);
        Result<RoadInfo<TNodeId>> TryGetRoadInfo(TRoadId roadId);
        Result<NodeInfo<TRoadId>> TryGetNodeInfo(TNodeId nodeId);
    }
    
    
}