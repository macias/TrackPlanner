﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Geo;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Storage;
using TrackPlanner.Structures;
using TNodeId = TrackPlanner.Shared.Data.OsmId;
using TRoadId = TrackPlanner.Shared.Data.OsmId;

namespace TrackPlanner.Mapping
{
    public sealed class WorldMapMemory : IWorldMap<TNodeId, TRoadId>
    {
        public static WorldMapMemory CreateOnlyRoads(ILogger logger,
            IMap<TNodeId, NodeInfo<TRoadId>> nodes,
            IMap<TRoadId, RoadInfo<TNodeId>> roads,
           // NodeRoadsDictionary<TNodeId, TRoadId> backReferences,
            List<(TNodeId, PointOfInterest)> attractions,
            List<(TNodeId, CityInfo)> cities,
            int gridCellSize, string? debugDirectory)
        {
            return new WorldMapMemory(logger, nodes, roads,
               // backReferences,
                attractions,
                forests: null, rivers: null, cities, waters: null, protectedArea: null,
                noZone: null, railways: null, gridCellSize, debugDirectory, onlyRoads: true);
        }

        private readonly IReadOnlyList<NodeLine<TNodeId>>? _protected;
        private readonly IReadOnlyList<NodeLine<TNodeId>>? forests;

       // private readonly NodeRoadsDictionary<TNodeId, TRoadId> nodeRoadReferences;
        private readonly IReadOnlyList<NodeLine<TNodeId>>? noZone;
        private readonly ILogger logger;
        private readonly bool onlyRoads;

        private readonly IReadOnlyList<NodeLine<TNodeId>>? railways;
        private readonly IReadOnlyList<(RiverKind kind, NodeLine<TNodeId> indices)>? rivers;
        private readonly IReadOnlyList<NodeLine<TNodeId>>? waters;

        private IReadOnlyDictionary<TNodeId,TRoadId>? bikeFootExpressNearbyNodes;
        private RoadGridMemory grid;
        public IGrid<TNodeId, TRoadId> Grid => this.grid;
        public IReadOnlyList<(TNodeId nodeId, PointOfInterest attraction)> Attractions { get; set; }

        private IReadOnlyMap<TNodeId, NodeInfo<TRoadId>> nodes { get; }
        private IMap<TRoadId, RoadInfo<TNodeId>> roads { get; }
        private IReadOnlyList<NodeLine<TNodeId>> Railways => this.railways ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        private IReadOnlyList<NodeLine<TNodeId>> Forests => this.forests ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        private IReadOnlyList<(RiverKind kind, NodeLine<TNodeId> indices)> Rivers => this.rivers ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        public IReadOnlyList<(TNodeId nodeId,CityInfo info)> Cities { get; }
        private IReadOnlyList<NodeLine<TNodeId>> Waters => this.waters ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        private IReadOnlyList<NodeLine<TNodeId>> Protected => this._protected ?? throw new InvalidOperationException("Map was loaded only with roads info.");

        private IReadOnlyList<NodeLine<TNodeId>> NoZone => this.noZone ?? throw new InvalidOperationException("Map was loaded only with roads info.");
        public Angle Southmost { get; }
        public Angle Northmost { get; }
        public Angle Eastmost { get; }
        public Angle Westmost { get; }
       // public Dictionary<long, RoadExtra>? RoadExtras { get; set; }

        public WorldMapMemory(ILogger logger,
            IMap<TNodeId, NodeInfo<TRoadId>> nodes,
            IMap<TRoadId, RoadInfo<TNodeId>> roads,
          //  NodeRoadsDictionary<TNodeId, TRoadId> backReferences,
            IReadOnlyList<(TNodeId nodeId, PointOfInterest attraction)> attractions,
            List<NodeLine<TNodeId>>? forests,
            List<(RiverKind kind, NodeLine<TNodeId> indices)>? rivers,
            List<(TNodeId,CityInfo)> cities,
            List<NodeLine<TNodeId>>? waters,
            List<NodeLine<TNodeId>>? protectedArea,
            List<NodeLine<TNodeId>>? noZone,
            List<NodeLine<TNodeId>>? railways,
            int gridCellSize, string? debugDirectory,
            bool onlyRoads)
        {
            this.nodes = nodes;
            this.logger = logger;
            this.onlyRoads = onlyRoads;

            logger.Verbose($"Creating map with {roads.Count} roads");

            this.roads = roads;
            Attractions = attractions;
            this.Cities = cities;
            if (!onlyRoads)
            {
                this.forests = forests;
                this.rivers = rivers;
                this.waters = waters;
                this._protected = protectedArea;
                this.noZone = noZone;
                this.railways = railways;
            }

            validate(this.roads.SelectMany(it => it.Value.Nodes));
            validate(cities.Select(it => it.Item1));
            if (!onlyRoads)
            {
                validate(forests!.SelectMany(it => it.Nodes));
                validate(rivers!.SelectMany(it => it.indices.Nodes));
                validate(waters!.SelectMany(it => it.Nodes));
                validate(railways!.SelectMany(it => it.Nodes));
                validate(protectedArea!.SelectMany(it => it.Nodes));
                validate(noZone!.SelectMany(it => it.Nodes));
            }

            Southmost = this.nodes.Min(n => n.Value.Point.Latitude);
            Northmost = this.nodes.Max(n => n.Value.Point.Latitude);
            Eastmost = this.nodes.Max(n => n.Value.Point.Longitude);
            Westmost = this.nodes.Min(n => n.Value.Point.Longitude);

           // this.nodeRoadReferences = backReferences;

            {
                var calc = new ApproximateCalculator();
                this.grid = new RoadGridMemory(logger,
                    new RoadGridMemoryBuilder(logger, this, calc, gridCellSize, debugDirectory).BuildCells(),
                    this, calc, gridCellSize, debugDirectory);
            }
        }


        // note we can get even for the same road multiple indices, example case: roundabouts -- "start" and "end" are at the same point
        public IReadOnlySet<RoadIndex<TRoadId>> GetRoadsAtNode(TNodeId nodeId)
        {
            return this.nodes[nodeId].Roads;
        }

        public GeoZPoint GetPoint(TNodeId nodeId)
        {
            return this.nodes[nodeId].Point;
        }

        public IEnumerable<KeyValuePair<TNodeId, NodeInfo<TRoadId>>> GetAllNodes()
        {
            return nodes;
        }

        public string GetStats()
        {
            return this.Grid.GetStats();
        }

        public void SetExpressNearby(IReadOnlyDictionary<TNodeId,TRoadId> expressNearbyNodes)
        {
            this.bikeFootExpressNearbyNodes = expressNearbyNodes;
        }

        public TRoadId? GetBikeFootRoadExpressNearby(TNodeId nodeId)
        {
            //return this.bikeFootDangerousNearbyNodes.TryGetValue(roadId, out var indices) && indices.Contains(nodeId);
            if (this.bikeFootExpressNearbyNodes!.TryGetValue(nodeId, out var road_id))
                return road_id;
            else
                return null;
        }

        public Result<RoadInfo<TNodeId>> TryGetRoadInfo(TRoadId roadId)
        {
            return Result<RoadInfo<TNodeId>>.Valid( roads[roadId]);
        }

        public Result<NodeInfo<TRoadId>> TryGetNodeInfo(TNodeId nodeId)
        {
            return Result<NodeInfo<TRoadId>>.Valid( nodes[nodeId]);
        }

        public IEnumerable<KeyValuePair<TRoadId, RoadInfo<TNodeId>>> GetAllRoads()
        {
            return roads;
        }

        private void validate(IEnumerable<TNodeId> nodeReferences)
        {
            foreach (var node_id in nodeReferences)
                if (!nodes.ContainsKey(node_id))
                    throw new KeyNotFoundException($"Cannot find reference node {node_id}");
        }

        internal void Write(long timestamp, Stream stream)
        {
            if (DevelModes.True)
            throw new NotSupportedException();
            
            if (!onlyRoads)
                throw new Exception("Can write only roads map");

            using (var writer = new BinaryWriter(stream, Encoding.UTF8, leaveOpen: true))
            {
                writer.Write(StorageInfo.DataFormatVersion);
                writer.Write(timestamp);
                writer.Write(Grid.CellSize);

                GeoZPoint.WriteRawAngle(writer, this.Northmost);
                GeoZPoint.WriteRawAngle(writer, this.Eastmost);
                GeoZPoint.WriteRawAngle(writer, this.Southmost);
                GeoZPoint.WriteRawAngle(writer, this.Westmost);

                Dictionary<TNodeId, long> node_offsets;

                using (new OffsetKeeper(writer)) // grid offset
                {
                    using (new OffsetKeeper(writer)) // roads offset
                    {
                        // ---- writing nodes -----------------------------

                        // we create array to make sure we have the same order while iterating in two loops (C# framework does not guarantee this) 
                        var map_node_ids = nodes.Keys.OrderBy(x => x).ToArray();

                        var writer_offsets = new WriterOffsets<TNodeId>(writer);

                        writer.Write(nodes.Count);
                        foreach (var node_id in map_node_ids)
                        {
                            node_id.Write(writer);
                            writer_offsets.Register(node_id);
                        }

                        foreach (var node_id in map_node_ids)
                        {
                            var debug_pos = writer_offsets.AddOffset(node_id);
                            var traffic_info = this.GetBikeFootRoadExpressNearby(node_id);
                            writer.Write(traffic_info.HasValue);
                            DiskHelper.WriteId(writer,new TRoadId());
                            nodes[node_id].Write(writer);
                        }

                        node_offsets = writer_offsets.GetOffsets();

                        writer_offsets.WriteBackOffsets();
                    }

                    // ---- writing roads -----------------------------------
                    {
                        var map_road_ids = roads.Keys.OrderBy(x => x).ToArray();

                        var offsets = new WriterOffsets<TRoadId>(writer);

                        writer.Write(roads.Count);
                        foreach (var road_id in map_road_ids)
                        {
                            road_id.Write(writer);
                            offsets.Register(road_id);
                        }

                        foreach (var road_id in map_road_ids)
                        {
                            offsets.AddOffset(road_id);
                            road_id.Write(writer);
                            roads[road_id].Write(writer);
                        }

                        offsets.WriteBackOffsets();
                    }
                }
                // ---- writing grid ---------------------------

                grid.Write(writer, this, node_offsets);
            }
        }


        internal static WorldMapMemory ReadRawArray(ILogger logger, IEnumerable<string> fileNames,
            int gridCellSize, string? debugDirectory,
            out List<string> invalidFiles)
        {
            if (DevelModes.True)
            throw new NotSupportedException();
            // Loaded MEM in 131.860504244 s

// this way of reading, i.e. with mapping sparse identifiers into array indices
// is not flexible but it allowed to use arrays instead of dictionaries and 
// it saved around 3GB for node to roads references

            long? timestamp = null;

            //Console.WriteLine("PRESS KEY BEFORE STREAMS");
            //Console.ReadLine();

            invalidFiles = new List<string>();

            foreach (var fn in fileNames)
            {
                using (var reader = new BinaryReader(new FileStream(fn, FileMode.Open, FileAccess.Read), Encoding.UTF8, leaveOpen: false))
                {
                    var curr_version = reader.ReadInt32();
                    if (curr_version != StorageInfo.DataFormatVersion)
                    {
                        invalidFiles.Add(fn);
                        logger.Warning($"File {fn} uses format {curr_version}, supported {StorageInfo.DataFormatVersion}");
                        continue;
                    }

                    var ts = reader.ReadInt64();
                    if (!timestamp.HasValue)
                        timestamp = ts;
                    else if (timestamp != ts)
                        throw new ArgumentException($"Maps are not sync, road names use different identifiers {fn}.");

                    reader.ReadInt32(); // cell size

                    GeoZPoint.ReadRawAngle(reader);
                    GeoZPoint.ReadRawAngle(reader);
                    GeoZPoint.ReadRawAngle(reader);
                    GeoZPoint.ReadRawAngle(reader);


//                    logger.Info($"{fn} stats: {nameof(nodes_count)} {nodes_count}, {nameof(roads_count)} {roads_count}.");
                }
            }

            // here: 3 GB taken 

            //Console.WriteLine("PRESS KEY FOR REAL READ");
            //Console.ReadLine();

            var nodes = HashMap.Create<TNodeId, NodeInfo<TRoadId>>(); //capacity: total_nodes_count);
            var roads = HashMap.Create<TRoadId, RoadInfo<TNodeId>>(); //total_roads_count);

            var dangerous_nearby_nodes = new Dictionary<TNodeId, TRoadId>();

            foreach (var fn in fileNames)
            {
                logger.Verbose($"Loading raw map from {fn}");

                using (var reader = new BinaryReader(new FileStream(fn, FileMode.Open, FileAccess.Read), Encoding.UTF8, leaveOpen: false))
                {
                    var curr_version = reader.ReadInt32();
                    if (curr_version != StorageInfo.DataFormatVersion)
                    {
                        continue;
                    }

                    reader.ReadInt64(); // timestamp
                    reader.ReadInt32(); // cell size

                    GeoZPoint.ReadRawAngle(reader);
                    GeoZPoint.ReadRawAngle(reader);
                    GeoZPoint.ReadRawAngle(reader);
                    GeoZPoint.ReadRawAngle(reader);

                    reader.ReadInt64(); // grid offset
                    var roads_offset = reader.ReadInt64();

                    {
                        var node_offsets = ReaderOffsets.Read<OsmId>(reader, r => OsmIdExtension.Read(r));

                        foreach (var (id, offset) in node_offsets.Offsets.OrderBy(it => it.Value)) // sort in order to get sequential read from disk
                        {
                            reader.BaseStream.Seek(offset, SeekOrigin.Begin);

                            bool is_dangerous_nearby = reader.ReadBoolean();
                            var dangerous_id = DiskHelper.ReadId<TRoadId>(reader);
                            var info = NodeInfo<TRoadId>.Read(reader);
                            if (!nodes.TryGetValue(id, out var existing))
                            {
                                nodes.Add(id, info);
                            }
                            else
                            {
                                if (!info.Equals(existing))
                                {
                                    var (success, merged) = NodeInfo<TRoadId>.Merge(info, existing);
                                    if (success)
                                        nodes[id] = merged;
                                    else
                                        throw new Exception($"Node {id} already exists at {existing}, while trying to add at {info}");
                                }
                            }

                            if (is_dangerous_nearby)
                                dangerous_nearby_nodes.Add(id,dangerous_id);
                        }
                    }

                    reader.BaseStream.Seek(roads_offset, SeekOrigin.Begin);

                    {
                        var road_offsets = ReaderOffsets.Read<OsmId>(reader, r => OsmIdExtension.Read(r));

                        for (int i = 0; i < road_offsets.Count; ++i)
                        {
                            var road_id =OsmIdExtension.Read(reader);
                            var road = RoadInfo<TNodeId>.Read(reader);
                            if (!roads.TryGetValue(road_id, out var existing))
                            {
                                roads.Add(road_id, road);
                            }
                            else
                            {
                                if (road != existing)
                                {
                                    var (ok, merged) = RoadInfo<TNodeId>.Merge(road, existing);
                                    if (ok)
                                        roads[road_id] = merged;
                                    else
                                        throw new ArgumentException($"Road {road_id} already exists with {road}, while we have {existing}");
                                }
                            }
                        }
                    }
                }
            }

            nodes.TrimExcess();
            roads.TrimExcess();

            // here: 4.7 GB (+1.7GB) taken 
            // 27_309_382 nodes (exp: 28_137_923), 4_109_763 roads (exp: 4_185_103), 33_486_629 road points
//            logger.Verbose($"All maps loaded {nodes.Count} nodes (exp: {total_nodes_count}), {roads.Count} roads (exp: {total_roads_count}), {roads.Sum(it => it.Value.Nodes.Count)} road points");
            logger.Verbose($"All maps loaded {nodes.Count} nodes, {roads.Count} roads, {roads.Sum(it => it.Value.Nodes.Count)} road points");
            //Console.WriteLine("PRESS KEY FOR BACK REFS");
            //Console.ReadLine();
            var start = Stopwatch.GetTimestamp();
            //var nodes_to_roads = new NodeRoadsDictionary<TNodeId, TRoadId>(nodes, roads);

            // here: 5.7 GB (+1GB) taken 

            logger.Verbose($"Nodes to roads references created in {(Stopwatch.GetTimestamp() - start - 0.0) / Stopwatch.Frequency}s");
            //Console.WriteLine("PRESS KEY STOP");
            //Console.ReadLine();

            var map = CreateOnlyRoads(logger, nodes, roads,// nodes_to_roads,
                new List<(OsmId, PointOfInterest)>(),
                new List<(OsmId, CityInfo)>(),
                gridCellSize, debugDirectory);
            map.bikeFootExpressNearbyNodes = dangerous_nearby_nodes;

            return map;
        }

        public void ProcessRoadAssociations(RoadGridMemory grid, MapSettings mapSettings)
        {
            this.grid = grid;
            
            long start = Stopwatch.GetTimestamp();
            Dictionary<TNodeId, TRoadId> express_nearby = extractExpressNearby(mapSettings.HighTrafficProximity);

            logger.Info($"High traffic nodes {express_nearby.Count} computed in {((Stopwatch.GetTimestamp() - start + 0.0) / Stopwatch.Frequency)}s");

            this.SetExpressNearby(express_nearby);
            
            processFootways(mapSettings.UrbanFootwayProximity,mapSettings.UrbanFootwayCoverage);
        }

        private Dictionary<TNodeId, TRoadId> extractExpressNearby(Length highTrafficProximity)
        {
            // key: cycle/foot-way id 
            var express_nearby = new Dictionary<TNodeId, TRoadId>();
            // we no longer use this layout, but lets keep it here for the sake of history (maybe we will get back to this)

            foreach (var (danger_id, danger_road) in this.roads)
            {
                if (!danger_road.IsExpressTraffic)
                    continue;

                foreach (var danger_node_id in danger_road.Nodes)
                {
                    foreach (var cyclefoot_snap in grid.GetSnaps(this.nodes[danger_node_id].Point, highTrafficProximity,
                                 enforce:false,
                                 info => info.Layer == danger_road.Layer
                                         && (info.Kind == WayKind.Cycleway || info.Kind == WayKind.Footway)))
                    {
                        var cyclefoot_node_id = this.GetNode(cyclefoot_snap.BaseRoadIndex);

                        if (!express_nearby.TryGetValue(cyclefoot_node_id, out var existing_id)
                            || danger_road.Traffic.MoreRiskyThan(this.GetRoadInfo(existing_id).Traffic))
                        {
                            express_nearby[cyclefoot_node_id] = danger_id;
                        }
                    }
                }
            }

            return express_nearby;
        }

        private void processFootways(Length proximity,double coverage)
        {
            int count = 0;
            
            foreach (var (foot_id, foot_road) in this.roads)
            {
                if (foot_road.Kind!= WayKind.Footway || foot_road.HasUrbanSidewalk)
                    continue;

                bool passed = true;
                int fail_count = 0;
                
                var match_limit = foot_road.Nodes.Count * coverage;
                var fail_limit = foot_road.Nodes.Count - match_limit;

                for (int i=0;i< foot_road.Nodes.Count;++i)
                {
                    var foot_node_id = foot_road.Nodes[i];

                    if (grid.GetSnaps(this.nodes[foot_node_id].Point, proximity,
                            enforce:false,
                            info => info.Layer == foot_road.Layer
                                    && (info.Kind <= WayKind.TertiaryLink || info.HasName)).Any())
                    {
                        if (i+1-fail_count>=match_limit)
                        {
                            break;
                        }
                    }
                    else
                    {
                        ++fail_count;
                        if (fail_count>fail_limit)
                        {
                            passed = false;
                            break;
                        }
                    }
                }

                if (passed)
                {
                    this.roads[foot_id] = foot_road.BuildWithUrbanSidewalkFlag();
                    ++count;
                }
            }

            this.logger.Info($"Set urban to {count} roads with proximity {proximity}");
        }
        
        public OsmId GetOsmRoadId(TRoadId roadId)
        {
            return roadId;
        }

        public OsmId GetOsmNodeId(TNodeId nodeId)
        {
            return nodeId;
        }

        public IEnumerable<PointOfInterest> GetAttractions(TNodeId nodeId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(TNodeId, GeoPoint, PointOfInterest)> GetAttractionsWithin(Region region, PointOfInterest.Feature excludeFeatures)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(TNodeId, GeoPoint, CityInfo)> GetCitiesWithin(Region region)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<(TRoadId, NodeInfo<TNodeId>)> GetNodesWithin(Region region)
        {
            throw new NotImplementedException();
        }

        public TNodeId FromOsmNodeId(OsmId osmNodeId)
        {
            return osmNodeId;
        }

        public TRoadId FromOsmRoadId(OsmId osmRoadId)
        {
            return osmRoadId;
        }

        /*public void UpdateRoads(IReadOnlyDictionary<long, RoadExtra> extras)
        {
            foreach (var (road_id, extra) in extras)
            {
                if (extra.HasRedundantFlag)
                {
                    roads[road_id] = roads[road_id].BuildRedundantFlag();
                }
            }
        }*/
    }
}