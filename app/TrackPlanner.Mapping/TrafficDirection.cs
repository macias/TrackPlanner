﻿namespace TrackPlanner.Mapping
{
    public readonly record struct TrafficDirection<TRoadId>(bool Forward, bool Backward
        #if DEBUG
        , TRoadId RoadId,ushort FromIndex,ushort ToIndex
        #endif
        )
    {
        public static TrafficDirection<TRoadId> Both(   
#if DEBUG
         TRoadId roadId,ushort fromIndexAlongRoad,ushort toIndexAlongRoad
#endif
        )
        {
            return new TrafficDirection<TRoadId>(true, true
#if DEBUG
,roadId,fromIndexAlongRoad,toIndexAlongRoad
#endif
            );
        }

        public bool IsOneWay => this.Forward != this.Backward;

        public TrafficDirection<TRoadId> Reverse()
        {
            return new TrafficDirection<TRoadId>(Backward, Forward
#if DEBUG
                 ,RoadId,ToIndex,FromIndex
#endif
            );
        }

        public bool IsSameFlow(TrafficDirection<TRoadId> other)
        {
            return this.Forward == other.Forward && this.Backward == other.Backward;
        }
    }
}