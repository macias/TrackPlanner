﻿using System;
using System.Collections.Generic;
using OsmSharp;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public sealed class OsmExtractor
    {
        public enum Source
        {
            Historic,
            Buildings,
            Heritage,
            SiteType,
            Castles,
        }

        private readonly ILogger logger;
        private readonly IReadOnlyBasicMap<OsmId, GeoZPoint> nodes;
        private readonly Func<OsmId, OsmId> roadNodeGetter;
        private readonly HashSet<(OsmExtractor.Source, string)> unused;
        private readonly List<(PointOfInterest hist, OsmId? nodeId, OsmId? wayId)> attractions;

        public IEnumerable<(OsmExtractor.Source, string)> Unused => this.unused;

        internal OsmExtractor(ILogger logger, IReadOnlyBasicMap<OsmId, GeoZPoint> nodes,
            // road id -> any node of the road
            Func<OsmId, OsmId> roadNodeGetter)
        {
            this.logger = logger;
            this.nodes = nodes;
            this.roadNodeGetter = roadNodeGetter;
            this.unused = new HashSet<(OsmExtractor.Source, string)>();
            this.attractions = new List<(PointOfInterest hist, OsmId? nodeId, OsmId? wayId)>();
        }

        public void ReviewData(ILogger logger)
        {
            var dict = new Dictionary<OsmId, List<PointOfInterest>>();
            foreach ((PointOfInterest attraction, MapZPoint<OsmId,OsmId> location) in GetAttractions())
            {
                if (!dict.TryGetValue(location.NodeId!.Value, out var list))
                {
                    list = new List<PointOfInterest>();
                    dict.Add(location.NodeId.Value, list);
                }

                list.Add(attraction);
            }

            foreach (var (node_id, list) in dict.Where(it => it.Value.Count > 1))
            {
                for (int i = list.Count - 1; i >= 0; --i)
                for (int t = i - 1; t >= 0; --t)
                {
                    if (list[i].TryMerge(list[t], out var merged))
                    {
                        logger.Verbose($"Combining attractions at {node_id} {list[i]} and {list[t]} into {merged}");
                        list[t] = merged;
                        list.RemoveAt(i);
                        break;
                    }
                }

                if (list.Count > 1)
                    logger.Warning($"Multiple {list.Count} attractions for {node_id}: {(String.Join(" ;", list))}");
            }
        }

        public List<(PointOfInterest attraction, MapZPoint<OsmId,OsmId> location)> GetAttractions()
        {
            return attractions.Select(it =>
            {
                var effective_node_id = it.nodeId ?? this.roadNodeGetter(it.wayId!.Value);
                return (it.hist, new MapZPoint<OsmId,OsmId>(nodes[effective_node_id], 
                    effective_node_id
                    #if DEBUG
                    , DEBUG_mapRef:null
                    #endif
                    ));
            }).ToList();
        }

        public void Extract(OsmGeo element)
        {
            if (!element.Id.HasValue)
            {
                this.logger.Warning($"No id for {element}");
                return;
            }

            PointOfInterest.Feature? features = extractHistoric(element);
            element.Tags.TryGetValue("name", out var name_val);

            if (element.Tags.TryGetValue("shop", out var shop_val) 
                && (shop_val is "convenience" or "supermarket"))
            {
                features = (features ?? PointOfInterest.Feature.None) 
                           | PointOfInterest.Feature.FoodStore;

                if (element.Tags.TryGetValue("brand", out var brand_val))
                    name_val ??= brand_val;
            }

            element.Tags.TryGetValue("url", out string? url_value);

            var (node_id, way_id) = getEntityReference(element);
            if (features.HasValue && features!= PointOfInterest.Feature.None 
                                  && (node_id.HasValue || way_id.HasValue))
            {
                this.attractions.Add((new PointOfInterest(name_val ?? "", 
                    url_value, features.Value), node_id, way_id));
            }
        }

        private PointOfInterest.Feature? extractHistoric(OsmGeo element)
        {
            PointOfInterest.Feature? features = null;

            void add_feature(PointOfInterest.Feature feat)
            {
                features = (features ?? PointOfInterest.Feature.None) | feat;
            }

            void add_feat(string entry, string phrase, PointOfInterest.Feature feat, ref bool found)
            {
                if (entry.Contains(phrase))
                {
                    found = true;
                    features = (features ?? PointOfInterest.Feature.None) | feat;
                }
            }

            if (element.Tags.TryGetValue("historic", out string hist_value))
            {
                void set_feature(string type, PointOfInterest.Feature feat)
                {
                    bool found = false;
                    add_feat(hist_value, type, feat, ref found);
                }

                set_feature("aqueduct", PointOfInterest.Feature.Aqueduct);
                set_feature("castle", PointOfInterest.Feature.Castle);
                set_feature("mansion", PointOfInterest.Feature.Mansion);
                set_feature("lighthouse", PointOfInterest.Feature.Lighthouse);
                set_feature("wreck", PointOfInterest.Feature.Ship | PointOfInterest.Feature.Ruins);
                set_feature("boat", PointOfInterest.Feature.Ship);
                set_feature("ship", PointOfInterest.Feature.Ship);
                set_feature("tank", PointOfInterest.Feature.Tank);
                set_feature("tomb", PointOfInterest.Feature.Grave);
                set_feature("manor", PointOfInterest.Feature.Manor);
                set_feature("bunker", PointOfInterest.Feature.Bunker);
                set_feature("church", PointOfInterest.Feature.Church);
                set_feature("monastery", PointOfInterest.Feature.Church);
                set_feature("synagogue", PointOfInterest.Feature.Church);
                set_feature("palace", PointOfInterest.Feature.Palace);
                set_feature("tower", PointOfInterest.Feature.Tower);
                set_feature("chapel", PointOfInterest.Feature.Chapel);
                set_feature("ruins", PointOfInterest.Feature.Ruins);
                set_feature("bridge", PointOfInterest.Feature.Bridge);
                set_feature("building", PointOfInterest.Feature.Building);
                set_feature("city_gate", PointOfInterest.Feature.Wall);
                set_feature("citywalls", PointOfInterest.Feature.Wall);
                set_feature("earthworks", PointOfInterest.Feature.Fortification);
                set_feature("fort", PointOfInterest.Feature.Fortification);
                set_feature("fortification", PointOfInterest.Feature.Fortification);
                set_feature("watermill", PointOfInterest.Feature.Mill);
                set_feature("train_station", PointOfInterest.Feature.TrainStation);
                set_feature("pillory", PointOfInterest.Feature.Pillory);
                set_feature("quarry", PointOfInterest.Feature.Mine);
                set_feature("archaeological_site", PointOfInterest.Feature.ArchaeologicalSite);
                set_feature("aircraft", PointOfInterest.Feature.Aircraft);
                set_feature("helicopter", PointOfInterest.Feature.Aircraft);
                set_feature("locomotive", PointOfInterest.Feature.Train);
                set_feature("railway_car", PointOfInterest.Feature.Train);

                if (features == null)
                    this.unused.Add((OsmExtractor.Source.Historic, hist_value));
            }

            // https://wiki.openstreetmap.org/wiki/Key:heritage
            if (element.Tags.TryGetValue("heritage", out string heritage_value))
            {
                add_feature(PointOfInterest.Feature.None); // triggering focus
                this.unused.Add((OsmExtractor.Source.Heritage, heritage_value));
            }

            if (element.Tags.TryGetValue("tourism", out string? tourism_value)
                && tourism_value.Contains("attraction"))
            {
                add_feature(PointOfInterest.Feature.None); // triggering focus
            }

            if (element.Tags.TryGetValue("ruins", out string ruins_val) && ruins_val == "yes")
                add_feature(PointOfInterest.Feature.Ruins);


            if (features != null) // ok, we know this is interesting for us
            {
                if (element.Tags.TryGetValue("barrier", out string barrier_value))
                {
                    bool dummy = false;
                    add_feat(barrier_value, "wall", PointOfInterest.Feature.Wall, ref dummy);
                }

                if (element.Tags.TryGetValue("man_made", out string man_made_value))
                {
                    bool dummy = false;
                    add_feat(man_made_value, "lighthouse", PointOfInterest.Feature.Lighthouse, ref dummy);
                    add_feat(man_made_value, "tower", PointOfInterest.Feature.Tower, ref dummy);
                }

                if (element.Tags.TryGetValue("building", out string? building_value))
                {
                    var found = false;
                    add_feat(building_value, "ship", PointOfInterest.Feature.Ship, ref found);
                    add_feat(building_value, "synagogue", PointOfInterest.Feature.Church, ref found);
                    add_feat(building_value, "temple", PointOfInterest.Feature.Church, ref found);
                    add_feat(building_value, "cathedral", PointOfInterest.Feature.Church, ref found);
                    add_feat(building_value, "chapel", PointOfInterest.Feature.Church, ref found);
                    add_feat(building_value, "church", PointOfInterest.Feature.Church, ref found);
                    add_feat(building_value, "ruins", PointOfInterest.Feature.Ruins, ref found);
                    add_feat(building_value, "historic", PointOfInterest.Feature.None, ref found);
                    add_feat(building_value, "castle", PointOfInterest.Feature.Castle, ref found);
                    add_feat(building_value, "bunker", PointOfInterest.Feature.Bunker, ref found);
                    add_feat(building_value, "palace", PointOfInterest.Feature.Palace, ref found);
                    add_feat(building_value, "train_station", PointOfInterest.Feature.TrainStation, ref found);
                    add_feat(building_value, "yes", PointOfInterest.Feature.Building, ref found);

                    if (!found)
                        this.unused.Add((OsmExtractor.Source.Buildings, building_value));
                }
            }

            if (element.Tags.TryGetValue("site_type", out string? site_type_value))
            {
                bool found = false;
                if (site_type_value.Contains("tumulus"))
                {
                    // don't set this feature unless it has something already set
                    if (features != null)
                    {
                        add_feature(PointOfInterest.Feature.Grave);
                    }

                    found = true;
                }

                add_feat(site_type_value, "fortification", PointOfInterest.Feature.Fortification, ref found);
                add_feat(site_type_value, "earthworks", PointOfInterest.Feature.Fortification, ref found);

                if (!found)
                    this.unused.Add((OsmExtractor.Source.SiteType, site_type_value));
            }

            if (element.Tags.TryGetValue("castle_type", out string castle_type_val))
            {
                var found = false;
                add_feat(castle_type_val, "archaeological_site", PointOfInterest.Feature.ArchaeologicalSite, ref found);
                add_feat(castle_type_val, "manor", PointOfInterest.Feature.Manor, ref found);
                add_feat(castle_type_val, "palace", PointOfInterest.Feature.Palace, ref found);
                add_feat(castle_type_val, "fortress", PointOfInterest.Feature.Fortress, ref found);

                if (!found)
                    this.unused.Add((OsmExtractor.Source.Castles, castle_type_val));
            }

            return features;
        }

        private (OsmId? nodeId, OsmId? wayId) getEntityReference(OsmGeo element)
        {
            if (element is Way way)
            {
                return (OsmId.PureOsm( way.Nodes.First()), null);
            }
            else if (element is Node node)
            {
                OsmId node_id = OsmId.PureOsm(node.Id!.Value);

                if (node.Latitude.HasValue && node.Longitude.HasValue)
                {
                    return (node_id, null);
                }
                else
                    return (null, null);
            }
            else if (element is Relation relation)
            {
                var way_id = relation.Members.Where(it => it.Type == OsmGeoType.Way).Select(it => it.Id).FirstOrNone();
                if (way_id.HasValue)
                    return (null, OsmId.PureOsm(way_id.Value));
                else
                {
                    var node_id = relation.Members.Where(it => it.Type == OsmGeoType.Node).Select(it => it.Id).FirstOrNone();
                    if (node_id.HasValue)
                        return (OsmId.PureOsm(node_id.Value), null);
                    else
                    {
                        this.logger.Warning($"Relation {relation.Id} does not have any way or node.");
                        return (null, null);
                    }
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}