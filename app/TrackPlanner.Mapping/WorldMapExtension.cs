﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geo;
using MathUnit;
using SharpKml.Dom;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Structures;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.DataExchange;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.Mapping
{
    public static class WorldMapExtension
    {
        public static MapMode GetMapMode<TNodeId, TRoadId>()
        {
            if (typeof(TNodeId) == typeof(OsmId) && typeof(TRoadId) == typeof(OsmId))
                return MapMode.MemoryOnly;
            else if (typeof(TNodeId) == typeof(WorldIdentifier) && typeof(TRoadId) == typeof(WorldIdentifier))
                return MapMode.TrueDisk;
            else
                throw new NotSupportedException($"{nameof(TNodeId)}: {typeof(TNodeId)}, {nameof(TRoadId)}: {typeof(TRoadId)}");
        }

     

        public static Dictionary<TRoadId, RoadExtra> ComputeRoadExtras<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            ILogger logger,
            Length redundantCyclewaysRange,
            params TRoadId[] debugRoads)
            where TNodeId : struct where TRoadId : struct
        {
            Dictionary<TRoadId, RoadInfo<TNodeId>> cycleways = new Dictionary<TRoadId, RoadInfo<TNodeId>>();

            var fixed_cycleways = new HashSet<TRoadId>();

            foreach (var (road_id, road_info) in map.GetAllRoads())
            {
                if (road_info.Kind != WayKind.Cycleway)
                    continue;

                cycleways.Add(road_id, road_info);

                foreach (var node_id in road_info.Nodes)
                {
                    var bucket = map.Grid.CreateBucket(0, map.GetPoint(node_id), redundantCyclewaysRange, redundantCyclewaysRange,
                        forceClosest:false,
                        allowSmoothing: false,
                        allowGap:false);
                    if (bucket == null || !bucket.PrimarySnaps
                            .Any(it => map.GetRoadInfo(it.BaseRoadIndex.RoadId).IsCyclewayCounterpart()))
                    {
                        // if there is no "official" road nearby we assume we need to keep this cycleway
                        fixed_cycleways.Add(road_id);
                        var debug_idx = debugRoads.IndexOf(road_id);
                        if (debug_idx != -1)
                            logger.Verbose($"Fix on [{debug_idx}]{map.GetOsmRoadId(road_id)}: no nearby road at {map.GetOsmNodeId(node_id)}");
                        break;
                    }
                }
            }

            while (true)
            {
                int fixed_count = fixed_cycleways.Count;
                foreach (var (road_id, road_info) in cycleways)
                {
                    if (fixed_cycleways.Contains(road_id))
                        continue;

                    foreach (var node_id in road_info.Nodes)
                    {
                        IReadOnlySet<RoadIndex<TRoadId>> road_indices = map.GetRoadsAtNode(node_id);
                        // if we are connected to fixed cycleway we have to continue being fixed...
                        var other_fixed = road_indices.FirstOrNone(it => fixed_cycleways.Contains(it.RoadId));
                        if (other_fixed.HasValue
                            // ...unless it is at the crossing, so we could assume we ride further by road
                            // not cycleway
                            && !road_indices.Any(it => map.GetRoadInfo(it.RoadId).IsCyclewayCounterpart()))
                        {
                            fixed_cycleways.Add(road_id);
                            var debug_idx = debugRoads.IndexOf(road_id);
                            if (debug_idx != -1)
                                logger.Verbose($"Fix on [{debug_idx}]{map.GetOsmRoadId(road_id)} connected to {map.GetOsmRoadId(other_fixed.Value.RoadId)} at {map.GetOsmNodeId(node_id)}");
                            break;
                        }
                    }
                }

                if (fixed_count == fixed_cycleways.Count)
                    break;
            }

            logger.Info($"Found {fixed_cycleways.Count}/{cycleways.Count} fixed cycleways");

            return cycleways
                .Where(it => !fixed_cycleways.Contains(it.Key))
                .ToDictionary(it => it.Key, it => new RoadExtra(HasRedundantFlag: true));
        }

        public static bool IsRedundant<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TRoadId roadId)
            where TNodeId : struct
            where TRoadId : struct
        {
            return false; //map.GetRoad(roadId).HasRedundantFlag;
        }

        public static void Validate<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map)
            where TNodeId : struct
            where TRoadId : struct
        {
            foreach (var (node_id,_) in map.GetAllNodes())
            {
                map.GetRoadsAtNode(node_id);
                map.GetPoint(node_id);
            }
            foreach (var (road_id,_) in map.GetAllRoads())
            {
                var res = map.TryGetRoadInfo(road_id);
                if (!res.HasValue)
                    throw new Exception(res.ProblemMessage.TryFullMessage());
            }
        }

        public static bool IsPeak<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TNodeId nodeId, Length altitude)
            where TNodeId : struct
            where TRoadId : struct
        {
            int higher = 0;
            int equal = 0;
            foreach (var alt in map.GetAdjacentRoads(nodeId).Select(it => map.GetPoint(it).Altitude))
            {
                if (altitude < alt)
                    return false;
                else if (altitude > alt)
                    ++higher;
                else
                    ++equal;
            }

            return higher > 0 // we have at least one adjacent node below us
                   || equal == 0; // or there is no equals nodes, meaning it is lone node
        }

        public static int LEGACY_RoadSegmentsDistanceCount<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TRoadId roadId, int sourceIndex, int destIndex)
            where TNodeId : struct
            where TRoadId : struct
        {
            int min_idx = Math.Min(sourceIndex, destIndex);
            int max_idx = Math.Max(sourceIndex, destIndex);

            int count = max_idx - min_idx;

            {
                // when end is looped  (EXCLUDING start-end)
                int conn_idx = map.GetRoadInfo(roadId).Nodes.IndexOf(map.GetRoadInfo(roadId).Nodes.Last());
                if (conn_idx != 0 && conn_idx != map.GetRoadInfo(roadId).Nodes.Count - 1)
                {
                    count = Math.Min(count, map.GetRoadInfo(roadId).Nodes.Count - 1 - max_idx + Math.Abs(conn_idx - min_idx));
                }
            }
            {
                // when start is looped (including start-end)
                int conn_idx = map.GetRoadInfo(roadId).Nodes.Skip(1).IndexOf(map.GetRoadInfo(roadId).Nodes.First());
                if (conn_idx != -1)
                {
                    ++conn_idx;

                    count = Math.Min(count, min_idx + Math.Abs(conn_idx - max_idx));
                }
            }

            // todo: add handling other forms of loops, like knots

            return count;
        }

  /*      public static GeoZPoint GetRoundaboutCenter<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            IGeoCalculator calc, TRoadId roundaboutId)
            where TNodeId : struct
            where TRoadId : struct
        {
            GeoZPoint min = GeoZPoint.Create(Angle.PI, Angle.Zero, null);
            GeoZPoint max = GeoZPoint.Create(-Angle.PI, Angle.Zero, null);
            foreach (var node in map.GetRoadInfo(roundaboutId).Nodes)
            {
                GeoZPoint pt = map.GetPoint(node);
                if (min.Latitude >= pt.Latitude)
                    min = pt;
                if (max.Latitude <= pt.Latitude)
                    max = pt;
            }

            return calc.GetMidPoint(min, max);
        }
*/
        public static void SaveAsKml<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            UserVisualPreferences visualPrefs, string path, bool flatRoads)
            where TNodeId : struct
            where TRoadId : struct
        {
            var title = System.IO.Path.GetFileNameWithoutExtension(path);

            using (var stream = new FileStream(path, FileMode.CreateNew, FileAccess.Write))
            {
                SaveAsKml(map, visualPrefs, title, stream, flatRoads);
            }
        }

        public static void SaveAsKml<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            UserVisualPreferences visualPrefs, string title, Stream stream, bool flatRoads)
            where TNodeId : struct
            where TRoadId : struct
        {
            var speed_lines = TrackWriter.GetKmlSpeedLines(visualPrefs);

            var input = new TrackWriterInput() {Title = title};
            input.Lines = map.GetAllRoads().Select(it =>
            {
                var road = it.Value;
                GeoZPoint[] road_points = road.Nodes.Select(it =>
                {
                    var pt = map.GetPoint(it);
                    if (flatRoads)
                        pt = pt.WithAltitude(Length.Zero);
                    return pt;
                }).ToArray();
                return new LineDefinition(road_points, name: idToString( it.Key),
                    description: road.GetMapDataDetails(), speed_lines[it.Value.GetRoadSpeedMode().Style]);
            }).ToList();

            var node_conflicts = new Dictionary<GeoPoint,List<TNodeId>>();
            foreach (var (node_id, node_info) in map.GetAllNodes())
            {
                if (!node_conflicts.TryGetValue(node_info.Point.Convert2d(), out var list))
                {
                    list = new List<TNodeId>();
                    node_conflicts.Add(node_info.Point.Convert2d(), list);
                }
                list.Add(node_id);
            }
            
            input.Waypoints = map.GetAllNodes().Select(it =>
                {
                    var desc_sections = new List<string>();
                    var danger = map.GetBikeFootRoadExpressNearby(it.Key);
                        desc_sections.Add(danger is { } d ? idToString(d) : "");
                    var node_ids = node_conflicts[it.Value.Point.Convert2d()];
                    if (node_ids.Count > 1)
                        desc_sections.Add(String.Join($"{MapDataInfo.ValueSeparator} ", map.GetRoadsAtNode(it.Key).Select(it => idToString(it.RoadId))));
                    else
                    desc_sections.Add("");
                    desc_sections.Add(it.Value.GetMapDataDetails());
                   
                    return new WaypointDefinition(it.Value.Point, name: idToString(it.Key),
                        description: String.Join( MapDataInfo.SectionSeparator ,desc_sections), 
                        PointIcon.DotIcon);
                })
                .ToList();

            var kml = input.BuildDecoratedKml();

            if ((kml.Root as Document)!.Features.Any())
            {
                kml.Save(stream);
            }
        }

        private static string idToString<TIdentifier>(TIdentifier id)
        {
            if (id is OsmId l)
                return l.ToString();
            else
                throw new NotSupportedException($"{nameof(TIdentifier)}: {typeof(TIdentifier)}");
        }

        public static IWorldMap<OsmId, OsmId> ExtractMiniMap<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> sourceMap,
            ILogger logger, 
            int gridCellSize, string? debugDirectory,
            bool onlyRoadBased,
            bool strict,
            HashSet<TNodeId> seedNodes)
            where TNodeId : struct, IEquatable<TNodeId>
            where TRoadId : struct, IEquatable<TRoadId>
        {
            // added 2023-03-07, expansion, useful to grab alternate roads for turns 
            seedNodes.AddRange(seedNodes
                .SelectMany(it => sourceMap.GetAdjacentRoads(it)
                .Select(it => sourceMap.GetNode(it)))
                .ToArray());
                
            var roads_extract = seedNodes
                .SelectMany(node_id => sourceMap.GetRoadsAtNode(node_id).Select(it => it.RoadId))
                .Distinct()
                .ToDictionary(it => it,it => sourceMap.GetRoadInfo(it));

            if (strict)
                foreach (var (road_id, info) in roads_extract.ToArray())
                {
                    if (info.IsRoundabout)
                        continue;
                    
                    roads_extract[road_id] = info with
                    {
                        Nodes = info.Nodes
                            .SkipLastWhile(it => !seedNodes.Contains(it))
                            .SkipWhile(it => !seedNodes.Contains(it))
                            .ToList()
                    };
                }

            var nodes_extract = new HashSet<TNodeId>();
            if (!onlyRoadBased)
                nodes_extract.AddRange(seedNodes);

            var dangerous = new Dictionary<OsmId, OsmId>();
            while (true)
            {
                foreach (var node_id in roads_extract.Values.SelectMany(it => it.Nodes))
                {
                    nodes_extract.Add(node_id);
                }

                bool added_extras = false;
                
                foreach (var node_id in nodes_extract)
                    if (sourceMap.GetBikeFootRoadExpressNearby(node_id) is { } d)
                    {
                        dangerous.TryAdd(sourceMap.GetOsmNodeId(node_id), sourceMap.GetOsmRoadId(d));
                        if (!roads_extract.ContainsKey(d))
                        {
                            added_extras = true;
                            RoadInfo<TNodeId> dangerous_info = sourceMap.GetRoadInfo(d);
                            // we need just reference, so single segment will suffice
                            roads_extract.Add(d, dangerous_info with
                            {
                                Nodes = dangerous_info.Nodes.Take(2).ToList()
                            });
                        }
                    }

                if (!added_extras)
                    break;
            }

            var osm_nodes = nodes_extract.ToHashMap(sourceMap.GetOsmNodeId, 
                it => MapTranslator.ConvertNodeInfo(sourceMap.GetNodeInfo(it), sourceMap.GetOsmRoadId));
            var osm_roads = roads_extract.ToHashMap(it => sourceMap.GetOsmRoadId(it.Key),
                it => MapTranslator.ConvertRoadInfo(it.Value, sourceMap.GetOsmNodeId));
            var map_extract = WorldMapMemory.CreateOnlyRoads(logger, osm_nodes, osm_roads,
                //new NodeRoadsDictionary<OsmId, OsmId>(osm_nodes, osm_roads),
                new List<(OsmId, PointOfInterest)>(),
                new List<(OsmId, CityInfo)>(),
                gridCellSize, debugDirectory);
            map_extract.SetExpressNearby(dangerous);
            return map_extract;
        }

        public static HashSet<TNodeId> GetNodesAlongLine<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, 
            IGeoCalculator calc, Length range, params GeoPoint[] focusPoints) 
            where TNodeId : struct 
            where TRoadId : struct
        {
            var nodes_extract = new HashSet<TNodeId>();
            foreach (var (prev_point, next_point) in focusPoints.Slide())
            {
                var boundary = calc.GetBoundary(range, prev_point, next_point);
                foreach (var (node_id, node_info) in map.GetNodesWithin(boundary))
                {
                    if (nodes_extract.Contains(node_id))
                        continue;
                    var (dist, _, _) = calc.GetFlatDistanceToArcSegment(node_info.Point,
                        prev_point.Convert3d(), next_point.Convert3d(),stable:false);

                    if (dist > range)
                        continue;

                    nodes_extract.Add(node_id);
                }
            }

            return nodes_extract;
        }

         public static IEnumerable<RoadIndex<TRoadId>> GetAdjacentRoads<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TNodeId nodeId)
            where TNodeId : struct
            where TRoadId : struct
        {
            foreach (var road_idx in map.GetRoadsAtNode(nodeId))
            {
                if (map.tryGetSibling(road_idx, Direction.Backward, out var prev_road_idx))
                    yield return prev_road_idx;
                if (map.tryGetSibling(road_idx, Direction.Forward, out var next_road_idx))
                    yield return next_road_idx;
            }
        }

    
        private static bool tryGetSibling<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<TRoadId> current, Direction direction, out RoadIndex<TRoadId> sibling)
            where TNodeId : struct
            where TRoadId : struct
        {
            var target_index = current.IndexAlongRoad + direction.AsChange();

            if (target_index >= 0 && target_index < map.GetRoadInfo(current.RoadId).Nodes.Count)
            {
                sibling = new RoadIndex<TRoadId>(current.RoadId, target_index);
                return true;
            }

            sibling = default;
            return false;
        }

        public static bool TryGetPrevious<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<TRoadId> current, out RoadIndex<TRoadId> previous)
            where TNodeId : struct
            where TRoadId : struct
        {
            return tryGetSibling(map, current, Direction.Backward, out previous);
        }

        public static bool TryGetNext<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<TRoadId> current, out RoadIndex<TRoadId> next)
            where TNodeId : struct
            where TRoadId : struct
        {
            return tryGetSibling(map, current, Direction.Forward, out next);
        }

        public static TNodeId? GetNullableNode<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            in RoadIndex<TRoadId>? index)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (index is { } idx)
                return map.GetNode(idx);
            else
                return null;
        }
        public static TNodeId GetNode<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            in RoadIndex<TRoadId> idx)
            where TNodeId : struct
            where TRoadId : struct
        {
            var road_info = map.GetRoadInfo(idx.RoadId);
            #if DEBUG
            if (idx.IndexAlongRoad < 0 || idx.IndexAlongRoad >= road_info.Nodes.Count)
                throw new ArgumentOutOfRangeException($"{nameof(idx.IndexAlongRoad)} = {idx.IndexAlongRoad} / {road_info.Nodes.Count}");
            #endif
            return road_info.Nodes[idx.IndexAlongRoad];
        }

        public static GeoZPoint GetPoint<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, in RoadIndex<TRoadId> idx)
            where TNodeId : struct
            where TRoadId : struct
        {
            var node_id = map.GetNode(idx);
            return map.GetPoint(node_id);
        }

        public static bool IsCycleWay<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, TRoadId roadId)
            where TNodeId : struct
            where TRoadId : struct
            => map.GetRoadInfo(roadId).Kind == WayKind.Cycleway;

        public static bool IsMotorRoad<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, TRoadId roadId)
            where TNodeId : struct
            where TRoadId : struct
        {
            var road_kind = map.GetRoadInfo(roadId).Kind;
            return road_kind != WayKind.Cycleway && road_kind != WayKind.Footway && road_kind != WayKind.Steps;
        }

        public static bool IsSignificantMotorRoad<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, TRoadId roadId)
            where TNodeId : struct
            where TRoadId : struct
            => map.IsMotorRoad(roadId) && map.GetRoadInfo(roadId).Kind <= WayKind.Unclassified;

        internal static int CycleWeight<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, in RoadIndex<TRoadId> idx)
            where TNodeId : struct
            where TRoadId : struct
            => map.IsCycleWay(idx.RoadId) ? 1 : 0;

        public static bool IsRoadContinuation<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            TRoadId currentRoadId, TRoadId nextRoadId)
            where TNodeId : struct
            where TRoadId : struct
        {
            return EqualityComparer<TRoadId>.Default.Equals(currentRoadId, nextRoadId)
                   || (map.IsCycleWay(currentRoadId) && map.IsCycleWay(nextRoadId))
                   || (map.GetRoadInfo(currentRoadId).HasName && map.GetRoadInfo(nextRoadId).HasName
                                                          && map.GetRoadInfo(currentRoadId).RoadName == map.GetRoadInfo(nextRoadId).RoadName);
        }

        /*internal static bool IsRoadLooped<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, TRoadId roadId)
            where TNodeId : struct
            where TRoadId : struct
        {
            return map.GetRoad(roadId).Nodes.Count != map.GetRoad(roadId).Nodes.Distinct().Count();
        }
*/
        public static Result<TrafficDirection<TRoadId>> GetTrafficDirection<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            in RoadIndex<TRoadId> from, in RoadIndex<TRoadId> dest)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (!EqualityComparer<TRoadId>.Default.Equals(from.RoadId, dest.RoadId))
                return Result<TrafficDirection<TRoadId>>.Fail($"Cannot compute direction for two different roads {from.RoadId} {dest.RoadId}");
            var node_id = map.GetNode(from);
            if (EqualityComparer<TNodeId>.Default.Equals(node_id, map.GetNode(dest)))
                return Result<TrafficDirection<TRoadId>>.Fail($"Cannot compute direction for the same node {node_id}");

            RoadInfo<TNodeId> road_info = map.GetRoadInfo(from.RoadId);
            if (!road_info.OneWay)
                return Result<TrafficDirection<TRoadId>>.Valid(TrafficDirection<TRoadId>.Both(
                    #if DEBUG
                    from.RoadId,from.IndexAlongRoad,dest.IndexAlongRoad
                    #endif
                    ));

            /*  if (map.IsRoadLooped(from.RoadId))
              {
                  string road_info = "";
                  #if DEBUG
                  road_info = $" {map.GetOsmRoadId(from.RoadId)}";
                  #endif
                  return Result<bool>.Failure($"Cannot tell direction of looped road{road_info}");
              }
*/
            var forward = from.IndexAlongRoad < dest.IndexAlongRoad
                        || (from.IndexAlongRoad == road_info.Nodes.Count - 1 
                            && dest.IndexAlongRoad == 0);
            
            return Result<TrafficDirection<TRoadId>>.Valid(new TrafficDirection<TRoadId>(forward,!forward
#if DEBUG
           , from.RoadId,from.IndexAlongRoad,dest.IndexAlongRoad
#endif
            ));
        }

        public static Length GetRoadDistance<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map, IGeoCalculator calc,
            RoadIndex<TRoadId> start, RoadIndex<TRoadId> dest)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (EqualityComparer<TNodeId>.Default.Equals(map.GetNode(start), map.GetNode(dest)))
                return Length.Zero;

            if (!EqualityComparer<TRoadId>.Default.Equals(start.RoadId, dest.RoadId))
                throw new ArgumentException();

            if (start.IndexAlongRoad > dest.IndexAlongRoad)
                return GetRoadDistance(map, calc, dest, start);

            var length = Length.Zero;

            var curr = start;
            foreach (var next in GetRoadIndices(map, start, dest).Skip(1))
            {
                length += calc.GetDistance(map.GetPoint(curr), map.GetPoint(next));
                curr = next;
            }

            return length;
        }

        public static IEnumerable<RoadIndex<TRoadId>> GetRoadIndices<TNodeId, TRoadId>(this IWorldMap<TNodeId, TRoadId> map,
            RoadIndex<TRoadId> start, RoadIndex<TRoadId> dest)
            where TNodeId : struct
            where TRoadId : struct
        {
            if (!EqualityComparer<TRoadId>.Default.Equals(start.RoadId, dest.RoadId))
                throw new ArgumentException();

            // todo: add road loop handling

            yield return start;

            int dir = dest.IndexAlongRoad.CompareTo(start.IndexAlongRoad);
            for (var curr = start; curr != dest;)
            {
                var next = new RoadIndex<TRoadId>(curr.RoadId, curr.IndexAlongRoad + dir);
                yield return next;
                curr = next;
            }
        }
    }
}