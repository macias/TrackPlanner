﻿using MathUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Structures;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public abstract class RoadGrid<TNodeId, TRoadId, TCell> : IGrid<TNodeId, TRoadId>
        where TCell : RoadGridCell<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        public CellIndex NorthEast => this.GetCellIndex(this.map.Northmost, this.map.Eastmost);
        public CellIndex SouthWest => this.GetCellIndex(this.map.Southmost, this.map.Westmost);

        public int CellSize { get; }

        private readonly IWorldMap<TNodeId, TRoadId> map;
        public IGeoCalculator Calc { get; }
        private readonly ILogger logger;
        private readonly string? debugDirectory;
        private readonly IReadOnlyEnumerableDictionary<CellIndex, TCell> cells;

        public IEnumerable<KeyValuePair<CellIndex, IReadOnlyGridCell<TNodeId, TRoadId>>> Cells => this.cells
            .Select(it => new KeyValuePair<CellIndex, IReadOnlyGridCell<TNodeId, TRoadId>>(it.Key, it.Value));

        protected RoadGrid(ILogger logger, IReadOnlyEnumerableDictionary<CellIndex, TCell> cells,
            IWorldMap<TNodeId, TRoadId> map,
            IGeoCalculator calc,
            int gridCellSize, string? debugDirectory)
        {
            this.CellSize = gridCellSize;
            this.logger = logger;
            this.map = map;
            this.cells = cells;
            this.Calc = calc;
            this.debugDirectory = debugDirectory;
        }

        public abstract string GetStats();
        public abstract CellIndex GetIndexOfNode(TNodeId nodeId);

        public bool TryGetAllNodes(CellIndex cellIndex, out IEnumerable<TNodeId> nodes)
        {
            if (!this.cells.TryGetValue(cellIndex, out var cell))
            {
                nodes = ArraySegment<TNodeId>.Empty;
                return false;
            }

            nodes = cell.GetNodes();
            return true;
        }

        public List<RoadBucket<TNodeId, TRoadId>> GetRoadBuckets(
            IReadOnlyList<RequestPoint<TNodeId>> userPoints,
            Length proximityLimit,
            Length upperProximityLimit)
        {
            var buckets = new List<RoadBucket<TNodeId, TRoadId>>();
            for (int i = 0; i < userPoints.Count; ++i)
            {
                DEBUG_SWITCH.SubSubEnabled2 = DEBUG_SWITCH.SubEnabled && i == 1;
                RoadBucket<TNodeId, TRoadId>? bucket;
                bucket = CreateBucket(i, userPoints[i].UserPoint3d,
                    proximityLimit, upperProximityLimit,
                    forceClosest: userPoints[i].EnforcePoint,
                    allowSmoothing: userPoints[i].AllowSmoothing,
                    allowGap: userPoints[i].AllowGap);

                if (bucket != null)
                    buckets.Add(bucket);
            }

            return buckets;
        }

        public RoadBucket<TNodeId, TRoadId> CreateBucket(int debugIndex,
            GeoZPoint point,
            Length initProximityLimit,
            Length finalProximityLimit,
            bool forceClosest, bool allowSmoothing, bool allowGap)
        {
            RoadSnapInfo<TNodeId, TRoadId>[] snaps = Array.Empty<RoadSnapInfo<TNodeId, TRoadId>>();
            // nodes which are within snap reach but were rejected because of winner-takes-all mode
            var primary_reachable_nodes = new HashSet<TNodeId>();
            Length primary_distance = finalProximityLimit;
            Length effective_proximity;
            for (effective_proximity = initProximityLimit; 
                 effective_proximity <= finalProximityLimit; 
                 effective_proximity += initProximityLimit)
            {
                snaps = getPointNode(point, effective_proximity, enforce: forceClosest,
                        singlePerRoad: false)
                    .Select(it => it.Value)
                    .ToArray();
                if (primary_distance>effective_proximity   && snaps.Any())
                {
                    primary_distance = effective_proximity;
                    
                    primary_reachable_nodes.AddRange(snaps.Select(it => this.map.GetNode(it.BaseRoadIndex)));
                }
                // the reason for searching until we find networked/connected road is that
                // sometimes map is not correctly prepared. For example church can have footpath
                // around it without connecting to anything. However we would like to keep such
                // extended finding as an emergency
                if (snaps.Any(it => forceClosest
                                    || this.map.GetRoadInfo(it.BaseRoadIndex.RoadId).IsNetworked()))
                    break;
                

                if (DEBUG_SWITCH.SubSubEnabled2)
                {
                    ;
                }
            }

            if (!snaps.Any())
                throw new Exception($"No nodes found for user final point {point} within {effective_proximity}");

            if (DEBUG_SWITCH.SubSubEnabled2)
            {
                ;
            }

            if (forceClosest)
            {
                var closest_snap_info = snaps.MinBy(it => it.TrackSnapDistance);
                snaps = new[] {closest_snap_info};
            }


            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            return new RoadBucket<TNodeId, TRoadId>(debugIndex, map, userNodeId: null,
                point.Convert2d(), Calc, snaps,
                primary_reachable_nodes,
                primary_distance, allowSmoothing, allowGap: allowGap);
        }

        private Dictionary<RoadIndex<TRoadId>, RoadSnapInfo<TNodeId, TRoadId>>
            getPointNode(GeoZPoint point, Length limit, bool enforce,
                // curved roads can give several hits, this flag limits it to give only the closest hit
                bool singlePerRoad)
        {
            // key: road id -> index along road -> value
            var result = new Dictionary<TRoadId, Dictionary<int,
                RoadSnapInfo<TNodeId, TRoadId>>>();

            foreach (var snap in GetSnaps(point, limit,
                         enforce,
                         predicate: null)) //road_info => !road_info.HasRedundantFlag))
            {
                if (!result.TryGetValue(snap.BaseRoadIndex.RoadId, out var road_dict))
                {
                    road_dict = new Dictionary<int, RoadSnapInfo<TNodeId, TRoadId>>();
                    result.Add(snap.BaseRoadIndex.RoadId, road_dict);
                }

                if (singlePerRoad)
                {
                    if (road_dict.Count > 0
                        && road_dict.Values.First().TrackSnapDistance > snap.TrackSnapDistance)
                        road_dict.Clear();
                    if (road_dict.Count == 0)
                        road_dict[snap.BaseRoadIndex.IndexAlongRoad] = snap;
                }
                else
                {
                    if (!road_dict.TryGetValue(snap.BaseRoadIndex.IndexAlongRoad,
                            out var existing)
                        || existing.TrackSnapDistance > snap.TrackSnapDistance)
                        road_dict[snap.BaseRoadIndex.IndexAlongRoad] = snap;
                }
            }

            return result
                .SelectMany(it => it.Value.Select(v => v.Value))
                .ToDictionary(it => it.BaseRoadIndex, it => it);
        }


        public IEnumerable<RoadSnapInfo<TNodeId, TRoadId>> GetSnaps(GeoZPoint point, Length limit,
            bool enforce,
            Func<RoadInfo<TNodeId>, bool>? predicate)
        {
            Calc.GetAngularDistances(point, limit,
                out Angle lat_limit, out Angle lon_limit);

            var region = new Region(
                north: point.Latitude + lat_limit,
                east: point.Longitude + lon_limit,
                south: point.Latitude - lat_limit,
                west: point.Longitude - lon_limit);
            IEnumerable<CellIndex> cell_indices = this.EnumerateCellIndices(region);
            if (DEBUG_SWITCH.SubSubEnabled2)
            {
                ;
            }

            foreach (var cell_idx in cell_indices)
            {
                if (this.cells.TryGetValue(cell_idx, out var cell))
                {
                    foreach (var snap in cell.GetSnaps(this.map, Calc, point,
                                 limit, enforce,
                                 predicate))
                        yield return snap;
                }
            }
        }
    }
}