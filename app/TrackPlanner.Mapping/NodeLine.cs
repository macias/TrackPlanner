﻿using System.Collections.Generic;

namespace TrackPlanner.Mapping
{
    public readonly record struct NodeLine<TNodeId>
    {
        public IReadOnlyList<TNodeId> Nodes { get; }

        public NodeLine(IReadOnlyList<TNodeId> nodes)
        {
            Nodes = nodes;
        }
    }
}