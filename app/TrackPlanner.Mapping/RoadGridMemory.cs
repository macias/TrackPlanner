﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrackPlanner.Shared;
using TrackPlanner.Mapping.Disk;
using TrackPlanner.Shared.Data;
using TrackPlanner.Storage;
using TrackPlanner.Structures;

using TNodeId = TrackPlanner.Shared.Data.OsmId;
using TRoadId = TrackPlanner.Shared.Data.OsmId;

namespace TrackPlanner.Mapping
{
    public sealed class RoadGridMemory : RoadGrid<TNodeId,TRoadId,RoadGridCell<TNodeId,TRoadId>>
    {
        private readonly HashMap<CellIndex, RoadGridCell<TNodeId,TRoadId>> cells;

        public int Count => this.cells.Count;
        
        public RoadGridMemory(ILogger logger, HashMap<CellIndex, RoadGridCell<TNodeId,TRoadId>> cells, 
            WorldMapMemory map, 
            IGeoCalculator calc, int gridCellSize, string? debugDirectory)
        : base(logger, cells, map, calc, gridCellSize, debugDirectory)
        {
            this.cells = cells;
        }

        public override CellIndex GetIndexOfNode(TNodeId nodeId)
        {
            throw new NotSupportedException();
        }

        internal void Write(BinaryWriter writer,WorldMapMemory map, IReadOnlyDictionary<TNodeId, long> nodeOffsets)
        {
            var offsets = new WriterOffsets<CellIndex>(writer);

            writer.Write(Count);
            // creating an array guarantees the same order of iteration in two loops
            var coords_array = this.cells.Keys.ToArray();
            foreach (var coords in coords_array)
            {
                coords.Write(writer);
                offsets.Register(coords);
            }

            foreach (var coords in coords_array)
            {
                offsets.AddOffset(coords);
                this.cells[coords].WriteMemory(writer,map,nodeOffsets);
            }

            offsets.WriteBackOffsets();
        }


        public override string GetStats()
        {
            return "no stats so far";
        }
    }
}