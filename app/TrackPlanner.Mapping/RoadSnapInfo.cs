﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using Geo;
using MathUnit;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public enum SnapKind
    {
        Node,
        Segment,
    }
    
    [StructLayout(LayoutKind.Auto)]
    public readonly struct RoadSnapInfo<TNodeId, TRoadId>
        where TNodeId:struct
        where TRoadId : struct
    {
        #if DEBUG
        public MapRef<TNodeId,TRoadId> DEBUG_MapRef { get; }
        #endif
        
        public RoadIndex<TRoadId> BaseRoadIndex { get; }
        // when we create snap using specific node, we cannot tell what road segment we are targetting
        // because we target single node, not road
        // true -- we target segment of road, from one node to another; false -- we target single node
        public SnapKind SnapKind { get; }
        public RoadIndex<TRoadId>? NextRoadIndex => SnapKind == SnapKind.Segment ? BaseRoadIndex.Next() : null;
        public Length TrackSnapDistance { get; }
        public GeoZPoint TrackCrosspoint { get; }
        
        public RoadSnapInfo( 
            #if DEBUG
            MapRef<TNodeId,TRoadId> DEBUG_mapRef,
            #endif
            RoadIndex<TRoadId> baseRoadIndex, SnapKind snapKind,
            Length trackSnapDistance, 
            in GeoZPoint trackCrosspoint)
        {
            #if DEBUG
            this.DEBUG_MapRef = DEBUG_mapRef;
            #endif
            this.BaseRoadIndex = baseRoadIndex;
            SnapKind = snapKind;
            this.TrackSnapDistance = trackSnapDistance;
            TrackCrosspoint = trackCrosspoint;
        }

        public bool IsAdjacentToNode(IWorldMap<TNodeId, TRoadId> map, TNodeId nodeId)
        {
            return (EqualityComparer<TNodeId>.Default.Equals(map.GetNode(BaseRoadIndex), nodeId)
                    || (NextRoadIndex is { }  next_idx && EqualityComparer<TNodeId>.Default.Equals(map.GetNode( next_idx), nodeId)));

        }
        
        public bool Matches(GeoPoint crosspoint,
            RoadIndex<TRoadId> baseIndex, RoadIndex<TRoadId>? nextIndex)
        {
            return this.BaseRoadIndex==baseIndex
                   && this.NextRoadIndex==nextIndex
                   && this.TrackCrosspoint.Convert2d() == crosspoint;
        }


        public string OsmString(IWorldMap<TNodeId, TRoadId> map)
        {
            string kind = SnapKind switch
            {
                SnapKind.Node => "N",
                SnapKind.Segment => "S",
                _ => "?"
            };
            return $"{kind}{TrackCrosspoint} & {map.GetOsmRoadId(BaseRoadIndex.RoadId)}[{BaseRoadIndex.IndexAlongRoad}]";
        }
    }
  }
