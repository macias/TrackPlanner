﻿using System.IO;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Mapping.Disk;

namespace TrackPlanner.Mapping.Experimental
{
    public readonly record struct RoadDirection<TRoadId>(TRoadId RoadId,Direction Direction)
    {
        public void Write(BinaryWriter writer)
        {
            DiskHelper.WriteId(writer,this.RoadId);
            writer.Write((sbyte)Direction);
        }

        public static RoadDirection<TRoadId> Read(BinaryReader reader)
        {
            TRoadId id = DiskHelper.ReadId<TRoadId>(reader);
            var dir = reader.ReadSByte();

            return new RoadDirection<TRoadId>(id, (Direction)dir);
        }  
    }
}