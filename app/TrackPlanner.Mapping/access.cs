﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TrackPlanner.Tests")]
[assembly: InternalsVisibleTo("TrackPlanner.TestToolbox")]

namespace TrackPlanner.Mapping
{
}