﻿using System;
using System.IO;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class DataDiskExtension
    {
        public static CityInfo ReadCity(BinaryReader reader)
        {
            string? name = reader.ReadString();
            if (name == "")
                name = null;
            var rank = (CityRank) reader.ReadUInt16();
            return new CityInfo(rank,name);
        }
        
        public static void WriteCity(this CityInfo info, BinaryWriter writer)
        {
            writer.Write(info.Name ?? "");
            writer.Write((UInt16) info.Rank);
        }


        public static PointOfInterest ReadAttraction(BinaryReader reader)
        {
            string? name = reader.ReadString();
            if (name == "")
                name = null;
            string? url = reader.ReadString();
            if (url == "")
                url = null;
            var features = (PointOfInterest.Feature) reader.ReadUInt32();
            return new PointOfInterest(name, url, features);
        }

        public static void WriteAttraction(this PointOfInterest attr, BinaryWriter writer)
        {
            writer.Write(attr.Name ?? "");
            writer.Write(attr.Url ?? "");
            writer.Write((UInt32) attr.Features);
        }

        public static bool TryMerge(this PointOfInterest attr, PointOfInterest other, out PointOfInterest merged)
        {
            var common = attr.Features & other.Features;
            if (common == attr.Features || common == other.Features)
            {
                merged = new PointOfInterest(attr.Name ?? other.Name, attr.Url ?? other.Url,
                    attr.Features | other.Features);
                return true;
            }

            merged = default;
            return false;
        }
    }
}