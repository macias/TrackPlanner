﻿using System.Collections.Generic;
using System.Linq;
using TrackPlanner.Structures;
using TrackPlanner.Mapping.Data;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.Mapping
{
    public static class NodeRoadsDictionary
    {
        public static NodeRoadsDictionary<TNodeId, TRoadId> Create<TNodeId, TRoadId, TContent>(IReadOnlyMap<TNodeId, TContent> nodes,
            IReadOnlyMap<TRoadId, RoadInfo<TNodeId>> roads)
            where TNodeId : notnull
            where TRoadId : struct
        {
            // roads can form strange loops like "q" shape (example: https://www.openstreetmap.org/way/23005989 )
            // or can have knots, example: https://www.openstreetmap.org/way/88373084

            var back_refs = nodes.ToDictionary(it => it.Key, _ => new HashSet<RoadIndex<TRoadId>>());

            foreach (var (road_id, road_info) in roads)
            {
                for (int i = 0; i < road_info.Nodes.Count; ++i)
                {
                    var node_set = back_refs[road_info.Nodes[i]];
                    node_set.Add(new RoadIndex<TRoadId>(road_id, i));
                }
            }

            var result = back_refs.ToDictionary(it => it.Key,
                it => it.Value.Me<IReadOnlySet<RoadIndex<TRoadId>>>());

            return new NodeRoadsDictionary<TNodeId, TRoadId>(result);
        }

    }

    // 8GB robocza, 10B peak, 19 sekund wczytanie
    public sealed class NodeRoadsDictionary<TNodeId, TRoadId> 
        where TNodeId : notnull
        where TRoadId : struct
    {
        private readonly IReadOnlyDictionary<TNodeId, IReadOnlySet<RoadIndex<TRoadId>>> roadReferences;

        public IReadOnlySet<RoadIndex<TRoadId>> this[TNodeId nodeId]
        {
            get
            {
                if (this.roadReferences.TryGetValue(nodeId, out var result))
                    return result;

                throw new KeyNotFoundException($"Node {nodeId} was not found.");
            }
        }

        public NodeRoadsDictionary(IReadOnlyDictionary<TNodeId, IReadOnlySet<RoadIndex<TRoadId>>> roadReferences)
        {
            this.roadReferences = roadReferences;
        }

        public NodeRoadsDictionary(IReadOnlyMap<TNodeId, GeoZPoint> nodes,
            IReadOnlyMap<TRoadId, RoadInfo<TNodeId>> roads)
        {
            // roads can form strange loops like "q" shape (example: https://www.openstreetmap.org/way/23005989 )
            // or can have knots, example: https://www.openstreetmap.org/way/88373084

            var back_refs = nodes.ToDictionary(it => it.Key, _ => new HashSet<RoadIndex<TRoadId>>());

            foreach (var (road_id, road_info) in roads)
            {
                for (int i = 0; i < road_info.Nodes.Count; ++i)
                {
                    var node_set = back_refs[road_info.Nodes[i]];
                    node_set.Add(new RoadIndex<TRoadId>(road_id, i));
                }
            }

            this.roadReferences = back_refs.ToDictionary(it => it.Key,
                it => it.Value.Me<IReadOnlySet<RoadIndex<TRoadId>>>());
        }
        
    }
}