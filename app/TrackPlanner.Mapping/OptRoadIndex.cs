﻿
namespace TrackPlanner.Mapping
{
    internal readonly record struct OptRoadIndex<TRoadId>(TRoadId RoadId, ushort? IndexAlongRoad)
    {

    }

}