using System;
using System.Threading.Tasks;
using TrackPlanner.Shared;

namespace TrackPlanner.RestClient
{
    public interface IVisualActor
    {
        ValueTask AlertAsync(string? message,MessageLevel severity );
        ValueTask<bool> ConfirmAsync(string message);
        IDisposable GetConcurrentLock(string message);
    }

    public static class VisualActorExtension
    {
        public static ValueTask AlertAsync(this IVisualActor actor,
            in ContextMessage? message)
        {
            if (message is { } m)
                return actor.AlertAsync(m.Message, m.Severity);
            else
                return ValueTask.CompletedTask;
        }
    }
}