using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Force.DeepCloner;
using Geo;
using MathUnit;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.Structures;

namespace TrackPlanner.RestClient
{
    public sealed class ScheduleManager<TNodeId, TRoadId> : IVisualReceiver
        where TNodeId : struct
        where TRoadId : struct
    {
        public bool CanUndo => this.stateManager.CanUndo;
        public bool CanRedo => this.stateManager.CanRedo;
        public string? UndoLabel => this.stateManager.UndoLabel;
        public string? RedoLabel => this.stateManager.RedoLabel;
        private bool autoBuild;
        public bool AutoBuild => autoBuild;
        public ScheduleSettings Settings => this.Schedule.Settings;

        private readonly ILogger logger;
        private readonly IVisualReceiver visualReceiver;
        private readonly IGeoCalculator calculator;
        private readonly StateManager<ScheduleState<UiState, TNodeId, TRoadId>> stateManager;
        private readonly UserVisualPreferences visualPreferences;
        private readonly PlanClient<TNodeId, TRoadId> planClient;
        private readonly IVisualActor actor;
        private readonly AuxiliarySchedule auxState;
        public IAuxiliarySchedule AuxState => this.auxState;
        public UiState UiState { get; }
        public VisualSchedule<TNodeId, TRoadId> Schedule { get; }

        public IReadOnlyList<(string label, Length distance, Length height)> ElevationPoints { get; private set; }
            = Array.Empty<(string label, Length distance, Length height)>();

        public IReadOnlyList<(string? label, Length distance, GeoZPoint point)> ElevationLine { get; private set; }
            = Array.Empty<(string? label, Length distance, GeoZPoint point)>();

        private IVisualReceiver asReceiver => this;
        public DispatchRecorder<TNodeId, TRoadId> Dispatcher { get; }

        public ScheduleManager(ILogger logger, IVisualReceiver visualReceiver,
            IGeoCalculator calculator, ScheduleSettings settings,
            UserVisualPreferences visualPreferences,
            PlanClient<TNodeId, TRoadId> planClient, IVisualActor actor,
            int stateHistoryLimit, bool autoBuild)
        {
            this.logger = logger;
            this.visualReceiver = visualReceiver;
            this.calculator = calculator;
            this.visualPreferences = visualPreferences;
            this.planClient = planClient;
            this.actor = actor;
            this.autoBuild = autoBuild;
            Schedule = new VisualSchedule<TNodeId, TRoadId>(logger, this, settings);
            this.auxState = new AuxiliarySchedule();
            this.UiState = new UiState();

            // first snapshot has to have correct initialized data
            InitCollapsedDays();
            this.stateManager = new StateManager<ScheduleState<UiState, TNodeId, TRoadId>>(
                this.createSnapshot,
                this.applySnapshotAsync,
                stateHistoryLimit);

            // yes,this is crazy, but it is the best way to protect this type from direct calls
            this.Dispatcher = new DispatchRecorder<TNodeId, TRoadId>(logger, actor, this);
        }
        
        public async ValueTask InitializeDefaultAsync()
        {
            // constructor creates blank project, this call creates user-default one
            // for example with home position
            // it would be useful to integrate this into constructor, but then it creates
            // loop -- receiver (like blazor page) creates manager, but on creation manager
            // sends notification, handling it requires having manager already created
            await this.Schedule.NewProjectAsync().ConfigureAwait(false);
            this.stateManager.Reset();
        }

        internal async ValueTask NewProjectAsync()
        {
            if (this.Schedule.IsModified
                && !await this.actor.ConfirmAsync("Really start from scratch?").ConfigureAwait(false))
                return;

            using (this.stateManager.OpenScope("New project"))
            {
                await this.Schedule.NewProjectAsync().ConfigureAwait(false);
                //  this.InitCollapsedDays();
            }
        }


        internal async ValueTask<Result<ValueTuple>> SetAutoBuildAsync(bool value)
        {
            if (this.autoBuild == value)
                return Result.ValidVoid();
            
            using (this.stateManager.OpenScope("Switch auto build"))
            {

                this.autoBuild = value;

                Result<bool> result = await this.TryPartialBuildPlanAsync(directUserRequest: false).ConfigureAwait(false);
                if (result.HasValue)
                    return Result.ValidVoid().WithMessage(result, force: true);
                else
                    return result.FailAs<ValueTuple>();
            }
        }

        internal async ValueTask FindAttractionsAsync(int dayIndex,PointOfInterest.Category category)
        {
            if (this.Schedule.GetSummary().Days[dayIndex].Checkpoints.Count == 0)
                return;

            var attr_result = await this.planClient.FindAttractionsAsync(
                Schedule.CreateInterestRequest(dayIndex,category), CancellationToken.None).ConfigureAwait(false);

            if (attr_result.HasValue)
            {
                //await Schedule.ClearAttractionsAsync(dayIndex).ConfigureAwait(false);
                var existing_attractions = this.Schedule.Route.Legs.SelectMany(it => it.Attractions)
                    .Select(it => it.GetTuple())
                    .ToHashSet();
                List<List<PlacedAttraction>> attractions = attr_result.Value.Attractions;
                foreach (var attr_leg in attractions)
                for (int i=attr_leg.Count-1;i>=0;--i)
                    if (existing_attractions.Contains(attr_leg[i].GetTuple()))
                        attr_leg.RemoveAt(i);
                await this.Schedule.AddAttractionsAsync(dayIndex, attractions).ConfigureAwait(false);
            }

            await this.actor.AlertAsync(attr_result.ProblemMessage).ConfigureAwait(false);
        }

        internal async ValueTask SaveScheduleAsync(string path, CancellationToken token)
        {
            using (this.actor.GetConcurrentLock($"Saving {path} ..."))
            {
                var schedule = this.Schedule.CreateJourneySchedule(this.visualPreferences,
                    onlyPinned: false);

                var save_result = await this.planClient.SaveScheduleAsync(schedule, path, token).ConfigureAwait(false);

                if (save_result.HasValue)
                    this.DataSaved(path);

                if (save_result.ProblemMessage is { } message)
                    await this.actor.AlertAsync($"{(save_result.HasValue?"Incomplete save":"Save failed")}: {message}", message.Severity).ConfigureAwait(false);

            }
        }

        public void InitCollapsedDays()
        {
            this.UiState.CopyFrom(this.Schedule.Days.Select(_ => true));
            this.UiState[^1] = false;
        }

        internal async ValueTask<Result<VisualAnchor>> AddAnchorAtPlaceholderAsync(GeoPoint coords)
        {
            if (this.Schedule.AnchorPlaceholder is not var (day_idx, anchor_idx))
                return Result<VisualAnchor>.Fail("No placeholder is set", MessageLevel.Debug);

            var anchors = this.Schedule.Days[day_idx].Anchors;
            if (anchor_idx < 0 || anchor_idx > anchors.Count)
                return Result<VisualAnchor>.Fail($"Invalid anchor index {anchor_idx}/{anchors.Count}");

            using (this.stateManager.OpenScope("Add anchor"))
            {
                return Result<VisualAnchor>.Valid(await Schedule.AddAnchorAsync(day_idx, anchor_idx, coords, "", LabelSource.AutoMap).ConfigureAwait(false));
            }
        }

        internal async ValueTask SetAnchorPlaceholderAsync(int dayIndex, int anchorIndex)
        {
            using (this.stateManager.OpenScope("Set placeholder"))
            {
                await this.Schedule.SetAnchorPlaceholderAsync(dayIndex, anchorIndex).ConfigureAwait(false);
            }
        }

        public void OnDayToggled(object? dayTag, bool collapsed)
        {
            if (!collapsed) // if it is open now
            {
                // collapse all others
                for (int i = 0; i < UiState.CollapsedDays.Count; ++i)
                {
                    // if (toggle != day.Accordion)
                    if (!i.Equals(dayTag))
                    {
                        UiState[i] = true;
                    }
                }
            }

            // StateHasChanged();
        }

        internal async ValueTask<Result<ValueTuple>> CompleteRebuildPlanAsync(bool directUserRequest)
        {
            if (!this.Schedule.HasEnoughAnchorsForBuild())
            {
                await this.Schedule.ResetSummaryAsync().ConfigureAwait(false);
                return Result<ValueTuple>.Fail("Not enough points to build a plan.");
            }

            bool calc_real = calcReal(directUserRequest);

            using (this.actor.GetConcurrentLock("Rebuilding..."))
            {
                PlanRequest<TNodeId> request = this.Schedule.CreateJourneySchedule(this.visualPreferences, onlyPinned: true)
                    .BuildPlanRequest<TNodeId>();
                request.RouterPreferences.FastModeLimit = null; // complete rebuild has to be precise

                var plan_result = await this.planClient.EnsureGetPlanAsync(
                    request, calc_real,
                    CancellationToken.None).ConfigureAwait(false);

                if (!plan_result.HasValue)
                    return plan_result.FailAs<ValueTuple>();

                await this.Schedule.SetRouteAsync(plan_result.Value.Route, plan_result.Value.Names).ConfigureAwait(false);

                this.logger.Verbose($"Complete rebuild: done {plan_result.ProblemMessage.TryShortMessage()}.");

                return Result<ValueTuple>.Valid(default).WithMessage(plan_result, force: true);
            }
        }

        internal async ValueTask<Result<ValueTuple>> PartialBuildPlanAsync(bool directUserRequest)
        {
            var result = await TryPartialBuildPlanAsync(directUserRequest).ConfigureAwait(false);
            if (!result.HasValue)
                return result.FailAs<ValueTuple>();
            else if (!result.Value)
                return Result<ValueTuple>.Fail("Not enough points for refresh build.");
            else
                return Result.ValidVoid().WithMessage(result, force: true);
        }

        internal async ValueTask<Result<bool>> TryPartialBuildPlanAsync(bool directUserRequest)
        {
            if (!this.Schedule.HasEnoughAnchorsForBuild())
            {
                // refresh summary always, for example we have only single point, and we move it
                // there will be no route computed, but summary should be updated
                await Schedule.ResetSummaryAsync().ConfigureAwait(false);
                return Result<bool>.Valid(false);
            }
            else
            {
                using (this.actor.GetConcurrentLock("Building..."))
                {
                    bool calc_real = calcReal(directUserRequest);

                    (bool changed, ContextMessage? problem) = await ScheduleHelper.PartialRebuild(this.planClient,
                        this.Schedule, this.visualPreferences,
                        calc_real, CancellationToken.None).ConfigureAwait(false);

                    if (problem != null)
                    {
                        this.logger.Verbose($"Partial rebuild: {problem}.");
                    }

                    if (changed)
                    {
                        await asReceiver.OnAnchorChangedAsync(0, 0).ConfigureAwait(false);
                        await Schedule.ResetSummaryAsync().ConfigureAwait(false);
                    }

                    if (problem != null)
                        return Result<bool>.Fail(problem);
                    else
                        return Result<bool>.Valid(true);
                }
            }
        }

        private bool calcReal(bool directUserRequest)
        {
            return (directUserRequest || this.AutoBuild);
        }

        private async ValueTask applySnapshotAsync(ScheduleState<UiState, TNodeId, TRoadId> snapshot)
        {
            await this.Schedule.SetScheduleAsync(snapshot.schedule.DeepClone(),
                snapshot.filename, snapshot.isModified).ConfigureAwait(false);
            // setting schedule caused setting default ui state, so now we have to restore the actual one...
            this.UiState.CopyFrom(snapshot.uiState);
            await addPeaksAsync(Length.Zero,  snapshot.auxiliary.Peaks).ConfigureAwait(false);
            // ... go through refresh
            await asReceiver.OnSummarySetAsync().ConfigureAwait(false);
        }

        private ScheduleState<UiState, TNodeId, TRoadId> createSnapshot(string label)
        {
            var state = new ScheduleState<UiState, TNodeId, TRoadId>(Label: label,
                this.UiState.DeepClone(),
                this.Schedule.Filename,
                this.Schedule.CreateJourneySchedule(null!, onlyPinned: false),
                this.auxState.DeepClone(),
                isModified: this.Schedule.IsModified);
#if DEBUG
            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            if (state.schedule.Route.Validate() is { } problem)
            {
                ;
                //   throw new Exception(problem.FullMessage());
            }
#endif
            return state;
        }

        internal async ValueTask ChangeAnchorPositionAsync(GeoPoint position, int dayIndex, int anchorIndex)
        {
            using (this.stateManager.OpenScope("Move anchor"))
            {
                await Schedule.ChangeAnchorPositionAsync(position, dayIndex, anchorIndex).ConfigureAwait(false);
            }
        }

        internal async ValueTask MergeDayToPreviousAsync(int dayIndex)
        {
            if (dayIndex < 1 || dayIndex >= this.Schedule.Days.Count)
            {
                await this.actor.AlertAsync($"Cannot merge day {dayIndex}/{this.Schedule.Days.Count}", MessageLevel.Warning).ConfigureAwait(false);
                return;
            }

            using (this.stateManager.OpenScope("Merge days"))
                await this.Schedule.MergeDayToPreviousAsync(dayIndex).ConfigureAwait(false);
        }

        internal async ValueTask AddRouteAttractionAsync(int legIndex, int attrIndex, PlacedAttraction attraction)
        {
            using (this.stateManager.OpenScope("Add attraction"))
            {
                await this.Schedule.AddRouteAttraction(legIndex, attrIndex, attraction).ConfigureAwait(false);
            }
        }

        internal async ValueTask AddExternalAttractionAsync(PlacedAttraction attraction)
        {
            using (this.stateManager.OpenScope("Add attraction"))
            {
                var title = attraction.Attraction.GetCompactLabel();
                var point = attraction.Point.Convert2d();
                await this.Schedule.SnapToLegAsync(computeSnapLegIndex(point),point, title,
                    string.IsNullOrEmpty(title) ? LabelSource.AutoMap : LabelSource.Attraction,
                    Settings.PlannerPreferences.DefaultAttractionBreak).ConfigureAwait(false);
            }
        }

        internal async ValueTask SnapToLineAtAsync(GeoPoint point)
        {
            using (this.stateManager.OpenScope("Snap to line"))
            {
                await this.Schedule.SnapToLegAsync(computeSnapLegIndex(point),point, "", LabelSource.AutoMap,
                    Settings.PlannerPreferences.DefaultAnchorBreak).ConfigureAwait(false);
            }
        }

        public async ValueTask PrepareAnchorAppendAsync(int dayIndex, int anchorIndex)
        {
            using (this.stateManager.OpenScope("Prepare append"))
            {
                var anchor = this.Schedule.Days[dayIndex].Anchors[anchorIndex];
                if (!anchor.IsPinned)
                    await this.PinAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);

                (dayIndex, anchorIndex) = Schedule.CampingAdjustInsertionAnchorIndices(dayIndex, anchorIndex + 1);
                await this.SetAnchorPlaceholderAsync(dayIndex, anchorIndex).ConfigureAwait(false);
            }
        }

        
        private int? computeSnapLegIndex(GeoPoint point)
        {
            if (this.Schedule.Route.IsEmpty())
                return null;

            int? winner_leg_idx = null;

            double winner_dist_squared = double.MaxValue;
            foreach (var (leg, leg_idx) in this.Schedule.Route.Legs.ZipIndex())
            {
                double stupid_distance_like(in GeoPoint p, in GeoPoint a, in GeoPoint b)
                {
                    return this.calculator.DistanceToSegmentSquared(p.Latitude.Degrees, p.Longitude.Degrees,
                        a.Latitude.Degrees, a.Longitude.Degrees,
                        b.Latitude.Degrees, b.Longitude.Degrees);
                }

                if (leg.Fragments.Count == 0)
                    continue;

                var dist_squared = stupid_distance_like(point, leg.FirstStep().Point.Convert2d(),
                    leg.LastStep().Point.Convert2d());
                if (winner_dist_squared > dist_squared)
                {
                    winner_dist_squared = dist_squared;
                    winner_leg_idx = leg_idx;
                }
            }

            return winner_leg_idx;
        }

        internal async ValueTask PinAnchorAsync(int dayIndex, int anchorIndex)
        {
            using (this.stateManager.OpenScope("Pin anchor"))
                await this.Schedule.PinAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);
        }

        internal async ValueTask TrySplitDayAsync(int checkpointDayIndex, int checkpointIndex)
        {
            var trait = new CheckpointTrait(this.Schedule, checkpointDayIndex, checkpointIndex);
            if (!trait.CanSplit)
            {
                await this.actor.AlertAsync($"Unable to split day at {checkpointDayIndex}:{checkpointIndex}", MessageLevel.Warning).ConfigureAwait(false);
                return;
            }

            using (this.stateManager.OpenScope("Split day"))
                await this.Schedule.SplitDayAsync(trait.AnchorDayIndex, trait.AnchorIndex).ConfigureAwait(false);
        }

        internal async ValueTask TryDeleteAnchorAsync(int dayIndex, int anchorIndex)
        {
            if (!this.Schedule.Days[dayIndex].Anchors[anchorIndex].IsPinned)
            {
                await this.actor.AlertAsync("Cannot delete auto anchor", MessageLevel.Warning);
                return;
            }

            using (this.stateManager.OpenScope("Remove anchor"))
            {
                await this.Schedule.DeleteAnchorAsync(dayIndex, anchorIndex).ConfigureAwait(false);
            }
        }

        public async ValueTask EditCheckpointDetailsAsync(int checkpointDayIndex, int checkpointIndex, AnchorEdit? result)
        {
            (var anchor_day_idx, int anchor_idx) = this.Schedule.CheckpointIndexToAnchor(checkpointDayIndex, checkpointIndex);
            if (result is not { } edit)
                return;

            if (edit == AnchorEdit.AnchorDelete)
                await this.Dispatcher.ExecuteAsync(new DeleteAnchor(anchor_day_idx, anchor_idx), CancellationToken.None).ConfigureAwait(false);
            else if (edit == AnchorEdit.DayMergePrevious)
                await this.Dispatcher.ExecuteAsync(new DaySplitRemove(anchor_day_idx), CancellationToken.None).ConfigureAwait(false);
            else if (edit == AnchorEdit.AnchorPin)
                await this.Dispatcher.ExecuteAsync(new PinAutoAnchor(anchor_day_idx, anchor_idx), CancellationToken.None).ConfigureAwait(false);
            else if (edit == AnchorEdit.FindPeaks)
                await this.Dispatcher.ExecuteAsync(new FindPeaks(anchor_day_idx, anchor_idx), CancellationToken.None).ConfigureAwait(false);
            else if (edit == AnchorEdit.OrderNext)
                await this.Dispatcher.ExecuteAsync(OrderAnchor.Next(anchor_day_idx, anchor_idx), CancellationToken.None).ConfigureAwait(false);
            else if (edit == AnchorEdit.OrderPrevious)
                await this.Dispatcher.ExecuteAsync(OrderAnchor.Previous(anchor_day_idx, anchor_idx), CancellationToken.None).ConfigureAwait(false);
            else if (edit == AnchorEdit.DaySplit)
            {
                await this.Dispatcher.ExecuteAsync(new DaySplit(checkpointDayIndex, checkpointIndex), CancellationToken.None)
                    .ConfigureAwait(false);
            }
            else
                using (this.stateManager.OpenScope("Anchor edit"))
                {
                    var schedule_day = this.Schedule.Days[anchor_day_idx];
                    var anchor = schedule_day.Anchors[anchor_idx];
                    if (edit.Point is { } point)
                        // THIS CAN CHANGE ANCHOR INDICES
                        await this.Schedule.ChangeAnchorPositionAsync(point, anchor_day_idx, anchor_idx).ConfigureAwait(false);
                    
                    if (edit.StartTime is { } start_time)
                        await schedule_day.SetStartAsync(start_time).ConfigureAwait(false);
                    if (edit.BreakTime is { } break_time)
                         await anchor.SetUserBreakAsync( break_time).ConfigureAwait(false);
                    if (edit.AllowGap is { } allow_gap)
                        await anchor.SetAllowGapAsync( allow_gap).ConfigureAwait(false);
                    if (edit.HasMap is { } has_map)
                        await anchor.SetHasMapAsync( has_map).ConfigureAwait(false);
                    if (edit.Label is { } label)
                        await anchor.SetLabelAsync( label,LabelSource.User).ConfigureAwait(false);
                }
        }


        public (int dayIndex, int anchorIndex, IReadOnlyAnchor anchor) IndexOfAnchor(IReadOnlyAnchor anchor)
        {
            for (int day_idx = 0; day_idx < this.Schedule.Days.Count; ++day_idx)
            {
                for (int anchor_idx = 0; anchor_idx < this.Schedule.Days[day_idx].Anchors.Count; ++anchor_idx)
                {
                    if (this.Schedule.Days[day_idx].Anchors[anchor_idx] == anchor)
                        return (day_idx, anchor_idx, anchor);
                }
            }

            throw new ArgumentException("Marker not found.");
        }

        internal async ValueTask SetSettingsAsync(ScheduleSettings? prefs)
        {
            if (prefs == null)
                return;

            using (this.stateManager.OpenScope("Parameters change"))
                await this.Schedule.SetSettingsAsync(prefs).ConfigureAwait(false);
        }

        ValueTask IVisualReceiver.OnAttractionsAddedAsync(IEnumerable<PlacedAttraction> attractions)
        {
            return this.visualReceiver.OnAttractionsAddedAsync(attractions);
        }

        async ValueTask IVisualReceiver.OnSummarySetAsync()
        {
            var open_day_idx = findOpenDayIndex();
            //logger.Verbose($"Calculating elevation for {open_day_idx} day");

            try
            {
                ElevationData elevation = ElevationData.Empty;
                if (open_day_idx != -1 && Schedule.HasEnoughAnchorsForBuild())
                {
                    var prof_result = ScheduleHelper.GetElevationProfile(this.Schedule, open_day_idx);
                    if (prof_result.ProblemMessage != null)
                        await actor.AlertAsync(prof_result.ProblemMessage).ConfigureAwait(false);
                    if (prof_result.HasValue)
                        elevation = prof_result.Value;
                }

                (this.ElevationLine, this.ElevationPoints) = elevation;
            }
            catch (Exception ex)
            {
                ex.LogDetails(this.logger, MessageLevel.Error);
                await this.actor.AlertAsync(ex.Message, MessageLevel.Error).ConfigureAwait(false);
            }

            await this.visualReceiver.OnSummarySetAsync().ConfigureAwait(false);
        }

        private int findOpenDayIndex()
        {
            return this.UiState.CollapsedDays.IndexOf(it => !it);
        }

        ValueTask IVisualReceiver.OnPlaceholderChangedAsync()
        {
            return this.visualReceiver.OnPlaceholderChangedAsync();
        }

        async ValueTask IVisualReceiver.OnLoopChangedAsync()
        {
            await this.visualReceiver.OnLoopChangedAsync().ConfigureAwait(false);

            var result = await this.TryPartialBuildPlanAsync(directUserRequest: false).ConfigureAwait(false);
            await this.actor.AlertAsync(result.ProblemMessage).ConfigureAwait(false);
        }

        async ValueTask IVisualReceiver.OnScheduleSetAsync()
        {
            this.InitCollapsedDays();
            this.auxState.ClearPeaks();

            await this.visualReceiver.OnPeaksClearedAsync().ConfigureAwait(false);
            await this.visualReceiver.OnScheduleSetAsync().ConfigureAwait(false);
        }

        ValueTask IVisualReceiver.OnPeakAddedAsync(GeoZPoint peak)
        {
            return this.visualReceiver.OnPeakAddedAsync(peak);
        }

        ValueTask IVisualReceiver.OnPeaksClearedAsync()
        {
            return ValueTask.CompletedTask;
        }

        internal Result<(string running, string remaining, string mode)> GetFragmentInformation(int legIndex,
            int fragmentIndex, GeoPoint point)
        {
            LegPlan<TNodeId, TRoadId> leg = this.Schedule.Route.Legs[legIndex];
            return Result<(string running, string remaining, string mode)>.Valid(GetFragmentInformation(leg, leg.Fragments[fragmentIndex], point));
        }

        public (string running, string remaining, string label) GetFragmentInformation(LegPlan<TNodeId, TRoadId> leg, LegFragment<TNodeId, TRoadId> fragment,
            GeoPoint point)
        {
            this.Schedule.ComputeProgress(this.calculator, point,
                leg, fragment, out Length running_distance,
                out TimeSpan day_time,
                out Length remaining_distance,
                out TimeSpan remaining_time);

            return ($"{DataFormat.FormatDistance(running_distance, withUnit: true)} @({DataFormat.Format(day_time)})",
                $"{DataFormat.FormatDistance(remaining_distance, withUnit: true)} ({DataFormat.Format(remaining_time)})",
                $"{fragment.SpeedStyling.Label}");
        }

        void IVisualReceiver.OnDayMerged(int dayIndex)
        {
            this.UiState.RemoveAt(dayIndex);

            this.visualReceiver.OnDayMerged(dayIndex);
        }

        ValueTask IVisualReceiver.OnLegRemovingAsync(int legIndex)
        {
            return this.visualReceiver.OnLegRemovingAsync(legIndex);
        }

        ValueTask IVisualReceiver.OnLegsAddedAsync(int firstLegIndex, int legCount)
        {
            return this.visualReceiver.OnLegsAddedAsync(firstLegIndex, legCount);
        }

        ValueTask IVisualReceiver.OnAllLegsRemovingAsync()
        {
            return this.visualReceiver.OnAllLegsRemovingAsync();
        }

        ValueTask IVisualReceiver.OnAttractionsRemovingAsync(IEnumerable< PlacedAttraction> attractions)
        {
            return this.visualReceiver.OnAttractionsRemovingAsync(attractions);
        }

        ValueTask IVisualReceiver.OnAnchorChangedAsync(int dayIndex, int anchorIndex)
        {
            return this.visualReceiver.OnAnchorChangedAsync(dayIndex, anchorIndex);
        }

        ValueTask IVisualReceiver.OnAnchorAddedAsync(VisualAnchor anchor, int dayIndex, int anchorIndex)
        {
            return this.visualReceiver.OnAnchorAddedAsync(anchor, dayIndex, anchorIndex);
        }

        ValueTask IVisualReceiver.OnAllAnchorsRemovingAsync()
        {
            return this.visualReceiver.OnAllAnchorsRemovingAsync();
        }

        ValueTask IVisualReceiver.OnAnchorRemovingAsync(VisualAnchor anchor)
        {
            return this.visualReceiver.OnAnchorRemovingAsync(anchor);
        }

        async ValueTask IVisualReceiver.OnAnchorMovedAsync(VisualAnchor anchor)
        {
            await this.visualReceiver.OnAnchorMovedAsync(anchor).ConfigureAwait(false);

            var result = await this.TryPartialBuildPlanAsync(directUserRequest: false).ConfigureAwait(false);
            await this.actor.AlertAsync(result.ProblemMessage).ConfigureAwait(false);
        }

        async ValueTask IVisualReceiver.OnPlanChangedAsync()
        {
            var result = await this.TryPartialBuildPlanAsync(directUserRequest: false).ConfigureAwait(false);
            await this.actor.AlertAsync(result.ProblemMessage).ConfigureAwait(false);
        }

        void IVisualReceiver.OnDaySplit(int dayIndex)
        {
            // keep "previous" day open
            this.UiState[dayIndex] = false;
            this.UiState.Insert(dayIndex + 1, true);

            this.visualReceiver.OnDaySplit(dayIndex);
        }

        internal async ValueTask TryUndoAsync()
        {
            using (this.actor.GetConcurrentLock("Undo..."))
            {
                await this.stateManager.TryUndoAsync().ConfigureAwait(false);
            }
        }

        internal async ValueTask TryRedoAsync()
        {
            using (this.actor.GetConcurrentLock("Redo..."))
            {
                await this.stateManager.TryRedoAsync().ConfigureAwait(false);
            }
        }

        public void DataSaved(string schedulePath)
        {
            using (this.stateManager.OpenScope($"Save {schedulePath}"))
            {
                // todo: fix this, for user it looks like save can be undone
                this.Schedule.DataSaved(schedulePath);
            }
        }

        internal async ValueTask LoadScheduleAsync(string schedulePath, CancellationToken token)
        {
            using (this.actor.GetConcurrentLock($"Loading {schedulePath} ..."))
            {
                long start = Stopwatch.GetTimestamp();

                this.logger.Verbose("Sending load request");
                Result<ScheduleJourney<TNodeId, TRoadId>> schedule_result = await this.planClient.LoadScheduleAsync(schedulePath, token).ConfigureAwait(false);

                await this.actor.AlertAsync(schedule_result.ProblemMessage).ConfigureAwait(false);

                if (schedule_result.HasValue)
                {
                    this.logger.Verbose($"Data loaded successfuly in {(Stopwatch.GetTimestamp() - start) / Stopwatch.Frequency}s");
                    var failure = schedule_result.Value.Route.Validate();
                    if (failure != null)
                        await this.actor.AlertAsync($"Loaded plan is invalid: {failure}", MessageLevel.Error).ConfigureAwait(false);

                    using (this.stateManager.OpenScope($"Load {schedulePath}"))
                    {
                        await this.Schedule.SetScheduleAsync(schedule_result.Value, schedulePath, modified: false).ConfigureAwait(false);
                    }
                }
            }
        }

        IDisposable IDataContext.NotifyOnChanging(object sender, string propertyName)
        {
            return this.stateManager.OpenScope(propertyName);
        }

        ValueTask IDataContext.NotifyOnChangedAsync(object sender, string propertyName)
        {
            return this.visualReceiver.NotifyOnChangedAsync(sender, propertyName);
        }

        internal async ValueTask ResetAnchorPlaceholderAsync()
        {
            using (this.stateManager.OpenScope("Reset placeholder"))
            {
                await this.Schedule.ResetAnchorPlaceholderAsync().ConfigureAwait(false);
            }
        }

        public async ValueTask SnapToPeakAsync(GeoZPoint peak, CancellationToken token)
        {
            var idx = this.auxState.Peaks.IndexOf(peak);
            if (idx == -1)
            {
                await this.actor.AlertAsync("Peak not found",MessageLevel.Warning).ConfigureAwait(false);
                return;
            }

            using (this.stateManager.OpenScope($"Snap to peak"))
            {
                this.auxState.RemovePeak(idx);
                var point = peak.Convert2d();
                await this.Schedule.SnapToLegAsync(computeSnapLegIndex(point),point, "", LabelSource.AutoMap,
                    Settings.PlannerPreferences.DefaultAnchorBreak).ConfigureAwait(false);
            }

        }
        public async ValueTask TryFindPeaksAsync(int dayIndex, int anchorIndex, CancellationToken token)
        {
            using (this.stateManager.OpenScope($"Find peaks"))
            {
                using (this.actor.GetConcurrentLock("Finding peaks..."))
                {
                    var pt = this.Schedule.Days[dayIndex].Anchors[anchorIndex].UserPoint;

                    var peak_result = await this.planClient.GetPeaksAsync(new PeaksRequest()
                    {
                        FocusPoint = pt,
                        Count = this.Schedule.Settings.PlannerPreferences.PeaksCount,
                        SearchRange = this.Schedule.Settings.PlannerPreferences.PeaksSearchRange,
                        SeparationDistance = this.Schedule.Settings.PlannerPreferences.PeaksSeparationDistance,
                    }, token);

                    await this.actor.AlertAsync(peak_result.ProblemMessage).ConfigureAwait(false);

                    if (peak_result.HasValue)
                    {
                        await addPeaksAsync(peak_result.Value.BaseHeight,
                            peak_result.Value.Peaks.Select(it => it.Point)).ConfigureAwait(false);
                    }
                }
            }
        }

        private async ValueTask addPeaksAsync(Length baseHeight, IEnumerable<GeoZPoint> peaks)
        {
            foreach (var peak in peaks)
            {
                await this.visualReceiver.OnPeakAddedAsync(peak).ConfigureAwait(false);
                this.auxState.AddPeak(peak);
            }
        }

        public async ValueTask ClearPeaksAsync(CancellationToken token)
        {
            using (this.stateManager.OpenScope($"Clear peaks"))
            {
                using (this.actor.GetConcurrentLock("Clearing peaks..."))
                {
                    this.auxState.ClearPeaks();
                    await this.visualReceiver.OnPeaksClearedAsync().ConfigureAwait(false);
                }
            }
        }

        public async ValueTask ClearAttractionsAsync(int? dayIndex)
        {
            using (this.stateManager.OpenScope($"Clear attractions"))
            {
                await this.Schedule.ClearAttractionsAsync(dayIndex).ConfigureAwait(false);
            }
        }

        public async ValueTask OrderAnchorAsync(int dayIndex, int anchorIndex, int change)
        {
            using (this.stateManager.OpenScope($"Reorder anchors"))
            {
                await this.Schedule.OrderAnchorAsync(dayIndex,anchorIndex, change).ConfigureAwait(false);
            }
        }
    }
}