using System.Collections.Generic;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;


namespace TrackPlanner.RestClient
{
    public interface IWorker<TNodeId,TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        Result<RouteResponse<TNodeId, TRoadId>> ComputeTrack(PlanRequest<TNodeId> request,bool mockReal,
            CancellationToken token,
             out List<List<FlowJump<TNodeId,TRoadId>>>? DEBUG_DailySteps);

        IEnumerable<List<PlacedAttraction>> GetAttractions(IReadOnlyList<GeoPoint> route,
            Length range, PointOfInterest.Feature excludeFeatures);
 
        PeaksResponse<TNodeId,TRoadId> FindPeaks(GeoPoint focusPoint, Length searchRange,Length separationDistance,
            int count);

        string? GetClosestCityName(GeoPoint point, Length range);
    }
}