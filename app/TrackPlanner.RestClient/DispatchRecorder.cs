using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Force.DeepCloner;
using TrackPlanner.RestClient.Commands;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;


namespace TrackPlanner.RestClient
{ 
    public sealed class DispatchRecorder<TNodeId, TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly ILogger logger;

        private readonly IVisualActor actor;
        // it is all twisted with internal interface, the reason for it is to guarantee
        // it is impossible to call a method on scheduler manager directly, i.e. skipping
        // recording of all commands executed
        
        internal ScheduleManager<TNodeId, TRoadId> Manager { get; }
        private readonly int limit;
        private readonly List<CommandHistory> history;
        private bool isHistoryTrimmed;
        public int HistoryCount => this.history.Count;

        internal DispatchRecorder(ILogger logger,IVisualActor actor,
            ScheduleManager<TNodeId, TRoadId> manager)
        {
            this.logger = logger;
            this.actor = actor;
            this.Manager = manager;
            this.limit = 1000;
            this.history = new List<CommandHistory>(capacity: limit);
        }
        
        public string StringifySchedule()
        {
            return String.Join("; ",Manager.Schedule.BuildPlanRequest<TNodeId>().GetPointsSequence()
                .Select(it => it.Stringify()));
        }
        
        public IEnumerable<string> HistoryAsCode()
        {
            var commands = new List<string>();
            foreach (var h in history)
            {
                if (h.CrashDetails is {} crash)
                {
                    commands.Add($"// CRASH: {crash}");
                }

                string comment = "";
                if (h.ResultType != typeof(ValueTuple))
                    comment = $" // -> {h.ResultType.Name}";
                commands.Add($"{h.Command?.SerializeAsCode()},{comment}");
            }

            var code = new List<string>();
            if (this.isHistoryTrimmed)
                code.Add("// trimmed");
            code.Add("// states");
            code.AddRange(this.history.Select(it => $"{it.State?.SerializeAsCode()},"));
            code.Add("// commands");
            code.Add($"// {nameof(Manager.Settings.PlannerPreferences.StartWithInsert)}: {Manager.Settings.PlannerPreferences.StartWithInsert}");
            code.Add($"// {nameof(Manager.Settings.PlannerPreferences.Home)}: {Manager.Settings.PlannerPreferences.Home}");
            code.AddRange(commands);
            return code;
        }
        public async ValueTask<Result<ValueTuple>> ExecuteAsync(VisualCommand command,CancellationToken token)
        {
            return await ExecuteAsync<ValueTuple>(command,token).ConfigureAwait(false);
        }

        public async ValueTask<Result<T>> ExecuteAsync<T>(VisualCommand command,CancellationToken token)
        {
            Result<T> result;
            try
            {
                 result = await executeAsync<T>(command, token).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                this.history[^1] = this.history[^1] with {CrashDetails = ex.ExtractDetails()};
                await actor.AlertAsync(ex.Message, MessageLevel.Error).ConfigureAwait(false);
                throw;
            }

            if (result.ProblemMessage is {} p)
            {
                this.logger.Log(p.Severity,p.FullMessage());
                await actor.AlertAsync(result.ProblemMessage).ConfigureAwait(false);
            }

            return result;
        }

        private async ValueTask<Result<T>> executeAsync<T>(VisualCommand command,CancellationToken token)
        {
            // why not "proper" methods per each command for execution? Because it has to have
            // the same signature across all commands, and for some we would like to return
            // the result. To achieve this we have to introduce secondary method, but then
            // either we expose Register method on dispatcher or duplicate all the methods from manager
            // this approach overall looks better, even if it is anti-pattern
            
            register<T>(command);

            Result<T> retype<TValue>(Result<TValue> result)
            {
                if (!result.HasValue)
                {
                    return result.FailAs<T>();// Result<T>.Fail(result.ProblemMessage, MessageSeverity);
                }
                else
                {
                    if (typeof(T) == typeof(ValueTuple))
                        return Result<T>.Valid(default(T)!,result.ProblemMessage);
                    if (result.Value is T retyped)
                        return Result<T>.Valid(retyped,result.ProblemMessage);
                    else
                        throw new ArgumentException($"Expected type {typeof(T).Name}, actual type {typeof(TValue).Name}");
                }
            }
            
            switch (command)
            {
                case  AddExternalAttraction cmd : await this.Manager.AddExternalAttractionAsync(cmd.Attraction).ConfigureAwait(false);
                    break;
                case  SnapToLineAt cmd : await this.Manager.SnapToLineAtAsync(cmd.Point).ConfigureAwait(false);
                    break;
                case  AddRouteAttraction cmd : await this.Manager.AddRouteAttractionAsync(cmd.LegIndex,cmd.AttrIndex,cmd.attraction).ConfigureAwait(false);
                    break;
                case  AddAnchorAtPlaceholder cmd : return retype(await this.Manager.AddAnchorAtPlaceholderAsync(cmd.Point).ConfigureAwait(false));
                case  AutoBuildSwitch cmd : return retype(await this.Manager.SetAutoBuildAsync(cmd.Enable).ConfigureAwait(false));
                case  ChangeAnchorPosition cmd : await this.Manager.ChangeAnchorPositionAsync(cmd.Point,cmd.DayIndex, cmd.AnchorIndex).ConfigureAwait(false);
                    break;
                case  ClearAttractions cmd : await this.Manager.ClearAttractionsAsync(cmd.DayIndex).ConfigureAwait(false);
                    break;
                case  OrderAnchor cmd : await this.Manager.OrderAnchorAsync(cmd.DayIndex,cmd.AnchorIndex,cmd.Change).ConfigureAwait(false);
                    break;
                case  CompleteRebuild cmd : return retype(await this.Manager.CompleteRebuildPlanAsync(directUserRequest:true).ConfigureAwait(false));
                case  DaySplitRemove cmd : await this.Manager.MergeDayToPreviousAsync(1+cmd.AnchorDayIndex).ConfigureAwait(false);
                    break;
                case  DaySplit cmd : await this.Manager.TrySplitDayAsync(cmd.CheckpointDayIndex, cmd.CheckpointIndex).ConfigureAwait(false);
                    break;
                case  PrepareAnchorAppend cmd : await this.Manager.PrepareAnchorAppendAsync(cmd.DayIndex, cmd.AnchorIndex).ConfigureAwait(false);
                    break;
                case  DeleteAnchor cmd : await this.Manager.TryDeleteAnchorAsync(cmd.DayIndex, cmd.AnchorIndex).ConfigureAwait(false);
                    break;
                case  FindPeaks cmd : await this.Manager.TryFindPeaksAsync(cmd.DayIndex,cmd.AnchorIndex,token).ConfigureAwait(false);
                    break;
                case  SnapToPeak cmd : await this.Manager.SnapToPeakAsync(cmd.Peak,token).ConfigureAwait(false);
                    break;
                case  ClearPeaks cmd : await this.Manager.ClearPeaksAsync(token).ConfigureAwait(false);
                    break;
                case  DeletePlaceholder cmd : await this.Manager.ResetAnchorPlaceholderAsync().ConfigureAwait(false);
                    break;
                case  FragmentInformationGetter cmd : return retype(this.Manager.GetFragmentInformation(cmd.LegIndex,cmd.FragmentIndex, cmd.Point));
                case LoopSwitch cmd:
                {
                    var settings = this.Manager.Settings.DeepClone();
                    settings.LoopRoute = cmd.IsLooped;
                    await this.Manager.SetSettingsAsync(settings).ConfigureAwait(false);
                    break;
                }
                case  NewProject cmd : await this.Manager.NewProjectAsync().ConfigureAwait(false);
                    break;
                case  Load cmd : await this.Manager.LoadScheduleAsync(cmd.Path, token).ConfigureAwait(false);
                    break;
                case  PartialBuild cmd : return retype(await this.Manager.PartialBuildPlanAsync(directUserRequest:true).ConfigureAwait(false));
                case  PinAutoAnchor cmd : await this.Manager.PinAnchorAsync(cmd.DayIndex, cmd.AnchorIndex).ConfigureAwait(false);
                    break;
                case  Redo cmd : await this.Manager.TryRedoAsync().ConfigureAwait(false);
                    break;
                case  FindRouteAttractions cmd : await this.Manager.FindAttractionsAsync(cmd.DayIndex,cmd.Category).ConfigureAwait(false);
                    break;
                case  Save cmd : await this.Manager.SaveScheduleAsync(cmd.Path, token).ConfigureAwait(false);
                    break;
                case  SetPlaceholder cmd : await this.Manager.SetAnchorPlaceholderAsync(cmd.DayIndex, cmd.AnchorIndex).ConfigureAwait(false);
                    break;
                case  SetSettings cmd : await this.Manager.SetSettingsAsync(cmd.Settings).ConfigureAwait(false);
                    break;
                case  Undo cmd :await this.Manager.TryUndoAsync().ConfigureAwait(false);
                    break;

                default: throw new NotImplementedException($"{command.GetType().Name}");
            }

            if (typeof(T) != typeof(ValueTuple))
                throw new NotImplementedException($"Unhandled command {command.GetType().Name} result type {typeof(T).Name}");
            
            return Result<T>.Valid( default(T)!);
        }


        private void register<T>(VisualCommand command)
        {
            if (this.history.Count == limit)
            {
                this.history.RemoveAt(0);
                this.isHistoryTrimmed = true;
            }

            this.history.Add(new CommandHistory(DebugState.Create(this.Manager.Schedule),
                command, typeof(T)));
        }
    }
}