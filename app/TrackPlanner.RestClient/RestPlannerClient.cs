using System;
using System.Threading;
using System.Threading.Tasks;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.RestSymbols;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.RestClient
{
    public sealed class RestPlannerClient<TNodeId, TRoadId> : PlanClient<TNodeId,TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly ILogger logger;
        private readonly JsonRestClient rest;
        private readonly string serverAddress;
        private readonly DraftRouter<TNodeId, TRoadId> draftRouter;

        public RestPlannerClient(ILogger logger, JsonRestClient rest, 
            string serverAddress)
        {
            this.logger = logger;
            this.rest = rest;
            this.serverAddress = serverAddress;
            this.draftRouter = new DraftRouter<TNodeId, TRoadId>(logger,
                new ApproximateCalculator(),
                new DraftOptions(),
                compactPreservesRoads: FinderConfiguration.Defaults().CompactPreservesRoads);
        }

        public override async ValueTask<Result<AttractionsResponse>> FindAttractionsAsync(InterestRequest request,
            CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Put_ComputeAttractions);
            return await rest.PutAsync<AttractionsResponse>(api_url, request, token).ConfigureAwait(false);
        }

        public async ValueTask<Result<AttractionsResponse>> GetLayerAsync(string path,
            CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Get_GetLayer);
            return await rest.GetAsync<AttractionsResponse>(api_url, 
                new RestQuery().Add(Parameters.Path, path),
                token).ConfigureAwait(false);
        }

        protected override ValueTask<Result<ValueTuple>> DoSaveScheduleAsync(ScheduleJourney<TNodeId,TRoadId> schedule,
            string path,CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Post_SaveSchedule);
            return rest.SuperPostAsync<ValueTuple>(api_url,
                new SaveRequest<TNodeId, TRoadId>() {Schedule = schedule, Path = path}, token);
        }
        public ValueTask<Result<ValueTuple>> SaveDebugAsync(string content,
            CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Post_SaveDebug);
            return rest.PostAsync<ValueTuple>(api_url,content, token);
        }

        public override async ValueTask<Result<PeaksResponse<TNodeId,TRoadId>>> GetPeaksAsync(PeaksRequest request,
            CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Put_FindPeaks);
            return await rest.SuperPutAsync<PeaksResponse<TNodeId,TRoadId>>(api_url, request, token).ConfigureAwait(false);
        }

        protected override ValueTask<Result<ScheduleJourney<TNodeId, TRoadId>>> DoLoadScheduleAsync(string path,CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Get_LoadSchedule);
            return this.rest.GetAsync<ScheduleJourney<TNodeId, TRoadId>>(api_url,
                new RestQuery().Add(Parameters.Path, path),
                token);
        }

        public override ValueTask<Result<DirectoryData>> GetScheduleDirectoryAsync(string directory,CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Get_ScheduleDirectory);
            return rest.GetAsync<DirectoryData>(api_url, new RestQuery().Add(Parameters.Directory, directory), token);
        }

        public override ValueTask<Result<DirectoryData>> GetLayersDirectoryAsync(string? directory, CancellationToken token)
        {
            var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Get_LayersDirectory);
            return this.rest.GetAsync<DirectoryData>(api_url,
                new RestQuery().Add(Parameters.Directory, directory),
                token);
        }

        public override async ValueTask<Result<RouteResponse<TNodeId, TRoadId>>> GetPlanAsync(PlanRequest<TNodeId> request,
            bool calcReal, CancellationToken token)
        {
            this.logger.Info($"Calculating route for {calcReal}");
            
            try
            {
                if (calcReal)
                {
                    var api_url = Url.Combine(this.serverAddress, Routes.Planner, Methods.Put_ComputeTrack);
                    var result = await rest.SuperPutAsync<RouteResponse<TNodeId, TRoadId>>(api_url,
                        request, token).ConfigureAwait(false);
                    if (!result.HasValue) 
                    {
                        logger.Error(result.ProblemMessage.EnsureFullMessage());
                        return result;
                    }
                    else if (result.Value.Route.IsEmpty())
                    {
                        return Result<RouteResponse<TNodeId, TRoadId>>.Fail("Empty route");
                    }
                    else
                    {
                        
                        var failure =  result.Value.Route.Validate();
                        if (failure != null)
                            logger.Error($"Plan received with {failure}.");

                        return result.WithMessage(failure);
                    }

                    // Console.WriteLine($"DEBUG, we have some auto anchors {response!.Route.Legs.Any(it => it.AutoAnchored)}");
                }
                else
                {
                    var response = this.draftRouter.BuildPlan(request,mockReal:false,token);
                    return Result<RouteResponse<TNodeId, TRoadId>>.Valid(response);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Result<RouteResponse<TNodeId, TRoadId>>.Fail($"Error {ex.Message}");
            }
        }

    }
}