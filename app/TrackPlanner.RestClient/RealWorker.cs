﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.Mapping;
using TrackPlanner.PathFinder;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Structures;
using TrackPlanner.Turner;

namespace TrackPlanner.RestClient
{ 
    public sealed class RealWorker<TNodeId, TRoadId> : IWorker<TNodeId, TRoadId>
        where TNodeId : struct, IEquatable<TNodeId>
        where TRoadId : struct, IEquatable<TRoadId>
    {
        private readonly ILogger logger;
        private readonly RouteManager<TNodeId, TRoadId> manager;
        private readonly AttractionsWorker<TNodeId, TRoadId> attrWorker;

        public RealWorker(ILogger? logger, RouteManager<TNodeId, TRoadId>? manager)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.manager = manager ?? throw new ArgumentNullException(nameof(manager));
            this.attrWorker = new AttractionsWorker<TNodeId, TRoadId>(logger, manager.Map, 
                manager.Calculator);
            logger.Info($"Starting {this}");
        }

      public  Result<RouteResponse<TNodeId, TRoadId>> ComputeTrack(PlanRequest<TNodeId> request,
          bool mockReal,
                CancellationToken token,
                // all legs per given day are merged here
                out List<List<FlowJump<TNodeId,TRoadId>>>? DEBUG_DailySteps)
        {
            //foreach (var pt in request.Points)
//                this.logger.Info($"{pt.UserPoint.Latitude} {pt.UserPoint.Longitude}");

            List<RequestPoint<TNodeId>> req_points = request.GetPointsSequence().ToList();
            var flat_res = manager.TryFindFlattenRoute(request.RouterPreferences, req_points,
                token);
            if (!flat_res.HasValue)
            {
                DEBUG_DailySteps = null;
                return flat_res.FailAs<RouteResponse<TNodeId, TRoadId>>();
            }

            var problem = flat_res.ProblemMessage;
            var legs = flat_res.Value.routeFlow;
            
#if DEBUG
            DEBUG_DailySteps = new List<List<FlowJump<TNodeId, TRoadId>>>();
#endif

            var turner = new NodeTurnWorker<TNodeId, TRoadId>(logger, manager.Map,
                new SystemTurnerConfig() {DebugDirectory = manager.DebugDirectory!},
                request.TurnerPreferences);

            var daily_turns = new List<List<TurnInfo<TNodeId, TRoadId>>>();

            {
                int leg_offset = 0;
                for (int day_idx = 0; day_idx < request.DailyPoints.Count; ++day_idx)
                {
                    int leg_count = ScheduleExtension.GetLegCount(day_idx, request.DailyPoints[day_idx].Count,
                        // the anchor is already added at the end when creating request
                        addLoopedAnchor: false);

                    var one_day_turns = new List<TurnInfo<TNodeId, TRoadId>>();

                    // one day could be segmented because of some draft legs
                    foreach (var daily_steps in
                             legs.Skip(leg_offset).Take(leg_count) // take daily portion
                                 .Partition(it => it.IsDraft) // segment it to real/draft
                                 .Select(it => it.SoftList()) 
                                 .Where(it => !it[0].IsDraft) // take only reals
                                 .Select(it => turner.MergeDailySteps(it))) 
                    {
#if DEBUG
                        DEBUG_DailySteps.Add(daily_steps);
#endif

                        Result<List<TurnInfo<TNodeId, TRoadId>>> turn_points_result = turner
                            .ComputeTurnPoints(daily_steps, token);
                        if (turn_points_result.HasValue)
                            one_day_turns.AddRange(turn_points_result.Value);

                        problem = problem.Override(turn_points_result.ProblemMessage);
                    }

                    daily_turns.Add(one_day_turns);
                    
                    leg_offset += leg_count;
                }
            }

            var plan = this.manager.CompactFlattenRoute(request.RouterPreferences,req_points,
                legs,token, out var p);
            
            problem = problem.Override(p.Override( plan.Validate( )));
            plan.DailyTurns = daily_turns;

            var response = new RouteResponse<TNodeId, TRoadId>()
            {
                Route = plan, // MapTranslator.ConvertToOsm(this.manager.Map, plan),
                Names = this.manager.FindNames(request, plan),
            };

            return Result<RouteResponse<TNodeId, TRoadId>>.Valid(response,problem);
        }

      public string? GetClosestCityName(GeoPoint point, Length range)
      {
          return this.manager.GetClosestCityName(point,range);
      }

      
        public IEnumerable<List<PlacedAttraction>> GetAttractions(IReadOnlyList<GeoPoint> route,
            Length range, PointOfInterest.Feature excludeFeatures)
        {
            foreach (var attr_leg in this.attrWorker.FindAttractions(route, range, excludeFeatures))
                yield return attr_leg.Select(it =>
                        new PlacedAttraction(
                            //MapTranslator.ConvertToOsm(this.manager.Map, it.place),
                            it.place.Point,
                            it.attraction))
                    .ToList();
        }

        public PeaksResponse<TNodeId,TRoadId> FindPeaks(GeoPoint focusPoint, Length searchRange,
            Length separationDistance, int count)
        {
            return this.manager.FindPeaks(focusPoint, searchRange, separationDistance, count);
        }
    }
}