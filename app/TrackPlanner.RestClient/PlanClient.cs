using System;
using System.Threading;
using System.Threading.Tasks;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.RestClient
{
    public abstract class PlanClient<TNodeId, TRoadId> : IPlanClient 
        where TNodeId : struct
        where TRoadId : struct
    {
        public string? LastUsedDirectory { get; private set; }
        
        public abstract ValueTask<Result<DirectoryData>> GetScheduleDirectoryAsync(string directory, CancellationToken token);
        public abstract ValueTask<Result<DirectoryData>> GetLayersDirectoryAsync(string? directory, CancellationToken token);
        public abstract ValueTask<Result<AttractionsResponse>> FindAttractionsAsync(InterestRequest request,
            CancellationToken token);
        public abstract  ValueTask<Result<RouteResponse<TNodeId, TRoadId>>> GetPlanAsync(PlanRequest<TNodeId> request,
            bool calcReal, CancellationToken token);
        public abstract   ValueTask<Result<PeaksResponse<TNodeId, TRoadId>>> GetPeaksAsync(PeaksRequest request,
            CancellationToken token);

        protected abstract   ValueTask<Result<ValueTuple>> DoSaveScheduleAsync(ScheduleJourney<TNodeId, TRoadId> schedule,
            string path, CancellationToken token);
        protected abstract ValueTask<Result<ScheduleJourney<TNodeId, TRoadId>>> DoLoadScheduleAsync(string path, CancellationToken token);

        public ValueTask<Result<ValueTuple>> SaveScheduleAsync(ScheduleJourney<TNodeId, TRoadId> schedule,
            string path, CancellationToken token)
        {
            this.LastUsedDirectory = System.IO.Path.GetDirectoryName(path);
            return DoSaveScheduleAsync(schedule, path, token);
        }

        public ValueTask<Result<ScheduleJourney<TNodeId, TRoadId>>> LoadScheduleAsync(string path, CancellationToken token)
        {
            this.LastUsedDirectory = System.IO.Path.GetDirectoryName(path);
            return DoLoadScheduleAsync( path, token);
        }
    }

    
}