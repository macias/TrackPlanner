using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.Structures;

namespace TrackPlanner.RestClient
{
    public readonly record struct ElevationData(
        IReadOnlyList<(string? label, Length distance, GeoZPoint point)> Line,
        IReadOnlyList<(string label, Length distance, Length height)> Points)
    {
        public static ElevationData Empty => new ElevationData(
            Array.Empty<(string? label, Length distance, GeoZPoint point)>(),
            Array.Empty<(string label, Length distance, Length height)>());

        /*public bool IsValid()
        {
            if (!isValid(Line.Select(it => it.distance)))
                return false;
            if (!isValid(Points.Select(it => it.distance)))
                return false;
            return true;
        }

        private static bool isValid(IEnumerable<Length> distances)
        {
            Length current = Length.MinValue;
            foreach (var d in distances)
            {
                if (current > d)
                    return false;
                current = d;
            }

            return true;
        }*/
    }
}