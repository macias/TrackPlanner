using Geo;

namespace TrackPlanner.RestClient.Commands
{



    public sealed record class ChangeAnchorPosition(GeoPoint Point, int DayIndex, int AnchorIndex) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({SerializeAsCode(Point)},{DayIndex},{AnchorIndex})";
        }
    }

}