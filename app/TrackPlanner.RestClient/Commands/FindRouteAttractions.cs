using TrackPlanner.Shared.Data;

namespace TrackPlanner.RestClient.Commands
{ 
    public sealed record class FindRouteAttractions(int DayIndex,PointOfInterest.Category Category) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({DayIndex},{Category})";
        }
    }
}