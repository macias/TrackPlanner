namespace TrackPlanner.RestClient.Commands
{
    public sealed record class AutoBuildSwitch(bool Enable) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({Enable.ToString().ToLower()})";
        }
    }
}