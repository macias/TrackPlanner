namespace TrackPlanner.RestClient.Commands
{


    public sealed record class DeletePlaceholder : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}()";
        }
    }
}