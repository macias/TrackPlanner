using TrackPlanner.Shared.Data;

namespace TrackPlanner.RestClient.Commands
{ 
    public sealed record class SnapToPeak(GeoZPoint Peak) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({SerializeAsCode(Peak)})";
        }
    }
}