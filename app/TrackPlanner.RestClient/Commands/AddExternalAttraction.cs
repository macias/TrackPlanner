using TrackPlanner.Shared.Data;

namespace TrackPlanner.RestClient.Commands
{ 
    public sealed record class AddExternalAttraction(PlacedAttraction Attraction) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({SerializeAsCode(Attraction)})";
        }
    }
}