using TrackPlanner.Shared.Stored;

namespace TrackPlanner.RestClient.Commands
{
    public sealed record class SetSettings(ScheduleSettings? Settings) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({Settings})";
        }
    }
}