namespace TrackPlanner.RestClient.Commands
{




    public sealed record class PinAutoAnchor(int DayIndex, int AnchorIndex) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({DayIndex},{AnchorIndex})";
        }
    }
}