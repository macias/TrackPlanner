namespace TrackPlanner.RestClient.Commands
{


    public sealed record class PartialBuild : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}()";
        }
    }
}