namespace TrackPlanner.RestClient.Commands
{ 
    public sealed record class ClearAttractions(int? DayIndex) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({SerializePrimitiveAsCode(DayIndex)})";
        }
    }
}