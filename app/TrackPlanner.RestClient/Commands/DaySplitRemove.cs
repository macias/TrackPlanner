namespace TrackPlanner.RestClient.Commands
{ 
    public sealed record class DaySplitRemove(int AnchorDayIndex) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({AnchorDayIndex})";
        }
    }
}