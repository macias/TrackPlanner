namespace TrackPlanner.RestClient.Commands
{
    // why not using only partial rebuild and dropping this command? This is because only when router see entire
    // request it can smooth out middle anchors. Partial rebuild is based on multiple requests with single leg each time
    // so smoothing is not possible
    public sealed record class CompleteRebuild : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}()";
        }
    }
}