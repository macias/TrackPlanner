namespace TrackPlanner.RestClient.Commands
{
    public sealed record class FindPeaks(int DayIndex,int AnchorIndex) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({DayIndex},{AnchorIndex})";
        }
    }
}