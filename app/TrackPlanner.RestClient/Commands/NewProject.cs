namespace TrackPlanner.RestClient.Commands
{
    public sealed record class NewProject : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}()";
        }
    }
}