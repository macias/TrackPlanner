namespace TrackPlanner.RestClient.Commands
{
    public sealed record class Undo : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}()";
        }
    }
}