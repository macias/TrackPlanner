using Geo;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.RestClient.Commands
{
    public abstract record class VisualCommand
    {
        public abstract string SerializeAsCode();

        public string Name => GetType().Name;

        private static string toCodeString(object? obj)
        {
            if (obj is string s)
                return $"\"{s}\"";
            else
                return obj?.ToString() ?? "null";
        }

        protected static string SerializePrimitiveAsCode<T>(T? value)
        {
            return value == null ? "null" : $"{value}";
        }

        protected static string SerializeAsCode(GeoPoint point)
        {
            return $"{nameof(GeoPoint)}.{nameof(GeoPoint.FromDegrees)}({DataFormat.RawFormat(point)})";
        }

        protected static string SerializeAsCode(GeoZPoint point)
        {
            return $"{nameof(GeoZPoint)}.{nameof(GeoZPoint.FromDegreesMeters)}({DataFormat.RawFormat(point)})";
        }

        protected static string SerializeAsCode(MapZPoint<OsmId,OsmId> point)
        {
            return $"new {nameof(MapZPoint<OsmId,OsmId>)}<OsmId,OsmId>( {SerializeAsCode(point.Point)},{toCodeString(point.NodeId)})";
        }

        protected static string SerializeAsCode(PointOfInterest interest)
        {
            return $"new {nameof(PointOfInterest)}({toCodeString(interest.Name)},{toCodeString(interest.Url)},{nameof(PointOfInterest)}.{nameof(PointOfInterest.Feature)}.{interest.Features})";
        }

        public static string SerializeAsCode(PlacedAttraction attraction)
        {
            return $"new {nameof(PlacedAttraction)}({SerializeAsCode(attraction.Point)},{SerializeAsCode(attraction.Attraction)})";
        }

        protected string SelfAsCode()
        {
            return this.GetType().Name;
        }
    }
}