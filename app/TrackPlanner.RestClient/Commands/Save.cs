namespace TrackPlanner.RestClient.Commands
{
    public sealed record class Save(string Path) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}(\"{Path}\")";
        }
    }
}