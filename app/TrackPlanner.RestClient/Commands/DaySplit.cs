namespace TrackPlanner.RestClient.Commands
{
    public sealed record class DaySplit(int CheckpointDayIndex, int CheckpointIndex) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({CheckpointDayIndex},{CheckpointIndex})";
        }
    }
}