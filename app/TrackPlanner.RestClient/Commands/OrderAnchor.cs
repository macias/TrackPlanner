namespace TrackPlanner.RestClient.Commands
{ 
    public sealed record class OrderAnchor(int DayIndex,int AnchorIndex, int Change) : VisualCommand
    {
        public static OrderAnchor Next(int DayIndex, int AnchorIndex)
        {
            return new OrderAnchor(DayIndex, AnchorIndex, +1);
        }
        public static OrderAnchor Previous(int DayIndex, int AnchorIndex)
        {
            return new OrderAnchor(DayIndex, AnchorIndex, -1);
        }
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({DayIndex},{AnchorIndex},{Change})";
        }
    }
}