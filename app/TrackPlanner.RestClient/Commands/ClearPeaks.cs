namespace TrackPlanner.RestClient.Commands
{
    public sealed record class ClearPeaks() : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}()";
        }
    }
}