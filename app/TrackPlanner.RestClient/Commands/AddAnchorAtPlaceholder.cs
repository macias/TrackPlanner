using Geo;

namespace TrackPlanner.RestClient.Commands
{


    public sealed record class AddAnchorAtPlaceholder(GeoPoint Point) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({SerializeAsCode(Point)})";
        }
    }
}