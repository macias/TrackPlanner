namespace TrackPlanner.RestClient.Commands
{
    public sealed record class LoopSwitch(bool IsLooped) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({IsLooped.ToString().ToLower()})";
        }
    }
}