namespace TrackPlanner.RestClient.Commands
{
    public sealed record class Redo : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}()";
        }
    }
}