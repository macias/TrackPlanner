namespace TrackPlanner.RestClient.Commands
{
    public sealed record class Load(string Path) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}(\"{Path}\")";
        }
    }
}