using TrackPlanner.Shared.Data;

namespace TrackPlanner.RestClient.Commands
{ 
    public sealed record class AddRouteAttraction(int LegIndex,int AttrIndex,PlacedAttraction attraction) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({LegIndex},{AttrIndex},{SerializeAsCode(attraction)})";
        }
    }
}