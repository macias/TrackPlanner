using Geo;

namespace TrackPlanner.RestClient.Commands
{

    public sealed record class FragmentInformationGetter(GeoPoint Point, int LegIndex, int FragmentIndex) : VisualCommand
    {
        public override string SerializeAsCode()
        {
            return $"new {SelfAsCode()}({SerializeAsCode(Point)},{LegIndex},{FragmentIndex})";
        }
    }
}