using System;
using System.Threading;
using System.Threading.Tasks;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;

namespace TrackPlanner.RestClient
{
    public static class PlanClientExtension
    {
        public static async ValueTask<Result<RouteResponse<TNodeId, TRoadId>>> EnsureGetPlanAsync<TNodeId, TRoadId>(this PlanClient<TNodeId, TRoadId> client, PlanRequest<TNodeId> request,
            bool calcReal, CancellationToken token)
            where TNodeId : struct
            where TRoadId : struct
        {
            var main_result = await client.GetPlanAsync(request, calcReal, token).ConfigureAwait(false);
            if (DEBUG_SWITCH.SubEnabled)
            {
                ;
            }

            if (main_result.HasValue || !calcReal)
                return main_result;
            var draft_result = await client.GetPlanAsync(request, calcReal: false, token).ConfigureAwait(false);
            if (draft_result.HasValue)
                return draft_result.WithMessage(main_result, force: true);
            else
                return main_result;
        }
    }
}