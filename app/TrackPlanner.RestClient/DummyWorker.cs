using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Geo;
using MathUnit;
using TrackPlanner.PathFinder;
using TrackPlanner.PathFinder.Drafter;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Responses;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.RestClient
{
    public sealed class DummyWorker<TNodeId,TRoadId> : IWorker<TNodeId,TRoadId>
        where TNodeId : struct
        where TRoadId : struct
    {
        private readonly ILogger? logger;
        private readonly DraftRouter<TNodeId,TRoadId> draftRouter;

        public DummyWorker(ILogger? logger,FinderConfiguration finderConfiguration,
            DraftOptions draftRealOptionses)
        {
            this.logger = logger ?? throw  new ArgumentNullException(nameof(logger));
            this.draftRouter = new DraftRouter<TNodeId,TRoadId>(logger,new ApproximateCalculator(),
                draftRealOptionses,
                finderConfiguration.CompactPreservesRoads);
            logger.Info($"Starting {this}");
        }
 
      public  Result<RouteResponse<TNodeId, TRoadId>> ComputeTrack(PlanRequest<TNodeId> request,bool mockReal,
            CancellationToken token,
            out List<List<FlowJump<TNodeId,TRoadId>>>? DEBUG_DailySteps)
        {
           var response = this.draftRouter.BuildPlan(request,mockReal, token);
            DEBUG_DailySteps = Array.Empty<List<FlowJump<TNodeId, TRoadId>>>().ToList();
            return Result<RouteResponse<TNodeId, TRoadId>>.Valid(response);
        }

        public IEnumerable<List<PlacedAttraction>> GetAttractions(IReadOnlyList<GeoPoint> route,
            Length range, PointOfInterest.Feature excludeFeatures)
        {
            var attractions = new List<List<PlacedAttraction>>();
            for (int i = 1; i < route.Count; ++i)
            {
                static PlacedAttraction get_attr(int seed, GeoPoint a, GeoPoint b)
                {
                    // we cannot randomize those points because on replay in unit test
                    // we could get different outcomes, so we just mess around with numbers
                    // but in 100% repeatable way
                    double lat, lon;
                    if (seed == 0)
                    {
                        lat = a.Latitude.Degrees + b.Longitude.Degrees;
                        lon = a.Longitude.Degrees + b.Latitude.Degrees;
                    }
                    else if (seed == 1)
                    {
                        lat = a.Longitude.Degrees + b.Latitude.Degrees;
                        lon = a.Latitude.Degrees + b.Longitude.Degrees;
                    }
                    else
                        throw new NotSupportedException();

                    lat = BigCoordRandomizer.South + AfterMath.TrueMod(lat - BigCoordRandomizer.South, BigCoordRandomizer.North - BigCoordRandomizer.South);
                    lon = BigCoordRandomizer.West + AfterMath.TrueMod(lon - BigCoordRandomizer.West, BigCoordRandomizer.East - BigCoordRandomizer.West);
                    var place = new MapZPoint<OsmId, OsmId>(
                        GeoZPoint.FromDegreesMeters(lat, lon, 0), nodeId: null
#if  DEBUG
, DEBUG_mapRef:null                    
#endif
                    );
                    var attr = new PlacedAttraction(place.Point, new PointOfInterest("dummy-attr", null, PointOfInterest.Feature.Bridge));
                    return attr;
                }

                attractions.Add(new List<PlacedAttraction>()
                {
                    get_attr(0, route[i - 1], route[i]),
                    get_attr(1, route[i - 1], route[i]),
                });
            }

            return attractions;
        }

        public PeaksResponse<TNodeId, TRoadId> FindPeaks(GeoPoint focusPoint, Length searchRange,
            Length separationDistance, int count)
        {
            return new PeaksResponse<TNodeId, TRoadId>()
            {
                Peaks = Array.Empty<MapZPoint<TNodeId, TRoadId>>(),
            };
        }

        public string? GetClosestCityName(GeoPoint point, Length range)
        {
            return null;
        }
    }

}