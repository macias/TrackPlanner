using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Force.DeepCloner;
using MathUnit;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Requests;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Shared.Visual;
using TrackPlanner.Structures;

namespace TrackPlanner.RestClient
{
    public static class ScheduleHelper
    {
        public static async ValueTask<(bool changed, ContextMessage? problem)>
            PartialRebuild<TNodeId, TRoadId>(PlanClient<TNodeId, TRoadId> planClient,
                VisualSchedule<TNodeId, TRoadId> visualSchedule,
                UserVisualPreferences visualPreferences,
                bool calcReal, CancellationToken token)
            where TNodeId : struct
            where TRoadId : struct
        {
            ContextMessage? first_problem = null;
            bool changed = false;

            Result<List<(PlanRequest<TNodeId> request, int legIndex)>> request_res
                = getPartialRawRequests(visualSchedule,
                    visualPreferences, calcReal);
            if (DEBUG_SWITCH.Enabled)
            {
                ;
            }

            first_problem ??= request_res.ProblemMessage;
            if (request_res.HasValue)
            {
                foreach (var (request, leg_idx) in request_res.Value)
                {
                    DEBUG_SWITCH.SubEnabled = DEBUG_SWITCH.Enabled && leg_idx == 0;

                    adjustForAdjacentLegs(visualSchedule, request, leg_idx,calcReal);

                    if (DEBUG_SWITCH.SubEnabled)
                    {
                        ;
                    }
                    var partial_result = await planClient.EnsureGetPlanAsync(request, calcReal,
                        token).ConfigureAwait(false);
                    first_problem ??= partial_result.ProblemMessage;

                    if (DEBUG_SWITCH.Enabled)
                    {
                        ;
                    }
                    if (!partial_result.HasValue)
                        continue;

#if DEBUG
                    partial_result.Value.Route.Legs[0].DEBUG_RequestStart 
                        = request.DailyPoints[0][0].UserPoint;
                    partial_result.Value.Route.Legs[^1].DEBUG_RequestEnd 
                        = request.DailyPoints[0][1].UserPoint;
#endif

                    await visualSchedule.ReplaceLegAsync(legIndex: leg_idx,
                            replacementLegs: partial_result.Value.Route.Legs,
                            partial_result.Value.Names)
                        .ConfigureAwait(false);
                    changed = true;
                }
            }

            if (calcReal)
            {
                var failure = visualSchedule.Route.Validate();
                if (failure != null)
                {
                    if (DEBUG_SWITCH.Enabled)
                    {
                        ;
                    }
                    first_problem ??= failure;
                }
            }

            return (changed, first_problem);
        }

        private static Result<List<(PlanRequest<TNodeId> request, int legIndex)>>
            getPartialRawRequests<TNodeId, TRoadId>(VisualSchedule<TNodeId, TRoadId> visualSchedule,
                UserVisualPreferences visualPreferences,
                bool calcReal)
            where TNodeId : struct
            where TRoadId : struct
        {
            // the reason we get only single partial request and not entire sequence
            // is because each request has to be "glued" to adjacent legs
            // so we have to know what we glue into (coordinates), and this is impossible
            // to predict in advance, so we create request one by one

            var router_prefs = visualSchedule.Settings.RouterPreferences.DeepClone();
            var turner_prefs = visualSchedule.Settings.TurnerPreferences.DeepClone();

            //var replacements = new List<(int index, List<LegPlan<long,long>> legs)>();


            var DEBUG_anchors = visualSchedule.Days.SelectMany(it => it.Anchors).ToList();
            if (visualSchedule.Settings.LoopRoute)
                DEBUG_anchors.Add(DEBUG_anchors[0]);

            var leg_idx = visualSchedule.Route.Legs.Count;

            if (DEBUG_anchors.Count != leg_idx + 1)
                return Result<List<(PlanRequest<TNodeId> request, int legIndex)>>.Fail($"Effective anchor count {DEBUG_anchors.Count} is not in sync with leg count {leg_idx}");

            var result = new List<(PlanRequest<TNodeId> request, int legIndex)>();
            foreach (var (next, prev) in visualSchedule
                         .CreateJourneySchedule(visualPreferences,
                             onlyPinned: false)
                         .BuildPlanRequest<TNodeId>().GetPointsSequence()
                         .Reverse()
                         .Slide())
            {
                --leg_idx;
                if (DEBUG_SWITCH.Enabled)
                {
                    ;
                }

                if (!visualSchedule.Route.Legs[leg_idx].NeedsRebuild(calcReal))
                    continue;

                //logger.LogDebug($"DEBUG, BuildPlanAsync {leg_idx}/{this.route.Legs.Count}, count {anchors_count}, markers loop {this.visualSchedule.Settings.LoopRoute}");
                if (!DEBUG_anchors[leg_idx].IsPinned || !DEBUG_anchors[leg_idx + 1].IsPinned)
                {
                    var (DEBUG_day_idx, DEBUG_anchor_idx) = visualSchedule.LegIndexToActualDayAnchor(leg_idx);
                    return Result<List<(PlanRequest<TNodeId> request, int legIndex)>>.Fail($"Impossible scenario, partial rebuilding on auto-anchors leg:{leg_idx} = {DEBUG_day_idx}:{DEBUG_anchor_idx} with {DEBUG_anchors[leg_idx].IsPinned} and {DEBUG_anchors[leg_idx + 1].IsPinned} of {visualSchedule.DEBUG_PinsToString()}.");
                }

                var prev_req = prev with {AllowSmoothing = false};
                var next_req = next with {AllowSmoothing = false};

                var request = new PlanRequest<TNodeId>()
                {
                    RouterPreferences = router_prefs,
                    TurnerPreferences = turner_prefs,
                    DailyPoints = new List<List<RequestPoint<TNodeId>>>()
                    {
                        new List<RequestPoint<TNodeId>>() {prev_req, next_req,}
                    }
                };

                result.Add((request, leg_idx));
            }

            return Result<List<(PlanRequest<TNodeId> request, int legIndex)>>.Valid(result);
        }

        private static void adjustForAdjacentLegs<TNodeId, TRoadId>(VisualSchedule<TNodeId, TRoadId> visualSchedule,
            PlanRequest<TNodeId> request,
            int legIndex,bool calcReal)
            where TNodeId : struct
            where TRoadId : struct
        {
            // we need to postpone setting point requests, because this dependent on adjacent legs
            // and adjacent legs will be computed (i.e. they are not know when creating all requests)

            var (day_idx,anchor_idx)=visualSchedule.LegIndexToActualDayAnchor(legIndex);
            if (DEBUG_SWITCH.SubEnabled)
            {
                ;
            }
            if (!request.DailyPoints[0][1].AllowGap)
            {
                var next_leg_idx = legIndex + 1;
                if (legIndex == visualSchedule.Route.Legs.Count 
                    && visualSchedule.IsLoopActivated())
                    next_leg_idx = 0;
                if (next_leg_idx < visualSchedule.Route.Legs.Count
                    && next_leg_idx != legIndex
                    && visualSchedule.Route.TryGetLegEdgeStep(next_leg_idx,
                        leg => !leg.NeedsRebuild(calcReal: calcReal),
                        first:true,bothSides:false,out var edge_step))
                {
                    // use computed point to avoid gaps and also enforce it, so the route finder
                    // won't jump over the point
                    ;
                    request.DailyPoints[0][1] = request.DailyPoints[0][1] with
                    {
                        UserPoint = edge_step.Point,
                        EnforcePoint = true,
                    };
                    if (DEBUG_SWITCH.SubEnabled)
                    {
                        ;
                    }
                }
            }

            if (!request.DailyPoints[0][0].AllowGap)
            {
                var prev_leg_idx = legIndex - 1;
                if (legIndex == -1 && visualSchedule.IsLoopActivated())
                    prev_leg_idx = visualSchedule.Route.Legs.Count - 1;
                if (prev_leg_idx >= 0
                    && prev_leg_idx != legIndex
                    && visualSchedule.Route.TryGetLegEdgeStep(prev_leg_idx,
                        leg => !leg.NeedsRebuild(calcReal: calcReal),
                        first:false,
                        bothSides:false,out var edge_step))
                {
                    request.DailyPoints[0][0] = request.DailyPoints[0][0] with
                    {
                        UserPoint =edge_step.Point,
                        EnforcePoint = true,
                    };
                }
            }
        }

        public static Result<ElevationData> GetElevationProfile<TNodeId, TRoadId>(VisualSchedule<TNodeId, TRoadId> visualSchedule, int dayIndex)
            where TNodeId : struct
            where TRoadId : struct
        {
            static bool try_to_add<T1, T3>(List<(T1 label, Length distance, T3 aux)> data,
                T1 label, Length distance, T3 aux)
            {
                if (data.Count > 0 && data[^1].distance > distance)
                    return false;
                data.Add((label, distance, aux));
                return true;
            }

            const string fail_message = "Invalid distances";

            IReadOnlyList<(string? label, Length distance, GeoZPoint point)> line;
            IReadOnlyList<(string label, Length distance, Length height)> points;


            {
                var data = new List<(string? label, Length distance, GeoZPoint point)>();
                var step_dist = Length.Zero;
                var chk_dist = Length.Zero;
                for (int idx = 0; idx < visualSchedule.GetSummary().Days[dayIndex].Checkpoints.Count; ++idx)
                {
                    if (idx > 0)
                    {
                        var leg_idx = visualSchedule.GetIncomingLegIndexByCheckpoint(dayIndex, idx,allowWrap:false);
                        foreach (var step in visualSchedule.Route.Legs[leg_idx!.Value].AllSteps().Skip(1).SkipLast(1))
                        {
                            step_dist += step.IncomingFlatDistance;
                            if (!try_to_add(data, null, step_dist, step.Point))
                                return Result<ElevationData>.Fail(fail_message);
                        }
                    }

                    var pt = visualSchedule.GetSummary().Days[dayIndex].Checkpoints[idx];
                    chk_dist += pt.IncomingDistance;
                    step_dist = chk_dist;
                    if (!try_to_add(data, $"{pt.Code} {pt.Label}", step_dist, pt.UserPoint))
                        return Result<ElevationData>.Fail(fail_message);
                }

                /*foreach (var step in this.visualSchedule.GetDayLegs(day_idx)
                             .SelectMany(it => it.AllSteps()))
                {
                    step_dist += step.IncomingFlatDistance;
                    data.Add((step_dist, step.Point));
                }*/

                line = data;
            }

            {
                var data = new List<(string label, Length distance, Length height)>();
                var dist = Length.Zero;
                foreach (var pt in visualSchedule.GetSummary().Days[dayIndex].Checkpoints)
                {
                    dist += pt.IncomingDistance;
                    if (!try_to_add(data, $"{pt.Code} {pt.Label}", dist, pt.UserPoint.Altitude))
                        return Result<ElevationData>.Fail(fail_message);
                }

                points = data;
            }

            return Result<ElevationData>.Valid(new ElevationData(line, points));
        }
    }
}