using System;
using Geo;

namespace TrackPlanner.RestClient
{
    public sealed class BigCoordRandomizer : CoordRandomizer
    {
        public static CoordRandomizer Instance { get; } = new BigCoordRandomizer();
        // suitable for mock router
        internal const double North = 53.56422;
        internal const double South = 52.6889;
        internal const double West = 19.12388;
        internal const double East = 17.69765;

        public override GeoPoint RandomizePoint(Random rnd)
        {
            return GeoPoint.FromDegrees(randomDouble(rnd, South, North),
                randomDouble(rnd, East, West));
        }
    }

}