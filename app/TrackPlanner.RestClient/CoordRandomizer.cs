using System;
using Geo;

namespace TrackPlanner.RestClient
{

    public abstract class CoordRandomizer
    {
        public abstract GeoPoint RandomizePoint(Random rnd);

        protected static double randomDouble(Random rnd, double min, double max)
        {
            return min + rnd.NextDouble() * (max - min);
        }
    }
}