using System;
using System.Linq;
using TrackPlanner.Shared.Data;


namespace TrackPlanner.RestClient
{
    public readonly record struct DebugState(int[] AnchorCount, int[] CheckpointCount,
        (int DayIndex, int AnchorIndex)? Placeholder, int LegCount)
    {
        // this type only exists because there is nasty bug in testing -- when running
        // monkey tests, program reports an error (exception caught) but when running the same scenario
        // as single test, everything is fine

        public static DebugState Create<TNodeId, TRoadId>(IReadOnlyScheduleSummary<TNodeId, TRoadId> schedule)
            where TNodeId : struct
            where TRoadId : struct
        {
            return new DebugState(schedule.Days.Select(it => it.Anchors.Count).ToArray(),
                schedule.GetSummary().Days.Select(it => it.Checkpoints.Count).ToArray(),
                schedule.AnchorPlaceholder,
                schedule.Route.Legs.Count);
        }

        public string SerializeAsCode()
        {
            string placeholder_str = Placeholder.HasValue ? Placeholder.Value.ToString() : "null";
            return $"new {nameof(DebugState)}(new []{{{(String.Join(",", AnchorCount))}}},new []{{{(String.Join(",", CheckpointCount))}}},{placeholder_str},{LegCount})";
        }

        public bool Equals(DebugState other)
        {
            return this.AnchorCount.Length == other.AnchorCount.Length
                   && Enumerable.SequenceEqual(this.AnchorCount, other.AnchorCount)
                   && this.CheckpointCount.Length == other.CheckpointCount.Length
                   && Enumerable.SequenceEqual(this.CheckpointCount, other.CheckpointCount)
                   && Placeholder == other.Placeholder
                   && this.LegCount == other.LegCount;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}