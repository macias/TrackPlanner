using System;
using TrackPlanner.RestClient.Commands;


namespace TrackPlanner.RestClient
{
    public readonly record struct CommandHistory(DebugState? State, 
        VisualCommand Command,
        Type ResultType,
        string? CrashDetails = null)
    {
    }
}