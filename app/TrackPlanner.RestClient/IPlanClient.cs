using System.Threading;
using System.Threading.Tasks;
using TrackPlanner.Shared;
using TrackPlanner.Shared.Data;

namespace TrackPlanner.RestClient
{
    public interface IPlanClient
    {
        string? LastUsedDirectory { get; }

        ValueTask<Result<DirectoryData>> GetScheduleDirectoryAsync(string directory, CancellationToken token);
        
        ValueTask<Result<DirectoryData>> GetLayersDirectoryAsync(string? directory, CancellationToken token);
    }

     
}