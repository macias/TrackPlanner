﻿
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using SharpKml.Base;
using SharpKml.Engine;
using TimeSpan = System.TimeSpan;
using TrackPlanner.Shared.Data;
using TrackPlanner.Shared.Stored;
using TrackPlanner.Structures;

namespace TrackPlanner.Shared.DataExchange
{
    public sealed class TrackWriter
    {
        private static LineDefinition segmentToKmlInput<TNodeId,TRoadId>(UserVisualPreferences visualPrefs, 
            LegFragment<TNodeId,TRoadId> fragment)
        where TNodeId:struct
        {
            string costFlags(CostInfo cost)
            {
                string s = "";
                if (cost.HasFlag(CostInfo.DangerousSpeed))
                    s += "D";
                if (cost.HasFlag(CostInfo.Suppressed))
                    s += "d";
                if (cost.HasFlag(CostInfo.UncomfortableSpeed))
                    s += "U";
                if (cost.HasFlag(CostInfo.HighTrafficBikeLane))
                    s += "B";
                return s;
            }

            var kml_lines = GetKmlSpeedLines(visualPrefs);
            var kml_forbidden = new KmlLineDecoration(new SharpKml.Base.Color32(visualPrefs.ForbiddenStyle.GetAbgrColor()), visualPrefs.ForbiddenStyle.Width);
            
                string name = (fragment.IsForbidden? visualPrefs.ForbiddenStyle: visualPrefs.SpeedStyles[fragment.SpeedStyling.Style]).Label;
                KmlLineDecoration style = fragment.IsForbidden? kml_forbidden : kml_lines[fragment.SpeedStyling.Style];
                
                
                name += $" {costFlags(fragment.CostInfo)}";

                string? description = null;
                if (fragment.RoadIds.Count == 1)
                    name += $" #{fragment.RoadIds.Single()}";
                else
                    description = String.Join(", ", fragment.RoadIds.Select(it => $"#{it}"));
                
                return new LineDefinition(fragment.GetSteps().Select(it => it.Point).ToArray(),
                    name,description, style);
            
        }

        public static Dictionary<RoadStyling, KmlLineDecoration> GetKmlSpeedLines(UserVisualPreferences visualPrefs)
        {
            return visualPrefs.SpeedStyles.ToDictionary(it => it.Key, it => new KmlLineDecoration(new SharpKml.Base.Color32(it.Value.GetAbgrColor()), it.Value.Width));
        }

        private static void saveAsKml<TNodeId, TRoadId>(UserVisualPreferences visualPrefs, Stream stream, string title,
            IEnumerable<SummaryCheckpoint> checkpoints,
            IEnumerable<LegPlan<TNodeId, TRoadId>> legs, IEnumerable<TurnInfo<TNodeId, TRoadId>>? turns)
            where TNodeId : struct
            where TRoadId : struct
        {
            bool for_trackradar = turns != null;
            
            legs = legs.Where(it => it.AllSteps().Any());

            var input = new TrackWriterInput() {Title = title};
            input.Lines.AddRange(legs.SelectMany(it => it.Fragments).Select(seg => segmentToKmlInput(visualPrefs, seg)));
            input.AddTurns(turns);
            input.Waypoints.AddRange(checkpoints
                .Select(it =>
                {
                    string name;
                    if (for_trackradar)
                        // "-" at start disables turn notification in TrackRadar
                        name = "-checkpoint";
                    else
                    {
                        name = $"{it.Label} at {DataFormat.Format(it.Arrival)}";
                        if (it.TotalBreakTime != TimeSpan.Zero)
                            name += $" >> {DataFormat.Format(it.Departure)}";
                    }

                    return new WaypointDefinition(it.UserPoint,
                        name,
                        description: null, PointIcon.StarIcon);
                }));

            input.SaveDecorated(stream);
        }

        public static void SaveAsKml<TNodeId,TRoadId>(UserVisualPreferences visualPrefs, Stream stream, string title, 
            IEnumerable<SummaryCheckpoint> checkpoints,
            IEnumerable<LegPlan<TNodeId,TRoadId>> legs,IEnumerable<TurnInfo<TNodeId,TRoadId>>? turns)
            where TNodeId:struct
        where TRoadId:struct
        {
            saveAsKml(visualPrefs,stream,title,checkpoints, legs,turns);
        }

        public static void SaveAsKml<TNodeId, TRoadId>(UserVisualPreferences visualPrefs, Stream stream, string title,
            RoutePlan<TNodeId, TRoadId> plan)
            where TNodeId : struct
            where TRoadId : struct
        {
            IEnumerable<SummaryCheckpoint> dummy_checkpoints = ImmutableArray<SummaryCheckpoint>.Empty;

            if (plan.Legs.Any())
                dummy_checkpoints = plan.Legs.Select(it => it.FirstStep())
                    .Concat(plan.Legs.Last().LastStep())
                    .Select(it => new SummaryCheckpoint(eventsCount: 0) {UserPoint = it.Point, Label = "-checkpoint"});

            SaveAsKml(visualPrefs, stream, title, dummy_checkpoints, plan.Legs, plan.DailyTurns?.SelectMany(x => x));
        }

        // https://github.com/samcragg/sharpkml/blob/main/docs/GettingStarted.md

        public static IEnumerable<(GeoZPoint point, string label)> LabelWaypoints(IEnumerable<GeoZPoint>? waypoints)
        {
            if (waypoints == null)
                yield break;

            int count = 0;
            foreach (GeoZPoint pt in waypoints)
                yield return (pt, $"Turn {count++}");
        }

        public static void Write(string filename, IReadOnlyList<GeoZPoint>? track, IEnumerable<GeoZPoint> waypoints,
            PointIcon? icon = null)
        {
            KmlFile kml = Build(track, waypoints, icon);
            kml.Save(filename);
        }

        public static KmlFile Build(IReadOnlyList<GeoZPoint>? track, IEnumerable<GeoZPoint>? waypoints, 
            PointIcon? icon = null)
        {
            return BuildLabeled(track, LabelWaypoints(waypoints), icon);
        }

        public static void WriteLabeled(string filename, IReadOnlyList<GeoZPoint>? track,
            IEnumerable<(GeoZPoint point, string label)>? waypoints, PointIcon? icon = null, Color32? color = null)
        {
            KmlFile kml = BuildLabeled(track, waypoints, icon);
            kml.Save(filename);
        }

        public static KmlFile BuildLabeled(IReadOnlyList<GeoZPoint>? track, IEnumerable<(GeoZPoint point, string label)>? waypoints, PointIcon? icon = null)
        {
            IEnumerable<(GeoZPoint point, string label, PointIcon icon)>? rich_waypoints = null;
            if (waypoints != null)
                rich_waypoints = waypoints.Select(it =>
                {
                    PointIcon point_icon = icon ?? PointIcon.DotIcon;
                    //  if (color.HasValue)
                    //    point_icon = new PointIcon(point_icon.Id, color.Value, point_icon.ImageUrl);
                    return (it.point, it.label, point_icon);
                });

            var tracks = new List<(IReadOnlyList<GeoZPoint>, string)>();
            if (track!=null)
                tracks.Add((track,"Track"));;
            KmlFile kml = BuildKml(tracks, rich_waypoints);
            return kml;
        }

        public static KmlFile BuildMultiple(IEnumerable<(IReadOnlyList<GeoZPoint> track, string label)>? tracks,
            IEnumerable<(GeoZPoint point, string label, PointIcon icon)>? waypoints)
        {
            KmlFile kml = BuildKml(tracks, waypoints);
            return kml;
        }

        public static KmlFile BuildKml(IEnumerable<(IReadOnlyList<GeoZPoint> track, string label)>? tracks, IEnumerable<(GeoZPoint point, string label, PointIcon icon)>? waypoints)
        {
            return BuildDecoratedKml(tracks?.Select(trk => (trk.track, trk.label, (KmlLineDecoration?)null)), waypoints);
        }
        public static KmlFile BuildDecoratedKml(IEnumerable<(IReadOnlyList<GeoZPoint> track, string label, KmlLineDecoration? style)>? tracks, 
            IEnumerable<(GeoZPoint point, string label, PointIcon icon)>? waypoints)
        {
            var input = new TrackWriterInput();
            if (tracks!=null)
                input.Lines.AddRange(tracks.Select(it => new LineDefinition(it.track,it.label, description:null,it.style)));
            if (waypoints!=null)
                input.Waypoints.AddRange(waypoints.Select(it => new WaypointDefinition(it.point,it.label,description:null,it.icon)));
            
            return input.BuildDecoratedKml();
        }
    }
}
